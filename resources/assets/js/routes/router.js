import Vue from 'vue'
import VurRouter from 'vue-router'


import Login from '../components/LoginComponent'
import LandingPage from '../components/HomeComponent'
import Master from '../components/inc/MasterComponent'
import Listing from '../components/ListingConponent'
import NewListing from '../components/NewListingComponent'
import Upgrade from '../components/UpgradeComponent'
import AccountVerification from '../components/AccountVerificationComponent'
import BookMarks from '../components/BookMarksComponent'
import MegaPhone from '../components/MegaPhoneComponent'
import NeedEntirePlace from '../components/NeedEntirePlaceComponent'
import NeedRoom from '../components/NeedRoomComponant'
import MyProfile from '../components/MyProfileComponent'
import OfferingEntirePlace from '../components/OfferingEntirePlaceComponent'
import OfferingRoom from '../components/OfferingRoomComponent'
import Settings from '../components/SettingsComponent'
import Support from '../components/SupportComponent'
import ListView from '../components/ListViewComponent'
import ChatApp from '../components/chat-app/ChatApp'
import ErrorPage from '../components/ErrorPageComponent'
import LoginMail from "../components/LoginMailComponent"
import RegisterMail from "../components/RegisterMailComponent";
import SingleListViewComponent from "../components/SingleListViewComponent";
import atenantsingleview from "../components/atenantsingleview";
import aroomsingleview from "../components/aroomsingleview";
import anEntirePlaceSingleView from "../components/anEntirePlaceSingleView";
import Tutorial from "../components/TutorialComponent";
import EditProfile from "../components/EditProfile";
import ListingEntirePlace from "../components/ListingEntirePlaceComponent";
import ListingAroomMate from "../components/ListingAroomMateComponent";
import ListingAtenant from "../components/ListingAtenantComponent";
import aroommatelistview from "../components/aroommatelistview";
import aroomlistview from "../components/aroomlistview";
import anEntirePlaceListView from "../components/anEntirePlaceListView";
import atenantlistview from "../components/atenantlistview";
import TryComponent from "../components/TryComponent";
import HomeSocialComponent from "../components/HomeSocialComponent";
import PrivacyComponent from "../components/PrivacyComponent";
import TermsComponent from "../components/TermsComponent"
import SingleUserProfile from "../components/SingleUserProfile";
import NeedApartmentEdit from "../components/NeedApartmentEdit";
import NeedRoomEdit from "../components/NeedRoomEdit";
import OfferingEntireplaceEdit from "../components/OfferingEntireplaceEdit";
import OfferingRoomEdit from "../components/OfferingRoomEdit";
import PaymentComponent from "../components/PaymentComponent";
import AroomMobileComponent from "../components/AroomMobileComponent";
import CommonSinglepageComponent from "../components/CommonSinglepageComponent";
Vue.use(VurRouter);
export const router = new VurRouter({
    mode: 'history',
    linkActiveClass: "active",
    routes:[
        {
            path: '/',
            name: 'Home',
            component: LandingPage,
            meta: {title: 'GettingRoom: Roommate Finder, Flatmates & Roommates'}
        },
        {
            path: '/social',
            name: 'HomeSocial',
            component: HomeSocialComponent,
            meta: {title: 'Getting Room | Home'}
        },
        {
            path: '/auth',
            name: 'Master',
            component: Master,
            redirect: '/login',
            children: [
                {
                    path: '/login',
                    name: 'Login',
                    component: Login,
                    meta: {title: 'Login'}
                },
                {
                    path: '/login/mail',
                    name: 'LoginMail',
                    component: LoginMail,
                    meta: {title: 'Login'}
                },
                {
                    path: '/register/mail',
                    name: 'RegisterMail',
                    component: RegisterMail,
                    meta: {title: 'Register'}
                },
                {
                    path: '/listing',
                    name: 'List',
                    redirect: '/listing/anywhere'
                },
                {
                    path: '/editprofile',
                    name: 'Editprofile',
                    component: EditProfile,
                    meta: {title: 'Edit Profile'}
                },
                {
                    path: '/listings/:id',
                    name: 'Listi',
                    props: true,
                    component: SingleListViewComponent,
                    meta: {title: 'Room share'}
                },
                {
                    path: '/profile/:id',
                    name: 'User',
                    props: true,
                    component: SingleUserProfile,
                    meta: {title: 'User'}
                },
                {
                    path: '/atenantsingleview/:id',
                    name: 'atenantsingleview',
                    props: true,
                    component: atenantsingleview,
                    meta: {title: ' Tenant'}
                },
                {
                    path: '/aroommobile',
                    name: 'AroomMobileComponent',
                    props: true,
                    component: AroomMobileComponent,
                    meta: {title: 'listing'}
                },
                {
                    path: '/aroom/:id',
                    name: 'aroomsingleview',
                    props: true,
                    component: aroomsingleview,
                    meta: {title: ' A Room'}
                },
                {
                    path: '/entireplace/:id',
                    name: 'anentireplacesingleview',
                    props: true,
                    component: anEntirePlaceSingleView,
                    meta: {title: ' A Room'}
                },
                {
                    path: '/listing/:place',
                    name: 'Listao',
                    component: Listing,
                    props: true,
                    meta: {title: 'Rooms in anywhere'}
                },
                {
                    path: '/haveshare/:id',
                    name: 'Listv',
                    component: Listing,
                    meta: {title: 'Room share'}
                },
                {
                    path: '/newlisting',
                    name: 'NewListing',
                    component: NewListing,
                    meta: {forUser:true,title: 'What are you looking for?'},
                },
                {
                    path: '/upgrade',
                    name: 'Upgrade',
                    component: Upgrade,
                    meta: {forUser:true,title: 'GettingRoom: Choose Package'}
                },
                {
                    path: '/accountverification',
                    name: 'AccountVerification',
                    component: AccountVerification,
                    meta: {forUser:true,title: 'Account Verification'}
                },
                {
                    path: '/bookmarks',
                    name: 'BookMarks',
                    component: BookMarks,
                    meta: {forUser:true,title: 'GettingRoom: Bookmarks'}
                },
                {
                    path: '/megaphone',
                    name: 'MegaPhone',
                    component: MegaPhone,
                    meta: {forUser:true,title: 'GettingRoom: Megaphone'}
                },
                {
                    path: '/tutorial',
                    name: 'tutorial',
                    component: Tutorial,
                    meta: {forUser:true,title: 'GettingRoom | Visual Tutorial'}
                },
                {
                    path: '/needentireplace',
                    name: 'NeedEntirePlace',
                    component: NeedEntirePlace,
                    meta: {forUser:true,title: 'NeedEntirePlace'}
                },
                {
                    path: '/listingentireplace',
                    redirect: '/listingentireplace/anywhere'
                },
                {
                    path: '/listingentireplace/:place',
                    name: 'ListingEntirePlace',
                    component: ListingEntirePlace,
                    props: true,
                    meta: {title: 'Places in anywhere'}
                },
                {
                    path: '/needroom',
                    name: 'NeedRoom',
                    component: NeedRoom,
                    meta: {forUser:true,title: 'Need A Room'}
                },
                {
                    path: '/myprofile',
                    name: 'MyProfile',
                    component: MyProfile,
                    meta: {forUser:true,title: 'My Profile'}
                },
                {
                    path: '/offeringentireplace',
                    name: 'OfferingEntirePlace',
                    component: OfferingEntirePlace,
                    meta: {forUser:true,title: 'Offering An EntirePlace'}
                },
                {
                    path: '/aroommate',
                    redirect: '/aroommate/anywhere'
                },
                {
                    path: '/aroommate/:place',
                    name: 'ListingAroomMateComponent',
                    props:true,
                    component: ListingAroomMate,
                    meta: {title: 'Roommate in anywhere'}
                },
                {
                    path: '/atenant',
                    redirect: '/atenant/anywhere',
                },
                {
                    path: '/atenant/:place',
                    name: 'ListingAtenantComponent',
                    component: ListingAtenant,
                    props: true,
                    meta: {title: 'Tenant in anywhere'}
                },
                {
                    path: '/offeringroom',
                    name: 'OfferingRoom',
                    component: OfferingRoom,
                    meta: {forUser:true,title: 'Offering A Room'}
                },
                {
                    path: '/offeringroom/edit/:id',
                    name: 'OfferingRoomEdit',
                    props: true,
                    component: OfferingRoomEdit,
                    meta: {forUser:true,title: 'Offering A Room'}
                },
                {
                    path: '/offerientireplace/edit/:id',
                    name: 'OfferingEntireplaceEdit',
                    props: true,
                    component: OfferingEntireplaceEdit,
                    meta: {forUser:true,title: 'Offering A Room'}
                },
                {
                    path: '/needroom/edit/:id',
                    name: 'NeedRoomEdit',
                    props: true,
                    component: NeedRoomEdit,
                    meta: {forUser:true,title: 'Offering A Room'}
                },
                {
                    path: '/needentireplace/edit/:id',
                    name: 'NeedApartmentEdit',
                    props: true,
                    component: NeedApartmentEdit,
                    meta: {forUser:true,title: 'Offering A Room'}
                },
                {
                    path: '/settings',
                    name: 'Settings',
                    component: Settings,
                    meta: {forUser:true,title: 'GettingRoom: Settings'}
                },
                {
                    path: '/support',
                    name: 'Support',
                    component: Support,
                    meta: {forUser:true,title: 'GettingRoom: Support'}
                },
                {
                    path: '/listview',
                    name: 'ListView',
                    component: ListView,
                    meta: {forUser:true,title: 'List View'}
                },
                {
                    path: '/aroommatelistview',
                    name: 'Aroommatelistview',
                    component: aroommatelistview,
                    meta: {title: 'A RoomMate'}
                },
                {
                    path: '/aroomlistview',
                    name: 'aroomlistview',
                    component: aroomlistview,
                    meta: {title: 'A Room'}
                },
                {
                    path: '/anentireplace',
                    name: 'anentireplacelistView',
                    component: anEntirePlaceListView,
                    meta: {title: 'A Room'}
                },
                {
                    path: '/privacy',
                    name: 'PrivacyComponentw',
                    component: PrivacyComponent,
                    meta: {title: 'Privacy'}
                },
                {
                    path: '/terms',
                    name: 'TermsComponent',
                    component: TermsComponent,
                    meta: {title: 'Terms'}
                },
                {
                    path: '/atenantlistview',
                    name: 'Atenantlistview',
                    component: atenantlistview,
                    meta: {forUser:true,title: 'Tenant List'}
                },
                {
                    path: '/chat',
                    name: 'Chat',
                    component: ChatApp,
                    meta: {forUser:true,title: 'GettingRoom: Mailbox'}
                },
                {
                    path: '/payment/:id',
                    name: 'Payment',
                    component: PaymentComponent,
                    props: true,
                    meta: {forUser:true,title: 'GettingRoom: Secure-payment'}
                },
                {
                    path: '/list/:id',
                    name: 'CommonList',
                    component: CommonSinglepageComponent,
                    props: true
                }
            ]
        },
        {
            path: '*',
            name  :'404',
            component: Master,
            redirect: '/404',
            children: [
                {
                    path: '404',
                    name: 'ERROR404',
                    component: ErrorPage,
                    meta: {title: 'Error 404'}
                },
            ]
        },
        {
            path: '/try',
            name: 'Trying',
            component: TryComponent,
            meta: {title: 'Try'}
        },

    ]
});