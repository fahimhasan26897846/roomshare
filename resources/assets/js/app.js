require('./bootstrap');
window.Vue = require('vue');
import BootstrapVue from 'bootstrap-vue'
import Jquery from 'jquery'
global.jquery = Jquery;
import {router} from './routes/router'
import {i18n} from "./plugin/i18n";
import VueCarousel from '@chenfengyuan/vue-carousel';
import * as VueGoogleMaps from 'vue2-google-maps';
import ImageUploader from 'vue-image-upload-resize'
import InputTag from 'vue-input-tag'
import Spinner from 'vue-spinkit'
var VueScrollTo = require('vue-scrollto');
import VueTelInput from 'vue-tel-input'

Vue.use(VueTelInput);

Vue.use(VueScrollTo, {
    container: "body",
    duration: 1500,
    easing: "ease-in-out",
    offset: 0,
    force: true,
    cancelable: true,
    onStart: false,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
});
Vue.component('Spinner', Spinner);
Vue.use(ImageUploader);
Vue.component('input-tag', InputTag);
import Dialog from 'vue-dialog-loading'
Vue.use(Dialog, {
    dialogBtnColor: '#0f0'
});
window.Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyCIpZ_VvjuDvVfg-hbbLkwGgoO7mWMzwJo',
        libraries: 'places',
    },
});
Vue.use(VueCarousel);
var token = window.localStorage.getItem('token');
router.beforeResolve((to, from, next) => {
    if (to.path) {
        if(to.matched.some(record => record.meta.forUser)){
            if(!token){
                next({
                    path: '/login'
                })
            }else next()
        }
    }
    document.title = to.meta.title;
    next()
});
window.trans = (string) => _.get(window.i18n, string);
Vue.component('App', require('./components/AppComponent'));
const app = new Vue({
    i18n,
    el: '#app',
    router
});