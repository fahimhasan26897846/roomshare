import Vue from 'vue'
import VueI18n from 'vue-i18n'
import en_us from '../lang/en-us.json'

Vue.use(VueI18n);

export const  i18n = new VueI18n({
    'locale':'en_us',
    'fallbackLocale': 'en_us',
    'messages':{
        en_us
    }
});