<!DOCTYPE html>
<html style="font-weight:300;">
<head style="font-weight:300;"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="utf-8" style="font-weight:300;">
    <meta name="viewport" content="width=device-width, initial-scale=1" style="font-weight:300;">
    <style style="font-weight:300;">
        * {
            font-weight: 300;
        }
        body {
            font-size: 16px; color: #2f2936; padding: 0; font-family: "Helvetica Neue", helvetica, sans-serif; -webkit-font-smoothing: antialiased; width: 100%; font-weight: 300; margin: 0;background-color: #f7f7f7;
        }
        td {
            padding: 0; font-weight: 300; margin: 0; text-align: center
        }
        h1 {
            font-size: 38px; color: #000; letter-spacing: -1px; padding: 0; font-weight: normal; margin: 0; line-height: 42px
        }
        h3 {
            font-size: 18px; font-weight: 700;margin: 0 0 20px;
        }
        p {
            font-weight: 300; margin: 0 0 15px; font-size: 16px; line-height: 24px
        }
        .preheader, .postfooter {
            padding: 0; font-size: 0; display: none; max-height: 0; font-weight: 300; mso-hide: all; line-height: 0
        }
        .main {
            border-radius: 4px; font-size: 16px; color: #2f2936; border-collapse: separate; box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1); border-spacing: 0; max-width: 700px; font-family: "Helvetica Neue", helvetica, sans-serif; border: 1px solid #c7d0d4; padding: 0; -webkit-font-smoothing: antialiased; width: 100%; font-weight: 300; margin: 15px auto;background-color: #fff;
        }
        .bottom {
            font-size: 14px; color: #2f2936; max-width: 700px; font-family: "Helvetica Neue", helvetica, sans-serif; padding: 0; -webkit-font-smoothing: antialiased; width: 100%; font-weight: 300; margin: 15px auto;
        }
        .header {
            padding: 23px 0; font-size: 14px; font-weight: 300;border-bottom: 1px solid #dee7eb;
        }
        .container {
            padding: 0 20px; max-width: 600px; font-weight: 300; margin: 0 auto;text-align: left;
        }
        .inner {
            padding: 30px 0 20px; font-weight: 300; background-color: #fff
        }
        .footer {
            padding: 35px 0; border-top: 1px solid #E7EBEE;font-weight: 300;
        }
        .btn {
            border-radius: 4px; box-shadow: 0 2px 0 rgba(0, 0, 0, 0.08); display: inline-block; color: #fff; font-size: 16px; text-decoration: none; padding: 8px 15px; font-weight: 600; border: 1px solid #346010; line-height: 18px; background-color: #4ba106
        }
        .hashtags {
            color: #4ba106; float: right; font-size: 16px; padding: 8px 15px; text-decoration: none; display: inline-block; font-weight: 400; margin: -3px 0 0; line-height: 42px;
        }
    </style>
</head>
<body dir="ltr" style='margin:0;font-family:"Helvetica Neue", helvetica, sans-serif;font-weight:300;width:100%;padding:0;font-size:16px;-webkit-font-smoothing:antialiased;color:#2f2936;background-color:#f7f7f7;'>
<div style="text-align:center;visibility:hidden;margin:0px;opacity:0;font-weight:300;width:1px;padding:0px;max-height:1px;font-size:0px;border-width:0px!important;line-height:0px!important;color:transparent;border:0;display:none!important;"><img border="0" width="1" height="1" src="http://trac.roomster.com/q/Ac4EEvrCflglr3TgXbWJBA~~/AAOdbwA~/RgRffMeAPVcDc3BjQgoAKIBCml2JY1coUhR5aGRnZGg1NmZnQGdtYWlsLmNvbVgEAAAAAA~~" style="font-weight:300;"></div>

<div class="preheader" style="font-weight:300;padding:0;max-height:0;font-size:0;mso-hide:all;line-height:0;display:none;">
    {{ucfirst($name)}}, please confirm email address.
</div>
<table class="main" style='margin:15px auto;font-weight:300;font-family:"Helvetica Neue", helvetica, sans-serif;width:100%;padding:0;max-width:700px;font-size:16px;-webkit-font-smoothing:antialiased;border-spacing:0;box-shadow:0 1px 3px rgba(0, 0, 0, 0.1);border-radius:4px;border-collapse:separate;color:#2f2936;border:1px solid #c7d0d4;background-color:#fff;'>
    <tr style="font-weight:300;">
        <td style="margin:0;font-weight:300;text-align:center;padding:0;">
            <div class="header" style="font-weight:300;padding:23px 0;font-size:14px;border-bottom:1px solid #dee7eb;">
                <div class="container" style="margin:0 auto;font-weight:300;text-align:left;padding:0 20px;max-width:600px;">
                    <h1 style="text-align: center; margin:0;font-weight:normal;padding:0;letter-spacing:-1px;font-size:38px;line-height:42px;color:#000;">
                        <a href="https://gettingroom.com" style="font-weight:500;text-decoration:none;color:#4ba106;">
                            <img alt="Roomster Logo" src="https://gettingroom.com/image/logo.png" width="140px" height="43px" style="font-weight:300;width:140px;height:43px;border:0;">
                        </a>
                    </h1>
                </div>
            </div>
        </td>
    </tr>
    <tr style="font-weight:300;">
        <td style="margin:0;font-weight:300;text-align:center;padding:0;">
            <div class="container" style="margin:0 auto;font-weight:300;text-align:left;padding:0 20px;max-width:600px;">
                <div class="inner" style="font-weight:300;padding:30px 0 20px;background-color:#fff;">
                    <h3 style="margin:0 0 20px;font-weight:700;font-size:18px;">Thank you for signing up.</h3>
                    <p style="margin:0 0 15px;font-weight:300;font-size:16px;line-height:24px;">In order to complete your registration, <a href="{{ env('APP_URL') }}/activate/{{ $activationCode }}" style="font-weight:300;">please confirm your email</a>.</p>
                    <a href="{{ env('APP_URL') }}/activate/{{ $activationCode }}" class="btn" style="font-weight:600;padding:8px 15px;text-decoration:none;font-size:16px;box-shadow:0 2px 0 rgba(0, 0, 0, 0.08);border-radius:4px;line-height:18px;color:#fff;border:1px solid #346010;background-color:#4ba106;display:inline-block;">Confirm</a>
                </div>
            </div>
            <div class="container" style="margin:0 auto;font-weight:300;text-align:left;padding:0 20px;max-width:600px;">
                <div class="footer" style="font-weight:300;padding:35px 0;border-top:1px solid #E7EBEE;">

                    <p style="margin:0 0 15px;font-weight:300;font-size:16px;line-height:24px;">You received this message because you are a member of GettingRoom.</p>
                </div>
            </div>

        </td>
    </tr>
</table>
<table class="bottom" style='margin:15px auto;font-weight:300;font-family:"Helvetica Neue", helvetica, sans-serif;width:100%;padding:0;max-width:700px;font-size:14px;-webkit-font-smoothing:antialiased;color:#2f2936;'>
    <tr style="font-weight:300;">
        <td style="margin:0;font-weight:300;text-align:center;padding:0;">
            <a href="#" style="font-weight:300;padding:5px;"><img alt="Download on the App Store" src="https://d1frv26ioc12ph.cloudfront.net/Content/images/email/Download_on_the_App_Store_Badge_US_UK.png" height="40" style="font-weight:300;"></a>
            <a href="#" style="font-weight:300;padding:5px;"><img alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/apps/en-play-badge.png" height="40" style="font-weight:300;"></a>
        </td>
    </tr>
</table>
<table class="bottom" style='margin:15px auto;font-weight:300;font-family:"Helvetica Neue", helvetica, sans-serif;width:100%;padding:0;max-width:700px;font-size:14px;-webkit-font-smoothing:antialiased;color:#2f2936;'>
    <tr style="font-weight:300;">
        <td style="margin:0;font-weight:300;text-align:center;padding:0;">
        </td>
    </tr>
</table>
<div class="postfooter" style="font-weight:300;padding:0;max-height:0;font-size:0;mso-hide:all;line-height:0;display:none;">

</div>

<img border="0" width="1" height="1" alt="" src="http://trac.roomster.com/q/PqHzsL7lDdjCA8ASQjklwQ~~/AAOdbwA~/RgRffMeAPlcDc3BjQgoAKIBCml2JY1coUhR5aGRnZGg1NmZnQGdtYWlsLmNvbVgEAAAAAA~~" style="font-weight:300;">
</body>
</html>
