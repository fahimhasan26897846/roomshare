@extends('affiliate.inc.master')
@section('content')
    <div id="container" class="affiliate-container">

        <div id="registrationWrapper">
            <div class="registrationWrapperHeaderHolder">
                <div class="registrationWrapperHeader">
                    Financial info
                </div>
                <!-- registrationWrapperHeader end -->
            </div>
            <!-- registrationWrapperHeaderHolder end -->
            <form method="POST">
                <div class="shortForm labelsWidth135">
                    <div class="formMargin">
                        <div class="fieldHolder">
                            <label class="boldLabel horizontalLabel" for="Payment_via">Payment via</label>
                            <input data-val="true" data-val-length="The field Payment via must be a string with a maximum length of 10." data-val-length-max="10" data-val-required="Required" id="PaymentType" name="PaymentType" type="hidden" value="PayPal">
                            <div style="width: 250px; font-weight: bold;padding-top: 10px;">PayPal</div>

                        </div>
                        <!-- WIRE -->
                        <div class="fieldHolder pmtWire" style="display: none;">
                            <label class="boldLabel horizontalLabel" for="AccountNumber">Account number</label>
                            <input class="textInput ignore" data-val="true" data-val-length="The field Account number must be a string with a maximum length of 200." data-val-length-max="200" id="AccountNumber" name="AccountNumber" placeholder="Account number" type="text" value="">
                            <span class="field-validation-valid" data-valmsg-for="AccountNumber" data-valmsg-replace="true"></span>
                        </div>
                        <div class="fieldHolder pmtWire" style="display: none;">
                            <label class="boldLabel horizontalLabel" for="AbaRouting">ABA/Routing</label>
                            <input class="textInput ignore" data-val="true" data-val-length="The field ABA/Routing must be a string with a maximum length of 20." data-val-length-max="20" id="AbaRouting" name="AbaRouting" placeholder="ABA/Routing" type="text" value="">
                            <span class="field-validation-valid" data-valmsg-for="AbaRouting" data-valmsg-replace="true"></span>
                        </div>
                        <div class="fieldHolder pmtWire" style="display: none;">
                            <label class="boldLabel horizontalLabel" for="BankName">Bank name</label>
                            <input class="textInput ignore" data-val="true" data-val-length="The field Bank name must be a string with a maximum length of 200." data-val-length-max="200" data-val-required="Required" id="BankName" name="BankName" placeholder="Bank name" type="text" value="">
                            <span class="field-validation-valid" data-valmsg-for="BankName" data-valmsg-replace="true"></span>
                        </div>
                        <div class="fieldHolder pmtWire" style="display: none;">
                            <label class="boldLabel horizontalLabel" for="Swift">SWIFT</label>
                            <input class="textInput ignore" data-val="true" data-val-length="The field SWIFT must be a string with a maximum length of 11." data-val-length-max="11" id="Swift" maxlength="11" name="Swift" placeholder="SWIFT" type="text" value="">
                            <span class="field-validation-valid" data-valmsg-for="Swift" data-valmsg-replace="true"></span>
                        </div>
                        <!-- CHECK -->
                        <div class="fieldHolder pmtCheck" style="display: none;">
                            <label class="boldLabel horizontalLabel" for="Address1">Address</label>
                            <input class="textInput ignore" data-val="true" data-val-length="The field Address must be a string with a maximum length of 100." data-val-length-max="100" data-val-required="Required" id="Address1" name="Address1" placeholder="Address" type="text" value="">
                            <span class="field-validation-valid" data-valmsg-for="Address1" data-valmsg-replace="true"></span>
                        </div>
                        <div class="fieldHolder pmtCheck" style="display: none;">
                            <label class="boldLabel horizontalLabel" for="City">City</label>
                            <input class="textInput ignore" data-val="true" data-val-length="The field City must be a string with a maximum length of 50." data-val-length-max="50" data-val-required="Required" id="City" name="City" placeholder="City" type="text" value="">
                            <span class="field-validation-valid" data-valmsg-for="City" data-valmsg-replace="true"></span>
                        </div>
                        <div class="fieldHolder pmtCheck" style="display: none;">
                            <label class="boldLabel horizontalLabel" for="Country">Country</label>
                            <select data-val="true" data-val-length="The field Country must be a string with a maximum length of 5." data-val-length-max="5" data-val-required="Required" id="Country" name="Country">
                                <option selected="selected" value="US">United States</option>
                                <option value="CA">Canada</option>
                                <option value="GB">United Kingdom</option>
                                <option value="AF">Afghanistan</option>
                                <option value="AL">Albania</option>
                                <option value="DZ">Algeria</option>
                                <option value="AS">American Samoa</option>
                                <option value="AD">Andorra</option>
                                <option value="AO">Angola</option>
                                <option value="AI">Anguilla</option>
                                <option value="AQ">Antarctica</option>
                                <option value="AG">Antigua And Barbuda</option>
                                <option value="AR">Argentina</option>
                                <option value="AM">Armenia</option>
                                <option value="AW">Aruba</option>
                                <option value="AU">Australia</option>
                                <option value="AT">Austria</option>
                                <option value="AZ">Azerbaijan</option>
                                <option value="BS">Bahamas</option>
                                <option value="BH">Bahrain</option>
                                <option value="BD">Bangladesh</option>
                                <option value="BB">Barbados</option>
                                <option value="BY">Belarus</option>
                                <option value="BE">Belgium</option>
                                <option value="BZ">Belize</option>
                                <option value="BJ">Benin</option>
                                <option value="BM">Bermuda</option>
                                <option value="BT">Bhutan</option>
                                <option value="BO">Bolivia</option>
                                <option value="BA">Bosnia And Herzegovina</option>
                                <option value="BW">Botswana</option>
                                <option value="BV">Bouvet Island</option>
                                <option value="BR">Brazil</option>
                                <option value="IO">British Indian Ocean Territory</option>
                                <option value="BN">Brunei Darussalam</option>
                                <option value="BG">Bulgaria</option>
                                <option value="BF">Burkina Faso</option>
                                <option value="BI">Burundi</option>
                                <option value="KH">Cambodia</option>
                                <option value="CM">Cameroon</option>
                                <option value="CV">Cape Verde</option>
                                <option value="KY">Cayman Islands</option>
                                <option value="CF">Central African Republic</option>
                                <option value="TD">Chad</option>
                                <option value="CL">Chile</option>
                                <option value="CN">China</option>
                                <option value="CX">Christmas Island</option>
                                <option value="CC">Cocos Keeling Islands</option>
                                <option value="CO">Colombia</option>
                                <option value="KM">Comoros</option>
                                <option value="CG">Congo</option>
                                <option value="CD">Congo, D.P.R</option>
                                <option value="CK">Cook Islands</option>
                                <option value="CR">Costa Rica</option>
                                <option value="CI">Cote D ivoire</option>
                                <option value="HR">Croatia</option>
                                <option value="CU">Cuba</option>
                                <option value="CY">Cyprus</option>
                                <option value="CZ">Czech Republic</option>
                                <option value="DK">Denmark</option>
                                <option value="DJ">Djibouti</option>
                                <option value="DM">Dominica</option>
                                <option value="DO">Dominican Republic</option>
                                <option value="TP">East Timor</option>
                                <option value="EC">Ecuador</option>
                                <option value="EG">Egypt</option>
                                <option value="SV">El Salvador</option>
                                <option value="GQ">Equatorial Guinea</option>
                                <option value="ER">Eritrea</option>
                                <option value="EE">Estonia</option>
                                <option value="ET">Ethiopia</option>
                                <option value="FK">Falkland Islands Malvinas </option>
                                <option value="FO">Faroe Islands</option>
                                <option value="FJ">Fiji</option>
                                <option value="FI">Finland</option>
                                <option value="FR">France</option>
                                <option value="GF">French Guiana</option>
                                <option value="PF">French Polynesia</option>
                                <option value="TF">French Southern Territories</option>
                                <option value="GA">Gabon</option>
                                <option value="GM">Gambia</option>
                                <option value="GE">Georgia</option>
                                <option value="DE">Germany</option>
                                <option value="GH">Ghana</option>
                                <option value="GI">Gibraltar</option>
                                <option value="GR">Greece</option>
                                <option value="GL">Greenland</option>
                                <option value="GD">Grenada</option>
                                <option value="GP">Guadeloupe</option>
                                <option value="GU">Guam</option>
                                <option value="GT">Guatemala</option>
                                <option value="GN">Guinea</option>
                                <option value="GW">Guinea-bissau</option>
                                <option value="GY">Guyana</option>
                                <option value="HT">Haiti</option>
                                <option value="HM">Heard And McDonald Islands</option>
                                <option value="HN">Honduras</option>
                                <option value="HK">Hong Kong</option>
                                <option value="HU">Hungary</option>
                                <option value="IS">Iceland</option>
                                <option value="IN">India</option>
                                <option value="ID">Indonesia</option>
                                <option value="IR">Iran, Islamic Republic Of</option>
                                <option value="IQ">Iraq</option>
                                <option value="IE">Ireland</option>
                                <option value="IL">Israel</option>
                                <option value="IT">Italy</option>
                                <option value="JM">Jamaica</option>
                                <option value="JP">Japan</option>
                                <option value="JO">Jordan</option>
                                <option value="KZ">Kazakstan</option>
                                <option value="KE">Kenya</option>
                                <option value="KI">Kiribati</option>
                                <option value="KP">Korea, D.P.R.</option>
                                <option value="KR">Korea, Republic Of</option>
                                <option value="KW">Kuwait</option>
                                <option value="KG">Kyrgyzstan</option>
                                <option value="LA">Lao</option>
                                <option value="LV">Latvia</option>
                                <option value="LB">Lebanon</option>
                                <option value="LS">Lesotho</option>
                                <option value="LR">Liberia</option>
                                <option value="LY">Libyan Arab Jamahiriya</option>
                                <option value="LI">Liechtenstein</option>
                                <option value="LT">Lithuania</option>
                                <option value="LU">Luxembourg</option>
                                <option value="MO">Macau</option>
                                <option value="MK">Macedonia</option>
                                <option value="MG">Madagascar</option>
                                <option value="MW">Malawi</option>
                                <option value="MY">Malaysia</option>
                                <option value="MV">Maldives</option>
                                <option value="ML">Mali</option>
                                <option value="MT">Malta</option>
                                <option value="MH">Marshall Islands</option>
                                <option value="MQ">Martinique</option>
                                <option value="MR">Mauritania</option>
                                <option value="MU">Mauritius</option>
                                <option value="YT">Mayotte</option>
                                <option value="MX">Mexico</option>
                                <option value="FM">Micronesia</option>
                                <option value="MD">Moldova, Republic Of</option>
                                <option value="MC">Monaco</option>
                                <option value="MN">Mongolia</option>
                                <option value="MS">Montserrat</option>
                                <option value="MA">Morocco</option>
                                <option value="MZ">Mozambique</option>
                                <option value="MM">Myanmar</option>
                                <option value="NA">Namibia</option>
                                <option value="NR">Nauru</option>
                                <option value="NP">Nepal</option>
                                <option value="NL">Netherlands</option>
                                <option value="AN">Netherlands Antilles</option>
                                <option value="NC">New Caledonia</option>
                                <option value="NZ">New Zealand</option>
                                <option value="NI">Nicaragua</option>
                                <option value="NE">Niger</option>
                                <option value="NG">Nigeria</option>
                                <option value="NU">Niue</option>
                                <option value="NF">Norfolk Island</option>
                                <option value="MP">Northern Mariana Islands</option>
                                <option value="NO">Norway</option>
                                <option value="OM">Oman</option>
                                <option value="PK">Pakistan</option>
                                <option value="PW">Palau</option>
                                <option value="PS">Palestine</option>
                                <option value="PA">Panama</option>
                                <option value="PG">Papua New Guinea</option>
                                <option value="PY">Paraguay</option>
                                <option value="PE">Peru</option>
                                <option value="PH">Philippines</option>
                                <option value="PN">Pitcairn</option>
                                <option value="PL">Poland</option>
                                <option value="PT">Portugal</option>
                                <option value="PR">Puerto Rico</option>
                                <option value="QA">Qatar</option>
                                <option value="RE">Reunion</option>
                                <option value="RO">Romania</option>
                                <option value="RU">Russian Federation</option>
                                <option value="RW">Rwanda</option>
                                <option value="SH">Saint Helena</option>
                                <option value="KN">Saint Kitts And Nevis</option>
                                <option value="LC">Saint Lucia</option>
                                <option value="PM">Saint Pierre And Miquelon</option>
                                <option value="WS">Samoa</option>
                                <option value="SM">San Marino</option>
                                <option value="ST">Sao Tome And Principe</option>
                                <option value="SA">Saudi Arabia</option>
                                <option value="SN">Senegal</option>
                                <option value="SC">Seychelles</option>
                                <option value="SL">Sierra Leone</option>
                                <option value="SG">Singapore</option>
                                <option value="SK">Slovakia</option>
                                <option value="SI">Slovenia</option>
                                <option value="SB">Solomon Islands</option>
                                <option value="SO">Somalia</option>
                                <option value="ZA">South Africa</option>
                                <option value="GS">South Georgia/Sandwich Islands</option>
                                <option value="ES">Spain</option>
                                <option value="LK">Sri Lanka</option>
                                <option value="VC">St Vincent/Grenadines</option>
                                <option value="SD">Sudan</option>
                                <option value="SR">Suriname</option>
                                <option value="SJ">Svalbard And Jan Mayen</option>
                                <option value="SZ">Swaziland</option>
                                <option value="SE">Sweden</option>
                                <option value="CH">Switzerland</option>
                                <option value="SY">Syrian Arab Republic</option>
                                <option value="TW">Taiwan</option>
                                <option value="TJ">Tajikistan</option>
                                <option value="TZ">Tanzania, United Republic Of</option>
                                <option value="TH">Thailand</option>
                                <option value="TG">Togo</option>
                                <option value="TK">Tokelau</option>
                                <option value="TO">Tonga</option>
                                <option value="TT">Trinidad And Tobago</option>
                                <option value="TN">Tunisia</option>
                                <option value="TR">Turkey</option>
                                <option value="TM">Turkmenistan</option>
                                <option value="TC">Turks And Caicos Islands</option>
                                <option value="TV">Tuvalu</option>
                                <option value="UG">Uganda</option>
                                <option value="UA">Ukraine</option>
                                <option value="AE">United Arab Emirates</option>
                                <option value="UY">Uruguay</option>
                                <option value="UM">US Minor Outlying Islands</option>
                                <option value="UZ">Uzbekistan</option>
                                <option value="VU">Vanuatu</option>
                                <option value="VA">Vatican City</option>
                                <option value="VE">Venezuela</option>
                                <option value="VN">Vietnam</option>
                                <option value="VG">Virgin Islands, British</option>
                                <option value="VI">Virgin Islands, U.S.</option>
                                <option value="WF">Wallis And Futuna</option>
                                <option value="EH">Western Sahara</option>
                                <option value="YE">Yemen</option>
                                <option value="YU">Yugoslavia</option>
                                <option value="ZM">Zambia</option>
                                <option value="ZW">Zimbabwe</option>
                            </select>
                        </div>
                        <div class="fieldHolder pmtCheck" style="display: none;">
                            <label class="boldLabel horizontalLabel" >State/Province</label>
                            <select data-val="true" data-val-length="The field State/Province must be a string with a maximum length of 5." data-val-length-max="5" data-val-required="Required" id="State" name="State">
                                <option selected="selected" value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AS">American Samoa</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="AA">Armed Forces - Americas</option>
                                <option value="AE">Armed Forces - Europe</option>
                                <option value="AP">Armed Forces - Pacific</option>
                                <option value="CA">California</option>
                                <option value="CO">Colorado</option>
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="DC">District of Columbia</option>
                                <option value="FM">Federated States of Micronesia</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="GU">Guam</option>
                                <option value="HI">Hawaii</option>
                                <option value="ID">Idaho</option>
                                <option value="IL">Illinois</option>
                                <option value="IN">Indiana</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="ME">Maine</option>
                                <option value="MH">Marshall Islands</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NV">Nevada</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NM">New Mexico</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="ND">North Dakota</option>
                                <option value="MP">Northern Mariana Islands</option>
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR">Oregon</option>
                                <option value="PW">Palau</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="PR">Puerto Rico</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VI">Virgin Islands of the U.S.A.</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                        </div>
                        <div class="fieldHolder pmtCheck" style="display: none;">
                            <label class="boldLabel horizontalLabel" for="Zip">Postal code</label>
                            <input class="textInput ignore" data-val="true" data-val-length="The field Postal code must be a string with a maximum length of 10." data-val-length-max="10" data-val-required="Required" id="Zip" name="Zip" placeholder="Postal code" type="text" value="">
                            <span class="field-validation-valid" data-valmsg-for="Zip" data-valmsg-replace="true"></span>
                        </div>
                        <!-- PAYPAL -->
                        <div class="fieldHolder pmtPayPal" style="display: block;">
                            <label class="boldLabel horizontalLabel" for="PaypalEmail">PayPal email</label>
                            <input class="textInput" data-val="true" data-val-length="The field PayPal email must be a string with a maximum length of 100." data-val-length-max="100" data-val-required="Required" id="PaypalEmail" name="PaypalEmail" placeholder="PayPal email" type="text" value="">
                            <span class="field-validation-valid" data-valmsg-for="PaypalEmail" data-valmsg-replace="true"></span>
                        </div>
                    </div>
                </div>
                <div class="buttons">
                    <input type="submit" value="Save" name="save" id="save" class="inputsubmit">
                </div>
            </form>
        </div>

    </div>
@endsection