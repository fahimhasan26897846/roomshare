<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js?20190108"></script>

    <script src="{{asset('affiliate/script/jquery-ui-1.10.3.custom.min.js')}}"></script>
    <script src="{{asset('affiliate/script/common.js')}}"></script>
    <script src="{{asset('affiliate/script/selectmenu.js')}}"></script>
    <script src="{{asset('affiliate/script/moon.js')}}"></script>
    <script src="{{asset('affiliate/script/routing.js')}}"></script>
    <link rel="stylesheet" href="{{asset('affiliate/css/affonts.css')}}">
    <link rel="stylesheet" href="{{asset('affiliate/css/afstyle.css')}}">
    <link rel="stylesheet" href="{{asset('affiliate/css/contentselector.css')}}">
    @yield('style')
    <title>Affiliate Programme</title>
</head>
<body data-gr-c-s-loaded="true">
<div class="errorNotifyBox hidden mainError" id="MainErrorMessage"></div>
@include('affiliate.inc.header')
@yield('content')
@include('affiliate.inc.footer')
</body>
<script src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071235486/?random=1565000835444&amp;cv=9&amp;fst=1565000835444&amp;num=1&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=768&amp;u_w=1366&amp;u_ah=728&amp;u_aw=1366&amp;u_cd=24&amp;u_his=13&amp;u_tz=360&amp;u_java=false&amp;u_nplug=3&amp;u_nmime=4&amp;sendb=1&amp;ig=1&amp;frm=0&amp;url=https%3A%2F%2Fwww.roomster.com%2Faffiliate%2Fdefault&amp;tiba=Roomster%20Affiliate%20Program&amp;rfmt=3&amp;fmt=4"></script>
@yield('script')
</html>