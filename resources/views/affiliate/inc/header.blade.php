<header>
    <div class="headerHolder">
        <nav>
            <ul class="userMenu ui-menu ui-widget ui-widget-content ui-corner-all" id="ui-id-5" role="menu" tabindex="0">
                <span class="helpdeskIconHolder ui-widget-content ui-menu-divider"></span>
                <li class="firstLine ui-menu-item" role="presentation">
                    <a aria-haspopup="true" id="ui-id-6" class="ui-corner-all" tabindex="-1" role="menuitem"><span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>
                        <span class="triangle-down"></span><span class="userImageMenu" onclick="$('.logOutSubMenu').toggle()"><img src="https://s.roomster.com/Content/images/placeholders/no_user_thumbnail.gif?20190108" width="52" height="52"></span>
                        <span class="clear"></span>
                    </a>
                    <ul class="submenu ui-menu ui-widget ui-widget-content ui-corner-all" role="menu" aria-hidden="true" aria-expanded="false" style="display: none;">
                        <li class="ui-menu-item" role="presentation">
                            <a href="{{url('affiliate/notifications')}}" id="ui-id-7" class="ui-corner-all" tabindex="-1" role="menuitem">Notifications</a>
                        </li>
                        <li class="ui-menu-item" role="presentation">
                            <a href="{{url('affiliate/support')}}" id="ui-id-8" class="ui-corner-all" tabindex="-1" role="menuitem">24/7 support<span class="helpdeskIconHolder"></span></a>
                        </li>
                        <li class="ui-menu-item" role="presentation">
                            <a href="{{url('affiliate/settings')}}" id="ui-id-9" class="ui-corner-all" tabindex="-1" role="menuitem">Account Settings</a>
                        </li>
                        <li class="ui-menu-item" role="presentation">
                            <a href="{{url('affiliate/financial')}}" id="ui-id-10" class="ui-corner-all" tabindex="-1" role="menuitem">Financial Info</a>
                        </li>
                        <li class="ui-menu-item" role="presentation">
                            <a href="#" class="logOutLink ui-corner-all" id="ui-id-11" tabindex="-1" role="menuitem">Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <a class="navLogo" href="{{url('affiliate/home')}}"></a>

            <div style="bottom: 0;position: absolute;font-size: 1.2em;padding-bottom: .8em; margin-left: -.4em;">
                <ul id="affiliate-navigation" class="ui-menu ui-widget ui-widget-content ui-corner-all" role="menu" tabindex="0">
                    <li class="ui-menu-item" role="presentation"><a href="{{url('affiliate/home')}}" class="ui-selected ui-corner-all" id="ui-id-1" tabindex="-1" role="menuitem">Home</a></li>
                    <li class="ui-menu-item" role="presentation"><a href="{{url('affiliate/campaigns')}}" class="ui-corner-all" id="ui-id-2" tabindex="-1" role="menuitem">Campaigns</a></li>
                    <li class="ui-menu-item" role="presentation"><a href="{{url('affiliate/promote')}}" class="ui-corner-all" id="ui-id-3" tabindex="-1" role="menuitem">Promote</a></li>
                    <li class="ui-menu-item" role="presentation"><a href="{{url('affiliate/rates')}}" class="ui-corner-all" id="ui-id-4" tabindex="-1" role="menuitem">Rates</a></li>
                </ul>
            </div>
            <script>
                $("#affiliate-navigation").menu();
            </script>

            <script>
                $(".userMenu").menu({
                    position: {
                        my: "right top",
                        at: "right+1 top+54"
                    }
                });
                $(".userMenu").unbind('mouseenter mouseleave');
            </script>
        </nav>
    </div>
    <!-- headerHolder end -->
    <div class="clear"></div>
</header>