<div id="mainFooter">
    <div class="footer-nav">
        <ul>
            <li><a href="{{url('affiliate/contact')}}">Contact</a></li>
            <li><a href="{{url('affiliate/terms')}}">Terms</a></li>
            <li><a href="{{url('affiliate/privacy')}}">Privacy</a></li>
        </ul>
    </div>
    <div class="footer-copy">
        Gettingroom © 2019
        <span class="social-follow">Follow us
            <a href="#" target="_blank"><i class="fa fa-facebook fa-lg"></i></a>
            <a href="#" target="_blank"><i class="fa fa-twitter fa-lg"></i></a>
            <a href="#" target="_blank"><i class="fa fa-google-plus fa-lg"></i></a>
            <a href="#" target="_blank"><i class="fa fa-vimeo fa-lg"></i></a>
            <a href="#" target="_blank"><i class="fa fa-youtube fa-lg"></i></a>
            <a href="#" target="_blank"><i class="fa fa-pinterest fa-lg"></i></a>
            <a href="#" target="_blank"><i class="fa fa-tumblr fa-lg"></i></a>
            <a href="#" target="_blank"><i class="fa fa-instagram fa-lg"></i></a>
        </span>
    </div>
</div>