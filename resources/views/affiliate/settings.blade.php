@extends('affiliate.inc.master')
@section('content')
    <div id="container" class="affiliate-container">

        <div id="registrationWrapper">
            <div class="registrationWrapperHeaderHolder">
                <div class="registrationWrapperHeader">
                    Change Password
                </div>
                <!-- registrationWrapperHeader end -->
            </div>
            <!-- registrationWrapperHeaderHolder end -->

            <form action="/Affiliate/Settings" method="post">

                <div class="shortForm_" style="margin-top: 35px;">
                    <div class="formMargin_ align-center">
                        <div class="fieldHolder" style="display: inline-block">
                            <input class="textInput" data-val="true" data-val-length="The field CurrentPassword must be a string with a maximum length of 100." data-val-length-max="100" data-val-remote="'CurrentPassword' is invalid." data-val-remote-additionalfields="*.CurrentPassword" data-val-remote-url="/Affiliate/VerifyPassword" data-val-required="Required" id="CurrentPassword" name="CurrentPassword" placeholder="Current Password" type="password" value="">
                            <span class="field-validation-valid" data-valmsg-for="CurrentPassword" data-valmsg-replace="true"></span>
                        </div>
                        <div class="clear"></div>
                        <div class="fieldHolder" style="display: inline-block">
                            <input class="textInput" data-val="true" data-val-length="Min 6 chars" data-val-length-max="100" data-val-length-min="6" data-val-required="Required" id="NewPassword" name="NewPassword" placeholder="New Password" type="password" value="">
                            <span class="field-validation-valid" data-valmsg-for="NewPassword" data-valmsg-replace="true"></span>
                        </div>
                        <div class="clear"></div>
                        <div class="fieldHolder" style="display: inline-block">
                            <input class="textInput" data-val="true" data-val-equalto="Passwords do not match" data-val-equalto-other="*.NewPassword" data-val-length="The field ConfirmPassword must be a string with a maximum length of 100." data-val-length-max="100" data-val-required="Required" id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm Password" type="password" value="">
                            <span class="field-validation-valid" data-valmsg-for="ConfirmPassword" data-valmsg-replace="true"></span>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="align-center stretch">
                    <input type="submit" value="Save" name="save" id="save" class="inputsubmit" style="display: inline-block">
                </div>

            </form>
        </div>

    </div>
@endsection