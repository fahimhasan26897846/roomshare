<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>
        Gettingroom Affiliate Program
    </title>
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css?20190108">
    <link type="text/css" rel="stylesheet" href="{{asset('affiliate/css/affonts.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('affiliate/css/common.css')}}">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js?20190108"></script>
    <script src="{{asset('affiliate/script/selectmenu.js')}}"></script>
    <script src="{{asset('affiliate/script/moon.js')}}"></script>
    <script src="{{asset('affiliate/script/routing.js')}}"></script>
    <script src="{{asset('affiliate/script/common.js')}}"></script>
    <link type="text/css" rel="stylesheet" href="http://cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css?20190108">
    <link type="text/css" rel="stylesheet" href="{{asset('affiliate/css/affiliates.css')}}">
    <script src="http://code.highcharts.com/highcharts.js?20190108"></script>
    <script src="http://code.highcharts.com/highcharts-more.js?20190108"></script>
    <script src="http://code.highcharts.com/modules/solid-gauge.js?20190108"></script>
    <script src="{{asset('affiliate/script/jquery-ui-1.10.3.custom.min.js')}}"></script>
    <script src="http://code.highcharts.com/modules/bullet.js?20190108"></script>
    <script src="http://cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js?20190108"></script>
    <script src="{{asset('affiliate/script/affiliates.js')}}"></script>
    <script>
        $(document).ready(function($) {
            $('input[placeholder]').placeholder();
        });
    </script>
    <style type="text/css">
        .errorNotifyBox {
            width: inherit;
            margin: 20px 0;
            padding: 13px 14px 13px 50px;
            box-shadow: none;
            border-radius: 0;
            background: url(/img/icons/errorMessImg.png) no-repeat 10px 10px, #ffeded;
        }

        .messageBoldText {
            font-weight: normal;
            font-size: 14px;
        }
    </style>

</head>
<body data-gr-c-s-loaded="true">

<noscript>

    <div class="noScriptWrap">
        <div class="noScriptBlock">
            <a href="http://www.enable-javascript.com/" target="_blank">Enable JavaScript in your browser in order to use GettingRoom</a>.
        </div>
    </div>

</noscript>

<div class="errorNotifyBox hidden mainError" id="MainErrorMessage"></div>
<header>
    <div class="headerHolder">
        <nav>
            <a class="navLogo" href="/Affiliate"></a>
        </nav>
    </div>
    <!-- headerHolder end -->
    <div class="clear"></div>
</header>

<div id="container" class="affiliate-container">

    <div>
        <div class="floatLeft" style="width: 60%;">
            <div style="padding: 30px 10px 10px 10px;">
                <h2>Join the Gettingroom affiliate program &amp; start making money on your site or blog right away!</h2>
                <p>Gettingroom is the largest shared housing multi platform company in the world. We take pride in knowing that we are one of the pioneers of the share economy. To continue that success, we are looking to partner with affiliates who will drive quality leads to Gettingroom. Quality leads are individuals looking for shared homes, furnished or unfurnished rooms and roommates to share with.
                </p>
                <div class="errorNotifyBox">
                    <div>Attention affiliates!</div>
                    <span class="messageBoldText">
			        We will not do business with any affiliates who post on Craigslist.<br>
			        We will not accept any leads that come from Craigslist.<br>
			        We will not pay for any leads that come from Craigslist.<br>
			        If you post on Craigslist your affiliate account will be terminated.<br><br>

                        December 15, 2017<br><br>

                        We recently became aware that an affiliate(s) is either posting, emailing or texting people on Craigslist.
                        This is not allowed. We have removed every affiliate who has been caught doing this in the past, and if we catch you we will remove your account.
                        If you are doing this you must stop at once!!! We do not want this type of traffic.<br><br>

                        John S. Shriber<br>
                        CEO, ROOMSTER.<br>

			        </span>
                </div>

            </div>
        </div>
        <div class="floatRight" style="width: 35%;">
            <div style="padding: 30px 10px 10px 10px;">
                <div class="box" style="padding: 10px 20px;">

                    <div>
                        <h2 class="textBig">Already an affiliate? Sign in to access your account.</h2>
                        <div>
                            <form action="{{url('affiliate/login')}}" method="post">
                                {{csrf_field()}}
                                <div class="fieldHolder">
                                    <input class="textInput emailIcon stretch valid" data-val="true" data-val-length="The field Email must be a string with a maximum length of 100." data-val-length-max="100" data-val-regex="The field Email must match the regular expression '^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$'." data-val-regex-pattern="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" data-val-required="The Email field is required." id="Email" name="Email" placeholder="Email" type="text" value="">
                                </div>
                                <div class="fieldHolder">
                                    <input class="textInput passwordIcon stretch valid" data-val="true" data-val-length="The field Password must be a string with a maximum length of 100." data-val-length-max="100" data-val-required="The Password field is required." id="Password" name="Password" placeholder="Password" type="password">
                                    <div class="marginMain">
                                        <div class="floatLeft">
                                            <input data-val="true" data-val-required="The Remember me field is required." id="Remember" name="Remember" type="checkbox" value="true">
                                            <input name="Remember" type="hidden" value="false">
                                            <label for="Remember">Remember me</label>
                                        </div>
                                        <div class="floatRight">
                                            <a href="{{url('affiliate/reset-password')}}">Forgot password?</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="fieldHolder">
                                    <input class="inputsubmit stretch" type="submit" value="Log in">

                                </div>
                            </form>

                        </div>

                    </div>
                </div>

                <div class="box" style="padding: 10px 20px;">
                   Dont have account? <a href="{{url('affiliate/register')}}">Register</a>
                </div>

            </div>
        </div>
        <div class="clear"></div>
    </div>

</div>

<div id="mainFooter">
    <div class="footer-nav">
        <ul>
            <li><a href="/affiliate/helpdesk">Contact</a></li>
            <li><a href="/affiliate/terms">Terms</a></li>
            <li><a href="/affiliate/privacy">Privacy</a></li>
        </ul>
    </div>
    <div class="footer-copy">
        GettingRoom © 2019
        <span class="social-follow">Follow us
            <a href="" target="_blank"><i class="fa fa-facebook fa-lg"></i></a>
            <a href="" target="_blank"><i class="fa fa-twitter fa-lg"></i></a>
            <a href="" target="_blank"><i class="fa fa-google-plus fa-lg"></i></a>
            <a href="" target="_blank"><i class="fa fa-vimeo fa-lg"></i></a>
            <a href="" target="_blank"><i class="fa fa-youtube fa-lg"></i></a>
            <a href="" target="_blank"><i class="fa fa-pinterest fa-lg"></i></a>
            <a href="" target="_blank"><i class="fa fa-tumblr fa-lg"></i></a>
            <a href="" target="_blank"><i class="fa fa-instagram fa-lg"></i></a>
        </span>
    </div>
</div>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>


</body>
</html>
