@extends('affiliate.inc.master')
@section('content')
    <div id="container" class="affiliate-container">

        <div class="content">

            <div class="box" style="padding: 1em; margin-bottom: 1em; border-color: #509E2A;">
                <div class="title">Attention all affiliates!</div>
                Roomster accepts leads of people that NEED Roommates, Room shares, Flat shares &amp; Apartment Shares. (Rooms / Shared &amp; Rooms Wanted).

                <ul style="padding-top: .5em;">
                    <li><strong>- We will no longer accept Apartment &amp; Flat wanted leads.</strong></li>
                    <li><strong>- All leads are reviewed by Roomster. We will terminate any affiliate account that sends fake leads.</strong></li>
                    <li><strong><span style="color: red;">- We will terminate any affiliate account that spams or posts on Facebook.</span></strong></li>
                    <li><strong><span style="color: #509E2A;">- We are so excited to announce new features to the affiliate program to help you get more leads and earn more money.
                                                                Links are now coded differently, but don’t worry! Any links you have in the old format will still work.
                                                                Please note that there is a new <a href="Promote">Generate links</a> tab in the Promote section. You can now build links to send traffic directly to specific areas of the site and to download mobile apps.</span></strong>

                    </li>
                    <li><strong><span style="color: red;">- No posting of our links is allowed! You must use a referring URL. We search for our links everyday. If we find them we remove you and withhold all payments.</span></strong></li>
                    <li><strong><span style="color: #509E2A;">- You MUST use links generated in the <a href="promote">"Promote"</a> section. </span></strong></li>
                    <li><span style="color: blue;"><strong>- September 27, 2017</strong>:

                    <p>
                        Affiliates are over posting and spamming FB groups. Do not post links in comments. Any affiliate who posts links in comments will be blocked &amp; account removed. You may only post rooms for rent, or people who need rooms (with pictures) and a link to the correct listing on Roomster.
                    </p>

                    </span></li>

                    <li>
                        1. Find a listing on Roomster.com that matches the city of the group you are posting in.
                        <br> 2. Use the <a href="promote">"Promote"</a> tab and create a "Listing page link". Enter in the "listing ID #" then generate a link. Click "?" for a visual.
                        <br> 3. Then use another URL, then also use tiny URL - this is the safe way!
                        <br> 4. Post the link in a FB group with Pictures.
                        <br>
                        <br> This will send traffic directly to the listings on Roomster and you will get leads. Post a listing in ever city group.
                    </li>

                    <li><span style="color: red;"><strong>- October 10, 2017</strong>:

                    <p>
                        FB group admins are complaining that Roomster affiliates are posting in the comments of groups. This is NOT allowed. You must post “rooms for rent” or “people looking for rooms”.  We are checking &amp; will remove any affiliate who is posting in comments!
                    </p>

                    </span></li>
                    <li><span><strong>- November 8, 2017</strong>:
                    <p>
                        We have raised <a style="font-weight: bolder;" href="rates">payout rates</a> in all markets. Get more leads from international markets. Post in all native languages &amp; create landing sites in localized languages. This will generate the most leads.
                    </p>

                    </span></li>
                    <li><span style="color: red;"><strong>- November 28, 2017</strong>:

                    <p>
                        We removed many affiliates today who posted auto-redirecting links on FB like <a href="https://goo.gl/f2WuK9">https://goo.gl/f2WuK9</a>. You must post links like <a href="http://www.crazroom.com/"> http://www.crazroom.com/</a>.
                    </p>

                    </span></li>

                    <li><span style="color: green;"><strong>- December 13, 2017</strong>:

                    <p>
                        All affiliates must remove the “Roomster” name and “logo” from all blogs &amp; sites. There can be no word that says “Roomster” found.
                    </p>

                    </span></li>

                    <li><span style="color: red;"><strong>- August 20, 2018</strong>:

                    <p>
                        We have learned that an affiliate is sending or posting links on another roommate site. Roomster will terminate all affiliates who do this.
                    </p>

                    </span></li>
                    <li><span style="color: red;"><strong>- August 28, 2018</strong>:

                    <p>
                        ATTENTION: This week we removed 7 affiliate accounts who sent fake leads. If you send fake leads you will lose your account and will not be paid.
                    </p>

                    </span></li>
                    <li><span style="color: green;"><strong>- September 13, 2018</strong>:

                    <p>
                        Yesterday we pushed a long anticipated upgrade of the Roomster site! It now offers a much faster and better user experience. There was a period of down time and we are sorry for the inconvenience. Thank you for your patience.
                    </p>

                    </span></li>
                    <li><span style="color: red;"><strong>- September 24, 2018</strong>:

                    <p>
                        ATTENTION: Any affiliate account posting or sending messages on other roommate matching sites will be terminated immediately.
                    </p>

                    </span></li>

                    <li><span style="color: orange;"><strong>- May 7, 2019</strong>:

                    <p>
                        This week several affiliates have been terminated for sending fake leads.
                    </p>

                    </span></li>
                </ul>

            </div>

            <div style="float: right;" class="clear"><a href="rates">Payout rates</a> current as of July 23, 2019</div>

            <div class="title">Estimated earnings</div>
            <div class="affiliate-earnings clearfix">
                <div class="earning floatLeft">
                    <div class="date-range-title"><a href="/affiliate/campaigns/2019-08-05/2019-08-05">Today so far</a></div>
                    <div class="date-range-amount">$0.80</div>
                    <div class="date-range-delta"></div>
                    <div class="date-range-comment"></div>
                </div>

                <div class="earning floatLeft down">
                    <div class="date-range-title"><a href="/affiliate/campaigns/2019-08-04/2019-08-04">Yesterday</a></div>
                    <div class="date-range-amount">
                        $13.60 <span class="arrow"></span></div>
                    <div class="date-range-delta">-$8.00 (37.0%)</div>
                    <div class="date-range-comment">vs same day last week</div>
                </div>

                <div class="earning floatLeft up">
                    <div class="date-range-title"><a href="/affiliate/campaigns/2019-07-30/2019-08-05">Last 7 days</a></div>
                    <div class="date-range-amount">
                        $110.40 <span class="arrow"></span></div>
                    <div class="date-range-delta">+$39.20 (55.1%)</div>
                    <div class="date-range-comment">vs previous 7 days</div>
                </div>

                <div class="earning floatLeft down">
                    <div class="date-range-title"><a href="/affiliate/campaigns/2019-07-09/2019-08-05">Last 28 days</a></div>
                    <div class="date-range-amount">
                        $189.60 <span class="arrow"></span></div>
                    <div class="date-range-delta">-$1382.25 (87.9%)</div>
                    <div class="date-range-comment">vs previous 28 days</div>
                </div>
            </div>

            <div class="title">Performance</div>

            <div class="performance floatLeft clear">
                <div class="selectric-wrapper">
                    <div class="selectric-hide-select">
                        <select id="performance-date-ranges" tabindex="0">
                            <option value="Today">Today so far</option>
                            <option value="Yesterday">Yesterday vs same day last week</option>
                            <option value="Last7Days">Last 7 days vs previous 7 days</option>
                            <option value="Last28Days">Last 28 days vs previous 28 days</option>
                        </select>
                    </div>
                    <div class="selectric">
                        <p class="label">Today so far</p><b class="button">▾</b></div>
                    <div class="selectric-items" tabindex="-1">
                        <div class="selectric-scroll">
                            <ul>
                                <li data-index="0" class="selected">Today so far</li>
                                <li data-index="1" class="">Yesterday vs same day last week</li>
                                <li data-index="2" class="">Last 7 days vs previous 7 days</li>
                                <li data-index="3" class="last">Last 28 days vs previous 28 days</li>
                            </ul>
                        </div>
                    </div>
                    <input class="selectric-input" tabindex="0">
                </div>
            </div>

            <div class="affiliate-performance clearfix">
                <div class="performance floatLeft">
                    <div class="box">

                        <div id="performance-Today" class="performance-data clearfix">

                            <div class="floatLeft" style="width: 50%">
                                <div class="date-range-title">Leads</div>
                                <div class="date-range-amount">1</div>
                                <div class="date-range-delta">&nbsp;</div>
                            </div>

                        </div>

                        <div id="performance-Yesterday" class="performance-data clearfix" style="display: none;">

                            <div class="floatLeft down" style="width: 50%">
                                <div class="date-range-title">Leads</div>
                                <div class="date-range-amount">17 <span class="arrow"></span></div>
                                <div class="date-range-delta">-10 (37.0%)</div>
                            </div>

                        </div>

                        <div id="performance-Last7Days" class="performance-data clearfix" style="display: none;">

                            <div class="floatLeft up" style="width: 50%">
                                <div class="date-range-title">Users</div>
                                <div class="date-range-amount">138 <span class="arrow"></span></div>
                                <div class="date-range-delta">+49 (55.1%)</div>
                            </div>

                        </div>

                        <div id="performance-Last28Days" class="performance-data clearfix" style="display: none;">

                            <div class="floatLeft down" style="width: 50%">
                                <div class="date-range-title">Users</div>
                                <div class="date-range-amount">237 <span class="arrow"></span></div>
                                <div class="date-range-delta">-1103 (82.3%)</div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="performance floatLeft">
                    <div class="box">
                        <div style="padding-bottom: .5em;">
                            <div class="date-range-title">Payout via <a href="/Affiliate/FinancialInfo">PayPal (mrobiul.alam56@gmail.com)</a> </div>

                            <div class="date-range-amount current-payout"><a href="rates">View payout rates</a></div>
                            <div class="date-range-comment">&nbsp;</div>

                            <div id="payouts-Yesterday" class="payouts-data" style="display: none;">

                            </div>
                            <div id="payouts-Last7Days" class="payouts-data " style="display: none;">

                            </div>
                            <div id="payouts-Last28Days" class="payouts-data " style="display: none;">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="affiliate-performance clearfix" style="display: none;">

                <div class="performance floatLeft">
                    <div class="box">
                        <div class="date-range-title">Quality Score <i class="quality-score-help fa fa-question-circle" title=""></i></div>
                        <div id="quality-score"></div>
                    </div>
                </div>

                <div class="performance floatLeft">
                    <div class="box">
                        <div class="date-range-title">Incentive Meter <i class="insentive-meter-help fa fa-question-circle" title=""></i></div>
                        <div class="performance-wrapper">
                            <div id="insentive-meter-disabled" style="display: none;">Disabled. Requires quality score of "Good" or above.</div>
                            <div id="insentive-meter-date">Ends 05 August 2019</div>
                            <div id="insentive-meter"></div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="affiliate-performance clearfix">
                <div class="box">
                    <div style="border: 1px solid #ccc;border-radius: 1em;padding: 1em;margin-bottom: 1em;background-color: #fffcf0;"><i class="fa fa-exclamation-circle" aria-hidden="true" style="color: red;"></i> <strong>Important:</strong> Please note that the numbers shown below are estimated and are subject to change as the numbers get reviewed before the invoice is generated and the payment is issued.</div>
                    <div class="insentive-meter-date floatRight">Ends 05 August 2019</div>
                    <div class="date-range-title">Incentive Meter <i class="insentive-meter-help fa fa-question-circle" title=""></i></div>
                    <div class="loader" style="margin: 5em auto; width: 5em; display: none;"><i class="fa fa-refresh fa-spin fa-2x"></i></div>
                    <div class="incentive-rates">
                        <div class="incentive-graph" id="incentive-graph-0" data-highcharts-chart="0" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-0" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-1-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 336.5 65 336.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 336.5 15 L 336.5 65 537.5 65 537.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 537.5 15 L 537.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="236" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.80</text>
                                    <text x="437" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.92</text>
                                    <text x="664" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 184.5 15 L 184.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 235.5 15 L 235.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 285.5 15 L 285.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 336.5 15 L 336.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 386.5 15 L 386.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 436.5 15 L 436.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 487.5 15 L 487.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 537.5 15 L 537.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 587.5 15 L 587.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 638.5 15 L 638.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 688.5 15 L 688.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 739.5 15 L 739.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-1-)">
                                            <rect x="17.5" y="79.5" width="15" height="576" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="150" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(576,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>286</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">USA</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="185.38461538462" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="235.76923076923" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="286.15384615385" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="336.53846153846" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="386.92307692308" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="437.30769230769" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="487.69230769231" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="538.07692307692" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="588.46153846154" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="638.84615384615" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="689.23076923077" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                        <text x="739.61538461538" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">300</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">325</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-1" data-highcharts-chart="1" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-3" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-4-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.20</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.31</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-4-)">
                                            <rect x="17.5" y="653.5" width="15" height="2" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(2,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>1</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">United Kingdom</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-2" data-highcharts-chart="2" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-6" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-7-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.20</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.31</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-7-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Ireland</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-3" data-highcharts-chart="3" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-9" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-10-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.20</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.31</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-10-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Italy</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-4" data-highcharts-chart="4" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-12" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-13-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.20</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.31</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-13-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Japan</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-5" data-highcharts-chart="5" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-15" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-16-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.20</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.31</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-16-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Norway</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-6" data-highcharts-chart="6" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-18" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-19-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.20</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.31</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-19-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Spain</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-7" data-highcharts-chart="7" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-21" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-22-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.20</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.31</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-22-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Sweden</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-8" data-highcharts-chart="8" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-24" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-25-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-25-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Australia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-9" data-highcharts-chart="9" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-27" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-28-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-28-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Austria</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-10" data-highcharts-chart="10" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-30" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-31-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-31-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Denmark</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-11" data-highcharts-chart="11" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-33" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-34-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-34-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Finland</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-12" data-highcharts-chart="12" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-36" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-37-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-37-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">France</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-13" data-highcharts-chart="13" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-39" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-40-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-40-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Germany</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-14" data-highcharts-chart="14" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-42" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-43-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-43-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Hong Kong</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-15" data-highcharts-chart="15" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-45" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-46-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-46-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Israel</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-16" data-highcharts-chart="16" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-48" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-49-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-49-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Netherlands</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-17" data-highcharts-chart="17" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-51" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-52-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-52-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">New Zealand</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-18" data-highcharts-chart="18" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-54" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-55-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-55-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Poland</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-19" data-highcharts-chart="19" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-57" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-58-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-58-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Russia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-20" data-highcharts-chart="20" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-60" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-61-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-61-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Switzerland</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-21" data-highcharts-chart="21" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-63" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-64-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.14</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-64-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">U. A. Emirates</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-22" data-highcharts-chart="22" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-66" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-67-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.92</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.05</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.15</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-67-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Liechtenstein</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-23" data-highcharts-chart="23" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-69" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-70-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.92</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.05</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.15</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-70-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Lithuania</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-24" data-highcharts-chart="24" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-72" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-73-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.92</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.05</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.15</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-73-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Montenegro</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-25" data-highcharts-chart="25" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-75" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-76-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.91</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.04</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.13</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-76-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Latvia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-26" data-highcharts-chart="26" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-78" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-79-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.90</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.03</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.12</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-79-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Albania</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-27" data-highcharts-chart="27" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-81" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-82-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.90</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.03</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.12</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-82-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Belgium</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-28" data-highcharts-chart="28" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-84" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-85-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.90</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.03</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.12</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-85-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Bulgaria</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-29" data-highcharts-chart="29" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-87" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-88-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.90</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.03</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.12</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-88-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Croatia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-30" data-highcharts-chart="30" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-90" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-91-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.90</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.03</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.12</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-91-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Hungary</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-31" data-highcharts-chart="31" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-93" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-94-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.90</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.03</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.12</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-94-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Monaco</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-32" data-highcharts-chart="32" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-96" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-97-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.90</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.03</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.12</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-97-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Romania</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-33" data-highcharts-chart="33" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-99" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-100-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.90</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.03</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.12</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-100-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Singapore</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-34" data-highcharts-chart="34" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-102" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-103-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.85</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.97</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.06</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-103-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Greenland</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-35" data-highcharts-chart="35" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-105" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-106-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.80</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.92</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-106-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Canada</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-36" data-highcharts-chart="36" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-108" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-109-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.80</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.92</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-109-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Georgia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-37" data-highcharts-chart="37" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-111" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-112-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.80</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.92</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-112-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Greece</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-38" data-highcharts-chart="38" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-114" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-115-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.80</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.92</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-115-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Iceland</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-39" data-highcharts-chart="39" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-117" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-118-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.80</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.92</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-118-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Luxembourg</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-40" data-highcharts-chart="40" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-120" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-121-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.80</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.92</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$1.00</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-121-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Portugal</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-41" data-highcharts-chart="41" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-123" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-124-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.75</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.86</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.93</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-124-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Czech Republic</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-42" data-highcharts-chart="42" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-126" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-127-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.75</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.86</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.93</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-127-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Serbia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-43" data-highcharts-chart="43" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-129" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-130-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.75</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.86</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.93</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-130-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Slovakia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-44" data-highcharts-chart="44" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-132" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-133-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.75</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.86</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.93</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-133-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Slovenia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-45" data-highcharts-chart="45" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-135" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-136-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.70</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.80</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.87</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-136-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">South Africa</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-46" data-highcharts-chart="46" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-138" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-139-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.70</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.80</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.87</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-139-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">South Korea</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-47" data-highcharts-chart="47" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-141" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-142-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.70</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.80</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.87</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-142-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Ukraine</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-48" data-highcharts-chart="48" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-144" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-145-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.50</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.57</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.62</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-145-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Belarus</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-49" data-highcharts-chart="49" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-147" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-148-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.50</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.57</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.62</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-148-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Bolivia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-50" data-highcharts-chart="50" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-150" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-151-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.50</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.57</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.62</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-151-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Moldova, Repu…</tspan>
                                            <title>Moldova, Republic of</title>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-51" data-highcharts-chart="51" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-153" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-154-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.50</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.57</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.62</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-154-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Puerto Rico</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-52" data-highcharts-chart="52" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-156" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-157-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.35</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.40</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.43</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-157-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Venezuela</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-53" data-highcharts-chart="53" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-159" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-160-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.30</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.34</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.37</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-160-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Andorra</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-54" data-highcharts-chart="54" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-162" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-163-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.30</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.34</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.37</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-163-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Anguilla</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-55" data-highcharts-chart="55" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-165" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-166-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.30</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.34</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.37</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-166-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Argentina</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-56" data-highcharts-chart="56" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-168" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-169-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.30</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.34</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.37</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-169-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Armenia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-57" data-highcharts-chart="57" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-171" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-172-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.30</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.34</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.37</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-172-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Azerbaijan</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-58" data-highcharts-chart="58" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-174" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-175-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.30</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.34</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.37</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-175-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Belize</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-59" data-highcharts-chart="59" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-177" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-178-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.30</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.34</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.37</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-178-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Bosnia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-60" data-highcharts-chart="60" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-180" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-181-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.30</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.34</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.37</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-181-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Panama</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-61" data-highcharts-chart="61" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-183" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-184-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.30</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.34</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.37</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-184-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Taiwan</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-62" data-highcharts-chart="62" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-186" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-187-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.20</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.23</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-187-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Brazil</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-63" data-highcharts-chart="63" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-189" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-190-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.20</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.23</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-190-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Chile</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-64" data-highcharts-chart="64" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-192" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-193-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.20</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.23</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-193-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">China</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-65" data-highcharts-chart="65" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-195" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-196-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.20</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.23</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-196-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Colombia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-66" data-highcharts-chart="66" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-198" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-199-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.20</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.23</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-199-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">El Salvador</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-67" data-highcharts-chart="67" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-201" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-202-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.20</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.23</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-202-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Paraguay</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-68" data-highcharts-chart="68" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-204" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-205-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.20</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.23</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.25</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-205-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Peru</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-69" data-highcharts-chart="69" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-207" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-208-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.10</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.11</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.12</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-208-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Malaysia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-70" data-highcharts-chart="70" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-210" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-211-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.10</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.11</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.12</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-211-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Thailand</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-71" data-highcharts-chart="71" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-213" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-214-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.10</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.11</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.12</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-214-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Turkey</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-72" data-highcharts-chart="72" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-216" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-217-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.10</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.11</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.12</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-217-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Vietnam</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-73" data-highcharts-chart="73" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-219" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-220-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.06</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-220-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">India</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-74" data-highcharts-chart="74" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-222" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-223-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.06</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-223-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Kazakhstan</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-75" data-highcharts-chart="75" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-225" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-226-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.06</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-226-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Kyrghyzstan</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-76" data-highcharts-chart="76" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-228" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-229-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.06</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-229-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Mexico</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-77" data-highcharts-chart="77" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-231" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-232-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.06</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-232-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Philippines</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-78" data-highcharts-chart="78" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-234" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-235-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.06</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-235-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Saudi Arabia</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-79" data-highcharts-chart="79" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-237" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-238-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.06</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-238-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Tajikistan</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-80" data-highcharts-chart="80" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-240" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-241-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.06</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-241-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Turkmenistan</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="incentive-graph" id="incentive-graph-81" data-highcharts-chart="81" style="overflow: hidden;">
                            <div id="highcharts-glb6dhy-243" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 800px; height: 85px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="85" viewBox="0 0 800 85">
                                    <desc>Created with Highcharts 7.1.2</desc>
                                    <defs>
                                        <clipPath id="highcharts-glb6dhy-244-">
                                            <rect x="0" y="0" width="50" height="655" fill="none"></rect>
                                        </clipPath>
                                    </defs>
                                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="85" rx="0" ry="0"></rect>
                                    <rect fill="none" class="highcharts-plot-background" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-pane-group" data-z-index="0"></g>
                                    <g class="highcharts-plot-bands-0" data-z-index="0">
                                        <path fill="#ff8080" class="highcharts-plot-band " d="M 134.5 15 L 134.5 65 372.5 65 372.5 15 z"></path>
                                        <path fill="#ffd633" class="highcharts-plot-band " d="M 372.5 15 L 372.5 65 610.5 65 610.5 15 z"></path>
                                        <path fill="#5cd65c" class="highcharts-plot-band " d="M 610.5 15 L 610.5 65 789.5 65 789.5 15 z"></path>
                                    </g>
                                    <text x="254" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="492" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.05</text>
                                    <text x="700" text-anchor="middle" class="highcharts-plot-band-label " data-z-index="0" transform="translate(0,0)" y="12">$0.06</text>
                                    <g class="highcharts-grid highcharts-xaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 135 40.5 L 790 40.5" opacity="1"></path>
                                    </g>
                                    <g class="highcharts-grid highcharts-yaxis-grid" data-z-index="1">
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 134.5 15 L 134.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 194.5 15 L 194.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 253.5 15 L 253.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 313.5 15 L 313.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 372.5 15 L 372.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 432.5 15 L 432.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 491.5 15 L 491.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 551.5 15 L 551.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 610.5 15 L 610.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 670.5 15 L 670.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 729.5 15 L 729.5 65" opacity="1"></path>
                                        <path fill="none" data-z-index="1" class="highcharts-grid-line" d="M 789.5 15 L 789.5 65" opacity="1"></path>
                                    </g>
                                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="135" y="15" width="655" height="50"></rect>
                                    <g class="highcharts-axis highcharts-xaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 134.5 15 L 134.5 65"></path>
                                    </g>
                                    <g class="highcharts-axis highcharts-yaxis" data-z-index="2">
                                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 135 65 L 790 65"></path>
                                    </g>
                                    <g class="highcharts-series-group" data-z-index="3">
                                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="url(#highcharts-glb6dhy-244-)">
                                            <rect x="17.5" y="655.5" width="15" height="0" fill="#fff" stroke="#555" stroke-width="1" opacity="1" class="highcharts-point"></rect>
                                            <rect fill="#fff" x="25" y="59" width="0" height="3" class="highcharts-point highcharts-bullet-target"></rect>
                                        </g>
                                        <g data-z-index="0.1" class="highcharts-markers highcharts-series-0 highcharts-bullet-series " transform="translate(790,65) rotate(90) scale(-1,1) scale(1 1)" clip-path="none"></g>
                                    </g>
                                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text>
                                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="24"></text>
                                    <g data-z-index="6" class="highcharts-data-labels highcharts-series-0 highcharts-bullet-series  highcharts-tracker" transform="translate(135,15) scale(1 1)" opacity="1">
                                        <g class="highcharts-label highcharts-data-label highcharts-data-label-color-undefined" data-z-index="1" transform="translate(0,11)">
                                            <text x="5" data-z-index="1" style="font-size:12px;font-weight:normal;color:#000000;fill:#000000;" y="17">
                                                <tspan>0</tspan>
                                            </text>
                                        </g>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-xaxis-labels" data-z-index="7">
                                        <text x="120" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="41" opacity="1">
                                            <tspan class="hc-cat-title">Uzbekistan</tspan>
                                        </text>
                                    </g>
                                    <g class="highcharts-axis-labels highcharts-yaxis-labels" data-z-index="7">
                                        <text x="135" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">0</text>
                                        <text x="194.54545454545" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">25</text>
                                        <text x="254.09090909091" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">50</text>
                                        <text x="313.63636363636" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">75</text>
                                        <text x="373.18181818182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">100</text>
                                        <text x="432.72727272727" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">125</text>
                                        <text x="492.27272727273" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">150</text>
                                        <text x="551.81818181818" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">175</text>
                                        <text x="611.36363636364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">200</text>
                                        <text x="670.90909090909" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">225</text>
                                        <text x="730.45454545455" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">250</text>
                                        <text x="779.5390625" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="84" opacity="1">275</text>
                                    </g>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <!-- POPULATE -->
                </div>
            </div>

        </div>
        <div id="quality-score-help-text" style="display: none;">
            <div class="default-styles">
                <h3>Quality score</h3> We now monitor all incoming affiliate leads and have a new reporting tool that is available to you.
                <br>
                <br> Your lead quality score will go up, as your lead quality improves.
                <br>
                <br> Improving your lead quality score may take just a few tweaks to your current methods of getting leads and how you send your leads to Roomster!
                <br>
                <br> Tips:
                <ol>
                    <li>Make sure you are promoting only to people looking for Roommates, Rooms &amp; Apartments to share</li>
                    <li>Make sure any messages sent are clear and written in easy to read English, or whatever the local language you are promoting to is.</li>
                    <li>Send leads to search results for the correct location they are looking for, or to profiles of actual listings of places they may be interested in.</li>
                </ol>
            </div>
        </div>

        <div id="insentive-meter-help-text" style="display: none;">
            <div class="default-styles">
                <h3>Incentive meter</h3>
                <!--If your lead quality score is “Good” or “Excellent” you will be eligible to increase the price you earn per lead by increasing lead volume. <br/><br/>-->
                There are 3 different levels for lead payouts based on lead volume that you send during any single pay period.
                <br>
                <br> Your incentive meter will re-set at the beginning of each new pay period.
                <br>
                <br> The more leads you send the more money you make!
            </div>
        </div>

        <script>
            $('#performance-date-ranges').selectric({
                expandToItemText: true
            });
            $('#performance-date-ranges').on("change", function() {
                $('.performance-data').hide();
                $('#performance-' + $(this).val()).show();

                $('.payouts-data').hide();
                $('#payouts-' + $(this).val()).show();
            });

            $(function() {
                $('.quality-score-help').tooltip({
                    content: function() {
                        return $("#quality-score-help-text").html()
                    },
                    tooltipClass: "tooltip"
                });

                $('.insentive-meter-help').tooltip({
                    content: function() {
                        return $("#insentive-meter-help-text").html()
                    },
                    tooltipClass: "tooltip"
                });

                Roomster.Affiliate.IncentiveRates(".incentive-rates");

            });
        </script>

    </div>
@endsection