@extends('affiliate.inc.master')
@section('content')
    <div id="container" class="affiliate-container">

        <h2>Rates per lead </h2>

        <div id="affiliate-rates_wrapper" class="dataTables_wrapper no-footer">
            <div class="dataTables_length" id="affiliate-rates_length">
                <label>Show
                    <select name="affiliate-rates_length" aria-controls="affiliate-rates" class="">
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="-1">All</option>
                    </select> entries</label>
            </div>
            <div id="affiliate-rates_filter" class="dataTables_filter">
                <label>Search:
                    <input type="search" class="" placeholder="" aria-controls="affiliate-rates">
                </label>
            </div>
            <table id="affiliate-rates" class="display dataTable no-footer" style="" role="grid" aria-describedby="affiliate-rates_info">
                <thead>
                <tr role="row">
                    <th style="width: 60px;" class="sorting" tabindex="0" aria-controls="affiliate-rates" rowspan="1" colspan="1" aria-label="Country: activate to sort column ascending">Country</th>
                    <th style="width: 20px;" class="sorting_desc" tabindex="0" aria-controls="affiliate-rates" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rate: activate to sort column ascending">Rate</th>
                    <th style="width: 20px;" class="sorting" tabindex="0" aria-controls="affiliate-rates" rowspan="1" colspan="1" aria-label="Last updated: activate to sort column ascending">Last updated</th>
                </tr>
                </thead>
                <tfoot>
                </tfoot>
                <tbody>

                <tr role="row" class="odd">
                    <td>Ireland </td>
                    <td class="sorting_1">$1.05</td>
                    <td> 6/24/2019 </td>
                </tr>
                <tr role="row" class="even">
                    <td>Italy </td>
                    <td class="sorting_1">$1.05</td>
                    <td> 6/24/2019 </td>
                </tr>
                <tr role="row" class="odd">
                    <td>Japan </td>
                    <td class="sorting_1">$1.05</td>
                    <td> 6/24/2019 </td>
                </tr>
                <tr role="row" class="even">
                    <td>Norway </td>
                    <td class="sorting_1">$1.05</td>
                    <td> 6/24/2019 </td>
                </tr>
                <tr role="row" class="odd">
                    <td>Spain </td>
                    <td class="sorting_1">$1.05</td>
                    <td> 6/24/2019 </td>
                </tr>
                <tr role="row" class="even">
                    <td>Sweden </td>
                    <td class="sorting_1">$1.05</td>
                    <td> 6/24/2019 </td>
                </tr>
                <tr role="row" class="odd">
                    <td>United Kingdom </td>
                    <td class="sorting_1">$1.05</td>
                    <td> 6/24/2019 </td>
                </tr>
                <tr role="row" class="even">
                    <td>Australia </td>
                    <td class="sorting_1">$1.00</td>
                    <td> 7/23/2019 </td>
                </tr>
                <tr role="row" class="odd">
                    <td>Austria </td>
                    <td class="sorting_1">$1.00</td>
                    <td> 7/23/2019 </td>
                </tr>
                <tr role="row" class="even">
                    <td>Denmark </td>
                    <td class="sorting_1">$1.00</td>
                    <td> 7/23/2019 </td>
                </tr>
                <tr role="row" class="odd">
                    <td>Finland </td>
                    <td class="sorting_1">$1.00</td>
                    <td> 7/23/2019 </td>
                </tr>
                <tr role="row" class="even">
                    <td>France </td>
                    <td class="sorting_1">$1.00</td>
                    <td> 7/23/2019 </td>
                </tr>
                <tr role="row" class="odd">
                    <td>Germany </td>
                    <td class="sorting_1">$1.00</td>
                    <td> 7/23/2019 </td>
                </tr>
                <tr role="row" class="even">
                    <td>Hong Kong </td>
                    <td class="sorting_1">$1.00</td>
                    <td> 7/23/2019 </td>
                </tr>
                <tr role="row" class="odd">
                    <td>Israel </td>
                    <td class="sorting_1">$1.00</td>
                    <td> 7/23/2019 </td>
                </tr>
                <tr role="row" class="even">
                    <td>Netherlands </td>
                    <td class="sorting_1">$1.00</td>
                    <td> 7/23/2019 </td>
                </tr>
                <tr role="row" class="odd">
                    <td>New Zealand </td>
                    <td class="sorting_1">$1.00</td>
                    <td> 6/24/2019 </td>
                </tr>
                <tr role="row" class="even">
                    <td>Poland </td>
                    <td class="sorting_1">$1.00</td>
                    <td> 6/24/2019 </td>
                </tr>
                <tr role="row" class="odd">
                    <td>Russia </td>
                    <td class="sorting_1">$1.00</td>
                    <td> 7/23/2019 </td>
                </tr>
                <tr role="row" class="even">
                    <td>Switzerland </td>
                    <td class="sorting_1">$1.00</td>
                    <td> 6/24/2019 </td>
                </tr>
                </tbody>
            </table>
            <div class="dataTables_info" id="affiliate-rates_info" role="status" aria-live="polite">Showing 1 to 20 of 216 entries</div>
            <div class="dataTables_paginate paging_simple_numbers" id="affiliate-rates_paginate"><a class="paginate_button previous disabled" aria-controls="affiliate-rates" data-dt-idx="0" tabindex="0" id="affiliate-rates_previous">Previous</a><span><a class="paginate_button current" aria-controls="affiliate-rates" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="affiliate-rates" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="affiliate-rates" data-dt-idx="3" tabindex="0">3</a><a class="paginate_button " aria-controls="affiliate-rates" data-dt-idx="4" tabindex="0">4</a><a class="paginate_button " aria-controls="affiliate-rates" data-dt-idx="5" tabindex="0">5</a><span class="ellipsis">…</span><a class="paginate_button " aria-controls="affiliate-rates" data-dt-idx="6" tabindex="0">11</a></span><a class="paginate_button next" aria-controls="affiliate-rates" data-dt-idx="7" tabindex="0" id="affiliate-rates_next">Next</a></div>
        </div>

    </div>
@endsection