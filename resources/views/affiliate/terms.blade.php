@extends('affiliate.inc.master')
@section('content')
    <div id="container" class="affiliate-container">

        <div class="terms">
            <h1 style="text-align: center;">GettingRoom Corp. Affiliate Marketing Program Terms and Condition</h1>

            <p>
                These GettingRoom Affiliate Marketing Program Terms and Conditions (this “Agreement”) constitute a contract between you and GettingRoom Corp. (referred to in this Agreement as “Company,” “we,” or “us”) and set forth the terms and conditions under which you (referred to in this Agreement as “Affiliate” or “you”) may participate in the GettingRoom Affiliate Marketing Program (the “Affiliate Program”) in connection with the roomster.com website and any other website that is owned or operated on behalf of Company and identified by Company as participating in the Affiliate Program (each, a “Company Website”). By becoming a participant in the Affiliate Program, you are entering into a binding and enforceable contract with Company.
            </p>

            <p>
                BY ENROLLING IN THE AFFILIATE PROGRAM, PARTICIPATING IN THE AFFILIATE PROGRAM AND/OR CLICKING “I AGREE” ON THE ENROLLMENT FORM YOU ARE AFFIRMATIVELY STATING THAT YOU HAVE READ, UNDERSTOOD AND ACCEPT THIS AGREEMENT AND AGREE TO BE BOUND BY ALL OF THE TERMS AND CONDITIONS AND PROGRAM RULES SET FORTH BELOW.
            </p>

            <ol style="list-style-type: decimal;">
                <li>
                    <strong>Enrollment in the Affiliate Program.</strong> To enroll in the Affiliate Program, you must submit a complete affiliate application form through the Affiliate interface on the Company Website. You must complete all the required fields as prompted during the enrollment process, and provide us with your full name, a valid e-mail address, and the URL for your website that will link to the Company Website in connection with the Affiliate Program (the “Affiliate Website”). You acknowledge and agree that we may e-mail you notices about the Affiliate Program and your account, as well as other transactional and promotional information based on the information that you provide to us when you enroll to participate in the Affiliate Program. We reserve the absolute right to deny or refuse any person or entity participation in the Affiliate Program.
                    <p></p>
                </li>
                <li>
                    <strong>Affirmative Representations and Warranties of Affiliate.</strong> By participating in the Affiliate Program, you represent that: (i) you have read and accept the terms and conditions set forth in this Agreement, including the payment terms and conditions associated with the Affiliate Program as set forth in this Agreement or otherwise provided to you by Company in connection with the Affiliate Program, (ii) you are at least eighteen years of age and have reached the age of majority under applicable law where you reside and/or do business, (iii) you have all necessary rights and authority to enter into this Agreement and perform your obligations in connection with this Agreement, (iv) you will provide truthful, accurate and complete information to Company concerning your identity, payment account, address or other required information, (v) you have independently evaluated the desirability of participating in the Affiliate Program and are not relying on any representation, guarantee or statement by Company other than as expressly set forth in this Agreement, and (vi) all obligations owed to third parties with respect to the activities contemplated to be undertaken by you pursuant to this Agreement are or will be fully satisfied by you, so that Company will not have any obligations with respect thereto.
                    <p></p>
                </li>
                <li>
                    <strong>Rights Granted to Affiliate.</strong>  Subject to the terms and conditions set forth in this Agreement, we grant you the following rights and licenses:
                    <ol style="list-style-type: upper-alpha">
                        <li>
                            The non-exclusive, non-transferable, revocable right and license to place authorized links on the Affiliate Website, for which you will be entitled to receive a referral fee in accordance with the terms of this Agreement, subject to your compliance with the terms and conditions of this Agreement; and
                            <p></p>
                        </li>
                        <li>
                            The nonexclusive, non-transferable, revocable right and license to display on the Affiliate Website proprietary Company materials provided to you by Company through the Affiliate interface in connection with the Affiliate Program, including, but not limited to: banners, text links, graphic images, and text (hereafter collectively referred to as “Program Materials”) solely for the purpose of promoting the Company products and services associated with the Affiliate Program and encouraging visitors to the Affiliate Website to click through to the Company Website(s).  You may not alter or change the Program Materials without the Company’s prior consent.
                            <p></p>
                        </li>
                    </ol>
                </li>
                <li>
                    <strong>Program Materials.</strong> You acknowledge and agree that Company is, and shall remain, the exclusive owner of the Program Materials provided to you by Company pursuant to this Agreement and in connection with the Affiliate Program, and that the Program Materials may only be used by you to generate “referrals,” as that term is defined in Section 6.3, during the period of your authorized participation in the Affiliate Program. Program Materials may not be otherwise copied, reproduced, altered, modified, changed, broadcast, distributed, transmitted, disseminated or offered for sale or rental in any manner. Company reserves all of its rights in its trade names, trademarks and service marks and all other intellectual property associated with the Program Materials. Nothing in this Agreement shall be construed as a grant or assignment of any rights in any intellectual property owned by Company, including, without limitation, any of its trademarks or service marks. Any and all use of Company's trademarks shall be in direct association with the Program Materials and all good will generated from the use of Company’s trademarks shall inure to the sole and exclusive benefit of Company. Company reserves the right to revoke your right to use the Program Materials and any of Company’s trademarks or service marks at any time in its sole discretion and you shall immediately cease use of any applicable Program Materials, trademarks or service marks upon notice from Company, including removing all of the applicable Program Materials from the Affiliate Website.
                    <p></p>
                </li>
                <li>
                    <strong>Suitability Requirements; Prohibited Activity.</strong>   In consideration of Company providing you with the Program Materials and the other benefits of the Affiliate Program, and as a condition of participation in the Affiliate Program, (i) you must comply with all of the provisions of the CAN-SPAM Act and any comparable state anti-spamming statutes, (ii) you must comply with all applicable FTC guidelines and you must not use text messaging to send any text message, including SMS, or MMS messages, without the prior express written consent of the recipient as set forth in 47 C.F.R. § 64.1200(f)(8), including, without limitation, junk mail, chain letters, or any materials that promote malware, spyware or downloadable items or otherwise use text messaging in violation of applicable law, and (iii) you must not make any representations about Company or its services that are inconsistent with statements set forth on the Company Website.
                    <p>In addition, you hereby represent, acknowledge, agree, and warrant to Company that:</p>
                    <ol style="list-style-type: upper-alpha">
                        <li>
                            The Affiliate Website, and any content, goods or services offered in, at or through the Affiliate Website, and your use of any Program Materials, shall not at any time during your participation in the Affiliate Program:
                            <ol style="list-style-type: lower-roman">
                                <li>
                                    violate any law, statute, ordinance or regulation, or promote illegal activities;
                                    <p></p>
                                </li>
                                <li>
                                    contain or promote obscene materials, including, without limitation, any material depicting bestiality, rape or torture;
                                    <p></p>
                                </li>
                                <li>
                                    contain or promote harmful or indecent matter to minors;
                                    <p></p>
                                </li>
                                <li>
                                    contain any material in which persons under the age of eighteen are depicted in actual, simulated or suggestive sexual situations or which constitutes child pornography or matter which involves depictions of nudity or sexuality by an age inappropriate-looking performer (i.e. someone who looks younger than 18 years of age), or by a performer who is portrayed or made to appear to be a person under the age of 18 years of age by virtue of the script, make-up, demeanor, costuming, setting, etc.;
                                    <p></p>
                                </li>
                                <li>
                                    contain any material which constitutes an infringement, misappropriation or violation of any person’s or entity’s intellectual property rights such as copyrights, trademark rights, rights of publicity or patent rights, or which constitute a violation of any person’s or entity’s personal property rights, privacy rights or any other rights;
                                    <p></p>
                                </li>
                                <li>
                                    promote violence or discrimination based on race, sex, religion, nationality, disability, sexual orientation, or age;
                                    <p></p>
                                </li>
                                <li>
                                    contain content which is defamatory, libelous, hateful, threatening, abusive or harassing; or
                                    <p></p>
                                </li>
                                <li>
                                    send unsolicited bulk, junk, spam e-mail or any program, file, data stream or other material that contains viruses, worms, “Trojan horses,” or any other destructive feature;
                                    <p></p>
                                </li>
                                <li>
                                    include any content that violates any laws, including but not limited to 18 U.S.C. Section 2257, 18 U.S.C. Section 2257A and 28 CFR 75 et seq.;
                                    <p></p>
                                </li>
                                <li>
                                    promote the sale or use of tobacco products, alcohol products, or gambling;
                                    <p></p>
                                </li>
                                <li>
                                    be directed toward children under 13 years of age, as defined by the Children’s Online Privacy Protection Act (15 U.S.C. Sections 6501-6506) and any regulations promulgated thereunder; or
                                    <p></p>
                                </li>
                                <li>
                                    otherwise be offensive or inappropriate as determined by Company in its sole discretion.
                                    <p></p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            You will not undertake or engage in the following practices:
                            <ol style="list-style-type: lower-roman">
                                <li>
                                    use unsolicited bulk email, or permit any person under your control to use unsolicited bulk email, in direct or indirect association with your participation in the Affiliate Program, including but not limited to, using unsolicited bulk Instant Messages, Chatroom, Newsgroup, ICQ or IRC postings;
                                    <p></p>
                                </li>
                                <li>
                                    promote or deal in illegal activity, including any use of prohibited content in hidden meta tags, text, links, graphic(s) or any HTML;
                                    <p></p>
                                </li>
                                <li>
                                    attempt to mislead or defraud Company in any way;
                                    <p></p>
                                </li>
                                <li>
                                    hijack any traffic;
                                    <p></p>
                                </li>
                                <li>
                                    provide any incorrect or incomplete account or address information;
                                    <p></p>
                                </li>
                                <li>
                                    generate any hits or signups as a requirement to enter or obtain access to goods or services on any Affiliate Website or any third party website;
                                    <p></p>
                                </li>
                                <li>
                                    use any sort of mechanism or effort that falsely generates hits or signups, including signups by you;
                                    <p></p>
                                </li>
                                <li>
                                    use any automation software, bots, hacks, mods or any unauthorized third party software designed to modify or interfere with the Company Website;
                                    <p></p>
                                </li>
                                <li>
                                    violate, infringe or misappropriate any person’s or entity’s copyrights, trademark rights, patent rights, rights of publicity, privacy rights or any other intellectual, personal or property right;
                                    <p></p>
                                </li>
                                <li>
                                    cheat or defraud any other affiliate marketing program or its sponsor;
                                    <p></p>
                                </li>
                                <li>
                                    misrepresent or disparage any of the Company Websites or Company’s services, programs, performers or content including but not limited to, misrepresenting the cost of a subscription, terms of membership, or content contained within the Company Websites;
                                    <p></p>
                                </li>
                                <li>
                                    improperly exploit any Company Website, including but not limited to, instructing, informing, urging or incentivizing a person to sign up and cancel services;
                                    <p></p>
                                </li>
                                <li>
                                    make any unauthorized modification, alteration or use of Program Materials or any of the Company's other intellectual property, including its trademarks; or
                                    <p></p>
                                </li>
                                <li>
                                    include any of the Company’s trademarks or service marks, or variations or misspellings thereof, in your domain names.
                                    <p></p>
                                </li>
                            </ol>
                        </li>
                    </ol>
                </li>
                <li>
                    <strong>Calculation of Referral Fees.</strong>
                    <ol style="list-style-type: upper-alpha">
                        <li>
                            All payment obligations owed to you by us and all payments paid or payable to you pursuant to your participation in the Affiliate Program shall be subject to the terms of this Agreement and your full compliance with, and performance of, all obligations set forth in this Agreement. Any and all payment and other compensation options offered by Company are subject to change and all programs may be modified, suspended, cancelled or terminated at Company’s election.
                            <p></p>
                        </li>
                        <li>
                            Subject to the terms of this Agreement, for each person who becomes a subscriber to, or purchases qualifying services from, a Company Website, where such person has been tracked and verified by Company as a “referral” to a Company Website from the Affiliate Website, as the term “referral” is defined in Section 6.C below, you will be entitled to receive a referral fee in the amount specified on the Affiliate interface on the Company Website. Company has the right to determine the amount of applicable referral fees in its sole discretion.
                            <p></p>
                        </li>
                        <li>
                            For the purposes of this Agreement a “referral” that entitles you to a referral fee payment in accordance with the payment terms of this Agreement and as provided through the Affiliate interface shall be defined as a person who has been directed to a Company Website through the use of a hypertext transfer link residing on the Affiliate Website which is in the form of a banner ad or other promotional link which automatically connects any person who clicks on the applicable banner ad or other promotional link to a Company Website, and which banner ad or other promotional link has been supplied to you by Company as part of the Affiliate Program in which you are participating.
                            <p></p>
                        </li>
                        <li>
                            You acknowledge and agree that you shall not be entitled to a referral fee for any person who you send, direct or refer to the Company Website(s) in violation of the terms of this Agreement or if you are otherwise in violation of the terms of this Agreement at the time of such referral.  You also acknowledge and agree that you shall not be entitled to a referral fee of any kind from Company for any referral that Company determines is the result of possible fraudulent activity. You further acknowledge and agree that Company shall have the right, in its sole and exclusive discretion, at any time to expand or modify what it determines to constitute possible fraudulent activity.
                            <p></p>
                        </li>
                        <li>
                            Only Company databases and files will be used in determining the amount of valid hits, signups, referrals, cancellations, credits and any referral fees owed to any Affiliate pursuant to this Agreement. You must notify us of any discrepancies, inaccuracies, errors or complaints regarding any such determinations within 30 days of your receipt of a related payment from us, or else they will be deemed permanently waived.
                            <p></p>
                        </li>
                    </ol>
                </li>
                <li>
                    <strong>Payment of Referral Fees.</strong> We will distribute referral fee payments to you on a bi-weekly basis to the PayPal account you designate. All referral fees will be disbursed in U.S. Dollars. In the event that payments to us by our payment processors pertaining to referrals you have generated are delayed for any reason, you agree that we shall have a reasonable period of time for the receipt and processing of such payments, and for transmission of corresponding referral fee payments to you. You hereby acknowledge that if you reside in the United States or any of its territories, you are required to complete a W9 form with the correct information and enter a Federal Tax ID or Social Security Number or we will be forced to withhold tax as required by law. We will register a 1099 for all U.S. domestic entities to which we issue combined yearly payments of $600.00 or more. Failure to provide this or other required tax or identification information will cause your payment to be delayed or withheld. You acknowledge and agree that you and you alone are fully responsible for the payment of all taxes directly or indirectly related to your participation in the Affiliate Program.
                    <p></p>
                </li>
                <li>
                    <strong>Termination.</strong>
                    <ol style="list-style-type: upper-alpha">
                        <li>
                            Either party may terminate your participation in the Affiliate Program, and this Agreement, at any time, with or without cause, by giving the other party notice of termination. Notice by e-mail to the e-mail address we have on file for you shall be considered sufficient notice for us to terminate or cancel your participation in the Affiliate Program. Upon receipt of notice from us of your termination from the Affiliate Program, or your notice to us of your election to terminate, you shall immediately cease using all the Program Materials and other Company intellectual property provided under this Agreement, and you shall immediately remove all Program Materials from the Affiliate Website, and, except as otherwise set forth herein, Company will pay you the referral fees owed to you in connection with the Affiliate Program as of the date of termination.
                            <p></p>
                        </li>
                        <li>
                            If we find that you have violated the terms of this Agreement, including, without limitation, the requirements set forth in Section 5, we reserve the right in our sole and absolute discretion to terminate this Agreement and your participation in the Affiliate Program, at any time, without prior notification, and you acknowledge and agree that in such circumstances, you will not be paid for any referral fees owed to you in connection with the Affiliate Program and not paid to you prior to the date of termination, and you may be barred from any future participation in any of our Affiliate Programs.
                            <p></p>
                        </li>
                        <li>
                            You acknowledge and agree that Company shall have the right to deny or withhold payment from you and to terminate you from the Affiliate Program if there has been an abnormal number of chargebacks or cancellations of memberships or subscriptions that have been referred to any of the Company Websites through, from, or in association with, the Affiliate Website. You further acknowledge and agree that Company shall have the right, in its sole and exclusive judgment, to determine what constitutes an abnormal number of chargebacks or cancellations of memberships or subscriptions.
                            <p></p>
                        </li>
                    </ol>
                </li>
                <li>
                    <strong>Lawsuits and Claims.</strong> We reserve the right to report to law enforcement authorities and/or take legal action against anyone who violates the terms of this Agreement, and cooperate in any and all legal actions and legal investigations relating to activities involving any violation of this Agreement, including disclosure of your account information to third parties in connection with any applicable legal action or investigation.
                    <p></p>
                </li>
                <li>
                    <strong>Indemnification by Affiliate.</strong> You agree that you shall fully indemnify, defend and hold us harmless from any and all damages, losses and costs (including attorneys’ fees) resulting from any breach by you of any warranty or obligation under this Agreement and/or any act taken by you or any persons under your control in violation of this Agreement, including any failure to act when required to under this Agreement.
                    <p></p>
                </li>
                <li>
                    <strong>Relationship of the Parties.</strong> Nothing in this Agreement is intended by you or us to create or constitute a joint or collaborative venture or partnership of any kind between you and us, nor shall anything in this Agreement be construed as constituting or creating any agency, employment relationship, joint or collaborative venture or partnership between you and Company, its employees, agents or assigns. You acknowledge and agree that we shall have no control or ownership interests of any kind in your business or the Affiliate Website. You acknowledge and agree that you shall have no financial or other interest in Company or any property owned by Company, its affiliates, agents, successors or assigns. You acknowledge and agree that your relationship with us shall be restricted to matters pertaining to the Affiliate Program exclusively and shall be governed entirely by the terms and conditions of this Agreement.
                    <p></p>
                </li>
                <li>
                    <strong>Management of Affiliate Website.</strong> You will be solely responsible for managing the Affiliate Website, including its development, operation and maintenance. You will be solely responsible for ensuring the accuracy, completeness, and appropriateness of any materials you post on the Affiliate Website and disclosing on the Affiliate Website accurately and adequately, either through a privacy policy or other means, how you collect, use, store and disclose data collected from visitors to the Affiliate Website. We will have the right, but not the obligation, to monitor, supervise or review any content appearing or otherwise distributed on, at or in association with the Affiliate Website including any Program Materials or content which you have received from us. You acknowledge that neither Company nor any employee, associate, agent, assign or successor of Company shall exert or provide any direct or indirect control over, monitoring of, supervision of, prior approval of, or review of the content appearing or otherwise distributed on, at or in association with the Affiliate Website, and that you shall be solely responsible for any legal liabilities or consequences resulting from your dissemination of that content, including the Program Materials, on or through the Affiliate Website.
                    <p></p>
                </li>
                <li>
                    <strong>Limitation of Liability.</strong> To the extent permitted by applicable law, You acknowledge and agree that under no circumstances shall Company, its employees, independent contractors, authors, agents, attorneys, representatives, assigns and successors be liable to You, or any other person or entity, for any direct or indirect losses, injuries, special or incidental or consequential damages of any kind (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, GOODWILL, LOSS OF BUSINESS INFORMATION, OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES OR ANY OTHER PECUNIARY LOSS) with regard to the Affiliate Program, any link to any Company website(s), or arising from or in connection with the use of the Program Materials, or due to any mistakes, omissions, delays, errors, interruptions in the transmission, or receipt of Company's services, content or Program Materials, including without limitation any losses due to server problems, computer or other equipment failure, or due to incorrect placement of HTML, regardless whether based upon breach of contract, negligence or any other claim or cause of action, In the event of any network downtime, computer or technical error We will not be held responsible for any lost hits, signups, traffic or income. Notwithstanding the foregoing express limitations of liability, You acknowledge and agree that should Company, its officers, employees, successors, or assigns be held liable to You for damages, injuries or losses of any kind, directly or indirectly resulting from Your participation in the Program, that the maximum total dollar amount of liquidated damages for any and all of Your claims, injuries, damages or losses shall not exceed a total of ten dollars ($10.00).
                    <p></p>
                    SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF YOU RESIDE IN ONE OF THESE STATES OR JURISDICTIONS, THE LIMITATIONS OR EXCLUSIONS in this Section 13 MAY NOT APPLY TO YOU.
                    <p></p>
                </li>
                <li>
                    <strong>No Guarantee of Success.</strong> You confirm, acknowledge and expressly agree that neither Company, any agent or representative of Company, nor any other person has at any time in the past, represented to you or has otherwise directly or indirectly communicated in any manner to you any guarantee, reassurance or any other statement of any kind regarding: (i) the potential profitability or likelihood of success of your participation in the Affiliate Program as set forth in this Agreement or otherwise; (ii) the possibility or likelihood that use of any products and/or services provided by Company pursuant to this Agreement can or will result in the recoupment of any funds expended by you for the promotion of the Affiliate Website or any other purpose; or (iii) the existence, nonexistence, size or any other characteristics of any market for any products or services which involve your participation in the Affiliate Program pursuant to this Agreement.
                    <p></p>
                </li>
                <li>
                    <strong>Modifications.</strong> We reserve the right to alter or modify this Agreement and the terms applicable to the Affiliate Program at any time. We will notify you of material changes to this Agreement or other Affiliate Program terms by sending an email to the email associated with your Affiliate Program account or using the other contact information you provide in connection with your Affiliate Program. If you do not agree with the proposed changes, you should discontinue your participation in the Affiliate Program prior to the time the new terms take effect. If you continue to participate in the Affiliate Program after the new terms take effect, you will be bound by the modified terms. You agree that no modification of this Agreement by you, your employees, representatives, agents, assigns or successors shall be enforceable of have any effect unless first reduced to writing, agreed to in writing by Company and signed by Company's duly authorized representative.
                    <p></p>
                </li>
                <li>
                    <strong>Entire Agreement.</strong> This Agreement constitutes the entire agreement between you and Company with respect to the subject matter hereof, and supersedes and cancels all other prior agreements, discussion, or representations, whether written or oral.
                    <p></p>
                </li>
                <li>
                    <strong>No Waiver.</strong> You acknowledge and agree that the failure of Company to enforce any of the specific provisions of this Agreement shall not preclude any other or further enforcement of such provision(s) or the exercise of any other right hereunder.
                    <p></p>
                </li>
                <li>
                    <strong>Assignment.</strong> You agree that all promises, obligations, duties and warranties made by you in this Agreement are personal to you and that neither they nor any benefits hereunder may be assigned by you to any other person or entity. You agree that Company may at any time, and without prior notice to you, freely assign all or part of its duties, obligations and benefits hereunder.
                    <p></p>
                </li>
                <li>
                    <strong>Applicable Law; Venue.</strong> You agree that federal laws and the laws of the State of New York, without regard to principles of conflict of laws, will govern this Agreement and any claim or dispute that has arisen or may arise between Company and Affiliate. The parties agree that any litigation between them shall be filed exclusively in or where venue is proper for state or federal courts located in New York County, New York.
                    <p></p>
                </li>
                <li>
                    <strong>Agreement Construction.</strong> For purposes of construction of this Agreement, both Company and you shall be deemed to have mutually drafted this Agreement and all parts thereof. Paragraph and subparagraph headings of this Agreement are inserted for convenience only and shall not be deemed to constitute a part hereof nor to affect the meaning thereof.
                    <p></p>
                </li>
                <li>
                    <strong>Severability.</strong> If any provision of this Agreement is held void or unenforceable to any extent, such provision shall be deemed excised and removed to make the remaining provisions enforceable. Unless otherwise specifically provided, the provisions of this Agreement shall survive its termination.
                    <p></p>
                </li>
                <li>
                    <strong>Force Majeure.</strong> Either party shall be excused from delays in performing or from its failure to perform hereunder to the extent that such delays or failures result from causes beyond the reasonable control of such party, including, without limitation, acts of God, nature, any government agency(ies), war, civil disturbance, labor disputes or shortages, electrical or mechanical breakdowns, inability or refusal of a common carrier to provide communications capabilities, or any other cause beyond either party's direct control, including but not limited to, the issuance of an order by any regulatory, administrative, judicial or legislative prohibiting or interfering with either party from carrying on its day-to-day operations as contemplated under this Agreement.
                    <p></p>
                </li>
                <li>
                    <strong>Review of Terms.</strong> We strongly advise that you review this Agreement with legal counsel before you enter into it. You acknowledge and agree that nothing herein and no statement by us or any employee, representative, agent or other person associated with us has in any way prevented or inhibited you in any way from seeking such advice prior to entering into this Agreement. You hereby acknowledge and agree that the terms of this Agreement are reasonable and fair; all terms have been fully disclosed in writing, and that you have been given a reasonable chance to seek advice of independent legal counsel with respect to this Agreement and all transactions associated herewith.
                    <p></p>
                </li>
            </ol>
        </div>

    </div>
@endsection