@extends('affiliate.inc.master')
@section('content')
    <div id="container" class="affiliate-container">

        <div class="content">

            <div id="promo-tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
                <ul id="tabs-list" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                    <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="tabs-0" aria-labelledby="ui-id-12" aria-selected="true">
                        <a href="#tabs-0" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-12">Generate links</a>
                    </li>
                    <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-1" aria-labelledby="ui-id-13" aria-selected="false">
                        <a href="#tabs-1" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-13">XML feed</a>
                    </li>
                    <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-2" aria-labelledby="ui-id-14" aria-selected="false">
                        <a href="#tabs-2" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-14">Embed resources</a>
                    </li>
                    <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-3" aria-labelledby="ui-id-15" aria-selected="false">
                        <a href="#tabs-3" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-15">How to create a blog</a>
                    </li>
                </ul>
                <div id="tabs-0" style="" aria-labelledby="ui-id-12" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="true" aria-hidden="false">
                    <div id="generate-links">
                        <table style="width: 100%; margin-bottom: 10px;">
                            <tbody>
                            <tr>
                                <th colspan="2" style="background-color: #f0f0f0;">Home page link <a href="#index-help-text" rel="modal:open"><i class="index-help fa fa-question-circle" title=""></i></a></th>
                            </tr>
                            <tr>
                                <td width="90%">
                                    <div class="selectric-wrapper selectric-hover">
                                        <div class="selectric-hide-select">
                                            <select id="index-campaigns" style="display: inline-table;" tabindex="0">
                                                <option value="">select campaign</option>
                                                <option value="00008db4">SM-M (00008db4)</option>
                                                <option value="00008ece">SM-T (00008ece)</option>
                                                <option value="00008fca">LOG SM-A (00008fca)</option>
                                                <option value="00009172">SM-J (00009172)</option>
                                                <option value="00009272">SM-B (00009272)</option>
                                                <option value="00009466">SM-R (00009466)</option>
                                                <option value="00009563">SM-S (00009563)</option>
                                                <option value="00009847">SUB- T (00009847)</option>
                                                <option value="0000b46e">SUB- R (0000b46e)</option>
                                                <option value="0000be1f">Do not use this champing SUB- J (0000be1f)</option>
                                                <option value="0000d59c">SHARE- R (0000d59c)</option>
                                                <option value="0000d59d">SHARE- T (0000d59d)</option>
                                                <option value="0000d59e">SHARE- J (0000d59e)</option>
                                                <option value="0000d59f">SHARE- A (0000d59f)</option>
                                                <option value="0000d5a0">SHARE- B (0000d5a0)</option>
                                                <option value="0000d5a1">SUB- S (0000d5a1)</option>
                                                <option value="0000d5a2">SHARE- M (0000d5a2)</option>
                                                <option value="0000d7b4">SUB- M (0000d7b4)</option>
                                                <option value="0000d7d1">SUB- B (0000d7d1)</option>
                                                <option value="0000d7d2">LOG SUB- A (0000d7d2)</option>
                                                <option value="0000d7d3">SHARE- S (0000d7d3)</option>
                                                <option value="0000daaf">LOG RENT R (0000daaf)</option>
                                                <option value="0000dab1">LOG RENT T (0000dab1)</option>
                                                <option value="0000dab2">LOG SUB T (0000dab2)</option>
                                                <option value="0000dbff">LOG SUB R (0000dbff)</option>
                                                <option value="0000dc31">SHARE- AM (0000dc31)</option>
                                                <option value="0000dc32">SM- AM (0000dc32)</option>
                                                <option value="0000dca0">REP - R (0000dca0)</option>
                                                <option value="0000dca1">REP - J (0000dca1)</option>
                                                <option value="0000dca2">REP - T (0000dca2)</option>
                                                <option value="0000dca3">REP - A (0000dca3)</option>
                                                <option value="0000dca4">REP - B (0000dca4)</option>
                                                <option value="0000ddc1">SHARET -R (0000ddc1)</option>
                                                <option value="0000ddc2">SUBT- R (0000ddc2)</option>
                                                <option value="0000ddc3">SMT-R (0000ddc3)</option>
                                                <option value="0000ddc4">SUBT- J (0000ddc4)</option>
                                                <option value="0000ddc5">SMT- J (0000ddc5)</option>
                                                <option value="0000ddcd">SHARET- J (0000ddcd)</option>
                                                <option value="0000ddce">SHARET- T (0000ddce)</option>
                                                <option value="0000ddcf">SUBT- T (0000ddcf)</option>
                                                <option value="0000ddd0">SMT-T (0000ddd0)</option>
                                                <option value="0000df52">Place Share - R (0000df52)</option>
                                                <option value="0000df53">Place Sublet- R (0000df53)</option>
                                                <option value="0000df54">Place Rent- R (0000df54)</option>
                                                <option value="0000df55">Place Share - T (0000df55)</option>
                                                <option value="0000df56">Place Sublet- T (0000df56)</option>
                                                <option value="0000df57">Place Rent- T (0000df57)</option>
                                                <option value="0000df58">Place Share - B (0000df58)</option>
                                                <option value="0000df59">Place Sublet- B (0000df59)</option>
                                                <option value="0000df5c">Place Rent- B (0000df5c)</option>
                                                <option value="0000e32b">Log Sub- J (0000e32b)</option>
                                            </select>
                                        </div>
                                        <div class="selectric">
                                            <p class="label">select campaign</p><b class="button">▾</b></div>
                                        <div class="selectric-items" tabindex="-1">
                                            <div class="selectric-scroll">
                                                <ul>
                                                    <li data-index="0" class="selected">select campaign</li>
                                                    <li data-index="1" class="">SM-M (00008db4)</li>
                                                    <li data-index="2" class="">SM-T (00008ece)</li>
                                                    <li data-index="3" class="">LOG SM-A (00008fca)</li>
                                                    <li data-index="4" class="">SM-J (00009172)</li>
                                                    <li data-index="5" class="">SM-B (00009272)</li>
                                                    <li data-index="6" class="">SM-R (00009466)</li>
                                                    <li data-index="7" class="">SM-S (00009563)</li>
                                                    <li data-index="8" class="">SUB- T (00009847)</li>
                                                    <li data-index="9" class="">SUB- R (0000b46e)</li>
                                                    <li data-index="10" class="">Do not use this champing SUB- J (0000be1f)</li>
                                                    <li data-index="11" class="">SHARE- R (0000d59c)</li>
                                                    <li data-index="12" class="">SHARE- T (0000d59d)</li>
                                                    <li data-index="13" class="">SHARE- J (0000d59e)</li>
                                                    <li data-index="14" class="">SHARE- A (0000d59f)</li>
                                                    <li data-index="15" class="">SHARE- B (0000d5a0)</li>
                                                    <li data-index="16" class="">SUB- S (0000d5a1)</li>
                                                    <li data-index="17" class="">SHARE- M (0000d5a2)</li>
                                                    <li data-index="18" class="">SUB- M (0000d7b4)</li>
                                                    <li data-index="19" class="">SUB- B (0000d7d1)</li>
                                                    <li data-index="20" class="">LOG SUB- A (0000d7d2)</li>
                                                    <li data-index="21" class="">SHARE- S (0000d7d3)</li>
                                                    <li data-index="22" class="">LOG RENT R (0000daaf)</li>
                                                    <li data-index="23" class="">LOG RENT T (0000dab1)</li>
                                                    <li data-index="24" class="">LOG SUB T (0000dab2)</li>
                                                    <li data-index="25" class="">LOG SUB R (0000dbff)</li>
                                                    <li data-index="26" class="">SHARE- AM (0000dc31)</li>
                                                    <li data-index="27" class="">SM- AM (0000dc32)</li>
                                                    <li data-index="28" class="">REP - R (0000dca0)</li>
                                                    <li data-index="29" class="">REP - J (0000dca1)</li>
                                                    <li data-index="30" class="">REP - T (0000dca2)</li>
                                                    <li data-index="31" class="">REP - A (0000dca3)</li>
                                                    <li data-index="32" class="">REP - B (0000dca4)</li>
                                                    <li data-index="33" class="">SHARET -R (0000ddc1)</li>
                                                    <li data-index="34" class="">SUBT- R (0000ddc2)</li>
                                                    <li data-index="35" class="">SMT-R (0000ddc3)</li>
                                                    <li data-index="36" class="">SUBT- J (0000ddc4)</li>
                                                    <li data-index="37" class="">SMT- J (0000ddc5)</li>
                                                    <li data-index="38" class="">SHARET- J (0000ddcd)</li>
                                                    <li data-index="39" class="">SHARET- T (0000ddce)</li>
                                                    <li data-index="40" class="">SUBT- T (0000ddcf)</li>
                                                    <li data-index="41" class="">SMT-T (0000ddd0)</li>
                                                    <li data-index="42" class="">Place Share - R (0000df52)</li>
                                                    <li data-index="43" class="">Place Sublet- R (0000df53)</li>
                                                    <li data-index="44" class="">Place Rent- R (0000df54)</li>
                                                    <li data-index="45" class="">Place Share - T (0000df55)</li>
                                                    <li data-index="46" class="">Place Sublet- T (0000df56)</li>
                                                    <li data-index="47" class="">Place Rent- T (0000df57)</li>
                                                    <li data-index="48" class="">Place Share - B (0000df58)</li>
                                                    <li data-index="49" class="">Place Sublet- B (0000df59)</li>
                                                    <li data-index="50" class="">Place Rent- B (0000df5c)</li>
                                                    <li data-index="51" class="last">Log Sub- J (0000e32b)</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <input class="selectric-input" tabindex="0">
                                    </div>
                                </td>
                                <td>
                                    <input type="button" class="inputsubmit" value="Generate" id="generate-index-link" onclick="GenerateLink('index')">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">Link: <span id="index-link"></span></td>
                            </tr>
                            </tbody>
                        </table>

                        <table style="width: 100%; margin-bottom: 10px;">
                            <tbody>
                            <tr>
                                <th colspan="2" style="background-color: #f0f0f0;">Login/Sign up page link <a href="#login-help-text" rel="modal:open"><i class="login-help fa fa-question-circle" title=""></i></a></th>
                            </tr>
                            <tr>
                                <td width="90%">
                                    <div class="selectric-wrapper">
                                        <div class="selectric-hide-select">
                                            <select id="login-campaigns" style="display: inline-table;" tabindex="0">
                                                <option value="">select campaign</option>
                                                <option value="00008db4">SM-M (00008db4)</option>
                                                <option value="00008ece">SM-T (00008ece)</option>
                                                <option value="00008fca">LOG SM-A (00008fca)</option>
                                                <option value="00009172">SM-J (00009172)</option>
                                                <option value="00009272">SM-B (00009272)</option>
                                                <option value="00009466">SM-R (00009466)</option>
                                                <option value="00009563">SM-S (00009563)</option>
                                                <option value="00009847">SUB- T (00009847)</option>
                                                <option value="0000b46e">SUB- R (0000b46e)</option>
                                                <option value="0000be1f">Do not use this champing SUB- J (0000be1f)</option>
                                                <option value="0000d59c">SHARE- R (0000d59c)</option>
                                                <option value="0000d59d">SHARE- T (0000d59d)</option>
                                                <option value="0000d59e">SHARE- J (0000d59e)</option>
                                                <option value="0000d59f">SHARE- A (0000d59f)</option>
                                                <option value="0000d5a0">SHARE- B (0000d5a0)</option>
                                                <option value="0000d5a1">SUB- S (0000d5a1)</option>
                                                <option value="0000d5a2">SHARE- M (0000d5a2)</option>
                                                <option value="0000d7b4">SUB- M (0000d7b4)</option>
                                                <option value="0000d7d1">SUB- B (0000d7d1)</option>
                                                <option value="0000d7d2">LOG SUB- A (0000d7d2)</option>
                                                <option value="0000d7d3">SHARE- S (0000d7d3)</option>
                                                <option value="0000daaf">LOG RENT R (0000daaf)</option>
                                                <option value="0000dab1">LOG RENT T (0000dab1)</option>
                                                <option value="0000dab2">LOG SUB T (0000dab2)</option>
                                                <option value="0000dbff">LOG SUB R (0000dbff)</option>
                                                <option value="0000dc31">SHARE- AM (0000dc31)</option>
                                                <option value="0000dc32">SM- AM (0000dc32)</option>
                                                <option value="0000dca0">REP - R (0000dca0)</option>
                                                <option value="0000dca1">REP - J (0000dca1)</option>
                                                <option value="0000dca2">REP - T (0000dca2)</option>
                                                <option value="0000dca3">REP - A (0000dca3)</option>
                                                <option value="0000dca4">REP - B (0000dca4)</option>
                                                <option value="0000ddc1">SHARET -R (0000ddc1)</option>
                                                <option value="0000ddc2">SUBT- R (0000ddc2)</option>
                                                <option value="0000ddc3">SMT-R (0000ddc3)</option>
                                                <option value="0000ddc4">SUBT- J (0000ddc4)</option>
                                                <option value="0000ddc5">SMT- J (0000ddc5)</option>
                                                <option value="0000ddcd">SHARET- J (0000ddcd)</option>
                                                <option value="0000ddce">SHARET- T (0000ddce)</option>
                                                <option value="0000ddcf">SUBT- T (0000ddcf)</option>
                                                <option value="0000ddd0">SMT-T (0000ddd0)</option>
                                                <option value="0000df52">Place Share - R (0000df52)</option>
                                                <option value="0000df53">Place Sublet- R (0000df53)</option>
                                                <option value="0000df54">Place Rent- R (0000df54)</option>
                                                <option value="0000df55">Place Share - T (0000df55)</option>
                                                <option value="0000df56">Place Sublet- T (0000df56)</option>
                                                <option value="0000df57">Place Rent- T (0000df57)</option>
                                                <option value="0000df58">Place Share - B (0000df58)</option>
                                                <option value="0000df59">Place Sublet- B (0000df59)</option>
                                                <option value="0000df5c">Place Rent- B (0000df5c)</option>
                                                <option value="0000e32b">Log Sub- J (0000e32b)</option>
                                            </select>
                                        </div>
                                        <div class="selectric">
                                            <p class="label">select campaign</p><b class="button">▾</b></div>
                                        <div class="selectric-items" tabindex="-1">
                                            <div class="selectric-scroll">
                                                <ul>
                                                    <li data-index="0" class="selected">select campaign</li>
                                                    <li data-index="1" class="">SM-M (00008db4)</li>
                                                    <li data-index="2" class="">SM-T (00008ece)</li>
                                                    <li data-index="3" class="">LOG SM-A (00008fca)</li>
                                                    <li data-index="4" class="">SM-J (00009172)</li>
                                                    <li data-index="5" class="">SM-B (00009272)</li>
                                                    <li data-index="6" class="">SM-R (00009466)</li>
                                                    <li data-index="7" class="">SM-S (00009563)</li>
                                                    <li data-index="8" class="">SUB- T (00009847)</li>
                                                    <li data-index="9" class="">SUB- R (0000b46e)</li>
                                                    <li data-index="10" class="">Do not use this champing SUB- J (0000be1f)</li>
                                                    <li data-index="11" class="">SHARE- R (0000d59c)</li>
                                                    <li data-index="12" class="">SHARE- T (0000d59d)</li>
                                                    <li data-index="13" class="">SHARE- J (0000d59e)</li>
                                                    <li data-index="14" class="">SHARE- A (0000d59f)</li>
                                                    <li data-index="15" class="">SHARE- B (0000d5a0)</li>
                                                    <li data-index="16" class="">SUB- S (0000d5a1)</li>
                                                    <li data-index="17" class="">SHARE- M (0000d5a2)</li>
                                                    <li data-index="18" class="">SUB- M (0000d7b4)</li>
                                                    <li data-index="19" class="">SUB- B (0000d7d1)</li>
                                                    <li data-index="20" class="">LOG SUB- A (0000d7d2)</li>
                                                    <li data-index="21" class="">SHARE- S (0000d7d3)</li>
                                                    <li data-index="22" class="">LOG RENT R (0000daaf)</li>
                                                    <li data-index="23" class="">LOG RENT T (0000dab1)</li>
                                                    <li data-index="24" class="">LOG SUB T (0000dab2)</li>
                                                    <li data-index="25" class="">LOG SUB R (0000dbff)</li>
                                                    <li data-index="26" class="">SHARE- AM (0000dc31)</li>
                                                    <li data-index="27" class="">SM- AM (0000dc32)</li>
                                                    <li data-index="28" class="">REP - R (0000dca0)</li>
                                                    <li data-index="29" class="">REP - J (0000dca1)</li>
                                                    <li data-index="30" class="">REP - T (0000dca2)</li>
                                                    <li data-index="31" class="">REP - A (0000dca3)</li>
                                                    <li data-index="32" class="">REP - B (0000dca4)</li>
                                                    <li data-index="33" class="">SHARET -R (0000ddc1)</li>
                                                    <li data-index="34" class="">SUBT- R (0000ddc2)</li>
                                                    <li data-index="35" class="">SMT-R (0000ddc3)</li>
                                                    <li data-index="36" class="">SUBT- J (0000ddc4)</li>
                                                    <li data-index="37" class="">SMT- J (0000ddc5)</li>
                                                    <li data-index="38" class="">SHARET- J (0000ddcd)</li>
                                                    <li data-index="39" class="">SHARET- T (0000ddce)</li>
                                                    <li data-index="40" class="">SUBT- T (0000ddcf)</li>
                                                    <li data-index="41" class="">SMT-T (0000ddd0)</li>
                                                    <li data-index="42" class="">Place Share - R (0000df52)</li>
                                                    <li data-index="43" class="">Place Sublet- R (0000df53)</li>
                                                    <li data-index="44" class="">Place Rent- R (0000df54)</li>
                                                    <li data-index="45" class="">Place Share - T (0000df55)</li>
                                                    <li data-index="46" class="">Place Sublet- T (0000df56)</li>
                                                    <li data-index="47" class="">Place Rent- T (0000df57)</li>
                                                    <li data-index="48" class="">Place Share - B (0000df58)</li>
                                                    <li data-index="49" class="">Place Sublet- B (0000df59)</li>
                                                    <li data-index="50" class="">Place Rent- B (0000df5c)</li>
                                                    <li data-index="51" class="last">Log Sub- J (0000e32b)</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <input class="selectric-input" tabindex="0">
                                    </div>
                                </td>
                                <td>
                                    <input type="button" class="inputsubmit" value="Generate" id="generate-login-link" onclick="GenerateLink('login')">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">Link: <span id="login-link"></span></td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="width: 100%; margin-bottom: 10px;">
                            <tbody>
                            <tr>
                                <th colspan="4" style="background-color: #f0f0f0;">Search page link <a href="#search-help-text" rel="modal:open"><i class="search-help fa fa-question-circle" title=""></i></a></th>
                            </tr>
                            <tr>
                                <td width="30%">
                                    <div class="selectric-wrapper">
                                        <div class="selectric-hide-select">
                                            <select id="search-campaigns" style="display: inline-table;" tabindex="0">
                                                <option value="">select campaign</option>
                                                <option value="00008db4">SM-M (00008db4)</option>
                                                <option value="00008ece">SM-T (00008ece)</option>
                                                <option value="00008fca">LOG SM-A (00008fca)</option>
                                                <option value="00009172">SM-J (00009172)</option>
                                                <option value="00009272">SM-B (00009272)</option>
                                                <option value="00009466">SM-R (00009466)</option>
                                                <option value="00009563">SM-S (00009563)</option>
                                                <option value="00009847">SUB- T (00009847)</option>
                                                <option value="0000b46e">SUB- R (0000b46e)</option>
                                                <option value="0000be1f">Do not use this champing SUB- J (0000be1f)</option>
                                                <option value="0000d59c">SHARE- R (0000d59c)</option>
                                                <option value="0000d59d">SHARE- T (0000d59d)</option>
                                                <option value="0000d59e">SHARE- J (0000d59e)</option>
                                                <option value="0000d59f">SHARE- A (0000d59f)</option>
                                                <option value="0000d5a0">SHARE- B (0000d5a0)</option>
                                                <option value="0000d5a1">SUB- S (0000d5a1)</option>
                                                <option value="0000d5a2">SHARE- M (0000d5a2)</option>
                                                <option value="0000d7b4">SUB- M (0000d7b4)</option>
                                                <option value="0000d7d1">SUB- B (0000d7d1)</option>
                                                <option value="0000d7d2">LOG SUB- A (0000d7d2)</option>
                                                <option value="0000d7d3">SHARE- S (0000d7d3)</option>
                                                <option value="0000daaf">LOG RENT R (0000daaf)</option>
                                                <option value="0000dab1">LOG RENT T (0000dab1)</option>
                                                <option value="0000dab2">LOG SUB T (0000dab2)</option>
                                                <option value="0000dbff">LOG SUB R (0000dbff)</option>
                                                <option value="0000dc31">SHARE- AM (0000dc31)</option>
                                                <option value="0000dc32">SM- AM (0000dc32)</option>
                                                <option value="0000dca0">REP - R (0000dca0)</option>
                                                <option value="0000dca1">REP - J (0000dca1)</option>
                                                <option value="0000dca2">REP - T (0000dca2)</option>
                                                <option value="0000dca3">REP - A (0000dca3)</option>
                                                <option value="0000dca4">REP - B (0000dca4)</option>
                                                <option value="0000ddc1">SHARET -R (0000ddc1)</option>
                                                <option value="0000ddc2">SUBT- R (0000ddc2)</option>
                                                <option value="0000ddc3">SMT-R (0000ddc3)</option>
                                                <option value="0000ddc4">SUBT- J (0000ddc4)</option>
                                                <option value="0000ddc5">SMT- J (0000ddc5)</option>
                                                <option value="0000ddcd">SHARET- J (0000ddcd)</option>
                                                <option value="0000ddce">SHARET- T (0000ddce)</option>
                                                <option value="0000ddcf">SUBT- T (0000ddcf)</option>
                                                <option value="0000ddd0">SMT-T (0000ddd0)</option>
                                                <option value="0000df52">Place Share - R (0000df52)</option>
                                                <option value="0000df53">Place Sublet- R (0000df53)</option>
                                                <option value="0000df54">Place Rent- R (0000df54)</option>
                                                <option value="0000df55">Place Share - T (0000df55)</option>
                                                <option value="0000df56">Place Sublet- T (0000df56)</option>
                                                <option value="0000df57">Place Rent- T (0000df57)</option>
                                                <option value="0000df58">Place Share - B (0000df58)</option>
                                                <option value="0000df59">Place Sublet- B (0000df59)</option>
                                                <option value="0000df5c">Place Rent- B (0000df5c)</option>
                                                <option value="0000e32b">Log Sub- J (0000e32b)</option>
                                            </select>
                                        </div>
                                        <div class="selectric">
                                            <p class="label">select campaign</p><b class="button">▾</b></div>
                                        <div class="selectric-items" tabindex="-1">
                                            <div class="selectric-scroll">
                                                <ul>
                                                    <li data-index="0" class="selected">select campaign</li>
                                                    <li data-index="1" class="">SM-M (00008db4)</li>
                                                    <li data-index="2" class="">SM-T (00008ece)</li>
                                                    <li data-index="3" class="">LOG SM-A (00008fca)</li>
                                                    <li data-index="4" class="">SM-J (00009172)</li>
                                                    <li data-index="5" class="">SM-B (00009272)</li>
                                                    <li data-index="6" class="">SM-R (00009466)</li>
                                                    <li data-index="7" class="">SM-S (00009563)</li>
                                                    <li data-index="8" class="">SUB- T (00009847)</li>
                                                    <li data-index="9" class="">SUB- R (0000b46e)</li>
                                                    <li data-index="10" class="">Do not use this champing SUB- J (0000be1f)</li>
                                                    <li data-index="11" class="">SHARE- R (0000d59c)</li>
                                                    <li data-index="12" class="">SHARE- T (0000d59d)</li>
                                                    <li data-index="13" class="">SHARE- J (0000d59e)</li>
                                                    <li data-index="14" class="">SHARE- A (0000d59f)</li>
                                                    <li data-index="15" class="">SHARE- B (0000d5a0)</li>
                                                    <li data-index="16" class="">SUB- S (0000d5a1)</li>
                                                    <li data-index="17" class="">SHARE- M (0000d5a2)</li>
                                                    <li data-index="18" class="">SUB- M (0000d7b4)</li>
                                                    <li data-index="19" class="">SUB- B (0000d7d1)</li>
                                                    <li data-index="20" class="">LOG SUB- A (0000d7d2)</li>
                                                    <li data-index="21" class="">SHARE- S (0000d7d3)</li>
                                                    <li data-index="22" class="">LOG RENT R (0000daaf)</li>
                                                    <li data-index="23" class="">LOG RENT T (0000dab1)</li>
                                                    <li data-index="24" class="">LOG SUB T (0000dab2)</li>
                                                    <li data-index="25" class="">LOG SUB R (0000dbff)</li>
                                                    <li data-index="26" class="">SHARE- AM (0000dc31)</li>
                                                    <li data-index="27" class="">SM- AM (0000dc32)</li>
                                                    <li data-index="28" class="">REP - R (0000dca0)</li>
                                                    <li data-index="29" class="">REP - J (0000dca1)</li>
                                                    <li data-index="30" class="">REP - T (0000dca2)</li>
                                                    <li data-index="31" class="">REP - A (0000dca3)</li>
                                                    <li data-index="32" class="">REP - B (0000dca4)</li>
                                                    <li data-index="33" class="">SHARET -R (0000ddc1)</li>
                                                    <li data-index="34" class="">SUBT- R (0000ddc2)</li>
                                                    <li data-index="35" class="">SMT-R (0000ddc3)</li>
                                                    <li data-index="36" class="">SUBT- J (0000ddc4)</li>
                                                    <li data-index="37" class="">SMT- J (0000ddc5)</li>
                                                    <li data-index="38" class="">SHARET- J (0000ddcd)</li>
                                                    <li data-index="39" class="">SHARET- T (0000ddce)</li>
                                                    <li data-index="40" class="">SUBT- T (0000ddcf)</li>
                                                    <li data-index="41" class="">SMT-T (0000ddd0)</li>
                                                    <li data-index="42" class="">Place Share - R (0000df52)</li>
                                                    <li data-index="43" class="">Place Sublet- R (0000df53)</li>
                                                    <li data-index="44" class="">Place Rent- R (0000df54)</li>
                                                    <li data-index="45" class="">Place Share - T (0000df55)</li>
                                                    <li data-index="46" class="">Place Sublet- T (0000df56)</li>
                                                    <li data-index="47" class="">Place Rent- T (0000df57)</li>
                                                    <li data-index="48" class="">Place Share - B (0000df58)</li>
                                                    <li data-index="49" class="">Place Sublet- B (0000df59)</li>
                                                    <li data-index="50" class="">Place Rent- B (0000df5c)</li>
                                                    <li data-index="51" class="last">Log Sub- J (0000e32b)</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <input class="selectric-input" tabindex="0">
                                    </div>
                                </td>
                                <td width="30%">
                                    <div class="selectric-wrapper">
                                        <div class="selectric-hide-select">
                                            <select id="search-type" style="display: inline-table;" tabindex="0">
                                                <option value="">select search type</option>
                                                <option value="haveshare">a room</option>
                                                <option value="haveapartment">an entire place</option>
                                                <option value="needroom">a roommate</option>
                                                <option value="needapartment">a tenant</option>
                                            </select>
                                        </div>
                                        <div class="selectric">
                                            <p class="label">select search type</p><b class="button">▾</b></div>
                                        <div class="selectric-items" tabindex="-1">
                                            <div class="selectric-scroll">
                                                <ul>
                                                    <li data-index="0" class="selected">select search type</li>
                                                    <li data-index="1" class="">a room</li>
                                                    <li data-index="2" class="">an entire place</li>
                                                    <li data-index="3" class="">a roommate</li>
                                                    <li data-index="4" class="last">a tenant</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <input class="selectric-input" tabindex="0">
                                    </div>
                                </td>
                                <td width="30%">
                                    <input type="text" id="search-location" class="textInput" placeholder="Type location i.e. New York, NY">
                                </td>
                                <td>
                                    <input type="button" class="inputsubmit" value="Generate" id="generate-search-link" onclick="GenerateLink('search')">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">Link: <span id="search-link"></span></td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="width: 100%; margin-bottom: 10px;">
                            <tbody>
                            <tr>
                                <th colspan="4" style="background-color: #f0f0f0;">Listing page link <a href="#listing-help-text" rel="modal:open"><i class="listing-help fa fa-question-circle" title=""></i></a></th>
                            </tr>
                            <tr>
                                <td width="45%">
                                    <div class="selectric-wrapper">
                                        <div class="selectric-hide-select">
                                            <select id="listing-campaigns" style="display: inline-table;" tabindex="0">
                                                <option value="">select campaign</option>
                                                <option value="00008db4">SM-M (00008db4)</option>
                                                <option value="00008ece">SM-T (00008ece)</option>
                                                <option value="00008fca">LOG SM-A (00008fca)</option>
                                                <option value="00009172">SM-J (00009172)</option>
                                                <option value="00009272">SM-B (00009272)</option>
                                                <option value="00009466">SM-R (00009466)</option>
                                                <option value="00009563">SM-S (00009563)</option>
                                                <option value="00009847">SUB- T (00009847)</option>
                                                <option value="0000b46e">SUB- R (0000b46e)</option>
                                                <option value="0000be1f">Do not use this champing SUB- J (0000be1f)</option>
                                                <option value="0000d59c">SHARE- R (0000d59c)</option>
                                                <option value="0000d59d">SHARE- T (0000d59d)</option>
                                                <option value="0000d59e">SHARE- J (0000d59e)</option>
                                                <option value="0000d59f">SHARE- A (0000d59f)</option>
                                                <option value="0000d5a0">SHARE- B (0000d5a0)</option>
                                                <option value="0000d5a1">SUB- S (0000d5a1)</option>
                                                <option value="0000d5a2">SHARE- M (0000d5a2)</option>
                                                <option value="0000d7b4">SUB- M (0000d7b4)</option>
                                                <option value="0000d7d1">SUB- B (0000d7d1)</option>
                                                <option value="0000d7d2">LOG SUB- A (0000d7d2)</option>
                                                <option value="0000d7d3">SHARE- S (0000d7d3)</option>
                                                <option value="0000daaf">LOG RENT R (0000daaf)</option>
                                                <option value="0000dab1">LOG RENT T (0000dab1)</option>
                                                <option value="0000dab2">LOG SUB T (0000dab2)</option>
                                                <option value="0000dbff">LOG SUB R (0000dbff)</option>
                                                <option value="0000dc31">SHARE- AM (0000dc31)</option>
                                                <option value="0000dc32">SM- AM (0000dc32)</option>
                                                <option value="0000dca0">REP - R (0000dca0)</option>
                                                <option value="0000dca1">REP - J (0000dca1)</option>
                                                <option value="0000dca2">REP - T (0000dca2)</option>
                                                <option value="0000dca3">REP - A (0000dca3)</option>
                                                <option value="0000dca4">REP - B (0000dca4)</option>
                                                <option value="0000ddc1">SHARET -R (0000ddc1)</option>
                                                <option value="0000ddc2">SUBT- R (0000ddc2)</option>
                                                <option value="0000ddc3">SMT-R (0000ddc3)</option>
                                                <option value="0000ddc4">SUBT- J (0000ddc4)</option>
                                                <option value="0000ddc5">SMT- J (0000ddc5)</option>
                                                <option value="0000ddcd">SHARET- J (0000ddcd)</option>
                                                <option value="0000ddce">SHARET- T (0000ddce)</option>
                                                <option value="0000ddcf">SUBT- T (0000ddcf)</option>
                                                <option value="0000ddd0">SMT-T (0000ddd0)</option>
                                                <option value="0000df52">Place Share - R (0000df52)</option>
                                                <option value="0000df53">Place Sublet- R (0000df53)</option>
                                                <option value="0000df54">Place Rent- R (0000df54)</option>
                                                <option value="0000df55">Place Share - T (0000df55)</option>
                                                <option value="0000df56">Place Sublet- T (0000df56)</option>
                                                <option value="0000df57">Place Rent- T (0000df57)</option>
                                                <option value="0000df58">Place Share - B (0000df58)</option>
                                                <option value="0000df59">Place Sublet- B (0000df59)</option>
                                                <option value="0000df5c">Place Rent- B (0000df5c)</option>
                                                <option value="0000e32b">Log Sub- J (0000e32b)</option>
                                            </select>
                                        </div>
                                        <div class="selectric">
                                            <p class="label">select campaign</p><b class="button">▾</b></div>
                                        <div class="selectric-items" tabindex="-1">
                                            <div class="selectric-scroll">
                                                <ul>
                                                    <li data-index="0" class="selected">select campaign</li>
                                                    <li data-index="1" class="">SM-M (00008db4)</li>
                                                    <li data-index="2" class="">SM-T (00008ece)</li>
                                                    <li data-index="3" class="">LOG SM-A (00008fca)</li>
                                                    <li data-index="4" class="">SM-J (00009172)</li>
                                                    <li data-index="5" class="">SM-B (00009272)</li>
                                                    <li data-index="6" class="">SM-R (00009466)</li>
                                                    <li data-index="7" class="">SM-S (00009563)</li>
                                                    <li data-index="8" class="">SUB- T (00009847)</li>
                                                    <li data-index="9" class="">SUB- R (0000b46e)</li>
                                                    <li data-index="10" class="">Do not use this champing SUB- J (0000be1f)</li>
                                                    <li data-index="11" class="">SHARE- R (0000d59c)</li>
                                                    <li data-index="12" class="">SHARE- T (0000d59d)</li>
                                                    <li data-index="13" class="">SHARE- J (0000d59e)</li>
                                                    <li data-index="14" class="">SHARE- A (0000d59f)</li>
                                                    <li data-index="15" class="">SHARE- B (0000d5a0)</li>
                                                    <li data-index="16" class="">SUB- S (0000d5a1)</li>
                                                    <li data-index="17" class="">SHARE- M (0000d5a2)</li>
                                                    <li data-index="18" class="">SUB- M (0000d7b4)</li>
                                                    <li data-index="19" class="">SUB- B (0000d7d1)</li>
                                                    <li data-index="20" class="">LOG SUB- A (0000d7d2)</li>
                                                    <li data-index="21" class="">SHARE- S (0000d7d3)</li>
                                                    <li data-index="22" class="">LOG RENT R (0000daaf)</li>
                                                    <li data-index="23" class="">LOG RENT T (0000dab1)</li>
                                                    <li data-index="24" class="">LOG SUB T (0000dab2)</li>
                                                    <li data-index="25" class="">LOG SUB R (0000dbff)</li>
                                                    <li data-index="26" class="">SHARE- AM (0000dc31)</li>
                                                    <li data-index="27" class="">SM- AM (0000dc32)</li>
                                                    <li data-index="28" class="">REP - R (0000dca0)</li>
                                                    <li data-index="29" class="">REP - J (0000dca1)</li>
                                                    <li data-index="30" class="">REP - T (0000dca2)</li>
                                                    <li data-index="31" class="">REP - A (0000dca3)</li>
                                                    <li data-index="32" class="">REP - B (0000dca4)</li>
                                                    <li data-index="33" class="">SHARET -R (0000ddc1)</li>
                                                    <li data-index="34" class="">SUBT- R (0000ddc2)</li>
                                                    <li data-index="35" class="">SMT-R (0000ddc3)</li>
                                                    <li data-index="36" class="">SUBT- J (0000ddc4)</li>
                                                    <li data-index="37" class="">SMT- J (0000ddc5)</li>
                                                    <li data-index="38" class="">SHARET- J (0000ddcd)</li>
                                                    <li data-index="39" class="">SHARET- T (0000ddce)</li>
                                                    <li data-index="40" class="">SUBT- T (0000ddcf)</li>
                                                    <li data-index="41" class="">SMT-T (0000ddd0)</li>
                                                    <li data-index="42" class="">Place Share - R (0000df52)</li>
                                                    <li data-index="43" class="">Place Sublet- R (0000df53)</li>
                                                    <li data-index="44" class="">Place Rent- R (0000df54)</li>
                                                    <li data-index="45" class="">Place Share - T (0000df55)</li>
                                                    <li data-index="46" class="">Place Sublet- T (0000df56)</li>
                                                    <li data-index="47" class="">Place Rent- T (0000df57)</li>
                                                    <li data-index="48" class="">Place Share - B (0000df58)</li>
                                                    <li data-index="49" class="">Place Sublet- B (0000df59)</li>
                                                    <li data-index="50" class="">Place Rent- B (0000df5c)</li>
                                                    <li data-index="51" class="last">Log Sub- J (0000e32b)</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <input class="selectric-input" tabindex="0">
                                    </div>
                                </td>
                                <td width="45%">
                                    <input type="text" id="listing-id" class="textInput" placeholder="Type listing id">
                                </td>
                                <td>
                                    <input type="button" class="inputsubmit" value="Generate" id="generate-listing-link" onclick="GenerateLink('listing')">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">Link: <span id="listing-link"></span></td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="width: 100%; margin-bottom: 10px;">
                            <tbody>
                            <tr>
                                <th colspan="4" style="background-color: #f0f0f0;">User profile link <a href="#user-help-text" rel="modal:open"><i class="user-help fa fa-question-circle" title=""></i></a></th>
                            </tr>
                            <tr>
                                <td width="45%">
                                    <div class="selectric-wrapper">
                                        <div class="selectric-hide-select">
                                            <select id="user-campaigns" style="display: inline-table;" tabindex="0">
                                                <option value="">select campaign</option>
                                                <option value="00008db4">SM-M (00008db4)</option>
                                                <option value="00008ece">SM-T (00008ece)</option>
                                                <option value="00008fca">LOG SM-A (00008fca)</option>
                                                <option value="00009172">SM-J (00009172)</option>
                                                <option value="00009272">SM-B (00009272)</option>
                                                <option value="00009466">SM-R (00009466)</option>
                                                <option value="00009563">SM-S (00009563)</option>
                                                <option value="00009847">SUB- T (00009847)</option>
                                                <option value="0000b46e">SUB- R (0000b46e)</option>
                                                <option value="0000be1f">Do not use this champing SUB- J (0000be1f)</option>
                                                <option value="0000d59c">SHARE- R (0000d59c)</option>
                                                <option value="0000d59d">SHARE- T (0000d59d)</option>
                                                <option value="0000d59e">SHARE- J (0000d59e)</option>
                                                <option value="0000d59f">SHARE- A (0000d59f)</option>
                                                <option value="0000d5a0">SHARE- B (0000d5a0)</option>
                                                <option value="0000d5a1">SUB- S (0000d5a1)</option>
                                                <option value="0000d5a2">SHARE- M (0000d5a2)</option>
                                                <option value="0000d7b4">SUB- M (0000d7b4)</option>
                                                <option value="0000d7d1">SUB- B (0000d7d1)</option>
                                                <option value="0000d7d2">LOG SUB- A (0000d7d2)</option>
                                                <option value="0000d7d3">SHARE- S (0000d7d3)</option>
                                                <option value="0000daaf">LOG RENT R (0000daaf)</option>
                                                <option value="0000dab1">LOG RENT T (0000dab1)</option>
                                                <option value="0000dab2">LOG SUB T (0000dab2)</option>
                                                <option value="0000dbff">LOG SUB R (0000dbff)</option>
                                                <option value="0000dc31">SHARE- AM (0000dc31)</option>
                                                <option value="0000dc32">SM- AM (0000dc32)</option>
                                                <option value="0000dca0">REP - R (0000dca0)</option>
                                                <option value="0000dca1">REP - J (0000dca1)</option>
                                                <option value="0000dca2">REP - T (0000dca2)</option>
                                                <option value="0000dca3">REP - A (0000dca3)</option>
                                                <option value="0000dca4">REP - B (0000dca4)</option>
                                                <option value="0000ddc1">SHARET -R (0000ddc1)</option>
                                                <option value="0000ddc2">SUBT- R (0000ddc2)</option>
                                                <option value="0000ddc3">SMT-R (0000ddc3)</option>
                                                <option value="0000ddc4">SUBT- J (0000ddc4)</option>
                                                <option value="0000ddc5">SMT- J (0000ddc5)</option>
                                                <option value="0000ddcd">SHARET- J (0000ddcd)</option>
                                                <option value="0000ddce">SHARET- T (0000ddce)</option>
                                                <option value="0000ddcf">SUBT- T (0000ddcf)</option>
                                                <option value="0000ddd0">SMT-T (0000ddd0)</option>
                                                <option value="0000df52">Place Share - R (0000df52)</option>
                                                <option value="0000df53">Place Sublet- R (0000df53)</option>
                                                <option value="0000df54">Place Rent- R (0000df54)</option>
                                                <option value="0000df55">Place Share - T (0000df55)</option>
                                                <option value="0000df56">Place Sublet- T (0000df56)</option>
                                                <option value="0000df57">Place Rent- T (0000df57)</option>
                                                <option value="0000df58">Place Share - B (0000df58)</option>
                                                <option value="0000df59">Place Sublet- B (0000df59)</option>
                                                <option value="0000df5c">Place Rent- B (0000df5c)</option>
                                                <option value="0000e32b">Log Sub- J (0000e32b)</option>
                                            </select>
                                        </div>
                                        <div class="selectric">
                                            <p class="label">select campaign</p><b class="button">▾</b></div>
                                        <div class="selectric-items" tabindex="-1">
                                            <div class="selectric-scroll">
                                                <ul>
                                                    <li data-index="0" class="selected">select campaign</li>
                                                    <li data-index="1" class="">SM-M (00008db4)</li>
                                                    <li data-index="2" class="">SM-T (00008ece)</li>
                                                    <li data-index="3" class="">LOG SM-A (00008fca)</li>
                                                    <li data-index="4" class="">SM-J (00009172)</li>
                                                    <li data-index="5" class="">SM-B (00009272)</li>
                                                    <li data-index="6" class="">SM-R (00009466)</li>
                                                    <li data-index="7" class="">SM-S (00009563)</li>
                                                    <li data-index="8" class="">SUB- T (00009847)</li>
                                                    <li data-index="9" class="">SUB- R (0000b46e)</li>
                                                    <li data-index="10" class="">Do not use this champing SUB- J (0000be1f)</li>
                                                    <li data-index="11" class="">SHARE- R (0000d59c)</li>
                                                    <li data-index="12" class="">SHARE- T (0000d59d)</li>
                                                    <li data-index="13" class="">SHARE- J (0000d59e)</li>
                                                    <li data-index="14" class="">SHARE- A (0000d59f)</li>
                                                    <li data-index="15" class="">SHARE- B (0000d5a0)</li>
                                                    <li data-index="16" class="">SUB- S (0000d5a1)</li>
                                                    <li data-index="17" class="">SHARE- M (0000d5a2)</li>
                                                    <li data-index="18" class="">SUB- M (0000d7b4)</li>
                                                    <li data-index="19" class="">SUB- B (0000d7d1)</li>
                                                    <li data-index="20" class="">LOG SUB- A (0000d7d2)</li>
                                                    <li data-index="21" class="">SHARE- S (0000d7d3)</li>
                                                    <li data-index="22" class="">LOG RENT R (0000daaf)</li>
                                                    <li data-index="23" class="">LOG RENT T (0000dab1)</li>
                                                    <li data-index="24" class="">LOG SUB T (0000dab2)</li>
                                                    <li data-index="25" class="">LOG SUB R (0000dbff)</li>
                                                    <li data-index="26" class="">SHARE- AM (0000dc31)</li>
                                                    <li data-index="27" class="">SM- AM (0000dc32)</li>
                                                    <li data-index="28" class="">REP - R (0000dca0)</li>
                                                    <li data-index="29" class="">REP - J (0000dca1)</li>
                                                    <li data-index="30" class="">REP - T (0000dca2)</li>
                                                    <li data-index="31" class="">REP - A (0000dca3)</li>
                                                    <li data-index="32" class="">REP - B (0000dca4)</li>
                                                    <li data-index="33" class="">SHARET -R (0000ddc1)</li>
                                                    <li data-index="34" class="">SUBT- R (0000ddc2)</li>
                                                    <li data-index="35" class="">SMT-R (0000ddc3)</li>
                                                    <li data-index="36" class="">SUBT- J (0000ddc4)</li>
                                                    <li data-index="37" class="">SMT- J (0000ddc5)</li>
                                                    <li data-index="38" class="">SHARET- J (0000ddcd)</li>
                                                    <li data-index="39" class="">SHARET- T (0000ddce)</li>
                                                    <li data-index="40" class="">SUBT- T (0000ddcf)</li>
                                                    <li data-index="41" class="">SMT-T (0000ddd0)</li>
                                                    <li data-index="42" class="">Place Share - R (0000df52)</li>
                                                    <li data-index="43" class="">Place Sublet- R (0000df53)</li>
                                                    <li data-index="44" class="">Place Rent- R (0000df54)</li>
                                                    <li data-index="45" class="">Place Share - T (0000df55)</li>
                                                    <li data-index="46" class="">Place Sublet- T (0000df56)</li>
                                                    <li data-index="47" class="">Place Rent- T (0000df57)</li>
                                                    <li data-index="48" class="">Place Share - B (0000df58)</li>
                                                    <li data-index="49" class="">Place Sublet- B (0000df59)</li>
                                                    <li data-index="50" class="">Place Rent- B (0000df5c)</li>
                                                    <li data-index="51" class="last">Log Sub- J (0000e32b)</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <input class="selectric-input" tabindex="0">
                                    </div>
                                </td>
                                <td width="45%">
                                    <input type="text" id="user-id" class="textInput" placeholder="Type user id">
                                </td>
                                <td>
                                    <input type="button" class="inputsubmit" value="Generate" id="generate-user-link" onclick="GenerateLink('user')">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">Link: <span id="user-link"></span></td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="width: 100%; margin-bottom: 10px;">
                            <tbody>
                            <tr>
                                <th colspan="4" style="background-color: #f0f0f0;">Apple App Store and Google PlayStore link <a href="#app-help-text" rel="modal:open"><i class="app-help fa fa-question-circle" title=""></i></a></th>
                            </tr>
                            <tr>
                                <td width="90%">
                                    <div class="selectric-wrapper">
                                        <div class="selectric-hide-select">
                                            <select id="app-campaigns" style="display: inline-table;" tabindex="0">
                                                <option value="">select campaign</option>
                                                <option value="00008db4">SM-M (00008db4)</option>
                                                <option value="00008ece">SM-T (00008ece)</option>
                                                <option value="00008fca">LOG SM-A (00008fca)</option>
                                                <option value="00009172">SM-J (00009172)</option>
                                                <option value="00009272">SM-B (00009272)</option>
                                                <option value="00009466">SM-R (00009466)</option>
                                                <option value="00009563">SM-S (00009563)</option>
                                                <option value="00009847">SUB- T (00009847)</option>
                                                <option value="0000b46e">SUB- R (0000b46e)</option>
                                                <option value="0000be1f">Do not use this champing SUB- J (0000be1f)</option>
                                                <option value="0000d59c">SHARE- R (0000d59c)</option>
                                                <option value="0000d59d">SHARE- T (0000d59d)</option>
                                                <option value="0000d59e">SHARE- J (0000d59e)</option>
                                                <option value="0000d59f">SHARE- A (0000d59f)</option>
                                                <option value="0000d5a0">SHARE- B (0000d5a0)</option>
                                                <option value="0000d5a1">SUB- S (0000d5a1)</option>
                                                <option value="0000d5a2">SHARE- M (0000d5a2)</option>
                                                <option value="0000d7b4">SUB- M (0000d7b4)</option>
                                                <option value="0000d7d1">SUB- B (0000d7d1)</option>
                                                <option value="0000d7d2">LOG SUB- A (0000d7d2)</option>
                                                <option value="0000d7d3">SHARE- S (0000d7d3)</option>
                                                <option value="0000daaf">LOG RENT R (0000daaf)</option>
                                                <option value="0000dab1">LOG RENT T (0000dab1)</option>
                                                <option value="0000dab2">LOG SUB T (0000dab2)</option>
                                                <option value="0000dbff">LOG SUB R (0000dbff)</option>
                                                <option value="0000dc31">SHARE- AM (0000dc31)</option>
                                                <option value="0000dc32">SM- AM (0000dc32)</option>
                                                <option value="0000dca0">REP - R (0000dca0)</option>
                                                <option value="0000dca1">REP - J (0000dca1)</option>
                                                <option value="0000dca2">REP - T (0000dca2)</option>
                                                <option value="0000dca3">REP - A (0000dca3)</option>
                                                <option value="0000dca4">REP - B (0000dca4)</option>
                                                <option value="0000ddc1">SHARET -R (0000ddc1)</option>
                                                <option value="0000ddc2">SUBT- R (0000ddc2)</option>
                                                <option value="0000ddc3">SMT-R (0000ddc3)</option>
                                                <option value="0000ddc4">SUBT- J (0000ddc4)</option>
                                                <option value="0000ddc5">SMT- J (0000ddc5)</option>
                                                <option value="0000ddcd">SHARET- J (0000ddcd)</option>
                                                <option value="0000ddce">SHARET- T (0000ddce)</option>
                                                <option value="0000ddcf">SUBT- T (0000ddcf)</option>
                                                <option value="0000ddd0">SMT-T (0000ddd0)</option>
                                                <option value="0000df52">Place Share - R (0000df52)</option>
                                                <option value="0000df53">Place Sublet- R (0000df53)</option>
                                                <option value="0000df54">Place Rent- R (0000df54)</option>
                                                <option value="0000df55">Place Share - T (0000df55)</option>
                                                <option value="0000df56">Place Sublet- T (0000df56)</option>
                                                <option value="0000df57">Place Rent- T (0000df57)</option>
                                                <option value="0000df58">Place Share - B (0000df58)</option>
                                                <option value="0000df59">Place Sublet- B (0000df59)</option>
                                                <option value="0000df5c">Place Rent- B (0000df5c)</option>
                                                <option value="0000e32b">Log Sub- J (0000e32b)</option>
                                            </select>
                                        </div>
                                        <div class="selectric">
                                            <p class="label">select campaign</p><b class="button">▾</b></div>
                                        <div class="selectric-items" tabindex="-1">
                                            <div class="selectric-scroll">
                                                <ul>
                                                    <li data-index="0" class="selected">select campaign</li>
                                                    <li data-index="1" class="">SM-M (00008db4)</li>
                                                    <li data-index="2" class="">SM-T (00008ece)</li>
                                                    <li data-index="3" class="">LOG SM-A (00008fca)</li>
                                                    <li data-index="4" class="">SM-J (00009172)</li>
                                                    <li data-index="5" class="">SM-B (00009272)</li>
                                                    <li data-index="6" class="">SM-R (00009466)</li>
                                                    <li data-index="7" class="">SM-S (00009563)</li>
                                                    <li data-index="8" class="">SUB- T (00009847)</li>
                                                    <li data-index="9" class="">SUB- R (0000b46e)</li>
                                                    <li data-index="10" class="">Do not use this champing SUB- J (0000be1f)</li>
                                                    <li data-index="11" class="">SHARE- R (0000d59c)</li>
                                                    <li data-index="12" class="">SHARE- T (0000d59d)</li>
                                                    <li data-index="13" class="">SHARE- J (0000d59e)</li>
                                                    <li data-index="14" class="">SHARE- A (0000d59f)</li>
                                                    <li data-index="15" class="">SHARE- B (0000d5a0)</li>
                                                    <li data-index="16" class="">SUB- S (0000d5a1)</li>
                                                    <li data-index="17" class="">SHARE- M (0000d5a2)</li>
                                                    <li data-index="18" class="">SUB- M (0000d7b4)</li>
                                                    <li data-index="19" class="">SUB- B (0000d7d1)</li>
                                                    <li data-index="20" class="">LOG SUB- A (0000d7d2)</li>
                                                    <li data-index="21" class="">SHARE- S (0000d7d3)</li>
                                                    <li data-index="22" class="">LOG RENT R (0000daaf)</li>
                                                    <li data-index="23" class="">LOG RENT T (0000dab1)</li>
                                                    <li data-index="24" class="">LOG SUB T (0000dab2)</li>
                                                    <li data-index="25" class="">LOG SUB R (0000dbff)</li>
                                                    <li data-index="26" class="">SHARE- AM (0000dc31)</li>
                                                    <li data-index="27" class="">SM- AM (0000dc32)</li>
                                                    <li data-index="28" class="">REP - R (0000dca0)</li>
                                                    <li data-index="29" class="">REP - J (0000dca1)</li>
                                                    <li data-index="30" class="">REP - T (0000dca2)</li>
                                                    <li data-index="31" class="">REP - A (0000dca3)</li>
                                                    <li data-index="32" class="">REP - B (0000dca4)</li>
                                                    <li data-index="33" class="">SHARET -R (0000ddc1)</li>
                                                    <li data-index="34" class="">SUBT- R (0000ddc2)</li>
                                                    <li data-index="35" class="">SMT-R (0000ddc3)</li>
                                                    <li data-index="36" class="">SUBT- J (0000ddc4)</li>
                                                    <li data-index="37" class="">SMT- J (0000ddc5)</li>
                                                    <li data-index="38" class="">SHARET- J (0000ddcd)</li>
                                                    <li data-index="39" class="">SHARET- T (0000ddce)</li>
                                                    <li data-index="40" class="">SUBT- T (0000ddcf)</li>
                                                    <li data-index="41" class="">SMT-T (0000ddd0)</li>
                                                    <li data-index="42" class="">Place Share - R (0000df52)</li>
                                                    <li data-index="43" class="">Place Sublet- R (0000df53)</li>
                                                    <li data-index="44" class="">Place Rent- R (0000df54)</li>
                                                    <li data-index="45" class="">Place Share - T (0000df55)</li>
                                                    <li data-index="46" class="">Place Sublet- T (0000df56)</li>
                                                    <li data-index="47" class="">Place Rent- T (0000df57)</li>
                                                    <li data-index="48" class="">Place Share - B (0000df58)</li>
                                                    <li data-index="49" class="">Place Sublet- B (0000df59)</li>
                                                    <li data-index="50" class="">Place Rent- B (0000df5c)</li>
                                                    <li data-index="51" class="last">Log Sub- J (0000e32b)</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <input class="selectric-input" tabindex="0">
                                    </div>
                                </td>
                                <td>
                                    <input type="button" class="inputsubmit" value="Generate" id="generate-app-link" onclick="GenerateLink('app')">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">Link: <span id="app-link"></span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div id="index-help-text" style="display: none;">
                        <div class="default-styles">
                            <h3>Home page</h3>
                            <img src="https://d1frv26ioc12ph.cloudfront.net/Content/images/affiliates/index-help.png">
                        </div>
                    </div>

                    <div id="login-help-text" style="display: none;">
                        <div class="default-styles">
                            <h3>Login/Sign up page</h3>
                            <img src="https://d1frv26ioc12ph.cloudfront.net/Content/images/affiliates/login-help.png">
                        </div>
                    </div>

                    <div id="search-help-text" style="display: none;">
                        <div class="default-styles">
                            <h3>Search results by type and location</h3>
                            <img src="https://d1frv26ioc12ph.cloudfront.net/Content/images/affiliates/search-help.png">
                        </div>
                    </div>

                    <div id="listing-help-text" style="display: none;">
                        <div class="default-styles">
                            <h3>Send traffic directly to specific lisitngs</h3>
                            <img src="https://d1frv26ioc12ph.cloudfront.net/Content/images/affiliates/listing-help.png">
                        </div>
                    </div>

                    <div id="user-help-text" style="display: none;">
                        <div class="default-styles">
                            <h3>Send traffic directly to specific user profiles</h3>
                            <img src="https://d1frv26ioc12ph.cloudfront.net/Content/images/affiliates/user-help.png">
                        </div>
                    </div>

                    <div id="app-help-text" style="display: none;">
                        <div class="-default-styles">
                            <h3>Send traffic to download apps directly. Android users will be directed to PlayStore. iPhone users will be directed to iTunes App Store. *Leads will be counted the same as if the traffic goes to the site.</h3>
                            <img src="https://d1frv26ioc12ph.cloudfront.net/Content/images/affiliates/app-help.png">
                        </div>
                    </div>

                </div>
                <div id="tabs-1" style="display: none" aria-labelledby="ui-id-13" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="false" aria-hidden="true">
                    <div>
                        <table style="width: 100%">
                            <tbody>
                            <tr>
                                <th width="50%">Campaign</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th>Download</th>
                            </tr>

                            <tr>
                                <td>
                                    SM-M (00008db4)
                                </td>
                                <td>
                                    <span id="feed-status-00008db4">Active</span>
                                </td>
                                <td>
                                    <a id="status-action-00008db4" class="status-action" data-action="Pause" data-campaign="00008db4"> Pause</a>
                                    <div class="floatRight">
                                        <a style="" id="feed-edit-00008db4" onclick="setFeedOptions('00008db4', 'SM-M (00008db4) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-00008db4">

                                        <span style="color: #999; padding: 0;">Scheduled. Available within 24 hrs.</span>

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SM-T (00008ece)
                                </td>
                                <td>
                                    <span id="feed-status-00008ece">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-00008ece" class="status-action" data-action="Activate" data-campaign="00008ece"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-00008ece" onclick="setFeedOptions('00008ece', 'SM-T  (00008ece) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-00008ece">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    LOG SM-A (00008fca)
                                </td>
                                <td>
                                    <span id="feed-status-00008fca">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-00008fca" class="status-action" data-action="Activate" data-campaign="00008fca"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-00008fca" onclick="setFeedOptions('00008fca', 'LOG SM-A (00008fca) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-00008fca">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SM-J (00009172)
                                </td>
                                <td>
                                    <span id="feed-status-00009172">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-00009172" class="status-action" data-action="Activate" data-campaign="00009172"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-00009172" onclick="setFeedOptions('00009172', 'SM-J (00009172) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-00009172">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SM-B (00009272)
                                </td>
                                <td>
                                    <span id="feed-status-00009272">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-00009272" class="status-action" data-action="Activate" data-campaign="00009272"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-00009272" onclick="setFeedOptions('00009272', 'SM-B (00009272) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-00009272">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SM-R (00009466)
                                </td>
                                <td>
                                    <span id="feed-status-00009466">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-00009466" class="status-action" data-action="Activate" data-campaign="00009466"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-00009466" onclick="setFeedOptions('00009466', 'SM-R (00009466) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-00009466">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SM-S (00009563)
                                </td>
                                <td>
                                    <span id="feed-status-00009563">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-00009563" class="status-action" data-action="Activate" data-campaign="00009563"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-00009563" onclick="setFeedOptions('00009563', 'SM-S (00009563) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-00009563">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SUB- T (00009847)
                                </td>
                                <td>
                                    <span id="feed-status-00009847">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-00009847" class="status-action" data-action="Activate" data-campaign="00009847"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-00009847" onclick="setFeedOptions('00009847', 'SUB- T (00009847) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-00009847">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SUB- R (0000b46e)
                                </td>
                                <td>
                                    <span id="feed-status-0000b46e">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000b46e" class="status-action" data-action="Activate" data-campaign="0000b46e"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000b46e" onclick="setFeedOptions('0000b46e', 'SUB- R (0000b46e) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000b46e">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Do not use this champing SUB- J (0000be1f)
                                </td>
                                <td>
                                    <span id="feed-status-0000be1f">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000be1f" class="status-action" data-action="Activate" data-campaign="0000be1f"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000be1f" onclick="setFeedOptions('0000be1f', 'Do not use this champing SUB- J (0000be1f) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000be1f">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SHARE- R (0000d59c)
                                </td>
                                <td>
                                    <span id="feed-status-0000d59c">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000d59c" class="status-action" data-action="Activate" data-campaign="0000d59c"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000d59c" onclick="setFeedOptions('0000d59c', 'SHARE- R (0000d59c) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000d59c">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SHARE- T (0000d59d)
                                </td>
                                <td>
                                    <span id="feed-status-0000d59d">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000d59d" class="status-action" data-action="Activate" data-campaign="0000d59d"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000d59d" onclick="setFeedOptions('0000d59d', 'SHARE- T (0000d59d) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000d59d">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SHARE- J (0000d59e)
                                </td>
                                <td>
                                    <span id="feed-status-0000d59e">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000d59e" class="status-action" data-action="Activate" data-campaign="0000d59e"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000d59e" onclick="setFeedOptions('0000d59e', 'SHARE- J (0000d59e) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000d59e">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SHARE- A (0000d59f)
                                </td>
                                <td>
                                    <span id="feed-status-0000d59f">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000d59f" class="status-action" data-action="Activate" data-campaign="0000d59f"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000d59f" onclick="setFeedOptions('0000d59f', 'SHARE- A (0000d59f) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000d59f">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SHARE- B (0000d5a0)
                                </td>
                                <td>
                                    <span id="feed-status-0000d5a0">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000d5a0" class="status-action" data-action="Activate" data-campaign="0000d5a0"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000d5a0" onclick="setFeedOptions('0000d5a0', 'SHARE- B (0000d5a0) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000d5a0">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SUB- S (0000d5a1)
                                </td>
                                <td>
                                    <span id="feed-status-0000d5a1">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000d5a1" class="status-action" data-action="Activate" data-campaign="0000d5a1"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000d5a1" onclick="setFeedOptions('0000d5a1', 'SUB- S (0000d5a1) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000d5a1">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SHARE- M (0000d5a2)
                                </td>
                                <td>
                                    <span id="feed-status-0000d5a2">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000d5a2" class="status-action" data-action="Activate" data-campaign="0000d5a2"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000d5a2" onclick="setFeedOptions('0000d5a2', 'SHARE- M (0000d5a2) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000d5a2">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SUB- M (0000d7b4)
                                </td>
                                <td>
                                    <span id="feed-status-0000d7b4">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000d7b4" class="status-action" data-action="Activate" data-campaign="0000d7b4"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000d7b4" onclick="setFeedOptions('0000d7b4', 'SUB- M (0000d7b4) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000d7b4">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SUB- B (0000d7d1)
                                </td>
                                <td>
                                    <span id="feed-status-0000d7d1">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000d7d1" class="status-action" data-action="Activate" data-campaign="0000d7d1"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000d7d1" onclick="setFeedOptions('0000d7d1', 'SUB- B (0000d7d1) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000d7d1">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    LOG SUB- A (0000d7d2)
                                </td>
                                <td>
                                    <span id="feed-status-0000d7d2">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000d7d2" class="status-action" data-action="Activate" data-campaign="0000d7d2"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000d7d2" onclick="setFeedOptions('0000d7d2', 'LOG SUB- A (0000d7d2) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000d7d2">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SHARE- S (0000d7d3)
                                </td>
                                <td>
                                    <span id="feed-status-0000d7d3">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000d7d3" class="status-action" data-action="Activate" data-campaign="0000d7d3"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000d7d3" onclick="setFeedOptions('0000d7d3', 'SHARE- S (0000d7d3) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000d7d3">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    LOG RENT R (0000daaf)
                                </td>
                                <td>
                                    <span id="feed-status-0000daaf">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000daaf" class="status-action" data-action="Activate" data-campaign="0000daaf"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000daaf" onclick="setFeedOptions('0000daaf', 'LOG RENT R (0000daaf) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000daaf">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    LOG RENT T (0000dab1)
                                </td>
                                <td>
                                    <span id="feed-status-0000dab1">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000dab1" class="status-action" data-action="Activate" data-campaign="0000dab1"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000dab1" onclick="setFeedOptions('0000dab1', 'LOG RENT T (0000dab1) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000dab1">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    LOG SUB T (0000dab2)
                                </td>
                                <td>
                                    <span id="feed-status-0000dab2">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000dab2" class="status-action" data-action="Activate" data-campaign="0000dab2"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000dab2" onclick="setFeedOptions('0000dab2', 'LOG SUB T (0000dab2) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000dab2">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    LOG SUB R (0000dbff)
                                </td>
                                <td>
                                    <span id="feed-status-0000dbff">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000dbff" class="status-action" data-action="Activate" data-campaign="0000dbff"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000dbff" onclick="setFeedOptions('0000dbff', 'LOG SUB R (0000dbff) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000dbff">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SHARE- AM (0000dc31)
                                </td>
                                <td>
                                    <span id="feed-status-0000dc31">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000dc31" class="status-action" data-action="Activate" data-campaign="0000dc31"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000dc31" onclick="setFeedOptions('0000dc31', 'SHARE- AM (0000dc31) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000dc31">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SM- AM (0000dc32)
                                </td>
                                <td>
                                    <span id="feed-status-0000dc32">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000dc32" class="status-action" data-action="Activate" data-campaign="0000dc32"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000dc32" onclick="setFeedOptions('0000dc32', 'SM- AM (0000dc32) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000dc32">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    REP - R (0000dca0)
                                </td>
                                <td>
                                    <span id="feed-status-0000dca0">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000dca0" class="status-action" data-action="Activate" data-campaign="0000dca0"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000dca0" onclick="setFeedOptions('0000dca0', 'REP - R (0000dca0) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000dca0">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    REP - J (0000dca1)
                                </td>
                                <td>
                                    <span id="feed-status-0000dca1">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000dca1" class="status-action" data-action="Activate" data-campaign="0000dca1"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000dca1" onclick="setFeedOptions('0000dca1', 'REP - J (0000dca1) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000dca1">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    REP - T (0000dca2)
                                </td>
                                <td>
                                    <span id="feed-status-0000dca2">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000dca2" class="status-action" data-action="Activate" data-campaign="0000dca2"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000dca2" onclick="setFeedOptions('0000dca2', 'REP - T (0000dca2) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000dca2">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    REP - A (0000dca3)
                                </td>
                                <td>
                                    <span id="feed-status-0000dca3">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000dca3" class="status-action" data-action="Activate" data-campaign="0000dca3"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000dca3" onclick="setFeedOptions('0000dca3', 'REP - A (0000dca3) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000dca3">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    REP - B (0000dca4)
                                </td>
                                <td>
                                    <span id="feed-status-0000dca4">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000dca4" class="status-action" data-action="Activate" data-campaign="0000dca4"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000dca4" onclick="setFeedOptions('0000dca4', 'REP - B (0000dca4) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000dca4">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SHARET -R (0000ddc1)
                                </td>
                                <td>
                                    <span id="feed-status-0000ddc1">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000ddc1" class="status-action" data-action="Activate" data-campaign="0000ddc1"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000ddc1" onclick="setFeedOptions('0000ddc1', 'SHARET -R (0000ddc1) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000ddc1">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SUBT- R (0000ddc2)
                                </td>
                                <td>
                                    <span id="feed-status-0000ddc2">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000ddc2" class="status-action" data-action="Activate" data-campaign="0000ddc2"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000ddc2" onclick="setFeedOptions('0000ddc2', 'SUBT- R (0000ddc2) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000ddc2">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SMT-R (0000ddc3)
                                </td>
                                <td>
                                    <span id="feed-status-0000ddc3">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000ddc3" class="status-action" data-action="Activate" data-campaign="0000ddc3"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000ddc3" onclick="setFeedOptions('0000ddc3', 'SMT-R (0000ddc3) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000ddc3">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SUBT- J (0000ddc4)
                                </td>
                                <td>
                                    <span id="feed-status-0000ddc4">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000ddc4" class="status-action" data-action="Activate" data-campaign="0000ddc4"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000ddc4" onclick="setFeedOptions('0000ddc4', 'SUBT- J (0000ddc4) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000ddc4">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SMT- J (0000ddc5)
                                </td>
                                <td>
                                    <span id="feed-status-0000ddc5">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000ddc5" class="status-action" data-action="Activate" data-campaign="0000ddc5"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000ddc5" onclick="setFeedOptions('0000ddc5', 'SMT- J (0000ddc5) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000ddc5">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SHARET- J (0000ddcd)
                                </td>
                                <td>
                                    <span id="feed-status-0000ddcd">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000ddcd" class="status-action" data-action="Activate" data-campaign="0000ddcd"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000ddcd" onclick="setFeedOptions('0000ddcd', 'SHARET- J (0000ddcd) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000ddcd">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SHARET- T (0000ddce)
                                </td>
                                <td>
                                    <span id="feed-status-0000ddce">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000ddce" class="status-action" data-action="Activate" data-campaign="0000ddce"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000ddce" onclick="setFeedOptions('0000ddce', 'SHARET- T (0000ddce) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000ddce">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SUBT- T (0000ddcf)
                                </td>
                                <td>
                                    <span id="feed-status-0000ddcf">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000ddcf" class="status-action" data-action="Activate" data-campaign="0000ddcf"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000ddcf" onclick="setFeedOptions('0000ddcf', 'SUBT- T (0000ddcf) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000ddcf">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    SMT-T (0000ddd0)
                                </td>
                                <td>
                                    <span id="feed-status-0000ddd0">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000ddd0" class="status-action" data-action="Activate" data-campaign="0000ddd0"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000ddd0" onclick="setFeedOptions('0000ddd0', 'SMT-T (0000ddd0) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000ddd0">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Place Share - R (0000df52)
                                </td>
                                <td>
                                    <span id="feed-status-0000df52">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000df52" class="status-action" data-action="Activate" data-campaign="0000df52"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000df52" onclick="setFeedOptions('0000df52', 'Place Share - R (0000df52) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000df52">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Place Sublet- R (0000df53)
                                </td>
                                <td>
                                    <span id="feed-status-0000df53">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000df53" class="status-action" data-action="Activate" data-campaign="0000df53"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000df53" onclick="setFeedOptions('0000df53', 'Place Sublet- R (0000df53) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000df53">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Place Rent- R (0000df54)
                                </td>
                                <td>
                                    <span id="feed-status-0000df54">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000df54" class="status-action" data-action="Activate" data-campaign="0000df54"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000df54" onclick="setFeedOptions('0000df54', 'Place Rent- R (0000df54) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000df54">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Place Share - T (0000df55)
                                </td>
                                <td>
                                    <span id="feed-status-0000df55">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000df55" class="status-action" data-action="Activate" data-campaign="0000df55"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000df55" onclick="setFeedOptions('0000df55', 'Place Share - T (0000df55) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000df55">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Place Sublet- T (0000df56)
                                </td>
                                <td>
                                    <span id="feed-status-0000df56">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000df56" class="status-action" data-action="Activate" data-campaign="0000df56"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000df56" onclick="setFeedOptions('0000df56', 'Place Sublet- T (0000df56) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000df56">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Place Rent- T (0000df57)
                                </td>
                                <td>
                                    <span id="feed-status-0000df57">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000df57" class="status-action" data-action="Activate" data-campaign="0000df57"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000df57" onclick="setFeedOptions('0000df57', 'Place Rent- T (0000df57) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000df57">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Place Share - B (0000df58)
                                </td>
                                <td>
                                    <span id="feed-status-0000df58">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000df58" class="status-action" data-action="Activate" data-campaign="0000df58"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000df58" onclick="setFeedOptions('0000df58', 'Place Share - B (0000df58) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000df58">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Place Sublet- B (0000df59)
                                </td>
                                <td>
                                    <span id="feed-status-0000df59">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000df59" class="status-action" data-action="Activate" data-campaign="0000df59"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000df59" onclick="setFeedOptions('0000df59', 'Place Sublet- B (0000df59) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000df59">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Place Rent- B (0000df5c)
                                </td>
                                <td>
                                    <span id="feed-status-0000df5c">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000df5c" class="status-action" data-action="Activate" data-campaign="0000df5c"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000df5c" onclick="setFeedOptions('0000df5c', 'Place Rent- B (0000df5c) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000df5c">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Log Sub- J (0000e32b)
                                </td>
                                <td>
                                    <span id="feed-status-0000e32b">Inactive</span>
                                </td>
                                <td>
                                    <a id="status-action-0000e32b" class="status-action" data-action="Activate" data-campaign="0000e32b"> Activate</a>
                                    <div class="floatRight">
                                        <a style="display: none" id="feed-edit-0000e32b" onclick="setFeedOptions('0000e32b', 'Log Sub- J (0000e32b) ', 'Update')"><i class="fa  fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="clearfix"></div>

                                </td>
                                <td>
                                    <div id="download-status-0000e32b">

                                    </div>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="tabs-2" style="display: none" aria-labelledby="ui-id-14" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="false" aria-hidden="true">
                    <div>
                        <div class="selectric-wrapper">
                            <div class="selectric-hide-select">
                                <select id="campaigns_list" style="display: inline-block;" tabindex="0">
                                    <option value="">Step 1: Select campaign</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=00008db4">SM-M (00008db4)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=00008ece">SM-T (00008ece)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=00008fca">LOG SM-A (00008fca)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=00009172">SM-J (00009172)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=00009272">SM-B (00009272)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=00009466">SM-R (00009466)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=00009563">SM-S (00009563)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=00009847">SUB- T (00009847)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000b46e">SUB- R (0000b46e)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000be1f">Do not use this champing SUB- J (0000be1f)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000d59c">SHARE- R (0000d59c)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000d59d">SHARE- T (0000d59d)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000d59e">SHARE- J (0000d59e)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000d59f">SHARE- A (0000d59f)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000d5a0">SHARE- B (0000d5a0)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000d5a1">SUB- S (0000d5a1)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000d5a2">SHARE- M (0000d5a2)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000d7b4">SUB- M (0000d7b4)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000d7d1">SUB- B (0000d7d1)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000d7d2">LOG SUB- A (0000d7d2)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000d7d3">SHARE- S (0000d7d3)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000daaf">LOG RENT R (0000daaf)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000dab1">LOG RENT T (0000dab1)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000dab2">LOG SUB T (0000dab2)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000dbff">LOG SUB R (0000dbff)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000dc31">SHARE- AM (0000dc31)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000dc32">SM- AM (0000dc32)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000dca0">REP - R (0000dca0)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000dca1">REP - J (0000dca1)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000dca2">REP - T (0000dca2)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000dca3">REP - A (0000dca3)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000dca4">REP - B (0000dca4)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000ddc1">SHARET -R (0000ddc1)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000ddc2">SUBT- R (0000ddc2)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000ddc3">SMT-R (0000ddc3)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000ddc4">SUBT- J (0000ddc4)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000ddc5">SMT- J (0000ddc5)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000ddcd">SHARET- J (0000ddcd)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000ddce">SHARET- T (0000ddce)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000ddcf">SUBT- T (0000ddcf)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000ddd0">SMT-T (0000ddd0)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000df52">Place Share - R (0000df52)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000df53">Place Sublet- R (0000df53)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000df54">Place Rent- R (0000df54)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000df55">Place Share - T (0000df55)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000df56">Place Sublet- T (0000df56)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000df57">Place Rent- T (0000df57)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000df58">Place Share - B (0000df58)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000df59">Place Sublet- B (0000df59)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000df5c">Place Rent- B (0000df5c)</option>
                                    <option value="https://bnc.lt/diNm/gXarFVq2IB?referral=0000e32b">Log Sub- J (0000e32b)</option>
                                </select>
                            </div>
                            <div class="selectric">
                                <p class="label">Step 1: Select campaign</p><b class="button">▾</b></div>
                            <div class="selectric-items" tabindex="-1">
                                <div class="selectric-scroll">
                                    <ul>
                                        <li data-index="0" class="selected">Step 1: Select campaign</li>
                                        <li data-index="1" class="">SM-M (00008db4)</li>
                                        <li data-index="2" class="">SM-T (00008ece)</li>
                                        <li data-index="3" class="">LOG SM-A (00008fca)</li>
                                        <li data-index="4" class="">SM-J (00009172)</li>
                                        <li data-index="5" class="">SM-B (00009272)</li>
                                        <li data-index="6" class="">SM-R (00009466)</li>
                                        <li data-index="7" class="">SM-S (00009563)</li>
                                        <li data-index="8" class="">SUB- T (00009847)</li>
                                        <li data-index="9" class="">SUB- R (0000b46e)</li>
                                        <li data-index="10" class="">Do not use this champing SUB- J (0000be1f)</li>
                                        <li data-index="11" class="">SHARE- R (0000d59c)</li>
                                        <li data-index="12" class="">SHARE- T (0000d59d)</li>
                                        <li data-index="13" class="">SHARE- J (0000d59e)</li>
                                        <li data-index="14" class="">SHARE- A (0000d59f)</li>
                                        <li data-index="15" class="">SHARE- B (0000d5a0)</li>
                                        <li data-index="16" class="">SUB- S (0000d5a1)</li>
                                        <li data-index="17" class="">SHARE- M (0000d5a2)</li>
                                        <li data-index="18" class="">SUB- M (0000d7b4)</li>
                                        <li data-index="19" class="">SUB- B (0000d7d1)</li>
                                        <li data-index="20" class="">LOG SUB- A (0000d7d2)</li>
                                        <li data-index="21" class="">SHARE- S (0000d7d3)</li>
                                        <li data-index="22" class="">LOG RENT R (0000daaf)</li>
                                        <li data-index="23" class="">LOG RENT T (0000dab1)</li>
                                        <li data-index="24" class="">LOG SUB T (0000dab2)</li>
                                        <li data-index="25" class="">LOG SUB R (0000dbff)</li>
                                        <li data-index="26" class="">SHARE- AM (0000dc31)</li>
                                        <li data-index="27" class="">SM- AM (0000dc32)</li>
                                        <li data-index="28" class="">REP - R (0000dca0)</li>
                                        <li data-index="29" class="">REP - J (0000dca1)</li>
                                        <li data-index="30" class="">REP - T (0000dca2)</li>
                                        <li data-index="31" class="">REP - A (0000dca3)</li>
                                        <li data-index="32" class="">REP - B (0000dca4)</li>
                                        <li data-index="33" class="">SHARET -R (0000ddc1)</li>
                                        <li data-index="34" class="">SUBT- R (0000ddc2)</li>
                                        <li data-index="35" class="">SMT-R (0000ddc3)</li>
                                        <li data-index="36" class="">SUBT- J (0000ddc4)</li>
                                        <li data-index="37" class="">SMT- J (0000ddc5)</li>
                                        <li data-index="38" class="">SHARET- J (0000ddcd)</li>
                                        <li data-index="39" class="">SHARET- T (0000ddce)</li>
                                        <li data-index="40" class="">SUBT- T (0000ddcf)</li>
                                        <li data-index="41" class="">SMT-T (0000ddd0)</li>
                                        <li data-index="42" class="">Place Share - R (0000df52)</li>
                                        <li data-index="43" class="">Place Sublet- R (0000df53)</li>
                                        <li data-index="44" class="">Place Rent- R (0000df54)</li>
                                        <li data-index="45" class="">Place Share - T (0000df55)</li>
                                        <li data-index="46" class="">Place Sublet- T (0000df56)</li>
                                        <li data-index="47" class="">Place Rent- T (0000df57)</li>
                                        <li data-index="48" class="">Place Share - B (0000df58)</li>
                                        <li data-index="49" class="">Place Sublet- B (0000df59)</li>
                                        <li data-index="50" class="">Place Rent- B (0000df5c)</li>
                                        <li data-index="51" class="last">Log Sub- J (0000e32b)</li>
                                    </ul>
                                </div>
                            </div>
                            <input class="selectric-input" tabindex="0">
                        </div>
                    </div>
                    <div style="padding-top: 1em;">
                        <div class="selectric-wrapper   selectric-disabled">
                            <div class="selectric-hide-select">
                                <select id="promo_types" style="display: inline-block;" disabled="disabled">
                                    <option value="">Step 2: Select promo type</option>
                                    <option value="#promo-banners">Banners</option>
                                    <option value="#promo-links">Links</option>
                                </select>
                            </div>
                            <div class="selectric">
                                <p class="label">Step 2: Select promo type</p><b class="button">▾</b></div>
                            <div class="selectric-items" tabindex="-1">
                                <div class="selectric-scroll">
                                    <ul>
                                        <li data-index="0" class="selected">Step 2: Select promo type</li>
                                        <li data-index="1" class="">Banners</li>
                                        <li data-index="2" class="last">Links</li>
                                    </ul>
                                </div>
                            </div>
                            <input class="selectric-input" disabled="">
                        </div>
                    </div>
                    <div id="promo-banners"></div>
                    <div id="promo-links"></div>
                </div>
                <div id="tabs-3" style="display: none" aria-labelledby="ui-id-15" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="false" aria-hidden="true">
                    <h1>Why writing a blog is good for your business.</h1>
                    <p>Personal <a href="http://www.womenonbusiness.com/5-reasons-blogging-important-business-2015/" target="_blank">blogs became a hot trend</a> in today’s online marketing strategy, as an effective and discreet way to build awareness of a brand or a product and increase online traffic resulting in more leads and sales. As recent research conducted by eMarketer showed about 140 million people in the US are reading blogs in 2015 and, according to the same research, about 31% of consumers admitted that their decision to purchase this or that product was influenced by reading an authoritative blog. With this said, we strongly recommend you to <a href="http://firstsiteguide.com/blogging-intro/" target="_blank">start writing a blog</a> as part of your promotional campaign.</p>

                    <h2>Why start writing a blog?</h2>
                    <p>Creating and promoting your blog provides you with valuable sales skills that can be both applied in all aspects of your life and added to your professional resume.</p>
                    <p>Writing a blog can become a perfect opportunity for creating a side income as there are many ways to monetize your blog online.</p>
                    <p>It is fun! Creating a blog can easily be compared to creating your own unique world, the ideal one, where everything is exactly the way you want to be, you can truly open up to your readers and share your thoughts and ideas with them. </p>
                    <p>It inspires your creativity, there are no rules and no boundaries when it comes to writing, which makes it a perfect opportunity to get to know yourself better, expand your consciousness and improve your personality.</p>

                    <table style="width: 100%; margin-bottom: 1em;">
                        <tbody>
                        <tr>
                            <td style="width: 25%;">
                                <a href="http://www.usroomies.com/" target="_blank"><img src="https://s.roomster.com/Content/images/promo/usroomies.com.jpg?20190108" style="width: 100%;"></a>
                            </td>
                            <td style="width: 25%;">
                                <a href="http://www.room-roommate.org/" target="_blank"><img src="https://s.roomster.com/Content/images/promo/room-roommate.org.jpg?20190108" style="width: 100%;"></a>
                            </td>
                            <td style="width: 25%;">
                                <a href="http://www.room-roommate.biz/" target="_blank"><img src="https://s.roomster.com/Content/images/promo/room-roommate.biz.jpg?20190108" style="width: 100%;"></a>
                            </td>
                            <td style="width: 25%;">
                                <a href="http://www.room-roommate.com/" target="_blank"><img src="https://s.roomster.com/Content/images/promo/room-roommate.com.jpg?20190108" style="width: 100%;"></a>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <h2> How to start writing a blog?</h2>
                    <h3>Creating a concept. </h3>
                    <p>What do you want to blog about? Even though you are going to promote roomster.com on your pages, your blog doesn’t have to be about roommates. Maybe you want to write about college life, or traveling or building a career in a large corporation or home improvement ideas? just find your passion and share it with the world!! Being genuinely passionate about something is the shortest and the most reliable way to gain followers, fans and loyal readers. </p>

                    <h3>Choosing the platform.</h3>
                    <p>Today there are many affordable beginner friendly <a href="http://tech.co/10-best-website-builder-platforms-for-beginners-2015-02" target="_blank">website building platforms</a> out there such as <a href="http://www.squarespace.com/" target="_blank">Squarespace.com</a>, <a href="http://www.wix.com/" target="_blank">Wix.com</a> and <a href="https://wordpress.com/" target="_blank">Wordpress.com</a> etc. These platforms are extremely user friendly have a lot very helpful features such as a built in SEO tool or on site stats checker as well as commerce tools. All of them offer step by step guidance instructions on how to start your website.</p>

                    <h3>Setting up a domain name. </h3>
                    <p><a href="http://www.searchenginejournal.com/how-your-domain-name-will-impact-seo-social-media-marketing/" target="_blank">Choosing your domain</a> is a very important factor of your blogs success, so you want it to be original, easy to remember and relevant to your website concept.</p>

                    <h3>Designing and personalizing your website. </h3>
                    <p>Let the fun begin! Creating a website is like creating your own personal universe, you can do anything you want and your web platform allows.</p>

                    <h3>Adding content. </h3>
                    <p>The key to success here is being <a href="http://blog.hubspot.com/blog/tabid/6307/bid/33906/6-Tips-for-Creating-Content-That-Spreads-Naturally.aspx" target="_blank">authentic and original</a>. A perfect article showing up in search results is both concise and informative, written in a simple and dynamic manner. </p>

                    <h3>Promoting your website online. </h3>
                    <p>The key component of your blogs success is online exposure. It doesn’t matter how stunning is your sites design, how amazing and interesting the content is if nobody ever reads it! </p>

                    <p>Just as we said before, an interesting and engaging blog post is as (if not more) effective, as a masterfully written promotional email, so we strongly encourage you to try blogging as one more tool in your inventory. We would also like to thank you for the amazing job you do promoting roomster.com online and bringing high quality leads to our website everyday. </p>
                    <p>Let’s take a look at the blogs that do well on the web. While being unique and interesting, they all comply with a few basic principles that ensure their exposure to the search engines (otherwise, what is the point of writing a blog?).</p>

                    <table style="width: 50%; margin: 0 auto 1em;">
                        <tbody>
                        <tr>
                            <td style="width: 50%;">
                                <a href="http://www.hellofashionblog.com/" target="_blank"><img src="https://s.roomster.com/Content/images/promo/hellofashionblog.com.jpg?20190108" style="width: 100%;"></a>
                            </td>
                            <td style="width: 50%;">
                                <a href="http://www.immovingtola.com/" target="_blank"><img src="https://s.roomster.com/Content/images/promo/immovingtola.com.jpg?20190108" style="width: 100%;"></a>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <h3>So, which blogs show up in search results?</h3>
                    <p>Blogs that are being <a href="http://www.entrepreneur.com/article/232478" target="_blank">updated regularly</a> (adding new content such as articles, pictures, media files or notes and comments will certainly be noticed by google).</p>
                    <p>Blogs that are unique (nope, copying and pasting the content from other websites is not going to work, google is working really hard to make sure that websites that do this never see the search result pages. Even re-writing other people’s content in your own words is not going to work.) Having a good content is everything for your blog's success, so you want to make sure your articles are unique, informative, not too short and backed up by expert opinion and facts from reputable websites. </p>
                    <p>Blogs that have proper <a href="http://www.wordstream.com/blog/ws/2015/04/30/seo-basics" target="_blank">SEO</a> (make sure you do a proper <a href="https://moz.com/beginners-guide-to-seo/keyword-research" target="_blank">keyword research</a> when setting up your website and then incorporate those keywords in your domain name, your site title and description, the main headings on the website and all the pictures descriptions).</p>
                    <p>Blogs that have (quality) <a href="http://searchengineland.com/whats-state-link-building-seo-2015-beyond-215578" target="_blank">links</a>. Let us stress out the importance of linking to the high quality (spam free) websites with content relevant to the subject you are writing about, as checking all your links is another way google bots analyze your content quality.</p>
                    <p>Do not underestimate the importance of pictures on your pages they can make or break your website. Good quality pictures catch the eye of a visitor while bad pictures undermine your site’s reputation. It doesn’t matter how good is your content, blurry pictures just make it look bad and drive the traffic away from your site. Don’t forget to add a proper description (containing your target keywords) to each picture on your site. </p>
                    <p>And the last, but certainly not the least is to spread the word about your amazing website among as many people as possible. Utilizing your social network accounts like Facebook, Twitter, Tumblr, Instagram etc. will also help. As well as registering and adding your links to various social bookmarking websites like reddit, stumble upon, delicious and other sites like this. </p>
                </div>

            </div>

            <div id="promo-banners-template" style="display: none;">
                <table class="display box" style="width: 100%;">
                    <thead>
                    <tr>
                        <th style="width: 180px;">Banner</th>
                        <th>Code</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">
                                    <img src="https://s.roomster.com/Content/images/promo/people-with-places-120x600.jpg?20190108" width="28" height="140" style="border: none;">
                                </a>
                            </div>
                            <div>120 x 600</div>
                            <div>
                                <a onclick="showFullSize('https://s.roomster.com/Content/images/promo/people-with-places-120x600.jpg?20190108', 120, 600)">View full-size banner</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;&lt;img src="https://s.roomster.com/Content/images/promo/people-with-places-120x600.jpg?20190108" width="120" height="600" style="border: none;"/&gt;&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">
                                    <img src="https://s.roomster.com/Content/images/promo/people-with-places-160x600.jpg?20190108" width="37" height="140" style="border: none;">
                                </a>
                            </div>
                            <div>160 x 600</div>
                            <div>
                                <a onclick="showFullSize('https://s.roomster.com/Content/images/promo/people-with-places-160x600.jpg?20190108', 160, 600)">View full-size banner</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;&lt;img src="https://s.roomster.com/Content/images/promo/people-with-places-160x600.jpg?20190108" width="160" height="600" style="border: none;"/&gt;&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">
                                    <img src="https://s.roomster.com/Content/images/promo/people-with-places-250x250.jpg?20190108" width="100" height="100" style="border: none;">
                                </a>
                            </div>
                            <div>250 x 250</div>
                            <div>
                                <a onclick="showFullSize('https://s.roomster.com/Content/images/promo/people-with-places-250x250.jpg?20190108', 250, 250)">View full-size banner</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;&lt;img src="https://s.roomster.com/Content/images/promo/people-with-places-250x250.jpg?20190108" width="250" height="250" style="border: none;"/&gt;&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">
                                    <img src="https://s.roomster.com/Content/images/promo/people-with-places-300x250.jpg?20190108" width="120" height="100" style="border: none;">
                                </a>
                            </div>
                            <div>300 x 250</div>
                            <div>
                                <a onclick="showFullSize('https://s.roomster.com/Content/images/promo/people-with-places-300x250.jpg?20190108', 300, 250)">View full-size banner</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;&lt;img src="https://s.roomster.com/Content/images/promo/people-with-places-300x250.jpg?20190108" width="300" height="250" style="border: none;"/&gt;&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">
                                    <img src="https://s.roomster.com/Content/images/promo/people-with-places-368x280.jpg?20190108" width="147" height="112" style="border: none;">
                                </a>
                            </div>
                            <div>368 x 280</div>
                            <div>
                                <a onclick="showFullSize('https://s.roomster.com/Content/images/promo/people-with-places-368x280.jpg?20190108', 368, 280)">View full-size banner</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;&lt;img src="https://s.roomster.com/Content/images/promo/people-with-places-368x280.jpg?20190108" width="368" height="280" style="border: none;"/&gt;&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">
                                    <img src="https://s.roomster.com/Content/images/promo/people-with-places-468x60.jpg?20190108" width="180" height="23" style="border: none;">
                                </a>
                            </div>
                            <div>468 x 60</div>
                            <div>
                                <a onclick="showFullSize('https://s.roomster.com/Content/images/promo/people-with-places-468x60.jpg?20190108', 468, 60)">View full-size banner</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;&lt;img src="https://s.roomster.com/Content/images/promo/people-with-places-468x60.jpg?20190108" width="468" height="60" style="border: none;"/&gt;&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">
                                    <img src="https://s.roomster.com/Content/images/promo/people-with-places-728x90.jpg?20190108" width="180" height="22" style="border: none;">
                                </a>
                            </div>
                            <div>728 x 90</div>
                            <div>
                                <a onclick="showFullSize('https://s.roomster.com/Content/images/promo/people-with-places-728x90.jpg?20190108', 728, 90)">View full-size banner</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;&lt;img src="https://s.roomster.com/Content/images/promo/people-with-places-728x90.jpg?20190108" width="728" height="90" style="border: none;"/&gt;&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">
                                    <img src="https://s.roomster.com/Content/images/promo/rooms-and-shares-120x240.jpg?20190108" width="48" height="96" style="border: none;">
                                </a>
                            </div>
                            <div>120 x 240</div>
                            <div>
                                <a onclick="showFullSize('https://s.roomster.com/Content/images/promo/rooms-and-shares-120x240.jpg?20190108', 120, 240)">View full-size banner</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;&lt;img src="https://s.roomster.com/Content/images/promo/rooms-and-shares-120x240.jpg?20190108" width="120" height="240" style="border: none;"/&gt;&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">
                                    <img src="https://s.roomster.com/Content/images/promo/rooms-and-shares-125x125.jpg?20190108" width="50" height="50" style="border: none;">
                                </a>
                            </div>
                            <div>125 x 125</div>
                            <div>
                                <a onclick="showFullSize('https://s.roomster.com/Content/images/promo/rooms-and-shares-125x125.jpg?20190108', 125, 125)">View full-size banner</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;&lt;img src="https://s.roomster.com/Content/images/promo/rooms-and-shares-125x125.jpg?20190108" width="125" height="125" style="border: none;"/&gt;&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">
                                    <img src="https://s.roomster.com/Content/images/promo/rooms-and-shares-200x200.jpg?20190108" width="80" height="80" style="border: none;">
                                </a>
                            </div>
                            <div>200 x 200</div>
                            <div>
                                <a onclick="showFullSize('https://s.roomster.com/Content/images/promo/rooms-and-shares-200x200.jpg?20190108', 200, 200)">View full-size banner</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;&lt;img src="https://s.roomster.com/Content/images/promo/rooms-and-shares-200x200.jpg?20190108" width="200" height="200" style="border: none;"/&gt;&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">
                                    <img src="https://s.roomster.com/Content/images/promo/rooms-and-shares-234x60.jpg?20190108" width="93" height="24" style="border: none;">
                                </a>
                            </div>
                            <div>234 x 60</div>
                            <div>
                                <a onclick="showFullSize('https://s.roomster.com/Content/images/promo/rooms-and-shares-234x60.jpg?20190108', 234, 60)">View full-size banner</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;&lt;img src="https://s.roomster.com/Content/images/promo/rooms-and-shares-234x60.jpg?20190108" width="234" height="60" style="border: none;"/&gt;&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">
                                    <img src="https://s.roomster.com/Content/images/promo/rooms-and-shares-300x50.jpg?20190108" width="120" height="20" style="border: none;">
                                </a>
                            </div>
                            <div>300 x 50</div>
                            <div>
                                <a onclick="showFullSize('https://s.roomster.com/Content/images/promo/rooms-and-shares-300x50.jpg?20190108', 300, 50)">View full-size banner</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;&lt;img src="https://s.roomster.com/Content/images/promo/rooms-and-shares-300x50.jpg?20190108" width="300" height="50" style="border: none;"/&gt;&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <div id="promo-links-template" style="display: none;">
                <table class="display box" style="width: 100%;">
                    <thead>
                    <tr>
                        <th style="width: 180px;">Link</th>
                        <th>Code</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">Need a roommate?</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;Need a roommate?&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">Need an Apartment, Roommate or Sublet?</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;Need an Apartment, Roommate or Sublet?&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">Roomster.com for Apartments, Roommates &amp; Sublets</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;Roomster.com for Apartments, Roommates &amp; Sublets&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">1000's of Roommates &amp; Apartments</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;1000's of Roommates &amp; Apartments&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <a href="{url}" target="_blank">Roomster.com</a>
                            </div>
                        </td>
                        <td>
                            <textarea>&lt;a href="{url}"&gt;Roomster.com&lt;/a&gt;</textarea>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <div id="full-size" style="display: none;">
            <p class="basePopupTitle">
                <span style="font-size: .7em;">{width} x {height}</span></p>
            <img src="{src}" width="{width}" height="{height}">
        </div>

        <div id="feed-options" style="display: none;">

            <input type="hidden" id="frm-action">

            <p class="basePopupTitle">Feed settings</p>
            <div>
                <div style="font-size: larger;">

                    <div style="font-weight: bold; padding: .5em 0;">Listing types:</div>

                    <div class="listing-types">

                        <label style="padding: 1em 1em 1em 0;">
                            <input type="checkbox" data-val="1" name="serviceTypes">&nbsp;&nbsp;Roommate</label>

                        <label style="padding: 1em 1em 1em 0;">
                            <input type="checkbox" data-val="2" name="serviceTypes">&nbsp;&nbsp;Room</label>

                        <label style="padding: 1em 1em 1em 0;">
                            <input type="checkbox" data-val="4" name="serviceTypes">&nbsp;&nbsp;Tenant</label>

                        <label style="padding: 1em 1em 1em 0;">
                            <input type="checkbox" data-val="5" name="serviceTypes">&nbsp;&nbsp;Apartment</label>

                    </div>

                    <div style="font-weight: bold; margin-top: 1em;padding: .5em 0;">Countries:</div>
                    <input type="text" id="countries_list">

                </div>

                <div class="floatRight" style="">
                    <input type="button" class="inputsubmit" style="width: inherit;" value="Submit" id="submit-settings">
                </div>
                <div class="floatRight" style="">
                    <span style="display: block; padding: 1.2em; font-size: 1.5em;"><a onclick="$('#feed-options').dialog('close');">Cancel</a></span>
                </div>

                <div class="clear"></div>
            </div>
        </div>

    </div>
@endsection