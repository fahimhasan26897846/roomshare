@extends('affiliate.inc.master')
@section('content')
    <div id="container" class="affiliate-container">

        <div id="registrationWrapper">
            <div class="registrationWrapperHeaderHolder">
                <div class="registrationWrapperHeader">
                    Notifications
                </div>
                <!-- registrationWrapperHeader end -->
            </div>
            <!-- registrationWrapperHeaderHolder end -->
            <div class="guideLine ">Roomster notification settings allow you to control what notifications get delivered to <strong>Lisaolinia@gmail.com</strong></div>
            <form action="/Affiliate/Notifications" method="post">
                <div class="form">
                    <div class="notification-checkboxes clear coltitle">
                        <div class="floatLeft">Notifications</div>
                        <div class="floatRight">Email</div>
                    </div>
                    <div class="divider"></div>
                    <div>
                        <div class="notification-checkboxes clear">
                            <input id="Helpdesk" name="NotificationTypes" type="checkbox" value="Helpdesk">
                            <label for="Helpdesk">Helpdesk activity updates</label>
                        </div>
                        <div class="divider"></div>
                        <div class="notification-checkboxes clear">
                            <input id="Account" name="NotificationTypes" type="checkbox" value="Account">
                            <label for="Account">General account updates</label>
                        </div>
                        <div class="divider"></div>

                    </div>
                </div>
                <div class="buttons">
                    <input type="submit" value="Save" name="save" id="save" class="inputsubmit">
                </div>
            </form>
        </div>

    </div>
@endsection