@extends('affiliate.inc.master')
@section('content')
    <div id="container" class="affiliate-container">

        <div class="content">

            <div class="clearfix stretch" style="margin-bottom: 1em;">
                <div class="floatLeft">
                    <a href="/affiliate/campaign" class="button-add">CAMPAIGN</a>
                </div>
                <div class="selectric-wrapper floatRight">
                    <div class="selectric" onclick="$('#dateRangeContainer').toggle();">
                        <p class="label bold"><i class="fa fa-calendar-o"></i>Aug 05, 2019&nbsp;–&nbsp;Aug 05, 2019</p><b class="button">▾</b>
                    </div>
                </div>
                <div class="clear"></div>

                <div id="dateRangeContainer" class="floatRight" style="display: none;">
                    <form>

                        <div class="clearfix">
                            <div class="floatLeft" style="padding-right: 2em;">
                                <div class="calendar-title">DATE RANGE</div>
                                <div class="calendar-input">
                                    <input id="dateFrom" type="text" class="textInput" value="2019-08-05" maxlength="10">
                                </div>
                                <div class="ndash">–</div>
                                <div class="calendar-input">
                                    <input id="dateTo" type="text" class="calendar-input textInput" value="2019-08-05" maxlength="10">
                                </div>
                                <div class="calendar-buttons">
                                    <input type="button" class="inputsubmit" value="Apply" id="apllyDateRange">
                                    <input type="button" class="inputsubmit button-red" value="Cancel" id="cancelDateRange">
                                </div>
                            </div>
                            <div class="floatRight">
                                <div class="calendar-title">QUICK DATES</div>

                                <div class="clearfix">
                                    <div class="floatLeft">
                                        <div class="calendar-quick-date">
                                            <a href="/affiliate/campaigns/2019-08-05/2019-08-05">Today: August 05</a>
                                        </div>
                                        <div class="calendar-quick-date">
                                            <a href="/affiliate/campaigns/2019-08-04/2019-08-04">Yesterday: August 04</a>
                                        </div>
                                        <div class="calendar-quick-date">
                                            <a href="/affiliate/campaigns/2019-07-29/2019-08-04">Last 7 days</a>
                                        </div>
                                        <div class="calendar-quick-date">
                                            <a href="/affiliate/campaigns/2019-07-07/2019-08-04">Last 30 days</a>
                                        </div>
                                    </div>
                                    <div class="floatRight">
                                        <div class="calendar-quick-date">
                                            <a href="/affiliate/campaigns/2019-08-01/2019-08-05">This month: August</a>
                                        </div>
                                        <div class="calendar-quick-date">
                                            <a href="/affiliate/campaigns/2019-07-01/2019-07-31">Last month: July</a>
                                        </div>
                                        <div class="calendar-quick-date">
                                            <a href="/affiliate/campaigns/2017-04-22/2019-08-05">All time</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            <div id="affiliate-campaigns_wrapper" class="dataTables_wrapper">
                <div class="dataTables_length" id="affiliate-campaigns_length">
                    <label>Show
                        <select name="affiliate-campaigns_length" aria-controls="affiliate-campaigns" class="">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="-1">All</option>
                        </select> entries</label>
                </div>
                <div id="affiliate-campaigns_filter" class="dataTables_filter">
                    <label>Search:
                        <input type="search" class="" placeholder="" aria-controls="affiliate-campaigns">
                    </label>
                </div>
                <table id="affiliate-campaigns" class="display dataTable" style="" role="grid" aria-describedby="affiliate-campaigns_info">
                    <thead>
                    <tr role="row">
                        <th class="sorting" tabindex="0" aria-controls="affiliate-campaigns" rowspan="1" colspan="1" aria-label="Campaign: activate to sort column ascending" style="width: 0px;">Campaign</th>
                        <th class="sorting" tabindex="0" aria-controls="affiliate-campaigns" rowspan="1" colspan="1" aria-label="Tracking Id: activate to sort column ascending" style="width: 0px;">Tracking Id</th>
                        <th class="sorting_desc" tabindex="0" aria-controls="affiliate-campaigns" rowspan="1" colspan="1" aria-sort="descending" aria-label="Leads: activate to sort column ascending" style="width: 0px;">Leads</th>
                        <th class="sorting" tabindex="0" aria-controls="affiliate-campaigns" rowspan="1" colspan="1" aria-label="Earnings: activate to sort column ascending" style="width: 0px;">Earnings</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th class="align-right" colspan="2" rowspan="1">Total</th>
                        <th class="align-right" rowspan="1" colspan="1">9</th>
                        <th class="align-right" rowspan="1" colspan="1">$7.20</th>
                    </tr>
                    </tfoot>
                    <tbody>

                    <tr role="row" class="odd">
                        <td class="edit-link">
                            <a href="/affiliate/campaign/54685">SHARE- T<i class="fa fa-pencil"></i></a>
                        </td>

                        <td>0000d59d</td>
                        <td class="align-right sorting_1">4</td>
                        <td class="align-right">$3.20</td>
                    </tr>
                    <tr role="row" class="even">
                        <td class="edit-link">
                            <a href="/affiliate/campaign/54686">SHARE- J<i class="fa fa-pencil"></i></a>
                        </td>

                        <td>0000d59e</td>
                        <td class="align-right sorting_1">3</td>
                        <td class="align-right">$2.40</td>
                    </tr>
                    <tr role="row" class="odd">
                        <td class="edit-link">
                            <a href="/affiliate/campaign/37234">SM-J<i class="fa fa-pencil"></i></a>
                        </td>

                        <td>00009172</td>
                        <td class="align-right sorting_1">2</td>
                        <td class="align-right">$1.60</td>
                    </tr>
                    <tr role="row" class="even">
                        <td class="edit-link">
                            <a href="/affiliate/campaign/36276">SM-M<i class="fa fa-pencil"></i></a>
                        </td>

                        <td>00008db4</td>
                        <td class="align-right sorting_1"></td>
                        <td class="align-right">$0.00</td>
                    </tr>
                    <tr role="row" class="odd">
                        <td class="edit-link">
                            <a href="/affiliate/campaign/36558">SM-T <i class="fa fa-pencil"></i></a>
                        </td>

                        <td>00008ece</td>
                        <td class="align-right sorting_1"></td>
                        <td class="align-right">$0.00</td>
                    </tr>
                    <tr role="row" class="even">
                        <td class="edit-link">
                            <a href="/affiliate/campaign/36810">LOG SM-A<i class="fa fa-pencil"></i></a>
                        </td>

                        <td>00008fca</td>
                        <td class="align-right sorting_1"></td>
                        <td class="align-right">$0.00</td>
                    </tr>
                    <tr role="row" class="odd">
                        <td class="edit-link">
                            <a href="/affiliate/campaign/37490">SM-B<i class="fa fa-pencil"></i></a>
                        </td>

                        <td>00009272</td>
                        <td class="align-right sorting_1"></td>
                        <td class="align-right">$0.00</td>
                    </tr>
                    <tr role="row" class="even">
                        <td class="edit-link">
                            <a href="/affiliate/campaign/37990">SM-R<i class="fa fa-pencil"></i></a>
                        </td>

                        <td>00009466</td>
                        <td class="align-right sorting_1"></td>
                        <td class="align-right">$0.00</td>
                    </tr>
                    <tr role="row" class="odd">
                        <td class="edit-link">
                            <a href="/affiliate/campaign/38243">SM-S<i class="fa fa-pencil"></i></a>
                        </td>

                        <td>00009563</td>
                        <td class="align-right sorting_1"></td>
                        <td class="align-right">$0.00</td>
                    </tr>
                    <tr role="row" class="even">
                        <td class="edit-link">
                            <a href="/affiliate/campaign/38983">SUB- T<i class="fa fa-pencil"></i></a>
                        </td>

                        <td>00009847</td>
                        <td class="align-right sorting_1"></td>
                        <td class="align-right">$0.00</td>
                    </tr>
                    </tbody>
                </table>
                <div class="dataTables_info" id="affiliate-campaigns_info" role="status" aria-live="polite">Showing 1 to 10 of 51 entries</div>

                <div class="dataTables_paginate paging_simple_numbers" id="affiliate-campaigns_paginate"><a class="paginate_button previous disabled" aria-controls="affiliate-campaigns" data-dt-idx="0" tabindex="0" id="affiliate-campaigns_previous">Previous</a><span><a class="paginate_button current" aria-controls="affiliate-campaigns" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="affiliate-campaigns" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="affiliate-campaigns" data-dt-idx="3" tabindex="0">3</a><a class="paginate_button " aria-controls="affiliate-campaigns" data-dt-idx="4" tabindex="0">4</a><a class="paginate_button " aria-controls="affiliate-campaigns" data-dt-idx="5" tabindex="0">5</a><a class="paginate_button " aria-controls="affiliate-campaigns" data-dt-idx="6" tabindex="0">6</a></span><a class="paginate_button next" aria-controls="affiliate-campaigns" data-dt-idx="7" tabindex="0" id="affiliate-campaigns_next">Next</a></div>
            </div>
        </div>

    </div>
@endsection