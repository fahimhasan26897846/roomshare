<h2>Reset Password</h2>
<form action="/reset-password" method="post">
    {{csrf_field()}}
    <input type="hidden" name="reset-code" value="{{ $resetCode }}">
    <label>
        Password:
        <input type="password" name="password">
    </label>
    <label>
        Confirm Password:
        <input type="password" name="confirm-password">
    </label>
    <button type="submit">Change Password</button>
</form>