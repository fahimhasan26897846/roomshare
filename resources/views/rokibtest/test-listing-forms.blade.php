<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Test</title>
</head>
<body>

<div id="app">
    
    <h3>Looking for room</h3>
    <form action="/listings/need-room/new" method="post">
        {{ csrf_field() }}
        <label>Headline: <input type="text" name="headline"></label> <br>
        <label>Location: <input type="text" name="location"></label> <br>
        <label>Currency: <input type="text" name="rental_currency"></label> <br>
        <label>Monthly Rent: <input type="number" name="monthly_rent"></label> <br>
        <label><input type="radio" name="rent_duration" value="short">Short term</label> <br>
        <label><input type="radio" name="rent_duration" value="long">Long term</label> <br>
        <label>Move date: <input type="date" name="move_date"></label> <br>
        <label>Leave date: <input type="date" name="leave_date"></label> <br>
        <label>Description: <textarea name="description" cols="30" rows="10"></textarea></label> <br>
        <label>Clenliness: <input type="text" name="cleanliness"></label> <br>
        <label>Overnight Guests: <input type="text" name="overnight_guests"></label> <br>
        <label>Party habits: <input type="text" name="party_habits"></label> <br>
        <label>Get up: <input type="text" name="get_up"></label> <br>
        <label>Go to bed: <input type="text" name="go_to_bed"></label> <br>
        <label>Food preference: <input type="text" name="food_preference"></label> <br>
        <label>Smoker: <input type="text" name="smoker"></label> <br>
        <label>Work_schedule: <input type="text" name="work_schedule"></label> <br>
        <label>Occupation: <input type="text" name="occupation"></label> <br>
        <label>Prefer Min Age: <input type="text" name="prefer_min_age"></label> <br>
        <label>Prefer Max Age: <input type="text" name="prefer_max_age"></label> <br>
        <label>Prefer Smoker: <input type="text" name="prefer_smoker"></label> <br>
        <label>Prefer Student: <input type="text" name="prefer_student"></label> <br>
        <label><input type="checkbox" name="prefer_cats">Cats</label> <br>
        <label><input type="checkbox" name="prefer_dogs">Dogs</label> <br>
        <label><input type="checkbox" name="prefer_small_pets">Small Pets</label> <br>
        <label><input type="checkbox" name="prefer_birds">Birds</label> <br>
        <label><input type="checkbox" name="prefer_fish">Fish</label> <br>
        <label><input type="checkbox" name="prefer_reptiles">Reptiles</label> <br>
        <button type="submit">Submit</button>
    </form>

</div>

<script src="{{ asset('js/app.js') }}"></script>
<script>
    const app = new Vue({ });
    app.$mount('#app');
</script>
</body>
</html>

<?php
dump(\Cartalyst\Sentinel\Laravel\Facades\Sentinel::check());
?>