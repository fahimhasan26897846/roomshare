<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Chat App</title>

    {{--<link rel="stylesheet" href="{{ asset('css/app.css') }}">--}}
<body>

<div id="app">
    <chat-app :channel="clientChannel"
              :push-message="pushMessage"
              @changechat="changeChatChannel" ></chat-app>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script>
    const app = new Vue({
        data() {
            return {
                pushMessage: null,
                clientChannel: ''
            }
        },
        mounted() {
            console.log(this);
            this.clientChannel = '{{ $clientChannel }}';
            @if($userChannel)
            const channel = window.Echo.private('{{ $userChannel }}');
            channel.listen('new-message', data => {
                this.pushMessage = data;
                console.log(data);
            });
            @endif
        },
        methods: {
            changeChatChannel(chatChannel) {
                this.clientChannel = chatChannel;
            }
        }
    });
    app.$mount('#app');

</script>
</body>
</html>
