<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Test</title>
</head>
<body>

<form action="/logout" method="post">
    {{csrf_field()}}
    <button type="submit">Logout</button>
</form>
<hr>

<h3>All Users</h3>
@foreach($users as $user)
    <a href="/chat/p-c-{{ $user->id }}" type="submit">Chat with {{ $user->first_name }} {{ $user->last_name }}</a>
    <br>
@endforeach
<hr>

<div id="app">
    <notification />
</div>
{{--<h2>Registration</h2>
<form action="/register" method="post">
    {{csrf_field()}}
    <label>
        First Name:
        <input type="text" name="first_name">
    </label>
    <label>
        Last Name:
        <input type="text" name="last_name">
    </label>
    <label>
        Email:
        <input type="email" name="email">
    </label>
    <label>
        Password:
        <input type="password" name="password">
    </label>
    <label>
        Confirm Password:
        <input type="password" name="confirm-password">
    </label>
    <label>
        Date of Birth:
        <input type="date" name="dob">
    </label>
    <label>
        Gender:
        <input type="radio" name="gender" value="male"> Male
        <input type="radio" name="gender" value="female"> Female
        <input type="radio" name="gender" value="other"> Other
    </label>
    <button type="submit">Register</button>
</form>--}}

<hr>

<h2>Login</h2>
<form action="/login" method="post">
    {{csrf_field()}}
    <label>
        Email:
        <input type="email" name="email">
    </label>
    <label>
        Password:
        <input type="password" name="password">
    </label>
    <label>
        <input type="checkbox" name="remember">
        Remember me
    </label>
    <a href="#forgot-password">Forgot your password?</a>
    <button type="submit">Login</button>
</form>

<hr>

{{--<h2>Forgot Password</h2>
<form id="forgot-password" action="/forgot-password" method="post">
    {{csrf_field()}}
    <label>
        Email:
        <input type="email" name="email">
    </label>
    <button type="submit">Send link</button>
</form>--}}

<script src="{{ asset('js/app.js') }}"></script>
<script>
    const app = new Vue({
        mounted() {
            @if($userChannel)
            const channel = window.Echo.private('{{ $userChannel }}');
            channel.listen('new-message', function(data) {
                console.log(data);
            });
            @endif
        }
    });
    app.$mount('#app');
</script>
</body>
</html>

<?php
    dump(\Cartalyst\Sentinel\Laravel\Facades\Sentinel::check());
?>