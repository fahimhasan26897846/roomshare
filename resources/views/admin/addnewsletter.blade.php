@extends('admin.inc.master')
@php
$active = 'newsletter';
@endphp
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Newsletter</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                    <li class="active">Newsletter</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">

                    <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#exampleModalLong"><i class="fa fa-plus"></i>&nbsp; Add Neswletter</button>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Add New Newsletter</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group"><label for="name">Name</label><input id="name" type="text" name="name" class="form-control"></div>
                                    <div class="form-group"><label for="body">Body</label><textarea name="body"
                                                                                                    id="body"
                                                                                                    cols="30" rows="10"
                                                                                                    class="form-control"></textarea></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary btn-submit">Create</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <div class="row mt-2 mb-2">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="loading-id">
                            <thead class="appand">
                            <tr>
                                <th>Name</th>
                                <th class="text-nowrap">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($newsletter as $news)
                                @php
                                    $id = \Illuminate\Support\Facades\Crypt::encrypt($news->id)
                                @endphp
                                <tr id="ids{{$id}}">
                                    <td>{{ $news->name }}</td>
                                    <td class="text-nowrap">
                                        <a href="#" class="delete-newsletter" data-id="{{$id}}" data-token="{{csrf_token()}}"> <i class="fa fa-close text-danger"></i> </a>
                                        <a href="#" class="send-newsletter" data-id="{{$id}}" data-token="{{csrf_token()}}"><i class="fa fa-send text-warning"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
    <script>
        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
        });

        $('.btn-submit').on('click',function (e) {
            e.preventDefault();
            var name = $("input[name=name]").val();
            var body = $('#body').val();
            $.ajax({
                url: '/post-create-newsletter',
                type: 'POST',
                data: {
                    name : name,
                    body : body
                },
                success: function (response) {
                    $.toast({
                        heading: 'Success',
                        text: response.success,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'info',
                        hideAfter: 3500,
                        stack: 6
                    });
                    $('#exampleModalLong').modal('hide');
                    $('#name').val("");
                    $('#body').val("");
                    // $('.appand').append('<tr><td>'+name+'</td><td class="test-nowrap"> Refresh Page</td></tr>');
                    $('#loading-id').load('{{URL::to('/add-newsletter')}}'+' #loading-id');

                },
                error: function (response) {

                    $.toast({
                        heading: 'Error',
                        text: 'Item Null',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500,
                        stack: 6
                    })

                }

            });
        });
    </script>
    <script>
        $('.delete-newsletter').on('click',function (e) {
            e.preventDefault();
            var del= confirm("Do you really want to delete this?");
            if (del){ var id = $(this).data("id");
                var token = $(this).data("token");
                $.ajax(
                    {
                        url: "/delete-newsletter/" + id,
                        type: 'DELETE',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token
                        },
                        success: function (response) {
                            $.toast({
                                heading: 'Success',
                                text: response.success,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'info',
                                hideAfter: 3500,
                                stack: 6
                            });
                            $('#ids'+id).remove();
                        },
                        error: function (response) {
                            $.toast({
                                heading: 'Error',
                                text: 'Item Null',
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'error',
                                hideAfter: 3500,
                                stack: 6
                            })
                        }
                    })}

        })
    </script>
    <script>
       $('.send-newsletter').on('click',function (e) {
           e.preventDefault();
           var sendId = $(this).data("id");
           var sendToken = $(this).data("token");
           $.ajax({
               url: "/send-newsletter/"+sendId,
               type: 'POST',
               dataType: "JSON",
               data: {
                   'id': sendId,
                   "_method": 'POST',
                   "_token": sendToken
               },
               success: function (response) {
                   $.toast({
                       heading: 'Success',
                       text: response.success,
                       position: 'top-right',
                       loaderBg: '#ff6849',
                       icon: 'info',
                       hideAfter: 3500,
                       stack: 6
                   });
               },
               error: function (response) {
                   $.toast({
                       heading: 'Error',
                       text: response.responseJSON.error,
                       position: 'top-right',
                       loaderBg: '#ff6849',
                       icon: 'error',
                       hideAfter: 3500,
                       stack: 6
                   })
               }

           })
       })
    </script>
@endsection