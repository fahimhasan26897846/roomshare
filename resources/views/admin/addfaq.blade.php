@extends('admin.inc.master')
@section('title','ADD FAQ')
@php
    $active = "faq";
@endphp
@section('style')
    <link rel="stylesheet" href="{{asset('css/admin/css/bootstrap-wysihtml5.css')}}">
    <link href="{{asset('css/admin/css/bootstrap-tagsinput.css')}}" rel="stylesheet" />
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">ADD FAQ</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <form id="ajax-form">
                        <div class="form-group">
                        <label class="col-md-12">Asked Question</label>
                        <input type="text" class="form-control" name="question" required="">
                        </div>

                        <div class="form-group">
                            <label for="mymcei">Answer</label>
                        <textarea id="mymcei" class="form-control" cols="30" rows="10" name="mymce" required=""></textarea>
                        </div>

                        <h3 class="box-title">Meta Tags</h3>
                        <div class="tags-default">
                            <input type="text" value="pricecomparison.com,e-commerce,compare price" data-role="tagsinput" placeholder="add tags" name="meta_tag" required=""/>
                        </div>

                        <div class="form-group text-right pt-4">
                            <button type="submit" class="btn-submit btn btn-primary">SUBMIT</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            if ($("#mymce").length > 0) {
                tinymce.init({
                    selector: "textarea#mymce",
                    mode:  "textareas",
                    theme: "modern",
                    height: 300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker", "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking", "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                });
            }
        });
    </script>
    <script src="{{asset('js/admin/js/bootstrap-tagsinput.js')}}"></script>
    <script>
        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
        });

        $(".btn-submit").click(function(e){

            e.preventDefault();


            var question = $("input[name=question]").val();
            var answer = $("#mymcei").val();
            var metaTag = $("input[name=meta_tag]").val();
            $.ajax({

                type:'POST',
                url:'/add-faq',
                data:{question: question,
                      answer:   answer,
                      meta:     metaTag
                },
                success:function(response){
                    $.toast({
                        heading: 'Success',
                        text: response.success,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'info',
                        hideAfter: 3500,
                        stack: 6
                    })
                    document.getElementById("ajax-form").reset();
                },
                error: function (response) {
                    $.toast({
                        heading: 'Error',
                        text: response.responseJSON.error,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500,
                        stack: 6
                    })

                }

            });



        });
    </script>
@endsection