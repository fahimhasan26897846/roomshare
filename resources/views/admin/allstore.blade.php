@extends('admin.inc.master')
@section('title','DASHBOARD')
@php
    $active = "store";
@endphp
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">DASHBOARD</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                    <li class="active">All store</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row">
            @foreach($stores as $store)
                @php
                $id = \Illuminate\Support\Facades\Crypt::encrypt($store->id);
                @endphp
            <div class="col-lg-4 col-sm-4" id="ides{{$id}}">
                <div class="panel panel-primary">
                    <div class="panel-heading"> {{$store->name}}
                        <div class="pull-right"><a href="/edit-store/{{$id}}"><i class="fa fa-pencil"></i></a><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" class="deleteProduct" data-perform="panel-dismiss" data-id="{{ $id }}" data-token="{{ csrf_token() }}" ><i class="ti-close"></i></a> </div>
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <p>{{$store->description}}</p><br>
                            <img src="{{asset('file/store/avatar/'.$store->avatar)}}" height="100px" width="150px" alt="{{$store->name}}">
                        </div>
                    </div>
                </div>
            </div>
                @endforeach
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
    <script>
        $(".deleteProduct").click(function() {
            var del= confirm("Do you really want to delete this?");
            if (del){ var id = $(this).data("id");
                var token = $(this).data("token");
                $.ajax(
                    {
                        url: "/store-delete/" + id,
                        type: 'DELETE',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        success: function (response) {
                            $.toast({
                                heading: 'Success',
                                text: response.success,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'info',
                                hideAfter: 3500,
                                stack: 6
                            });
                            $('#ides'+id).remove();
                        },
                        error: function (response) {
                            $.toast({
                                heading: 'Error',
                                text: rsponse.responseJSON.error,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'error',
                                hideAfter: 3500,
                                stack: 6
                            })
                        }
                    })}

        })
    </script>
@endsection