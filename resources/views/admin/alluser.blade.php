@extends('admin.inc.master')
@section('title','All user')
@php
    $active='user'
@endphp
@section('content')
    <div class="container-fluid">
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Users</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                <li class="active">User</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
        <div class="row">

                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">User Table</h3>
                            <div class="table-responsive">
                                <table class="table color-table primary-table">
                                    <thead>
                                    <tr>
                                        <th>Number</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>DOB</th>
                                        <th>Role</th>
                                        <th>Gender</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $number = 1 ?>
                                    @if($admins != NULL)
                                    @foreach($admins as $user)
                                    <tr>
                                        <td>{{$number}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->dob}}</td>
                                        <td>{{$user->slug}}</td>
                                        <td>{{$user->gender}}</td>
                                        <td><a href="#"  class="mr-2" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a><a href="#" class="mr-2 text-warning" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                                        class="fa fa-pencil"></i></a><a href="#" class="mr-2 text-danger" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a><a
                                                    href="#"class="mr-2 text-success" data-toggle="tooltip" data-placement="top" title="Active"><i class="fa fa-circle"></i></a></td>
                                    </tr>
                                    @endforeach
                                        @else {!! ' NULL ' !!}
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

            </div>

        </div>
@endsection