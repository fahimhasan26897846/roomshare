@extends('admin.inc.master')
@section('title','DASHBOARD')
@php
    $active = "role";
@endphp
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">DASHBOARD</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>

        <div class="row">
            <div class="col-md-12">
                @php
                $me = 1;
                @endphp
                @foreach($roles as $role)
                    @php
                        $id = \Illuminate\Support\Facades\Crypt::encrypt($role->id);
                    @endphp
                <div class="panel panel-default" id="ide{{$id}}">
                    <div class="panel-heading">{{$role->name}}</div>

                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            Name : {{$role->name}} <br>
                            Slug : {{$role->slug}} <br>
                            Permission: {{$role->permissions}}

                        </div>
                    </div>

                    <div class="panel-footer"><a data-toggle="modal" data-target="#edit{{$me}}" href="#"><i class="fa fa-pencil pr-2"></i></a><a href="#" data-id="{{ $id }}" data-token="{{ csrf_token() }}" class="deleteProduct"><i class="fa fa-trash"></i></a> </div>
                </div>

                    <!-- Modal -->
                    <div class="modal fade" id="edit{{$me}}" tabindex="-1" role="dialog" aria-labelledby="#edits{{$me}}" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="edits{{$me}}">{{$role->name}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form id="unique{{$me}}">
                                <div class="modal-body">
                                        <input type="hidden" name="id" value="{{$id}}">
                                        <div class="form-group"><label for="name">Name</label><input type="text" name="name" class="form-control" value="{{$role->name}}"></div>
                                        <div class="form-group"><label for="slug">Slug</label><input type="text" name="slug" class="form-control" value="{{$role->slug}}"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="roleedit btn btn-primary">Save changes</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>




                    @php
                    $me++;
                            @endphp
                    @endforeach
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
    <script>
        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
        });

        $(".roleedit").click(function(e){

            e.preventDefault();


            var name = $("input[name=name]").val();
            var slug = $("input[name=slug]").val();
            $.ajax({

                type:'PUT',
                url:'/role-update',
                data:{name: name,
                      slug:   slug
                },
                success:function(response){
                    $.toast({
                        heading: 'Success',
                        text: response.success,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'info',
                        hideAfter: 3500,
                        stack: 6
                    })
                },
                error: function (response) {
                    $.toast({
                        heading: 'Error',
                        text: 'Item Null',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500,
                        stack: 6
                    })

                }

            });



        });
    </script>
    <script>
        $(".deleteProduct").click(function() {
            var del= confirm("Do you really want to delete this?");
            if (del){ var id = $(this).data("id");
                var token = $(this).data("token");
                $.ajax(
                    {
                        url: "/role-delete/" + id,
                        type: 'DELETE',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        success: function (response) {
                            $.toast({
                                heading: 'Success',
                                text: response.success,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'info',
                                hideAfter: 3500,
                                stack: 6
                            });
                            $('#ide'+id).remove();
                        },
                        error: function (response) {
                            $.toast({
                                heading: 'Error',
                                text: 'Item Null',
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'error',
                                hideAfter: 3500,
                                stack: 6
                            })
                        }
                    })}

        })
    </script>
@endsection