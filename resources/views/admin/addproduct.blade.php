@extends('admin.inc.master')
@php
    $active = 'product'
@endphp
@section('style')
    <link rel="stylesheet" href="{{asset('css/admin/css/dropify.min.css')}}">
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add Products</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                    <li class="active">Add products</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <form id="ajaxi-form" method="post">
                        {{csrf_field()}}
                        <div class="form-group"> <h3 class="box-title m-b-0">Store</h3>
                            <select class="form-control select2" id="store" name="store">
                                <option>Select</option>
                                @foreach($store as $dot)
                                    <option value="{{$dot->id}}">{{$dot->name}}</option>
                                @endforeach
                            </select></div>
                        <div class="form-group"> <h3 class="box-title m-b-0">Category</h3>
                            <select class="form-control select2" id="category" name="category_id">
                                <option>Select</option>
                                @foreach($category as $cat)
                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                @endforeach
                            </select></div>
                        <div class="form-group"> <h3 class="box-title m-b-0">Sub Category</h3>
                            <select class="form-control selectinfinity" id="testSub" name="sub_category_id">
                                <option>Select</option>
                            </select></div>

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" id="name" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="name">Link</label>
                            <input type="text" id="link" name="link" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="name">Price</label>
                            <input type="number" id="price" name="price" class="form-control">
                            <p class="text-muted">Amount in $ doller.</p>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea  id="description" cols="30" rows="10" name="description" class="form-control"></textarea></div>
                        <label for="logo">Image</label>
                        <input type="file" id="image" name="image" class="dropify" data-default-file="{{asset('file/defaultavatar.png')}}"/>
                        <br>
                        <br>
                        <button class="btn btn-primary create-product" type="submit">Create Product</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('js/admin/js/dropify.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();
            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });
            // Used events
            var drEvent = $('#input-file-events').dropify();
            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });
            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });
            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });
            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>
    <script>
        $('#ajaxi-form').on('submit',function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: '/post-product',
                type: 'POST',
                data: formData,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success:function(response){
                    $.toast({
                        heading: 'Success',
                        text: response.success,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'info',
                        hideAfter: 3500,
                        stack: 6
                    });
                    document.getElementById("ajaxi-form").reset();
                },
                error: function (response) {
                    $.toast({
                        heading: 'Error',
                        text: 'Item Null',
                        position: response.responseJSON.error,
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500,
                        stack: 6
                    });

                }
            });

        });
    </script>
    <script>
        $(document).on('change','#category',function (event) {
            event.preventDefault();
                var gaga = $('#category').val();
                $.ajax({
                    url: '/sub-category-ajax',
                    type: 'GET',
                    data: {
                        subCategory : gaga
                    },
                    success: function (response) {
                        if(response.success){
                            $('.selectinfinity').empty();
                            $('#testSub').append(response.success);
                        }else {
                            $('#testSub').val("");
                        }

                    },
                    error: function (response) {
                    }
                })

        })
    </script>
@endsection