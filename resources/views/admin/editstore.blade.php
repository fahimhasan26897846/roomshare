@extends('admin.inc.master')
@section('title','DASHBOARD')
@php
    $active = "store";
@endphp
@section('style')
    <link rel="stylesheet" href="{{asset('css/admin/css/dropify.min.css')}}">
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">DASHBOARD</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <form id="ajax-form" method="POST">
                        {{csrf_field()}}
                        <div class="form-group">
                            @php
                                $id = \Illuminate\Support\Facades\Crypt::encrypt($content->id);
                                    @endphp
                            <input type="hidden" id="id" name="id" value="{{$id}}">
                            <label for="name">Name</label>
                            <input type="text" id="name" name="name" class="form-control" value="{{$content->name}}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea  id="description" cols="30" rows="10" name="description" class="form-control">{{$content->description}}</textarea></div>
                        <label for="logo">Logo</label>
                        <input type="file" id="logo" name="logo" class="dropify" data-default-file="{{asset('file/store/logo/'.$content->brand_logo)}}" />
                        <br>
                        <label for="avatar">Avatar:(should be in png format)</label>
                        <input type="file" id="avatar" name="avatar" class="dropify" data-default-file="{{asset('file/store/avatar/'.$content->avatar)}}" />
                        <br>
                        <button class="btn btn-primary" type="submit">Create Brand</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
    <script src="{{asset('js/admin/js/dropify.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();
            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });
            // Used events
            var drEvent = $('#input-file-events').dropify();
            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });
            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });
            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });
            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>
    <script>
        $('#ajax-form').on('submit',function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: '/post-edit-store',
                type: 'POST',
                data: formData,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success:function(response){
                    $.toast({
                        heading: 'Success',
                        text: response.success,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'info',
                        hideAfter: 3500,
                        stack: 6
                    });
                },
                error: function (response) {
                    $.toast({
                        heading: 'Error',
                        text: response.responseJSON.error,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500,
                        stack: 6
                    })

                }
            });

        });
    </script>
@endsection