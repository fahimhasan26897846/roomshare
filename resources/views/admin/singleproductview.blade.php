@extends('admin.inc.master')
@section('title','SUBSCRIBER')
@php
    $active = "product";
@endphp
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Product</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                    <li class="active">Product</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
    </div>
    <div class="container">
        <!-- .row -->
        <div class="row mt-2 mb-2">
            <div class="col-md-6">
                <div class="white-box"><img src="{{asset('file/product/'.$single->featured_image)}}" height="200px" width="260px" alt="price comparison"></div>

            </div>
            <div class="col-md-6">
                <div class="white-box">
                    Name: {{$single->name}} <br>
                    Category: {{$single->natagory}} <br>
                    Price: {{$single->price}}<br>
                    Detais: <br><p>{{$single->description}}</p> <br>
                    Link <a href="{{$single->link}}" class="btn btn-info">View</a>
                </div>

            </div>
        </div>
        <a href="/all-products" class="btn btn-rounded btn-primary"><i class="fa fa-plus"></i> GO BACK</a>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
    <script>
        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
        });
        $('.button-submit').on('click',function (e) {
            e.preventDefault();
            var email = $('input[name=email]').val();
            $.ajax({
                url: '/admin-add-subscriber',
                type: 'POST',
                data: {email: email},
                success:function(response){
                    $.toast({
                        heading: 'Success',
                        text: response.success,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'info',
                        hideAfter: 3500,
                        stack: 6
                    });
                    $('.appand').append('<tr><td>'+email+'</td><td class="test-nowrap"><a href="#" data-toggle="tooltip" class="delete-subscriber" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a></td></tr>');
                },
                error: function (response) {
                    $.toast({
                        heading: 'Error',
                        text: response.responseJSON.error,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500,
                        stack: 6
                    })

                }


            })

        })
    </script>
    <script>
        $('.delete-subscriber').on('click',function (e) {
            e.preventDefault();
            var del= confirm("Do you really want to delete this?");
            if (del){ var id = $(this).data("id");
                var token = $(this).data("token");
                $.ajax(
                    {
                        url: "/delete-subscriber/" + id,
                        type: 'DELETE',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        success: function (response) {
                            $.toast({
                                heading: 'Success',
                                text: response.success,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'info',
                                hideAfter: 3500,
                                stack: 6
                            });
                            $('#ids'+id).remove();
                        },
                        error: function (response) {
                            $.toast({
                                heading: 'Error',
                                text: response.responseJSON.error,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'error',
                                hideAfter: 3500,
                                stack: 6
                            })
                        }
                    })}

        })
    </script>
@endsection