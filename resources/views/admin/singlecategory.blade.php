@extends('admin.inc.master')
@section('title','DASHBOARD')
@php
    $active = "category";
@endphp
@section('style')
    <link rel="stylesheet" href="{{asset('css/admin/css/bootstrap-wysihtml5.css')}}">
    <link href="{{asset('css/admin/css/bootstrap-tagsinput.css')}}" rel="stylesheet" />
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">DASHBOARD</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                    <li class="active">Single Category</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row">
            <div class="col-md-12">
                <div class="white-box mb-4">
                    Category name: {{$category->name}} <br>
                    Category description: {{$category->description}} <br>
                </div>
                <button class="btn btn-primary mb-4" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i>&nbsp; Add new sub category</button>


                    @php
                    $enc = \Illuminate\Support\Facades\Crypt::encrypt($category->id);
                    @endphp
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">ADD NEW SUB CATEGORY</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form id="ajax-form">
                                <div class="modal-body">
                                    <input type="hidden" name="category_id" value="{{$enc}}">
                                    <div class="form-group"><label for="name">Name</label><input type="text" id="name" name="name" class="form-control"></div>
                                    <div class="form-group"><label for="description">Description</label><textarea
                                                name="description" class="form-control" id="description" cols="30" rows="10"></textarea></div>
                                    <div class="form-group"><label for="meta-description">Meta description</label><textarea
                                                name="meta_description" class="form-control" id="meta-description" cols="30"
                                                rows="10"></textarea></div>
                                    <div class="form-group"><label for="meta_description">Key words</label><input type="text" name="meta_tag" class="form-control" value="pricecomparison.com,e-commerce,compare price" data-role="tagsinput" placeholder="add tags" required=""></div>
                                    <div class="form-group"><label for="meta-author">Author</label><input type="text" name="meta_author" id="meta-author" class="form-control"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn-submit btn btn-primary">Save changes</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <div class="refresh">
                @foreach($subs as $sub)
                    @php
                        $encSub = \Illuminate\Support\Facades\Crypt::encrypt($sub->id);
                    @endphp
                <div class="white-box" id="ide{{$encSub}}">
                    Name: {{$sub->name}} <br>
                    Description: {{$sub->description}}
                    <br>
                    <div class="d-flex">
                        <a href="/editsub/{{$encSub}}"><i class="fa fa-pencil mr-4"></i></a><a class="deletecat" data-id="{{ $encSub }}" data-token="{{ csrf_token() }}" href="#"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
                    @endforeach
                </div>
                </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
    <script src="{{asset('js/admin/js/bootstrap-tagsinput.js')}}"></script>
    <script>
        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
        });

        $(".btn-submit").click(function(e){

            e.preventDefault();


            var name = $("input[name=name]").val();
            var categoryId= $("input[name=category_id]").val();
            var description= $("#description").val();
            var metaKeyword = $("input[name=meta_tag]").val();
            var metaDescription = $("#meta-description").val();
            var metaAuthor = $("input[name=meta_author]").val();

            $.ajax({

                type:'POST',
                url:'/create-subcategory',
                data:{
                    name:           name,
                    category_id:    categoryId,
                    description:    description,
                    meta_tag:       metaKeyword,
                    meta_description: metaDescription,
                    meta_author:     metaAuthor
                },
                success:function(response){
                    $.toast({
                        heading: 'Success',
                        text: response.success,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'info',
                        hideAfter: 3500,
                        stack: 6
                    });
                    document.getElementById("ajax-form").reset();
                    $(".refresh").append("<div class='white-box'>Name:"+name+"<br> Description: "+description+"<div class='d-flex'>Newly added data does not has operations without page refresh.</div></div>");
                },
                error: function (response) {
                    $.toast({
                        heading: 'Error',
                        text: 'Item Null',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500,
                        stack: 6
                    })

                }

            });



        });
    </script>
    <script>
        $(".deletecat").click(function() {
            var del= confirm("Do you really want to delete this?");
            if (del){ var id = $(this).data("id");
                var token = $(this).data("token");
                $.ajax(
                    {
                        url: "/delete-sub-cetegory/" + id,
                        type: 'DELETE',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        success: function (response) {
                            $.toast({
                                heading: 'Success',
                                text: response.success,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'info',
                                hideAfter: 3500,
                                stack: 6
                            });
                            $('#ide'+id).remove();
                        },
                        error: function (response) {
                            $.toast({
                                heading: 'Error',
                                text: response.responseJSON.error,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'error',
                                hideAfter: 3500,
                                stack: 6
                            })
                        }
                    })}

        })
    </script>
@endsection