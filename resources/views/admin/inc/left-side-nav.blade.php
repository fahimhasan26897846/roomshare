<!-- Left navbar-sidebar -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- Search input-group this is only view in mobile -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                        </span>
                </div>
                <!-- / Search input-group this is only view in mobile-->
            </li>
            <!-- User profile-->
            <li class="user-pro">
                <a href="#" class="waves-effect"><img src="{{asset('image/pluginimage/users/varun.jpg')}}" alt="user-img" class="img-circle"> <span class="hide-menu"> Steve Gection<span class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
                    <li><a href="javascript:void(0)" onclick="document.location='/'"><i class="ti-home"></i> Home</a></li>
                    <li><a href="javascript:void(0)"><i class="ti-email"></i> Inbox</a></li>
                    <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>
                    <li><a href="javascript:void(0)" onclick="document.location='/admin/logout'"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
            </li>
            <!-- User profile-->
            <li class="nav-small-cap m-t-10">--- Main Menu</li>
            <li>
                <a href="{{url('admin/dashboard')}}" class="waves-effect <?php if ($active== 'dashboard') echo "active" ?> "><i data-icon="T" class="linea-icon linea-ecommerce fa-fw"></i> <span class="hide-menu">DASHBOARD</span></a>
            </li>
            @if(Sentinel::getUser()->hasAccess('user.view'))
            <li>
                <a href="javascript:void(0)" class="waves-effect <?php if ($active== 'user') echo "active" ?>"><i data-icon="Y" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">User<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{{url('admin/user/list')}}">All user</a></li>
                    <li><a href="{{url('admin/affiliaters/list')}}">Affiliaters</a></li>
                    <li><a href="{{url('admin/admin/registration')}}">Add user</a></li>
                </ul>
            </li>
            @endif
            @if(Sentinel::getUser()->hasAccess('user.view'))
            <li>
                <a href="javascript:void(0)" class="waves-effect <?php if ($active== 'role') echo "active" ?>"><i data-icon="P" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">ROLES<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{{url('admin/allrole')}}">All roles</a></li>
                    <li><a href="{{url('admin/addrole')}}">Add role</a></li>
                </ul>
            </li>
            @endif
            @if(Sentinel::getUser()->hasAccess('user.view'))
            <li>
                <a href="{{url('admin/subscribers')}}" class="waves-effect <?php if ($active== 'subscriber') echo "active" ?>"><i data-icon="b" class="linea-icon linea-music fa-fw"></i> <span class="hide-menu">Subscriber</span></a>
            </li>
            @endif
            @if(Sentinel::getUser()->hasAccess('newsletter.view'))
            <li>
                <a href="{{url('admin/add-newsletter')}}" class="waves-effect <?php if ($active== 'newsletter') echo "active" ?>"><i data-icon=")" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">NEWSLETTER</span></a>
            </li>
            @endif
            @if(Sentinel::getUser()->hasAccess('contacts.view'))
            <li>
                <a href="javascript:void(0)" class="waves-effect <?php if ($active== 'contact') echo "active" ?>"><i data-icon="1" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">CONTACT<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="javascript:void(0)">Reply contact</a> </li>
                    <li> <a href="javascript:void(0)">Contact info list</a></li>
                </ul>
            </li>
            @endif
            @if(Sentinel::getUser()->hasAccess('calender.view'))
            <li>
                <a href="{{url('admin/calender')}}" class="waves-effect <?php if ($active== 'calender') echo "active" ?>"><i data-icon="r" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Calender</span></a>
            </li>
            @endif
            @if(Sentinel::getUser()->hasAccess('faq.view'))
            <li>
                <a href="javascript:void(0)" class="waves-effect <?php if ($active== 'faq') echo "active" ?>"><i data-icon="&#xe027;" class="linea-icon linea-aerrow fa-fw"></i> <span class="hide-menu">FAQ<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{{url('admin/allfaq')}}">All faq</a></li>
                    <li><a href="{{url('admin/addfaq')}}">Add faq</a></li>
                </ul>
            </li>
            @endif
            <li>
                <a href="javascript:void(0)" class="waves-effect <?php if ($active== 'documentation') echo "active" ?>"><i data-icon="m" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Documentations<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="javascript:void(0)">Admin policy</a> </li>
                    <li> <a href="javascript:void(0)">User policy</a></li>
                    <li><a href="javascript:void(0)">Product policy</a></li>
                </ul>
            </li>
            <li>
                <a href="{{url('admin/aboutdevelopper')}}" class="waves-effect <?php if ($active== 'about-developper') echo "active" ?>"><i data-icon="&#xe028;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">About us</span></a>
            </li>
        </ul>
    </div>
</div>
<!-- Left navbar-sidebar end -->