<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
        <div class="top-left-part"><a class="logo" href="{{url('/admin/dashboard')}}"><b><img src="{{asset('image/pluginimage/eliteadmin-logo.png')}}" alt="home" /></b><span class="hidden-xs"><img src="{{asset('image/pluginimage/eliteadmin-text.png')}}" alt="home" /></span></a></div>
        <ul class="nav navbar-top-links navbar-left hidden-xs">
            <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">
        @php
            $user = \App\User::find(Sentinel::getUser()->id);
        @endphp
        <!-- /.dropdown -->
            <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-bell"></i>
                    <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                </a>
                <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                    @if(count($user->unreadNotifications) == 0)
                        @foreach($user->notifications->take(3) as $notification)
                            <li>
                                <a href="{{url($notification->data['link'])}}" >
                                    <div>
                                        <p> <strong>{{$notification->data['data']}}</strong></p>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                        @endforeach
                    @else
                        @foreach($user->unreadNotifications->take(3) as $notification)
                            <li>
                                <a href="{{url($notification->data['link'])}}" >
                                    <div>
                                        <p> <strong>{{$notification->data['data']}}</strong></p>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                        @endforeach
                    @endif
                    <li>
                        <a class="text-center" href="{{url('admin/notification')}}"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a>
                    </li>
                </ul>
                <!-- /.dropdown-tasks -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="{{asset('image/pluginimage/users/varun.jpg')}}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">{{Sentinel::getUser()->first_name}}</b> </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                    <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>