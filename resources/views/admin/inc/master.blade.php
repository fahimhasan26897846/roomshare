<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('image/pluginimage/favicon.png')}}">
    <title>GETTING ROOM | @yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('css/admin/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/admin/css/bootstrap-extension.css')}}" rel="stylesheet">
    <!-- This is Sidebar menu CSS -->
    <link href="{{asset('css/admin/css/sidebar-nav.min.css')}}" rel="stylesheet">
    <!-- This is a Animation CSS -->
    <link href="{{asset('css/admin/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/admin/css/jquery.toast.css')}}" rel="stylesheet">
    @yield('style')
    <!-- This is a Custom CSS -->
    <link href="{{asset('css/admin/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/admin/css/customstyle.css')}}" rel="stylesheet">
    <!-- color CSS you can use different color css from css/colors folder -->
    <!-- We have chosen the skin-blue (blue.css) for this starter
         page. However, you can choose any other skin from folder css / colors .
         -->
    <link href="{{asset('css/admin/css/colors/blue.css')}}" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
    @include('admin.inc.header')
    @include('admin.inc.left-side-nav')
    <!-- Page Content -->
    <div id="page-wrapper">
    @yield('content')




        @include('admin.inc.footer')
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="{{asset('js/admin/js/jquery.min.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{asset('js/admin/js/tether.min.js')}}"></script>
<script src="{{asset('js/admin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/admin/js/bootstrap-extension.min.js')}}"></script>
<!-- Sidebar menu plugin JavaScript -->
<script src="{{asset('js/admin/js/sidebar-nav.min.js')}}"></script>
<!--Slimscroll JavaScript For custom scroll-->
<script src="{{asset('js/admin/js/jquery.slimscroll.js')}}"></script>
<!--Wave Effects -->
<script src="{{asset('js/admin/js/waves.js')}}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{asset('js/admin/js/custom.min.js')}}"></script>
<script src="{{asset('js/admin/js/jquery.toast.js')}}"></script>
@yield('script')
<script>
    @if(session('success'))
    alert("{{session('success')}}");
    @endif
    @if(session('error'))
    alert("{{session('error')}}");
    @endif
</script>
</body>

</html>
