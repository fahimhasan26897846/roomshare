@extends('admin.inc.master')
@section('title','DASHBOARD')
@php
    $active = "category";
@endphp
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Category</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                    <li class="active">Category</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>

        <div class="row">
            @foreach($categories as $cat)
                @php
                        $enc = \Illuminate\Support\Facades\Crypt::encrypt($cat->id);
                        @endphp
            <div class="panel panel-default mr-4" id="ide{{$enc}}">
                <div class="panel-heading">{{$cat->name}}</div>

                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        Name: {{$cat->name}}
                       Description: {{$cat->description}} <br>
                        Meta Author: {{$cat->meta_author}} <br>
                        Meta Tag: {{$cat->meta_keywords}} <br>
                        Meta Description: {{$cat->meta_description}} <br>
                    </div>
                </div>

                <div class="panel-footer"><a href="/show-single/{{$enc}}" class="pr-4"><i class="fa fa-eye"></i></a><a href="edit-category/{{$enc}}" class="pr-4"><i class="fa fa-pencil"></i></a><a href="#"><i
                               data-id="{{$enc}}" data-token="{{csrf_token()}}" class="deletecat fa fa-trash" ></i></a> </div>
            </div><br><br>
                @endforeach
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')

<script>
    $(".deletecat").click(function() {
        var del= confirm("Do you really want to delete this?");
        if (del){ var id = $(this).data("id");
            var token = $(this).data("token");
            $.ajax(
                {
                    url: "/delete-category/" + id,
                    type: 'DELETE',
                    dataType: "JSON",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    success: function (response) {
                        $.toast({
                            heading: 'Success',
                            text: response.success,
                            position: 'top-right',
                            loaderBg: '#ff6849',
                            icon: 'info',
                            hideAfter: 3500,
                            stack: 6
                        });
                        $('#ide'+id).remove();
                    },
                    error: function (response) {
                        $.toast({
                            heading: 'Error',
                            text: response.responseJSON.error,
                            position: 'top-right',
                            loaderBg: '#ff6849',
                            icon: 'error',
                            hideAfter: 3500,
                            stack: 6
                        })
                    }
                })}

    })
</script>
@endsection