@extends('admin.inc.master')
@section('title','DASHBOARD')
@php
    $active = "category";
@endphp
@section('style')
    <link rel="stylesheet" href="{{asset('css/admin/css/bootstrap-wysihtml5.css')}}">
    <link href="{{asset('css/admin/css/bootstrap-tagsinput.css')}}" rel="stylesheet" />
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Category Create Page</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                    <li class="active">Category</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <form id="ajaxy-form">
                        @php
                                $crypt = \Illuminate\Support\Facades\Crypt::encrypt($content->id);
                                @endphp
                        <input type="hidden" id="id" value="{{$crypt}}">
                        <div class="form-group">
                            <label for="name" class="col-md-12">Name</label>
                            <input type="text" id="name" class="form-control" name="names" value="{{$content->name}}">
                        </div>

                        <div class="form-group">
                            <label for="des">Description</label>
                            <textarea id="des" class="form-control" cols="30" rows="10" required="">{{$content->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="mdes">Meta Description</label>
                            <textarea id="mdes" class="form-control" cols="30" rows="10">{{$content->meta_description}}</textarea>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12" for="author">Meta Author</label>
                            <input type="text" id="author" class="form-control" name="meta_author" value="{{$content->meta_author}}">
                        </div>

                        <h3 class="box-title">Meta Tags</h3>
                        <div class="tags-default">
                            <input type="text" id="meta-tag" value="{{$content->meta_keywords}}" data-role="tagsinput" placeholder="add tags" name="meta_tag"/>
                        </div>

                        <div class="form-group text-right pt-4">
                            <button type="submit" class="btn-submits btn btn-primary">SUBMIT</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
    <script src="{{asset('js/admin/js/bootstrap-tagsinput.js')}}"></script>
    <script>
        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
        });

        $(".btn-submits").click(function(e){

            e.preventDefault();

            var id  =             $("#id").val()
            var name =            $("#name").val();
            var description =     $("#des").val();
            var metaDescription = $("#mdes").val();
            var metaAuthor =      $("#author").val();
            var metaTag =         $("#meta-tag").val();
            $.ajax({

                type:'POST',
                url:'/update-category',
                data:{
                    id: id,
                    name:             name,
                    description:      description,
                    meta_tag:         metaTag,
                    meta_author:      metaAuthor,
                    meta_description: metaDescription
                },
                success:function(response){
                    $.toast({
                        heading: 'Success',
                        text: response.success,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'info',
                        hideAfter: 3500,
                        stack: 6
                    })
                    document.getElementById("ajaxy-form").reset();
                },
                error: function (response) {
                    $.toast({
                        heading: 'Error',
                        text: response.responseJSON.error,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500,
                        stack: 6
                    })

                }

            });



        });
    </script>
@endsection