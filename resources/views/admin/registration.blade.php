@extends('admin.inc.master')
@section('title','Registration')
@php
    $active = 'admin'
@endphp
@section('content')
    <div class="container-fluid">
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">ADD NEW USER</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                <li class="active">ADD USER</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading"> Register New User</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                            <div class="form-body">
                                <h3 class="box-title">Person Info</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Name</label>
                                            <input type="text" id="name" name="name" class="form-control" placeholder="Name"></div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Email</label>
                                            <input type="email" id="email" name="email" class="form-control" placeholder="Email"></div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Gender</label>
                                            <select class="form-control" id="gender" name="gender">
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                                <option value="other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Date of Birth</label>
                                            <input type="date" id="dob" name="dob" class="form-control" placeholder="dd/mm/yyyy"> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Role</label>
                                            <select class="form-control" id="role" name="role" data-placeholder="Choose a Category" tabindex="1">
                                                @foreach($role as $single)
                                                <option value="{{$single->slug}}">{{$single->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <h3 class="box-title m-t-40">Password</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" id="password" name="password" class="form-control"> </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input type="password" id="confirm-password" name="confirm_password" class="form-control"> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                            <div class="form-actions">
                                <button type="button" class="btn btn-success register-button"> <i class="fa fa-plus"></i> CREATE</button>
                                <button type="button" class="btn btn-default">Cancel</button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('script')
    <script>
        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
        });
        $('.register-button').on('click',function (e) {
            e.preventDefault();
            var name = $("input[name=name]").val();
            var email = $('input[name=email]').val();
            var gender = $("#gender").val();
            var dob = $('#dob').val();
            var role = $('#role').val();
            var password = $('#password').val();
            var confirmPassowrd = $('#confirm-password').val();

            $.ajax({
                url: '/admin-post-registration',
                type: 'POST',
                data: {
                    name: name,
                    email: email,
                    gender: gender,
                    dob: dob,
                    role: role,
                    password: password,
                    confirm_pssword: confirmPassowrd
                },
                success:function(response){
                    $.toast({
                        heading: 'Success',
                        text: response.success,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'info',
                        hideAfter: 3500,
                        stack: 6
                    });
                    $('#name').val("");
                    $('#email').val("");
                    $('#gender').val("");
                    $('#dob').val("");
                    $('#role').val("");
                    $('#password').val("");
                    $('#confirm-password').val("");
                },
                error: function (response) {
                    $.toast({
                        heading: 'Error',
                        text: response.responseJSON.error,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500,
                        stack: 6
                    });

                }

            })
        })
    </script>
@endsection