@extends('admin.inc.master')
@section('title','SUBSCRIBER')
@php
    $active = "subscriber";
@endphp
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">SUBSCRIBERS</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
           <i class="fa fa-plus"></i>&nbsp; ADD NEW SUBSCRIBER
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">SUBSCRIBER</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group"><label for="email">Enter New Email</label><input id="email" name="email" type="email" class="form-control"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn  btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn button-submit btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- .row -->
        <div class="row mt-2 mb-2">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="appand">
                            <tr>
                                <th>Task</th>
                                <th class="text-nowrap">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($subscribers as $subscriber)
                                @php
                                $id = \Illuminate\Support\Facades\Crypt::encrypt($subscriber->id)
                                        @endphp
                            <tr id="ids{{$id}}">
                                <td>{{ $subscriber->email }}</td>
                                <td class="text-nowrap">
                                    <a href="#" data-toggle="tooltip" class="delete-subscriber" data-original-title="Close" data-id="{{$id}}" data-token="{{csrf_token()}}"> <i class="fa fa-close text-danger"></i> </a>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
    <script>
        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
        });
        $('.button-submit').on('click',function (e) {
            e.preventDefault();
            var email = $('input[name=email]').val();
            $.ajax({
                url: '/admin-add-subscriber',
                type: 'POST',
                data: {email: email},
                success:function(response){
                    $.toast({
                        heading: 'Success',
                        text: response.success,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'info',
                        hideAfter: 3500,
                        stack: 6
                    });
                    $('.appand').append('<tr><td>'+email+'</td><td class="test-nowrap"><a href="#" data-toggle="tooltip" class="delete-subscriber" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a></td></tr>');
                },
                error: function (response) {
                    $.toast({
                        heading: 'Error',
                        text: response.responseJSON.error,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500,
                        stack: 6
                    })

                }


            })

        })
    </script>
    <script>
        $('.delete-subscriber').on('click',function (e) {
            e.preventDefault();
            var del= confirm("Do you really want to delete this?");
            if (del){ var id = $(this).data("id");
                var token = $(this).data("token");
                $.ajax(
                    {
                        url: "/delete-subscriber/" + id,
                        type: 'DELETE',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        success: function (response) {
                            $.toast({
                                heading: 'Success',
                                text: response.success,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'info',
                                hideAfter: 3500,
                                stack: 6
                            });
                            $('#ids'+id).remove();
                        },
                        error: function (response) {
                            $.toast({
                                heading: 'Error',
                                text: response.responseJSON.error,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'error',
                                hideAfter: 3500,
                                stack: 6
                            })
                        }
                    })}

        })
    </script>
@endsection