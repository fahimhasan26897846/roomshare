@extends('admin.inc.master')
@section('title','DASHBOARD')
@php
    $active = "role";
@endphp
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">ADD ROLE</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{asset('/dashboard')}}">DASHBOARD</a></li>
                    <li CLASS="active">ROLE</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Add New Role</h3>
                    <form class="form-horizontal" id="ajax-role-form">
                        <div class="form-group">
                            <label class="col-md-12">ROLE NAME</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="name" placeholder="NAME">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="example-email">SLUG</label>
                            <div class="col-md-12">
                                <input type="text" id="example-email" name="slug" class="form-control" placeholder="Slug">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <div class="form-check">
                                    <div>Admin</div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'admin':true," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">True</span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'admin':false," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">False</span>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <div>User</div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'user':true," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">True</span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'user':false," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">False</span>
                                    </label>
                                </div>
                                <div class="form-check bd-example-indeterminate">
                                    <div>Role</div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'role':true," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">True</span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'role':false," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">False</span>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <div>Category</div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'category':true," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">True</span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'category':false," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">False</span>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <div>Products</div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'product': true," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">True</span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'product': false," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">False</span>
                                    </label>
                                </div>
                                <div class="form-check bd-example-indeterminate">
                                    <div>Store:</div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'store': true," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">True</span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'store': false," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">False</span>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <div>Offer</div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox"  name="permission[]" value="'offer': true," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">True</span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox"  name="permission[]" value="'offer': false," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">False</span>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <div>Subscribe</div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'subscribe': true," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">True</span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'subscribe': false," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">False</span>
                                    </label>
                                </div>
                                <div class="form-check bd-example-indeterminate">
                                    <div>Newsletter</div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'newsletter': true," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">True</span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'newsletter': true," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">False</span>
                                    </label>
                                </div>
                                <div class="form-check">
                                   <div>Contact</div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="contact" name="permission[]" value="'contact': true ," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">True</span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="contact" name="permission[]" value="'contact': false ," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">False</span>
                                    </label>
                                </div>
                                <div class="form-check bd-example-indeterminate">
                                     <div>Calender:</div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'calender': true," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">True</span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="permission[]" value="'calender': false," class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">False</span>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <div>Faq:</div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="faq" name="permission[]" value="'faq':true" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">True</span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="faq" name="permission[]" value="'faq':false" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">False</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="role-submit btn btn-primary"><i class="fa fa-plus"></i>&nbsp;CREATE</button>

                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
    <script>
        if(document.getElementById("admin").checked) {
            document.getElementById('admin-hidden').disabled = true;
        }
        if(document.getElementById("role").checked) {
            document.getElementById('role-hidden').disabled = true;
        }
        if(document.getElementById("category").checked) {
            document.getElementById('category-hidden').disabled = true;
        }
        if(document.getElementById("product").checked) {
            document.getElementById('product-hidden').disabled = true;
        }
        if(document.getElementById("store").checked) {
            document.getElementById('store-hidden').disabled = true;
        }
        if(document.getElementById("offer").checked) {
            document.getElementById('offer-hidden').disabled = true;
        }
        if(document.getElementById("subscribe").checked) {
            document.getElementById('subscribe-hidden').disabled = true;
        }
        if(document.getElementById("newsletter").checked) {
            document.getElementById('newsletter-hidden').disabled = true;
        }
        if(document.getElementById("contact").checked) {
            document.getElementById('contact-hidden').disabled = true;
        }
        if(document.getElementById("calender").checked) {
            document.getElementById('calender-hidden').disabled = true;
        }
        if(document.getElementById("faq").checked) {
            document.getElementById('faq-hidden').disabled = true;
        }
    </script>
    <script>
        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
        });

        $(".role-submit").click(function(e){

            e.preventDefault();


            var name = $("input[name=name]").val();
            var slug = $("input[name=slug]").val();
            // var permission = { 'permission[]' : []};
            // $(":checked").each(function() {
            //     permission['permission[]'].push($(this).val());
            // });
            var permission = [];
            $('.custom-control-input').each(function(){
                if ($(this).is(":checked")){
                    permission.push($(this).val());
                }
            });

            $.ajax({

                type:'POST',
                url:'/post-role',
                data:{name: name,
                    slug:   slug,
                    permission: permission
                },
                success:function(response){
                    $.toast({
                        heading: 'Success',
                        text: response.success,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'info',
                        hideAfter: 3500,
                        stack: 6
                    })
                    document.getElementById("ajax-role-form").reset();
                },
                error: function (response) {
                    $.toast({
                        heading: 'Error',
                        text: response.responseJSON.error,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500,
                        stack: 6
                    })

                }

            });



        });
    </script>
@endsection