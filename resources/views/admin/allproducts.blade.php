@extends('admin.inc.master')
@section('title','All product')
@php
    $active = 'product'
@endphp
@section('style')
    <link href="{{asset('css/admin/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="container-fluid">
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">All Products</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                <li class="active">All products</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Products</h3>
                <p class="text-muted m-b-30">Products of all store and category</p>
                <div class="table-responsive">
                    <table id="myTable" class="table table-striped">
                        <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Actons</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                        $serial = 1;
                        @endphp
                        @foreach($product as $single)
                            @php
                            $request = \Illuminate\Support\Facades\Crypt::encrypt($single->id);
                            @endphp
                        <tr id="id{{$request}}">
                            <td>{{$serial}}</td>
                            <td><img src="{{asset('file/product/'.$single->featured_image)}}" height="50px" width="60px" alt="price comparison"></td>
                            <td>{{$single->name}}</td>
                            <td>{{$single->price}}</td>
                            <td><a href="/product-view-single/{{$request}}" class="pr-2"><i class="fa fa-eye text-info"></i></a>
                                <a href="/edit-product/{{$request}}" class="pr-2"><i class="fa fa-pencil text-warning"></i></a>
                                <a href="#" class="pr-2 deleteProduct" data-id="{{ $request }}"  data-token="{{ csrf_token() }}"><i class="fa fa-trash text-danger"></i></a></td>
                        </tr>
                            @php
                            $serial ++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="{{asset('js/admin/js/jquery.dataTables.min.js')}}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
            $(document).ready(function() {
                var table = $('#example').DataTable({
                    "columnDefs": [{
                        "visible": false,
                        "targets": 2
                    }],
                    "order": [
                        [2, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function(settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;
                        api.column(2, {
                            page: 'current'
                        }).data().each(function(group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                last = group;
                            }
                        });
                    }
                });
            });
        });
    </script>
    <script>
        $(".deleteProduct").click(function() {
            var del= confirm("Do you really want to delete this?");
            if (del){ var id = $(this).data("id");
                var token = $(this).data("token");
                $.ajax(
                    {
                        url: "/delete-product/" + id,
                        type: 'DELETE',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        success: function (response) {
                            $.toast({
                                heading: 'Success',
                                text: response.success,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'info',
                                hideAfter: 3500,
                                stack: 6
                            });
                            $('#id'+id).remove();
                        },
                        error: function (response) {
                            $.toast({
                                heading: 'Error',
                                text: response.responseJSON.error,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'error',
                                hideAfter: 3500,
                                stack: 6
                            })
                        }
                    })}

        })
    </script>
@endsection