@extends('admin.inc.master')
@section('title','FAQ')
@php
$active = "faq"
@endphp
@section('content')
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Faqs page</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="/addfaq" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">ADD NEW FAQ</a>
                    <ol class="breadcrumb">
                        <li><a href="/dashboard">Dashboard</a></li>
                        <li class="active">Faqs page</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group" role="tablist" aria-multiselectable="true">
                        @php
                        $content = 1;
                        @endphp
                        @foreach($obj as $objects)
                            @php
                                  $request = \Illuminate\Support\Facades\Crypt::encrypt($objects->id);
                            @endphp
                        <div class="panel panel-default" id="id{{$request}}">
                            <div class="panel-heading" role="tab" id="heading{{$content}}">
                                <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$content}}" aria-expanded="true" aria-controls="collapse{{$content}}}" class="font-bold">{{$objects->question}}</a> </h4> </div>
                            <div id="collapse{{$content}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$content}}">
                                <div class="panel-body">{{$objects->answer}}
                                    <hr>
                                    <a href="/edit-faq/{{$request}}"><i class="fa fa-pencil m-4"></i></a><a data-id="{{ $request }}"  href="#" data-token="{{ csrf_token() }}" class="deleteProduct"><i class="fa fa-trash m-4"></i></a>
                                </div>
                            </div>
                        </div>
                            @php
                            $content ++;
                            @endphp
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- .row -->
            <!-- /.row -->
        </div>

@endsection
@section('script')
    <script>
        $(".deleteProduct").click(function() {
            var del= confirm("Do you really want to delete this?");
            if (del){ var id = $(this).data("id");
                var token = $(this).data("token");
                $.ajax(
                    {
                        url: "/delete-faq/" + id,
                        type: 'DELETE',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        success: function (response) {
                            $.toast({
                                heading: 'Success',
                                text: response.success,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'info',
                                hideAfter: 3500,
                                stack: 6
                            });
                            $('#id'+id).remove();
                        },
                        error: function (response) {
                            $.toast({
                                heading: 'Error',
                                text: response.responseJSON.error,
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'error',
                                hideAfter: 3500,
                                stack: 6
                            })
                        }
                    })}

        })
    </script>
@endsection