<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('image/pluginimage/favicon.png')}}">
    <title>GettingRoom admin panel</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('css/admin/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/admin/css/bootstrap-extension.css')}}" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{asset('css/admin/css/animate.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('css/admin/css/style.css')}}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{asset('css/admin/css/blue.css')}}" id="theme" rel="stylesheet">
    <link href="{{asset('css/admin/css/jquery.toast.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
    <div class="login-box login-sidebar">
        <div class="white-box">
                <a href="javascript:void(0)" class="text-center db"><img src="{{asset('image/pluginimage/eliteadmin-logo-dark.png')}}" alt="Home" />
                    <br/><img src="{{asset('image/pluginimage/eliteadmin-text-dark.png')}}" alt="Home" /></a>
                <div class="form-group m-t-40">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" name="email" required="" placeholder="Username">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="password" required="" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox checkbox-primary pull-left p-t-0">
                            <input id="checkbox-signup" type="checkbox">
                            <label for="checkbox-signup"> Remember me </label>
                        </div>
                        <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-submit btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                    </div>
                </div>
            <form class="form-horizontal" id="recoverform" action="{{url('admin/reset-password')}}">
                <div class="form-group ">
                    <div class="col-xs-12">
                        <h3>Recover Password</h3>
                        <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="Email">
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- jQuery -->
<script src="{{asset('js/admin/js/jquery.min.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{asset('js/admin/js/tether.min.js')}}"></script>
<script src="{{asset('js/admin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/admin/js/bootstrap-extension.min.js')}}"></script>
<!-- Menu Plugin JavaScript -->
<script src="{{asset('js/admin/js/sidebar-nav.min.js')}}"></script>
<!--slimscroll JavaScript -->
<script src="{{asset('js/admin/js/jquery.slimscroll.js')}}"></script>
<!--Wave Effects -->
<script src="{{asset('js/admin/js/waves.js')}}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{asset('js/admin/js/custom.min.js')}}"></script>
<!--Style Switcher -->
<script src="{{asset('js/admin/js/jQuery.style.switcher.js')}}"></script>
<script src="{{asset('js/admin/js/jquery.toast.js')}}"></script>
<script>
    @if(session('success'))
    $.toast({
        heading: 'Success',
        text:'{{session('success')}}',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: 'info',
        hideAfter: 3500,
        stack: 6
    });
    @endif
    @if(session('error'))
    $.toast({
        heading: 'Error',
        text:'{{session('error')}}',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: 'info',
        hideAfter: 3500,
        stack: 6
    });
    @endif
</script>
<script>
    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }
    });

    $('.btn-submit').on('click',function (e) {
        e.preventDefault();
        var email = $("input[name=email]").val();
        var password = $("input[name=password]").val();
        var rememberMe = $('#checkbox-signup').is(':checked');
        $.ajax({
            url: '/admin/post/login',
            type: 'POST',
            data: {
                email : email,
                password : password,
                remember_me: rememberMe
            },
            success: function (response) {
                location.href="/admin/dashboard"
            },
            error: function (response) {

                $.toast({
                    heading: 'Error',
                    text: response.responseJSON.error,
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500,
                    stack: 6
                });
            }

        });
    });
</script>
</body>

</html>
