@extends('admin.inc.master')
@section('title','DASHBOARD')
@php
    $active = "category";
@endphp
@section('style')
    <link rel="stylesheet" href="{{asset('css/admin/css/bootstrap-wysihtml5.css')}}">
    <link href="{{asset('css/admin/css/bootstrap-tagsinput.css')}}" rel="stylesheet" />
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Edit Sub Category</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{asset('/dashboard')}}">Dashboard</a></li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        @php
        $enc = \Illuminate\Support\Facades\Crypt::encrypt($sub->id);
        @endphp
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <form>
                        {{csrf_field()}}
                        <div class="form-group"><input type="hidden" id="id" value="{{$enc}}" class="form-control"></div>
                        <div class="form-group"><label for="name">Name</label><input type="text" id="name" class="form-control" value="{{$sub->name}}"></div>
                        <div class="form-group"><label for="description">Description</label><textarea class="form-control" id="description" cols="30" rows="10">{{$sub->description}}</textarea></div>
                        <div class="form-group"><label for="meta-description">Meta Description</label><textarea class="form-control" id="meta-description" cols="30" rows="10">{{$sub->meta_description}}</textarea></div>
                        <div class="form-group"><label for="meta-tag">Meta tag</label><input type="text" class="form-control" id="meta-tag" value="{{$sub->meta_keywords}}"></div>
                        <div class="form-group"><label for="meta-author">Author</label><input type="text" value="{{$sub->meta_author}}" id="meta-author" class="form-control"></div>
                        <button type="submit" class="btn-submit btn btn-primary"> Edit</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
<script src="{{asset('js/admin/js/bootstrap-tagsinput.js')}}"></script>
<script>
    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }
    });

    $(".btn-submit").click(function(e){

        e.preventDefault();


        var names             =    $("#name").val();
        var ids               =    $("#id").val();
        var descriptions      =    $("#description").val();
        var metaDescriptions  =    $("#meta-description").val();
        var metaTags          =    $("#meta-tag").val();
        var metaAuthors       =    $("#meta-author").val();
        $.ajax({

            type:'POST',
            url:'/edit-subcatagory/',
            data:{
                name: names,
                id:   ids,
                description: descriptions,
                meta_tag:  metaTags,
                meta_author: metaAuthors,
                meta_description: metaDescriptions
            },
            success:function(response){
                $.toast({
                    heading: 'Success',
                    text: response.success,
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'info',
                    hideAfter: 3500,
                    stack: 6
                })
            },
            error: function (response) {
                $.toast({
                    heading: 'Error',
                    text: response.responseJSON.error,
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500,
                    stack: 6
                })
            }

        });



    });
</script>

@endsection