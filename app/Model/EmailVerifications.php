<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmailVerifications extends Model
{
    protected $fillable = ['user_id','verification_code','status'];
    protected $table = 'email_verifications';
}
