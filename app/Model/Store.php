<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable =['name','brand_logo','avatar','description'];
    protected $table = 'stores';
}
