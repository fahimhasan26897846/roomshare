<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PhoneVerifications extends Model
{
    protected $fillable = ['user_id','otp_code','status'];
    protected $table = 'phone_verifications';
}
