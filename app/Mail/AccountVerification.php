<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountVerification extends Mailable
{
    use Queueable, SerializesModels;
    public $activationCode;
    public $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($activationCode,$name)
    {
        $this->activationCode = $activationCode;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.activate-account')
                    ->subject(ucfirst($this->name).', please confirm you\'re email address');
    }
}
