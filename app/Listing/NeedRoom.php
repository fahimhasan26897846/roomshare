<?php

namespace App\Listing;

use Illuminate\Database\Eloquent\Model;

class NeedRoom extends Model
{
    protected $table = 'looking_for_rooms';
    protected $guarded = [];
}
