<?php

namespace App\Listing;

use Illuminate\Database\Eloquent\Model;

class OfferingApartment extends Model
{
    protected $table = "offering_apartments";
    protected $guarded = [];
}
