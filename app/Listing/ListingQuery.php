<?php

namespace App\Listing;


use Illuminate\Support\Facades\DB;

class ListingQuery
{
    public static function findAll($userId)
    {
        $needRooms = static::findNeedRooms($userId);
        $needApartments = static::findNeedApartments($userId);
        $offeringRooms = static::findOfferingRooms($userId);
        $offeringApartments = static::findOfferingApartments($userId);
        return array_merge($needRooms, $needApartments, $offeringRooms, $offeringApartments);
    }

    public static function findNeedRooms($userId)
    {
        return DB::table('looking_for_rooms')
            ->where('user_id', '=', $userId)
            ->get(['*','looking_for_rooms.id AS huda', DB::raw("'Flatmate' as type")])->toArray();
    }

    public static function findNeedApartments($userId)
    {
        return DB::table('looking_for_apartments')
            ->where('user_id', '=', $userId)
            ->get(['*','looking_for_apartments.id AS huda', DB::raw("'Tenant' as type")])->toArray();
    }

    public static function findOfferingRooms($userId)
    {
        return DB::table('offering_rooms')
            ->where('user_id', '=', $userId)
            ->get(['*','offering_rooms.id AS huda', DB::raw("'Flat' as type")])->toArray();
    }

    public static function findOfferingApartments($userId)
    {
        return DB::table('offering_apartments')
            ->where('user_id', '=', $userId)
            ->get(['*','offering_apartments.id AS huda', DB::raw("'Apartment' as type")])->toArray();
    }
}
