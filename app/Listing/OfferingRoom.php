<?php

namespace App\Listing;

use Illuminate\Database\Eloquent\Model;

class OfferingRoom extends Model
{
    protected $table = 'offering_rooms';
    protected $guarded = [];
}
