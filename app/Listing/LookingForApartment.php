<?php

namespace App\Listing;

use Illuminate\Database\Eloquent\Model;

class LookingForApartment extends Model
{
    protected $table = 'looking_for_apartments';
    protected $guarded = [];
}
