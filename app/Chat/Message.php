<?php

namespace App\Chat;

use App\Exceptions\CanNotCreateConversationException;
use App\Exceptions\CanNotMarkMessagesAsReadException;
use App\Exceptions\CanNotStoreMessageException;
use App\Exceptions\CanNotUpdateConversationException;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;

class Message
{
    public static function storeMessage($message, $from, $to)
    {
        $conversation = static::conversationExists($from, $to);
        $timestamp = (new \DateTime())->format('Y-m-d H:i:s');
        DB::beginTransaction();
        $success = DB::table('messages')
            ->insert([
                [
                    'message' => $message,
                    'from' => $from,
                    'to' => $to,
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp
                ]
            ]);
        if (! $success) {
            DB::rollback();
            throw new CanNotStoreMessageException();
        }
        if ($conversation) {
            $success = static::updateConversation($conversation->id, $timestamp);
            if (! $success) {
                DB::rollback();
                throw new CanNotUpdateConversationException();
            }
        } else {
            $success = static::createConversation($from, $to, $timestamp);
            if (! $success) {
                DB::rollback();
                throw new CanNotCreateConversationException();
            }
        }
        DB::commit();
    }

    private static function conversationExists($user, $client)
    {
        return DB::table('conversations')
            ->select('id')
            ->where([
                ['user1', '=', $user],
                ['user2', '=', $client],
            ])
            ->orWhere([
                ['user1', '=', $client],
                ['user2', '=', $user],
            ])
            ->first();
    }

    private static function createConversation($user, $client, $timestamp)
    {
        return DB::table('conversations')
            ->insert([
                [
                    'user1' => $user,
                    'user2' => $client,
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp
                ]
            ]);
    }

    private static function updateConversation($id, $timestamp)
    {
        return DB::table('conversations')
            ->where('id', '=', $id)
            ->update(['updated_at' => $timestamp]);
    }

    public static function getFullConversation($user, $client)
    {
        static::setMessagesAsRead($user, $client);

        return DB::table('messages as m')
            ->join('users as u', 'm.from', '=', 'u.id')
            ->where([
                ['m.from', '=', $user],
                ['m.to', '=', $client]
            ])
            ->orWhere([
                ['m.from', '=', $client],
                ['m.to', '=', $user]
            ])
            ->orderBy('m.created_at', 'asc')
            ->get();
    }

    public static function getLatestConversation($user, $client, $limit)
    {
        static::setMessagesAsRead($user, $client);

        return DB::table('messages as m')
            ->join('users as u', 'm.from', '=', 'u.id')
            ->where([
                ['m.from', '=', $user],
                ['m.to', '=', $client]
            ])
            ->orWhere([
                ['m.from', '=', $client],
                ['m.to', '=', $user]
            ])
            ->orderBy('m.created_at', 'asc')
            ->limit($limit)
            ->get();
    }

    public static function setMessagesAsRead($user, $client)
    {
        $timestamp = (new \DateTime())->format('Y-m-d H:i:s');
        return DB::table('messages')
            ->where([
                ['from', '=', $client],
                ['to', '=', $user],
                ['is_read', '=', FALSE]
            ])
            ->update([
                'is_read' => TRUE,
                'updated_at' => $timestamp
            ]);
    }

    public static function countUnreadConversations($client)
    {
        return DB::table('messages')
            ->select('from', 'to', DB::raw('COUNT(message) as messages'))
            ->where('to', '=', $client)
            ->groupBy('from')
            ->get();
    }

    public static function getConversationList($user)
    {
        return DB::table('conversations')
            ->where('user1', '=', $user)
            ->orWhere('user2', '=', $user)
            ->orderBy('updated_at', 'desc')
            ->get();
    }

    public static function findUnread($user, $client)
    {
        return DB::table('messages')
            ->where([
                ['from', '=', $client],
                ['to', '=', $user],
                ['is_read', '=', FALSE]
            ])
            ->count();

    }
}
