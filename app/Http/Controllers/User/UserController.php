<?php

namespace App\Http\Controllers\User;

use App\Contact;
use App\Model\Bookmark;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\User;
use DB;

class UserController extends Controller
{
    public function saveUser(Request $request){
        $id = Sentinel::getUser()->id;
        $user = User::find($id);
        if($request->get('day'))
        {
            $user->dob = $request->year.'-'.$request->month.'-'.$request->day;
        }
        if($request->get('tags'))
        {
            $user->language = implode(" ",$request->tags);
        }
        if($request->get('details'))
        {
            $user->details = $request->details;
        }
        if($request->get('hometown'))
        {
            $user->city = $request->hometown;
        }
        if($request->get('gender'))
        {
            $user->gender = $request->gender;
        }
        if($request->get('photo'))
        {
            $image = $request->get('photo');
            $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('photo'))->save(public_path('images/').$name);
            $user->avatar = '/images/'.$name;
        }
        $user->save();
    }
    public function getSingleUser($id){
        $user = User::find($id);
        return $user;
    }
    public function sendQuery(Request $request){
        $result = Contact::create([
            'user_id'=>Sentinel::getUser()->id,
            'type'=>'user',
            'subject'=>$request->subject,
            'message'=>$request->message
        ]);
        if ($result){
            return response()->json(['success'=>'Message sent']);
        }else{
            return response()->json(['error'=>'Message not sent'],500);
        }
    }
    public function unreadContact(){
        $user = Sentinel::getUser()->id;
        $unread = Contact::where(['user_id'=>$user,'type'=>'admin','read'=>0])->get();
        return count($unread);
    }
    public function unreadContactRead(){
        $user = Sentinel::getUser()->id;
        $unread = Contact::where(['user_id'=>$user,'type'=>'admin','read'=>0])
            ->update(['read'=>1]);
    }
    public function allContactsData(){
        $user = Sentinel::getUser()->id;
        $unread = Contact::where('user_id',$user)->get();
        return $unread;
    }
    public function addBookMark($id,$type){
        $user = Sentinel::getUser()->id;
        $existed = Bookmark::where(['user_id'=>$user,'listing_id'=>$id,'listing_type'=>$type])->get();
        if (count($existed)>0){
            $duplicate = Bookmark::where(['user_id'=>$user,'listing_id'=>$id])->first();
            $got = Bookmark::find($duplicate->id);
            $got->delete();
        }else{
            $result = Bookmark::create([
                'user_id'=>$user,
                'listing_id'=>$id,
                'listing_type'=>$type
            ]);
            return $result;
        }

    }
    public function usersBookmark(){
        $user = Sentinel::getUser()->id;
        $roommate = DB::table('bookmarks')
            ->where(['bookmarks.user_id'=>$user,'bookmarks.listing_type'=>'roommate'])
            ->join('looking_for_apartments', 'bookmarks.listing_id', '=', 'looking_for_apartments.id')
            ->join('users', 'looking_for_apartments.user_id', '=', 'users.id')
            ->select('users.*','users.id AS bedi','users.avatar AS avatar','looking_for_apartments.*','looking_for_apartments.id AS huda','looking_for_apartments.user_id AS pakna','bookmarks.*')
            ->get()->toArray();
        $tenant = DB::table('bookmarks')
            ->where(['bookmarks.user_id'=>$user,'bookmarks.listing_type'=>'tenant'])
            ->join('looking_for_rooms', 'bookmarks.listing_id', '=', 'looking_for_rooms.id')
            ->join('users', 'looking_for_rooms.user_id', '=', 'users.id')
            ->select('bookmarks.id AS id','users.*','users.id AS bedi','users.avatar AS avatar','looking_for_rooms.*','looking_for_rooms.id AS huda','bookmarks.*')
            ->get()->toArray();
        $rooms = DB::table('bookmarks')
            ->where(['bookmarks.user_id'=>$user,'bookmarks.listing_type'=>'room'])
            ->join('offering_rooms', 'bookmarks.listing_id', '=', 'offering_rooms.id')
            ->join('users', 'offering_rooms.user_id', '=', 'users.id')
            ->select('bookmarks.id AS id','users.*','users.id AS bedi','users.avatar AS avatar','offering_rooms.*','offering_rooms.id AS huda','bookmarks.*')
            ->get()->toArray();
        $apartments = DB::table('bookmarks')
            ->where(['bookmarks.user_id'=>$user,'bookmarks.listing_type'=>'apartment'])
            ->join('offering_apartments', 'bookmarks.listing_id', '=', 'offering_apartments.id')
            ->join('users', 'offering_apartments.user_id', '=', 'users.id')
            ->select('bookmarks.id AS id','users.*','users.id AS bedi','users.avatar AS avatar','offering_apartments.*','offering_apartments.id AS huda','bookmarks.*')
            ->get()->toArray();
        return array_merge($roommate, $tenant, $rooms, $apartments);
    }
    public function accountDeactivate(){
        $id= Sentinel::getUser()->id;
        $user = User::find($id);
        $user->active_status = 0;
        $user->save();
    }
    public function settingsResetPassword(Request $request){
        $hasher = Sentinel::getHasher();
        $oldPassword = $request->old_password;
        $password = $request->password;
        $passwordConf = $request->confirm_password;
        $user = Sentinel::getUser();
        if (!$hasher->check($oldPassword, $user->password) || $password != $passwordConf) {
            return response()->json(['error'=>'Invalid data'],500);
        }
        Sentinel::update($user, array('password' => $password));
    }
}
