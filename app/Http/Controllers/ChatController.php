<?php

namespace App\Http\Controllers;

use App\Chat\Message;
use App\Events\NewMessage;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function loadChatRoom($channel)
    {
        $clientId = (int)(\explode('-', $channel))[2];
        $user = Sentinel::getUser();
        $client = Sentinel::findById($clientId);
        if ($user && $client) {
            $userChannel = "p-c-{$user->id}";
            $clientChannel = "p-c-{$client->id}";
            return view('test-chat')
                ->with('userChannel', $userChannel)
                ->with('clientChannel', $clientChannel);
        }
        return redirect()->back();
    }

    public function fetchConversation($channel)
    {
        $clientId = (int)(\explode('-', $channel))[2];
        $user = Sentinel::getUser();
        $client = Sentinel::findById($clientId);
        $conversation = [];
        if ($user && $client) {
            try {
                $conversation = Message::getFullConversation($user->id, $client->id);
            } catch (\Exception $e) {
                return $e; //DEV::TESTING
            }
        }
        $response = [
            'conversation' => $conversation,
            'user' => $user,
            'client' => $client
        ];
        return response()->json($response);
    }

    public function sendMessage(Request $request)
    {
        $clientId = (int)(\explode('-', $request->get('channel')))[2];
        $user = Sentinel::getUser();
        $client = Sentinel::findById($clientId);
        if ($user && $client) {
            $clientChannel = "p-c-{$client->id}";
            try {
                Message::storeMessage($request->get('message'), $user->id, $client->id);
                \event(new NewMessage($request->get('message'), $clientChannel, $user, $client));
                return 'Message sent!';
            } catch(\Exception $e) {
                return $e;
            }
        }
        return 'Message could not be sent!';
    }

    public function messageReceived(Request $request)
    {
        $clientId = (int)(\explode('-', $request->get('channel')))[2];
        $user = Sentinel::getUser();
        $client = Sentinel::findById($clientId);
        if ($user && $client) {
            try {
                Message::setMessagesAsRead($user->id, $client->id);
                return $this->fetchConversationList();
            } catch(\Exception $e) {
                return $e;
            }
        }
        return 'Message could not be marked as read';
    }

    public function fetchConversationList()
    {
        $conversations = [];
        $list = [];
        $user = Sentinel::getUser();
        if ($user) {
            $list = Message::getConversationList($user->id);
        }
        if (! $list) { return response()->json($conversations); }
        foreach ($list as $conversation) {
            if ($conversation->user1 === $user->id) {
                $client = Sentinel::findById($conversation->user2);
                $hasUnread = Message::findUnread($user->id, $client->id);
            } elseif ($conversation->user2 === $user->id) {
                $client = Sentinel::findById($conversation->user1);
                $hasUnread = Message::findUnread($user->id, $client->id);
            } else {
                continue;
            }
            $conversations[] = [
                'id' => $client->id,
                'first_name' => $client->first_name,
                'last_name' => $client->last_name,
                'avatar' => $client->avatar,
                'has_unread' => $hasUnread,
                'channel' => "p-c-{$client->id}"
            ];
        }
        return response()->json($conversations);
    }
}
