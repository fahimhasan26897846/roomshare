<?php

namespace App\Http\Controllers\Affiliate;

use App\Model\AffiliateUser;
use App\Notifications\NewAffiliaterNotification;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;

class UserController extends Controller
{
    public function login(){return view('affiliate.login');}
    public function postLogin(Request $request){
        return $request->all();
    }
    public function register(){return view('affiliate.register');}
    public function postRegister(Request $request){
        $applicant = AffiliateUser::where('email',$request->email)->first();
        if($applicant != null){
            return redirect()->back()->with(['error'=>'Email exists. Try another one']);

        }
        $url = time().'/affiliate/'.rand(2236,3647);
        $result = AffiliateUser::create([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
            'phone'=>$request->phone_number,
            'promote_url'=> $url
        ]);
        $superAdmins = $roles = Role::find(1)->users()->get();
        if ($result){
            $link = 'admin/applicants';
            foreach ($superAdmins as $super){
                User::find($super->id)->notify(new NewAffiliaterNotification($link));
            }
            return redirect('affiliate/login');
        }
    }
    public function home(){return view('affiliate.home');}
    public function campaigns(){return view('affiliate.campaigns');}
    public function remote(){return view('affiliate.campaigns');}
    public function rates(){return view('affiliate.rates');}
    public function notifications(){return view('affiliate.notifications');}
    public function support(){return view('affiliate.support');}
    public function settings(){return view('affiliate.settings');}
    public function financial(){return view('affiliate.financial');}
    public function logout(){}
    public function promote(){return view('affiliate.promote');}
}