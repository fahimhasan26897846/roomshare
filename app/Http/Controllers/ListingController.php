<?php

namespace App\Http\Controllers;

use App\Listing\LookingForApartment;
use App\Listing\NeedRoom;
use App\Listing\OfferingApartment;
use App\Listing\OfferingRoom;
use App\Listing\ListingQuery;
use App\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use DB;
use File;

class ListingController extends Controller
{
    public function processNeedRoomForm(Request $request)
    {
        $data = $this->validate($request, [
            'headline'          => 'nullable',
            'location'          => 'required',
            'latitude'          => 'nullable',
            'longitude'         => 'nullable',
            'rental_currency'   => 'required|in:AED,ARS,AUD,BRL,CAD,CHF,CNY,CZK,DKK,EUR,GBP,HKD,HUF,IDR,ILS,INR,JPY,KRW,MXN,MYR,NOK,NZD,PHP,PLN,RUB,SEK,SGD,THB,TRY,TWD,UAH,USD,VND,ZAR',
            'monthly_rent'      => 'required|numeric',
            'is_short_term'     => 'required|boolean',
            'move_date'         => 'required|date',
            'leave_date'        => 'nullable|date',
            'description'       => 'nullable',
            'cleanliness'       => 'nullable|in:Clean,Average,Messy',
            'overnight_guests'  => 'nullable|in:Never,Rarely,Occasionally,Often',
            'party_habits'      => 'nullable|in:Rarely,Occasionally,Weekends,Daily',
            'get_up'            => 'nullable|in:Before 6AM,6AM - 8AM,8AM - 10AM,10AM - 12AM,After 12AM',
            'go_to_bed'         => 'nullable|in:Before 8PM,8PM - 10PM,10PM - 12AM,12AM - 2AM,After 2AM',
            'food_preference'   => 'nullable|in:Almost Anything,Fish Only,Vegetarian,Vegan,Kosher',
            'smoker'            => 'nullable|in:No,Yes,Outside Only',
            'work_schedule'     => 'nullable|in:Professional (9-5),Student,Work at Night,Work at Home,Mixed',
            'occupation'        => 'nullable|in:Accounting,Administration/Secretarial,Advertising,Architecture/Design,Automotive/Mechanical,Aviation/Aeronautical,Beauty/Fashion,Biology/Life Science,Broadcasting/Publishing,Consulting,Customer Service/Support,Creative/Media,Education/Tranining,Engineering,Finance/Banking,Government/Social Service,Health Services,Hospitality,Human Resource/Recruiting,IT/Computers,Insurance,Internet/Media,Law Enforcement/Security,Legal,Manufacturing/Production,Marketing/Public Relations,Military,Non-Profit,Other,Physical Science/Environment,Real Estate,Retail,Sales,Sports/Fitness,Student/Graduate,Student/Undergraduate,Telecommunications,Trades/Labor,Transportation/Logistic',
            'prefer_min_age'    => 'required|numeric|between:18,99',
            'prefer_max_age'    => 'required|numeric|between:18,99',
            'prefer_smoker'     => 'nullable|in:No,Yes,Outside Only',
            'prefer_student'    => 'nullable|boolean',
            'have_cats'         => 'nullable|boolean',
            'have_dogs'         => 'nullable|boolean',
            'have_small_pets'   => 'nullable|boolean',
            'have_birds'        => 'nullable|boolean',
            'have_fish'         => 'nullable|boolean',
            'have_reptiles'     => 'nullable|boolean',
            'prefer_cats'       => 'nullable|boolean',
            'prefer_dogs'       => 'nullable|boolean',
            'prefer_small_pets' => 'nullable|boolean',
            'prefer_birds'      => 'nullable|boolean',
            'prefer_fish'       => 'nullable|boolean',
            'prefer_reptiles'   => 'nullable|boolean',
        ]);

        if ($currentUser = Sentinel::getUser()) {
            $data['user_id'] = $currentUser->id;
        }
        $nid = Sentinel::getUser()->id;
        $use = User::find($nid);
        $use->new_user = 1;
        $use->save();

        try {
            NeedRoom::create($data);
            return response()->json(['status' => 'succeed', 'code' => 200]);
        } catch (\Exception $e) {
            return $e;
        }
    }
    public function processOfferingRoomForm(Request $request)
    {
        $data = $this->validate($request, [
            'headline'          => 'nullable',
            'location'          => 'required',
            'latitude'          => 'nullable',
            'longitude'         => 'nullable',
            'rental_currency'   => 'required|in:AED,ARS,AUD,BRL,CAD,CHF,CNY,CZK,DKK,EUR,GBP,HKD,HUF,IDR,ILS,INR,JPY,KRW,MXN,MYR,NOK,NZD,PHP,PLN,RUB,SEK,SGD,THB,TRY,TWD,UAH,USD,VND,ZAR',
            'monthly_rent'      => 'required|numeric',
            'is_short_term'     => 'required|boolean',
            'move_date'         => 'required|date',
            'leave_date'        => 'nullable|date',
            'minimum_stay'      => 'nullable|in:1 night,2 nights,3 nights,4 nights,5 nights,6 nights,1 week,2 weeks,3 weeks,1 month,2 months,3 months,4 months,5 months,6 months,7 months,8 months,9 months,10 months,11 months,12 months',
            'description'       => 'nullable',
            'building_type'     => 'nullable|in:Apartment Building (1-10 units),Apartment Building (10+ units),Apartment Complex,House',
            'move_in_fee'       => 'nullable|numeric',
            'utilities_cost'    => 'nullable|numeric',
            'parking_rent'      => 'nullable|numeric',
            'is_furnished'      => 'nullable|boolean',
            'have_air_conditioning' => 'nullable|boolean',
            'have_deck_or_patio'    => 'nullable|boolean',
            'have_wood_floors'  => 'nullable|boolean',
            'have_yard'         => 'nullable|boolean',
            'have_carpet'       => 'nullable|boolean',
            'have_storage'      => 'nullable|boolean',
            'have_gym'          => 'nullable|boolean',
            'have_parking'      => 'nullable|boolean',
            'have_elevator'     => 'nullable|boolean',
            'have_fireplace'    => 'nullable|boolean',
            'have_laundry'      => 'nullable|boolean',
            'have_tennis'       => 'nullable|boolean',
            'have_jacuzzi'      => 'nullable|boolean',
            'have_dishwasher'   => 'nullable|boolean',
            'have_pool'         => 'nullable|boolean',
            'have_private_bathroom' => 'nullable|boolean',
            'have_phone_jack'   => 'nullable|boolean',
            'have_private_entrance' => 'nullable|boolean',
            'have_cable_tv_jack'    => 'nullable|boolean',
            'have_private_closet'   => 'nullable|boolean',
            'have_internet'     => 'nullable|boolean',
            'have_wireless_internet' => 'nullable|boolean',
            'min_household_age' => 'nullable|numeric|between:18,99',
            'max_household_age' => 'nullable|numeric|between:18,99',
            'people_in_household'   => 'nullable|numeric',
            'household_sex'     => 'nullable|in:Female,Male,Co-ed',
            'cleanliness'       => 'nullable|in:Clean,Average,Messy',
            'overnight_guests'  => 'nullable|in:Never,Rarely,Occasionally,Often',
            'party_habits'      => 'nullable|in:Rarely,Occasionally,Weekends,Daily',
            'get_up'            => 'nullable|in:Before 6AM,6AM - 8AM,8AM - 10AM,10AM - 12AM,After 12AM',
            'go_to_bed'         => 'nullable|in:Before 8PM,8PM - 10PM,10PM - 12AM,12AM - 2AM,After 2AM',
            'food_preference'   => 'nullable|in:Almost Anything,Fish Only,Vegetarian,Vegan,Kosher',
            'smoker'            => 'nullable|in:No,Yes,Outside Only',
            'work_schedule'     => 'nullable|in:Professional (9-5),Student,Work at Night,Work at Home,Mixed',
            'occupation'        => 'nullable|in:Accounting,Administration/Secretarial,Advertising,Architecture/Design,Automotive/Mechanical,Aviation/Aeronautical,Beauty/Fashion,Biology/Life Science,Broadcasting/Publishing,Consulting,Customer Service/Support,Creative/Media,Education/Tranining,Engineering,Finance/Banking,Government/Social Service,Health Services,Hospitality,Human Resource/Recruiting,IT/Computers,Insurance,Internet/Media,Law Enforcement/Security,Legal,Manufacturing/Production,Marketing/Public Relations,Military,Non-Profit,Other,Physical Science/Environment,Real Estate,Retail,Sales,Sports/Fitness,Student/Graduate,Student/Undergraduate,Telecommunications,Trades/Labor,Transportation/Logistic',
            'prefer_min_age'    => 'required|numeric|between:18,99',
            'prefer_max_age'    => 'required|numeric|between:18,99',
            'prefer_smoker'     => 'nullable|in:No,Yes,Outside Only',
            'prefer_student'    => 'nullable|boolean',
            'have_cats'         => 'nullable|boolean',
            'have_dogs'         => 'nullable|boolean',
            'have_small_pets'   => 'nullable|boolean',
            'have_birds'        => 'nullable|boolean',
            'have_fish'         => 'nullable|boolean',
            'have_reptiles'     => 'nullable|boolean',
            'prefer_cats'       => 'nullable|boolean',
            'prefer_dogs'       => 'nullable|boolean',
            'prefer_small_pets' => 'nullable|boolean',
            'prefer_birds'      => 'nullable|boolean',
            'prefer_fish'       => 'nullable|boolean',
            'prefer_reptiles'   => 'nullable|boolean',
        ]);

        if ($currentUser = Sentinel::getUser()) {
            $data['user_id'] = $currentUser->id;
        }
        $nid = Sentinel::getUser()->id;
        $use = User::find($nid);
        $use->new_user = 1;
        $use->save();
        if($request->input('photos')){
            $try = [];
            foreach($request->input('photos') as $photo){
                $image = $photo;
                $name = time().'.' .rand(1,9).'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($photo)->resize(400, 300)->save(public_path('images/').$name);
                array_push($try,'/images/'.$name);
            }
            $data['photos'] =   implode(" ",$try);
        }
        try {
            OfferingRoom::create($data);
            return response()->json(['status' => 'succeed', 'code' => 200]);
        } catch (\Exception $e) {
            return $e;
        }
    }
    public function processNeedApartmentForm(Request $request)
    {
        $data = $this->validate($request, [
            'headline'          => 'nullable',
            'location'          => 'required',
            'latitude'          => 'nullable',
            'longitude'         => 'nullable',
            'rental_currency'   => 'required|in:AED,ARS,AUD,BRL,CAD,CHF,CNY,CZK,DKK,EUR,GBP,HKD,HUF,IDR,ILS,INR,JPY,KRW,MXN,MYR,NOK,NZD,PHP,PLN,RUB,SEK,SGD,THB,TRY,TWD,UAH,USD,VND,ZAR',
            'monthly_rent'      => 'required|numeric',
            'is_short_term'     => 'required|boolean',
            'move_date'         => 'required|date',
            'leave_date'        => 'nullable|date',
            'description'       => 'nullable',
            'is_studio'         => 'nullable|boolean',
            'is_one_bed'        => 'nullable|boolean',
            'is_two_bed'        => 'nullable|boolean',
            'is_three_bed'      => 'nullable|boolean',
            'is_four_bed'       => 'nullable|boolean',
            'is_five_bed'       => 'nullable|boolean',
            'is_furnished'      => 'nullable|boolean',
            'is_high_rise'      => 'nullable|boolean',
            'is_low_rise'       => 'nullable|boolean',
            'have_disability_access' => 'nullable|boolean',
            'have_doorman'      => 'nullable|boolean',
            'have_elevator'     => 'nullable|boolean',
            'have_walkup'       => 'nullable|boolean',
            'have_health_club'  => 'nullable|boolean',
            'have_laundromat'   => 'nullable|boolean',
            'have_covered_parking'  => 'nullable|boolean',
            'have_garage'       => 'nullable|boolean',
            'have_parking_lot'  => 'nullable|boolean',
            'have_street_parking'   => 'nullable|boolean',
            'have_near_bus_stop'    => 'nullable|boolean',
            'have_near_subway'  => 'nullable|boolean',
            'have_electronic_security' => 'nullable|boolean',
            'have_security'     => 'nullable|boolean',
            'have_swimming_pool'    => 'nullable|boolean',
            'have_internet'     => 'nullable|boolean',
            'have_wireless_internet' => 'nullable|boolean'
        ]);

        if ($currentUser = Sentinel::getUser()) {
            $data['user_id'] = $currentUser->id;
        }
        $nid = Sentinel::getUser()->id;
        $use = User::find($nid);
        $use->new_user = 1;
        $use->save();

        try {
            LookingForApartment::create($data);
            return response()->json(['status' => 'succeed', 'code' => 200]);
        } catch (\Exception $e) {
            return $e;
        }
    }
    public function processOfferingApartmentForm(Request $request)
    {
        $data = $this->validate($request, [
            'headline'          => 'nullable',
            'location'          => 'required',
            'latitude'          => 'nullable',
            'longitude'         => 'nullable',
            'rental_currency'   => 'required|in:AED,ARS,AUD,BRL,CAD,CHF,CNY,CZK,DKK,EUR,GBP,HKD,HUF,IDR,ILS,INR,JPY,KRW,MXN,MYR,NOK,NZD,PHP,PLN,RUB,SEK,SGD,THB,TRY,TWD,UAH,USD,VND,ZAR',
            'monthly_rent'      => 'required|numeric',
            'is_short_term'     => 'required|boolean',
            'move_date'         => 'required|date',
            'leave_date'        => 'nullable|date',
            'minimum_stay'      => 'nullable|in:1 night,2 nights,3 nights,4 nights,5 nights,6 nights,1 week,2 weeks,3 weeks,1 month,2 months,3 months,4 months,5 months,6 months,7 months,8 months,9 months,10 months,11 months,12 months',
            'description'       => 'nullable',
            'bedrooms'          => 'nullable|in:Studio,1 bedroom,2 bedrooms,3 bedrooms,4 bedrooms,5 bedrooms',
            'bathrooms'         => 'nullable|in:1,1.5,2,2.5,3,3.5+',
            'measurement'       => 'nullable|numeric',
            'measurement_unit'  => 'nullable|in:Square meter,Square feet',
            'is_furnished'      => 'nullable|boolean',
            'is_high_rise'      => 'nullable|boolean',
            'is_low_rise'       => 'nullable|boolean',
            'have_disability_access' => 'nullable|boolean',
            'have_doorman'      => 'nullable|boolean',
            'have_elevator'     => 'nullable|boolean',
            'have_walkup'       => 'nullable|boolean',
            'have_health_club'  => 'nullable|boolean',
            'have_laundromat'   => 'nullable|boolean',
            'have_covered_parking'  => 'nullable|boolean',
            'have_garage'       => 'nullable|boolean',
            'have_parking_lot'  => 'nullable|boolean',
            'have_street_parking'   => 'nullable|boolean',
            'have_near_bus_stop'    => 'nullable|boolean',
            'have_near_subway'  => 'nullable|boolean',
            'have_electronic_security' => 'nullable|boolean',
            'have_security'     => 'nullable|boolean',
            'have_swimming_pool'    => 'nullable|boolean',
            'have_internet'     => 'nullable|boolean',
            'have_wireless_internet' => 'nullable|boolean'
        ]);

        if ($currentUser = Sentinel::getUser()) {
            $data['user_id'] = $currentUser->id;
        }
        $nid = Sentinel::getUser()->id;
        $use = User::find($nid);
        $use->new_user = 1;
        $use->save();
        if($request->input('photos')){
            $try = [];
            foreach($request->input('photos') as $photo){
                $image = $photo;
                $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($photo)->save(public_path('images/').$name);
//                $user->avatar = '/images/'.$name;
                array_push($try,'/images/'.$name);
            }
            $data['photos'] =   implode(" ",$try);
        }

        try {
            OfferingApartment::create($data);
            return response()->json(['status' => 'succeed', 'code' => 200]);
        } catch (\Exception $e) {
            return $e;
        }
    }
    public function getListings(Request $request)
    {
        $currentUser = Sentinel::getUser();

        $dateOfBirth = $currentUser->dob;
        $currentUser->age = Carbon::parse($dateOfBirth)->age;

        if ($currentUser) {
            return [
                'user' => $currentUser,
                'listings' => ListingQuery::findAll($currentUser->id)
            ];
        }

        return 'Error! No user logged!';
    }
    public function needSingleRoom($id){
        $room = DB::table('looking_for_rooms')
            ->where('looking_for_rooms.id',$id)
            ->join('users', 'looking_for_rooms.user_id', '=', 'users.id')
            ->select('looking_for_rooms.*','looking_for_rooms.id AS moja','users.first_name AS hati','users.id AS bedi','users.avatar AS avatar','users.gender as genu','users.dob AS dob')
            ->get();
        return $room;
    }
    public function needApartmentSingle($id){
        $apartment = DB::table('looking_for_apartments')
            ->where('looking_for_apartments.id',$id)
            ->join('users', 'looking_for_apartments.user_id', '=', 'users.id')
            ->select('looking_for_apartments.*','looking_for_apartments.id AS moja','users.id AS bedi','users.first_name AS hati','users.avatar AS avatar','users.gender as genu','users.dob AS dob')
            ->get();
        return $apartment;
    }
    public function needApartmentAll($place, $min, $max,$sort){
        if($place == 'anywhere'){
            $room = DB::table('looking_for_apartments')
                ->join('users', 'looking_for_apartments.user_id', '=', 'users.id')
                ->where('users.active_status',1)
                ->select('looking_for_apartments.*','looking_for_apartments.id AS moja','users.id AS bedi','users.first_name AS hati','users.avatar AS avatar','users.gender as genu','users.dob AS dob')
                ->whereBetween('looking_for_apartments.monthly_rent', array($min, $max))
                ->when($sort,function ($query,$sort){
                    if ($sort == 'newest'){
                        return $query->orderBy('looking_for_apartments.id', 'desc');
                    }elseif($sort == 'activity'){
                        return $query->orderBy('looking_for_apartments.updated_at','desc');
                    }else{
                        return $query->orderBy('looking_for_apartments.monthly_rent','asc');
                    }
                })
                ->get();
            return $room;
        }
        else{
            $room = DB::table('looking_for_apartments')
                ->join('users', 'looking_for_apartments.user_id', '=', 'users.id')
                ->where('users.active_status',1)
                ->select('looking_for_apartments.*','users.id AS bedi','users.first_name AS hati','users.avatar AS avatar','users.gender as genu','users.dob AS dob')
                ->whereBetween('monthly_rent', array($min, $max))
                ->orderBy('looking_for_apartments.id', 'desc')
                ->where('looking_for_apartments.location','LIKE','%'.$place.'%')
                ->when($sort,function ($query,$sort){
                    if ($sort == 'newest'){
                        return $query->orderBy('looking_for_apartments.id', 'desc');
                    }elseif($sort == 'activity'){
                        return $query->orderBy('looking_for_apartments.updated_at','desc');
                    }else{
                        return $query->orderBy('looking_for_apartments.monthly_rent','asc');
                    }
                })
                ->get();
            return $room;
        }
    }
    public function needAllRoom($place, $min, $max,$sort){

        if($place == 'anywhere'){
            $room = DB::table('looking_for_rooms')
                ->join('users', 'looking_for_rooms.user_id', '=', 'users.id')
                ->where('users.active_status',1)
                ->select('looking_for_rooms.*','users.id AS bedi','users.first_name AS hati','users.avatar AS avatar','users.gender as genu','users.dob AS dob')
                ->whereBetween('monthly_rent', array($min, $max))
                ->when($sort,function ($query,$sort){
                    if ($sort == 'newest'){
                        return $query->orderBy('looking_for_rooms.id', 'desc');
                    }elseif($sort == 'activity'){
                        return $query->orderBy('looking_for_rooms.updated_at','desc');
                    }else{
                        return $query->orderBy('looking_for_rooms.monthly_rent','asc');
                    }
                })
                ->get();
            return $room;
        }
        else{
            $room = DB::table('looking_for_rooms')
                ->join('users', 'looking_for_rooms.user_id', '=', 'users.id')
                ->where('users.active_status',1)
                ->select('looking_for_rooms.*','users.id AS bedi','users.first_name AS hati','users.avatar AS avatar','users.gender as genu','users.dob AS dob')
                ->whereBetween('monthly_rent', array($min, $max))
                ->orderBy('looking_for_rooms.id', 'desc')
                ->where('looking_for_rooms.location','LIKE','%'.$place.'%')
                ->when($sort,function ($query,$sort){
                    if ($sort == 'newest'){
                        return $query->orderBy('looking_for_rooms.id', 'desc');
                    }elseif($sort == 'activity'){
                        return $query->orderBy('looking_for_rooms.updated_at','desc');
                    }else{
                        return $query->orderBy('looking_for_rooms.monthly_rent','asc');
                    }
                })
                ->get();
            return $room;
        }
    }
    public function needaRoomList($place, $min, $max,$sort){
        if($place == 'anywhere'){
            $aroom = DB::table('offering_rooms')
                ->join('users', 'offering_rooms.user_id', '=', 'users.id')
                ->where('users.active_status',1)
                ->select('offering_rooms.*','users.id AS bedi','users.first_name AS hati','users.avatar AS avatar','users.gender as genu','users.dob AS dob')
                ->whereBetween('offering_rooms.monthly_rent', array($min, $max))
                ->when($sort,function ($query,$sort){
                    if ($sort == 'newest'){
                        return $query->orderBy('offering_rooms.id', 'desc');
                    }elseif($sort == 'activity'){
                        return $query->orderBy('offering_rooms.updated_at','desc');
                    }else{
                        return $query->orderBy('offering_rooms.monthly_rent','asc');
                    }
                })
                ->get();
            return $aroom;
        }
        else{
            $aroom = DB::table('offering_rooms')
                ->join('users', 'offering_rooms.user_id', '=', 'users.id')
                ->where('users.active_status',1)
                ->select('offering_rooms.*','users.id AS bedi','users.first_name AS hati','users.avatar AS avatar','users.gender as genu','users.dob AS dob')
                ->where('offering_rooms.location','LIKE','%'.$place.'%')
                ->whereBetween('monthly_rent', array($min, $max))
                ->when($sort,function ($query,$sort){
                    if ($sort == 'newest'){
                        return $query->orderBy('offering_rooms.id', 'desc');
                    }elseif($sort == 'activity'){
                        return $query->orderBy('offering_rooms.updated_at','desc');
                    }else{
                        return $query->orderBy('offering_rooms.monthly_rent','asc');
                    }
                })
                ->get();
            return $aroom;
        }
    }
    public function anEntirePlaceList($place, $min, $max, $sort){

        if($place == 'anywhere'){
            $entireplace = DB::table('offering_apartments')
                ->join('users', 'offering_apartments.user_id', '=', 'users.id')
                ->where('users.active_status',1)
                ->select('offering_apartments.*','users.id AS bedi','users.first_name AS hati','users.avatar AS avatar','users.gender as genu','users.dob AS dob')
                ->whereBetween('monthly_rent', array($min, $max))
                ->when($sort,function ($query,$sort){
                    if ($sort == 'newest'){
                        return $query->orderBy('offering_apartments.id', 'desc');
                    }elseif($sort == 'activity'){
                        return $query->orderBy('offering_apartments.updated_at','desc');
                    }else{
                        return $query->orderBy('offering_apartments.monthly_rent','asc');
                    }
                })
                ->get();
            return $entireplace;
        }
        else{
            $entireplace = DB::table('offering_apartments')
                ->join('users', 'offering_apartments.user_id', '=', 'users.id')
                ->where('users.active_status',1)
                ->select('offering_apartments.*','users.first_name AS hati','users.avatar AS avatar','users.gender as genu','users.dob AS dob')
                ->where('offering_apartments.location','LIKE','%'.$place.'%')
                ->whereBetween('monthly_rent', array($min, $max))
                ->when($sort,function ($query,$sort){
                    if ($sort == 'newest'){
                        return $query->orderBy('offering_apartments.id', 'desc');
                    }elseif($sort == 'activity'){
                        return $query->orderBy('offering_apartments.updated_at','desc');
                    }else{
                        return $query->orderBy('offering_apartments.monthly_rent','asc');
                    }
                })
                ->get();
            return $entireplace;
        }
    }
    public function needaRoomSingle($id){
        $aroom = DB::table('offering_rooms')
            ->where('offering_rooms.id',$id)
            ->join('users', 'offering_rooms.user_id', '=', 'users.id')
            ->select('offering_rooms.*','offering_rooms.id AS moja','users.id AS bedi','users.first_name AS hati','users.avatar AS avatar','users.gender as genu','users.dob AS dob')
            ->get();
        return $aroom;
    }
    public function anEntirePlaceSingle($id){
        $entireplace = DB::table('offering_apartments')
            ->where('offering_apartments.id',$id)
            ->join('users', 'offering_apartments.user_id', '=', 'users.id')
            ->select('offering_apartments.*','offering_apartments.id AS moja','users.id AS bedi','users.first_name AS hati','users.avatar AS avatar','users.gender as genu','users.dob AS dob')
            ->get();
        return $entireplace;
    }
    public function uploadPhotos(Request $request){
        $objFile =    $request->file('file');
        $audio =  time().$objFile->getClientOriginalName();
        @mkdir('image');
        $destinationPath = 'image';
        $objFile->move($destinationPath,$audio);

    }
    public function citySort($id){
        $room = DB::table('looking_for_rooms')
            ->join('users', 'looking_for_rooms.user_id', '=', 'users.id')
            ->select('looking_for_rooms.*','users.first_name AS hati','users.avatar AS avatar','users.gender as genu','users.dob AS dob')
            ->where('looking_for_rooms.location',$id)
            ->orderBy('looking_for_rooms.id', 'desc')
            ->get();
        return $room;
    }

    public function needPlace($place){
        if($place == 'anywhere'){
            $location = ["lat"=>37.090,"lng"=>95.7129];
            $aroom = json_encode($location);
            return $aroom;
        }
        else{
            $aroom = DB::table('offering_rooms')
                ->select('offering_rooms.latitude AS lat','offering_rooms.longitude AS lng')
                ->where('offering_rooms.location','LIKE','%'.$place.'%')
                ->first();
            if(is_null($aroom)){
                $location = ["lat"=>37.090,"lng"=>95.7129];
                $aroom = json_encode($location);
                return $aroom;
            }
            return response()->json($aroom);
        }
    }
    public function needEntirePlace($place){
        if($place == 'anywhere'){
            $location = ["lat"=>37.090,"lng"=>95.7129];
            $aroom = json_encode($location);
            return $aroom;
        }
        else{
            $aroom = DB::table('offering_apartments')
                ->select('offering_apartments.latitude AS lat','offering_apartments.longitude AS lng')
                ->where('offering_apartments.location','LIKE','%'.$place.'%')
                ->first();
            if(is_null($aroom)){
                $location = ["lat"=>37.090,"lng"=>95.7129];
                $aroom = json_encode($location);
                return $aroom;
            }
            return response()->json($aroom);
        }
    }
    public function needRoommatePlace($place){
        if($place == 'anywhere'){
            $location = ["lat"=>37.090,"lng"=>95.7129];
            $aroom = json_encode($location);
            return $aroom;
        }
        else{
            $aroom = DB::table('looking_for_rooms')
                ->select('looking_for_rooms.latitude AS lat','looking_for_rooms.longitude AS lng')
                ->where('looking_for_rooms.location','LIKE','%'.$place.'%')
                ->first();
            if(is_null($aroom)){
                $location = ["lat"=>37.090,"lng"=>95.7129];
                $aroom = json_encode($location);
                return $aroom;
            }
            return response()->json($aroom);
        }
    }
    public function needTenantPlace($place){
        if($place == 'anywhere'){
            $location = ["lat"=>37.090,"lng"=>95.7129];
            $aroom = json_encode($location);
            return $aroom;
        }
        else{
            $aroom = DB::table('looking_for_apartments')
                ->select('looking_for_apartments.latitude AS lat','looking_for_apartments.longitude AS lng')
                ->where('looking_for_apartments.location','LIKE','%'.$place.'%')
                ->first();
            if(is_null($aroom)){
                $location = ["lat"=>37.090,"lng"=>95.7129];
                $aroom = json_encode($location);
                return $aroom;
            }
            return response()->json($aroom);
        }
    }


    public function offeringRoomMarkers(){
        $aroom = DB::table('offering_rooms')
            ->select('offering_rooms.latitude AS lat','offering_rooms.longitude AS lng')
            ->get();
        return response()->json($aroom);
    }
    public function offeringRoommateMarkers(){
        $aroom = DB::table('looking_for_rooms')
            ->select('looking_for_rooms.latitude AS lat','looking_for_rooms.longitude AS lng')
            ->get();
        return response()->json($aroom);
    }
    public function offeringEntireplaceMarkers(){
        $aroom = DB::table('offering_apartments')
            ->select('offering_apartments.latitude AS lat','offering_apartments.longitude AS lng')
            ->get();
        return response()->json($aroom);
    }
    public function offeringTenantMarkers(){
        $aroom = DB::table('looking_for_apartments')
            ->select('looking_for_apartments.latitude AS lat','looking_for_apartments.longitude AS lng')
            ->get();
        return response()->json($aroom);
    }

    public function needRoomDelete($id){
        $room = OfferingRoom::find($id);
        $room->delete();
        return redirect('/myprofile');
    }

    public function needApartmentDelete($id){
        $room = OfferingApartment::find($id);
        $room->delete();
        return redirect('/myprofile');
    }

    public function needRoommateDelete($id){
        $room = NeedRoom::find($id);
        $room->delete();
        return redirect('/myprofile');
    }

    public function needTenantDelete($id){
        $room = LookingForApartment::find($id);
        $room->delete();
        return redirect('/myprofile');
    }

    public function needRoomEdit(Request $request){
        $data = $this->validate($request, [
            'headline'          => 'nullable',
            'location'          => 'required',
            'latitude'          => 'nullable',
            'longitude'         => 'nullable',
            'rental_currency'   => 'required|in:AED,ARS,AUD,BRL,CAD,CHF,CNY,CZK,DKK,EUR,GBP,HKD,HUF,IDR,ILS,INR,JPY,KRW,MXN,MYR,NOK,NZD,PHP,PLN,RUB,SEK,SGD,THB,TRY,TWD,UAH,USD,VND,ZAR',
            'monthly_rent'      => 'required|numeric',
            'is_short_term'     => 'required|boolean',
            'move_date'         => 'required|date',
            'leave_date'        => 'nullable|date',
            'description'       => 'nullable',
            'cleanliness'       => 'nullable|in:Clean,Average,Messy',
            'overnight_guests'  => 'nullable|in:Never,Rarely,Occasionally,Often',
            'party_habits'      => 'nullable|in:Rarely,Occasionally,Weekends,Daily',
            'get_up'            => 'nullable|in:Before 6AM,6AM - 8AM,8AM - 10AM,10AM - 12AM,After 12AM',
            'go_to_bed'         => 'nullable|in:Before 8PM,8PM - 10PM,10PM - 12AM,12AM - 2AM,After 2AM',
            'food_preference'   => 'nullable|in:Almost Anything,Fish Only,Vegetarian,Vegan,Kosher',
            'smoker'            => 'nullable|in:No,Yes,Outside Only',
            'work_schedule'     => 'nullable|in:Professional (9-5),Student,Work at Night,Work at Home,Mixed',
            'occupation'        => 'nullable|in:Accounting,Administration/Secretarial,Advertising,Architecture/Design,Automotive/Mechanical,Aviation/Aeronautical,Beauty/Fashion,Biology/Life Science,Broadcasting/Publishing,Consulting,Customer Service/Support,Creative/Media,Education/Tranining,Engineering,Finance/Banking,Government/Social Service,Health Services,Hospitality,Human Resource/Recruiting,IT/Computers,Insurance,Internet/Media,Law Enforcement/Security,Legal,Manufacturing/Production,Marketing/Public Relations,Military,Non-Profit,Other,Physical Science/Environment,Real Estate,Retail,Sales,Sports/Fitness,Student/Graduate,Student/Undergraduate,Telecommunications,Trades/Labor,Transportation/Logistic',
            'prefer_min_age'    => 'required|numeric|between:18,99',
            'prefer_max_age'    => 'required|numeric|between:18,99',
            'prefer_smoker'     => 'nullable|in:No,Yes,Outside Only',
            'prefer_student'    => 'nullable|boolean',
            'have_cats'         => 'nullable|boolean',
            'have_dogs'         => 'nullable|boolean',
            'have_small_pets'   => 'nullable|boolean',
            'have_birds'        => 'nullable|boolean',
            'have_fish'         => 'nullable|boolean',
            'have_reptiles'     => 'nullable|boolean',
            'prefer_cats'       => 'nullable|boolean',
            'prefer_dogs'       => 'nullable|boolean',
            'prefer_small_pets' => 'nullable|boolean',
            'prefer_birds'      => 'nullable|boolean',
            'prefer_fish'       => 'nullable|boolean',
            'prefer_reptiles'   => 'nullable|boolean',
        ]);

        if ($currentUser = Sentinel::getUser()) {
            $data['user_id'] = $currentUser->id;
        }

        try {
            NeedRoom::where('id',$request->id)
            ->update($data);
            return response()->json(['status' => 'succeed', 'code' => 200]);
        } catch (\Exception $e) {
            return $e;
        }
    }
    public function needApartmentEdit(Request $request){}
    public function needRoommateEdit(Request $request){}
    public function needTenantEdit(Request $request){}
}