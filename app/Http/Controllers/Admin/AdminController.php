<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Faq;
use App\Model\AffiliateUser;
use App\Newsletter;
use App\Product;
use App\Role;
use App\Store;
use App\SubCategory;
use App\Subscriber;
use App\User;
use Reminder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Sentinel;
use Mail;
use Activation;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;

class AdminController extends Controller
{
    public function affiliaterList(){
        $affiliate = AffiliateUser::where('approved',0)->get();
        return view('admin.affiliaters',compact('affiliate'));
    }

    public function approveAffiliater($id){
        $unApproved = AffiliateUser::find($id);
        $unApproved->approved = 1;
        $unApproved->save();
        return redirect()->back();
    }

    public function showLogin(){
        if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug != 'user')
            return redirect('admin/dashboard');
        else
            return view('admin.login');
    }
    public function showRegisterPage(){
        $user = Sentinel::getUser();
        if ($user->hasAccess('user.view'))
        {
         return View::make('admin.registration')
            ->with('role',Role::all());
        }
            else
             abort('404');
    }
    public function postAdminRegistration(Request $request){
        $user = Sentinel::getUser();
        if ($user->hasAccess('user.view'))
        {
        if (Sentinel::findByCredentials(['email' => $request->email])) {
           return response()->json(['error' => 'Opps! Something went wrong'],500);
        }

        $user = Sentinel::registerAndActivate($request->all());
         $role = Sentinel::findRoleBySlug($request->role);
        $role->users()->attach($user);

        if ($role)
        return response()->json(['success' => 'User created']);
        else
            return response()->json(['error' => 'Opps something went wrong'],500);
        }
            else
                abort('404');
    }
    public function allAdmin(){
        $user = Sentinel::getUser();
        if ($user->hasAccess('user.view')) {
            $admins = DB::table('users')->join('role_users', 'users.id', '=', 'role_users.user_id')
                ->join('roles', 'roles.id', '=', 'role_users.role_id')
                ->select('users.*',
                    'roles.*')
                ->get();
            return View::make('admin.alluser')
                ->with('admins', $admins);
        }
        else abort('404');
    }
   public function viewDashboard(){
       return view('admin.dashboard');
   }
   public function viewAboutUs(){
       return view('admin.aboutus');
   }
   public function viewCalender(){
       $user = Sentinel::getUser();
       if ($user->hasAccess('calender.view')) {
       return view('admin.calender');}
       else
           abort('404');
   }
    public function viewAllFaq(){
        $user = Sentinel::getUser();
        if ($user->hasAccess('faq.view')) {
        return View::make('admin.allfaq')
            ->with('obj',Faq::all());}
            else
                abort('404');
    }
    public function addFaq(){
        $user = Sentinel::getUser();
        if ($user->hasAccess('faq.view')) {
        return view('admin.addfaq');}
        else
            abort('404');
    }
   public function addNewFaq(Request $request){
       $user = Sentinel::getUser();
       if ($user->hasAccess('faq.add')) {
       $object = new Faq();
       $object->question = $request->question;
       $object->answer = $request->answer;
       $object->meta_keywords = $request->meta;
       $result= $object->save();
       if ($result) {
           return response()->json(['success' => 'New faq created.']);
       }
       else

           {return response()->json(['error' => 'Opps! Something went wrong'],500);}
   }
    else
        abort('404');
    }
   public function editFaq($id){
       $user = Sentinel::getUser();
       if ($user->hasAccess('faq.edit')) {
           $obj = Crypt::decrypt($id);
           $content = Faq::where('id', '=', $obj)->first();
           if (!$content) {
               abort(404);
           }
           return View::make('admin.editfaq')
               ->with('content', $id)
               ->with('contents', $content);
       }
       else
           abort('404');
   }
   public function postEditFaq(Request $request){
       $user = Sentinel::getUser();
       if ($user->hasAccess('faq.add')) {
       $key = Crypt::decrypt($request->id);
       $content = Faq::where('id', '=', $key)->first();
       if (! $content){
           abort(404);
       }
       $obj = new Faq();
       $selected = $obj->find($key);
       $selected->question = $request->question;
       $selected->answer = $request->answer;
       $selected->meta_keywords = $request->meta;
       $result= $selected->update();
       if ($result) {
           return response()->json(['success' => 'Faq updated successfully.']);
       }
       else

       {return response()->json(['error' => 'Opps! Something went wrong'],500);}}
       else
           abort('404');
   }
   public function deleteFaq($id){
       $user = Sentinel::getUser();
       if ($user->hasAccess('faq.delete')) {
           $obj = Crypt::decrypt($id);
           $content = Faq::where('id', '=', $obj)->first();
           if (!$content) {
               abort(404);
           }
           $del = new Faq();
           $delete = $del->find($obj);
           $result = $delete->delete();
           if ($result) {
               return response()->json(['success' => 'Item deleted.']);
           } else {
               return response()->json(['error' => 'Opps! Something went wrong'], 500);
           }
       }
       else
           abort('404');
   }
    public function addRole(){
        $user = Sentinel::getUser();
        if ($user->hasAccess('role.add')) {
       return view('admin.addrole');}
       else
           abort('404');
    }
    public function roleIndex(){
        $user = Sentinel::getUser();
        if ($user->hasAccess('role.view')) {
       return View::make('admin.allrole')
           ->with('roles',Role::all());}
           else
               abort('404');
    }
    public function postAddRole(Request $request){
        $user = Sentinel::getUser();
        if ($user->hasAccess('role.add')) {
       $obj = new Role();
       $obj->name= $request->name;
       $obj->slug = $request->slug;
       $imp = implode($request->permission);
       $replaced = str_replace("'",'"',$imp);
       $obj->permissions = "{".$replaced."}";
       $result=$obj->save();
        if ($result) {
            return response()->json(['success' => 'Role Added.']);
        }
        else

        {return response()->json(['error' => 'Opps! Something went wrong'],500);}}
        else
            abort('404');
   }
   public function updateRole(Request $request){
       $user = Sentinel::getUser();
       if ($user->hasAccess('role.edit')) {
           $decrypt = Crypt::decrypt($request->id);
           $content = Role::where('id', '=', $decrypt)->first();
           if (!$content) {
               abort(404);
           }
           $obj = new Role();
           $object = $obj->find($decrypt);
           $object->name = $request->name;
           $object->slug = $request->slug;
           $result = $object->update();
           if ($result) {
               return response()->json(['success' => 'Role updated.']);
           } else {
               return response()->json(['error' => 'Opps! Something went wrong'], 500);
           }
       }
    else
        abort('404');
    }
   public function deleteRole($id){
       $user = Sentinel::getUser();
       if ($user->hasAccess('role.delete')) {
           $obj = Crypt::decrypt($id);
           $content = Role::where('id', '=', $obj)->first();
           if (!$content) {
               abort(404);
           }
           $del = new Role();
           $delete = $del->find($obj);
           $result = $delete->delete();
           if ($result) {
               return response()->json(['success' => 'Item deleted.']);
           } else {
               return response()->json(['error' => 'Opps! Something went wrong'], 500);
           }
       }
       else
           abort('404');
   }
    public function subscriberIndex(){
        $user = Sentinel::getUser();
        if ($user->hasAccess('user.view')) {
       return View::make('admin.subscriber')
           ->with('subscribers',Subscriber::all());}
           else
               abort('404');
    }
    public function adminPostSubscriber(Request $request){
        $user = Sentinel::getUser();
        if ($user->hasAccess('subscribe.add')) {
        $content = Subscriber::where('email', '=', $request->email)->first();
        if ($content) {
           return response()->json(['error' => 'Email exists'],500);
        }
        $obj = new Subscriber();
        $obj->email = $request->email;
        $result = $obj->save();
        if ($result) {
            return response()->json(['success' => 'subscriber added.']);
        }
        else

        {return response()->json(['error' => 'Opps! Something went wrong'],500);}

    }
    else abort('404');
    }
    public function adminDeleteSubscriber($id)
    {
        $user = Sentinel::getUser();
        if ($user->hasAccess('subscribe.delete')) {
            $del = Crypt::decrypt($id);
            $content = Subscriber::where('id', '=', $del)->first();
            if (!$content) {
                abort(404);
            }
            $obj = new Subscriber();
            $object = $obj->find($del);
            $result = $object->delete();
            if ($result) {
                return response()->json(['success' => 'Subscriber deleted.']);
            } else {
                return response()->json(['error' => 'Opps! Something went wrong'], 500);
            }

        }
    else
        abort('404');

    }
    public function addNewsletter()
    {
        $user = Sentinel::getUser();
        if ($user->hasAccess('newsletter.view')) {
            return View::make('admin.addnewsletter')
                ->with('newsletter', Newsletter::all());
        }
        else
            abort('404');
    }
    public function postNewsLetter(Request $request){
        $user = Sentinel::getUser();
        if ($user->hasAccess('newsletter.add')) {
            $obj = new Newsletter();
            $obj->name = $request->name;
            $obj->body = $request->body;
            $result = $obj->save();
            if ($result) {
                return response()->json(['success' => 'New Newsletter created.']);
            } else {
                return response()->json(['error' => 'Opps! Something went wrong'], 500);
            }

        }
        else
            abort('404');
    }
    public function deleteNewsletter($id){
        $user = Sentinel::getUser();
        if ($user->hasAccess('newsletter.delete')) {
        $del = Crypt::decrypt($id);
        $content = Newsletter::where('id', '=', $del)->first();
        if (! $content) {
            abort(404);
        }
        $obj = new Newsletter();
        $object = $obj->find($del);
        $result = $object->delete();
        if ($result) {
            return response()->json(['success' => 'Newsletter deleted.']);
        }
        else

        {return response()->json(['error' => 'Opps! Something went wrong'],500);}

    }
    else
        abort('404');
    }
    public function sendNewsletter($id){
        $user = Sentinel::getUser();
        if ($user->hasAccess('newsletter.add')) {
            $content = Crypt::decrypt($id);
            $segment = Newsletter::where('id', '=', $content)->first();
            if (!$segment) {
                abort(404);
            }
            $body = new Newsletter();
            $bodyContent = $body->find($content);
            $emails = DB::table('subscribers')->pluck('email');
            foreach ($emails as $email) {
                $data = array(
                    'email' => $email,
                    'subject' => "INFO ISSUE",
                    'bodyMessage' => $bodyContent->body

                );

                Mail::send('mail.newsletter', $data, function ($message) use ($data) {
                    $message->to($data['email']);
                    $message->subject($data['subject']);
                });
            }

            return response()->json(['success' => 'All newsletter sent.']);
        }
        else
            abort('404');
    }
    public function adminPostLogin(Request $request){
            $request->validate([
                'email'  => 'required',
                'password'=>'required'
            ]);
            $user = User::where('email', '=', $request->email)->first();
            if ($user === null) {
                return response()->json(['error'=>'Wrong login information'],500);
            }
            try {
                $rememberMe = false;
                if(isset($request->remember_me))
                    $rememberMe = true;
                if (Sentinel::authenticate($request->all(), $rememberMe))
                {
                    if(Sentinel::check()){
                        if (Sentinel::getUser()->roles()->first()->slug === 'user')
                        {
                            return response()->json(['error'=>'Wrong login information'],500);
                        }

                        else
                            {
                            return redirect('/dashboard')->with(['success'=>'Welcome to the dashboard']);
                        }
                        }

                }
                else
                    {
                    return response()->json(['error' => 'Wrong login information'],500);
                }
            } catch (ThrottlingException $e) {
                $delay = $e->getDelay();
                return response()->json(['error' => "You are band for $delay seconds"],500);

            } catch (NotActivatedException $e) {
                return response()->json(['error' => "Your account is not activated"],500);
            }
            }
    public function postLogout(){
        Sentinel::logout();
        return redirect('/admin/login')->with(['success'=>'Logged out']);
    }
}
