<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\Http\Controllers\Controller;

class ActivationController extends Controller
{
    public function activateUser($sentCode)
    {
        $activation = Activation::createModel()->where('code', $sentCode)->first();
        if ($activation) {
            $newUser = User::find($activation->user_id);
            Activation::complete($newUser, $sentCode);

            return redirect('/');
        }

        return redirect('/activation-failed');
    }
}
