<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{
    public function processLogin(Request $request)
    {
        $data = $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        try {
            if ('on' === $request->get('remember')) {
               $user = Sentinel::authenticateAndRemember($data);
            } else {
               $user = Sentinel::authenticate($data);
            }
            $cooler = User::where('email',$request->email)->first();
            $auth = Sentinel::getUser()->id;
            $chosen = User::find($auth);
            $chosen->active_status = 1;
            $chosen->save();
            $token = JWTAuth::fromUser($cooler);
            return response()->json(compact('token'));
        } catch(NotActivatedException $e) {
            return response()->json('error', 'Account not activated');
        }
    }

    public function logout()
    {
        Sentinel::logout(NULL, TRUE);
        return redirect('/');
    }
}
