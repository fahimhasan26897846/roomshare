<?php

namespace App\Http\Controllers\Auth;

use App\Contact;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class RegistrationController extends Controller
{
    public function processRegistration(Request $request)
    {
        $data = $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'day' => 'required|numeric|between:1,31',
            'month' => 'required|numeric|between:1,12',
            'year' => 'required|digits:4',
            'gender' => 'required|in:Male,Female'
        ]);
        if (! \checkdate($request->get('month'), $request->get('day'), $request->get('year'))) {
            return redirect()->back();
        }
        $data['dob'] = "{$request->get('year')}-{$request->get('month')}-{$request->get('day')}";

        if (! Sentinel::validForCreation($data)) {
            return redirect('/failed');
        }
        $user = Sentinel::registerAndActivate($data);
        $auth = $request->only(['email', 'password']);
        $bio = Sentinel::authenticateAndRemember($auth);
        $token = JWTAuth::fromUser($user);
        $welcome = Contact::create([
            'user_id'=> $user->id,
            'type'=>'admin',
            'subject'=>'Welcome to GettingRoom!',
            'message'=>'If you have any questions about your Roomster account, there is a site representative who is always available, and in most cases responds to you within minutes.'
        ]);
        return response()->json(compact('token'));
    }
}