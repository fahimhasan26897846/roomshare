<?php

namespace App\Http\Controllers\Auth;

use App\Mail\AccountRecovery;
use App\Mail\AccountVerification;
use App\User;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class AccountRecoveryController extends Controller
{
    public function processForgotPassword(Request $request)
    {
        $data = $this->validate($request, [
            'email' => 'required|email'
        ]);
        $authenticUser = Sentinel::findByCredentials($data);
        if ($authenticUser) {
            $reminder = Reminder::exists($authenticUser) ?: Reminder::create($authenticUser);
            $this->sendMail($authenticUser, $reminder->code);
        }
    }

    public function resetPassword($resetCode)
    {
        $reminder = Reminder::createModel()
            ->where('code', $resetCode)
            ->where('completed', FALSE)
            ->first();
        if ($reminder) {
            $authenticUser = User::find($reminder->user_id);
            if ($authenticUser) {
                return view('test-reset-password')->with('resetCode', $resetCode);
            }
        }
        return redirect('/password-reset-failed');
    }

    public function processResetPassword(Request $request)
    {
        $this->validate($request, [
            'reset-code' => 'required',
            'password' => 'required | same:confirm-password',
            'confirm-password'  => 'required | same:password'
        ]);
        $resetCode = $request->get('reset-code');
        $reminder = Reminder::createModel()
            ->where('code', $resetCode)
            ->where('completed', FALSE)
            ->first();

        if ($reminder) {
            $authenticUser = User::find($reminder->user_id);
            Reminder::complete($authenticUser, $resetCode, $request->get('password'));
            return redirect('/');
        }

        return redirect('/password-reset-failed');
    }

    private function sendMail($user, $resetCode)
    {
        Mail::to($user->email)
        ->send(new AccountVerification($resetCode,$user->first_name));
    }
}
