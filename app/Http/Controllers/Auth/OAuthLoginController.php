<?php

namespace App\Http\Controllers\Auth;

use App\Contact;
use App\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class OAuthLoginController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::with($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        $authenticUser = User::where('provider', $provider)
            ->where('provider_id', $user->getId())->first();
        if (!$authenticUser) {
            list($firstName, $lastName) = [$user->getName(),$user->getName()];
            $credentials = [
                'email'         => NULL,
                'password'      => 'secret',
                'provider'      => $provider,
                'provider_id'   => $user->getId(),
                'first_name'    => $firstName,
                'last_name'     => $lastName,
                'avatar'        => $user->getAvatar()
            ];
            $authenticUser = Sentinel::registerAndActivate($credentials);
            $welcome = Contact::create([
                'user_id'=> $user->id,
                'type'=>'admin',
                'subject'=>'Welcome to GettingRoom!',
                'message'=>'If you have any questions about your Roomster account, there is a site representative who is always available, and in most cases responds to you within minutes.'
            ]);
        }
        Sentinel::loginAndRemember($authenticUser);
        $auth = Sentinel::getUser()->id;
        $chosen = User::find($auth);
        $chosen->active_status = 1;
        $chosen->save();
        return redirect('/social');
    }
    public function currentUser(){

    }
}