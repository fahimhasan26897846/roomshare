<?php

namespace App\Events;

use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $message;
    private $user;
    private $client;
    private $channel;

    public function __construct($message, $channel, User $user, User $client)
    {
        $this->channel = $channel;
        $this->message = $message;
        $this->user = $user;
        $this->client = $client;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("{$this->channel}");
    }

    public function broadcastAs()
    {
        return 'App\Events\new-message';
    }

    public function broadCastWith()
    {
        return [
            'message' => $this->message,
            'first_name' => $this->user->first_name,
            'last_name' => $this->user->last_name,
            'from' => $this->user->id,
            'to' => $this->client->id,
            'channel' => "p-c-{$this->user->id}"
        ];
    }
}
