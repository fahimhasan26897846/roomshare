<?php
Route::get('/login/{provider}', 'Auth\OAuthLoginController@redirectToProvider')
    ->where('provider', 'facebook|google|instagram|linkedin|twitter|vkontakte');
Route::get('/login/{provider}/callback', 'Auth\OAuthLoginController@handleProviderCallback')
    ->where('provider', 'facebook|google|instagram|linkedin|twitter|vkontakte');
Route::post('/logout', 'Auth\LoginController@logout');
Route::post('/login', 'Auth\LoginController@processLogin');
Route::post('/register', 'Auth\RegistrationController@processRegistration');
Route::get('/activate/{activationCode}', 'Auth\ActivationController@activateUser');
Route::post('/forgot-password', 'Auth\AccountRecoveryController@processForgotPassword');
Route::get('/reset-password/{resetCode}', 'Auth\AccountRecoveryController@resetPassword');
Route::post('/reset-password', 'Auth\AccountRecoveryController@processResetPassword');
Route::get('/mylistings', 'ListingController@getListings');
Route::post('/needroom/new', 'ListingController@processNeedRoomForm');
Route::get('/allroom/{place}/{min}/{max}/{sort}', 'ListingController@needAllRoom');
Route::get('/allapartment/{place}/{min}/{max}/{sort}', 'ListingController@needApartmentAll');
Route::get('/single/room/{id}', 'ListingController@needSingleRoom');

Route::get('/anentireplacelist/{place}/{min}/{max}/{sort}', 'ListingController@anEntirePlaceList');
Route::get('/single/entireplace/{id}', 'ListingController@anEntirePlaceSingle');
Route::get('/single/aroom/{id}', 'ListingController@needaRoomSingle');
Route::get('/needaroomlist/{place}/{min}/{max}/{sort}', 'ListingController@needaRoomList');

Route::get('/single/apartment/{id}', 'ListingController@needApartmentSingle');\

Route::get('/needplace/{place}', 'ListingController@needPlace');
Route::get('/offeringroom/markers', 'ListingController@offeringRoomMarkers');
Route::get('/needentireplace/{place}', 'ListingController@needEntirePlace');
Route::get('/offeringroommate/markers', 'ListingController@offeringRoommateMarkers');
Route::get('/needroommateplace/{place}', 'ListingController@needRoommatePlace');
Route::get('/offeringentireplace/markers', 'ListingController@offeringEntireplaceMarkers');
Route::get('/needtenantplace/{place}', 'ListingController@needTenantPlace');
Route::get('/offeringtenant/markers', 'ListingController@offeringTenantMarkers');

//list delete route
Route::get('/needroom/delete/{id}', 'ListingController@needRoomDelete');
Route::get('/needapartment/delete/{id}', 'ListingController@needApartmentDelete');
Route::get('/needroommate/delete/{id}', 'ListingController@needRoommateDelete');
Route::get('/needtenant/delete/{id}', 'ListingController@needTenantDelete');

//list edit route
Route::post('/needroom/edit/', 'ListingController@needRoomEdit');
Route::post('/needapartment/edit/', 'ListingController@needApartmentEdit');
Route::post('/needroommate/edit/', 'ListingController@needRoommateEdit');
Route::post('/needtenant/edit/', 'ListingController@needTenantEdit');


Route::post('/needapartment/new', 'ListingController@processNeedApartmentForm');
Route::post('/offeringroom/new', 'ListingController@processOfferingRoomForm');
Route::post('/offeringapartment/new', 'ListingController@processOfferingApartmentForm');
Route::get('/chat/list', 'ChatController@fetchConversationList');
Route::get('/chat/{channel}', 'ChatController@loadChatRoom');
Route::post('/chat/received', 'ChatController@messageReceived');
Route::get('/chat/{channel}/messages', 'ChatController@fetchConversation');
Route::post('/chat/message', 'ChatController@sendMessage');\
Route::get('current/user','Auth\OAuthLoginController@currentUser');
Route::get('current/user/social','Auth\OAuthLoginController@currentUser');
Route::get('get/user','User\AuthController@getUser');
Route::get('set/user','User\AuthController@setUser');
Route::post('save/user','User\UserController@saveUser');
Route::get('get/auser/{id}','User\UserController@getSingleUser');
Route::get('add/bookmark/{id}/{type}','User\UserController@addBookMark');
Route::get('account/deactivate','User\UserController@accountDeactivate');
Route::get('/mybookmarks','User\UserController@usersBookmark');
Route::post('/reset/password/settings','User\UserController@settingsResetPassword');

Route::post('/sendquery','User\UserController@sendQuery');
Route::get('/unread/contact','User\UserController@unreadContact');
Route::get('/unread/read','User\UserController@unreadContactRead');
Route::get('/all/contacts/data','User\UserController@allContactsData');

//email verify this user ***************************************************
Route::get('/email/verify','User\UserController@emailVerifyUser');

//single page route*********************************************************
Route::get('/{path}','HomeController@home')->where('path', '.*');