<?php

Route::group(['prefix' => 'admin', 'as' => 'admin.','namespace' => 'Admin'], function () {
    include(__DIR__.'/Admin/index.php');
});

Route::group(['prefix' => 'affiliate', 'as' => 'affiliate.','namespace' => 'Affiliate'], function () {
    include(__DIR__.'/Affiliate/index.php');
});

Route::group(['as' => 'user.'], function () {
    include(__DIR__ . '/User/index.php');
});
