<?php
Route::get('login','AdminController@showLogin');
Route::post('post/login','AdminController@adminPostLogin');
Route::group(['middleware'=>'admin'],function () {/*Admin*/
    Route::get('registration','AdminController@showRegisterPage');
    Route::post('post/registration','AdminController@postAdminRegistration');
    Route::get('user/list','AdminController@allAdmin');
    Route::get('dashboard','AdminController@viewDashboard');
    Route::get('aboutdevelopper','AdminController@viewAboutUs');
    Route::get('logout','AdminController@postLogout');
    /*faq*/
    Route::get('allfaq','AdminController@viewAllFaq');
    Route::get('addfaq','AdminController@addFaq');
    Route::post('add/faq','AdminController@addNewFaq');
    Route::get('edit/faq/{id}','AdminController@editFaq');
    Route::post('edit/faq/{id}','AdminController@postEditFaq');
    Route::delete('delete/faq/{id}','AdminController@deleteFaq');
    /*endfaq*/
    /*role*/
    Route::get('/allrole','AdminController@roleIndex');
    Route::get('/addrole','AdminController@addRole');
    Route::post('/post/role','AdminController@postAddRole');
    Route::put('/role/update','AdminController@updateRole');
    Route::delete('/role/delete/{id}','AdminController@deleteRole');
    /*end role*/
    /*Subscriber*/
    Route::get('/subscribers','AdminController@subscriberIndex');
    Route::post('add/subscriber','AdminController@adminPostSubscriber');
    Route::delete('/delete/subscriber/{id}','AdminController@adminDeleteSubscriber');
    /*end subscriber*/
    /*newsletter*/
    Route::get('/add/newsletter','AdminController@addNewsletter');
    Route::post('/post/create/newsletter','AdminController@postNewsLetter');
    Route::delete('/delete/newsletter/{id}','AdminController@deleteNewsletter');
    Route::post('/send/newsletter/{id}','AdminController@sendNewsletter');
    /*End Newsletter*/
    /*calender*/
    Route::get('/calender','AdminController@viewCalender');
    Route::get('/affiliaters/list','AdminController@affiliaterList');
    Route::get('approve/affiliater/{id}','AdminController@approveAffiliater');
});