let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.disableNotifications();
mix.js('resources/assets/js/app.js', 'public/js')
    .js(['public/frontend/resourses/jquery-1.9.1.min.js','public/frontend/resourses/script/script.js'],'public/js/all.js')
    .styles([
    'public/frontend/resourses/css/style.css',
    'public/frontend/resourses/css/normal.css',
    'public/frontend/resourses/css/new.css'
], 'public/css/all.css')
   .sass('resources/assets/sass/app.scss', 'public/css/app.css');

