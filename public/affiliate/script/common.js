/*/Scripts/moment/moment.min.js*/
//! moment.js
//! version : 2.5.1
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com
(function(a) {
    function b() {
        return {
            empty: !1,
            unusedTokens: [],
            unusedInput: [],
            overflow: -2,
            charsLeftOver: 0,
            nullInput: !1,
            invalidMonth: null,
            invalidFormat: !1,
            userInvalidated: !1,
            iso: !1
        }
    }

    function c(a, b) {
        return function(c) {
            return k(a.call(this, c), b)
        }
    }

    function d(a, b) {
        return function(c) {
            return this.lang().ordinal(a.call(this, c), b)
        }
    }

    function e() {}

    function f(a) {
        w(a), h(this, a)
    }

    function g(a) {
        var b = q(a),
            c = b.year || 0,
            d = b.month || 0,
            e = b.week || 0,
            f = b.day || 0,
            g = b.hour || 0,
            h = b.minute || 0,
            i = b.second || 0,
            j = b.millisecond || 0;
        this._milliseconds = +j + 1e3 * i + 6e4 * h + 36e5 * g, this._days = +f + 7 * e, this._months = +d + 12 * c, this._data = {}, this._bubble()
    }

    function h(a, b) {
        for (var c in b) b.hasOwnProperty(c) && (a[c] = b[c]);
        return b.hasOwnProperty("toString") && (a.toString = b.toString), b.hasOwnProperty("valueOf") && (a.valueOf = b.valueOf), a
    }

    function i(a) {
        var b, c = {};
        for (b in a) a.hasOwnProperty(b) && qb.hasOwnProperty(b) && (c[b] = a[b]);
        return c
    }

    function j(a) {
        return 0 > a ? Math.ceil(a) : Math.floor(a)
    }

    function k(a, b, c) {
        for (var d = "" + Math.abs(a), e = a >= 0; d.length < b;) d = "0" + d;
        return (e ? c ? "+" : "" : "-") + d
    }

    function l(a, b, c, d) {
        var e, f, g = b._milliseconds,
            h = b._days,
            i = b._months;
        g && a._d.setTime(+a._d + g * c), (h || i) && (e = a.minute(), f = a.hour()), h && a.date(a.date() + h * c), i && a.month(a.month() + i * c), g && !d && db.updateOffset(a), (h || i) && (a.minute(e), a.hour(f))
    }

    function m(a) {
        return "[object Array]" === Object.prototype.toString.call(a)
    }

    function n(a) {
        return "[object Date]" === Object.prototype.toString.call(a) || a instanceof Date
    }

    function o(a, b, c) {
        var d, e = Math.min(a.length, b.length),
            f = Math.abs(a.length - b.length),
            g = 0;
        for (d = 0; e > d; d++)(c && a[d] !== b[d] || !c && s(a[d]) !== s(b[d])) && g++;
        return g + f
    }

    function p(a) {
        if (a) {
            var b = a.toLowerCase().replace(/(.)s$/, "$1");
            a = Tb[a] || Ub[b] || b
        }
        return a
    }

    function q(a) {
        var b, c, d = {};
        for (c in a) a.hasOwnProperty(c) && (b = p(c), b && (d[b] = a[c]));
        return d
    }

    function r(b) {
        var c, d;
        if (0 === b.indexOf("week")) c = 7, d = "day";
        else {
            if (0 !== b.indexOf("month")) return;
            c = 12, d = "month"
        }
        db[b] = function(e, f) {
            var g, h, i = db.fn._lang[b],
                j = [];
            if ("number" == typeof e && (f = e, e = a), h = function(a) {
                var b = db().utc().set(d, a);
                return i.call(db.fn._lang, b, e || "")
            }, null != f) return h(f);
            for (g = 0; c > g; g++) j.push(h(g));
            return j
        }
    }

    function s(a) {
        var b = +a,
            c = 0;
        return 0 !== b && isFinite(b) && (c = b >= 0 ? Math.floor(b) : Math.ceil(b)), c
    }

    function t(a, b) {
        return new Date(Date.UTC(a, b + 1, 0)).getUTCDate()
    }

    function u(a) {
        return v(a) ? 366 : 365
    }

    function v(a) {
        return a % 4 === 0 && a % 100 !== 0 || a % 400 === 0
    }

    function w(a) {
        var b;
        a._a && -2 === a._pf.overflow && (b = a._a[jb] < 0 || a._a[jb] > 11 ? jb : a._a[kb] < 1 || a._a[kb] > t(a._a[ib], a._a[jb]) ? kb : a._a[lb] < 0 || a._a[lb] > 23 ? lb : a._a[mb] < 0 || a._a[mb] > 59 ? mb : a._a[nb] < 0 || a._a[nb] > 59 ? nb : a._a[ob] < 0 || a._a[ob] > 999 ? ob : -1, a._pf._overflowDayOfYear && (ib > b || b > kb) && (b = kb), a._pf.overflow = b)
    }

    function x(a) {
        return null == a._isValid && (a._isValid = !isNaN(a._d.getTime()) && a._pf.overflow < 0 && !a._pf.empty && !a._pf.invalidMonth && !a._pf.nullInput && !a._pf.invalidFormat && !a._pf.userInvalidated, a._strict && (a._isValid = a._isValid && 0 === a._pf.charsLeftOver && 0 === a._pf.unusedTokens.length)), a._isValid
    }

    function y(a) {
        return a ? a.toLowerCase().replace("_", "-") : a
    }

    function z(a, b) {
        return b._isUTC ? db(a).zone(b._offset || 0) : db(a).local()
    }

    function A(a, b) {
        return b.abbr = a, pb[a] || (pb[a] = new e), pb[a].set(b), pb[a]
    }

    function B(a) {
        delete pb[a]
    }

    function C(a) {
        var b, c, d, e, f = 0,
            g = function(a) {
                if (!pb[a] && rb) try {
                    require("./lang/" + a)
                } catch (b) {}
                return pb[a]
            };
        if (!a) return db.fn._lang;
        if (!m(a)) {
            if (c = g(a)) return c;
            a = [a]
        }
        for (; f < a.length;) {
            for (e = y(a[f]).split("-"), b = e.length, d = y(a[f + 1]), d = d ? d.split("-") : null; b > 0;) {
                if (c = g(e.slice(0, b).join("-"))) return c;
                if (d && d.length >= b && o(e, d, !0) >= b - 1) break;
                b--
            }
            f++
        }
        return db.fn._lang
    }

    function D(a) {
        return a.match(/\[[\s\S]/) ? a.replace(/^\[|\]$/g, "") : a.replace(/\\/g, "")
    }

    function E(a) {
        var b, c, d = a.match(vb);
        for (b = 0, c = d.length; c > b; b++) d[b] = Yb[d[b]] ? Yb[d[b]] : D(d[b]);
        return function(e) {
            var f = "";
            for (b = 0; c > b; b++) f += d[b] instanceof Function ? d[b].call(e, a) : d[b];
            return f
        }
    }

    function F(a, b) {
        return a.isValid() ? (b = G(b, a.lang()), Vb[b] || (Vb[b] = E(b)), Vb[b](a)) : a.lang().invalidDate()
    }

    function G(a, b) {
        function c(a) {
            return b.longDateFormat(a) || a
        }
        var d = 5;
        for (wb.lastIndex = 0; d >= 0 && wb.test(a);) a = a.replace(wb, c), wb.lastIndex = 0, d -= 1;
        return a
    }

    function H(a, b) {
        var c, d = b._strict;
        switch (a) {
            case "DDDD":
                return Ib;
            case "YYYY":
            case "GGGG":
            case "gggg":
                return d ? Jb : zb;
            case "Y":
            case "G":
            case "g":
                return Lb;
            case "YYYYYY":
            case "YYYYY":
            case "GGGGG":
            case "ggggg":
                return d ? Kb : Ab;
            case "S":
                if (d) return Gb;
            case "SS":
                if (d) return Hb;
            case "SSS":
                if (d) return Ib;
            case "DDD":
                return yb;
            case "MMM":
            case "MMMM":
            case "dd":
            case "ddd":
            case "dddd":
                return Cb;
            case "a":
            case "A":
                return C(b._l)._meridiemParse;
            case "X":
                return Fb;
            case "Z":
            case "ZZ":
                return Db;
            case "T":
                return Eb;
            case "SSSS":
                return Bb;
            case "MM":
            case "DD":
            case "YY":
            case "GG":
            case "gg":
            case "HH":
            case "hh":
            case "mm":
            case "ss":
            case "ww":
            case "WW":
                return d ? Hb : xb;
            case "M":
            case "D":
            case "d":
            case "H":
            case "h":
            case "m":
            case "s":
            case "w":
            case "W":
            case "e":
            case "E":
                return xb;
            default:
                return c = new RegExp(P(O(a.replace("\\", "")), "i"))
        }
    }

    function I(a) {
        a = a || "";
        var b = a.match(Db) || [],
            c = b[b.length - 1] || [],
            d = (c + "").match(Qb) || ["-", 0, 0],
            e = +(60 * d[1]) + s(d[2]);
        return "+" === d[0] ? -e : e
    }

    function J(a, b, c) {
        var d, e = c._a;
        switch (a) {
            case "M":
            case "MM":
                null != b && (e[jb] = s(b) - 1);
                break;
            case "MMM":
            case "MMMM":
                d = C(c._l).monthsParse(b), null != d ? e[jb] = d : c._pf.invalidMonth = b;
                break;
            case "D":
            case "DD":
                null != b && (e[kb] = s(b));
                break;
            case "DDD":
            case "DDDD":
                null != b && (c._dayOfYear = s(b));
                break;
            case "YY":
                e[ib] = s(b) + (s(b) > 68 ? 1900 : 2e3);
                break;
            case "YYYY":
            case "YYYYY":
            case "YYYYYY":
                e[ib] = s(b);
                break;
            case "a":
            case "A":
                c._isPm = C(c._l).isPM(b);
                break;
            case "H":
            case "HH":
            case "h":
            case "hh":
                e[lb] = s(b);
                break;
            case "m":
            case "mm":
                e[mb] = s(b);
                break;
            case "s":
            case "ss":
                e[nb] = s(b);
                break;
            case "S":
            case "SS":
            case "SSS":
            case "SSSS":
                e[ob] = s(1e3 * ("0." + b));
                break;
            case "X":
                c._d = new Date(1e3 * parseFloat(b));
                break;
            case "Z":
            case "ZZ":
                c._useUTC = !0, c._tzm = I(b);
                break;
            case "w":
            case "ww":
            case "W":
            case "WW":
            case "d":
            case "dd":
            case "ddd":
            case "dddd":
            case "e":
            case "E":
                a = a.substr(0, 1);
            case "gg":
            case "gggg":
            case "GG":
            case "GGGG":
            case "GGGGG":
                a = a.substr(0, 2), b && (c._w = c._w || {}, c._w[a] = b)
        }
    }

    function K(a) {
        var b, c, d, e, f, g, h, i, j, k, l = [];
        if (!a._d) {
            for (d = M(a), a._w && null == a._a[kb] && null == a._a[jb] && (f = function(b) {
                var c = parseInt(b, 10);
                return b ? b.length < 3 ? c > 68 ? 1900 + c : 2e3 + c : c : null == a._a[ib] ? db().weekYear() : a._a[ib]
            }, g = a._w, null != g.GG || null != g.W || null != g.E ? h = Z(f(g.GG), g.W || 1, g.E, 4, 1) : (i = C(a._l), j = null != g.d ? V(g.d, i) : null != g.e ? parseInt(g.e, 10) + i._week.dow : 0, k = parseInt(g.w, 10) || 1, null != g.d && j < i._week.dow && k++, h = Z(f(g.gg), k, j, i._week.doy, i._week.dow)), a._a[ib] = h.year, a._dayOfYear = h.dayOfYear), a._dayOfYear && (e = null == a._a[ib] ? d[ib] : a._a[ib], a._dayOfYear > u(e) && (a._pf._overflowDayOfYear = !0), c = U(e, 0, a._dayOfYear), a._a[jb] = c.getUTCMonth(), a._a[kb] = c.getUTCDate()), b = 0; 3 > b && null == a._a[b]; ++b) a._a[b] = l[b] = d[b];
            for (; 7 > b; b++) a._a[b] = l[b] = null == a._a[b] ? 2 === b ? 1 : 0 : a._a[b];
            l[lb] += s((a._tzm || 0) / 60), l[mb] += s((a._tzm || 0) % 60), a._d = (a._useUTC ? U : T).apply(null, l)
        }
    }

    function L(a) {
        var b;
        a._d || (b = q(a._i), a._a = [b.year, b.month, b.day, b.hour, b.minute, b.second, b.millisecond], K(a))
    }

    function M(a) {
        var b = new Date;
        return a._useUTC ? [b.getUTCFullYear(), b.getUTCMonth(), b.getUTCDate()] : [b.getFullYear(), b.getMonth(), b.getDate()]
    }

    function N(a) {
        a._a = [], a._pf.empty = !0;
        var b, c, d, e, f, g = C(a._l),
            h = "" + a._i,
            i = h.length,
            j = 0;
        for (d = G(a._f, g).match(vb) || [], b = 0; b < d.length; b++) e = d[b], c = (h.match(H(e, a)) || [])[0], c && (f = h.substr(0, h.indexOf(c)), f.length > 0 && a._pf.unusedInput.push(f), h = h.slice(h.indexOf(c) + c.length), j += c.length), Yb[e] ? (c ? a._pf.empty = !1 : a._pf.unusedTokens.push(e), J(e, c, a)) : a._strict && !c && a._pf.unusedTokens.push(e);
        a._pf.charsLeftOver = i - j, h.length > 0 && a._pf.unusedInput.push(h), a._isPm && a._a[lb] < 12 && (a._a[lb] += 12), a._isPm === !1 && 12 === a._a[lb] && (a._a[lb] = 0), K(a), w(a)
    }

    function O(a) {
        return a.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function(a, b, c, d, e) {
            return b || c || d || e
        })
    }

    function P(a) {
        return a.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
    }

    function Q(a) {
        var c, d, e, f, g;
        if (0 === a._f.length) return a._pf.invalidFormat = !0, a._d = new Date(0 / 0), void 0;
        for (f = 0; f < a._f.length; f++) g = 0, c = h({}, a), c._pf = b(), c._f = a._f[f], N(c), x(c) && (g += c._pf.charsLeftOver, g += 10 * c._pf.unusedTokens.length, c._pf.score = g, (null == e || e > g) && (e = g, d = c));
        h(a, d || c)
    }

    function R(a) {
        var b, c, d = a._i,
            e = Mb.exec(d);
        if (e) {
            for (a._pf.iso = !0, b = 0, c = Ob.length; c > b; b++)
                if (Ob[b][1].exec(d)) {
                    a._f = Ob[b][0] + (e[6] || " ");
                    break
                }
            for (b = 0, c = Pb.length; c > b; b++)
                if (Pb[b][1].exec(d)) {
                    a._f += Pb[b][0];
                    break
                }
            d.match(Db) && (a._f += "Z"), N(a)
        } else a._d = new Date(d)
    }

    function S(b) {
        var c = b._i,
            d = sb.exec(c);
        c === a ? b._d = new Date : d ? b._d = new Date(+d[1]) : "string" == typeof c ? R(b) : m(c) ? (b._a = c.slice(0), K(b)) : n(c) ? b._d = new Date(+c) : "object" == typeof c ? L(b) : b._d = new Date(c)
    }

    function T(a, b, c, d, e, f, g) {
        var h = new Date(a, b, c, d, e, f, g);
        return 1970 > a && h.setFullYear(a), h
    }

    function U(a) {
        var b = new Date(Date.UTC.apply(null, arguments));
        return 1970 > a && b.setUTCFullYear(a), b
    }

    function V(a, b) {
        if ("string" == typeof a)
            if (isNaN(a)) {
                if (a = b.weekdaysParse(a), "number" != typeof a) return null
            } else a = parseInt(a, 10);
        return a
    }

    function W(a, b, c, d, e) {
        return e.relativeTime(b || 1, !!c, a, d)
    }

    function X(a, b, c) {
        var d = hb(Math.abs(a) / 1e3),
            e = hb(d / 60),
            f = hb(e / 60),
            g = hb(f / 24),
            h = hb(g / 365),
            i = 45 > d && ["s", d] || 1 === e && ["m"] || 45 > e && ["mm", e] || 1 === f && ["h"] || 22 > f && ["hh", f] || 1 === g && ["d"] || 25 >= g && ["dd", g] || 45 >= g && ["M"] || 345 > g && ["MM", hb(g / 30)] || 1 === h && ["y"] || ["yy", h];
        return i[2] = b, i[3] = a > 0, i[4] = c, W.apply({}, i)
    }

    function Y(a, b, c) {
        var d, e = c - b,
            f = c - a.day();
        return f > e && (f -= 7), e - 7 > f && (f += 7), d = db(a).add("d", f), {
            week: Math.ceil(d.dayOfYear() / 7),
            year: d.year()
        }
    }

    function Z(a, b, c, d, e) {
        var f, g, h = U(a, 0, 1).getUTCDay();
        return c = null != c ? c : e, f = e - h + (h > d ? 7 : 0) - (e > h ? 7 : 0), g = 7 * (b - 1) + (c - e) + f + 1, {
            year: g > 0 ? a : a - 1,
            dayOfYear: g > 0 ? g : u(a - 1) + g
        }
    }

    function $(a) {
        var b = a._i,
            c = a._f;
        return null === b ? db.invalid({
            nullInput: !0
        }) : ("string" == typeof b && (a._i = b = C().preparse(b)), db.isMoment(b) ? (a = i(b), a._d = new Date(+b._d)) : c ? m(c) ? Q(a) : N(a) : S(a), new f(a))
    }

    function _(a, b) {
        db.fn[a] = db.fn[a + "s"] = function(a) {
            var c = this._isUTC ? "UTC" : "";
            return null != a ? (this._d["set" + c + b](a), db.updateOffset(this), this) : this._d["get" + c + b]()
        }
    }

    function ab(a) {
        db.duration.fn[a] = function() {
            return this._data[a]
        }
    }

    function bb(a, b) {
        db.duration.fn["as" + a] = function() {
            return +this / b
        }
    }

    function cb(a) {
        var b = !1,
            c = db;
        "undefined" == typeof ender && (a ? (gb.moment = function() {
            return !b && console && console.warn && (b = !0, console.warn("Accessing Moment through the global scope is deprecated, and will be removed in an upcoming release.")), c.apply(null, arguments)
        }, h(gb.moment, c)) : gb.moment = db)
    }
    for (var db, eb, fb = "2.5.1", gb = this, hb = Math.round, ib = 0, jb = 1, kb = 2, lb = 3, mb = 4, nb = 5, ob = 6, pb = {}, qb = {
        _isAMomentObject: null,
        _i: null,
        _f: null,
        _l: null,
        _strict: null,
        _isUTC: null,
        _offset: null,
        _pf: null,
        _lang: null
    }, rb = "undefined" != typeof module && module.exports && "undefined" != typeof require, sb = /^\/?Date\((\-?\d+)/i, tb = /(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/, ub = /^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/, vb = /(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|X|zz?|ZZ?|.)/g, wb = /(\[[^\[]*\])|(\\)?(LT|LL?L?L?|l{1,4})/g, xb = /\d\d?/, yb = /\d{1,3}/, zb = /\d{1,4}/, Ab = /[+\-]?\d{1,6}/, Bb = /\d+/, Cb = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i, Db = /Z|[\+\-]\d\d:?\d\d/gi, Eb = /T/i, Fb = /[\+\-]?\d+(\.\d{1,3})?/, Gb = /\d/, Hb = /\d\d/, Ib = /\d{3}/, Jb = /\d{4}/, Kb = /[+-]?\d{6}/, Lb = /[+-]?\d+/, Mb = /^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/, Nb = "YYYY-MM-DDTHH:mm:ssZ", Ob = [
        ["YYYYYY-MM-DD", /[+-]\d{6}-\d{2}-\d{2}/],
        ["YYYY-MM-DD", /\d{4}-\d{2}-\d{2}/],
        ["GGGG-[W]WW-E", /\d{4}-W\d{2}-\d/],
        ["GGGG-[W]WW", /\d{4}-W\d{2}/],
        ["YYYY-DDD", /\d{4}-\d{3}/]
    ], Pb = [
        ["HH:mm:ss.SSSS", /(T| )\d\d:\d\d:\d\d\.\d{1,3}/],
        ["HH:mm:ss", /(T| )\d\d:\d\d:\d\d/],
        ["HH:mm", /(T| )\d\d:\d\d/],
        ["HH", /(T| )\d\d/]
    ], Qb = /([\+\-]|\d\d)/gi, Rb = "Date|Hours|Minutes|Seconds|Milliseconds".split("|"), Sb = {
        Milliseconds: 1,
        Seconds: 1e3,
        Minutes: 6e4,
        Hours: 36e5,
        Days: 864e5,
        Months: 2592e6,
        Years: 31536e6
    }, Tb = {
        ms: "millisecond",
        s: "second",
        m: "minute",
        h: "hour",
        d: "day",
        D: "date",
        w: "week",
        W: "isoWeek",
        M: "month",
        y: "year",
        DDD: "dayOfYear",
        e: "weekday",
        E: "isoWeekday",
        gg: "weekYear",
        GG: "isoWeekYear"
    }, Ub = {
        dayofyear: "dayOfYear",
        isoweekday: "isoWeekday",
        isoweek: "isoWeek",
        weekyear: "weekYear",
        isoweekyear: "isoWeekYear"
    }, Vb = {}, Wb = "DDD w W M D d".split(" "), Xb = "M D H h m s w W".split(" "), Yb = {
        M: function() {
            return this.month() + 1
        },
        MMM: function(a) {
            return this.lang().monthsShort(this, a)
        },
        MMMM: function(a) {
            return this.lang().months(this, a)
        },
        D: function() {
            return this.date()
        },
        DDD: function() {
            return this.dayOfYear()
        },
        d: function() {
            return this.day()
        },
        dd: function(a) {
            return this.lang().weekdaysMin(this, a)
        },
        ddd: function(a) {
            return this.lang().weekdaysShort(this, a)
        },
        dddd: function(a) {
            return this.lang().weekdays(this, a)
        },
        w: function() {
            return this.week()
        },
        W: function() {
            return this.isoWeek()
        },
        YY: function() {
            return k(this.year() % 100, 2)
        },
        YYYY: function() {
            return k(this.year(), 4)
        },
        YYYYY: function() {
            return k(this.year(), 5)
        },
        YYYYYY: function() {
            var a = this.year(),
                b = a >= 0 ? "+" : "-";
            return b + k(Math.abs(a), 6)
        },
        gg: function() {
            return k(this.weekYear() % 100, 2)
        },
        gggg: function() {
            return k(this.weekYear(), 4)
        },
        ggggg: function() {
            return k(this.weekYear(), 5)
        },
        GG: function() {
            return k(this.isoWeekYear() % 100, 2)
        },
        GGGG: function() {
            return k(this.isoWeekYear(), 4)
        },
        GGGGG: function() {
            return k(this.isoWeekYear(), 5)
        },
        e: function() {
            return this.weekday()
        },
        E: function() {
            return this.isoWeekday()
        },
        a: function() {
            return this.lang().meridiem(this.hours(), this.minutes(), !0)
        },
        A: function() {
            return this.lang().meridiem(this.hours(), this.minutes(), !1)
        },
        H: function() {
            return this.hours()
        },
        h: function() {
            return this.hours() % 12 || 12
        },
        m: function() {
            return this.minutes()
        },
        s: function() {
            return this.seconds()
        },
        S: function() {
            return s(this.milliseconds() / 100)
        },
        SS: function() {
            return k(s(this.milliseconds() / 10), 2)
        },
        SSS: function() {
            return k(this.milliseconds(), 3)
        },
        SSSS: function() {
            return k(this.milliseconds(), 3)
        },
        Z: function() {
            var a = -this.zone(),
                b = "+";
            return 0 > a && (a = -a, b = "-"), b + k(s(a / 60), 2) + ":" + k(s(a) % 60, 2)
        },
        ZZ: function() {
            var a = -this.zone(),
                b = "+";
            return 0 > a && (a = -a, b = "-"), b + k(s(a / 60), 2) + k(s(a) % 60, 2)
        },
        z: function() {
            return this.zoneAbbr()
        },
        zz: function() {
            return this.zoneName()
        },
        X: function() {
            return this.unix()
        },
        Q: function() {
            return this.quarter()
        }
    }, Zb = ["months", "monthsShort", "weekdays", "weekdaysShort", "weekdaysMin"]; Wb.length;) eb = Wb.pop(), Yb[eb + "o"] = d(Yb[eb], eb);
    for (; Xb.length;) eb = Xb.pop(), Yb[eb + eb] = c(Yb[eb], 2);
    for (Yb.DDDD = c(Yb.DDD, 3), h(e.prototype, {
        set: function(a) {
            var b, c;
            for (c in a) b = a[c], "function" == typeof b ? this[c] = b : this["_" + c] = b
        },
        _months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
        months: function(a) {
            return this._months[a.month()]
        },
        _monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
        monthsShort: function(a) {
            return this._monthsShort[a.month()]
        },
        monthsParse: function(a) {
            var b, c, d;
            for (this._monthsParse || (this._monthsParse = []), b = 0; 12 > b; b++)
                if (this._monthsParse[b] || (c = db.utc([2e3, b]), d = "^" + this.months(c, "") + "|^" + this.monthsShort(c, ""), this._monthsParse[b] = new RegExp(d.replace(".", ""), "i")), this._monthsParse[b].test(a)) return b
        },
        _weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
        weekdays: function(a) {
            return this._weekdays[a.day()]
        },
        _weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
        weekdaysShort: function(a) {
            return this._weekdaysShort[a.day()]
        },
        _weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
        weekdaysMin: function(a) {
            return this._weekdaysMin[a.day()]
        },
        weekdaysParse: function(a) {
            var b, c, d;
            for (this._weekdaysParse || (this._weekdaysParse = []), b = 0; 7 > b; b++)
                if (this._weekdaysParse[b] || (c = db([2e3, 1]).day(b), d = "^" + this.weekdays(c, "") + "|^" + this.weekdaysShort(c, "") + "|^" + this.weekdaysMin(c, ""), this._weekdaysParse[b] = new RegExp(d.replace(".", ""), "i")), this._weekdaysParse[b].test(a)) return b
        },
        _longDateFormat: {
            LT: "h:mm A",
            L: "MM/DD/YYYY",
            LL: "MMMM D YYYY",
            LLL: "MMMM D YYYY LT",
            LLLL: "dddd, MMMM D YYYY LT"
        },
        longDateFormat: function(a) {
            var b = this._longDateFormat[a];
            return !b && this._longDateFormat[a.toUpperCase()] && (b = this._longDateFormat[a.toUpperCase()].replace(/MMMM|MM|DD|dddd/g, function(a) {
                return a.slice(1)
            }), this._longDateFormat[a] = b), b
        },
        isPM: function(a) {
            return "p" === (a + "").toLowerCase().charAt(0)
        },
        _meridiemParse: /[ap]\.?m?\.?/i,
        meridiem: function(a, b, c) {
            return a > 11 ? c ? "pm" : "PM" : c ? "am" : "AM"
        },
        _calendar: {
            sameDay: "[Today at] LT",
            nextDay: "[Tomorrow at] LT",
            nextWeek: "dddd [at] LT",
            lastDay: "[Yesterday at] LT",
            lastWeek: "[Last] dddd [at] LT",
            sameElse: "L"
        },
        calendar: function(a, b) {
            var c = this._calendar[a];
            return "function" == typeof c ? c.apply(b) : c
        },
        _relativeTime: {
            future: "in %s",
            past: "%s ago",
            s: "a few seconds",
            m: "a minute",
            mm: "%d minutes",
            h: "an hour",
            hh: "%d hours",
            d: "a day",
            dd: "%d days",
            M: "a month",
            MM: "%d months",
            y: "a year",
            yy: "%d years"
        },
        relativeTime: function(a, b, c, d) {
            var e = this._relativeTime[c];
            return "function" == typeof e ? e(a, b, c, d) : e.replace(/%d/i, a)
        },
        pastFuture: function(a, b) {
            var c = this._relativeTime[a > 0 ? "future" : "past"];
            return "function" == typeof c ? c(b) : c.replace(/%s/i, b)
        },
        ordinal: function(a) {
            return this._ordinal.replace("%d", a)
        },
        _ordinal: "%d",
        preparse: function(a) {
            return a
        },
        postformat: function(a) {
            return a
        },
        week: function(a) {
            return Y(a, this._week.dow, this._week.doy).week
        },
        _week: {
            dow: 0,
            doy: 6
        },
        _invalidDate: "Invalid date",
        invalidDate: function() {
            return this._invalidDate
        }
    }), db = function(c, d, e, f) {
        var g;
        return "boolean" == typeof e && (f = e, e = a), g = {}, g._isAMomentObject = !0, g._i = c, g._f = d, g._l = e, g._strict = f, g._isUTC = !1, g._pf = b(), $(g)
    }, db.utc = function(c, d, e, f) {
        var g;
        return "boolean" == typeof e && (f = e, e = a), g = {}, g._isAMomentObject = !0, g._useUTC = !0, g._isUTC = !0, g._l = e, g._i = c, g._f = d, g._strict = f, g._pf = b(), $(g).utc()
    }, db.unix = function(a) {
        return db(1e3 * a)
    }, db.duration = function(a, b) {
        var c, d, e, f = a,
            h = null;
        return db.isDuration(a) ? f = {
            ms: a._milliseconds,
            d: a._days,
            M: a._months
        } : "number" == typeof a ? (f = {}, b ? f[b] = a : f.milliseconds = a) : (h = tb.exec(a)) ? (c = "-" === h[1] ? -1 : 1, f = {
            y: 0,
            d: s(h[kb]) * c,
            h: s(h[lb]) * c,
            m: s(h[mb]) * c,
            s: s(h[nb]) * c,
            ms: s(h[ob]) * c
        }) : (h = ub.exec(a)) && (c = "-" === h[1] ? -1 : 1, e = function(a) {
            var b = a && parseFloat(a.replace(",", "."));
            return (isNaN(b) ? 0 : b) * c
        }, f = {
            y: e(h[2]),
            M: e(h[3]),
            d: e(h[4]),
            h: e(h[5]),
            m: e(h[6]),
            s: e(h[7]),
            w: e(h[8])
        }), d = new g(f), db.isDuration(a) && a.hasOwnProperty("_lang") && (d._lang = a._lang), d
    }, db.version = fb, db.defaultFormat = Nb, db.updateOffset = function() {}, db.lang = function(a, b) {
        var c;
        return a ? (b ? A(y(a), b) : null === b ? (B(a), a = "en") : pb[a] || C(a), c = db.duration.fn._lang = db.fn._lang = C(a), c._abbr) : db.fn._lang._abbr
    }, db.langData = function(a) {
        return a && a._lang && a._lang._abbr && (a = a._lang._abbr), C(a)
    }, db.isMoment = function(a) {
        return a instanceof f || null != a && a.hasOwnProperty("_isAMomentObject")
    }, db.isDuration = function(a) {
        return a instanceof g
    }, eb = Zb.length - 1; eb >= 0; --eb) r(Zb[eb]);
    for (db.normalizeUnits = function(a) {
        return p(a)
    }, db.invalid = function(a) {
        var b = db.utc(0 / 0);
        return null != a ? h(b._pf, a) : b._pf.userInvalidated = !0, b
    }, db.parseZone = function(a) {
        return db(a).parseZone()
    }, h(db.fn = f.prototype, {
        clone: function() {
            return db(this)
        },
        valueOf: function() {
            return +this._d + 6e4 * (this._offset || 0)
        },
        unix: function() {
            return Math.floor(+this / 1e3)
        },
        toString: function() {
            return this.clone().lang("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")
        },
        toDate: function() {
            return this._offset ? new Date(+this) : this._d
        },
        toISOString: function() {
            var a = db(this).utc();
            return 0 < a.year() && a.year() <= 9999 ? F(a, "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]") : F(a, "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")
        },
        toArray: function() {
            var a = this;
            return [a.year(), a.month(), a.date(), a.hours(), a.minutes(), a.seconds(), a.milliseconds()]
        },
        isValid: function() {
            return x(this)
        },
        isDSTShifted: function() {
            return this._a ? this.isValid() && o(this._a, (this._isUTC ? db.utc(this._a) : db(this._a)).toArray()) > 0 : !1
        },
        parsingFlags: function() {
            return h({}, this._pf)
        },
        invalidAt: function() {
            return this._pf.overflow
        },
        utc: function() {
            return this.zone(0)
        },
        local: function() {
            return this.zone(0), this._isUTC = !1, this
        },
        format: function(a) {
            var b = F(this, a || db.defaultFormat);
            return this.lang().postformat(b)
        },
        add: function(a, b) {
            var c;
            return c = "string" == typeof a ? db.duration(+b, a) : db.duration(a, b), l(this, c, 1), this
        },
        subtract: function(a, b) {
            var c;
            return c = "string" == typeof a ? db.duration(+b, a) : db.duration(a, b), l(this, c, -1), this
        },
        diff: function(a, b, c) {
            var d, e, f = z(a, this),
                g = 6e4 * (this.zone() - f.zone());
            return b = p(b), "year" === b || "month" === b ? (d = 432e5 * (this.daysInMonth() + f.daysInMonth()), e = 12 * (this.year() - f.year()) + (this.month() - f.month()), e += (this - db(this).startOf("month") - (f - db(f).startOf("month"))) / d, e -= 6e4 * (this.zone() - db(this).startOf("month").zone() - (f.zone() - db(f).startOf("month").zone())) / d, "year" === b && (e /= 12)) : (d = this - f, e = "second" === b ? d / 1e3 : "minute" === b ? d / 6e4 : "hour" === b ? d / 36e5 : "day" === b ? (d - g) / 864e5 : "week" === b ? (d - g) / 6048e5 : d), c ? e : j(e)
        },
        from: function(a, b) {
            return db.duration(this.diff(a)).lang(this.lang()._abbr).humanize(!b)
        },
        fromNow: function(a) {
            return this.from(db(), a)
        },
        calendar: function() {
            var a = z(db(), this).startOf("day"),
                b = this.diff(a, "days", !0),
                c = -6 > b ? "sameElse" : -1 > b ? "lastWeek" : 0 > b ? "lastDay" : 1 > b ? "sameDay" : 2 > b ? "nextDay" : 7 > b ? "nextWeek" : "sameElse";
            return this.format(this.lang().calendar(c, this))
        },
        isLeapYear: function() {
            return v(this.year())
        },
        isDST: function() {
            return this.zone() < this.clone().month(0).zone() || this.zone() < this.clone().month(5).zone()
        },
        day: function(a) {
            var b = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
            return null != a ? (a = V(a, this.lang()), this.add({
                d: a - b
            })) : b
        },
        month: function(a) {
            var b, c = this._isUTC ? "UTC" : "";
            return null != a ? "string" == typeof a && (a = this.lang().monthsParse(a), "number" != typeof a) ? this : (b = this.date(), this.date(1), this._d["set" + c + "Month"](a), this.date(Math.min(b, this.daysInMonth())), db.updateOffset(this), this) : this._d["get" + c + "Month"]()
        },
        startOf: function(a) {
            switch (a = p(a)) {
                case "year":
                    this.month(0);
                case "month":
                    this.date(1);
                case "week":
                case "isoWeek":
                case "day":
                    this.hours(0);
                case "hour":
                    this.minutes(0);
                case "minute":
                    this.seconds(0);
                case "second":
                    this.milliseconds(0)
            }
            return "week" === a ? this.weekday(0) : "isoWeek" === a && this.isoWeekday(1), this
        },
        endOf: function(a) {
            return a = p(a), this.startOf(a).add("isoWeek" === a ? "week" : a, 1).subtract("ms", 1)
        },
        isAfter: function(a, b) {
            return b = "undefined" != typeof b ? b : "millisecond", +this.clone().startOf(b) > +db(a).startOf(b)
        },
        isBefore: function(a, b) {
            return b = "undefined" != typeof b ? b : "millisecond", +this.clone().startOf(b) < +db(a).startOf(b)
        },
        isSame: function(a, b) {
            return b = b || "ms", +this.clone().startOf(b) === +z(a, this).startOf(b)
        },
        min: function(a) {
            return a = db.apply(null, arguments), this > a ? this : a
        },
        max: function(a) {
            return a = db.apply(null, arguments), a > this ? this : a
        },
        zone: function(a) {
            var b = this._offset || 0;
            return null == a ? this._isUTC ? b : this._d.getTimezoneOffset() : ("string" == typeof a && (a = I(a)), Math.abs(a) < 16 && (a = 60 * a), this._offset = a, this._isUTC = !0, b !== a && l(this, db.duration(b - a, "m"), 1, !0), this)
        },
        zoneAbbr: function() {
            return this._isUTC ? "UTC" : ""
        },
        zoneName: function() {
            return this._isUTC ? "Coordinated Universal Time" : ""
        },
        parseZone: function() {
            return this._tzm ? this.zone(this._tzm) : "string" == typeof this._i && this.zone(this._i), this
        },
        hasAlignedHourOffset: function(a) {
            return a = a ? db(a).zone() : 0, (this.zone() - a) % 60 === 0
        },
        daysInMonth: function() {
            return t(this.year(), this.month())
        },
        dayOfYear: function(a) {
            var b = hb((db(this).startOf("day") - db(this).startOf("year")) / 864e5) + 1;
            return null == a ? b : this.add("d", a - b)
        },
        quarter: function() {
            return Math.ceil((this.month() + 1) / 3)
        },
        weekYear: function(a) {
            var b = Y(this, this.lang()._week.dow, this.lang()._week.doy).year;
            return null == a ? b : this.add("y", a - b)
        },
        isoWeekYear: function(a) {
            var b = Y(this, 1, 4).year;
            return null == a ? b : this.add("y", a - b)
        },
        week: function(a) {
            var b = this.lang().week(this);
            return null == a ? b : this.add("d", 7 * (a - b))
        },
        isoWeek: function(a) {
            var b = Y(this, 1, 4).week;
            return null == a ? b : this.add("d", 7 * (a - b))
        },
        weekday: function(a) {
            var b = (this.day() + 7 - this.lang()._week.dow) % 7;
            return null == a ? b : this.add("d", a - b)
        },
        isoWeekday: function(a) {
            return null == a ? this.day() || 7 : this.day(this.day() % 7 ? a : a - 7)
        },
        get: function(a) {
            return a = p(a), this[a]()
        },
        set: function(a, b) {
            return a = p(a), "function" == typeof this[a] && this[a](b), this
        },
        lang: function(b) {
            return b === a ? this._lang : (this._lang = C(b), this)
        }
    }), eb = 0; eb < Rb.length; eb++) _(Rb[eb].toLowerCase().replace(/s$/, ""), Rb[eb]);
    _("year", "FullYear"), db.fn.days = db.fn.day, db.fn.months = db.fn.month, db.fn.weeks = db.fn.week, db.fn.isoWeeks = db.fn.isoWeek, db.fn.toJSON = db.fn.toISOString, h(db.duration.fn = g.prototype, {
        _bubble: function() {
            var a, b, c, d, e = this._milliseconds,
                f = this._days,
                g = this._months,
                h = this._data;
            h.milliseconds = e % 1e3, a = j(e / 1e3), h.seconds = a % 60, b = j(a / 60), h.minutes = b % 60, c = j(b / 60), h.hours = c % 24, f += j(c / 24), h.days = f % 30, g += j(f / 30), h.months = g % 12, d = j(g / 12), h.years = d
        },
        weeks: function() {
            return j(this.days() / 7)
        },
        valueOf: function() {
            return this._milliseconds + 864e5 * this._days + this._months % 12 * 2592e6 + 31536e6 * s(this._months / 12)
        },
        humanize: function(a) {
            var b = +this,
                c = X(b, !a, this.lang());
            return a && (c = this.lang().pastFuture(b, c)), this.lang().postformat(c)
        },
        add: function(a, b) {
            var c = db.duration(a, b);
            return this._milliseconds += c._milliseconds, this._days += c._days, this._months += c._months, this._bubble(), this
        },
        subtract: function(a, b) {
            var c = db.duration(a, b);
            return this._milliseconds -= c._milliseconds, this._days -= c._days, this._months -= c._months, this._bubble(), this
        },
        get: function(a) {
            return a = p(a), this[a.toLowerCase() + "s"]()
        },
        as: function(a) {
            return a = p(a), this["as" + a.charAt(0).toUpperCase() + a.slice(1) + "s"]()
        },
        lang: db.fn.lang,
        toIsoString: function() {
            var a = Math.abs(this.years()),
                b = Math.abs(this.months()),
                c = Math.abs(this.days()),
                d = Math.abs(this.hours()),
                e = Math.abs(this.minutes()),
                f = Math.abs(this.seconds() + this.milliseconds() / 1e3);
            return this.asSeconds() ? (this.asSeconds() < 0 ? "-" : "") + "P" + (a ? a + "Y" : "") + (b ? b + "M" : "") + (c ? c + "D" : "") + (d || e || f ? "T" : "") + (d ? d + "H" : "") + (e ? e + "M" : "") + (f ? f + "S" : "") : "P0D"
        }
    });
    for (eb in Sb) Sb.hasOwnProperty(eb) && (bb(eb, Sb[eb]), ab(eb.toLowerCase()));
    bb("Weeks", 6048e5), db.duration.fn.asMonths = function() {
        return (+this - 31536e6 * this.years()) / 2592e6 + 12 * this.years()
    }, db.lang("en", {
        ordinal: function(a) {
            var b = a % 10,
                c = 1 === s(a % 100 / 10) ? "th" : 1 === b ? "st" : 2 === b ? "nd" : 3 === b ? "rd" : "th";
            return a + c
        }
    }), rb ? (module.exports = db, cb(!0)) : "function" == typeof define && define.amd ? define("moment", function(b, c, d) {
        return d.config && d.config() && d.config().noGlobal !== !0 && cb(d.config().noGlobal === a), db
    }) : cb()
}).call(this);
/*EOF /Scripts/moment/moment.min.js*/
/*/script/jquery/jquery.cookie.js*/
/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

/**
 * Create a cookie with the given name and value and other optional parameters.
 *
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Set the value of a cookie.
 * @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'jquery.com', secure: true });
 * @desc Create a cookie with all available options.
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Create a session cookie.
 * @example $.cookie('the_cookie', null);
 * @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
 *       used when the cookie was set.
 *
 * @param String name The name of the cookie.
 * @param String value The value of the cookie.
 * @param Object options An object literal containing key/value pairs to provide optional cookie attributes.
 * @option Number|Date expires Either an integer specifying the expiration date from now on in days or a Date object.
 *                             If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
 *                             If set to null or omitted, the cookie will be a session cookie and will not be retained
 *                             when the the browser exits.
 * @option String path The value of the path atribute of the cookie (default: path of page that created the cookie).
 * @option String domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
 * @option Boolean secure If true, the secure attribute of the cookie will be set and the cookie transmission will
 *                        require a secure protocol (like HTTPS).
 * @type undefined
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */

/**
 * Get the value of a cookie with the given name.
 *
 * @example $.cookie('the_cookie');
 * @desc Get the value of a cookie.
 *
 * @param String name The name of the cookie.
 * @return The value of the cookie.
 * @type String
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */
jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options = $.extend({}, options); // clone object since it's unexpected behavior if the expired property were changed
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // NOTE Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};
/*EOF /script/jquery/jquery.cookie.js*/
/*/Scripts/Roomster/Roomster.Main.js*/

if (Roomster === undefined) {
    var Roomster = {};
}

Roomster.JqueryWaiter = {
    _handlers: [],
    _timeout: null,
    Add: function(handler) {
        if (typeof(jQuery) == 'undefined') {
            if (this._timeout != null) {
                window.clearTimeout(this._timeout);
            }
            this._timeout = window.setTimeout(this._Run, 100);
            this._handlers.push(handler);
        } else {
            handler();
        }
    },
    _Run: function() {
        this._timeout = null;
        if (typeof(jQuery) == 'undefined') {
            this._timeout = window.setTimeout(this._Run, 100);
        } else {
            for (var i = 0, len = this._handlers.length; i < len; i++) {
                this._handlers[i]();
            }
            this._handlers = [];
        }
    }
};

Roomster.JqueryWaiter._Run = Roomster.JqueryWaiter._Run.apply(Roomster.JqueryWaiter);

//short alias for Roomster.JqueryWaiter.Add
Roomster.WaitJquery =
    function(handler) {
        Roomster.JqueryWaiter.Add(handler);
    };
Roomster.Proxy =
    function() {
        if (arguments.length < 2) {
            return;
        }
        for (var i = 0; i < arguments.length - 1; i++) {
            arguments[arguments.length - 1][arguments[i]] = $.proxy(arguments[arguments.length - 1], arguments[i]);
        }
    };
if (Roomster.Geo == undefined) {
    Roomster.Geo = {};
}
if (Roomster.Cookie == undefined) {
    Roomster.Cookie = {};
}
if (Roomster.Currency == undefined) {
    Roomster.Currency = {};
}
if (Roomster.Tags == undefined) {
    Roomster.Tags = {};
}
if (Roomster.SavedSearch == undefined) {
    Roomster.SavedSearch = {};
}
if (Roomster.SavedSearch.UI == undefined) {
    Roomster.SavedSearch.UI = {};
}
if (Roomster.Tags.UI == undefined) {
    Roomster.Tags.UI = {};
}
if (Roomster.Templates == undefined) {
    Roomster.Templates = {};
}
if (Roomster.Resources == undefined) {
    Roomster.Resources = {};
}
if (Roomster.Search == undefined) {
    Roomster.Search = {};
}
if (Roomster.Search.UI == undefined) {
    Roomster.Search.UI = {};
}
if (Roomster.DateTime == undefined) {
    Roomster.DateTime = {};
}
if (Roomster.UI == undefined) {
    Roomster.UI = {};
}

if (Roomster.Profile == undefined) {
    Roomster.Profile = {};
}
if (Roomster.Profile.UI == undefined) {
    Roomster.Profile.UI = {};
}

if (Roomster.Listing == undefined) {
    Roomster.Listing = {};
}

if (Roomster.Listing.UI == undefined) {
    Roomster.Listing.UI = {};
}

if (Roomster.Dialog == undefined) {
    Roomster.Dialog = {};
}

if (Roomster.HomePage == undefined) {
    Roomster.HomePage = {};
}
if (Roomster.HomePage.UI == undefined) {
    Roomster.HomePage.UI = {};
}

if (Roomster.Megaphone == undefined) {
    Roomster.Megaphone = {};
}

if (Roomster.Megaphone.UI == undefined) {
    Roomster.Megaphone.UI = {};
}

if (Roomster.Ads == undefined) {
    Roomster.Ads = {};
}

if (Roomster.Common == undefined) {
    Roomster.Common = {};
}

if (Roomster.Images == undefined)
    Roomster.Images = {};

if (Roomster.Mailbox == undefined)
    Roomster.Mailbox = {};

if (Roomster.Mailbox.UI == undefined)
    Roomster.Mailbox.UI = {};

if (Roomster.Affiliate == undefined)
    Roomster.Affiliate = {};

if (Roomster.Finance == undefined)
    Roomster.Finance = {};

if (Roomster.Finance.UI == undefined)
    Roomster.Finance.UI = {};

Roomster.WaitJquery(
    function() {
        $.extend(
            Roomster, {
                _ErrorMessageDelay: 2000,
                _FadeTimeout: 500,
                ShowError: function(message) {
                    if (this._MessageBox != undefined) {
                        this._MessageBox.html(message);
                        this._MessageBox.center();
                        this._MessageBox.fadeIn(this._FadeTimeout).delay(this._ErrorMessageDelay).fadeOut(this._FadeTimeout);
                    }
                },
                CloseError: function() {
                    this._MessageBox.hide();
                }
            });
        Roomster.CloseError = $.proxy(Roomster.CloseError, Roomster);
    });

Roomster.WaitJquery(
    function() {
        $.fn.extend({
            FirstOrDefault: function(n) {
                if (this.length == 0) {
                    return null;
                }
                var collection = this;
                var isFunc = $.isFunction(n);
                for (var i = 0, cnt = collection.length; i < cnt; i++) {
                    if (isFunc ? n(collection[i]) : n == collection[i]) {
                        return collection[i];
                    }
                }
                return null;
            },
            ElementAtJson: function(lookupIndex) {
                var index = 0;
                for (var name in this[0]) {
                    if (lookupIndex == index) {
                        return this[0][name];
                    }
                    index++;
                }
                return null;
            },
            Any: function() {
                for (var name in this[0]) {
                    return true;
                }
                return false;
            }

        });
    });

Roomster.Extend =
    function(Child, Parent) {
        var F = function() {};
        F.prototype = Parent.prototype;
        Child.prototype = new F();
        Child.prototype.constructor = Child;
        Child.superclass = Parent.prototype;
    };
/*EOF /Scripts/Roomster/Roomster.Main.js*/
/*/Scripts/Roomster/Roomster.Resources.js*/
(function(ctx) {
    var it = Roomster.Resources = Roomster.Resources || {};

    $.extend(it, {
        Loaded: false,
        ResourcesList: {},
        Get: function(n, k) {
            var resource = it.ResourcesList[n];
            if (resource == null) {
                return null;
            }
            var result = $(resource).FirstOrDefault(function(s) {
                return s.Id == k;
            });
            return result == null ? (resource[k] ? resource[k] : null) : result.Name;
        },
        GetTranslation: function(s) {
            var result = it.Get('Localization', s);
            return result != null ? result : s;
        },
        GetByValue: function(n, k) {
            var resource = this.ResourcesList[n];
            if (resource == null) {
                return null;
            }
            var result = $(resource).FirstOrDefault(function(s) {
                return s.Value == k;
            });
            return result == null ? null : result.Name;
        },
        GetValues: function(n) {
            return it.ResourcesList[n];
        },
        GetId: function(n, k) {
            var resource = it.ResourcesList[n];
            if (resource == null) {
                return null;
            }
            var result = $(resource).FirstOrDefault(function(s) {
                return s.Name == k;
            });
            return result == null ? (resource[k] ? resource[k] : null) : result.Id;
        },
        Load: function() {
            if (it.Loaded) {
                return;
            }
            var url = "/Resources/Get?lsid=" + $.cookie("Roomster.Globalization") + '&' + it.SERVER_BUILD_DATE_IN_TICKS || 0;
            document.write('<s' + 'cript lang' + 'uage="jav' + 'ascript" src="' + url + '"></' + 'scr' + 'ipt>');
            it.Loaded = true;
            //            $.ajax({
            //                url: "/Resources/Get?lsid=" + $.cookie("Roomster.Globalization") +'&'+ it.SERVER_BUILD_DATE_IN_TICKS || 0,
            //                dataType: 'json',
            //                success: function (data) {
            //                    it.ResourcesList = data;
            //                },
            //                error: function (request, textStatus, errorThrown) {
            //                    console.log(errorThrown);
            //                }
            //            });
        }
    });

    it.Load();

})(Roomster);

/*EOF /Scripts/Roomster/Roomster.Resources.js*/
/*/Scripts/Roomster/Roomster.Console.js*/
(function(ctx) {
    ctx.console = ctx.console || {
        log: function() {}
    };
})(window);

/*EOF /Scripts/Roomster/Roomster.Console.js*/
/*/Scripts/Roomster/Roomster.Facebook.js*/
/*facebook firefox hack*/
if (window.location.hash && window.location.hash == '#_=_') {
    window.location.hash = '';
}
/*EOF /Scripts/Roomster/Roomster.Facebook.js*/
/*/Scripts/native.history.js*/
/*
    json2.js
    2012-10-08

    Public Domain.

    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.

    See http://www.JSON.org/js.html

    This code should be minified before deployment.
    See http://javascript.crockford.com/jsmin.html

    USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
    NOT CONTROL.

    This file creates a global JSON object containing two methods: stringify
    and parse.

        JSON.stringify(value, replacer, space)
            value       any JavaScript value, usually an object or array.

            replacer    an optional parameter that determines how object
                        values are stringified for objects. It can be a
                        function or an array of strings.

            space       an optional parameter that specifies the indentation
                        of nested structures. If it is omitted, the text will
                        be packed without extra whitespace. If it is a number,
                        it will specify the number of spaces to indent at each
                        level. If it is a string (such as '\t' or '&nbsp;'),
                        it contains the characters used to indent at each level.

            This method produces a JSON text from a JavaScript value.

            When an object value is found, if the object contains a toJSON
            method, its toJSON method will be called and the result will be
            stringified. A toJSON method does not serialize: it returns the
            value represented by the name/value pair that should be serialized,
            or undefined if nothing should be serialized. The toJSON method
            will be passed the key associated with the value, and this will be
            bound to the value

            For example, this would serialize Dates as ISO strings.

                Date.prototype.toJSON = function (key) {
                    function f(n) {
                        // Format integers to have at least two digits.
                        return n < 10 ? '0' + n : n;
                    }

                    return this.getUTCFullYear()   + '-' +
                         f(this.getUTCMonth() + 1) + '-' +
                         f(this.getUTCDate())      + 'T' +
                         f(this.getUTCHours())     + ':' +
                         f(this.getUTCMinutes())   + ':' +
                         f(this.getUTCSeconds())   + 'Z';
                };

            You can provide an optional replacer method. It will be passed the
            key and value of each member, with this bound to the containing
            object. The value that is returned from your method will be
            serialized. If your method returns undefined, then the member will
            be excluded from the serialization.

            If the replacer parameter is an array of strings, then it will be
            used to select the members to be serialized. It filters the results
            such that only members with keys listed in the replacer array are
            stringified.

            Values that do not have JSON representations, such as undefined or
            functions, will not be serialized. Such values in objects will be
            dropped; in arrays they will be replaced with null. You can use
            a replacer function to replace those with JSON values.
            JSON.stringify(undefined) returns undefined.

            The optional space parameter produces a stringification of the
            value that is filled with line breaks and indentation to make it
            easier to read.

            If the space parameter is a non-empty string, then that string will
            be used for indentation. If the space parameter is a number, then
            the indentation will be that many spaces.

            Example:

            text = JSON.stringify(['e', {pluribus: 'unum'}]);
            // text is '["e",{"pluribus":"unum"}]'

            text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
            // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'

            text = JSON.stringify([new Date()], function (key, value) {
                return this[key] instanceof Date ?
                    'Date(' + this[key] + ')' : value;
            });
            // text is '["Date(---current time---)"]'

        JSON.parse(text, reviver)
            This method parses a JSON text to produce an object or array.
            It can throw a SyntaxError exception.

            The optional reviver parameter is a function that can filter and
            transform the results. It receives each of the keys and values,
            and its return value is used instead of the original value.
            If it returns what it received, then the structure is not modified.
            If it returns undefined then the member is deleted.

            Example:

            // Parse the text. Values that look like ISO date strings will
            // be converted to Date objects.

            myData = JSON.parse(text, function (key, value) {
                var a;
                if (typeof value === 'string') {
                    a =
/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                    if (a) {
                        return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                            +a[5], +a[6]));
                    }
                }
                return value;
            });

            myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
                var d;
                if (typeof value === 'string' &&
                        value.slice(0, 5) === 'Date(' &&
                        value.slice(-1) === ')') {
                    d = new Date(value.slice(5, -1));
                    if (d) {
                        return d;
                    }
                }
                return value;
            });

    This is a reference implementation. You are free to copy, modify, or
    redistribute.
*/

/*jslint evil: true, regexp: true */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
    call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
    getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
    lastIndex, length, parse, prototype, push, replace, slice, stringify,
    test, toJSON, toString, valueOf
*/

// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

if (typeof JSON !== 'object') {
    JSON = {};
}

(function() {
    'use strict';

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function(key) {

            return isFinite(this.valueOf()) ? this.getUTCFullYear() + '-' +
                f(this.getUTCMonth() + 1) + '-' +
                f(this.getUTCDate()) + 'T' +
                f(this.getUTCHours()) + ':' +
                f(this.getUTCMinutes()) + ':' +
                f(this.getUTCSeconds()) + 'Z' : null;
        };

        String.prototype.toJSON =
            Number.prototype.toJSON =
                Boolean.prototype.toJSON = function(key) {
                    return this.valueOf();
                };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = { // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"': '\\"',
            '\\': '\\\\'
        },
        rep;

    function quote(string) {

        // If the string contains no control characters, no quote characters, and no
        // backslash characters, then we can safely slap some quotes around it.
        // Otherwise we must also replace the offending characters with safe escape
        // sequences.

        escapable.lastIndex = 0;
        return escapable.test(string) ? '"' + string.replace(escapable, function(a) {
            var c = meta[a];
            return typeof c === 'string' ? c : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
        }) + '"' : '"' + string + '"';
    }

    function str(key, holder) {

        // Produce a string from holder[key].

        var i, // The loop counter.
            k, // The member key.
            v, // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

        // If the value has a toJSON method, call it to obtain a replacement value.

        if (value && typeof value === 'object' &&
            typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }

        // If we were called with a replacer function, then call the replacer to
        // obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

        // What happens next depends on the value's type.

        switch (typeof value) {
            case 'string':
                return quote(value);

            case 'number':

                // JSON numbers must be finite. Encode non-finite numbers as null.

                return isFinite(value) ? String(value) : 'null';

            case 'boolean':
            case 'null':

                // If the value is a boolean or null, convert it to a string. Note:
                // typeof null does not produce 'null'. The case is included here in
                // the remote chance that this gets fixed someday.

                return String(value);

            // If the type is 'object', we might be dealing with an object or an array or
            // null.

            case 'object':

                // Due to a specification blunder in ECMAScript, typeof null is 'object',
                // so watch out for that case.

                if (!value) {
                    return 'null';
                }

                // Make an array to hold the partial results of stringifying this object value.

                gap += indent;
                partial = [];

                // Is the value an array?

                if (Object.prototype.toString.apply(value) === '[object Array]') {

                    // The value is an array. Stringify every element. Use null as a placeholder
                    // for non-JSON values.

                    length = value.length;
                    for (i = 0; i < length; i += 1) {
                        partial[i] = str(i, value) || 'null';
                    }

                    // Join all of the elements together, separated with commas, and wrap them in
                    // brackets.

                    v = partial.length === 0 ? '[]' : gap ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']' : '[' + partial.join(',') + ']';
                    gap = mind;
                    return v;
                }

                // If the replacer is an array, use it to select the members to be stringified.

                if (rep && typeof rep === 'object') {
                    length = rep.length;
                    for (i = 0; i < length; i += 1) {
                        if (typeof rep[i] === 'string') {
                            k = rep[i];
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                } else {

                    // Otherwise, iterate through all of the keys in the object.

                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                }

                // Join all of the member texts together, separated with commas,
                // and wrap them in braces.

                v = partial.length === 0 ? '{}' : gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' : '{' + partial.join(',') + '}';
                gap = mind;
                return v;
        }
    }

    // If the JSON object does not yet have a stringify method, give it one.

    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function(value, replacer, space) {

            // The stringify method takes a value and an optional replacer, and an optional
            // space parameter, and returns a JSON text. The replacer can be a function
            // that can replace values, or an array of strings that will select the keys.
            // A default replacer method can be provided. Use of the space parameter can
            // produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

            // If the space parameter is a number, make an indent string containing that
            // many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

                // If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

            // If there is a replacer, it must be a function or an array.
            // Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                (typeof replacer !== 'object' ||
                    typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

            // Make a fake root object containing our value under the key of ''.
            // Return the result of stringifying the value.

            return str('', {
                '': value
            });
        };
    }

    // If the JSON object does not yet have a parse method, give it one.

    if (typeof JSON.parse !== 'function') {
        JSON.parse = function(text, reviver) {

            // The parse method takes a text and an optional reviver function, and returns
            // a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

                // The walk method is used to recursively walk the resulting structure so
                // that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }

            // Parsing happens in four stages. In the first stage, we replace certain
            // Unicode characters with escape sequences. JavaScript handles many characters
            // incorrectly, either silently deleting them, or treating them as line endings.

            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function(a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

            // In the second stage, we run the text against regular expressions that look
            // for non-JSON patterns. We are especially concerned with '()' and 'new'
            // because they can cause invocation, and '=' because it can cause mutation.
            // But just to be safe, we want to reject all unexpected forms.

            // We split the second stage into 4 regexp operations in order to work around
            // crippling inefficiencies in IE's and Safari's regexp engines. First we
            // replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
            // replace all simple value tokens with ']' characters. Third, we delete all
            // open brackets that follow a colon or comma or that begin the text. Finally,
            // we look to see that the remaining characters are only whitespace or ']' or
            // ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$/
                .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                    .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                    .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

                // In the third stage we use the eval function to compile the text into a
                // JavaScript structure. The '{' operator is subject to a syntactic ambiguity
                // in JavaScript: it can begin a block or an object literal. We wrap the text
                // in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

                // In the optional fourth stage, we recursively walk the new structure, passing
                // each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function' ? walk({
                    '': j
                }, '') : j;
            }

            // If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON.parse');
        };
    }
}());
/**
 * History.js Native Adapter
 * @author Benjamin Arthur Lupton <contact@balupton.com>
 * @copyright 2010-2011 Benjamin Arthur Lupton <contact@balupton.com>
 * @license New BSD License <http://creativecommons.org/licenses/BSD/>
 */

// Closure
(function(window, undefined) {
    "use strict";

    // Localise Globals
    var History = window.History = window.History || {};

    // Check Existence
    if (typeof History.Adapter !== 'undefined') {
        throw new Error('History.js Adapter has already been loaded...');
    }

    // Add the Adapter
    History.Adapter = {
        /**
         * History.Adapter.handlers[uid][eventName] = Array
         */
        handlers: {},

        /**
         * History.Adapter._uid
         * The current element unique identifier
         */
        _uid: 1,

        /**
         * History.Adapter.uid(element)
         * @param {Element} element
         * @return {String} uid
         */
        uid: function(element) {
            return element._uid || (element._uid = History.Adapter._uid++);
        },

        /**
         * History.Adapter.bind(el,event,callback)
         * @param {Element} element
         * @param {String} eventName - custom and standard events
         * @param {Function} callback
         * @return
         */
        bind: function(element, eventName, callback) {
            // Prepare
            var uid = History.Adapter.uid(element);

            // Apply Listener
            History.Adapter.handlers[uid] = History.Adapter.handlers[uid] || {};
            History.Adapter.handlers[uid][eventName] = History.Adapter.handlers[uid][eventName] || [];
            History.Adapter.handlers[uid][eventName].push(callback);

            // Bind Global Listener
            element['on' + eventName] = (function(element, eventName) {
                return function(event) {
                    History.Adapter.trigger(element, eventName, event);
                };
            })(element, eventName);
        },

        /**
         * History.Adapter.trigger(el,event)
         * @param {Element} element
         * @param {String} eventName - custom and standard events
         * @param {Object} event - a object of event data
         * @return
         */
        trigger: function(element, eventName, event) {
            // Prepare
            event = event || {};
            var uid = History.Adapter.uid(element),
                i, n;

            // Apply Listener
            History.Adapter.handlers[uid] = History.Adapter.handlers[uid] || {};
            History.Adapter.handlers[uid][eventName] = History.Adapter.handlers[uid][eventName] || [];

            // Fire Listeners
            for (i = 0, n = History.Adapter.handlers[uid][eventName].length; i < n; ++i) {
                History.Adapter.handlers[uid][eventName][i].apply(this, [event]);
            }
        },

        /**
         * History.Adapter.extractEventData(key,event,extra)
         * @param {String} key - key for the event data to extract
         * @param {String} event - custom and standard events
         * @return {mixed}
         */
        extractEventData: function(key, event) {
            var result = (event && event[key]) || undefined;
            return result;
        },

        /**
         * History.Adapter.onDomLoad(callback)
         * @param {Function} callback
         * @return
         */
        onDomLoad: function(callback) {
            var timeout = window.setTimeout(function() {
                callback();
            }, 2000);
            window.onload = function() {
                clearTimeout(timeout);
                callback();
            };
        }
    };

    // Try to Initialise History
    if (typeof History.init !== 'undefined') {
        History.init();
    }

})(window);
/**
 * History.js HTML4 Support
 * Depends on the HTML5 Support
 * @author Benjamin Arthur Lupton <contact@balupton.com>
 * @copyright 2010-2011 Benjamin Arthur Lupton <contact@balupton.com>
 * @license New BSD License <http://creativecommons.org/licenses/BSD/>
 */

(function(window, undefined) {
    "use strict";

    // ========================================================================
    // Initialise

    // Localise Globals
    var
        document = window.document, // Make sure we are using the correct document
        setTimeout = window.setTimeout || setTimeout,
        clearTimeout = window.clearTimeout || clearTimeout,
        setInterval = window.setInterval || setInterval,
        History = window.History = window.History || {}; // Public History Object

    // Check Existence
    if (typeof History.initHtml4 !== 'undefined') {
        throw new Error('History.js HTML4 Support has already been loaded...');
    }

    // ========================================================================
    // Initialise HTML4 Support

    // Initialise HTML4 Support
    History.initHtml4 = function() {
        // Initialise
        if (typeof History.initHtml4.initialized !== 'undefined') {
            // Already Loaded
            return false;
        } else {
            History.initHtml4.initialized = true;
        }

        // ====================================================================
        // Properties

        /**
         * History.enabled
         * Is History enabled?
         */
        History.enabled = true;

        // ====================================================================
        // Hash Storage

        /**
         * History.savedHashes
         * Store the hashes in an array
         */
        History.savedHashes = [];

        /**
         * History.isLastHash(newHash)
         * Checks if the hash is the last hash
         * @param {string} newHash
         * @return {boolean} true
         */
        History.isLastHash = function(newHash) {
            // Prepare
            var oldHash = History.getHashByIndex(),
                isLast;

            // Check
            isLast = newHash === oldHash;

            // Return isLast
            return isLast;
        };

        /**
         * History.isHashEqual(newHash, oldHash)
         * Checks to see if two hashes are functionally equal
         * @param {string} newHash
         * @param {string} oldHash
         * @return {boolean} true
         */
        History.isHashEqual = function(newHash, oldHash) {
            newHash = encodeURIComponent(newHash).replace(/%25/g, "%");
            oldHash = encodeURIComponent(oldHash).replace(/%25/g, "%");
            return newHash === oldHash;
        };

        /**
         * History.saveHash(newHash)
         * Push a Hash
         * @param {string} newHash
         * @return {boolean} true
         */
        History.saveHash = function(newHash) {
            // Check Hash
            if (History.isLastHash(newHash)) {
                return false;
            }

            // Push the Hash
            History.savedHashes.push(newHash);

            // Return true
            return true;
        };

        /**
         * History.getHashByIndex()
         * Gets a hash by the index
         * @param {integer} index
         * @return {string}
         */
        History.getHashByIndex = function(index) {
            // Prepare
            var hash = null;

            // Handle
            if (typeof index === 'undefined') {
                // Get the last inserted
                hash = History.savedHashes[History.savedHashes.length - 1];
            } else if (index < 0) {
                // Get from the end
                hash = History.savedHashes[History.savedHashes.length + index];
            } else {
                // Get from the beginning
                hash = History.savedHashes[index];
            }

            // Return hash
            return hash;
        };

        // ====================================================================
        // Discarded States

        /**
         * History.discardedHashes
         * A hashed array of discarded hashes
         */
        History.discardedHashes = {};

        /**
         * History.discardedStates
         * A hashed array of discarded states
         */
        History.discardedStates = {};

        /**
         * History.discardState(State)
         * Discards the state by ignoring it through History
         * @param {object} State
         * @return {true}
         */
        History.discardState = function(discardedState, forwardState, backState) {
            //History.debug('History.discardState', arguments);
            // Prepare
            var discardedStateHash = History.getHashByState(discardedState),
                discardObject;

            // Create Discard Object
            discardObject = {
                'discardedState': discardedState,
                'backState': backState,
                'forwardState': forwardState
            };

            // Add to DiscardedStates
            History.discardedStates[discardedStateHash] = discardObject;

            // Return true
            return true;
        };

        /**
         * History.discardHash(hash)
         * Discards the hash by ignoring it through History
         * @param {string} hash
         * @return {true}
         */
        History.discardHash = function(discardedHash, forwardState, backState) {
            //History.debug('History.discardState', arguments);
            // Create Discard Object
            var discardObject = {
                'discardedHash': discardedHash,
                'backState': backState,
                'forwardState': forwardState
            };

            // Add to discardedHash
            History.discardedHashes[discardedHash] = discardObject;

            // Return true
            return true;
        };

        /**
         * History.discardedState(State)
         * Checks to see if the state is discarded
         * @param {object} State
         * @return {bool}
         */
        History.discardedState = function(State) {
            // Prepare
            var StateHash = History.getHashByState(State),
                discarded;

            // Check
            discarded = History.discardedStates[StateHash] || false;

            // Return true
            return discarded;
        };

        /**
         * History.discardedHash(hash)
         * Checks to see if the state is discarded
         * @param {string} State
         * @return {bool}
         */
        History.discardedHash = function(hash) {
            // Check
            var discarded = History.discardedHashes[hash] || false;

            // Return true
            return discarded;
        };

        /**
         * History.recycleState(State)
         * Allows a discarded state to be used again
         * @param {object} data
         * @param {string} title
         * @param {string} url
         * @return {true}
         */
        History.recycleState = function(State) {
            //History.debug('History.recycleState', arguments);
            // Prepare
            var StateHash = History.getHashByState(State);

            // Remove from DiscardedStates
            if (History.discardedState(State)) {
                delete History.discardedStates[StateHash];
            }

            // Return true
            return true;
        };

        // ====================================================================
        // HTML4 HashChange Support

        if (History.emulated.hashChange) {
            /*
             * We must emulate the HTML4 HashChange Support by manually checking for hash changes
             */

            /**
             * History.hashChangeInit()
             * Init the HashChange Emulation
             */
            History.hashChangeInit = function() {
                // Define our Checker Function
                History.checkerFunction = null;

                // Define some variables that will help in our checker function
                var lastDocumentHash = '',
                    iframeId, iframe,
                    lastIframeHash, checkerRunning,
                    startedWithHash = Boolean(History.getHash());

                // Handle depending on the browser
                if (History.isInternetExplorer()) {
                    // IE6 and IE7
                    // We need to use an iframe to emulate the back and forward buttons

                    // Create iFrame
                    iframeId = 'historyjs-iframe';
                    iframe = document.createElement('iframe');

                    // Adjust iFarme
                    // IE 6 requires iframe to have a src on HTTPS pages, otherwise it will throw a
                    // "This page contains both secure and nonsecure items" warning.
                    iframe.setAttribute('id', iframeId);
                    iframe.setAttribute('src', '#');
                    iframe.style.display = 'none';

                    // Append iFrame
                    document.body.appendChild(iframe);

                    // Create initial history entry
                    iframe.contentWindow.document.open();
                    iframe.contentWindow.document.close();

                    // Define some variables that will help in our checker function
                    lastIframeHash = '';
                    checkerRunning = false;

                    // Define the checker function
                    History.checkerFunction = function() {
                        // Check Running
                        if (checkerRunning) {
                            return false;
                        }

                        // Update Running
                        checkerRunning = true;

                        // Fetch
                        var
                            documentHash = History.getHash(),
                            iframeHash = History.getHash(iframe.contentWindow.document);

                        // The Document Hash has changed (application caused)
                        if (documentHash !== lastDocumentHash) {
                            // Equalise
                            lastDocumentHash = documentHash;

                            // Create a history entry in the iframe
                            if (iframeHash !== documentHash) {
                                //History.debug('hashchange.checker: iframe hash change', 'documentHash (new):', documentHash, 'iframeHash (old):', iframeHash);

                                // Equalise
                                lastIframeHash = iframeHash = documentHash;

                                // Create History Entry
                                iframe.contentWindow.document.open();
                                iframe.contentWindow.document.close();

                                // Update the iframe's hash
                                iframe.contentWindow.document.location.hash = History.escapeHash(documentHash);
                            }

                            // Trigger Hashchange Event
                            History.Adapter.trigger(window, 'hashchange');
                        }

                        // The iFrame Hash has changed (back button caused)
                        else if (iframeHash !== lastIframeHash) {
                            //History.debug('hashchange.checker: iframe hash out of sync', 'iframeHash (new):', iframeHash, 'documentHash (old):', documentHash);

                            // Equalise
                            lastIframeHash = iframeHash;

                            // If there is no iframe hash that means we're at the original
                            // iframe state.
                            // And if there was a hash on the original request, the original
                            // iframe state was replaced instantly, so skip this state and take
                            // the user back to where they came from.
                            if (startedWithHash && iframeHash === '') {
                                History.back();
                            } else {
                                // Update the Hash
                                History.setHash(iframeHash, false);
                            }
                        }

                        // Reset Running
                        checkerRunning = false;

                        // Return true
                        return true;
                    };
                } else {
                    // We are not IE
                    // Firefox 1 or 2, Opera

                    // Define the checker function
                    History.checkerFunction = function() {
                        // Prepare
                        var documentHash = History.getHash() || '';

                        // The Document Hash has changed (application caused)
                        if (documentHash !== lastDocumentHash) {
                            // Equalise
                            lastDocumentHash = documentHash;

                            // Trigger Hashchange Event
                            History.Adapter.trigger(window, 'hashchange');
                        }

                        // Return true
                        return true;
                    };
                }

                // Apply the checker function
                History.intervalList.push(setInterval(History.checkerFunction, History.options.hashChangeInterval));

                // Done
                return true;
            }; // History.hashChangeInit

            // Bind hashChangeInit
            History.Adapter.onDomLoad(History.hashChangeInit);

        } // History.emulated.hashChange

        // ====================================================================
        // HTML5 State Support

        // Non-Native pushState Implementation
        if (History.emulated.pushState) {
            /*
             * We must emulate the HTML5 State Management by using HTML4 HashChange
             */

            /**
             * History.onHashChange(event)
             * Trigger HTML5's window.onpopstate via HTML4 HashChange Support
             */
            History.onHashChange = function(event) {
                //History.debug('History.onHashChange', arguments);

                // Prepare
                var currentUrl = ((event && event.newURL) || History.getLocationHref()),
                    currentHash = History.getHashByUrl(currentUrl),
                    currentState = null,
                    currentStateHash = null,
                    currentStateHashExits = null,
                    discardObject;

                // Check if we are the same state
                if (History.isLastHash(currentHash)) {
                    // There has been no change (just the page's hash has finally propagated)
                    //History.debug('History.onHashChange: no change');
                    History.busy(false);
                    return false;
                }

                // Reset the double check
                History.doubleCheckComplete();

                // Store our location for use in detecting back/forward direction
                History.saveHash(currentHash);

                // Expand Hash
                if (currentHash && History.isTraditionalAnchor(currentHash)) {
                    //History.debug('History.onHashChange: traditional anchor', currentHash);
                    // Traditional Anchor Hash
                    History.Adapter.trigger(window, 'anchorchange');
                    History.busy(false);
                    return false;
                }

                // Create State
                currentState = History.extractState(History.getFullUrl(currentHash || History.getLocationHref()), true);

                // Check if we are the same state
                if (History.isLastSavedState(currentState)) {
                    //History.debug('History.onHashChange: no change');
                    // There has been no change (just the page's hash has finally propagated)
                    History.busy(false);
                    return false;
                }

                // Create the state Hash
                currentStateHash = History.getHashByState(currentState);

                // Check if we are DiscardedState
                discardObject = History.discardedState(currentState);
                if (discardObject) {
                    // Ignore this state as it has been discarded and go back to the state before it
                    if (History.getHashByIndex(-2) === History.getHashByState(discardObject.forwardState)) {
                        // We are going backwards
                        //History.debug('History.onHashChange: go backwards');
                        History.back(false);
                    } else {
                        // We are going forwards
                        //History.debug('History.onHashChange: go forwards');
                        History.forward(false);
                    }
                    return false;
                }

                // Push the new HTML5 State
                //History.debug('History.onHashChange: success hashchange');
                History.pushState(currentState.data, currentState.title, encodeURI(currentState.url), false);

                // End onHashChange closure
                return true;
            };
            History.Adapter.bind(window, 'hashchange', History.onHashChange);

            /**
             * History.pushState(data,title,url)
             * Add a new State to the history object, become it, and trigger onpopstate
             * We have to trigger for HTML4 compatibility
             * @param {object} data
             * @param {string} title
             * @param {string} url
             * @return {true}
             */
            History.pushState = function(data, title, url, queue) {
                //History.debug('History.pushState: called', arguments);

                // We assume that the URL passed in is URI-encoded, but this makes
                // sure that it's fully URI encoded; any '%'s that are encoded are
                // converted back into '%'s
                url = encodeURI(url).replace(/%25/g, "%");

                // Check the State
                if (History.getHashByUrl(url)) {
                    throw new Error('History.js does not support states with fragment-identifiers (hashes/anchors).');
                }

                // Handle Queueing
                if (queue !== false && History.busy()) {
                    // Wait + Push to Queue
                    //History.debug('History.pushState: we must wait', arguments);
                    History.pushQueue({
                        scope: History,
                        callback: History.pushState,
                        args: arguments,
                        queue: queue
                    });
                    return false;
                }

                // Make Busy
                History.busy(true);

                // Fetch the State Object
                var newState = History.createStateObject(data, title, url),
                    newStateHash = History.getHashByState(newState),
                    oldState = History.getState(false),
                    oldStateHash = History.getHashByState(oldState),
                    html4Hash = History.getHash(),
                    wasExpected = History.expectedStateId == newState.id;

                // Store the newState
                History.storeState(newState);
                History.expectedStateId = newState.id;

                // Recycle the State
                History.recycleState(newState);

                // Force update of the title
                History.setTitle(newState);

                // Check if we are the same State
                if (newStateHash === oldStateHash) {
                    //History.debug('History.pushState: no change', newStateHash);
                    History.busy(false);
                    return false;
                }

                // Update HTML5 State
                History.saveState(newState);

                // Fire HTML5 Event
                if (!wasExpected)
                    History.Adapter.trigger(window, 'statechange');

                // Update HTML4 Hash
                if (!History.isHashEqual(newStateHash, html4Hash) && !History.isHashEqual(newStateHash, History.getShortUrl(History.getLocationHref()))) {
                    History.setHash(newStateHash, false);
                }

                History.busy(false);

                // End pushState closure
                return true;
            };

            /**
             * History.replaceState(data,title,url)
             * Replace the State and trigger onpopstate
             * We have to trigger for HTML4 compatibility
             * @param {object} data
             * @param {string} title
             * @param {string} url
             * @return {true}
             */
            History.replaceState = function(data, title, url, queue) {
                //History.debug('History.replaceState: called', arguments);

                // We assume that the URL passed in is URI-encoded, but this makes
                // sure that it's fully URI encoded; any '%'s that are encoded are
                // converted back into '%'s
                url = encodeURI(url).replace(/%25/g, "%");

                // Check the State
                if (History.getHashByUrl(url)) {
                    throw new Error('History.js does not support states with fragment-identifiers (hashes/anchors).');
                }

                // Handle Queueing
                if (queue !== false && History.busy()) {
                    // Wait + Push to Queue
                    //History.debug('History.replaceState: we must wait', arguments);
                    History.pushQueue({
                        scope: History,
                        callback: History.replaceState,
                        args: arguments,
                        queue: queue
                    });
                    return false;
                }

                // Make Busy
                History.busy(true);

                // Fetch the State Objects
                var newState = History.createStateObject(data, title, url),
                    newStateHash = History.getHashByState(newState),
                    oldState = History.getState(false),
                    oldStateHash = History.getHashByState(oldState),
                    previousState = History.getStateByIndex(-2);

                // Discard Old State
                History.discardState(oldState, newState, previousState);

                // If the url hasn't changed, just store and save the state
                // and fire a statechange event to be consistent with the
                // html 5 api
                if (newStateHash === oldStateHash) {
                    // Store the newState
                    History.storeState(newState);
                    History.expectedStateId = newState.id;

                    // Recycle the State
                    History.recycleState(newState);

                    // Force update of the title
                    History.setTitle(newState);

                    // Update HTML5 State
                    History.saveState(newState);

                    // Fire HTML5 Event
                    //History.debug('History.pushState: trigger popstate');
                    History.Adapter.trigger(window, 'statechange');
                    History.busy(false);
                } else {
                    // Alias to PushState
                    History.pushState(newState.data, newState.title, newState.url, false);
                }

                // End replaceState closure
                return true;
            };

        } // History.emulated.pushState

        // ====================================================================
        // Initialise

        // Non-Native pushState Implementation
        if (History.emulated.pushState) {
            /**
             * Ensure initial state is handled correctly
             */
            if (History.getHash() && !History.emulated.hashChange) {
                History.Adapter.onDomLoad(function() {
                    History.Adapter.trigger(window, 'hashchange');
                });
            }

        } // History.emulated.pushState

    }; // History.initHtml4

    // Try to Initialise History
    if (typeof History.init !== 'undefined') {
        History.init();
    }

})(window);
/**
 * History.js Core
 * @author Benjamin Arthur Lupton <contact@balupton.com>
 * @copyright 2010-2011 Benjamin Arthur Lupton <contact@balupton.com>
 * @license New BSD License <http://creativecommons.org/licenses/BSD/>
 */

(function(window, undefined) {
    "use strict";

    // ========================================================================
    // Initialise

    // Localise Globals
    var
        console = window.console || undefined, // Prevent a JSLint complain
        document = window.document, // Make sure we are using the correct document
        navigator = window.navigator, // Make sure we are using the correct navigator
        sessionStorage = window.sessionStorage || false, // sessionStorage
        setTimeout = window.setTimeout,
        clearTimeout = window.clearTimeout,
        setInterval = window.setInterval,
        clearInterval = window.clearInterval,
        JSON = window.JSON,
        alert = window.alert,
        History = window.History = window.History || {}, // Public History Object
        history = window.history; // Old History Object

    try {
        sessionStorage.setItem('TEST', '1');
        sessionStorage.removeItem('TEST');
    } catch (e) {
        sessionStorage = false;
    }

    // MooTools Compatibility
    JSON.stringify = JSON.stringify || JSON.encode;
    JSON.parse = JSON.parse || JSON.decode;

    // Check Existence
    if (typeof History.init !== 'undefined') {
        throw new Error('History.js Core has already been loaded...');
    }

    // Initialise History
    History.init = function(options) {
        // Check Load Status of Adapter
        if (typeof History.Adapter === 'undefined') {
            return false;
        }

        // Check Load Status of Core
        if (typeof History.initCore !== 'undefined') {
            History.initCore();
        }

        // Check Load Status of HTML4 Support
        if (typeof History.initHtml4 !== 'undefined') {
            History.initHtml4();
        }

        // Return true
        return true;
    };

    // ========================================================================
    // Initialise Core

    // Initialise Core
    History.initCore = function(options) {
        // Initialise
        if (typeof History.initCore.initialized !== 'undefined') {
            // Already Loaded
            return false;
        } else {
            History.initCore.initialized = true;
        }

        // ====================================================================
        // Options

        /**
         * History.options
         * Configurable options
         */
        History.options = History.options || {};

        /**
         * History.options.hashChangeInterval
         * How long should the interval be before hashchange checks
         */
        History.options.hashChangeInterval = History.options.hashChangeInterval || 100;

        /**
         * History.options.safariPollInterval
         * How long should the interval be before safari poll checks
         */
        History.options.safariPollInterval = History.options.safariPollInterval || 500;

        /**
         * History.options.doubleCheckInterval
         * How long should the interval be before we perform a double check
         */
        History.options.doubleCheckInterval = History.options.doubleCheckInterval || 500;

        /**
         * History.options.disableSuid
         * Force History not to append suid
         */
        History.options.disableSuid = History.options.disableSuid || false;

        /**
         * History.options.storeInterval
         * How long should we wait between store calls
         */
        History.options.storeInterval = History.options.storeInterval || 1000;

        /**
         * History.options.busyDelay
         * How long should we wait between busy events
         */
        History.options.busyDelay = History.options.busyDelay || 250;

        /**
         * History.options.debug
         * If true will enable debug messages to be logged
         */
        History.options.debug = History.options.debug || false;

        /**
         * History.options.initialTitle
         * What is the title of the initial state
         */
        History.options.initialTitle = History.options.initialTitle || document.title;

        /**
         * History.options.html4Mode
         * If true, will force HTMl4 mode (hashtags)
         */
        History.options.html4Mode = History.options.html4Mode || false;

        /**
         * History.options.delayInit
         * Want to override default options and call init manually.
         */
        History.options.delayInit = History.options.delayInit || false;

        // ====================================================================
        // Interval record

        /**
         * History.intervalList
         * List of intervals set, to be cleared when document is unloaded.
         */
        History.intervalList = [];

        /**
         * History.clearAllIntervals
         * Clears all setInterval instances.
         */
        History.clearAllIntervals = function() {
            var i, il = History.intervalList;
            if (typeof il !== "undefined" && il !== null) {
                for (i = 0; i < il.length; i++) {
                    clearInterval(il[i]);
                }
                History.intervalList = null;
            }
        };

        // ====================================================================
        // Debug

        /**
         * History.debug(message,...)
         * Logs the passed arguments if debug enabled
         */
        History.debug = function() {
            if ((History.options.debug || false)) {
                History.log.apply(History, arguments);
            }
        };

        /**
         * History.log(message,...)
         * Logs the passed arguments
         */
        History.log = function() {
            // Prepare
            var
                consoleExists = !(typeof console === 'undefined' || typeof console.log === 'undefined' || typeof console.log.apply === 'undefined'),
                textarea = document.getElementById('log'),
                message,
                i, n,
                args, arg;

            // Write to Console
            if (consoleExists) {
                args = Array.prototype.slice.call(arguments);
                message = args.shift();
                if (typeof console.debug !== 'undefined') {
                    console.debug.apply(console, [message, args]);
                } else {
                    console.log.apply(console, [message, args]);
                }
            } else {
                message = ("\n" + arguments[0] + "\n");
            }

            // Write to log
            for (i = 1, n = arguments.length; i < n; ++i) {
                arg = arguments[i];
                if (typeof arg === 'object' && typeof JSON !== 'undefined') {
                    try {
                        arg = JSON.stringify(arg);
                    } catch (Exception) {
                        // Recursive Object
                    }
                }
                message += "\n" + arg + "\n";
            }

            // Textarea
            if (textarea) {
                textarea.value += message + "\n-----\n";
                textarea.scrollTop = textarea.scrollHeight - textarea.clientHeight;
            }
            // No Textarea, No Console
            else if (!consoleExists) {
                alert(message);
            }

            // Return true
            return true;
        };

        // ====================================================================
        // Emulated Status

        /**
         * History.getInternetExplorerMajorVersion()
         * Get's the major version of Internet Explorer
         * @return {integer}
         * @license Public Domain
         * @author Benjamin Arthur Lupton <contact@balupton.com>
         * @author James Padolsey <https://gist.github.com/527683>
         */
        History.getInternetExplorerMajorVersion = function() {
            var result = History.getInternetExplorerMajorVersion.cached =
                (typeof History.getInternetExplorerMajorVersion.cached !== 'undefined') ? History.getInternetExplorerMajorVersion.cached : (function() {
                    var v = 3,
                        div = document.createElement('div'),
                        all = div.getElementsByTagName('i');
                    while ((div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->') && all[0]) {}
                    return (v > 4) ? v : false;
                })();
            return result;
        };

        /**
         * History.isInternetExplorer()
         * Are we using Internet Explorer?
         * @return {boolean}
         * @license Public Domain
         * @author Benjamin Arthur Lupton <contact@balupton.com>
         */
        History.isInternetExplorer = function() {
            var result =
                History.isInternetExplorer.cached =
                    (typeof History.isInternetExplorer.cached !== 'undefined') ? History.isInternetExplorer.cached : Boolean(History.getInternetExplorerMajorVersion());
            return result;
        };

        /**
         * History.emulated
         * Which features require emulating?
         */

        if (History.options.html4Mode) {
            History.emulated = {
                pushState: true,
                hashChange: true
            };
        } else {

            History.emulated = {
                pushState: !Boolean(
                    window.history && window.history.pushState && window.history.replaceState && !(
                        (/ Mobile\/([1-7][a-z]|(8([abcde]|f(1[0-8]))))/i).test(navigator.userAgent) /* disable for versions of iOS before version 4.3 (8F190) */ || (/AppleWebKit\/5([0-2]|3[0-2])/i).test(navigator.userAgent) /* disable for the mercury iOS browser, or at least older versions of the webkit engine */
                    )
                ),
                hashChange: Boolean(!(('onhashchange' in window) || ('onhashchange' in document)) ||
                    (History.isInternetExplorer() && History.getInternetExplorerMajorVersion() < 8)
                )
            };
        }

        /**
         * History.enabled
         * Is History enabled?
         */
        History.enabled = !History.emulated.pushState;

        /**
         * History.bugs
         * Which bugs are present
         */
        History.bugs = {
            /**
             * Safari 5 and Safari iOS 4 fail to return to the correct state once a hash is replaced by a `replaceState` call
             * https://bugs.webkit.org/show_bug.cgi?id=56249
             */
            setHash: Boolean(!History.emulated.pushState && navigator.vendor === 'Apple Computer, Inc.' && /AppleWebKit\/5([0-2]|3[0-3])/.test(navigator.userAgent)),

            /**
             * Safari 5 and Safari iOS 4 sometimes fail to apply the state change under busy conditions
             * https://bugs.webkit.org/show_bug.cgi?id=42940
             */
            safariPoll: Boolean(!History.emulated.pushState && navigator.vendor === 'Apple Computer, Inc.' && /AppleWebKit\/5([0-2]|3[0-3])/.test(navigator.userAgent)),

            /**
             * MSIE 6 and 7 sometimes do not apply a hash even it was told to (requiring a second call to the apply function)
             */
            ieDoubleCheck: Boolean(History.isInternetExplorer() && History.getInternetExplorerMajorVersion() < 8),

            /**
             * MSIE 6 requires the entire hash to be encoded for the hashes to trigger the onHashChange event
             */
            hashEscape: Boolean(History.isInternetExplorer() && History.getInternetExplorerMajorVersion() < 7)
        };

        /**
         * History.isEmptyObject(obj)
         * Checks to see if the Object is Empty
         * @param {Object} obj
         * @return {boolean}
         */
        History.isEmptyObject = function(obj) {
            for (var name in obj) {
                if (obj.hasOwnProperty(name)) {
                    return false;
                }
            }
            return true;
        };

        /**
         * History.cloneObject(obj)
         * Clones a object and eliminate all references to the original contexts
         * @param {Object} obj
         * @return {Object}
         */
        History.cloneObject = function(obj) {
            var hash, newObj;
            if (obj) {
                hash = JSON.stringify(obj);
                newObj = JSON.parse(hash);
            } else {
                newObj = {};
            }
            return newObj;
        };

        // ====================================================================
        // URL Helpers

        /**
         * History.getRootUrl()
         * Turns "http://mysite.com/dir/page.html?asd" into "http://mysite.com"
         * @return {String} rootUrl
         */
        History.getRootUrl = function() {
            // Create
            var rootUrl = document.location.protocol + '//' + (document.location.hostname || document.location.host);
            if (document.location.port || false) {
                rootUrl += ':' + document.location.port;
            }
            rootUrl += '/';

            // Return
            return rootUrl;
        };

        /**
         * History.getBaseHref()
         * Fetches the `href` attribute of the `<base href="...">` element if it exists
         * @return {String} baseHref
         */
        History.getBaseHref = function() {
            // Create
            var
                baseElements = document.getElementsByTagName('base'),
                baseElement = null,
                baseHref = '';

            // Test for Base Element
            if (baseElements.length === 1) {
                // Prepare for Base Element
                baseElement = baseElements[0];
                baseHref = baseElement.href.replace(/[^\/]+$/, '');
            }

            // Adjust trailing slash
            baseHref = baseHref.replace(/\/+$/, '');
            if (baseHref) baseHref += '/';

            // Return
            return baseHref;
        };

        /**
         * History.getBaseUrl()
         * Fetches the baseHref or basePageUrl or rootUrl (whichever one exists first)
         * @return {String} baseUrl
         */
        History.getBaseUrl = function() {
            // Create
            var baseUrl = History.getBaseHref() || History.getBasePageUrl() || History.getRootUrl();

            // Return
            return baseUrl;
        };

        /**
         * History.getPageUrl()
         * Fetches the URL of the current page
         * @return {String} pageUrl
         */
        History.getPageUrl = function() {
            // Fetch
            var
                State = History.getState(false, false),
                stateUrl = (State || {}).url || History.getLocationHref(),
                pageUrl;

            // Create
            pageUrl = stateUrl.replace(/\/+$/, '').replace(/[^\/]+$/, function(part, index, string) {
                return (/\./).test(part) ? part : part + '/';
            });

            // Return
            return pageUrl;
        };

        /**
         * History.getBasePageUrl()
         * Fetches the Url of the directory of the current page
         * @return {String} basePageUrl
         */
        History.getBasePageUrl = function() {
            // Create
            var basePageUrl = (History.getLocationHref()).replace(/[#\?].*/, '').replace(/[^\/]+$/, function(part, index, string) {
                return (/[^\/]$/).test(part) ? '' : part;
            }).replace(/\/+$/, '') + '/';

            // Return
            return basePageUrl;
        };

        /**
         * History.getFullUrl(url)
         * Ensures that we have an absolute URL and not a relative URL
         * @param {string} url
         * @param {Boolean} allowBaseHref
         * @return {string} fullUrl
         */
        History.getFullUrl = function(url, allowBaseHref) {
            // Prepare
            var fullUrl = url,
                firstChar = url.substring(0, 1);
            allowBaseHref = (typeof allowBaseHref === 'undefined') ? true : allowBaseHref;

            // Check
            if (/[a-z]+\:\/\//.test(url)) {
                // Full URL
            } else if (firstChar === '/') {
                // Root URL
                fullUrl = History.getRootUrl() + url.replace(/^\/+/, '');
            } else if (firstChar === '#') {
                // Anchor URL
                fullUrl = History.getPageUrl().replace(/#.*/, '') + url;
            } else if (firstChar === '?') {
                // Query URL
                fullUrl = History.getPageUrl().replace(/[\?#].*/, '') + url;
            } else {
                // Relative URL
                if (allowBaseHref) {
                    fullUrl = History.getBaseUrl() + url.replace(/^(\.\/)+/, '');
                } else {
                    fullUrl = History.getBasePageUrl() + url.replace(/^(\.\/)+/, '');
                }
                // We have an if condition above as we do not want hashes
                // which are relative to the baseHref in our URLs
                // as if the baseHref changes, then all our bookmarks
                // would now point to different locations
                // whereas the basePageUrl will always stay the same
            }

            // Return
            return fullUrl.replace(/\#$/, '');
        };

        /**
         * History.getShortUrl(url)
         * Ensures that we have a relative URL and not a absolute URL
         * @param {string} url
         * @return {string} url
         */
        History.getShortUrl = function(url) {
            // Prepare
            var shortUrl = url,
                baseUrl = History.getBaseUrl(),
                rootUrl = History.getRootUrl();

            // Trim baseUrl
            if (History.emulated.pushState) {
                // We are in a if statement as when pushState is not emulated
                // The actual url these short urls are relative to can change
                // So within the same session, we the url may end up somewhere different
                shortUrl = shortUrl.replace(baseUrl, '');
            }

            // Trim rootUrl
            shortUrl = shortUrl.replace(rootUrl, '/');

            // Ensure we can still detect it as a state
            if (History.isTraditionalAnchor(shortUrl)) {
                shortUrl = './' + shortUrl;
            }

            // Clean It
            shortUrl = shortUrl.replace(/^(\.\/)+/g, './').replace(/\#$/, '');

            // Return
            return shortUrl;
        };

        /**
         * History.getLocationHref(document)
         * Returns a normalized version of document.location.href
         * accounting for browser inconsistencies, etc.
         *
         * This URL will be URI-encoded and will include the hash
         *
         * @param {object} document
         * @return {string} url
         */
        History.getLocationHref = function(doc) {
            doc = doc || document;

            // most of the time, this will be true
            if (doc.URL === doc.location.href)
                return doc.location.href;

            // some versions of webkit URI-decode document.location.href
            // but they leave document.URL in an encoded state
            if (doc.location.href === decodeURIComponent(doc.URL))
                return doc.URL;

            // FF 3.6 only updates document.URL when a page is reloaded
            // document.location.href is updated correctly
            if (doc.location.hash && decodeURIComponent(doc.location.href.replace(/^[^#]+/, "")) === doc.location.hash)
                return doc.location.href;

            if (doc.URL.indexOf('#') == -1 && doc.location.href.indexOf('#') != -1)
                return doc.location.href;

            return doc.URL || doc.location.href;
        };

        // ====================================================================
        // State Storage

        /**
         * History.store
         * The store for all session specific data
         */
        History.store = {};

        /**
         * History.idToState
         * 1-1: State ID to State Object
         */
        History.idToState = History.idToState || {};

        /**
         * History.stateToId
         * 1-1: State String to State ID
         */
        History.stateToId = History.stateToId || {};

        /**
         * History.urlToId
         * 1-1: State URL to State ID
         */
        History.urlToId = History.urlToId || {};

        /**
         * History.storedStates
         * Store the states in an array
         */
        History.storedStates = History.storedStates || [];

        /**
         * History.savedStates
         * Saved the states in an array
         */
        History.savedStates = History.savedStates || [];

        /**
         * History.noramlizeStore()
         * Noramlize the store by adding necessary values
         */
        History.normalizeStore = function() {
            History.store.idToState = History.store.idToState || {};
            History.store.urlToId = History.store.urlToId || {};
            History.store.stateToId = History.store.stateToId || {};
        };

        /**
         * History.getState()
         * Get an object containing the data, title and url of the current state
         * @param {Boolean} friendly
         * @param {Boolean} create
         * @return {Object} State
         */
        History.getState = function(friendly, create) {
            // Prepare
            if (typeof friendly === 'undefined') {
                friendly = true;
            }
            if (typeof create === 'undefined') {
                create = true;
            }

            // Fetch
            var State = History.getLastSavedState();

            // Create
            if (!State && create) {
                State = History.createStateObject();
            }

            // Adjust
            if (friendly) {
                State = History.cloneObject(State);
                State.url = State.cleanUrl || State.url;
            }

            // Return
            return State;
        };

        /**
         * History.getIdByState(State)
         * Gets a ID for a State
         * @param {State} newState
         * @return {String} id
         */
        History.getIdByState = function(newState) {

            // Fetch ID
            var id = History.extractId(newState.url),
                str;

            if (!id) {
                // Find ID via State String
                str = History.getStateString(newState);
                if (typeof History.stateToId[str] !== 'undefined') {
                    id = History.stateToId[str];
                } else if (typeof History.store.stateToId[str] !== 'undefined') {
                    id = History.store.stateToId[str];
                } else {
                    // Generate a new ID
                    while (true) {
                        id = (new Date()).getTime() + String(Math.random()).replace(/\D/g, '');
                        if (typeof History.idToState[id] === 'undefined' && typeof History.store.idToState[id] === 'undefined') {
                            break;
                        }
                    }

                    // Apply the new State to the ID
                    History.stateToId[str] = id;
                    History.idToState[id] = newState;
                }
            }

            // Return ID
            return id;
        };

        /**
         * History.normalizeState(State)
         * Expands a State Object
         * @param {object} State
         * @return {object}
         */
        History.normalizeState = function(oldState) {
            // Variables
            var newState, dataNotEmpty;

            // Prepare
            if (!oldState || (typeof oldState !== 'object')) {
                oldState = {};
            }

            // Check
            if (typeof oldState.normalized !== 'undefined') {
                return oldState;
            }

            // Adjust
            if (!oldState.data || (typeof oldState.data !== 'object')) {
                oldState.data = {};
            }

            // ----------------------------------------------------------------

            // Create
            newState = {};
            newState.normalized = true;
            newState.title = oldState.title || '';
            newState.url = History.getFullUrl(oldState.url ? oldState.url : (History.getLocationHref()));
            newState.hash = History.getShortUrl(newState.url);
            newState.data = History.cloneObject(oldState.data);

            // Fetch ID
            newState.id = History.getIdByState(newState);

            // ----------------------------------------------------------------

            // Clean the URL
            newState.cleanUrl = newState.url.replace(/\??\&_suid.*/, '');
            newState.url = newState.cleanUrl;

            // Check to see if we have more than just a url
            dataNotEmpty = !History.isEmptyObject(newState.data);

            // Apply
            if ((newState.title || dataNotEmpty) && History.options.disableSuid !== true) {
                // Add ID to Hash
                newState.hash = History.getShortUrl(newState.url).replace(/\??\&_suid.*/, '');
                if (!/\?/.test(newState.hash)) {
                    newState.hash += '?';
                }
                newState.hash += '&_suid=' + newState.id;
            }

            // Create the Hashed URL
            newState.hashedUrl = History.getFullUrl(newState.hash);

            // ----------------------------------------------------------------

            // Update the URL if we have a duplicate
            if ((History.emulated.pushState || History.bugs.safariPoll) && History.hasUrlDuplicate(newState)) {
                newState.url = newState.hashedUrl;
            }

            // ----------------------------------------------------------------

            // Return
            return newState;
        };

        /**
         * History.createStateObject(data,title,url)
         * Creates a object based on the data, title and url state params
         * @param {object} data
         * @param {string} title
         * @param {string} url
         * @return {object}
         */
        History.createStateObject = function(data, title, url) {
            // Hashify
            var State = {
                'data': data,
                'title': title,
                'url': url
            };

            // Expand the State
            State = History.normalizeState(State);

            // Return object
            return State;
        };

        /**
         * History.getStateById(id)
         * Get a state by it's UID
         * @param {String} id
         */
        History.getStateById = function(id) {
            // Prepare
            id = String(id);

            // Retrieve
            var State = History.idToState[id] || History.store.idToState[id] || undefined;

            // Return State
            return State;
        };

        /**
         * Get a State's String
         * @param {State} passedState
         */
        History.getStateString = function(passedState) {
            // Prepare
            var State, cleanedState, str;

            // Fetch
            State = History.normalizeState(passedState);

            // Clean
            cleanedState = {
                data: State.data,
                title: passedState.title,
                url: passedState.url
            };

            // Fetch
            str = JSON.stringify(cleanedState);

            // Return
            return str;
        };

        /**
         * Get a State's ID
         * @param {State} passedState
         * @return {String} id
         */
        History.getStateId = function(passedState) {
            // Prepare
            var State, id;

            // Fetch
            State = History.normalizeState(passedState);

            // Fetch
            id = State.id;

            // Return
            return id;
        };

        /**
         * History.getHashByState(State)
         * Creates a Hash for the State Object
         * @param {State} passedState
         * @return {String} hash
         */
        History.getHashByState = function(passedState) {
            // Prepare
            var State, hash;

            // Fetch
            State = History.normalizeState(passedState);

            // Hash
            hash = State.hash;

            // Return
            return hash;
        };

        /**
         * History.extractId(url_or_hash)
         * Get a State ID by it's URL or Hash
         * @param {string} url_or_hash
         * @return {string} id
         */
        History.extractId = function(url_or_hash) {
            // Prepare
            var id, parts, url, tmp;

            // Extract

            // If the URL has a #, use the id from before the #
            if (url_or_hash.indexOf('#') != -1) {
                tmp = url_or_hash.split("#")[0];
            } else {
                tmp = url_or_hash;
            }

            parts = /(.*)\&_suid=([0-9]+)$/.exec(tmp);
            url = parts ? (parts[1] || url_or_hash) : url_or_hash;
            id = parts ? String(parts[2] || '') : '';

            // Return
            return id || false;
        };

        /**
         * History.isTraditionalAnchor
         * Checks to see if the url is a traditional anchor or not
         * @param {String} url_or_hash
         * @return {Boolean}
         */
        History.isTraditionalAnchor = function(url_or_hash) {
            // Check
            var isTraditional = !(/[\/\?\.]/.test(url_or_hash));

            // Return
            return isTraditional;
        };

        /**
         * History.extractState
         * Get a State by it's URL or Hash
         * @param {String} url_or_hash
         * @return {State|null}
         */
        History.extractState = function(url_or_hash, create) {
            // Prepare
            var State = null,
                id, url;
            create = create || false;

            // Fetch SUID
            id = History.extractId(url_or_hash);
            if (id) {
                State = History.getStateById(id);
            }

            // Fetch SUID returned no State
            if (!State) {
                // Fetch URL
                url = History.getFullUrl(url_or_hash);

                // Check URL
                id = History.getIdByUrl(url) || false;
                if (id) {
                    State = History.getStateById(id);
                }

                // Create State
                if (!State && create && !History.isTraditionalAnchor(url_or_hash)) {
                    State = History.createStateObject(null, null, url);
                }
            }

            // Return
            return State;
        };

        /**
         * History.getIdByUrl()
         * Get a State ID by a State URL
         */
        History.getIdByUrl = function(url) {
            // Fetch
            var id = History.urlToId[url] || History.store.urlToId[url] || undefined;

            // Return
            return id;
        };

        /**
         * History.getLastSavedState()
         * Get an object containing the data, title and url of the current state
         * @return {Object} State
         */
        History.getLastSavedState = function() {
            return History.savedStates[History.savedStates.length - 1] || undefined;
        };

        /**
         * History.getLastStoredState()
         * Get an object containing the data, title and url of the current state
         * @return {Object} State
         */
        History.getLastStoredState = function() {
            return History.storedStates[History.storedStates.length - 1] || undefined;
        };

        /**
         * History.hasUrlDuplicate
         * Checks if a Url will have a url conflict
         * @param {Object} newState
         * @return {Boolean} hasDuplicate
         */
        History.hasUrlDuplicate = function(newState) {
            // Prepare
            var hasDuplicate = false,
                oldState;

            // Fetch
            oldState = History.extractState(newState.url);

            // Check
            hasDuplicate = oldState && oldState.id !== newState.id;

            // Return
            return hasDuplicate;
        };

        /**
         * History.storeState
         * Store a State
         * @param {Object} newState
         * @return {Object} newState
         */
        History.storeState = function(newState) {
            // Store the State
            History.urlToId[newState.url] = newState.id;

            // Push the State
            History.storedStates.push(History.cloneObject(newState));

            // Return newState
            return newState;
        };

        /**
         * History.isLastSavedState(newState)
         * Tests to see if the state is the last state
         * @param {Object} newState
         * @return {boolean} isLast
         */
        History.isLastSavedState = function(newState) {
            // Prepare
            var isLast = false,
                newId, oldState, oldId;

            // Check
            if (History.savedStates.length) {
                newId = newState.id;
                oldState = History.getLastSavedState();
                oldId = oldState.id;

                // Check
                isLast = (newId === oldId);
            }

            // Return
            return isLast;
        };

        /**
         * History.saveState
         * Push a State
         * @param {Object} newState
         * @return {boolean} changed
         */
        History.saveState = function(newState) {
            // Check Hash
            if (History.isLastSavedState(newState)) {
                return false;
            }

            // Push the State
            History.savedStates.push(History.cloneObject(newState));

            // Return true
            return true;
        };

        /**
         * History.getStateByIndex()
         * Gets a state by the index
         * @param {integer} index
         * @return {Object}
         */
        History.getStateByIndex = function(index) {
            // Prepare
            var State = null;

            // Handle
            if (typeof index === 'undefined') {
                // Get the last inserted
                State = History.savedStates[History.savedStates.length - 1];
            } else if (index < 0) {
                // Get from the end
                State = History.savedStates[History.savedStates.length + index];
            } else {
                // Get from the beginning
                State = History.savedStates[index];
            }

            // Return State
            return State;
        };

        /**
         * History.getCurrentIndex()
         * Gets the current index
         * @return (integer)
         */
        History.getCurrentIndex = function() {
            // Prepare
            var index = null;

            // No states saved
            if (History.savedStates.length < 1) {
                index = 0;
            } else {
                index = History.savedStates.length - 1;
            }
            return index;
        };

        // ====================================================================
        // Hash Helpers

        /**
         * History.getHash()
         * @param {Location=} location
         * Gets the current document hash
         * Note: unlike location.hash, this is guaranteed to return the escaped hash in all browsers
         * @return {string}
         */
        History.getHash = function(doc) {
            var url = History.getLocationHref(doc),
                hash;
            hash = History.getHashByUrl(url);
            return hash;
        };

        /**
         * History.unescapeHash()
         * normalize and Unescape a Hash
         * @param {String} hash
         * @return {string}
         */
        History.unescapeHash = function(hash) {
            // Prepare
            var result = History.normalizeHash(hash);

            // Unescape hash
            result = decodeURIComponent(result);

            // Return result
            return result;
        };

        /**
         * History.normalizeHash()
         * normalize a hash across browsers
         * @return {string}
         */
        History.normalizeHash = function(hash) {
            // Prepare
            var result = hash.replace(/[^#]*#/, '').replace(/#.*/, '');

            // Return result
            return result;
        };

        /**
         * History.setHash(hash)
         * Sets the document hash
         * @param {string} hash
         * @return {History}
         */
        History.setHash = function(hash, queue) {
            // Prepare
            var State, pageUrl;

            // Handle Queueing
            if (queue !== false && History.busy()) {
                // Wait + Push to Queue
                //History.debug('History.setHash: we must wait', arguments);
                History.pushQueue({
                    scope: History,
                    callback: History.setHash,
                    args: arguments,
                    queue: queue
                });
                return false;
            }

            // Log
            //History.debug('History.setHash: called',hash);

            // Make Busy + Continue
            History.busy(true);

            // Check if hash is a state
            State = History.extractState(hash, true);
            if (State && !History.emulated.pushState) {
                // Hash is a state so skip the setHash
                //History.debug('History.setHash: Hash is a state so skipping the hash set with a direct pushState call',arguments);

                // PushState
                History.pushState(State.data, State.title, State.url, false);
            } else if (History.getHash() !== hash) {
                // Hash is a proper hash, so apply it

                // Handle browser bugs
                if (History.bugs.setHash) {
                    // Fix Safari Bug https://bugs.webkit.org/show_bug.cgi?id=56249

                    // Fetch the base page
                    pageUrl = History.getPageUrl();

                    // Safari hash apply
                    History.pushState(null, null, pageUrl + '#' + hash, false);
                } else {
                    // Normal hash apply
                    document.location.hash = hash;
                }
            }

            // Chain
            return History;
        };

        /**
         * History.escape()
         * normalize and Escape a Hash
         * @return {string}
         */
        History.escapeHash = function(hash) {
            // Prepare
            var result = History.normalizeHash(hash);

            // Escape hash
            result = window.encodeURIComponent(result);

            // IE6 Escape Bug
            if (!History.bugs.hashEscape) {
                // Restore common parts
                result = result
                    .replace(/\%21/g, '!')
                    .replace(/\%26/g, '&')
                    .replace(/\%3D/g, '=')
                    .replace(/\%3F/g, '?');
            }

            // Return result
            return result;
        };

        /**
         * History.getHashByUrl(url)
         * Extracts the Hash from a URL
         * @param {string} url
         * @return {string} url
         */
        History.getHashByUrl = function(url) {
            // Extract the hash
            var hash = String(url)
                .replace(/([^#]*)#?([^#]*)#?(.*)/, '$2');

            // Unescape hash
            hash = History.unescapeHash(hash);

            // Return hash
            return hash;
        };

        /**
         * History.setTitle(title)
         * Applies the title to the document
         * @param {State} newState
         * @return {Boolean}
         */
        History.setTitle = function(newState) {
            // Prepare
            var title = newState.title,
                firstState;

            // Initial
            if (!title) {
                firstState = History.getStateByIndex(0);
                if (firstState && firstState.url === newState.url) {
                    title = firstState.title || History.options.initialTitle;
                }
            }

            // Apply
            try {
                document.getElementsByTagName('title')[0].innerHTML = title.replace('<', '&lt;').replace('>', '&gt;').replace(' & ', ' &amp; ');
            } catch (Exception) {}
            document.title = title;

            // Chain
            return History;
        };

        // ====================================================================
        // Queueing

        /**
         * History.queues
         * The list of queues to use
         * First In, First Out
         */
        History.queues = [];

        /**
         * History.busy(value)
         * @param {boolean} value [optional]
         * @return {boolean} busy
         */
        History.busy = function(value) {
            // Apply
            if (typeof value !== 'undefined') {
                //History.debug('History.busy: changing ['+(History.busy.flag||false)+'] to ['+(value||false)+']', History.queues.length);
                History.busy.flag = value;
            }
            // Default
            else if (typeof History.busy.flag === 'undefined') {
                History.busy.flag = false;
            }

            // Queue
            if (!History.busy.flag) {
                // Execute the next item in the queue
                clearTimeout(History.busy.timeout);
                var fireNext = function() {
                    var i, queue, item;
                    if (History.busy.flag) return;
                    for (i = History.queues.length - 1; i >= 0; --i) {
                        queue = History.queues[i];
                        if (queue.length === 0) continue;
                        item = queue.shift();
                        History.fireQueueItem(item);
                        History.busy.timeout = setTimeout(fireNext, History.options.busyDelay);
                    }
                };
                History.busy.timeout = setTimeout(fireNext, History.options.busyDelay);
            }

            // Return
            return History.busy.flag;
        };

        /**
         * History.busy.flag
         */
        History.busy.flag = false;

        /**
         * History.fireQueueItem(item)
         * Fire a Queue Item
         * @param {Object} item
         * @return {Mixed} result
         */
        History.fireQueueItem = function(item) {
            return item.callback.apply(item.scope || History, item.args || []);
        };

        /**
         * History.pushQueue(callback,args)
         * Add an item to the queue
         * @param {Object} item [scope,callback,args,queue]
         */
        History.pushQueue = function(item) {
            // Prepare the queue
            History.queues[item.queue || 0] = History.queues[item.queue || 0] || [];

            // Add to the queue
            History.queues[item.queue || 0].push(item);

            // Chain
            return History;
        };

        /**
         * History.queue (item,queue), (func,queue), (func), (item)
         * Either firs the item now if not busy, or adds it to the queue
         */
        History.queue = function(item, queue) {
            // Prepare
            if (typeof item === 'function') {
                item = {
                    callback: item
                };
            }
            if (typeof queue !== 'undefined') {
                item.queue = queue;
            }

            // Handle
            if (History.busy()) {
                History.pushQueue(item);
            } else {
                History.fireQueueItem(item);
            }

            // Chain
            return History;
        };

        /**
         * History.clearQueue()
         * Clears the Queue
         */
        History.clearQueue = function() {
            History.busy.flag = false;
            History.queues = [];
            return History;
        };

        // ====================================================================
        // IE Bug Fix

        /**
         * History.stateChanged
         * States whether or not the state has changed since the last double check was initialised
         */
        History.stateChanged = false;

        /**
         * History.doubleChecker
         * Contains the timeout used for the double checks
         */
        History.doubleChecker = false;

        /**
         * History.doubleCheckComplete()
         * Complete a double check
         * @return {History}
         */
        History.doubleCheckComplete = function() {
            // Update
            History.stateChanged = true;

            // Clear
            History.doubleCheckClear();

            // Chain
            return History;
        };

        /**
         * History.doubleCheckClear()
         * Clear a double check
         * @return {History}
         */
        History.doubleCheckClear = function() {
            // Clear
            if (History.doubleChecker) {
                clearTimeout(History.doubleChecker);
                History.doubleChecker = false;
            }

            // Chain
            return History;
        };

        /**
         * History.doubleCheck()
         * Create a double check
         * @return {History}
         */
        History.doubleCheck = function(tryAgain) {
            // Reset
            History.stateChanged = false;
            History.doubleCheckClear();

            // Fix IE6,IE7 bug where calling history.back or history.forward does not actually change the hash (whereas doing it manually does)
            // Fix Safari 5 bug where sometimes the state does not change: https://bugs.webkit.org/show_bug.cgi?id=42940
            if (History.bugs.ieDoubleCheck) {
                // Apply Check
                History.doubleChecker = setTimeout(
                    function() {
                        History.doubleCheckClear();
                        if (!History.stateChanged) {
                            //History.debug('History.doubleCheck: State has not yet changed, trying again', arguments);
                            // Re-Attempt
                            tryAgain();
                        }
                        return true;
                    },
                    History.options.doubleCheckInterval
                );
            }

            // Chain
            return History;
        };

        // ====================================================================
        // Safari Bug Fix

        /**
         * History.safariStatePoll()
         * Poll the current state
         * @return {History}
         */
        History.safariStatePoll = function() {
            // Poll the URL

            // Get the Last State which has the new URL
            var
                urlState = History.extractState(History.getLocationHref()),
                newState;

            // Check for a difference
            if (!History.isLastSavedState(urlState)) {
                newState = urlState;
            } else {
                return;
            }

            // Check if we have a state with that url
            // If not create it
            if (!newState) {
                //History.debug('History.safariStatePoll: new');
                newState = History.createStateObject();
            }

            // Apply the New State
            //History.debug('History.safariStatePoll: trigger');
            History.Adapter.trigger(window, 'popstate');

            // Chain
            return History;
        };

        // ====================================================================
        // State Aliases

        /**
         * History.back(queue)
         * Send the browser history back one item
         * @param {Integer} queue [optional]
         */
        History.back = function(queue) {
            //History.debug('History.back: called', arguments);

            // Handle Queueing
            if (queue !== false && History.busy()) {
                // Wait + Push to Queue
                //History.debug('History.back: we must wait', arguments);
                History.pushQueue({
                    scope: History,
                    callback: History.back,
                    args: arguments,
                    queue: queue
                });
                return false;
            }

            // Make Busy + Continue
            History.busy(true);

            // Fix certain browser bugs that prevent the state from changing
            History.doubleCheck(function() {
                History.back(false);
            });

            // Go back
            history.go(-1);

            // End back closure
            return true;
        };

        /**
         * History.forward(queue)
         * Send the browser history forward one item
         * @param {Integer} queue [optional]
         */
        History.forward = function(queue) {
            //History.debug('History.forward: called', arguments);

            // Handle Queueing
            if (queue !== false && History.busy()) {
                // Wait + Push to Queue
                //History.debug('History.forward: we must wait', arguments);
                History.pushQueue({
                    scope: History,
                    callback: History.forward,
                    args: arguments,
                    queue: queue
                });
                return false;
            }

            // Make Busy + Continue
            History.busy(true);

            // Fix certain browser bugs that prevent the state from changing
            History.doubleCheck(function() {
                History.forward(false);
            });

            // Go forward
            history.go(1);

            // End forward closure
            return true;
        };

        /**
         * History.go(index,queue)
         * Send the browser history back or forward index times
         * @param {Integer} queue [optional]
         */
        History.go = function(index, queue) {
            //History.debug('History.go: called', arguments);

            // Prepare
            var i;

            // Handle
            if (index > 0) {
                // Forward
                for (i = 1; i <= index; ++i) {
                    History.forward(queue);
                }
            } else if (index < 0) {
                // Backward
                for (i = -1; i >= index; --i) {
                    History.back(queue);
                }
            } else {
                throw new Error('History.go: History.go requires a positive or negative integer passed.');
            }

            // Chain
            return History;
        };

        // ====================================================================
        // HTML5 State Support

        // Non-Native pushState Implementation
        if (History.emulated.pushState) {
            /*
             * Provide Skeleton for HTML4 Browsers
             */

            // Prepare
            var emptyFunction = function() {};
            History.pushState = History.pushState || emptyFunction;
            History.replaceState = History.replaceState || emptyFunction;
        } // History.emulated.pushState

        // Native pushState Implementation
        else {
            /*
             * Use native HTML5 History API Implementation
             */

            /**
             * History.onPopState(event,extra)
             * Refresh the Current State
             */
            History.onPopState = function(event, extra) {
                // Prepare
                var stateId = false,
                    newState = false,
                    currentHash, currentState;

                // Reset the double check
                History.doubleCheckComplete();

                // Check for a Hash, and handle apporiatly
                currentHash = History.getHash();
                if (currentHash) {
                    // Expand Hash
                    currentState = History.extractState(currentHash || History.getLocationHref(), true);
                    if (currentState) {
                        // We were able to parse it, it must be a State!
                        // Let's forward to replaceState
                        //History.debug('History.onPopState: state anchor', currentHash, currentState);
                        History.replaceState(currentState.data, currentState.title, currentState.url, false);
                    } else {
                        // Traditional Anchor
                        //History.debug('History.onPopState: traditional anchor', currentHash);
                        History.Adapter.trigger(window, 'anchorchange');
                        History.busy(false);
                    }

                    // We don't care for hashes
                    History.expectedStateId = false;
                    return false;
                }

                // Ensure
                stateId = History.Adapter.extractEventData('state', event, extra) || false;

                // Fetch State
                if (stateId) {
                    // Vanilla: Back/forward button was used
                    newState = History.getStateById(stateId);
                } else if (History.expectedStateId) {
                    // Vanilla: A new state was pushed, and popstate was called manually
                    newState = History.getStateById(History.expectedStateId);
                } else {
                    // Initial State
                    newState = History.extractState(History.getLocationHref());
                }

                // The State did not exist in our store
                if (!newState) {
                    // Regenerate the State
                    newState = History.createStateObject(null, null, History.getLocationHref());
                }

                // Clean
                History.expectedStateId = false;

                // Check if we are the same state
                if (History.isLastSavedState(newState)) {
                    // There has been no change (just the page's hash has finally propagated)
                    //History.debug('History.onPopState: no change', newState, History.savedStates);
                    History.busy(false);
                    return false;
                }

                // Store the State
                History.storeState(newState);
                History.saveState(newState);

                // Force update of the title
                History.setTitle(newState);

                // Fire Our Event
                History.Adapter.trigger(window, 'statechange');
                History.busy(false);

                // Return true
                return true;
            };
            History.Adapter.bind(window, 'popstate', History.onPopState);

            /**
             * History.pushState(data,title,url)
             * Add a new State to the history object, become it, and trigger onpopstate
             * We have to trigger for HTML4 compatibility
             * @param {object} data
             * @param {string} title
             * @param {string} url
             * @return {true}
             */
            History.pushState = function(data, title, url, queue) {
                //History.debug('History.pushState: called', arguments);

                // Check the State
                if (History.getHashByUrl(url) && History.emulated.pushState) {
                    throw new Error('History.js does not support states with fragement-identifiers (hashes/anchors).');
                }

                // Handle Queueing
                if (queue !== false && History.busy()) {
                    // Wait + Push to Queue
                    //History.debug('History.pushState: we must wait', arguments);
                    History.pushQueue({
                        scope: History,
                        callback: History.pushState,
                        args: arguments,
                        queue: queue
                    });
                    return false;
                }

                // Make Busy + Continue
                History.busy(true);

                // Create the newState
                var newState = History.createStateObject(data, title, url);

                // Check it
                if (History.isLastSavedState(newState)) {
                    // Won't be a change
                    History.busy(false);
                } else {
                    // Store the newState
                    History.storeState(newState);
                    History.expectedStateId = newState.id;

                    // Push the newState
                    history.pushState(newState.id, newState.title, newState.url);

                    // Fire HTML5 Event
                    History.Adapter.trigger(window, 'popstate');
                }

                // End pushState closure
                return true;
            };

            /**
             * History.replaceState(data,title,url)
             * Replace the State and trigger onpopstate
             * We have to trigger for HTML4 compatibility
             * @param {object} data
             * @param {string} title
             * @param {string} url
             * @return {true}
             */
            History.replaceState = function(data, title, url, queue) {
                //History.debug('History.replaceState: called', arguments);

                // Check the State
                if (History.getHashByUrl(url) && History.emulated.pushState) {
                    throw new Error('History.js does not support states with fragement-identifiers (hashes/anchors).');
                }

                // Handle Queueing
                if (queue !== false && History.busy()) {
                    // Wait + Push to Queue
                    //History.debug('History.replaceState: we must wait', arguments);
                    History.pushQueue({
                        scope: History,
                        callback: History.replaceState,
                        args: arguments,
                        queue: queue
                    });
                    return false;
                }

                // Make Busy + Continue
                History.busy(true);

                // Create the newState
                var newState = History.createStateObject(data, title, url);

                // Check it
                if (History.isLastSavedState(newState)) {
                    // Won't be a change
                    History.busy(false);
                } else {
                    // Store the newState
                    History.storeState(newState);
                    History.expectedStateId = newState.id;

                    // Push the newState
                    history.replaceState(newState.id, newState.title, newState.url);

                    // Fire HTML5 Event
                    History.Adapter.trigger(window, 'popstate');
                }

                // End replaceState closure
                return true;
            };

        } // !History.emulated.pushState

        // ====================================================================
        // Initialise

        /**
         * Load the Store
         */
        if (sessionStorage) {
            // Fetch
            try {
                History.store = JSON.parse(sessionStorage.getItem('History.store')) || {};
            } catch (err) {
                History.store = {};
            }

            // Normalize
            History.normalizeStore();
        } else {
            // Default Load
            History.store = {};
            History.normalizeStore();
        }

        /**
         * Clear Intervals on exit to prevent memory leaks
         */
        History.Adapter.bind(window, "unload", History.clearAllIntervals);

        /**
         * Create the initial State
         */
        History.saveState(History.storeState(History.extractState(History.getLocationHref(), true)));

        /**
         * Bind for Saving Store
         */
        if (sessionStorage) {
            // When the page is closed
            History.onUnload = function() {
                // Prepare
                var currentStore, item, currentStoreString;

                // Fetch
                try {
                    currentStore = JSON.parse(sessionStorage.getItem('History.store')) || {};
                } catch (err) {
                    currentStore = {};
                }

                // Ensure
                currentStore.idToState = currentStore.idToState || {};
                currentStore.urlToId = currentStore.urlToId || {};
                currentStore.stateToId = currentStore.stateToId || {};

                // Sync
                for (item in History.idToState) {
                    if (!History.idToState.hasOwnProperty(item)) {
                        continue;
                    }
                    currentStore.idToState[item] = History.idToState[item];
                }
                for (item in History.urlToId) {
                    if (!History.urlToId.hasOwnProperty(item)) {
                        continue;
                    }
                    currentStore.urlToId[item] = History.urlToId[item];
                }
                for (item in History.stateToId) {
                    if (!History.stateToId.hasOwnProperty(item)) {
                        continue;
                    }
                    currentStore.stateToId[item] = History.stateToId[item];
                }

                // Update
                History.store = currentStore;
                History.normalizeStore();

                // In Safari, going into Private Browsing mode causes the
                // Session Storage object to still exist but if you try and use
                // or set any property/function of it it throws the exception
                // "QUOTA_EXCEEDED_ERR: DOM Exception 22: An attempt was made to
                // add something to storage that exceeded the quota." infinitely
                // every second.
                currentStoreString = JSON.stringify(currentStore);
                try {
                    // Store
                    sessionStorage.setItem('History.store', currentStoreString);
                } catch (e) {
                    if (e.code === DOMException.QUOTA_EXCEEDED_ERR) {
                        if (sessionStorage.length) {
                            // Workaround for a bug seen on iPads. Sometimes the quota exceeded error comes up and simply
                            // removing/resetting the storage can work.
                            sessionStorage.removeItem('History.store');
                            sessionStorage.setItem('History.store', currentStoreString);
                        } else {
                            // Otherwise, we're probably private browsing in Safari, so we'll ignore the exception.
                        }
                    } else {
                        throw e;
                    }
                }
            };

            // For Internet Explorer
            History.intervalList.push(setInterval(History.onUnload, History.options.storeInterval));

            // For Other Browsers
            History.Adapter.bind(window, 'beforeunload', History.onUnload);
            History.Adapter.bind(window, 'unload', History.onUnload);

            // Both are enabled for consistency
        }

        // Non-Native pushState Implementation
        if (!History.emulated.pushState) {
            // Be aware, the following is only for native pushState implementations
            // If you are wanting to include something for all browsers
            // Then include it above this if block

            /**
             * Setup Safari Fix
             */
            if (History.bugs.safariPoll) {
                History.intervalList.push(setInterval(History.safariStatePoll, History.options.safariPollInterval));
            }

            /**
             * Ensure Cross Browser Compatibility
             */
            if (navigator.vendor === 'Apple Computer, Inc.' || (navigator.appCodeName || '') === 'Mozilla') {
                /**
                 * Fix Safari HashChange Issue
                 */

                // Setup Alias
                History.Adapter.bind(window, 'hashchange', function() {
                    History.Adapter.trigger(window, 'popstate');
                });

                // Initialise Alias
                if (History.getHash()) {
                    History.Adapter.onDomLoad(function() {
                        History.Adapter.trigger(window, 'hashchange');
                    });
                }
            }

        } // !History.emulated.pushState

    }; // History.initCore

    // Try to Initialise History
    if (!History.options || !History.options.delayInit) {
        History.init();
    }

})(window);
/*EOF /Scripts/native.history.js*/
/*/Scripts/MicrosoftAjax.js*/
//----------------------------------------------------------
// Copyright (C) Microsoft Corporation. All rights reserved.
//----------------------------------------------------------
// MicrosoftAjax.js
Function.__typeName = "Function";
Function.__class = true;
Function.createCallback = function(b, a) {
    return function() {
        var e = arguments.length;
        if (e > 0) {
            var d = [];
            for (var c = 0; c < e; c++) d[c] = arguments[c];
            d[e] = a;
            return b.apply(this, d)
        }
        return b.call(this, a)
    }
};
Function.createDelegate = function(a, b) {
    return function() {
        return b.apply(a, arguments)
    }
};
Function.emptyFunction = Function.emptyMethod = function() {};
Function.validateParameters = function(c, b, a) {
    return Function._validateParams(c, b, a)
};
Function._validateParams = function(g, e, c) {
    var a, d = e.length;
    c = c || typeof c === "undefined";
    a = Function._validateParameterCount(g, e, c);
    if (a) {
        a.popStackFrame();
        return a
    }
    for (var b = 0, i = g.length; b < i; b++) {
        var f = e[Math.min(b, d - 1)],
            h = f.name;
        if (f.parameterArray) h += "[" + (b - d + 1) + "]";
        else if (!c && b >= d) break;
        a = Function._validateParameter(g[b], f, h);
        if (a) {
            a.popStackFrame();
            return a
        }
    }
    return null
};
Function._validateParameterCount = function(j, d, i) {
    var a, c, b = d.length,
        e = j.length;
    if (e < b) {
        var f = b;
        for (a = 0; a < b; a++) {
            var g = d[a];
            if (g.optional || g.parameterArray) f--
        }
        if (e < f) c = true
    } else if (i && e > b) {
        c = true;
        for (a = 0; a < b; a++)
            if (d[a].parameterArray) {
                c = false;
                break
            }
    }
    if (c) {
        var h = Error.parameterCount();
        h.popStackFrame();
        return h
    }
    return null
};
Function._validateParameter = function(c, a, h) {
    var b, g = a.type,
        l = !!a.integer,
        k = !!a.domElement,
        m = !!a.mayBeNull;
    b = Function._validateParameterType(c, g, l, k, m, h);
    if (b) {
        b.popStackFrame();
        return b
    }
    var e = a.elementType,
        f = !!a.elementMayBeNull;
    if (g === Array && typeof c !== "undefined" && c !== null && (e || !f)) {
        var j = !!a.elementInteger,
            i = !!a.elementDomElement;
        for (var d = 0; d < c.length; d++) {
            var n = c[d];
            b = Function._validateParameterType(n, e, j, i, f, h + "[" + d + "]");
            if (b) {
                b.popStackFrame();
                return b
            }
        }
    }
    return null
};
Function._validateParameterType = function(b, c, k, j, h, d) {
    var a, g;
    if (typeof b === "undefined")
        if (h) return null;
        else {
            a = Error.argumentUndefined(d);
            a.popStackFrame();
            return a
        }
    if (b === null)
        if (h) return null;
        else {
            a = Error.argumentNull(d);
            a.popStackFrame();
            return a
        }
    if (c && c.__enum) {
        if (typeof b !== "number") {
            a = Error.argumentType(d, Object.getType(b), c);
            a.popStackFrame();
            return a
        }
        if (b % 1 === 0) {
            var e = c.prototype;
            if (!c.__flags || b === 0) {
                for (g in e)
                    if (e[g] === b) return null
            } else {
                var i = b;
                for (g in e) {
                    var f = e[g];
                    if (f === 0) continue;
                    if ((f & b) === f) i -= f;
                    if (i === 0) return null
                }
            }
        }
        a = Error.argumentOutOfRange(d, b, String.format(Sys.Res.enumInvalidValue, b, c.getName()));
        a.popStackFrame();
        return a
    }
    if (j && (!Sys._isDomElement(b) || b.nodeType === 3)) {
        a = Error.argument(d, Sys.Res.argumentDomElement);
        a.popStackFrame();
        return a
    }
    if (c && !Sys._isInstanceOfType(c, b)) {
        a = Error.argumentType(d, Object.getType(b), c);
        a.popStackFrame();
        return a
    }
    if (c === Number && k)
        if (b % 1 !== 0) {
            a = Error.argumentOutOfRange(d, b, Sys.Res.argumentInteger);
            a.popStackFrame();
            return a
        }
    return null
};
Error.__typeName = "Error";
Error.__class = true;
Error.create = function(d, b) {
    var a = new Error(d);
    a.message = d;
    if (b)
        for (var c in b) a[c] = b[c];
    a.popStackFrame();
    return a
};
Error.argument = function(a, c) {
    var b = "Sys.ArgumentException: " + (c ? c : Sys.Res.argument);
    if (a) b += "\n" + String.format(Sys.Res.paramName, a);
    var d = Error.create(b, {
        name: "Sys.ArgumentException",
        paramName: a
    });
    d.popStackFrame();
    return d
};
Error.argumentNull = function(a, c) {
    var b = "Sys.ArgumentNullException: " + (c ? c : Sys.Res.argumentNull);
    if (a) b += "\n" + String.format(Sys.Res.paramName, a);
    var d = Error.create(b, {
        name: "Sys.ArgumentNullException",
        paramName: a
    });
    d.popStackFrame();
    return d
};
Error.argumentOutOfRange = function(c, a, d) {
    var b = "Sys.ArgumentOutOfRangeException: " + (d ? d : Sys.Res.argumentOutOfRange);
    if (c) b += "\n" + String.format(Sys.Res.paramName, c);
    if (typeof a !== "undefined" && a !== null) b += "\n" + String.format(Sys.Res.actualValue, a);
    var e = Error.create(b, {
        name: "Sys.ArgumentOutOfRangeException",
        paramName: c,
        actualValue: a
    });
    e.popStackFrame();
    return e
};
Error.argumentType = function(d, c, b, e) {
    var a = "Sys.ArgumentTypeException: ";
    if (e) a += e;
    else if (c && b) a += String.format(Sys.Res.argumentTypeWithTypes, c.getName(), b.getName());
    else a += Sys.Res.argumentType;
    if (d) a += "\n" + String.format(Sys.Res.paramName, d);
    var f = Error.create(a, {
        name: "Sys.ArgumentTypeException",
        paramName: d,
        actualType: c,
        expectedType: b
    });
    f.popStackFrame();
    return f
};
Error.argumentUndefined = function(a, c) {
    var b = "Sys.ArgumentUndefinedException: " + (c ? c : Sys.Res.argumentUndefined);
    if (a) b += "\n" + String.format(Sys.Res.paramName, a);
    var d = Error.create(b, {
        name: "Sys.ArgumentUndefinedException",
        paramName: a
    });
    d.popStackFrame();
    return d
};
Error.format = function(a) {
    var c = "Sys.FormatException: " + (a ? a : Sys.Res.format),
        b = Error.create(c, {
            name: "Sys.FormatException"
        });
    b.popStackFrame();
    return b
};
Error.invalidOperation = function(a) {
    var c = "Sys.InvalidOperationException: " + (a ? a : Sys.Res.invalidOperation),
        b = Error.create(c, {
            name: "Sys.InvalidOperationException"
        });
    b.popStackFrame();
    return b
};
Error.notImplemented = function(a) {
    var c = "Sys.NotImplementedException: " + (a ? a : Sys.Res.notImplemented),
        b = Error.create(c, {
            name: "Sys.NotImplementedException"
        });
    b.popStackFrame();
    return b
};
Error.parameterCount = function(a) {
    var c = "Sys.ParameterCountException: " + (a ? a : Sys.Res.parameterCount),
        b = Error.create(c, {
            name: "Sys.ParameterCountException"
        });
    b.popStackFrame();
    return b
};
Error.prototype.popStackFrame = function() {
    if (typeof this.stack === "undefined" || this.stack === null || typeof this.fileName === "undefined" || this.fileName === null || typeof this.lineNumber === "undefined" || this.lineNumber === null) return;
    var a = this.stack.split("\n"),
        c = a[0],
        e = this.fileName + ":" + this.lineNumber;
    while (typeof c !== "undefined" && c !== null && c.indexOf(e) === -1) {
        a.shift();
        c = a[0]
    }
    var d = a[1];
    if (typeof d === "undefined" || d === null) return;
    var b = d.match(/@(.*):(\d+)$/);
    if (typeof b === "undefined" || b === null) return;
    this.fileName = b[1];
    this.lineNumber = parseInt(b[2]);
    a.shift();
    this.stack = a.join("\n")
};
Object.__typeName = "Object";
Object.__class = true;
Object.getType = function(b) {
    var a = b.constructor;
    if (!a || typeof a !== "function" || !a.__typeName || a.__typeName === "Object") return Object;
    return a
};
Object.getTypeName = function(a) {
    return Object.getType(a).getName()
};
String.__typeName = "String";
String.__class = true;
String.prototype.endsWith = function(a) {
    return this.substr(this.length - a.length) === a
};
String.prototype.startsWith = function(a) {
    return this.substr(0, a.length) === a
};
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "")
};
String.prototype.trimEnd = function() {
    return this.replace(/\s+$/, "")
};
String.prototype.trimStart = function() {
    return this.replace(/^\s+/, "")
};
String.format = function() {
    return String._toFormattedString(false, arguments)
};
String._toFormattedString = function(l, j) {
    var c = "",
        e = j[0];
    for (var a = 0; true;) {
        var f = e.indexOf("{", a),
            d = e.indexOf("}", a);
        if (f < 0 && d < 0) {
            c += e.slice(a);
            break
        }
        if (d > 0 && (d < f || f < 0)) {
            c += e.slice(a, d + 1);
            a = d + 2;
            continue
        }
        c += e.slice(a, f);
        a = f + 1;
        if (e.charAt(a) === "{") {
            c += "{";
            a++;
            continue
        }
        if (d < 0) break;
        var h = e.substring(a, d),
            g = h.indexOf(":"),
            k = parseInt(g < 0 ? h : h.substring(0, g), 10) + 1,
            i = g < 0 ? "" : h.substring(g + 1),
            b = j[k];
        if (typeof b === "undefined" || b === null) b = "";
        if (b.toFormattedString) c += b.toFormattedString(i);
        else if (l && b.localeFormat) c += b.localeFormat(i);
        else if (b.format) c += b.format(i);
        else c += b.toString();
        a = d + 1
    }
    return c
};
Boolean.__typeName = "Boolean";
Boolean.__class = true;
Boolean.parse = function(b) {
    var a = b.trim().toLowerCase();
    if (a === "false") return false;
    if (a === "true") return true
};
Date.__typeName = "Date";
Date.__class = true;
Number.__typeName = "Number";
Number.__class = true;
RegExp.__typeName = "RegExp";
RegExp.__class = true;
if (!window) this.window = this;
window.Type = Function;
Type.prototype.callBaseMethod = function(a, d, b) {
    var c = Sys._getBaseMethod(this, a, d);
    if (!b) return c.apply(a);
    else return c.apply(a, b)
};
Type.prototype.getBaseMethod = function(a, b) {
    return Sys._getBaseMethod(this, a, b)
};
Type.prototype.getBaseType = function() {
    return typeof this.__baseType === "undefined" ? null : this.__baseType
};
Type.prototype.getInterfaces = function() {
    var a = [],
        b = this;
    while (b) {
        var c = b.__interfaces;
        if (c)
            for (var d = 0, f = c.length; d < f; d++) {
                var e = c[d];
                if (!Array.contains(a, e)) a[a.length] = e
            }
        b = b.__baseType
    }
    return a
};
Type.prototype.getName = function() {
    return typeof this.__typeName === "undefined" ? "" : this.__typeName
};
Type.prototype.implementsInterface = function(d) {
    this.resolveInheritance();
    var c = d.getName(),
        a = this.__interfaceCache;
    if (a) {
        var e = a[c];
        if (typeof e !== "undefined") return e
    } else a = this.__interfaceCache = {};
    var b = this;
    while (b) {
        var f = b.__interfaces;
        if (f)
            if (Array.indexOf(f, d) !== -1) return a[c] = true;
        b = b.__baseType
    }
    return a[c] = false
};
Type.prototype.inheritsFrom = function(b) {
    this.resolveInheritance();
    var a = this.__baseType;
    while (a) {
        if (a === b) return true;
        a = a.__baseType
    }
    return false
};
Type.prototype.initializeBase = function(a, b) {
    this.resolveInheritance();
    if (this.__baseType)
        if (!b) this.__baseType.apply(a);
        else this.__baseType.apply(a, b);
    return a
};
Type.prototype.isImplementedBy = function(a) {
    if (typeof a === "undefined" || a === null) return false;
    var b = Object.getType(a);
    return !!(b.implementsInterface && b.implementsInterface(this))
};
Type.prototype.isInstanceOfType = function(a) {
    return Sys._isInstanceOfType(this, a)
};
Type.prototype.registerClass = function(c, b, d) {
    this.prototype.constructor = this;
    this.__typeName = c;
    this.__class = true;
    if (b) {
        this.__baseType = b;
        this.__basePrototypePending = true
    }
    Sys.__upperCaseTypes[c.toUpperCase()] = this;
    if (d) {
        this.__interfaces = [];
        for (var a = 2, f = arguments.length; a < f; a++) {
            var e = arguments[a];
            this.__interfaces.push(e)
        }
    }
    return this
};
Type.prototype.registerInterface = function(a) {
    Sys.__upperCaseTypes[a.toUpperCase()] = this;
    this.prototype.constructor = this;
    this.__typeName = a;
    this.__interface = true;
    return this
};
Type.prototype.resolveInheritance = function() {
    if (this.__basePrototypePending) {
        var b = this.__baseType;
        b.resolveInheritance();
        for (var a in b.prototype) {
            var c = b.prototype[a];
            if (!this.prototype[a]) this.prototype[a] = c
        }
        delete this.__basePrototypePending
    }
};
Type.getRootNamespaces = function() {
    return Array.clone(Sys.__rootNamespaces)
};
Type.isClass = function(a) {
    if (typeof a === "undefined" || a === null) return false;
    return !!a.__class
};
Type.isInterface = function(a) {
    if (typeof a === "undefined" || a === null) return false;
    return !!a.__interface
};
Type.isNamespace = function(a) {
    if (typeof a === "undefined" || a === null) return false;
    return !!a.__namespace
};
Type.parse = function(typeName, ns) {
    var fn;
    if (ns) {
        fn = Sys.__upperCaseTypes[ns.getName().toUpperCase() + "." + typeName.toUpperCase()];
        return fn || null
    }
    if (!typeName) return null;
    if (!Type.__htClasses) Type.__htClasses = {};
    fn = Type.__htClasses[typeName];
    if (!fn) {
        fn = eval(typeName);
        Type.__htClasses[typeName] = fn
    }
    return fn
};
Type.registerNamespace = function(e) {
    var d = window,
        c = e.split(".");
    for (var b = 0; b < c.length; b++) {
        var f = c[b],
            a = d[f];
        if (!a) a = d[f] = {};
        if (!a.__namespace) {
            if (b === 0 && e !== "Sys") Sys.__rootNamespaces[Sys.__rootNamespaces.length] = a;
            a.__namespace = true;
            a.__typeName = c.slice(0, b + 1).join(".");
            a.getName = function() {
                return this.__typeName
            }
        }
        d = a
    }
};
Type._checkDependency = function(c, a) {
    var d = Type._registerScript._scripts,
        b = d ? !!d[c] : false;
    if (typeof a !== "undefined" && !b) throw Error.invalidOperation(String.format(Sys.Res.requiredScriptReferenceNotIncluded, a, c));
    return b
};
Type._registerScript = function(a, c) {
    var b = Type._registerScript._scripts;
    if (!b) Type._registerScript._scripts = b = {};
    if (b[a]) throw Error.invalidOperation(String.format(Sys.Res.scriptAlreadyLoaded, a));
    b[a] = true;
    if (c)
        for (var d = 0, f = c.length; d < f; d++) {
            var e = c[d];
            if (!Type._checkDependency(e)) throw Error.invalidOperation(String.format(Sys.Res.scriptDependencyNotFound, a, e))
        }
};
Type.registerNamespace("Sys");
Sys.__upperCaseTypes = {};
Sys.__rootNamespaces = [Sys];
Sys._isInstanceOfType = function(c, b) {
    if (typeof b === "undefined" || b === null) return false;
    if (b instanceof c) return true;
    var a = Object.getType(b);
    return !!(a === c) || a.inheritsFrom && a.inheritsFrom(c) || a.implementsInterface && a.implementsInterface(c)
};
Sys._getBaseMethod = function(d, e, c) {
    var b = d.getBaseType();
    if (b) {
        var a = b.prototype[c];
        return a instanceof Function ? a : null
    }
    return null
};
Sys._isDomElement = function(a) {
    var c = false;
    if (typeof a.nodeType !== "number") {
        var b = a.ownerDocument || a.document || a;
        if (b != a) {
            var d = b.defaultView || b.parentWindow;
            c = d != a
        } else c = typeof b.body === "undefined"
    }
    return !c
};
Array.__typeName = "Array";
Array.__class = true;
Array.add = Array.enqueue = function(a, b) {
    a[a.length] = b
};
Array.addRange = function(a, b) {
    a.push.apply(a, b)
};
Array.clear = function(a) {
    a.length = 0
};
Array.clone = function(a) {
    if (a.length === 1) return [a[0]];
    else return Array.apply(null, a)
};
Array.contains = function(a, b) {
    return Sys._indexOf(a, b) >= 0
};
Array.dequeue = function(a) {
    return a.shift()
};
Array.forEach = function(b, e, d) {
    for (var a = 0, f = b.length; a < f; a++) {
        var c = b[a];
        if (typeof c !== "undefined") e.call(d, c, a, b)
    }
};
Array.indexOf = function(a, c, b) {
    return Sys._indexOf(a, c, b)
};
Array.insert = function(a, b, c) {
    a.splice(b, 0, c)
};
Array.parse = function(value) {
    if (!value) return [];
    return eval(value)
};
Array.remove = function(b, c) {
    var a = Sys._indexOf(b, c);
    if (a >= 0) b.splice(a, 1);
    return a >= 0
};
Array.removeAt = function(a, b) {
    a.splice(b, 1)
};
Sys._indexOf = function(d, e, a) {
    if (typeof e === "undefined") return -1;
    var c = d.length;
    if (c !== 0) {
        a = a - 0;
        if (isNaN(a)) a = 0;
        else {
            if (isFinite(a)) a = a - a % 1;
            if (a < 0) a = Math.max(0, c + a)
        }
        for (var b = a; b < c; b++)
            if (typeof d[b] !== "undefined" && d[b] === e) return b
    }
    return -1
};
Type._registerScript._scripts = {
    "MicrosoftAjaxCore.js": true,
    "MicrosoftAjaxGlobalization.js": true,
    "MicrosoftAjaxSerialization.js": true,
    "MicrosoftAjaxComponentModel.js": true,
    "MicrosoftAjaxHistory.js": true,
    "MicrosoftAjaxNetwork.js": true,
    "MicrosoftAjaxWebServices.js": true
};
Sys.IDisposable = function() {};
Sys.IDisposable.prototype = {};
Sys.IDisposable.registerInterface("Sys.IDisposable");
Sys.StringBuilder = function(a) {
    this._parts = typeof a !== "undefined" && a !== null && a !== "" ? [a.toString()] : [];
    this._value = {};
    this._len = 0
};
Sys.StringBuilder.prototype = {
    append: function(a) {
        this._parts[this._parts.length] = a
    },
    appendLine: function(a) {
        this._parts[this._parts.length] = typeof a === "undefined" || a === null || a === "" ? "\r\n" : a + "\r\n"
    },
    clear: function() {
        this._parts = [];
        this._value = {};
        this._len = 0
    },
    isEmpty: function() {
        if (this._parts.length === 0) return true;
        return this.toString() === ""
    },
    toString: function(a) {
        a = a || "";
        var b = this._parts;
        if (this._len !== b.length) {
            this._value = {};
            this._len = b.length
        }
        var d = this._value;
        if (typeof d[a] === "undefined") {
            if (a !== "")
                for (var c = 0; c < b.length;)
                    if (typeof b[c] === "undefined" || b[c] === "" || b[c] === null) b.splice(c, 1);
                    else c++;
            d[a] = this._parts.join(a)
        }
        return d[a]
    }
};
Sys.StringBuilder.registerClass("Sys.StringBuilder");
Sys.Browser = {};
Sys.Browser.InternetExplorer = {};
Sys.Browser.Firefox = {};
Sys.Browser.Safari = {};
Sys.Browser.Opera = {};
Sys.Browser.agent = null;
Sys.Browser.hasDebuggerStatement = false;
Sys.Browser.name = navigator.appName;
Sys.Browser.version = parseFloat(navigator.appVersion);
Sys.Browser.documentMode = 0;
if (navigator.userAgent.indexOf(" MSIE ") > -1) {
    Sys.Browser.agent = Sys.Browser.InternetExplorer;
    Sys.Browser.version = parseFloat(navigator.userAgent.match(/MSIE (\d+\.\d+)/)[1]);
    if (Sys.Browser.version >= 8)
        if (document.documentMode >= 7) Sys.Browser.documentMode = document.documentMode;
    Sys.Browser.hasDebuggerStatement = true
} else if (navigator.userAgent.indexOf(" Firefox/") > -1) {
    Sys.Browser.agent = Sys.Browser.Firefox;
    Sys.Browser.version = parseFloat(navigator.userAgent.match(/Firefox\/(\d+\.\d+)/)[1]);
    Sys.Browser.name = "Firefox";
    Sys.Browser.hasDebuggerStatement = true
} else if (navigator.userAgent.indexOf(" AppleWebKit/") > -1) {
    Sys.Browser.agent = Sys.Browser.Safari;
    Sys.Browser.version = parseFloat(navigator.userAgent.match(/AppleWebKit\/(\d+(\.\d+)?)/)[1]);
    Sys.Browser.name = "Safari"
} else if (navigator.userAgent.indexOf("Opera/") > -1) Sys.Browser.agent = Sys.Browser.Opera;
Sys.EventArgs = function() {};
Sys.EventArgs.registerClass("Sys.EventArgs");
Sys.EventArgs.Empty = new Sys.EventArgs;
Sys.CancelEventArgs = function() {
    Sys.CancelEventArgs.initializeBase(this);
    this._cancel = false
};
Sys.CancelEventArgs.prototype = {
    get_cancel: function() {
        return this._cancel
    },
    set_cancel: function(a) {
        this._cancel = a
    }
};
Sys.CancelEventArgs.registerClass("Sys.CancelEventArgs", Sys.EventArgs);
Type.registerNamespace("Sys.UI");
Sys._Debug = function() {};
Sys._Debug.prototype = {
    _appendConsole: function(a) {
        if (typeof Debug !== "undefined" && Debug.writeln) Debug.writeln(a);
        if (window.console && window.console.log) window.console.log(a);
        if (window.opera) window.opera.postError(a);
        if (window.debugService) window.debugService.trace(a)
    },
    _appendTrace: function(b) {
        var a = document.getElementById("TraceConsole");
        if (a && a.tagName.toUpperCase() === "TEXTAREA") a.value += b + "\n"
    },
    assert: function(c, a, b) {
        if (!c) {
            a = b && this.assert.caller ? String.format(Sys.Res.assertFailedCaller, a, this.assert.caller) : String.format(Sys.Res.assertFailed, a);
            if (confirm(String.format(Sys.Res.breakIntoDebugger, a))) this.fail(a)
        }
    },
    clearTrace: function() {
        var a = document.getElementById("TraceConsole");
        if (a && a.tagName.toUpperCase() === "TEXTAREA") a.value = ""
    },
    fail: function(message) {
        this._appendConsole(message);
        if (Sys.Browser.hasDebuggerStatement) eval("debugger")
    },
    trace: function(a) {
        this._appendConsole(a);
        this._appendTrace(a)
    },
    traceDump: function(a, b) {
        var c = this._traceDump(a, b, true)
    },
    _traceDump: function(a, c, f, b, d) {
        c = c ? c : "traceDump";
        b = b ? b : "";
        if (a === null) {
            this.trace(b + c + ": null");
            return
        }
        switch (typeof a) {
            case "undefined":
                this.trace(b + c + ": Undefined");
                break;
            case "number":
            case "string":
            case "boolean":
                this.trace(b + c + ": " + a);
                break;
            default:
                if (Date.isInstanceOfType(a) || RegExp.isInstanceOfType(a)) {
                    this.trace(b + c + ": " + a.toString());
                    break
                }
                if (!d) d = [];
                else if (Array.contains(d, a)) {
                    this.trace(b + c + ": ...");
                    return
                }
                Array.add(d, a);
                if (a == window || a === document || window.HTMLElement && a instanceof HTMLElement || typeof a.nodeName === "string") {
                    var k = a.tagName ? a.tagName : "DomElement";
                    if (a.id) k += " - " + a.id;
                    this.trace(b + c + " {" + k + "}")
                } else {
                    var i = Object.getTypeName(a);
                    this.trace(b + c + (typeof i === "string" ? " {" + i + "}" : ""));
                    if (b === "" || f) {
                        b += "    ";
                        var e, j, l, g, h;
                        if (Array.isInstanceOfType(a)) {
                            j = a.length;
                            for (e = 0; e < j; e++) this._traceDump(a[e], "[" + e + "]", f, b, d)
                        } else
                            for (g in a) {
                                h = a[g];
                                if (!Function.isInstanceOfType(h)) this._traceDump(h, g, f, b, d)
                            }
                    }
                }
                Array.remove(d, a)
        }
    }
};
Sys._Debug.registerClass("Sys._Debug");
Sys.Debug = new Sys._Debug;
Sys.Debug.isDebug = false;

function Sys$Enum$parse(c, e) {
    var a, b, i;
    if (e) {
        a = this.__lowerCaseValues;
        if (!a) {
            this.__lowerCaseValues = a = {};
            var g = this.prototype;
            for (var f in g) a[f.toLowerCase()] = g[f]
        }
    } else a = this.prototype;
    if (!this.__flags) {
        i = e ? c.toLowerCase() : c;
        b = a[i.trim()];
        if (typeof b !== "number") throw Error.argument("value", String.format(Sys.Res.enumInvalidValue, c, this.__typeName));
        return b
    } else {
        var h = (e ? c.toLowerCase() : c).split(","),
            j = 0;
        for (var d = h.length - 1; d >= 0; d--) {
            var k = h[d].trim();
            b = a[k];
            if (typeof b !== "number") throw Error.argument("value", String.format(Sys.Res.enumInvalidValue, c.split(",")[d].trim(), this.__typeName));
            j |= b
        }
        return j
    }
}

function Sys$Enum$toString(c) {
    if (typeof c === "undefined" || c === null) return this.__string;
    var d = this.prototype,
        a;
    if (!this.__flags || c === 0) {
        for (a in d)
            if (d[a] === c) return a
    } else {
        var b = this.__sortedValues;
        if (!b) {
            b = [];
            for (a in d) b[b.length] = {
                key: a,
                value: d[a]
            };
            b.sort(function(a, b) {
                return a.value - b.value
            });
            this.__sortedValues = b
        }
        var e = [],
            g = c;
        for (a = b.length - 1; a >= 0; a--) {
            var h = b[a],
                f = h.value;
            if (f === 0) continue;
            if ((f & c) === f) {
                e[e.length] = h.key;
                g -= f;
                if (g === 0) break
            }
        }
        if (e.length && g === 0) return e.reverse().join(", ")
    }
    return ""
}
Type.prototype.registerEnum = function(b, c) {
    Sys.__upperCaseTypes[b.toUpperCase()] = this;
    for (var a in this.prototype) this[a] = this.prototype[a];
    this.__typeName = b;
    this.parse = Sys$Enum$parse;
    this.__string = this.toString();
    this.toString = Sys$Enum$toString;
    this.__flags = c;
    this.__enum = true
};
Type.isEnum = function(a) {
    if (typeof a === "undefined" || a === null) return false;
    return !!a.__enum
};
Type.isFlags = function(a) {
    if (typeof a === "undefined" || a === null) return false;
    return !!a.__flags
};
Sys.CollectionChange = function(e, a, c, b, d) {
    this.action = e;
    if (a)
        if (!(a instanceof Array)) a = [a];
    this.newItems = a || null;
    if (typeof c !== "number") c = -1;
    this.newStartingIndex = c;
    if (b)
        if (!(b instanceof Array)) b = [b];
    this.oldItems = b || null;
    if (typeof d !== "number") d = -1;
    this.oldStartingIndex = d
};
Sys.CollectionChange.registerClass("Sys.CollectionChange");
Sys.NotifyCollectionChangedAction = function() {
    throw Error.notImplemented()
};
Sys.NotifyCollectionChangedAction.prototype = {
    add: 0,
    remove: 1,
    reset: 2
};
Sys.NotifyCollectionChangedAction.registerEnum("Sys.NotifyCollectionChangedAction");
Sys.NotifyCollectionChangedEventArgs = function(a) {
    this._changes = a;
    Sys.NotifyCollectionChangedEventArgs.initializeBase(this)
};
Sys.NotifyCollectionChangedEventArgs.prototype = {
    get_changes: function() {
        return this._changes || []
    }
};
Sys.NotifyCollectionChangedEventArgs.registerClass("Sys.NotifyCollectionChangedEventArgs", Sys.EventArgs);
Sys.Observer = function() {};
Sys.Observer.registerClass("Sys.Observer");
Sys.Observer.makeObservable = function(a) {
    var c = a instanceof Array,
        b = Sys.Observer;
    if (a.setValue === b._observeMethods.setValue) return a;
    b._addMethods(a, b._observeMethods);
    if (c) b._addMethods(a, b._arrayMethods);
    return a
};
Sys.Observer._addMethods = function(c, b) {
    for (var a in b) c[a] = b[a]
};
Sys.Observer._addEventHandler = function(c, a, b) {
    Sys.Observer._getContext(c, true).events._addHandler(a, b)
};
Sys.Observer.addEventHandler = function(c, a, b) {
    Sys.Observer._addEventHandler(c, a, b)
};
Sys.Observer._removeEventHandler = function(c, a, b) {
    Sys.Observer._getContext(c, true).events._removeHandler(a, b)
};
Sys.Observer.removeEventHandler = function(c, a, b) {
    Sys.Observer._removeEventHandler(c, a, b)
};
Sys.Observer.raiseEvent = function(b, e, d) {
    var c = Sys.Observer._getContext(b);
    if (!c) return;
    var a = c.events.getHandler(e);
    if (a) a(b, d)
};
Sys.Observer.addPropertyChanged = function(b, a) {
    Sys.Observer._addEventHandler(b, "propertyChanged", a)
};
Sys.Observer.removePropertyChanged = function(b, a) {
    Sys.Observer._removeEventHandler(b, "propertyChanged", a)
};
Sys.Observer.beginUpdate = function(a) {
    Sys.Observer._getContext(a, true).updating = true
};
Sys.Observer.endUpdate = function(b) {
    var a = Sys.Observer._getContext(b);
    if (!a || !a.updating) return;
    a.updating = false;
    var d = a.dirty;
    a.dirty = false;
    if (d) {
        if (b instanceof Array) {
            var c = a.changes;
            a.changes = null;
            Sys.Observer.raiseCollectionChanged(b, c)
        }
        Sys.Observer.raisePropertyChanged(b, "")
    }
};
Sys.Observer.isUpdating = function(b) {
    var a = Sys.Observer._getContext(b);
    return a ? a.updating : false
};
Sys.Observer._setValue = function(a, j, g) {
    var b, f, k = a,
        d = j.split(".");
    for (var i = 0, m = d.length - 1; i < m; i++) {
        var l = d[i];
        b = a["get_" + l];
        if (typeof b === "function") a = b.call(a);
        else a = a[l];
        var n = typeof a;
        if (a === null || n === "undefined") throw Error.invalidOperation(String.format(Sys.Res.nullReferenceInPath, j))
    }
    var e, c = d[m];
    b = a["get_" + c];
    f = a["set_" + c];
    if (typeof b === "function") e = b.call(a);
    else e = a[c];
    if (typeof f === "function") f.call(a, g);
    else a[c] = g;
    if (e !== g) {
        var h = Sys.Observer._getContext(k);
        if (h && h.updating) {
            h.dirty = true;
            return
        }
        Sys.Observer.raisePropertyChanged(k, d[0])
    }
};
Sys.Observer.setValue = function(b, a, c) {
    Sys.Observer._setValue(b, a, c)
};
Sys.Observer.raisePropertyChanged = function(b, a) {
    Sys.Observer.raiseEvent(b, "propertyChanged", new Sys.PropertyChangedEventArgs(a))
};
Sys.Observer.addCollectionChanged = function(b, a) {
    Sys.Observer._addEventHandler(b, "collectionChanged", a)
};
Sys.Observer.removeCollectionChanged = function(b, a) {
    Sys.Observer._removeEventHandler(b, "collectionChanged", a)
};
Sys.Observer._collectionChange = function(d, c) {
    var a = Sys.Observer._getContext(d);
    if (a && a.updating) {
        a.dirty = true;
        var b = a.changes;
        if (!b) a.changes = b = [c];
        else b.push(c)
    } else {
        Sys.Observer.raiseCollectionChanged(d, [c]);
        Sys.Observer.raisePropertyChanged(d, "length")
    }
};
Sys.Observer.add = function(a, b) {
    var c = new Sys.CollectionChange(Sys.NotifyCollectionChangedAction.add, [b], a.length);
    Array.add(a, b);
    Sys.Observer._collectionChange(a, c)
};
Sys.Observer.addRange = function(a, b) {
    var c = new Sys.CollectionChange(Sys.NotifyCollectionChangedAction.add, b, a.length);
    Array.addRange(a, b);
    Sys.Observer._collectionChange(a, c)
};
Sys.Observer.clear = function(a) {
    var b = Array.clone(a);
    Array.clear(a);
    Sys.Observer._collectionChange(a, new Sys.CollectionChange(Sys.NotifyCollectionChangedAction.reset, null, -1, b, 0))
};
Sys.Observer.insert = function(a, b, c) {
    Array.insert(a, b, c);
    Sys.Observer._collectionChange(a, new Sys.CollectionChange(Sys.NotifyCollectionChangedAction.add, [c], b))
};
Sys.Observer.remove = function(a, b) {
    var c = Array.indexOf(a, b);
    if (c !== -1) {
        Array.remove(a, b);
        Sys.Observer._collectionChange(a, new Sys.CollectionChange(Sys.NotifyCollectionChangedAction.remove, null, -1, [b], c));
        return true
    }
    return false
};
Sys.Observer.removeAt = function(b, a) {
    if (a > -1 && a < b.length) {
        var c = b[a];
        Array.removeAt(b, a);
        Sys.Observer._collectionChange(b, new Sys.CollectionChange(Sys.NotifyCollectionChangedAction.remove, null, -1, [c], a))
    }
};
Sys.Observer.raiseCollectionChanged = function(b, a) {
    Sys.Observer.raiseEvent(b, "collectionChanged", new Sys.NotifyCollectionChangedEventArgs(a))
};
Sys.Observer._observeMethods = {
    add_propertyChanged: function(a) {
        Sys.Observer._addEventHandler(this, "propertyChanged", a)
    },
    remove_propertyChanged: function(a) {
        Sys.Observer._removeEventHandler(this, "propertyChanged", a)
    },
    addEventHandler: function(a, b) {
        Sys.Observer._addEventHandler(this, a, b)
    },
    removeEventHandler: function(a, b) {
        Sys.Observer._removeEventHandler(this, a, b)
    },
    get_isUpdating: function() {
        return Sys.Observer.isUpdating(this)
    },
    beginUpdate: function() {
        Sys.Observer.beginUpdate(this)
    },
    endUpdate: function() {
        Sys.Observer.endUpdate(this)
    },
    setValue: function(b, a) {
        Sys.Observer._setValue(this, b, a)
    },
    raiseEvent: function(b, a) {
        Sys.Observer.raiseEvent(this, b, a)
    },
    raisePropertyChanged: function(a) {
        Sys.Observer.raiseEvent(this, "propertyChanged", new Sys.PropertyChangedEventArgs(a))
    }
};
Sys.Observer._arrayMethods = {
    add_collectionChanged: function(a) {
        Sys.Observer._addEventHandler(this, "collectionChanged", a)
    },
    remove_collectionChanged: function(a) {
        Sys.Observer._removeEventHandler(this, "collectionChanged", a)
    },
    add: function(a) {
        Sys.Observer.add(this, a)
    },
    addRange: function(a) {
        Sys.Observer.addRange(this, a)
    },
    clear: function() {
        Sys.Observer.clear(this)
    },
    insert: function(a, b) {
        Sys.Observer.insert(this, a, b)
    },
    remove: function(a) {
        return Sys.Observer.remove(this, a)
    },
    removeAt: function(a) {
        Sys.Observer.removeAt(this, a)
    },
    raiseCollectionChanged: function(a) {
        Sys.Observer.raiseEvent(this, "collectionChanged", new Sys.NotifyCollectionChangedEventArgs(a))
    }
};
Sys.Observer._getContext = function(b, c) {
    var a = b._observerContext;
    if (a) return a();
    if (c) return (b._observerContext = Sys.Observer._createContext())();
    return null
};
Sys.Observer._createContext = function() {
    var a = {
        events: new Sys.EventHandlerList
    };
    return function() {
        return a
    }
};
Date._appendPreOrPostMatch = function(e, b) {
    var d = 0,
        a = false;
    for (var c = 0, g = e.length; c < g; c++) {
        var f = e.charAt(c);
        switch (f) {
            case "'":
                if (a) b.append("'");
                else d++;
                a = false;
                break;
            case "\\":
                if (a) b.append("\\");
                a = !a;
                break;
            default:
                b.append(f);
                a = false
        }
    }
    return d
};
Date._expandFormat = function(a, b) {
    if (!b) b = "F";
    var c = b.length;
    if (c === 1) switch (b) {
        case "d":
            return a.ShortDatePattern;
        case "D":
            return a.LongDatePattern;
        case "t":
            return a.ShortTimePattern;
        case "T":
            return a.LongTimePattern;
        case "f":
            return a.LongDatePattern + " " + a.ShortTimePattern;
        case "F":
            return a.FullDateTimePattern;
        case "M":
        case "m":
            return a.MonthDayPattern;
        case "s":
            return a.SortableDateTimePattern;
        case "Y":
        case "y":
            return a.YearMonthPattern;
        default:
            throw Error.format(Sys.Res.formatInvalidString)
    } else if (c === 2 && b.charAt(0) === "%") b = b.charAt(1);
    return b
};
Date._expandYear = function(c, a) {
    var d = new Date,
        e = Date._getEra(d);
    if (a < 100) {
        var b = Date._getEraYear(d, c, e);
        a += b - b % 100;
        if (a > c.Calendar.TwoDigitYearMax) a -= 100
    }
    return a
};
Date._getEra = function(e, c) {
    if (!c) return 0;
    var b, d = e.getTime();
    for (var a = 0, f = c.length; a < f; a += 4) {
        b = c[a + 2];
        if (b === null || d >= b) return a
    }
    return 0
};
Date._getEraYear = function(d, b, e, c) {
    var a = d.getFullYear();
    if (!c && b.eras) a -= b.eras[e + 3];
    return a
};
Date._getParseRegExp = function(b, e) {
    if (!b._parseRegExp) b._parseRegExp = {};
    else if (b._parseRegExp[e]) return b._parseRegExp[e];
    var c = Date._expandFormat(b, e);
    c = c.replace(/([\^\$\.\*\+\?\|\[\]\(\)\{\}])/g, "\\\\$1");
    var a = new Sys.StringBuilder("^"),
        j = [],
        f = 0,
        i = 0,
        h = Date._getTokenRegExp(),
        d;
    while ((d = h.exec(c)) !== null) {
        var l = c.slice(f, d.index);
        f = h.lastIndex;
        i += Date._appendPreOrPostMatch(l, a);
        if (i % 2 === 1) {
            a.append(d[0]);
            continue
        }
        switch (d[0]) {
            case "dddd":
            case "ddd":
            case "MMMM":
            case "MMM":
            case "gg":
            case "g":
                a.append("(\\D+)");
                break;
            case "tt":
            case "t":
                a.append("(\\D*)");
                break;
            case "yyyy":
                a.append("(\\d{4})");
                break;
            case "fff":
                a.append("(\\d{3})");
                break;
            case "ff":
                a.append("(\\d{2})");
                break;
            case "f":
                a.append("(\\d)");
                break;
            case "dd":
            case "d":
            case "MM":
            case "M":
            case "yy":
            case "y":
            case "HH":
            case "H":
            case "hh":
            case "h":
            case "mm":
            case "m":
            case "ss":
            case "s":
                a.append("(\\d\\d?)");
                break;
            case "zzz":
                a.append("([+-]?\\d\\d?:\\d{2})");
                break;
            case "zz":
            case "z":
                a.append("([+-]?\\d\\d?)");
                break;
            case "/":
                a.append("(\\" + b.DateSeparator + ")")
        }
        Array.add(j, d[0])
    }
    Date._appendPreOrPostMatch(c.slice(f), a);
    a.append("$");
    var k = a.toString().replace(/\s+/g, "\\s+"),
        g = {
            "regExp": k,
            "groups": j
        };
    b._parseRegExp[e] = g;
    return g
};
Date._getTokenRegExp = function() {
    return /\/|dddd|ddd|dd|d|MMMM|MMM|MM|M|yyyy|yy|y|hh|h|HH|H|mm|m|ss|s|tt|t|fff|ff|f|zzz|zz|z|gg|g/g
};
Date.parseLocale = function(a) {
    return Date._parse(a, Sys.CultureInfo.CurrentCulture, arguments)
};
Date.parseInvariant = function(a) {
    return Date._parse(a, Sys.CultureInfo.InvariantCulture, arguments)
};
Date._parse = function(h, d, i) {
    var a, c, b, f, e, g = false;
    for (a = 1, c = i.length; a < c; a++) {
        f = i[a];
        if (f) {
            g = true;
            b = Date._parseExact(h, f, d);
            if (b) return b
        }
    }
    if (!g) {
        e = d._getDateTimeFormats();
        for (a = 0, c = e.length; a < c; a++) {
            b = Date._parseExact(h, e[a], d);
            if (b) return b
        }
    }
    return null
};
Date._parseExact = function(w, D, k) {
    w = w.trim();
    var g = k.dateTimeFormat,
        A = Date._getParseRegExp(g, D),
        C = (new RegExp(A.regExp)).exec(w);
    if (C === null) return null;
    var B = A.groups,
        x = null,
        e = null,
        c = null,
        j = null,
        i = null,
        d = 0,
        h, q = 0,
        r = 0,
        f = 0,
        n = null,
        v = false;
    for (var t = 0, E = B.length; t < E; t++) {
        var a = C[t + 1];
        if (a) switch (B[t]) {
            case "dd":
            case "d":
                j = parseInt(a, 10);
                if (j < 1 || j > 31) return null;
                break;
            case "MMMM":
                c = k._getMonthIndex(a);
                if (c < 0 || c > 11) return null;
                break;
            case "MMM":
                c = k._getAbbrMonthIndex(a);
                if (c < 0 || c > 11) return null;
                break;
            case "M":
            case "MM":
                c = parseInt(a, 10) - 1;
                if (c < 0 || c > 11) return null;
                break;
            case "y":
            case "yy":
                e = Date._expandYear(g, parseInt(a, 10));
                if (e < 0 || e > 9999) return null;
                break;
            case "yyyy":
                e = parseInt(a, 10);
                if (e < 0 || e > 9999) return null;
                break;
            case "h":
            case "hh":
                d = parseInt(a, 10);
                if (d === 12) d = 0;
                if (d < 0 || d > 11) return null;
                break;
            case "H":
            case "HH":
                d = parseInt(a, 10);
                if (d < 0 || d > 23) return null;
                break;
            case "m":
            case "mm":
                q = parseInt(a, 10);
                if (q < 0 || q > 59) return null;
                break;
            case "s":
            case "ss":
                r = parseInt(a, 10);
                if (r < 0 || r > 59) return null;
                break;
            case "tt":
            case "t":
                var z = a.toUpperCase();
                v = z === g.PMDesignator.toUpperCase();
                if (!v && z !== g.AMDesignator.toUpperCase()) return null;
                break;
            case "f":
                f = parseInt(a, 10) * 100;
                if (f < 0 || f > 999) return null;
                break;
            case "ff":
                f = parseInt(a, 10) * 10;
                if (f < 0 || f > 999) return null;
                break;
            case "fff":
                f = parseInt(a, 10);
                if (f < 0 || f > 999) return null;
                break;
            case "dddd":
                i = k._getDayIndex(a);
                if (i < 0 || i > 6) return null;
                break;
            case "ddd":
                i = k._getAbbrDayIndex(a);
                if (i < 0 || i > 6) return null;
                break;
            case "zzz":
                var u = a.split(/:/);
                if (u.length !== 2) return null;
                h = parseInt(u[0], 10);
                if (h < -12 || h > 13) return null;
                var o = parseInt(u[1], 10);
                if (o < 0 || o > 59) return null;
                n = h * 60 + (a.startsWith("-") ? -o : o);
                break;
            case "z":
            case "zz":
                h = parseInt(a, 10);
                if (h < -12 || h > 13) return null;
                n = h * 60;
                break;
            case "g":
            case "gg":
                var p = a;
                if (!p || !g.eras) return null;
                p = p.toLowerCase().trim();
                for (var s = 0, F = g.eras.length; s < F; s += 4)
                    if (p === g.eras[s + 1].toLowerCase()) {
                        x = s;
                        break
                    }
                if (x === null) return null
        }
    }
    var b = new Date,
        l, m = g.Calendar.convert;
    if (m) l = m.fromGregorian(b);
    if (!m) l = [b.getFullYear(), b.getMonth(), b.getDate()];
    if (e === null) e = l[0];
    else if (g.eras) e += g.eras[(x || 0) + 3];
    if (c === null) c = l[1];
    if (j === null) j = l[2];
    if (m) {
        b = m.toGregorian(e, c, j);
        if (b === null) return null
    } else {
        b.setFullYear(e, c, j);
        if (b.getDate() !== j) return null;
        if (i !== null && b.getDay() !== i) return null
    }
    if (v && d < 12) d += 12;
    b.setHours(d, q, r, f);
    if (n !== null) {
        var y = b.getMinutes() - (n + b.getTimezoneOffset());
        b.setHours(b.getHours() + parseInt(y / 60, 10), y % 60)
    }
    return b
};
Date.prototype.format = function(a) {
    return this._toFormattedString(a, Sys.CultureInfo.InvariantCulture)
};
Date.prototype.localeFormat = function(a) {
    return this._toFormattedString(a, Sys.CultureInfo.CurrentCulture)
};
Date.prototype._toFormattedString = function(e, j) {
    var b = j.dateTimeFormat,
        n = b.Calendar.convert;
    if (!e || !e.length || e === "i")
        if (j && j.name.length)
            if (n) return this._toFormattedString(b.FullDateTimePattern, j);
            else {
                var r = new Date(this.getTime()),
                    x = Date._getEra(this, b.eras);
                r.setFullYear(Date._getEraYear(this, b, x));
                return r.toLocaleString()
            } else return this.toString();
    var l = b.eras,
        k = e === "s";
    e = Date._expandFormat(b, e);
    var a = new Sys.StringBuilder,
        c;

    function d(a) {
        if (a < 10) return "0" + a;
        return a.toString()
    }

    function m(a) {
        if (a < 10) return "00" + a;
        if (a < 100) return "0" + a;
        return a.toString()
    }

    function v(a) {
        if (a < 10) return "000" + a;
        else if (a < 100) return "00" + a;
        else if (a < 1000) return "0" + a;
        return a.toString()
    }
    var h, p, t = /([^d]|^)(d|dd)([^d]|$)/g;

    function s() {
        if (h || p) return h;
        h = t.test(e);
        p = true;
        return h
    }
    var q = 0,
        o = Date._getTokenRegExp(),
        f;
    if (!k && n) f = n.fromGregorian(this);
    for (; true;) {
        var w = o.lastIndex,
            i = o.exec(e),
            u = e.slice(w, i ? i.index : e.length);
        q += Date._appendPreOrPostMatch(u, a);
        if (!i) break;
        if (q % 2 === 1) {
            a.append(i[0]);
            continue
        }

        function g(a, b) {
            if (f) return f[b];
            switch (b) {
                case 0:
                    return a.getFullYear();
                case 1:
                    return a.getMonth();
                case 2:
                    return a.getDate()
            }
        }
        switch (i[0]) {
            case "dddd":
                a.append(b.DayNames[this.getDay()]);
                break;
            case "ddd":
                a.append(b.AbbreviatedDayNames[this.getDay()]);
                break;
            case "dd":
                h = true;
                a.append(d(g(this, 2)));
                break;
            case "d":
                h = true;
                a.append(g(this, 2));
                break;
            case "MMMM":
                a.append(b.MonthGenitiveNames && s() ? b.MonthGenitiveNames[g(this, 1)] : b.MonthNames[g(this, 1)]);
                break;
            case "MMM":
                a.append(b.AbbreviatedMonthGenitiveNames && s() ? b.AbbreviatedMonthGenitiveNames[g(this, 1)] : b.AbbreviatedMonthNames[g(this, 1)]);
                break;
            case "MM":
                a.append(d(g(this, 1) + 1));
                break;
            case "M":
                a.append(g(this, 1) + 1);
                break;
            case "yyyy":
                a.append(v(f ? f[0] : Date._getEraYear(this, b, Date._getEra(this, l), k)));
                break;
            case "yy":
                a.append(d((f ? f[0] : Date._getEraYear(this, b, Date._getEra(this, l), k)) % 100));
                break;
            case "y":
                a.append((f ? f[0] : Date._getEraYear(this, b, Date._getEra(this, l), k)) % 100);
                break;
            case "hh":
                c = this.getHours() % 12;
                if (c === 0) c = 12;
                a.append(d(c));
                break;
            case "h":
                c = this.getHours() % 12;
                if (c === 0) c = 12;
                a.append(c);
                break;
            case "HH":
                a.append(d(this.getHours()));
                break;
            case "H":
                a.append(this.getHours());
                break;
            case "mm":
                a.append(d(this.getMinutes()));
                break;
            case "m":
                a.append(this.getMinutes());
                break;
            case "ss":
                a.append(d(this.getSeconds()));
                break;
            case "s":
                a.append(this.getSeconds());
                break;
            case "tt":
                a.append(this.getHours() < 12 ? b.AMDesignator : b.PMDesignator);
                break;
            case "t":
                a.append((this.getHours() < 12 ? b.AMDesignator : b.PMDesignator).charAt(0));
                break;
            case "f":
                a.append(m(this.getMilliseconds()).charAt(0));
                break;
            case "ff":
                a.append(m(this.getMilliseconds()).substr(0, 2));
                break;
            case "fff":
                a.append(m(this.getMilliseconds()));
                break;
            case "z":
                c = this.getTimezoneOffset() / 60;
                a.append((c <= 0 ? "+" : "-") + Math.floor(Math.abs(c)));
                break;
            case "zz":
                c = this.getTimezoneOffset() / 60;
                a.append((c <= 0 ? "+" : "-") + d(Math.floor(Math.abs(c))));
                break;
            case "zzz":
                c = this.getTimezoneOffset() / 60;
                a.append((c <= 0 ? "+" : "-") + d(Math.floor(Math.abs(c))) + ":" + d(Math.abs(this.getTimezoneOffset() % 60)));
                break;
            case "g":
            case "gg":
                if (b.eras) a.append(b.eras[Date._getEra(this, l) + 1]);
                break;
            case "/":
                a.append(b.DateSeparator)
        }
    }
    return a.toString()
};
String.localeFormat = function() {
    return String._toFormattedString(true, arguments)
};
Number.parseLocale = function(a) {
    return Number._parse(a, Sys.CultureInfo.CurrentCulture)
};
Number.parseInvariant = function(a) {
    return Number._parse(a, Sys.CultureInfo.InvariantCulture)
};
Number._parse = function(b, o) {
    b = b.trim();
    if (b.match(/^[+-]?infinity$/i)) return parseFloat(b);
    if (b.match(/^0x[a-f0-9]+$/i)) return parseInt(b);
    var a = o.numberFormat,
        g = Number._parseNumberNegativePattern(b, a, a.NumberNegativePattern),
        h = g[0],
        e = g[1];
    if (h === "" && a.NumberNegativePattern !== 1) {
        g = Number._parseNumberNegativePattern(b, a, 1);
        h = g[0];
        e = g[1]
    }
    if (h === "") h = "+";
    var j, d, f = e.indexOf("e");
    if (f < 0) f = e.indexOf("E");
    if (f < 0) {
        d = e;
        j = null
    } else {
        d = e.substr(0, f);
        j = e.substr(f + 1)
    }
    var c, k, m = d.indexOf(a.NumberDecimalSeparator);
    if (m < 0) {
        c = d;
        k = null
    } else {
        c = d.substr(0, m);
        k = d.substr(m + a.NumberDecimalSeparator.length)
    }
    c = c.split(a.NumberGroupSeparator).join("");
    var n = a.NumberGroupSeparator.replace(/\u00A0/g, " ");
    if (a.NumberGroupSeparator !== n) c = c.split(n).join("");
    var l = h + c;
    if (k !== null) l += "." + k;
    if (j !== null) {
        var i = Number._parseNumberNegativePattern(j, a, 1);
        if (i[0] === "") i[0] = "+";
        l += "e" + i[0] + i[1]
    }
    if (l.match(/^[+-]?\d*\.?\d*(e[+-]?\d+)?$/)) return parseFloat(l);
    return Number.NaN
};
Number._parseNumberNegativePattern = function(a, d, e) {
    var b = d.NegativeSign,
        c = d.PositiveSign;
    switch (e) {
        case 4:
            b = " " + b;
            c = " " + c;
        case 3:
            if (a.endsWith(b)) return ["-", a.substr(0, a.length - b.length)];
            else if (a.endsWith(c)) return ["+", a.substr(0, a.length - c.length)];
            break;
        case 2:
            b += " ";
            c += " ";
        case 1:
            if (a.startsWith(b)) return ["-", a.substr(b.length)];
            else if (a.startsWith(c)) return ["+", a.substr(c.length)];
            break;
        case 0:
            if (a.startsWith("(") && a.endsWith(")")) return ["-", a.substr(1, a.length - 2)]
    }
    return ["", a]
};
Number.prototype.format = function(a) {
    return this._toFormattedString(a, Sys.CultureInfo.InvariantCulture)
};
Number.prototype.localeFormat = function(a) {
    return this._toFormattedString(a, Sys.CultureInfo.CurrentCulture)
};
Number.prototype._toFormattedString = function(e, j) {
    if (!e || e.length === 0 || e === "i")
        if (j && j.name.length > 0) return this.toLocaleString();
        else return this.toString();
    var o = ["n %", "n%", "%n"],
        n = ["-n %", "-n%", "-%n"],
        p = ["(n)", "-n", "- n", "n-", "n -"],
        m = ["$n", "n$", "$ n", "n $"],
        l = ["($n)", "-$n", "$-n", "$n-", "(n$)", "-n$", "n-$", "n$-", "-n $", "-$ n", "n $-", "$ n-", "$ -n", "n- $", "($ n)", "(n $)"];

    function g(a, c, d) {
        for (var b = a.length; b < c; b++) a = d ? "0" + a : a + "0";
        return a
    }

    function i(j, i, l, n, p) {
        var h = l[0],
            k = 1,
            o = Math.pow(10, i),
            m = Math.round(j * o) / o;
        if (!isFinite(m)) m = j;
        j = m;
        var b = j.toString(),
            a = "",
            c, e = b.split(/e/i);
        b = e[0];
        c = e.length > 1 ? parseInt(e[1]) : 0;
        e = b.split(".");
        b = e[0];
        a = e.length > 1 ? e[1] : "";
        var q;
        if (c > 0) {
            a = g(a, c, false);
            b += a.slice(0, c);
            a = a.substr(c)
        } else if (c < 0) {
            c = -c;
            b = g(b, c + 1, true);
            a = b.slice(-c, b.length) + a;
            b = b.slice(0, -c)
        }
        if (i > 0) {
            if (a.length > i) a = a.slice(0, i);
            else a = g(a, i, false);
            a = p + a
        } else a = "";
        var d = b.length - 1,
            f = "";
        while (d >= 0) {
            if (h === 0 || h > d)
                if (f.length > 0) return b.slice(0, d + 1) + n + f + a;
                else return b.slice(0, d + 1) + a;
            if (f.length > 0) f = b.slice(d - h + 1, d + 1) + n + f;
            else f = b.slice(d - h + 1, d + 1);
            d -= h;
            if (k < l.length) {
                h = l[k];
                k++
            }
        }
        return b.slice(0, d + 1) + n + f + a
    }
    var a = j.numberFormat,
        d = Math.abs(this);
    if (!e) e = "D";
    var b = -1;
    if (e.length > 1) b = parseInt(e.slice(1), 10);
    var c;
    switch (e.charAt(0)) {
        case "d":
        case "D":
            c = "n";
            if (b !== -1) d = g("" + d, b, true);
            if (this < 0) d = -d;
            break;
        case "c":
        case "C":
            if (this < 0) c = l[a.CurrencyNegativePattern];
            else c = m[a.CurrencyPositivePattern];
            if (b === -1) b = a.CurrencyDecimalDigits;
            d = i(Math.abs(this), b, a.CurrencyGroupSizes, a.CurrencyGroupSeparator, a.CurrencyDecimalSeparator);
            break;
        case "n":
        case "N":
            if (this < 0) c = p[a.NumberNegativePattern];
            else c = "n";
            if (b === -1) b = a.NumberDecimalDigits;
            d = i(Math.abs(this), b, a.NumberGroupSizes, a.NumberGroupSeparator, a.NumberDecimalSeparator);
            break;
        case "p":
        case "P":
            if (this < 0) c = n[a.PercentNegativePattern];
            else c = o[a.PercentPositivePattern];
            if (b === -1) b = a.PercentDecimalDigits;
            d = i(Math.abs(this) * 100, b, a.PercentGroupSizes, a.PercentGroupSeparator, a.PercentDecimalSeparator);
            break;
        default:
            throw Error.format(Sys.Res.formatBadFormatSpecifier)
    }
    var k = /n|\$|-|%/g,
        f = "";
    for (; true;) {
        var q = k.lastIndex,
            h = k.exec(c);
        f += c.slice(q, h ? h.index : c.length);
        if (!h) break;
        switch (h[0]) {
            case "n":
                f += d;
                break;
            case "$":
                f += a.CurrencySymbol;
                break;
            case "-":
                if (/[1-9]/.test(d)) f += a.NegativeSign;
                break;
            case "%":
                f += a.PercentSymbol
        }
    }
    return f
};
Sys.CultureInfo = function(c, b, a) {
    this.name = c;
    this.numberFormat = b;
    this.dateTimeFormat = a
};
Sys.CultureInfo.prototype = {
    _getDateTimeFormats: function() {
        if (!this._dateTimeFormats) {
            var a = this.dateTimeFormat;
            this._dateTimeFormats = [a.MonthDayPattern, a.YearMonthPattern, a.ShortDatePattern, a.ShortTimePattern, a.LongDatePattern, a.LongTimePattern, a.FullDateTimePattern, a.RFC1123Pattern, a.SortableDateTimePattern, a.UniversalSortableDateTimePattern]
        }
        return this._dateTimeFormats
    },
    _getIndex: function(c, d, e) {
        var b = this._toUpper(c),
            a = Array.indexOf(d, b);
        if (a === -1) a = Array.indexOf(e, b);
        return a
    },
    _getMonthIndex: function(a) {
        if (!this._upperMonths) {
            this._upperMonths = this._toUpperArray(this.dateTimeFormat.MonthNames);
            this._upperMonthsGenitive = this._toUpperArray(this.dateTimeFormat.MonthGenitiveNames)
        }
        return this._getIndex(a, this._upperMonths, this._upperMonthsGenitive)
    },
    _getAbbrMonthIndex: function(a) {
        if (!this._upperAbbrMonths) {
            this._upperAbbrMonths = this._toUpperArray(this.dateTimeFormat.AbbreviatedMonthNames);
            this._upperAbbrMonthsGenitive = this._toUpperArray(this.dateTimeFormat.AbbreviatedMonthGenitiveNames)
        }
        return this._getIndex(a, this._upperAbbrMonths, this._upperAbbrMonthsGenitive)
    },
    _getDayIndex: function(a) {
        if (!this._upperDays) this._upperDays = this._toUpperArray(this.dateTimeFormat.DayNames);
        return Array.indexOf(this._upperDays, this._toUpper(a))
    },
    _getAbbrDayIndex: function(a) {
        if (!this._upperAbbrDays) this._upperAbbrDays = this._toUpperArray(this.dateTimeFormat.AbbreviatedDayNames);
        return Array.indexOf(this._upperAbbrDays, this._toUpper(a))
    },
    _toUpperArray: function(c) {
        var b = [];
        for (var a = 0, d = c.length; a < d; a++) b[a] = this._toUpper(c[a]);
        return b
    },
    _toUpper: function(a) {
        return a.split("\u00a0").join(" ").toUpperCase()
    }
};
Sys.CultureInfo.registerClass("Sys.CultureInfo");
Sys.CultureInfo._parse = function(a) {
    var b = a.dateTimeFormat;
    if (b && !b.eras) b.eras = a.eras;
    return new Sys.CultureInfo(a.name, a.numberFormat, b)
};
Sys.CultureInfo.InvariantCulture = Sys.CultureInfo._parse({
    "name": "",
    "numberFormat": {
        "CurrencyDecimalDigits": 2,
        "CurrencyDecimalSeparator": ".",
        "IsReadOnly": true,
        "CurrencyGroupSizes": [3],
        "NumberGroupSizes": [3],
        "PercentGroupSizes": [3],
        "CurrencyGroupSeparator": ",",
        "CurrencySymbol": "\u00a4",
        "NaNSymbol": "NaN",
        "CurrencyNegativePattern": 0,
        "NumberNegativePattern": 1,
        "PercentPositivePattern": 0,
        "PercentNegativePattern": 0,
        "NegativeInfinitySymbol": "-Infinity",
        "NegativeSign": "-",
        "NumberDecimalDigits": 2,
        "NumberDecimalSeparator": ".",
        "NumberGroupSeparator": ",",
        "CurrencyPositivePattern": 0,
        "PositiveInfinitySymbol": "Infinity",
        "PositiveSign": "+",
        "PercentDecimalDigits": 2,
        "PercentDecimalSeparator": ".",
        "PercentGroupSeparator": ",",
        "PercentSymbol": "%",
        "PerMilleSymbol": "\u2030",
        "NativeDigits": ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
        "DigitSubstitution": 1
    },
    "dateTimeFormat": {
        "AMDesignator": "AM",
        "Calendar": {
            "MinSupportedDateTime": "@-62135568000000@",
            "MaxSupportedDateTime": "@253402300799999@",
            "AlgorithmType": 1,
            "CalendarType": 1,
            "Eras": [1],
            "TwoDigitYearMax": 2029,
            "IsReadOnly": true
        },
        "DateSeparator": "/",
        "FirstDayOfWeek": 0,
        "CalendarWeekRule": 0,
        "FullDateTimePattern": "dddd, dd MMMM yyyy HH:mm:ss",
        "LongDatePattern": "dddd, dd MMMM yyyy",
        "LongTimePattern": "HH:mm:ss",
        "MonthDayPattern": "MMMM dd",
        "PMDesignator": "PM",
        "RFC1123Pattern": "ddd, dd MMM yyyy HH':'mm':'ss 'GMT'",
        "ShortDatePattern": "MM/dd/yyyy",
        "ShortTimePattern": "HH:mm",
        "SortableDateTimePattern": "yyyy'-'MM'-'dd'T'HH':'mm':'ss",
        "TimeSeparator": ":",
        "UniversalSortableDateTimePattern": "yyyy'-'MM'-'dd HH':'mm':'ss'Z'",
        "YearMonthPattern": "yyyy MMMM",
        "AbbreviatedDayNames": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        "ShortestDayNames": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
        "DayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        "AbbreviatedMonthNames": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""],
        "MonthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ""],
        "IsReadOnly": true,
        "NativeCalendarName": "Gregorian Calendar",
        "AbbreviatedMonthGenitiveNames": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""],
        "MonthGenitiveNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ""]
    },
    "eras": [1, "A.D.", null, 0]
});
if (typeof __cultureInfo === "object") {
    Sys.CultureInfo.CurrentCulture = Sys.CultureInfo._parse(__cultureInfo);
    delete __cultureInfo
} else Sys.CultureInfo.CurrentCulture = Sys.CultureInfo._parse({
    "name": "en-US",
    "numberFormat": {
        "CurrencyDecimalDigits": 2,
        "CurrencyDecimalSeparator": ".",
        "IsReadOnly": false,
        "CurrencyGroupSizes": [3],
        "NumberGroupSizes": [3],
        "PercentGroupSizes": [3],
        "CurrencyGroupSeparator": ",",
        "CurrencySymbol": "$",
        "NaNSymbol": "NaN",
        "CurrencyNegativePattern": 0,
        "NumberNegativePattern": 1,
        "PercentPositivePattern": 0,
        "PercentNegativePattern": 0,
        "NegativeInfinitySymbol": "-Infinity",
        "NegativeSign": "-",
        "NumberDecimalDigits": 2,
        "NumberDecimalSeparator": ".",
        "NumberGroupSeparator": ",",
        "CurrencyPositivePattern": 0,
        "PositiveInfinitySymbol": "Infinity",
        "PositiveSign": "+",
        "PercentDecimalDigits": 2,
        "PercentDecimalSeparator": ".",
        "PercentGroupSeparator": ",",
        "PercentSymbol": "%",
        "PerMilleSymbol": "\u2030",
        "NativeDigits": ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
        "DigitSubstitution": 1
    },
    "dateTimeFormat": {
        "AMDesignator": "AM",
        "Calendar": {
            "MinSupportedDateTime": "@-62135568000000@",
            "MaxSupportedDateTime": "@253402300799999@",
            "AlgorithmType": 1,
            "CalendarType": 1,
            "Eras": [1],
            "TwoDigitYearMax": 2029,
            "IsReadOnly": false
        },
        "DateSeparator": "/",
        "FirstDayOfWeek": 0,
        "CalendarWeekRule": 0,
        "FullDateTimePattern": "dddd, MMMM dd, yyyy h:mm:ss tt",
        "LongDatePattern": "dddd, MMMM dd, yyyy",
        "LongTimePattern": "h:mm:ss tt",
        "MonthDayPattern": "MMMM dd",
        "PMDesignator": "PM",
        "RFC1123Pattern": "ddd, dd MMM yyyy HH':'mm':'ss 'GMT'",
        "ShortDatePattern": "M/d/yyyy",
        "ShortTimePattern": "h:mm tt",
        "SortableDateTimePattern": "yyyy'-'MM'-'dd'T'HH':'mm':'ss",
        "TimeSeparator": ":",
        "UniversalSortableDateTimePattern": "yyyy'-'MM'-'dd HH':'mm':'ss'Z'",
        "YearMonthPattern": "MMMM, yyyy",
        "AbbreviatedDayNames": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        "ShortestDayNames": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
        "DayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        "AbbreviatedMonthNames": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""],
        "MonthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ""],
        "IsReadOnly": false,
        "NativeCalendarName": "Gregorian Calendar",
        "AbbreviatedMonthGenitiveNames": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""],
        "MonthGenitiveNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ""]
    },
    "eras": [1, "A.D.", null, 0]
});
Type.registerNamespace("Sys.Serialization");
Sys.Serialization.JavaScriptSerializer = function() {};
Sys.Serialization.JavaScriptSerializer.registerClass("Sys.Serialization.JavaScriptSerializer");
Sys.Serialization.JavaScriptSerializer._charsToEscapeRegExs = [];
Sys.Serialization.JavaScriptSerializer._charsToEscape = [];
Sys.Serialization.JavaScriptSerializer._dateRegEx = new RegExp('(^|[^\\\\])\\"\\\\/Date\\((-?[0-9]+)(?:[a-zA-Z]|(?:\\+|-)[0-9]{4})?\\)\\\\/\\"', "g");
Sys.Serialization.JavaScriptSerializer._escapeChars = {};
Sys.Serialization.JavaScriptSerializer._escapeRegEx = new RegExp('["\\\\\\x00-\\x1F]', "i");
Sys.Serialization.JavaScriptSerializer._escapeRegExGlobal = new RegExp('["\\\\\\x00-\\x1F]', "g");
Sys.Serialization.JavaScriptSerializer._jsonRegEx = new RegExp("[^,:{}\\[\\]0-9.\\-+Eaeflnr-u \\n\\r\\t]", "g");
Sys.Serialization.JavaScriptSerializer._jsonStringRegEx = new RegExp('"(\\\\.|[^"\\\\])*"', "g");
Sys.Serialization.JavaScriptSerializer._serverTypeFieldName = "__type";
Sys.Serialization.JavaScriptSerializer._init = function() {
    var c = ["\\u0000", "\\u0001", "\\u0002", "\\u0003", "\\u0004", "\\u0005", "\\u0006", "\\u0007", "\\b", "\\t", "\\n", "\\u000b", "\\f", "\\r", "\\u000e", "\\u000f", "\\u0010", "\\u0011", "\\u0012", "\\u0013", "\\u0014", "\\u0015", "\\u0016", "\\u0017", "\\u0018", "\\u0019", "\\u001a", "\\u001b", "\\u001c", "\\u001d", "\\u001e", "\\u001f"];
    Sys.Serialization.JavaScriptSerializer._charsToEscape[0] = "\\";
    Sys.Serialization.JavaScriptSerializer._charsToEscapeRegExs["\\"] = new RegExp("\\\\", "g");
    Sys.Serialization.JavaScriptSerializer._escapeChars["\\"] = "\\\\";
    Sys.Serialization.JavaScriptSerializer._charsToEscape[1] = '"';
    Sys.Serialization.JavaScriptSerializer._charsToEscapeRegExs['"'] = new RegExp('"', "g");
    Sys.Serialization.JavaScriptSerializer._escapeChars['"'] = '\\"';
    for (var a = 0; a < 32; a++) {
        var b = String.fromCharCode(a);
        Sys.Serialization.JavaScriptSerializer._charsToEscape[a + 2] = b;
        Sys.Serialization.JavaScriptSerializer._charsToEscapeRegExs[b] = new RegExp(b, "g");
        Sys.Serialization.JavaScriptSerializer._escapeChars[b] = c[a]
    }
};
Sys.Serialization.JavaScriptSerializer._serializeBooleanWithBuilder = function(b, a) {
    a.append(b.toString())
};
Sys.Serialization.JavaScriptSerializer._serializeNumberWithBuilder = function(a, b) {
    if (isFinite(a)) b.append(String(a));
    else throw Error.invalidOperation(Sys.Res.cannotSerializeNonFiniteNumbers)
};
Sys.Serialization.JavaScriptSerializer._serializeStringWithBuilder = function(a, c) {
    c.append('"');
    if (Sys.Serialization.JavaScriptSerializer._escapeRegEx.test(a)) {
        if (Sys.Serialization.JavaScriptSerializer._charsToEscape.length === 0) Sys.Serialization.JavaScriptSerializer._init();
        if (a.length < 128) a = a.replace(Sys.Serialization.JavaScriptSerializer._escapeRegExGlobal, function(a) {
            return Sys.Serialization.JavaScriptSerializer._escapeChars[a]
        });
        else
            for (var d = 0; d < 34; d++) {
                var b = Sys.Serialization.JavaScriptSerializer._charsToEscape[d];
                if (a.indexOf(b) !== -1)
                    if (Sys.Browser.agent === Sys.Browser.Opera || Sys.Browser.agent === Sys.Browser.FireFox) a = a.split(b).join(Sys.Serialization.JavaScriptSerializer._escapeChars[b]);
                    else a = a.replace(Sys.Serialization.JavaScriptSerializer._charsToEscapeRegExs[b], Sys.Serialization.JavaScriptSerializer._escapeChars[b])
            }
    }
    c.append(a);
    c.append('"')
};
Sys.Serialization.JavaScriptSerializer._serializeWithBuilder = function(b, a, i, g) {
    var c;
    switch (typeof b) {
        case "object":
            if (b)
                if (Number.isInstanceOfType(b)) Sys.Serialization.JavaScriptSerializer._serializeNumberWithBuilder(b, a);
                else if (Boolean.isInstanceOfType(b)) Sys.Serialization.JavaScriptSerializer._serializeBooleanWithBuilder(b, a);
                else if (String.isInstanceOfType(b)) Sys.Serialization.JavaScriptSerializer._serializeStringWithBuilder(b, a);
                else if (Array.isInstanceOfType(b)) {
                    a.append("[");
                    for (c = 0; c < b.length; ++c) {
                        if (c > 0) a.append(",");
                        Sys.Serialization.JavaScriptSerializer._serializeWithBuilder(b[c], a, false, g)
                    }
                    a.append("]")
                } else {
                    if (Date.isInstanceOfType(b)) {
                        a.append('"\\/Date(');
                        a.append(b.getTime());
                        a.append(')\\/"');
                        break
                    }
                    var d = [],
                        f = 0;
                    for (var e in b) {
                        if (e.startsWith("$")) continue;
                        if (e === Sys.Serialization.JavaScriptSerializer._serverTypeFieldName && f !== 0) {
                            d[f++] = d[0];
                            d[0] = e
                        } else d[f++] = e
                    }
                    if (i) d.sort();
                    a.append("{");
                    var j = false;
                    for (c = 0; c < f; c++) {
                        var h = b[d[c]];
                        if (typeof h !== "undefined" && typeof h !== "function") {
                            if (j) a.append(",");
                            else j = true;
                            Sys.Serialization.JavaScriptSerializer._serializeWithBuilder(d[c], a, i, g);
                            a.append(":");
                            Sys.Serialization.JavaScriptSerializer._serializeWithBuilder(h, a, i, g)
                        }
                    }
                    a.append("}")
                } else a.append("null");
            break;
        case "number":
            Sys.Serialization.JavaScriptSerializer._serializeNumberWithBuilder(b, a);
            break;
        case "string":
            Sys.Serialization.JavaScriptSerializer._serializeStringWithBuilder(b, a);
            break;
        case "boolean":
            Sys.Serialization.JavaScriptSerializer._serializeBooleanWithBuilder(b, a);
            break;
        default:
            a.append("null")
    }
};
Sys.Serialization.JavaScriptSerializer.serialize = function(b) {
    var a = new Sys.StringBuilder;
    Sys.Serialization.JavaScriptSerializer._serializeWithBuilder(b, a, false);
    return a.toString()
};
Sys.Serialization.JavaScriptSerializer.deserialize = function(data, secure) {
    if (data.length === 0) throw Error.argument("data", Sys.Res.cannotDeserializeEmptyString);
    try {
        var exp = data.replace(Sys.Serialization.JavaScriptSerializer._dateRegEx, "$1new Date($2)");
        if (secure && Sys.Serialization.JavaScriptSerializer._jsonRegEx.test(exp.replace(Sys.Serialization.JavaScriptSerializer._jsonStringRegEx, ""))) throw null;
        return eval("(" + exp + ")")
    } catch (a) {
        throw Error.argument("data", Sys.Res.cannotDeserializeInvalidJson)
    }
};
Type.registerNamespace("Sys.UI");
Sys.EventHandlerList = function() {
    this._list = {}
};
Sys.EventHandlerList.prototype = {
    _addHandler: function(b, a) {
        Array.add(this._getEvent(b, true), a)
    },
    addHandler: function(b, a) {
        this._addHandler(b, a)
    },
    _removeHandler: function(c, b) {
        var a = this._getEvent(c);
        if (!a) return;
        Array.remove(a, b)
    },
    removeHandler: function(b, a) {
        this._removeHandler(b, a)
    },
    getHandler: function(b) {
        var a = this._getEvent(b);
        if (!a || a.length === 0) return null;
        a = Array.clone(a);
        return function(c, d) {
            for (var b = 0, e = a.length; b < e; b++) a[b](c, d)
        }
    },
    _getEvent: function(a, b) {
        if (!this._list[a]) {
            if (!b) return null;
            this._list[a] = []
        }
        return this._list[a]
    }
};
Sys.EventHandlerList.registerClass("Sys.EventHandlerList");
Sys.CommandEventArgs = function(c, a, b) {
    Sys.CommandEventArgs.initializeBase(this);
    this._commandName = c;
    this._commandArgument = a;
    this._commandSource = b
};
Sys.CommandEventArgs.prototype = {
    _commandName: null,
    _commandArgument: null,
    _commandSource: null,
    get_commandName: function() {
        return this._commandName
    },
    get_commandArgument: function() {
        return this._commandArgument
    },
    get_commandSource: function() {
        return this._commandSource
    }
};
Sys.CommandEventArgs.registerClass("Sys.CommandEventArgs", Sys.CancelEventArgs);
Sys.INotifyPropertyChange = function() {};
Sys.INotifyPropertyChange.prototype = {};
Sys.INotifyPropertyChange.registerInterface("Sys.INotifyPropertyChange");
Sys.PropertyChangedEventArgs = function(a) {
    Sys.PropertyChangedEventArgs.initializeBase(this);
    this._propertyName = a
};
Sys.PropertyChangedEventArgs.prototype = {
    get_propertyName: function() {
        return this._propertyName
    }
};
Sys.PropertyChangedEventArgs.registerClass("Sys.PropertyChangedEventArgs", Sys.EventArgs);
Sys.INotifyDisposing = function() {};
Sys.INotifyDisposing.prototype = {};
Sys.INotifyDisposing.registerInterface("Sys.INotifyDisposing");
Sys.Component = function() {
    if (Sys.Application) Sys.Application.registerDisposableObject(this)
};
Sys.Component.prototype = {
    _id: null,
    _initialized: false,
    _updating: false,
    get_events: function() {
        if (!this._events) this._events = new Sys.EventHandlerList;
        return this._events
    },
    get_id: function() {
        return this._id
    },
    set_id: function(a) {
        this._id = a
    },
    get_isInitialized: function() {
        return this._initialized
    },
    get_isUpdating: function() {
        return this._updating
    },
    add_disposing: function(a) {
        this.get_events().addHandler("disposing", a)
    },
    remove_disposing: function(a) {
        this.get_events().removeHandler("disposing", a)
    },
    add_propertyChanged: function(a) {
        this.get_events().addHandler("propertyChanged", a)
    },
    remove_propertyChanged: function(a) {
        this.get_events().removeHandler("propertyChanged", a)
    },
    beginUpdate: function() {
        this._updating = true
    },
    dispose: function() {
        if (this._events) {
            var a = this._events.getHandler("disposing");
            if (a) a(this, Sys.EventArgs.Empty)
        }
        delete this._events;
        Sys.Application.unregisterDisposableObject(this);
        Sys.Application.removeComponent(this)
    },
    endUpdate: function() {
        this._updating = false;
        if (!this._initialized) this.initialize();
        this.updated()
    },
    initialize: function() {
        this._initialized = true
    },
    raisePropertyChanged: function(b) {
        if (!this._events) return;
        var a = this._events.getHandler("propertyChanged");
        if (a) a(this, new Sys.PropertyChangedEventArgs(b))
    },
    updated: function() {}
};
Sys.Component.registerClass("Sys.Component", null, Sys.IDisposable, Sys.INotifyPropertyChange, Sys.INotifyDisposing);

function Sys$Component$_setProperties(a, i) {
    var d, j = Object.getType(a),
        e = j === Object || j === Sys.UI.DomElement,
        h = Sys.Component.isInstanceOfType(a) && !a.get_isUpdating();
    if (h) a.beginUpdate();
    for (var c in i) {
        var b = i[c],
            f = e ? null : a["get_" + c];
        if (e || typeof f !== "function") {
            var k = a[c];
            if (!b || typeof b !== "object" || e && !k) a[c] = b;
            else Sys$Component$_setProperties(k, b)
        } else {
            var l = a["set_" + c];
            if (typeof l === "function") l.apply(a, [b]);
            else if (b instanceof Array) {
                d = f.apply(a);
                for (var g = 0, m = d.length, n = b.length; g < n; g++, m++) d[m] = b[g]
            } else if (typeof b === "object" && Object.getType(b) === Object) {
                d = f.apply(a);
                Sys$Component$_setProperties(d, b)
            }
        }
    }
    if (h) a.endUpdate()
}

function Sys$Component$_setReferences(c, b) {
    for (var a in b) {
        var e = c["set_" + a],
            d = $find(b[a]);
        e.apply(c, [d])
    }
}
var $create = Sys.Component.create = function(h, f, d, c, g) {
    var a = g ? new h(g) : new h,
        b = Sys.Application,
        i = b.get_isCreatingComponents();
    a.beginUpdate();
    if (f) Sys$Component$_setProperties(a, f);
    if (d)
        for (var e in d) a["add_" + e](d[e]);
    if (a.get_id()) b.addComponent(a);
    if (i) {
        b._createdComponents[b._createdComponents.length] = a;
        if (c) b._addComponentToSecondPass(a, c);
        else a.endUpdate()
    } else {
        if (c) Sys$Component$_setReferences(a, c);
        a.endUpdate()
    }
    return a
};
Sys.UI.MouseButton = function() {
    throw Error.notImplemented()
};
Sys.UI.MouseButton.prototype = {
    leftButton: 0,
    middleButton: 1,
    rightButton: 2
};
Sys.UI.MouseButton.registerEnum("Sys.UI.MouseButton");
Sys.UI.Key = function() {
    throw Error.notImplemented()
};
Sys.UI.Key.prototype = {
    backspace: 8,
    tab: 9,
    enter: 13,
    esc: 27,
    space: 32,
    pageUp: 33,
    pageDown: 34,
    end: 35,
    home: 36,
    left: 37,
    up: 38,
    right: 39,
    down: 40,
    del: 127
};
Sys.UI.Key.registerEnum("Sys.UI.Key");
Sys.UI.Point = function(a, b) {
    this.x = a;
    this.y = b
};
Sys.UI.Point.registerClass("Sys.UI.Point");
Sys.UI.Bounds = function(c, d, b, a) {
    this.x = c;
    this.y = d;
    this.height = a;
    this.width = b
};
Sys.UI.Bounds.registerClass("Sys.UI.Bounds");
Sys.UI.DomEvent = function(e) {
    var a = e,
        b = this.type = a.type.toLowerCase();
    this.rawEvent = a;
    this.altKey = a.altKey;
    if (typeof a.button !== "undefined") this.button = typeof a.which !== "undefined" ? a.button : a.button === 4 ? Sys.UI.MouseButton.middleButton : a.button === 2 ? Sys.UI.MouseButton.rightButton : Sys.UI.MouseButton.leftButton;
    if (b === "keypress") this.charCode = a.charCode || a.keyCode;
    else if (a.keyCode && a.keyCode === 46) this.keyCode = 127;
    else this.keyCode = a.keyCode;
    this.clientX = a.clientX;
    this.clientY = a.clientY;
    this.ctrlKey = a.ctrlKey;
    this.target = a.target ? a.target : a.srcElement;
    if (!b.startsWith("key"))
        if (typeof a.offsetX !== "undefined" && typeof a.offsetY !== "undefined") {
            this.offsetX = a.offsetX;
            this.offsetY = a.offsetY
        } else if (this.target && this.target.nodeType !== 3 && typeof a.clientX === "number") {
            var c = Sys.UI.DomElement.getLocation(this.target),
                d = Sys.UI.DomElement._getWindow(this.target);
            this.offsetX = (d.pageXOffset || 0) + a.clientX - c.x;
            this.offsetY = (d.pageYOffset || 0) + a.clientY - c.y
        }
    this.screenX = a.screenX;
    this.screenY = a.screenY;
    this.shiftKey = a.shiftKey
};
Sys.UI.DomEvent.prototype = {
    preventDefault: function() {
        if (this.rawEvent.preventDefault) this.rawEvent.preventDefault();
        else if (window.event) this.rawEvent.returnValue = false
    },
    stopPropagation: function() {
        if (this.rawEvent.stopPropagation) this.rawEvent.stopPropagation();
        else if (window.event) this.rawEvent.cancelBubble = true
    }
};
Sys.UI.DomEvent.registerClass("Sys.UI.DomEvent");
var $addHandler = Sys.UI.DomEvent.addHandler = function(a, d, e, g) {
        if (!a._events) a._events = {};
        var c = a._events[d];
        if (!c) a._events[d] = c = [];
        var b;
        if (a.addEventListener) {
            b = function(b) {
                return e.call(a, new Sys.UI.DomEvent(b))
            };
            a.addEventListener(d, b, false)
        } else if (a.attachEvent) {
            b = function() {
                var b = {};
                try {
                    b = Sys.UI.DomElement._getWindow(a).event
                } catch (c) {}
                return e.call(a, new Sys.UI.DomEvent(b))
            };
            a.attachEvent("on" + d, b)
        }
        c[c.length] = {
            handler: e,
            browserHandler: b,
            autoRemove: g
        };
        if (g) {
            var f = a.dispose;
            if (f !== Sys.UI.DomEvent._disposeHandlers) {
                a.dispose = Sys.UI.DomEvent._disposeHandlers;
                if (typeof f !== "undefined") a._chainDispose = f
            }
        }
    },
    $addHandlers = Sys.UI.DomEvent.addHandlers = function(f, d, c, e) {
        for (var b in d) {
            var a = d[b];
            if (c) a = Function.createDelegate(c, a);
            $addHandler(f, b, a, e || false)
        }
    },
    $clearHandlers = Sys.UI.DomEvent.clearHandlers = function(a) {
        Sys.UI.DomEvent._clearHandlers(a, false)
    };
Sys.UI.DomEvent._clearHandlers = function(a, g) {
    if (a._events) {
        var e = a._events;
        for (var b in e) {
            var d = e[b];
            for (var c = d.length - 1; c >= 0; c--) {
                var f = d[c];
                if (!g || f.autoRemove) $removeHandler(a, b, f.handler)
            }
        }
        a._events = null
    }
};
Sys.UI.DomEvent._disposeHandlers = function() {
    Sys.UI.DomEvent._clearHandlers(this, true);
    var b = this._chainDispose,
        a = typeof b;
    if (a !== "undefined") {
        this.dispose = b;
        this._chainDispose = null;
        if (a === "function") this.dispose()
    }
};
var $removeHandler = Sys.UI.DomEvent.removeHandler = function(b, a, c) {
    Sys.UI.DomEvent._removeHandler(b, a, c)
};
Sys.UI.DomEvent._removeHandler = function(a, e, f) {
    var d = null,
        c = a._events[e];
    for (var b = 0, g = c.length; b < g; b++)
        if (c[b].handler === f) {
            d = c[b].browserHandler;
            break
        }
    if (a.removeEventListener) a.removeEventListener(e, d, false);
    else if (a.detachEvent) a.detachEvent("on" + e, d);
    c.splice(b, 1)
};
Sys.UI.DomElement = function() {};
Sys.UI.DomElement.registerClass("Sys.UI.DomElement");
Sys.UI.DomElement.addCssClass = function(a, b) {
    if (!Sys.UI.DomElement.containsCssClass(a, b))
        if (a.className === "") a.className = b;
        else a.className += " " + b
};
Sys.UI.DomElement.containsCssClass = function(b, a) {
    return Array.contains(b.className.split(" "), a)
};
Sys.UI.DomElement.getBounds = function(a) {
    var b = Sys.UI.DomElement.getLocation(a);
    return new Sys.UI.Bounds(b.x, b.y, a.offsetWidth || 0, a.offsetHeight || 0)
};
var $get = Sys.UI.DomElement.getElementById = function(f, e) {
    if (!e) return document.getElementById(f);
    if (e.getElementById) return e.getElementById(f);
    var c = [],
        d = e.childNodes;
    for (var b = 0; b < d.length; b++) {
        var a = d[b];
        if (a.nodeType == 1) c[c.length] = a
    }
    while (c.length) {
        a = c.shift();
        if (a.id == f) return a;
        d = a.childNodes;
        for (b = 0; b < d.length; b++) {
            a = d[b];
            if (a.nodeType == 1) c[c.length] = a
        }
    }
    return null
};
if (document.documentElement.getBoundingClientRect) Sys.UI.DomElement.getLocation = function(b) {
    if (b.self || b.nodeType === 9) return new Sys.UI.Point(0, 0);
    var f = b.getBoundingClientRect();
    if (!f) return new Sys.UI.Point(0, 0);
    var i = b.ownerDocument.documentElement,
        c = Math.floor(f.left + .5) + i.scrollLeft,
        d = Math.floor(f.top + .5) + i.scrollTop;
    if (Sys.Browser.agent === Sys.Browser.InternetExplorer) {
        try {
            var h = b.ownerDocument.parentWindow.frameElement || null;
            if (h) {
                var k = h.frameBorder === "0" || h.frameBorder === "no" ? 2 : 0;
                c += k;
                d += k
            }
        } catch (l) {}
        if (Sys.Browser.version <= 7) {
            var a, j, g, e = document.createElement("div");
            e.style.cssText = "position:absolute !important;left:0px !important;right:0px !important;height:0px !important;width:1px !important;display:hidden !important";
            try {
                j = document.body.childNodes[0];
                document.body.insertBefore(e, j);
                g = e.getBoundingClientRect();
                document.body.removeChild(e);
                a = g.right - g.left
            } catch (l) {}
            if (a && a !== 1) {
                c = Math.floor(c / a);
                d = Math.floor(d / a)
            }
        }
        if ((document.documentMode || 0) < 8) {
            c -= 2;
            d -= 2
        }
    }
    return new Sys.UI.Point(c, d)
};
else if (Sys.Browser.agent === Sys.Browser.Safari) Sys.UI.DomElement.getLocation = function(c) {
    if (c.window && c.window === c || c.nodeType === 9) return new Sys.UI.Point(0, 0);
    var d = 0,
        e = 0,
        a, j = null,
        g = null,
        b;
    for (a = c; a; j = a, (g = b, a = a.offsetParent)) {
        b = Sys.UI.DomElement._getCurrentStyle(a);
        var f = a.tagName ? a.tagName.toUpperCase() : null;
        if ((a.offsetLeft || a.offsetTop) && (f !== "BODY" || (!g || g.position !== "absolute"))) {
            d += a.offsetLeft;
            e += a.offsetTop
        }
        if (j && Sys.Browser.version >= 3) {
            d += parseInt(b.borderLeftWidth);
            e += parseInt(b.borderTopWidth)
        }
    }
    b = Sys.UI.DomElement._getCurrentStyle(c);
    var h = b ? b.position : null;
    if (!h || h !== "absolute")
        for (a = c.parentNode; a; a = a.parentNode) {
            f = a.tagName ? a.tagName.toUpperCase() : null;
            if (f !== "BODY" && f !== "HTML" && (a.scrollLeft || a.scrollTop)) {
                d -= a.scrollLeft || 0;
                e -= a.scrollTop || 0
            }
            b = Sys.UI.DomElement._getCurrentStyle(a);
            var i = b ? b.position : null;
            if (i && i === "absolute") break
        }
    return new Sys.UI.Point(d, e)
};
else Sys.UI.DomElement.getLocation = function(d) {
        if (d.window && d.window === d || d.nodeType === 9) return new Sys.UI.Point(0, 0);
        var e = 0,
            f = 0,
            a, i = null,
            g = null,
            b = null;
        for (a = d; a; i = a, (g = b, a = a.offsetParent)) {
            var c = a.tagName ? a.tagName.toUpperCase() : null;
            b = Sys.UI.DomElement._getCurrentStyle(a);
            if ((a.offsetLeft || a.offsetTop) && !(c === "BODY" && (!g || g.position !== "absolute"))) {
                e += a.offsetLeft;
                f += a.offsetTop
            }
            if (i !== null && b) {
                if (c !== "TABLE" && c !== "TD" && c !== "HTML") {
                    e += parseInt(b.borderLeftWidth) || 0;
                    f += parseInt(b.borderTopWidth) || 0
                }
                if (c === "TABLE" && (b.position === "relative" || b.position === "absolute")) {
                    e += parseInt(b.marginLeft) || 0;
                    f += parseInt(b.marginTop) || 0
                }
            }
        }
        b = Sys.UI.DomElement._getCurrentStyle(d);
        var h = b ? b.position : null;
        if (!h || h !== "absolute")
            for (a = d.parentNode; a; a = a.parentNode) {
                c = a.tagName ? a.tagName.toUpperCase() : null;
                if (c !== "BODY" && c !== "HTML" && (a.scrollLeft || a.scrollTop)) {
                    e -= a.scrollLeft || 0;
                    f -= a.scrollTop || 0;
                    b = Sys.UI.DomElement._getCurrentStyle(a);
                    if (b) {
                        e += parseInt(b.borderLeftWidth) || 0;
                        f += parseInt(b.borderTopWidth) || 0
                    }
                }
            }
        return new Sys.UI.Point(e, f)
    };
Sys.UI.DomElement.isDomElement = function(a) {
    return Sys._isDomElement(a)
};
Sys.UI.DomElement.removeCssClass = function(d, c) {
    var a = " " + d.className + " ",
        b = a.indexOf(" " + c + " ");
    if (b >= 0) d.className = (a.substr(0, b) + " " + a.substring(b + c.length + 1, a.length)).trim()
};
Sys.UI.DomElement.resolveElement = function(b, c) {
    var a = b;
    if (!a) return null;
    if (typeof a === "string") a = Sys.UI.DomElement.getElementById(a, c);
    return a
};
Sys.UI.DomElement.raiseBubbleEvent = function(c, d) {
    var b = c;
    while (b) {
        var a = b.control;
        if (a && a.onBubbleEvent && a.raiseBubbleEvent) {
            Sys.UI.DomElement._raiseBubbleEventFromControl(a, c, d);
            return
        }
        b = b.parentNode
    }
};
Sys.UI.DomElement._raiseBubbleEventFromControl = function(a, b, c) {
    if (!a.onBubbleEvent(b, c)) a._raiseBubbleEvent(b, c)
};
Sys.UI.DomElement.setLocation = function(b, c, d) {
    var a = b.style;
    a.position = "absolute";
    a.left = c + "px";
    a.top = d + "px"
};
Sys.UI.DomElement.toggleCssClass = function(b, a) {
    if (Sys.UI.DomElement.containsCssClass(b, a)) Sys.UI.DomElement.removeCssClass(b, a);
    else Sys.UI.DomElement.addCssClass(b, a)
};
Sys.UI.DomElement.getVisibilityMode = function(a) {
    return a._visibilityMode === Sys.UI.VisibilityMode.hide ? Sys.UI.VisibilityMode.hide : Sys.UI.VisibilityMode.collapse
};
Sys.UI.DomElement.setVisibilityMode = function(a, b) {
    Sys.UI.DomElement._ensureOldDisplayMode(a);
    if (a._visibilityMode !== b) {
        a._visibilityMode = b;
        if (Sys.UI.DomElement.getVisible(a) === false)
            if (a._visibilityMode === Sys.UI.VisibilityMode.hide) a.style.display = a._oldDisplayMode;
            else a.style.display = "none";
        a._visibilityMode = b
    }
};
Sys.UI.DomElement.getVisible = function(b) {
    var a = b.currentStyle || Sys.UI.DomElement._getCurrentStyle(b);
    if (!a) return true;
    return a.visibility !== "hidden" && a.display !== "none"
};
Sys.UI.DomElement.setVisible = function(a, b) {
    if (b !== Sys.UI.DomElement.getVisible(a)) {
        Sys.UI.DomElement._ensureOldDisplayMode(a);
        a.style.visibility = b ? "visible" : "hidden";
        if (b || a._visibilityMode === Sys.UI.VisibilityMode.hide) a.style.display = a._oldDisplayMode;
        else a.style.display = "none"
    }
};
Sys.UI.DomElement._ensureOldDisplayMode = function(a) {
    if (!a._oldDisplayMode) {
        var b = a.currentStyle || Sys.UI.DomElement._getCurrentStyle(a);
        a._oldDisplayMode = b ? b.display : null;
        if (!a._oldDisplayMode || a._oldDisplayMode === "none") switch (a.tagName.toUpperCase()) {
            case "DIV":
            case "P":
            case "ADDRESS":
            case "BLOCKQUOTE":
            case "BODY":
            case "COL":
            case "COLGROUP":
            case "DD":
            case "DL":
            case "DT":
            case "FIELDSET":
            case "FORM":
            case "H1":
            case "H2":
            case "H3":
            case "H4":
            case "H5":
            case "H6":
            case "HR":
            case "IFRAME":
            case "LEGEND":
            case "OL":
            case "PRE":
            case "TABLE":
            case "TD":
            case "TH":
            case "TR":
            case "UL":
                a._oldDisplayMode = "block";
                break;
            case "LI":
                a._oldDisplayMode = "list-item";
                break;
            default:
                a._oldDisplayMode = "inline"
        }
    }
};
Sys.UI.DomElement._getWindow = function(a) {
    var b = a.ownerDocument || a.document || a;
    return b.defaultView || b.parentWindow
};
Sys.UI.DomElement._getCurrentStyle = function(a) {
    if (a.nodeType === 3) return null;
    var c = Sys.UI.DomElement._getWindow(a);
    if (a.documentElement) a = a.documentElement;
    var b = c && a !== c && c.getComputedStyle ? c.getComputedStyle(a, null) : a.currentStyle || a.style;
    if (!b && Sys.Browser.agent === Sys.Browser.Safari && a.style) {
        var g = a.style.display,
            f = a.style.position;
        a.style.position = "absolute";
        a.style.display = "block";
        var e = c.getComputedStyle(a, null);
        a.style.display = g;
        a.style.position = f;
        b = {};
        for (var d in e) b[d] = e[d];
        b.display = "none"
    }
    return b
};
Sys.IContainer = function() {};
Sys.IContainer.prototype = {};
Sys.IContainer.registerInterface("Sys.IContainer");
Sys.ApplicationLoadEventArgs = function(b, a) {
    Sys.ApplicationLoadEventArgs.initializeBase(this);
    this._components = b;
    this._isPartialLoad = a
};
Sys.ApplicationLoadEventArgs.prototype = {
    get_components: function() {
        return this._components
    },
    get_isPartialLoad: function() {
        return this._isPartialLoad
    }
};
Sys.ApplicationLoadEventArgs.registerClass("Sys.ApplicationLoadEventArgs", Sys.EventArgs);
Sys._Application = function() {
    Sys._Application.initializeBase(this);
    this._disposableObjects = [];
    this._components = {};
    this._createdComponents = [];
    this._secondPassComponents = [];
    this._unloadHandlerDelegate = Function.createDelegate(this, this._unloadHandler);
    Sys.UI.DomEvent.addHandler(window, "unload", this._unloadHandlerDelegate);
    this._domReady()
};
Sys._Application.prototype = {
    _creatingComponents: false,
    _disposing: false,
    _deleteCount: 0,
    get_isCreatingComponents: function() {
        return this._creatingComponents
    },
    get_isDisposing: function() {
        return this._disposing
    },
    add_init: function(a) {
        if (this._initialized) a(this, Sys.EventArgs.Empty);
        else this.get_events().addHandler("init", a)
    },
    remove_init: function(a) {
        this.get_events().removeHandler("init", a)
    },
    add_load: function(a) {
        this.get_events().addHandler("load", a)
    },
    remove_load: function(a) {
        this.get_events().removeHandler("load", a)
    },
    add_unload: function(a) {
        this.get_events().addHandler("unload", a)
    },
    remove_unload: function(a) {
        this.get_events().removeHandler("unload", a)
    },
    addComponent: function(a) {
        this._components[a.get_id()] = a
    },
    beginCreateComponents: function() {
        this._creatingComponents = true
    },
    dispose: function() {
        if (!this._disposing) {
            this._disposing = true;
            if (this._timerCookie) {
                window.clearTimeout(this._timerCookie);
                delete this._timerCookie
            }
            if (this._endRequestHandler) {
                Sys.WebForms.PageRequestManager.getInstance().remove_endRequest(this._endRequestHandler);
                delete this._endRequestHandler
            }
            if (this._beginRequestHandler) {
                Sys.WebForms.PageRequestManager.getInstance().remove_beginRequest(this._beginRequestHandler);
                delete this._beginRequestHandler
            }
            if (window.pageUnload) window.pageUnload(this, Sys.EventArgs.Empty);
            var c = this.get_events().getHandler("unload");
            if (c) c(this, Sys.EventArgs.Empty);
            var b = Array.clone(this._disposableObjects);
            for (var a = 0, f = b.length; a < f; a++) {
                var d = b[a];
                if (typeof d !== "undefined") d.dispose()
            }
            Array.clear(this._disposableObjects);
            Sys.UI.DomEvent.removeHandler(window, "unload", this._unloadHandlerDelegate);
            if (Sys._ScriptLoader) {
                var e = Sys._ScriptLoader.getInstance();
                if (e) e.dispose()
            }
            Sys._Application.callBaseMethod(this, "dispose")
        }
    },
    disposeElement: function(a, d) {
        if (a.nodeType === 1) {
            var c = a.getElementsByTagName("*");
            for (var b = c.length - 1; b >= 0; b--) this._disposeElementInternal(c[b]);
            if (!d) this._disposeElementInternal(a)
        }
    },
    endCreateComponents: function() {
        var b = this._secondPassComponents;
        for (var a = 0, d = b.length; a < d; a++) {
            var c = b[a].component;
            Sys$Component$_setReferences(c, b[a].references);
            c.endUpdate()
        }
        this._secondPassComponents = [];
        this._creatingComponents = false
    },
    findComponent: function(b, a) {
        return a ? Sys.IContainer.isInstanceOfType(a) ? a.findComponent(b) : a[b] || null : Sys.Application._components[b] || null
    },
    getComponents: function() {
        var a = [],
            b = this._components;
        for (var c in b) a[a.length] = b[c];
        return a
    },
    initialize: function() {
        if (!this.get_isInitialized() && !this._disposing) {
            Sys._Application.callBaseMethod(this, "initialize");
            this._raiseInit();
            if (this.get_stateString) {
                if (Sys.WebForms && Sys.WebForms.PageRequestManager) {
                    this._beginRequestHandler = Function.createDelegate(this, this._onPageRequestManagerBeginRequest);
                    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(this._beginRequestHandler);
                    this._endRequestHandler = Function.createDelegate(this, this._onPageRequestManagerEndRequest);
                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(this._endRequestHandler)
                }
                var a = this.get_stateString();
                if (a !== this._currentEntry) this._navigate(a);
                else this._ensureHistory()
            }
            this.raiseLoad()
        }
    },
    notifyScriptLoaded: function() {},
    registerDisposableObject: function(b) {
        if (!this._disposing) {
            var a = this._disposableObjects,
                c = a.length;
            a[c] = b;
            b.__msdisposeindex = c
        }
    },
    raiseLoad: function() {
        var b = this.get_events().getHandler("load"),
            a = new Sys.ApplicationLoadEventArgs(Array.clone(this._createdComponents), !!this._loaded);
        this._loaded = true;
        if (b) b(this, a);
        if (window.pageLoad) window.pageLoad(this, a);
        this._createdComponents = []
    },
    removeComponent: function(b) {
        var a = b.get_id();
        if (a) delete this._components[a]
    },
    unregisterDisposableObject: function(a) {
        if (!this._disposing) {
            var e = a.__msdisposeindex;
            if (typeof e === "number") {
                var b = this._disposableObjects;
                delete b[e];
                delete a.__msdisposeindex;
                if (++this._deleteCount > 1000) {
                    var c = [];
                    for (var d = 0, f = b.length; d < f; d++) {
                        a = b[d];
                        if (typeof a !== "undefined") {
                            a.__msdisposeindex = c.length;
                            c.push(a)
                        }
                    }
                    this._disposableObjects = c;
                    this._deleteCount = 0
                }
            }
        }
    },
    _addComponentToSecondPass: function(b, a) {
        this._secondPassComponents[this._secondPassComponents.length] = {
            component: b,
            references: a
        }
    },
    _disposeComponents: function(a) {
        if (a)
            for (var b = a.length - 1; b >= 0; b--) {
                var c = a[b];
                if (typeof c.dispose === "function") c.dispose()
            }
    },
    _disposeElementInternal: function(a) {
        var d = a.dispose;
        if (d && typeof d === "function") a.dispose();
        else {
            var c = a.control;
            if (c && typeof c.dispose === "function") c.dispose()
        }
        var b = a._behaviors;
        if (b) this._disposeComponents(b);
        b = a._components;
        if (b) {
            this._disposeComponents(b);
            a._components = null
        }
    },
    _domReady: function() {
        var a, g, f = this;

        function b() {
            f.initialize()
        }
        var c = function() {
            Sys.UI.DomEvent.removeHandler(window, "load", c);
            b()
        };
        Sys.UI.DomEvent.addHandler(window, "load", c);
        if (document.addEventListener) try {
            document.addEventListener("DOMContentLoaded", a = function() {
                document.removeEventListener("DOMContentLoaded", a, false);
                b()
            }, false)
        } catch (h) {} else if (document.attachEvent)
            if (window == window.top && document.documentElement.doScroll) {
                var e, d = document.createElement("div");
                a = function() {
                    try {
                        d.doScroll("left")
                    } catch (c) {
                        e = window.setTimeout(a, 0);
                        return
                    }
                    d = null;
                    b()
                };
                a()
            } else document.attachEvent("onreadystatechange", a = function() {
                if (document.readyState === "complete") {
                    document.detachEvent("onreadystatechange", a);
                    b()
                }
            })
    },
    _raiseInit: function() {
        var a = this.get_events().getHandler("init");
        if (a) {
            this.beginCreateComponents();
            a(this, Sys.EventArgs.Empty);
            this.endCreateComponents()
        }
    },
    _unloadHandler: function() {
        this.dispose()
    }
};
Sys._Application.registerClass("Sys._Application", Sys.Component, Sys.IContainer);
Sys.Application = new Sys._Application;
var $find = Sys.Application.findComponent;
Sys.UI.Behavior = function(b) {
    Sys.UI.Behavior.initializeBase(this);
    this._element = b;
    var a = b._behaviors;
    if (!a) b._behaviors = [this];
    else a[a.length] = this
};
Sys.UI.Behavior.prototype = {
    _name: null,
    get_element: function() {
        return this._element
    },
    get_id: function() {
        var a = Sys.UI.Behavior.callBaseMethod(this, "get_id");
        if (a) return a;
        if (!this._element || !this._element.id) return "";
        return this._element.id + "$" + this.get_name()
    },
    get_name: function() {
        if (this._name) return this._name;
        var a = Object.getTypeName(this),
            b = a.lastIndexOf(".");
        if (b !== -1) a = a.substr(b + 1);
        if (!this.get_isInitialized()) this._name = a;
        return a
    },
    set_name: function(a) {
        this._name = a
    },
    initialize: function() {
        Sys.UI.Behavior.callBaseMethod(this, "initialize");
        var a = this.get_name();
        if (a) this._element[a] = this
    },
    dispose: function() {
        Sys.UI.Behavior.callBaseMethod(this, "dispose");
        var a = this._element;
        if (a) {
            var c = this.get_name();
            if (c) a[c] = null;
            var b = a._behaviors;
            Array.remove(b, this);
            if (b.length === 0) a._behaviors = null;
            delete this._element
        }
    }
};
Sys.UI.Behavior.registerClass("Sys.UI.Behavior", Sys.Component);
Sys.UI.Behavior.getBehaviorByName = function(b, c) {
    var a = b[c];
    return a && Sys.UI.Behavior.isInstanceOfType(a) ? a : null
};
Sys.UI.Behavior.getBehaviors = function(a) {
    if (!a._behaviors) return [];
    return Array.clone(a._behaviors)
};
Sys.UI.Behavior.getBehaviorsByType = function(d, e) {
    var a = d._behaviors,
        c = [];
    if (a)
        for (var b = 0, f = a.length; b < f; b++)
            if (e.isInstanceOfType(a[b])) c[c.length] = a[b];
    return c
};
Sys.UI.VisibilityMode = function() {
    throw Error.notImplemented()
};
Sys.UI.VisibilityMode.prototype = {
    hide: 0,
    collapse: 1
};
Sys.UI.VisibilityMode.registerEnum("Sys.UI.VisibilityMode");
Sys.UI.Control = function(a) {
    Sys.UI.Control.initializeBase(this);
    this._element = a;
    a.control = this;
    var b = this.get_role();
    if (b) a.setAttribute("role", b)
};
Sys.UI.Control.prototype = {
    _parent: null,
    _visibilityMode: Sys.UI.VisibilityMode.hide,
    get_element: function() {
        return this._element
    },
    get_id: function() {
        if (!this._element) return "";
        return this._element.id
    },
    set_id: function() {
        throw Error.invalidOperation(Sys.Res.cantSetId)
    },
    get_parent: function() {
        if (this._parent) return this._parent;
        if (!this._element) return null;
        var a = this._element.parentNode;
        while (a) {
            if (a.control) return a.control;
            a = a.parentNode
        }
        return null
    },
    set_parent: function(a) {
        this._parent = a
    },
    get_role: function() {
        return null
    },
    get_visibilityMode: function() {
        return Sys.UI.DomElement.getVisibilityMode(this._element)
    },
    set_visibilityMode: function(a) {
        Sys.UI.DomElement.setVisibilityMode(this._element, a)
    },
    get_visible: function() {
        return Sys.UI.DomElement.getVisible(this._element)
    },
    set_visible: function(a) {
        Sys.UI.DomElement.setVisible(this._element, a)
    },
    addCssClass: function(a) {
        Sys.UI.DomElement.addCssClass(this._element, a)
    },
    dispose: function() {
        Sys.UI.Control.callBaseMethod(this, "dispose");
        if (this._element) {
            this._element.control = null;
            delete this._element
        }
        if (this._parent) delete this._parent
    },
    onBubbleEvent: function() {
        return false
    },
    raiseBubbleEvent: function(a, b) {
        this._raiseBubbleEvent(a, b)
    },
    _raiseBubbleEvent: function(b, c) {
        var a = this.get_parent();
        while (a) {
            if (a.onBubbleEvent(b, c)) return;
            a = a.get_parent()
        }
    },
    removeCssClass: function(a) {
        Sys.UI.DomElement.removeCssClass(this._element, a)
    },
    toggleCssClass: function(a) {
        Sys.UI.DomElement.toggleCssClass(this._element, a)
    }
};
Sys.UI.Control.registerClass("Sys.UI.Control", Sys.Component);
Sys.HistoryEventArgs = function(a) {
    Sys.HistoryEventArgs.initializeBase(this);
    this._state = a
};
Sys.HistoryEventArgs.prototype = {
    get_state: function() {
        return this._state
    }
};
Sys.HistoryEventArgs.registerClass("Sys.HistoryEventArgs", Sys.EventArgs);
Sys.Application._appLoadHandler = null;
Sys.Application._beginRequestHandler = null;
Sys.Application._clientId = null;
Sys.Application._currentEntry = "";
Sys.Application._endRequestHandler = null;
Sys.Application._history = null;
Sys.Application._enableHistory = false;
Sys.Application._historyFrame = null;
Sys.Application._historyInitialized = false;
Sys.Application._historyPointIsNew = false;
Sys.Application._ignoreTimer = false;
Sys.Application._initialState = null;
Sys.Application._state = {};
Sys.Application._timerCookie = 0;
Sys.Application._timerHandler = null;
Sys.Application._uniqueId = null;
Sys._Application.prototype.get_stateString = function() {
    var a = null;
    if (Sys.Browser.agent === Sys.Browser.Firefox) {
        var c = window.location.href,
            b = c.indexOf("#");
        if (b !== -1) a = c.substring(b + 1);
        else a = "";
        return a
    } else a = window.location.hash;
    if (a.length > 0 && a.charAt(0) === "#") a = a.substring(1);
    return a
};
Sys._Application.prototype.get_enableHistory = function() {
    return this._enableHistory
};
Sys._Application.prototype.set_enableHistory = function(a) {
    this._enableHistory = a
};
Sys._Application.prototype.add_navigate = function(a) {
    this.get_events().addHandler("navigate", a)
};
Sys._Application.prototype.remove_navigate = function(a) {
    this.get_events().removeHandler("navigate", a)
};
Sys._Application.prototype.addHistoryPoint = function(c, f) {
    this._ensureHistory();
    var b = this._state;
    for (var a in c) {
        var d = c[a];
        if (d === null) {
            if (typeof b[a] !== "undefined") delete b[a]
        } else b[a] = d
    }
    var e = this._serializeState(b);
    this._historyPointIsNew = true;
    this._setState(e, f);
    this._raiseNavigate()
};
Sys._Application.prototype.setServerId = function(a, b) {
    this._clientId = a;
    this._uniqueId = b
};
Sys._Application.prototype.setServerState = function(a) {
    this._ensureHistory();
    this._state.__s = a;
    this._updateHiddenField(a)
};
Sys._Application.prototype._deserializeState = function(a) {
    var e = {};
    a = a || "";
    var b = a.indexOf("&&");
    if (b !== -1 && b + 2 < a.length) {
        e.__s = a.substr(b + 2);
        a = a.substr(0, b)
    }
    var g = a.split("&");
    for (var f = 0, j = g.length; f < j; f++) {
        var d = g[f],
            c = d.indexOf("=");
        if (c !== -1 && c + 1 < d.length) {
            var i = d.substr(0, c),
                h = d.substr(c + 1);
            e[i] = decodeURIComponent(h)
        }
    }
    return e
};
Sys._Application.prototype._enableHistoryInScriptManager = function() {
    this._enableHistory = true
};
Sys._Application.prototype._ensureHistory = function() {
    if (!this._historyInitialized && this._enableHistory) {
        if (Sys.Browser.agent === Sys.Browser.InternetExplorer && Sys.Browser.documentMode < 8) {
            this._historyFrame = document.getElementById("__historyFrame");
            this._ignoreIFrame = true
        }
        this._timerHandler = Function.createDelegate(this, this._onIdle);
        this._timerCookie = window.setTimeout(this._timerHandler, 100);
        try {
            this._initialState = this._deserializeState(this.get_stateString())
        } catch (a) {}
        this._historyInitialized = true
    }
};
Sys._Application.prototype._navigate = function(c) {
    this._ensureHistory();
    var b = this._deserializeState(c);
    if (this._uniqueId) {
        var d = this._state.__s || "",
            a = b.__s || "";
        if (a !== d) {
            this._updateHiddenField(a);
            __doPostBack(this._uniqueId, a);
            this._state = b;
            return
        }
    }
    this._setState(c);
    this._state = b;
    this._raiseNavigate()
};
Sys._Application.prototype._onIdle = function() {
    delete this._timerCookie;
    var a = this.get_stateString();
    if (a !== this._currentEntry) {
        if (!this._ignoreTimer) {
            this._historyPointIsNew = false;
            this._navigate(a)
        }
    } else this._ignoreTimer = false;
    this._timerCookie = window.setTimeout(this._timerHandler, 100)
};
Sys._Application.prototype._onIFrameLoad = function(a) {
    this._ensureHistory();
    if (!this._ignoreIFrame) {
        this._historyPointIsNew = false;
        this._navigate(a)
    }
    this._ignoreIFrame = false
};
Sys._Application.prototype._onPageRequestManagerBeginRequest = function() {
    this._ignoreTimer = true
};
Sys._Application.prototype._onPageRequestManagerEndRequest = function(e, d) {
    var b = d.get_dataItems()[this._clientId],
        a = document.getElementById("__EVENTTARGET");
    if (a && a.value === this._uniqueId) a.value = "";
    if (typeof b !== "undefined") {
        this.setServerState(b);
        this._historyPointIsNew = true
    } else this._ignoreTimer = false;
    var c = this._serializeState(this._state);
    if (c !== this._currentEntry) {
        this._ignoreTimer = true;
        this._setState(c);
        this._raiseNavigate()
    }
};
Sys._Application.prototype._raiseNavigate = function() {
    var c = this.get_events().getHandler("navigate"),
        b = {};
    for (var a in this._state)
        if (a !== "__s") b[a] = this._state[a];
    var d = new Sys.HistoryEventArgs(b);
    if (c) c(this, d);
    var e;
    try {
        if (Sys.Browser.agent === Sys.Browser.Firefox && window.location.hash && (!window.frameElement || window.top.location.hash)) window.history.go(0)
    } catch (f) {}
};
Sys._Application.prototype._serializeState = function(d) {
    var b = [];
    for (var a in d) {
        var e = d[a];
        if (a === "__s") var c = e;
        else b[b.length] = a + "=" + encodeURIComponent(e)
    }
    return b.join("&") + (c ? "&&" + c : "")
};
Sys._Application.prototype._setState = function(a, b) {
    if (this._enableHistory) {
        a = a || "";
        if (a !== this._currentEntry) {
            if (window.theForm) {
                var d = window.theForm.action,
                    e = d.indexOf("#");
                window.theForm.action = (e !== -1 ? d.substring(0, e) : d) + "#" + a
            }
            if (this._historyFrame && this._historyPointIsNew) {
                this._ignoreIFrame = true;
                var c = this._historyFrame.contentWindow.document;
                c.open("javascript:'<html></html>'");
                c.write("<html><head><title>" + (b || document.title) + "</title><scri" + 'pt type="text/javascript">parent.Sys.Application._onIFrameLoad(' + Sys.Serialization.JavaScriptSerializer.serialize(a) + ");</scri" + "pt></head><body></body></html>");
                c.close()
            }
            this._ignoreTimer = false;
            this._currentEntry = a;
            if (this._historyFrame || this._historyPointIsNew) {
                var f = this.get_stateString();
                if (a !== f) {
                    window.location.hash = a;
                    this._currentEntry = this.get_stateString();
                    if (typeof b !== "undefined" && b !== null) document.title = b
                }
            }
            this._historyPointIsNew = false
        }
    }
};
Sys._Application.prototype._updateHiddenField = function(b) {
    if (this._clientId) {
        var a = document.getElementById(this._clientId);
        if (a) a.value = b
    }
};
if (!window.XMLHttpRequest) window.XMLHttpRequest = function() {
    var b = ["Msxml2.XMLHTTP.3.0", "Msxml2.XMLHTTP"];
    for (var a = 0, c = b.length; a < c; a++) try {
        return new ActiveXObject(b[a])
    } catch (d) {}
    return null
};
Type.registerNamespace("Sys.Net");
Sys.Net.WebRequestExecutor = function() {
    this._webRequest = null;
    this._resultObject = null
};
Sys.Net.WebRequestExecutor.prototype = {
    get_webRequest: function() {
        return this._webRequest
    },
    _set_webRequest: function(a) {
        this._webRequest = a
    },
    get_started: function() {
        throw Error.notImplemented()
    },
    get_responseAvailable: function() {
        throw Error.notImplemented()
    },
    get_timedOut: function() {
        throw Error.notImplemented()
    },
    get_aborted: function() {
        throw Error.notImplemented()
    },
    get_responseData: function() {
        throw Error.notImplemented()
    },
    get_statusCode: function() {
        throw Error.notImplemented()
    },
    get_statusText: function() {
        throw Error.notImplemented()
    },
    get_xml: function() {
        throw Error.notImplemented()
    },
    get_object: function() {
        if (!this._resultObject) this._resultObject = Sys.Serialization.JavaScriptSerializer.deserialize(this.get_responseData());
        return this._resultObject
    },
    executeRequest: function() {
        throw Error.notImplemented()
    },
    abort: function() {
        throw Error.notImplemented()
    },
    getResponseHeader: function() {
        throw Error.notImplemented()
    },
    getAllResponseHeaders: function() {
        throw Error.notImplemented()
    }
};
Sys.Net.WebRequestExecutor.registerClass("Sys.Net.WebRequestExecutor");
Sys.Net.XMLDOM = function(d) {
    if (!window.DOMParser) {
        var c = ["Msxml2.DOMDocument.3.0", "Msxml2.DOMDocument"];
        for (var b = 0, f = c.length; b < f; b++) try {
            var a = new ActiveXObject(c[b]);
            a.async = false;
            a.loadXML(d);
            a.setProperty("SelectionLanguage", "XPath");
            return a
        } catch (g) {}
    } else try {
        var e = new window.DOMParser;
        return e.parseFromString(d, "text/xml")
    } catch (g) {}
    return null
};
Sys.Net.XMLHttpExecutor = function() {
    Sys.Net.XMLHttpExecutor.initializeBase(this);
    var a = this;
    this._xmlHttpRequest = null;
    this._webRequest = null;
    this._responseAvailable = false;
    this._timedOut = false;
    this._timer = null;
    this._aborted = false;
    this._started = false;
    this._onReadyStateChange = function() {
        if (a._xmlHttpRequest.readyState === 4) {
            try {
                if (typeof a._xmlHttpRequest.status === "undefined") return
            } catch (b) {
                return
            }
            a._clearTimer();
            a._responseAvailable = true;
            try {
                a._webRequest.completed(Sys.EventArgs.Empty)
            } finally {
                if (a._xmlHttpRequest != null) {
                    a._xmlHttpRequest.onreadystatechange = Function.emptyMethod;
                    a._xmlHttpRequest = null
                }
            }
        }
    };
    this._clearTimer = function() {
        if (a._timer != null) {
            window.clearTimeout(a._timer);
            a._timer = null
        }
    };
    this._onTimeout = function() {
        if (!a._responseAvailable) {
            a._clearTimer();
            a._timedOut = true;
            a._xmlHttpRequest.onreadystatechange = Function.emptyMethod;
            a._xmlHttpRequest.abort();
            a._webRequest.completed(Sys.EventArgs.Empty);
            a._xmlHttpRequest = null
        }
    }
};
Sys.Net.XMLHttpExecutor.prototype = {
    get_timedOut: function() {
        return this._timedOut
    },
    get_started: function() {
        return this._started
    },
    get_responseAvailable: function() {
        return this._responseAvailable
    },
    get_aborted: function() {
        return this._aborted
    },
    executeRequest: function() {
        this._webRequest = this.get_webRequest();
        var c = this._webRequest.get_body(),
            a = this._webRequest.get_headers();
        this._xmlHttpRequest = new XMLHttpRequest;
        this._xmlHttpRequest.onreadystatechange = this._onReadyStateChange;
        var e = this._webRequest.get_httpVerb();
        this._xmlHttpRequest.open(e, this._webRequest.getResolvedUrl(), true);
        this._xmlHttpRequest.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        if (a)
            for (var b in a) {
                var f = a[b];
                if (typeof f !== "function") this._xmlHttpRequest.setRequestHeader(b, f)
            }
        if (e.toLowerCase() === "post") {
            if (a === null || !a["Content-Type"]) this._xmlHttpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
            if (!c) c = ""
        }
        var d = this._webRequest.get_timeout();
        if (d > 0) this._timer = window.setTimeout(Function.createDelegate(this, this._onTimeout), d);
        this._xmlHttpRequest.send(c);
        this._started = true
    },
    getResponseHeader: function(b) {
        var a;
        try {
            a = this._xmlHttpRequest.getResponseHeader(b)
        } catch (c) {}
        if (!a) a = "";
        return a
    },
    getAllResponseHeaders: function() {
        return this._xmlHttpRequest.getAllResponseHeaders()
    },
    get_responseData: function() {
        return this._xmlHttpRequest.responseText
    },
    get_statusCode: function() {
        var a = 0;
        try {
            a = this._xmlHttpRequest.status
        } catch (b) {}
        return a
    },
    get_statusText: function() {
        return this._xmlHttpRequest.statusText
    },
    get_xml: function() {
        var a = this._xmlHttpRequest.responseXML;
        if (!a || !a.documentElement) {
            a = Sys.Net.XMLDOM(this._xmlHttpRequest.responseText);
            if (!a || !a.documentElement) return null
        } else if (navigator.userAgent.indexOf("MSIE") !== -1) a.setProperty("SelectionLanguage", "XPath");
        if (a.documentElement.namespaceURI === "http://www.mozilla.org/newlayout/xml/parsererror.xml" && a.documentElement.tagName === "parsererror") return null;
        if (a.documentElement.firstChild && a.documentElement.firstChild.tagName === "parsererror") return null;
        return a
    },
    abort: function() {
        if (this._aborted || this._responseAvailable || this._timedOut) return;
        this._aborted = true;
        this._clearTimer();
        if (this._xmlHttpRequest && !this._responseAvailable) {
            this._xmlHttpRequest.onreadystatechange = Function.emptyMethod;
            this._xmlHttpRequest.abort();
            this._xmlHttpRequest = null;
            this._webRequest.completed(Sys.EventArgs.Empty)
        }
    }
};
Sys.Net.XMLHttpExecutor.registerClass("Sys.Net.XMLHttpExecutor", Sys.Net.WebRequestExecutor);
Sys.Net._WebRequestManager = function() {
    this._defaultTimeout = 0;
    this._defaultExecutorType = "Sys.Net.XMLHttpExecutor"
};
Sys.Net._WebRequestManager.prototype = {
    add_invokingRequest: function(a) {
        this._get_eventHandlerList().addHandler("invokingRequest", a)
    },
    remove_invokingRequest: function(a) {
        this._get_eventHandlerList().removeHandler("invokingRequest", a)
    },
    add_completedRequest: function(a) {
        this._get_eventHandlerList().addHandler("completedRequest", a)
    },
    remove_completedRequest: function(a) {
        this._get_eventHandlerList().removeHandler("completedRequest", a)
    },
    _get_eventHandlerList: function() {
        if (!this._events) this._events = new Sys.EventHandlerList;
        return this._events
    },
    get_defaultTimeout: function() {
        return this._defaultTimeout
    },
    set_defaultTimeout: function(a) {
        this._defaultTimeout = a
    },
    get_defaultExecutorType: function() {
        return this._defaultExecutorType
    },
    set_defaultExecutorType: function(a) {
        this._defaultExecutorType = a
    },
    executeRequest: function(webRequest) {
        var executor = webRequest.get_executor();
        if (!executor) {
            var failed = false;
            try {
                var executorType = eval(this._defaultExecutorType);
                executor = new executorType
            } catch (a) {
                failed = true
            }
            webRequest.set_executor(executor)
        }
        if (executor.get_aborted()) return;
        var evArgs = new Sys.Net.NetworkRequestEventArgs(webRequest),
            handler = this._get_eventHandlerList().getHandler("invokingRequest");
        if (handler) handler(this, evArgs);
        if (!evArgs.get_cancel()) executor.executeRequest()
    }
};
Sys.Net._WebRequestManager.registerClass("Sys.Net._WebRequestManager");
Sys.Net.WebRequestManager = new Sys.Net._WebRequestManager;
Sys.Net.NetworkRequestEventArgs = function(a) {
    Sys.Net.NetworkRequestEventArgs.initializeBase(this);
    this._webRequest = a
};
Sys.Net.NetworkRequestEventArgs.prototype = {
    get_webRequest: function() {
        return this._webRequest
    }
};
Sys.Net.NetworkRequestEventArgs.registerClass("Sys.Net.NetworkRequestEventArgs", Sys.CancelEventArgs);
Sys.Net.WebRequest = function() {
    this._url = "";
    this._headers = {};
    this._body = null;
    this._userContext = null;
    this._httpVerb = null;
    this._executor = null;
    this._invokeCalled = false;
    this._timeout = 0
};
Sys.Net.WebRequest.prototype = {
    add_completed: function(a) {
        this._get_eventHandlerList().addHandler("completed", a)
    },
    remove_completed: function(a) {
        this._get_eventHandlerList().removeHandler("completed", a)
    },
    completed: function(b) {
        var a = Sys.Net.WebRequestManager._get_eventHandlerList().getHandler("completedRequest");
        if (a) a(this._executor, b);
        a = this._get_eventHandlerList().getHandler("completed");
        if (a) a(this._executor, b)
    },
    _get_eventHandlerList: function() {
        if (!this._events) this._events = new Sys.EventHandlerList;
        return this._events
    },
    get_url: function() {
        return this._url
    },
    set_url: function(a) {
        this._url = a
    },
    get_headers: function() {
        return this._headers
    },
    get_httpVerb: function() {
        if (this._httpVerb === null) {
            if (this._body === null) return "GET";
            return "POST"
        }
        return this._httpVerb
    },
    set_httpVerb: function(a) {
        this._httpVerb = a
    },
    get_body: function() {
        return this._body
    },
    set_body: function(a) {
        this._body = a
    },
    get_userContext: function() {
        return this._userContext
    },
    set_userContext: function(a) {
        this._userContext = a
    },
    get_executor: function() {
        return this._executor
    },
    set_executor: function(a) {
        this._executor = a;
        this._executor._set_webRequest(this)
    },
    get_timeout: function() {
        if (this._timeout === 0) return Sys.Net.WebRequestManager.get_defaultTimeout();
        return this._timeout
    },
    set_timeout: function(a) {
        this._timeout = a
    },
    getResolvedUrl: function() {
        return Sys.Net.WebRequest._resolveUrl(this._url)
    },
    invoke: function() {
        Sys.Net.WebRequestManager.executeRequest(this);
        this._invokeCalled = true
    }
};
Sys.Net.WebRequest._resolveUrl = function(b, a) {
    if (b && b.indexOf("://") !== -1) return b;
    if (!a || a.length === 0) {
        var d = document.getElementsByTagName("base")[0];
        if (d && d.href && d.href.length > 0) a = d.href;
        else a = document.URL
    }
    var c = a.indexOf("?");
    if (c !== -1) a = a.substr(0, c);
    c = a.indexOf("#");
    if (c !== -1) a = a.substr(0, c);
    a = a.substr(0, a.lastIndexOf("/") + 1);
    if (!b || b.length === 0) return a;
    if (b.charAt(0) === "/") {
        var e = a.indexOf("://"),
            g = a.indexOf("/", e + 3);
        return a.substr(0, g) + b
    } else {
        var f = a.lastIndexOf("/");
        return a.substr(0, f + 1) + b
    }
};
Sys.Net.WebRequest._createQueryString = function(c, b, f) {
    b = b || encodeURIComponent;
    var h = 0,
        e, g, d, a = new Sys.StringBuilder;
    if (c)
        for (d in c) {
            e = c[d];
            if (typeof e === "function") continue;
            g = Sys.Serialization.JavaScriptSerializer.serialize(e);
            if (h++) a.append("&");
            a.append(d);
            a.append("=");
            a.append(b(g))
        }
    if (f) {
        if (h) a.append("&");
        a.append(f)
    }
    return a.toString()
};
Sys.Net.WebRequest._createUrl = function(a, b, c) {
    if (!b && !c) return a;
    var d = Sys.Net.WebRequest._createQueryString(b, null, c);
    return d.length ? a + (a && a.indexOf("?") >= 0 ? "&" : "?") + d : a
};
Sys.Net.WebRequest.registerClass("Sys.Net.WebRequest");
Sys._ScriptLoaderTask = function(b, a) {
    this._scriptElement = b;
    this._completedCallback = a
};
Sys._ScriptLoaderTask.prototype = {
    get_scriptElement: function() {
        return this._scriptElement
    },
    dispose: function() {
        if (this._disposed) return;
        this._disposed = true;
        this._removeScriptElementHandlers();
        Sys._ScriptLoaderTask._clearScript(this._scriptElement);
        this._scriptElement = null
    },
    execute: function() {
        this._addScriptElementHandlers();
        document.getElementsByTagName("head")[0].appendChild(this._scriptElement)
    },
    _addScriptElementHandlers: function() {
        this._scriptLoadDelegate = Function.createDelegate(this, this._scriptLoadHandler);
        if (Sys.Browser.agent !== Sys.Browser.InternetExplorer) {
            this._scriptElement.readyState = "loaded";
            $addHandler(this._scriptElement, "load", this._scriptLoadDelegate)
        } else $addHandler(this._scriptElement, "readystatechange", this._scriptLoadDelegate);
        if (this._scriptElement.addEventListener) {
            this._scriptErrorDelegate = Function.createDelegate(this, this._scriptErrorHandler);
            this._scriptElement.addEventListener("error", this._scriptErrorDelegate, false)
        }
    },
    _removeScriptElementHandlers: function() {
        if (this._scriptLoadDelegate) {
            var a = this.get_scriptElement();
            if (Sys.Browser.agent !== Sys.Browser.InternetExplorer) $removeHandler(a, "load", this._scriptLoadDelegate);
            else $removeHandler(a, "readystatechange", this._scriptLoadDelegate);
            if (this._scriptErrorDelegate) {
                this._scriptElement.removeEventListener("error", this._scriptErrorDelegate, false);
                this._scriptErrorDelegate = null
            }
            this._scriptLoadDelegate = null
        }
    },
    _scriptErrorHandler: function() {
        if (this._disposed) return;
        this._completedCallback(this.get_scriptElement(), false)
    },
    _scriptLoadHandler: function() {
        if (this._disposed) return;
        var a = this.get_scriptElement();
        if (a.readyState !== "loaded" && a.readyState !== "complete") return;
        this._completedCallback(a, true)
    }
};
Sys._ScriptLoaderTask.registerClass("Sys._ScriptLoaderTask", null, Sys.IDisposable);
Sys._ScriptLoaderTask._clearScript = function(a) {
    if (!Sys.Debug.isDebug) a.parentNode.removeChild(a)
};
Type.registerNamespace("Sys.Net");
Sys.Net.WebServiceProxy = function() {};
Sys.Net.WebServiceProxy.prototype = {
    get_timeout: function() {
        return this._timeout || 0
    },
    set_timeout: function(a) {
        if (a < 0) throw Error.argumentOutOfRange("value", a, Sys.Res.invalidTimeout);
        this._timeout = a
    },
    get_defaultUserContext: function() {
        return typeof this._userContext === "undefined" ? null : this._userContext
    },
    set_defaultUserContext: function(a) {
        this._userContext = a
    },
    get_defaultSucceededCallback: function() {
        return this._succeeded || null
    },
    set_defaultSucceededCallback: function(a) {
        this._succeeded = a
    },
    get_defaultFailedCallback: function() {
        return this._failed || null
    },
    set_defaultFailedCallback: function(a) {
        this._failed = a
    },
    get_enableJsonp: function() {
        return !!this._jsonp
    },
    set_enableJsonp: function(a) {
        this._jsonp = a
    },
    get_path: function() {
        return this._path || null
    },
    set_path: function(a) {
        this._path = a
    },
    get_jsonpCallbackParameter: function() {
        return this._callbackParameter || "callback"
    },
    set_jsonpCallbackParameter: function(a) {
        this._callbackParameter = a
    },
    _invoke: function(d, e, g, f, c, b, a) {
        c = c || this.get_defaultSucceededCallback();
        b = b || this.get_defaultFailedCallback();
        if (a === null || typeof a === "undefined") a = this.get_defaultUserContext();
        return Sys.Net.WebServiceProxy.invoke(d, e, g, f, c, b, a, this.get_timeout(), this.get_enableJsonp(), this.get_jsonpCallbackParameter())
    }
};
Sys.Net.WebServiceProxy.registerClass("Sys.Net.WebServiceProxy");
Sys.Net.WebServiceProxy.invoke = function(q, a, m, l, j, b, g, e, w, p) {
    var i = w !== false ? Sys.Net.WebServiceProxy._xdomain.exec(q) : null,
        c, n = i && i.length === 3 && (i[1] !== location.protocol || i[2] !== location.host);
    m = n || m;
    if (n) {
        p = p || "callback";
        c = "_jsonp" + Sys._jsonp++
    }
    if (!l) l = {};
    var r = l;
    if (!m || !r) r = {};
    var s, h, f = null,
        k, o = null,
        u = Sys.Net.WebRequest._createUrl(a ? q + "/" + encodeURIComponent(a) : q, r, n ? p + "=Sys." + c : null);
    if (n) {
        s = document.createElement("script");
        s.src = u;
        k = new Sys._ScriptLoaderTask(s, function(d, b) {
            if (!b || c) t({
                Message: String.format(Sys.Res.webServiceFailedNoMsg, a)
            }, -1)
        });

        function v() {
            if (f === null) return;
            f = null;
            h = new Sys.Net.WebServiceError(true, String.format(Sys.Res.webServiceTimedOut, a));
            k.dispose();
            delete Sys[c];
            if (b) b(h, g, a)
        }

        function t(d, e) {
            if (f !== null) {
                window.clearTimeout(f);
                f = null
            }
            k.dispose();
            delete Sys[c];
            c = null;
            if (typeof e !== "undefined" && e !== 200) {
                if (b) {
                    h = new Sys.Net.WebServiceError(false, d.Message || String.format(Sys.Res.webServiceFailedNoMsg, a), d.StackTrace || null, d.ExceptionType || null, d);
                    h._statusCode = e;
                    b(h, g, a)
                }
            } else if (j) j(d, g, a)
        }
        Sys[c] = t;
        e = e || Sys.Net.WebRequestManager.get_defaultTimeout();
        if (e > 0) f = window.setTimeout(v, e);
        k.execute();
        return null
    }
    var d = new Sys.Net.WebRequest;
    d.set_url(u);
    d.get_headers()["Content-Type"] = "application/json; charset=utf-8";
    if (!m) {
        o = Sys.Serialization.JavaScriptSerializer.serialize(l);
        if (o === "{}") o = ""
    }
    d.set_body(o);
    d.add_completed(x);
    if (e && e > 0) d.set_timeout(e);
    d.invoke();

    function x(d) {
        if (d.get_responseAvailable()) {
            var f = d.get_statusCode(),
                c = null;
            try {
                var e = d.getResponseHeader("Content-Type");
                if (e.startsWith("application/json")) c = d.get_object();
                else if (e.startsWith("text/xml")) c = d.get_xml();
                else c = d.get_responseData()
            } catch (m) {}
            var k = d.getResponseHeader("jsonerror"),
                h = k === "true";
            if (h) {
                if (c) c = new Sys.Net.WebServiceError(false, c.Message, c.StackTrace, c.ExceptionType, c)
            } else if (e.startsWith("application/json")) c = !c || typeof c.d === "undefined" ? c : c.d;
            if (f < 200 || f >= 300 || h) {
                if (b) {
                    if (!c || !h) c = new Sys.Net.WebServiceError(false, String.format(Sys.Res.webServiceFailedNoMsg, a));
                    c._statusCode = f;
                    b(c, g, a)
                }
            } else if (j) j(c, g, a)
        } else {
            var i;
            if (d.get_timedOut()) i = String.format(Sys.Res.webServiceTimedOut, a);
            else i = String.format(Sys.Res.webServiceFailedNoMsg, a);
            if (b) b(new Sys.Net.WebServiceError(d.get_timedOut(), i, "", ""), g, a)
        }
    }
    return d
};
Sys.Net.WebServiceProxy._generateTypedConstructor = function(a) {
    return function(b) {
        if (b)
            for (var c in b) this[c] = b[c];
        this.__type = a
    }
};
Sys._jsonp = 0;
Sys.Net.WebServiceProxy._xdomain = /^\s*([a-zA-Z0-9\+\-\.]+\:)\/\/([^?#\/]+)/;
Sys.Net.WebServiceError = function(d, e, c, a, b) {
    this._timedOut = d;
    this._message = e;
    this._stackTrace = c;
    this._exceptionType = a;
    this._errorObject = b;
    this._statusCode = -1
};
Sys.Net.WebServiceError.prototype = {
    get_timedOut: function() {
        return this._timedOut
    },
    get_statusCode: function() {
        return this._statusCode
    },
    get_message: function() {
        return this._message
    },
    get_stackTrace: function() {
        return this._stackTrace || ""
    },
    get_exceptionType: function() {
        return this._exceptionType || ""
    },
    get_errorObject: function() {
        return this._errorObject || null
    }
};
Sys.Net.WebServiceError.registerClass("Sys.Net.WebServiceError");
Type.registerNamespace('Sys');
Sys.Res = {
    'argumentInteger': 'Value must be an integer.',
    'invokeCalledTwice': 'Cannot call invoke more than once.',
    'webServiceFailed': 'The server method \'{0}\' failed with the following error: {1}',
    'argumentType': 'Object cannot be converted to the required type.',
    'argumentNull': 'Value cannot be null.',
    'scriptAlreadyLoaded': 'The script \'{0}\' has been referenced multiple times. If referencing Microsoft AJAX scripts explicitly, set the MicrosoftAjaxMode property of the ScriptManager to Explicit.',
    'scriptDependencyNotFound': 'The script \'{0}\' failed to load because it is dependent on script \'{1}\'.',
    'formatBadFormatSpecifier': 'Format specifier was invalid.',
    'requiredScriptReferenceNotIncluded': '\'{0}\' requires that you have included a script reference to \'{1}\'.',
    'webServiceFailedNoMsg': 'The server method \'{0}\' failed.',
    'argumentDomElement': 'Value must be a DOM element.',
    'invalidExecutorType': 'Could not create a valid Sys.Net.WebRequestExecutor from: {0}.',
    'cannotCallBeforeResponse': 'Cannot call {0} when responseAvailable is false.',
    'actualValue': 'Actual value was {0}.',
    'enumInvalidValue': '\'{0}\' is not a valid value for enum {1}.',
    'scriptLoadFailed': 'The script \'{0}\' could not be loaded.',
    'parameterCount': 'Parameter count mismatch.',
    'cannotDeserializeEmptyString': 'Cannot deserialize empty string.',
    'formatInvalidString': 'Input string was not in a correct format.',
    'invalidTimeout': 'Value must be greater than or equal to zero.',
    'cannotAbortBeforeStart': 'Cannot abort when executor has not started.',
    'argument': 'Value does not fall within the expected range.',
    'cannotDeserializeInvalidJson': 'Cannot deserialize. The data does not correspond to valid JSON.',
    'invalidHttpVerb': 'httpVerb cannot be set to an empty or null string.',
    'nullWebRequest': 'Cannot call executeRequest with a null webRequest.',
    'eventHandlerInvalid': 'Handler was not added through the Sys.UI.DomEvent.addHandler method.',
    'cannotSerializeNonFiniteNumbers': 'Cannot serialize non finite numbers.',
    'argumentUndefined': 'Value cannot be undefined.',
    'webServiceInvalidReturnType': 'The server method \'{0}\' returned an invalid type. Expected type: {1}',
    'servicePathNotSet': 'The path to the web service has not been set.',
    'argumentTypeWithTypes': 'Object of type \'{0}\' cannot be converted to type \'{1}\'.',
    'cannotCallOnceStarted': 'Cannot call {0} once started.',
    'badBaseUrl1': 'Base URL does not contain ://.',
    'badBaseUrl2': 'Base URL does not contain another /.',
    'badBaseUrl3': 'Cannot find last / in base URL.',
    'setExecutorAfterActive': 'Cannot set executor after it has become active.',
    'paramName': 'Parameter name: {0}',
    'nullReferenceInPath': 'Null reference while evaluating data path: \'{0}\'.',
    'cannotCallOutsideHandler': 'Cannot call {0} outside of a completed event handler.',
    'cannotSerializeObjectWithCycle': 'Cannot serialize object with cyclic reference within child properties.',
    'format': 'One of the identified items was in an invalid format.',
    'assertFailedCaller': 'Assertion Failed: {0}\r\nat {1}',
    'argumentOutOfRange': 'Specified argument was out of the range of valid values.',
    'webServiceTimedOut': 'The server method \'{0}\' timed out.',
    'notImplemented': 'The method or operation is not implemented.',
    'assertFailed': 'Assertion Failed: {0}',
    'invalidOperation': 'Operation is not valid due to the current state of the object.',
    'breakIntoDebugger': '{0}\r\n\r\nBreak into debugger?'
};

/*EOF /Scripts/MicrosoftAjax.js*/
/*/Scripts/MicrosoftMvcValidation.js*/
//----------------------------------------------------------
// Copyright (C) Microsoft Corporation. All rights reserved.
//----------------------------------------------------------
// MicrosoftMvcValidation.js

Type.registerNamespace('Sys.Mvc');
Sys.Mvc.$create_Validation = function() {
    return {};
}
Sys.Mvc.$create_JsonValidationField = function() {
    return {};
}
Sys.Mvc.$create_JsonValidationOptions = function() {
    return {};
}
Sys.Mvc.$create_JsonValidationRule = function() {
    return {};
}
Sys.Mvc.$create_ValidationContext = function() {
    return {};
}
Sys.Mvc.NumberValidator = function() {}
Sys.Mvc.NumberValidator.create = function(rule) {
    return Function.createDelegate(new Sys.Mvc.NumberValidator(), new Sys.Mvc.NumberValidator().validate);
}
Sys.Mvc.NumberValidator.prototype = {
    validate: function(value, context) {
        if (Sys.Mvc._ValidationUtil.$1(value)) {
            return true;
        }
        var $0 = Number.parseLocale(value);
        return (!isNaN($0));
    }
}
Sys.Mvc.FormContext = function(formElement, validationSummaryElement) {
    this.$5 = [];
    this.fields = new Array(0);
    this.$9 = formElement;
    this.$7 = validationSummaryElement;
    formElement['__MVC_FormValidation'] = this;
    if (validationSummaryElement) {
        var $0 = validationSummaryElement.getElementsByTagName('ul');
        if ($0.length > 0) {
            this.$8 = $0[0];
        }
    }
    this.$3 = Function.createDelegate(this, this.$D);
    this.$4 = Function.createDelegate(this, this.$E);
}
Sys.Mvc.FormContext._Application_Load = function() {
    var $0 = window.mvcClientValidationMetadata;
    if ($0) {
        while ($0.length > 0) {
            var $1 = $0.pop();
            Sys.Mvc.FormContext.$12($1);
        }
    }
}
Sys.Mvc.FormContext.$F = function($p0, $p1) {
    var $0 = [];
    var $1 = document.getElementsByName($p1);
    for (var $2 = 0; $2 < $1.length; $2++) {
        var $3 = $1[$2];
        if (Sys.Mvc.FormContext.$10($p0, $3)) {
            Array.add($0, $3);
        }
    }
    return $0;
}
Sys.Mvc.FormContext.getValidationForForm = function(formElement) {
    return formElement['__MVC_FormValidation'];
}
Sys.Mvc.FormContext.$10 = function($p0, $p1) {
    while ($p1) {
        if ($p0 === $p1) {
            return true;
        }
        $p1 = $p1.parentNode;
    }
    return false;
}
Sys.Mvc.FormContext.$12 = function($p0) {
    var $0 = $get($p0.FormId);
    var $1 = (!Sys.Mvc._ValidationUtil.$1($p0.ValidationSummaryId)) ? $get($p0.ValidationSummaryId) : null;
    var $2 = new Sys.Mvc.FormContext($0, $1);
    $2.enableDynamicValidation();
    $2.replaceValidationSummary = $p0.ReplaceValidationSummary;
    for (var $4 = 0; $4 < $p0.Fields.length; $4++) {
        var $5 = $p0.Fields[$4];
        var $6 = Sys.Mvc.FormContext.$F($0, $5.FieldName);
        var $7 = (!Sys.Mvc._ValidationUtil.$1($5.ValidationMessageId)) ? $get($5.ValidationMessageId) : null;
        var $8 = new Sys.Mvc.FieldContext($2);
        Array.addRange($8.elements, $6);
        $8.validationMessageElement = $7;
        $8.replaceValidationMessageContents = $5.ReplaceValidationMessageContents;
        for (var $9 = 0; $9 < $5.ValidationRules.length; $9++) {
            var $A = $5.ValidationRules[$9];
            var $B = Sys.Mvc.ValidatorRegistry.getValidator($A);
            if ($B) {
                var $C = Sys.Mvc.$create_Validation();
                $C.fieldErrorMessage = $A.ErrorMessage;
                $C.validator = $B;
                Array.add($8.validations, $C);
            }
        }
        $8.enableDynamicValidation();
        Array.add($2.fields, $8);
    }
    var $3 = $0.validationCallbacks;
    if (!$3) {
        $3 = [];
        $0.validationCallbacks = $3;
    }
    $3.push(Function.createDelegate(null, function() {
        return Sys.Mvc._ValidationUtil.$0($2.validate('submit'));
    }));
    return $2;
}
Sys.Mvc.FormContext.prototype = {
    $3: null,
    $4: null,
    $6: null,
    $7: null,
    $8: null,
    $9: null,
    replaceValidationSummary: false,
    addError: function(message) {
        this.addErrors([message]);
    },
    addErrors: function(messages) {
        if (!Sys.Mvc._ValidationUtil.$0(messages)) {
            Array.addRange(this.$5, messages);
            this.$11();
        }
    },
    clearErrors: function() {
        Array.clear(this.$5);
        this.$11();
    },
    $A: function() {
        if (this.$7) {
            if (this.$8) {
                Sys.Mvc._ValidationUtil.$3(this.$8);
                for (var $0 = 0; $0 < this.$5.length; $0++) {
                    var $1 = document.createElement('li');
                    Sys.Mvc._ValidationUtil.$4($1, this.$5[$0]);
                    this.$8.appendChild($1);
                }
            }
            Sys.UI.DomElement.removeCssClass(this.$7, 'validation-summary-valid');
            Sys.UI.DomElement.addCssClass(this.$7, 'validation-summary-errors');
        }
    },
    $B: function() {
        var $0 = this.$7;
        if ($0) {
            var $1 = this.$8;
            if ($1) {
                $1.innerHTML = '';
            }
            Sys.UI.DomElement.removeCssClass($0, 'validation-summary-errors');
            Sys.UI.DomElement.addCssClass($0, 'validation-summary-valid');
        }
    },
    enableDynamicValidation: function() {
        Sys.UI.DomEvent.addHandler(this.$9, 'click', this.$3);
        Sys.UI.DomEvent.addHandler(this.$9, 'submit', this.$4);
    },
    $C: function($p0) {
        if ($p0.disabled) {
            return null;
        }
        var $0 = $p0.tagName.toUpperCase();
        var $1 = $p0;
        if ($0 === 'INPUT') {
            var $2 = $1.type;
            if ($2 === 'submit' || $2 === 'image') {
                return $1;
            }
        } else if (($0 === 'BUTTON') && ($1.type === 'submit')) {
            return $1;
        }
        return null;
    },
    $D: function($p0) {
        this.$6 = this.$C($p0.target);
    },
    $E: function($p0) {
        var $0 = $p0.target;
        var $1 = this.$6;
        if ($1 && $1.disableValidation) {
            return;
        }
        var $2 = this.validate('submit');
        if (!Sys.Mvc._ValidationUtil.$0($2)) {
            $p0.preventDefault();
        }
    },
    $11: function() {
        if (!this.$5.length) {
            this.$B();
        } else {
            this.$A();
        }
    },
    validate: function(eventName) {
        var $0 = this.fields;
        var $1 = [];
        for (var $2 = 0; $2 < $0.length; $2++) {
            var $3 = $0[$2];
            if (!$3.elements[0].disabled) {
                var $4 = $3.validate(eventName);
                if ($4) {
                    Array.addRange($1, $4);
                }
            }
        }
        if (this.replaceValidationSummary) {
            this.clearErrors();
            this.addErrors($1);
        }
        return $1;
    }
}
Sys.Mvc.FieldContext = function(formContext) {
    this.$A = [];
    this.elements = new Array(0);
    this.validations = new Array(0);
    this.formContext = formContext;
    this.$6 = Function.createDelegate(this, this.$D);
    this.$7 = Function.createDelegate(this, this.$E);
    this.$8 = Function.createDelegate(this, this.$F);
    this.$9 = Function.createDelegate(this, this.$10);
}
Sys.Mvc.FieldContext.prototype = {
    $6: null,
    $7: null,
    $8: null,
    $9: null,
    defaultErrorMessage: null,
    formContext: null,
    replaceValidationMessageContents: false,
    validationMessageElement: null,
    addError: function(message) {
        this.addErrors([message]);
    },
    addErrors: function(messages) {
        if (!Sys.Mvc._ValidationUtil.$0(messages)) {
            Array.addRange(this.$A, messages);
            this.$14();
        }
    },
    clearErrors: function() {
        Array.clear(this.$A);
        this.$14();
    },
    $B: function() {
        var $0 = this.validationMessageElement;
        if ($0) {
            if (this.replaceValidationMessageContents) {
                Sys.Mvc._ValidationUtil.$4($0, this.$A[0]);
            }
            Sys.UI.DomElement.removeCssClass($0, 'field-validation-valid');
            Sys.UI.DomElement.addCssClass($0, 'field-validation-error');
        }
        var $1 = this.elements;
        for (var $2 = 0; $2 < $1.length; $2++) {
            var $3 = $1[$2];
            Sys.UI.DomElement.removeCssClass($3, 'input-validation-valid');
            Sys.UI.DomElement.addCssClass($3, 'input-validation-error');
        }
    },
    $C: function() {
        var $0 = this.validationMessageElement;
        if ($0) {
            if (this.replaceValidationMessageContents) {
                Sys.Mvc._ValidationUtil.$4($0, '');
            }
            Sys.UI.DomElement.removeCssClass($0, 'field-validation-error');
            Sys.UI.DomElement.addCssClass($0, 'field-validation-valid');
        }
        var $1 = this.elements;
        for (var $2 = 0; $2 < $1.length; $2++) {
            var $3 = $1[$2];
            Sys.UI.DomElement.removeCssClass($3, 'input-validation-error');
            Sys.UI.DomElement.addCssClass($3, 'input-validation-valid');
        }
    },
    $D: function($p0) {
        if ($p0.target['__MVC_HasTextChanged'] || $p0.target['__MVC_HasValidationFired']) {
            this.validate('blur');
        }
    },
    $E: function($p0) {
        $p0.target['__MVC_HasTextChanged'] = true;
    },
    $F: function($p0) {
        $p0.target['__MVC_HasTextChanged'] = true;
        if ($p0.target['__MVC_HasValidationFired']) {
            this.validate('input');
        }
    },
    $10: function($p0) {
        if ($p0.rawEvent.propertyName === 'value') {
            $p0.target['__MVC_HasTextChanged'] = true;
            if ($p0.target['__MVC_HasValidationFired']) {
                this.validate('input');
            }
        }
    },
    enableDynamicValidation: function() {
        var $0 = this.elements;
        for (var $1 = 0; $1 < $0.length; $1++) {
            var $2 = $0[$1];
            if (Sys.Mvc._ValidationUtil.$2($2, 'onpropertychange')) {
                var $3 = document.documentMode;
                if ($3 && $3 >= 8) {
                    Sys.UI.DomEvent.addHandler($2, 'propertychange', this.$9);
                }
            } else {
                Sys.UI.DomEvent.addHandler($2, 'input', this.$8);
            }
            Sys.UI.DomEvent.addHandler($2, 'change', this.$7);
            Sys.UI.DomEvent.addHandler($2, 'blur', this.$6);
        }
    },
    $11: function($p0, $p1) {
        var $0 = $p1 || this.defaultErrorMessage;
        if (Boolean.isInstanceOfType($p0)) {
            return ($p0) ? null : $0;
        }
        if (String.isInstanceOfType($p0)) {
            return (($p0).length) ? $p0 : $0;
        }
        return null;
    },
    $12: function() {
        var $0 = this.elements;
        return ($0.length > 0) ? $0[0].value : null;
    },
    $13: function() {
        var $0 = this.elements;
        for (var $1 = 0; $1 < $0.length; $1++) {
            var $2 = $0[$1];
            $2['__MVC_HasValidationFired'] = true;
        }
    },
    $14: function() {
        if (!this.$A.length) {
            this.$C();
        } else {
            this.$B();
        }
    },
    validate: function(eventName) {
        var $0 = this.validations;
        var $1 = [];
        var $2 = this.$12();
        for (var $3 = 0; $3 < $0.length; $3++) {
            var $4 = $0[$3];
            var $5 = Sys.Mvc.$create_ValidationContext();
            $5.eventName = eventName;
            $5.fieldContext = this;
            $5.validation = $4;
            var $6 = $4.validator($2, $5);
            var $7 = this.$11($6, $4.fieldErrorMessage);
            if (!Sys.Mvc._ValidationUtil.$1($7)) {
                Array.add($1, $7);
            }
        }
        this.$13();
        this.clearErrors();
        this.addErrors($1);
        return $1;
    }
}
Sys.Mvc.RangeValidator = function(minimum, maximum) {
    this.$0 = minimum;
    this.$1 = maximum;
}
Sys.Mvc.RangeValidator.create = function(rule) {
    var $0 = rule.ValidationParameters['min'];
    var $1 = rule.ValidationParameters['max'];
    return Function.createDelegate(new Sys.Mvc.RangeValidator($0, $1), new Sys.Mvc.RangeValidator($0, $1).validate);
}
Sys.Mvc.RangeValidator.prototype = {
    $0: null,
    $1: null,
    validate: function(value, context) {
        if (Sys.Mvc._ValidationUtil.$1(value)) {
            return true;
        }
        var $0 = Number.parseLocale(value);
        return (!isNaN($0) && this.$0 <= $0 && $0 <= this.$1);
    }
}
Sys.Mvc.RegularExpressionValidator = function(pattern) {
    this.$0 = pattern;
}
Sys.Mvc.RegularExpressionValidator.create = function(rule) {
    var $0 = rule.ValidationParameters['pattern'];
    return Function.createDelegate(new Sys.Mvc.RegularExpressionValidator($0), new Sys.Mvc.RegularExpressionValidator($0).validate);
}
Sys.Mvc.RegularExpressionValidator.prototype = {
    $0: null,
    validate: function(value, context) {
        if (Sys.Mvc._ValidationUtil.$1(value)) {
            return true;
        }
        var $0 = new RegExp(this.$0);
        var $1 = $0.exec(value);
        return (!Sys.Mvc._ValidationUtil.$0($1) && $1[0].length === value.length);
    }
}
Sys.Mvc.RequiredValidator = function() {}
Sys.Mvc.RequiredValidator.create = function(rule) {
    return Function.createDelegate(new Sys.Mvc.RequiredValidator(), new Sys.Mvc.RequiredValidator().validate);
}
Sys.Mvc.RequiredValidator.$0 = function($p0) {
    if ($p0.tagName.toUpperCase() === 'INPUT') {
        var $0 = ($p0.type).toUpperCase();
        if ($0 === 'RADIO') {
            return true;
        }
    }
    return false;
}
Sys.Mvc.RequiredValidator.$1 = function($p0) {
    if ($p0.tagName.toUpperCase() === 'SELECT') {
        return true;
    }
    return false;
}
Sys.Mvc.RequiredValidator.$2 = function($p0) {
    if ($p0.tagName.toUpperCase() === 'INPUT') {
        var $0 = ($p0.type).toUpperCase();
        switch ($0) {
            case 'TEXT':
            case 'PASSWORD':
            case 'FILE':
                return true;
        }
    }
    if ($p0.tagName.toUpperCase() === 'TEXTAREA') {
        return true;
    }
    return false;
}
Sys.Mvc.RequiredValidator.$3 = function($p0) {
    for (var $0 = 0; $0 < $p0.length; $0++) {
        var $1 = $p0[$0];
        if ($1.checked) {
            return true;
        }
    }
    return false;
}
Sys.Mvc.RequiredValidator.$4 = function($p0) {
    for (var $0 = 0; $0 < $p0.length; $0++) {
        var $1 = $p0[$0];
        if ($1.selected) {
            if (!Sys.Mvc._ValidationUtil.$1($1.value)) {
                return true;
            }
        }
    }
    return false;
}
Sys.Mvc.RequiredValidator.$5 = function($p0) {
    return (!Sys.Mvc._ValidationUtil.$1($p0.value));
}
Sys.Mvc.RequiredValidator.prototype = {
    validate: function(value, context) {
        var $0 = context.fieldContext.elements;
        if (!$0.length) {
            return true;
        }
        var $1 = $0[0];
        if (Sys.Mvc.RequiredValidator.$2($1)) {
            return Sys.Mvc.RequiredValidator.$5($1);
        }
        if (Sys.Mvc.RequiredValidator.$0($1)) {
            return Sys.Mvc.RequiredValidator.$3($0);
        }
        if (Sys.Mvc.RequiredValidator.$1($1)) {
            return Sys.Mvc.RequiredValidator.$4(($1).options);
        }
        return true;
    }
}
Sys.Mvc.StringLengthValidator = function(minLength, maxLength) {
    this.$1 = minLength;
    this.$0 = maxLength;
}
Sys.Mvc.StringLengthValidator.create = function(rule) {
    var $0 = (rule.ValidationParameters['min'] || 0);
    var $1 = (rule.ValidationParameters['max'] || Number.MAX_VALUE);
    return Function.createDelegate(new Sys.Mvc.StringLengthValidator($0, $1), new Sys.Mvc.StringLengthValidator($0, $1).validate);
}
Sys.Mvc.StringLengthValidator.prototype = {
    $0: 0,
    $1: 0,
    validate: function(value, context) {
        if (Sys.Mvc._ValidationUtil.$1(value)) {
            return true;
        }
        return (this.$1 <= value.length && value.length <= this.$0);
    }
}
Sys.Mvc._ValidationUtil = function() {}
Sys.Mvc._ValidationUtil.$0 = function($p0) {
    return (!$p0 || !$p0.length);
}
Sys.Mvc._ValidationUtil.$1 = function($p0) {
    return (!$p0 || !$p0.length);
}
Sys.Mvc._ValidationUtil.$2 = function($p0, $p1) {
    return ($p1 in $p0);
}
Sys.Mvc._ValidationUtil.$3 = function($p0) {
    while ($p0.firstChild) {
        $p0.removeChild($p0.firstChild);
    }
}
Sys.Mvc._ValidationUtil.$4 = function($p0, $p1) {
    var $0 = document.createTextNode($p1);
    Sys.Mvc._ValidationUtil.$3($p0);
    $p0.appendChild($0);
}
Sys.Mvc.ValidatorRegistry = function() {}
Sys.Mvc.ValidatorRegistry.getValidator = function(rule) {
    var $0 = Sys.Mvc.ValidatorRegistry.validators[rule.ValidationType];
    return ($0) ? $0(rule) : null;
}
Sys.Mvc.ValidatorRegistry.$0 = function() {
    return {
        required: Function.createDelegate(null, Sys.Mvc.RequiredValidator.create),
        length: Function.createDelegate(null, Sys.Mvc.StringLengthValidator.create),
        regex: Function.createDelegate(null, Sys.Mvc.RegularExpressionValidator.create),
        range: Function.createDelegate(null, Sys.Mvc.RangeValidator.create),
        number: Function.createDelegate(null, Sys.Mvc.NumberValidator.create)
    };
}
Sys.Mvc.NumberValidator.registerClass('Sys.Mvc.NumberValidator');
Sys.Mvc.FormContext.registerClass('Sys.Mvc.FormContext');
Sys.Mvc.FieldContext.registerClass('Sys.Mvc.FieldContext');
Sys.Mvc.RangeValidator.registerClass('Sys.Mvc.RangeValidator');
Sys.Mvc.RegularExpressionValidator.registerClass('Sys.Mvc.RegularExpressionValidator');
Sys.Mvc.RequiredValidator.registerClass('Sys.Mvc.RequiredValidator');
Sys.Mvc.StringLengthValidator.registerClass('Sys.Mvc.StringLengthValidator');
Sys.Mvc._ValidationUtil.registerClass('Sys.Mvc._ValidationUtil');
Sys.Mvc.ValidatorRegistry.registerClass('Sys.Mvc.ValidatorRegistry');
Sys.Mvc.ValidatorRegistry.validators = Sys.Mvc.ValidatorRegistry.$0();
// ---- Do not remove this footer ----
// Generated using Script# v0.5.0.0 (http://projects.nikhilk.net)
// -----------------------------------
Sys.Application.add_load(function() {
    Sys.Application.remove_load(arguments.callee);
    Sys.Mvc.FormContext._Application_Load();
});
/*EOF /Scripts/MicrosoftMvcValidation.js*/
/*/Scripts/jquery.creditCardValidator.js*/
// Generated by CoffeeScript 1.4.0

/*
jQuery Credit Card Validator

Copyright 2012 Pawel Decowski

This work is licensed under the Creative Commons Attribution-ShareAlike 3.0
Unported License. To view a copy of this license, visit:

http://creativecommons.org/licenses/by-sa/3.0/

or send a letter to:

Creative Commons, 444 Castro Street, Suite 900,
Mountain View, California, 94041, USA.
*/

(function() {
    var $,
        __indexOf = [].indexOf || function(item) {
            for (var i = 0, l = this.length; i < l; i++) {
                if (i in this && this[i] === item) return i;
            }
            return -1;
        };

    $ = jQuery;

    $.fn.validateCreditCard = function(callback, options) {
        var card, card_type, card_types, get_card_type, is_valid_length, is_valid_luhn, normalize, validate, validate_number, _i, _len, _ref, _ref1;
        card_types = [{
            name: 'Amex',
            pattern: /^3[47]/,
            valid_length: [15]
        }, {
            name: 'Switch',
            pattern: /^(4903|4905|4911|4936|564182|633110|6333|6759)/,
            valid_length: [14]
        }, {
            name: 'Solo',
            pattern: /^(6334|6767)/,
            valid_length: [14]
        }, {
            name: 'diners_club_carte_blanche',
            pattern: /^30[0-5]/,
            valid_length: [14]
        }, {
            name: 'Diners',
            pattern: /^(36|30[0-5])/,
            valid_length: [14]
        }, {
            name: 'JCB',
            pattern: /^35(2[89]|[3-8][0-9])/,
            valid_length: [16]
        }, {
            name: 'laser',
            pattern: /^(6304|670[69]|6771)/,
            valid_length: [16, 17, 18, 19]
        }, {
            name: 'Visa',
            pattern: /^(4|4026|417500|4508|4844|491(3|7))/,
            valid_length: [16]
        }, {
            name: 'Master',
            pattern: /^5[1-5]/,
            valid_length: [16]
        }, {
            name: 'Maestro',
            pattern: /^(5018|5020|5038|6304|6759|676[1-3])/,
            valid_length: [12, 13, 14, 15, 16, 17, 18, 19]
        }, {
            name: 'Discover',
            pattern: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/,
            valid_length: [16]
        }];
        if (options == null) {
            options = {};
        }
        if ((_ref = options.accept) == null) {
            options.accept = (function() {
                var _i, _len, _results;
                _results = [];
                for (_i = 0, _len = card_types.length; _i < _len; _i++) {
                    card = card_types[_i];
                    _results.push(card.name);
                }
                return _results;
            })();
        }
        _ref1 = options.accept;
        for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
            card_type = _ref1[_i];
            if (__indexOf.call((function() {
                var _j, _len1, _results;
                _results = [];
                for (_j = 0, _len1 = card_types.length; _j < _len1; _j++) {
                    card = card_types[_j];
                    _results.push(card.name);
                }
                return _results;
            })(), card_type) < 0) {
                throw "Credit card type '" + card_type + "' is not supported";
            }
        }
        get_card_type = function(number) {
            var _j, _len1, _ref2;
            _ref2 = (function() {
                var _k, _len1, _ref2, _results;
                _results = [];
                for (_k = 0, _len1 = card_types.length; _k < _len1; _k++) {
                    card = card_types[_k];
                    if (_ref2 = card.name, __indexOf.call(options.accept, _ref2) >= 0) {
                        _results.push(card);
                    }
                }
                return _results;
            })();
            for (_j = 0, _len1 = _ref2.length; _j < _len1; _j++) {
                card_type = _ref2[_j];
                if (number.match(card_type.pattern)) {
                    return card_type;
                }
            }
            return null;
        };
        is_valid_luhn = function(number) {
            var digit, n, sum, _j, _len1, _ref2;
            sum = 0;
            _ref2 = number.split('').reverse();
            for (n = _j = 0, _len1 = _ref2.length; _j < _len1; n = ++_j) {
                digit = _ref2[n];
                digit = +digit;
                if (n % 2) {
                    digit *= 2;
                    if (digit < 10) {
                        sum += digit;
                    } else {
                        sum += digit - 9;
                    }
                } else {
                    sum += digit;
                }
            }
            return sum % 10 === 0;
        };
        is_valid_length = function(number, card_type) {
            var _ref2;
            return _ref2 = number.length, __indexOf.call(card_type.valid_length, _ref2) >= 0;
        };
        validate_number = function(number) {
            var length_valid, luhn_valid;
            card_type = get_card_type(number);
            luhn_valid = false;
            length_valid = false;
            if (card_type != null) {
                luhn_valid = is_valid_luhn(number);
                length_valid = is_valid_length(number, card_type);
            }
            return callback({
                card_type: card_type,
                luhn_valid: luhn_valid,
                length_valid: length_valid
            });
        };
        validate = function() {
            var number;
            number = normalize($(this).val());
            return validate_number(number);
        };
        normalize = function(number) {
            return number.replace(/[ -]/g, '');
        };
        this.bind('input', function() {
            $(this).unbind('keyup');
            return validate.call(this);
        });
        this.bind('keyup', function() {
            return validate.call(this);
        });
        if (this.length !== 0) {
            validate.call(this);
        }
        return this;
    };

}).call(this);

/*EOF /Scripts/jquery.creditCardValidator.js*/
/*/Scripts/jquery.cached.script.js*/
jQuery.cachedScript = function(url, options) {

    // Allow user to set any option except for dataType, cache, and url
    options = $.extend(options || {}, {
        dataType: "script",
        cache: true,
        url: url
    });

    // Use $.ajax() since it is more flexible than $.getScript
    // Return the jqXHR object so we can chain callbacks
    return jQuery.ajax(options);
};
/*EOF /Scripts/jquery.cached.script.js*/
/*/script/facebox/facebox.js*/
/*
 * Facebox (for jQuery)
 * version: 1.2 (05/05/2008)
 * @requires jQuery v1.2 or later
 *
 * Examples at http://famspam.com/facebox/
 *
 * Licensed under the MIT:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2007, 2008 Chris Wanstrath [ chris@ozmm.org ]
 *
 * Usage:
 *
 *  jQuery(document).ready(function() {
 *    jQuery('a[rel*=facebox]').facebox()
 *  })
 *
 *  <a href="#terms" rel="facebox">Terms</a>
 *    Loads the #terms div in the box
 *
 *  <a href="terms.html" rel="facebox">Terms</a>
 *    Loads the terms.html page in the box
 *
 *  <a href="terms.png" rel="facebox">Terms</a>
 *    Loads the terms.png image in the box
 *
 *
 *  You can also use it programmatically:
 *
 *    jQuery.facebox('some html')
 *
 *  The above will open a facebox with "some html" as the content.
 *
 *    jQuery.facebox(function($) {
 *      $.get('blah.html', function(data) { $.facebox(data) })
 *    })
 *
 *  The above will show a loading screen before the passed function is called,
 *  allowing for a better ajaxy experience.
 *
 *  The facebox function can also display an ajax page or image:
 *
 *    jQuery.facebox({ ajax: 'remote.html' })
 *    jQuery.facebox({ image: 'dude.jpg' })
 *
 *  Want to close the facebox?  Trigger the 'close.facebox' document event:
 *
 *    jQuery(document).trigger('close.facebox')
 *
 *  Facebox also has a bunch of other hooks:
 *
 *    loading.facebox
 *    beforeReveal.facebox
 *    reveal.facebox (aliased as 'afterReveal.facebox')
 *    init.facebox
 *
 *  Simply bind a function to any of these hooks:
 *
 *   $(document).bind('reveal.facebox', function() { ...stuff to do after the facebox and contents are revealed... })
 *
 */
(function($) {
    $.facebox = function(data, klass) {
        $.facebox.loading()

        if (data.ajax) fillFaceboxFromAjax(data.ajax)
        else if (data.image) fillFaceboxFromImage(data.image)
        else if (data.div) fillFaceboxFromHref(data.div)
        else if ($.isFunction(data)) data.call($)
        else $.facebox.reveal(data, klass)
    }

    /*
     * Public, $.facebox methods
     */

    $.extend($.facebox, {
        settings: {
            opacity: 0,
            overlay: true,
            loadingImage: '/facebox/loading.gif',
            closeImage: '/facebox/closelabel.gif',
            imageTypes: ['png', 'jpg', 'jpeg', 'gif'],
            faceboxHtml: '\
    <div id="facebox" style="display:none;"> \
      <div class="popup"> \
        <table id="facebox_table"> \
          <tbody> \
            <tr> \
              <td class="tl"/><td class="b"/><td class="tr"/> \
            </tr> \
            <tr> \
              <td class="b"/> \
              <td class="body"> \
                <div class="footer"> \
                  <a href="#" class="close"><img src="/facebox/closelabel.gif" title="close" class="close_image" /></a> \
                </div> \
                <div class="content"> \
                </div> \
              </td> \
              <td class="b"/> \
            </tr> \
            <tr> \
              <td class="bl"/><td class="b"/><td class="br"/> \
            </tr> \
          </tbody> \
        </table> \
      </div> \
    </div>'
        },

        loading: function() {
            init()
            if ($('#facebox .loading').length == 1) return true
            showOverlay()

            $('#facebox .content').empty()
            $('#facebox .body').children().hide().end().
            append('<div class="loading"><img src="' + $.facebox.settings.loadingImage + '"/></div>')

            $('#facebox .header').children().hide().end().
            append('' + $.facebox.settings.headerText + '')

            $('#facebox').css({
                top: getPageScroll()[1] + (getPageHeight() / 10),
                left: 385.5
            }).show()

            $(document).bind('keydown.facebox', function(e) {
                if (e.keyCode == 27) $.facebox.close()
                return true
            })
            //$(document).trigger('loading.facebox')
        },

        reveal: function(data, klass) {
            $(document).trigger('beforeReveal.facebox')
            if (klass) $('#facebox .content').addClass(klass)
            $('#facebox .content').append(data)
            $('#facebox .loading').remove()
            $('#facebox .body').children().fadeIn('normal')
            $('#facebox').css('left', $(window).width() / 2 - ($('#facebox table').width() / 2))
            $(document).trigger('reveal.facebox').trigger('afterReveal.facebox')
        },

        close: function() {
            $(document).trigger('close.facebox')
            return false
        }
    })

    /*
     * Public, $.fn methods
     */

    $.fn.facebox = function(settings) {
        init(settings)

        function clickHandler() {
            $.facebox.loading(true)

            // support for rel="facebox.inline_popup" syntax, to add a class
            // also supports deprecated "facebox[.inline_popup]" syntax
            var klass = this.rel.match(/facebox\[?\.(\w+)\]?/)
            if (klass) klass = klass[1]

            fillFaceboxFromHref(this.href, klass)
            return false
        }

        return this.click(clickHandler)
    }

    /*
     * Private methods
     */

    // called one time to setup facebox on this page
    function init(settings) {
        if ($.facebox.settings.inited) return true
        else $.facebox.settings.inited = true

        $(document).trigger('init.facebox')
        makeCompatible()

        var imageTypes = $.facebox.settings.imageTypes.join('|')
        $.facebox.settings.imageTypesRegexp = new RegExp('\.' + imageTypes + '$', 'i')

        if (settings) $.extend($.facebox.settings, settings)
        $('body').append($.facebox.settings.faceboxHtml)

        var preload = [new Image(), new Image()]
        preload[0].src = $.facebox.settings.closeImage
        preload[1].src = $.facebox.settings.loadingImage

        $('#facebox').find('.b:first, .bl, .br, .tl, .tr').each(function() {
            preload.push(new Image())
            preload.slice(-1).src = $(this).css('background-image').replace(/url\((.+)\)/, '$1')
        })

        $('#facebox .close').click($.facebox.close)
        $('#facebox .close_image').attr('src', $.facebox.settings.closeImage)
    }

    // getPageScroll() by quirksmode.com
    function getPageScroll() {
        var xScroll, yScroll;
        if (self.pageYOffset) {
            yScroll = self.pageYOffset;
            xScroll = self.pageXOffset;
        } else if (document.documentElement && document.documentElement.scrollTop) { // Explorer 6 Strict
            yScroll = document.documentElement.scrollTop;
            xScroll = document.documentElement.scrollLeft;
        } else if (document.body) { // all other Explorers
            yScroll = document.body.scrollTop;
            xScroll = document.body.scrollLeft;
        }
        return new Array(xScroll, yScroll)
    }

    // Adapted from getPageSize() by quirksmode.com
    function getPageHeight() {
        var windowHeight
        if (self.innerHeight) { // all except Explorer
            windowHeight = self.innerHeight;
        } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
            windowHeight = document.documentElement.clientHeight;
        } else if (document.body) { // other Explorers
            windowHeight = document.body.clientHeight;
        }
        return windowHeight
    }

    // Backwards compatibility
    function makeCompatible() {
        var $s = $.facebox.settings

        $s.loadingImage = $s.loading_image || $s.loadingImage
        $s.closeImage = $s.close_image || $s.closeImage
        $s.imageTypes = $s.image_types || $s.imageTypes
        $s.faceboxHtml = $s.facebox_html || $s.faceboxHtml
    }

    // Figures out what you want to display and displays it
    // formats are:
    //     div: #id
    //   image: blah.extension
    //    ajax: anything else
    function fillFaceboxFromHref(href, klass) {
        // div
        if (href.match(/#/)) {
            var url = window.location.href.split('#')[0]
            var target = href.replace(url, '')
            $.facebox.reveal($(target).clone().show(), klass)

            // image
        } else if (href.match($.facebox.settings.imageTypesRegexp)) {
            fillFaceboxFromImage(href, klass)
            // ajax
        } else {
            fillFaceboxFromAjax(href, klass)
        }
    }

    function fillFaceboxFromImage(href, klass) {
        var image = new Image()
        image.onload = function() {
            $.facebox.reveal('<div class="image"><img src="' + image.src + '" /></div>', klass)
        }
        image.src = href
    }

    function fillFaceboxFromAjax(href, klass) {
        $.get(href, function(data) {
            $.facebox.reveal(data, klass)
        })
    }

    function skipOverlay() {
        return $.facebox.settings.overlay == false || $.facebox.settings.opacity === null
    }

    function showOverlay() {
        if (skipOverlay()) return

        if ($('facebox_overlay').length == 0)
            $("body").append('<div id="facebox_overlay" class="facebox_hide"></div>')

        $('#facebox_overlay').hide().addClass("facebox_overlayBG")
            .css('opacity', $.facebox.settings.opacity)
            .click(function() {
                $(document).trigger('close.facebox')
            })
            .fadeIn(200)
        return false
    }

    function hideOverlay() {
        if (skipOverlay()) return

        $('#facebox_overlay').fadeOut(200, function() {
            $("#facebox_overlay").removeClass("facebox_overlayBG")
            $("#facebox_overlay").addClass("facebox_hide")
            $("#facebox_overlay").remove()
        })

        return false
    }

    /*
     * Bindings
     */

    $(document).bind('close.facebox', function() {
        $(document).unbind('keydown.facebox')
        $('#facebox').fadeOut(function() {
            $('#facebox .content').removeClass().addClass('content')
            hideOverlay()
            $('#facebox .loading').remove()
        })
    })

})(jQuery);

/*EOF /script/facebox/facebox.js*/
/*/Scripts/doT-1.0.1.min.js*/
/* Laura Doktorova https://github.com/olado/doT */
(function() {
    function o() {
        var a = {
                "&": "&#38;",
                "<": "&#60;",
                ">": "&#62;",
                '"': "&#34;",
                "'": "&#39;",
                "/": "&#47;"
            },
            b = /&(?!#?\w+;)|<|>|"|'|\//g;
        return function() {
            return this ? this.replace(b, function(c) {
                return a[c] || c
            }) : this
        }
    }

    function p(a, b, c) {
        return (typeof b === "string" ? b : b.toString()).replace(a.define || i, function(l, e, f, g) {
            if (e.indexOf("def.") === 0) e = e.substring(4);
            if (!(e in c))
                if (f === ":") {
                    a.defineParams && g.replace(a.defineParams, function(n, h, d) {
                        c[e] = {
                            arg: h,
                            text: d
                        }
                    });
                    e in c || (c[e] = g)
                } else(new Function("def", "def['" +
                    e + "']=" + g))(c);
            return ""
        }).replace(a.use || i, function(l, e) {
            if (a.useParams) e = e.replace(a.useParams, function(g, n, h, d) {
                if (c[h] && c[h].arg && d) {
                    g = (h + ":" + d).replace(/'|\\/g, "_");
                    c.__exp = c.__exp || {};
                    c.__exp[g] = c[h].text.replace(RegExp("(^|[^\\w$])" + c[h].arg + "([^\\w$])", "g"), "$1" + d + "$2");
                    return n + "def.__exp['" + g + "']"
                }
            });
            var f = (new Function("def", "return " + e))(c);
            return f ? p(a, f, c) : f
        })
    }

    function m(a) {
        return a.replace(/\\('|\\)/g, "$1").replace(/[\r\t\n]/g, " ")
    }
    var j = {
            version: "1.0.1",
            templateSettings: {
                evaluate: /\{\{([\s\S]+?(\}?)+)\}\}/g,
                interpolate: /\{\{=([\s\S]+?)\}\}/g,
                encode: /\{\{!([\s\S]+?)\}\}/g,
                use: /\{\{#([\s\S]+?)\}\}/g,
                useParams: /(^|[^\w$])def(?:\.|\[[\'\"])([\w$\.]+)(?:[\'\"]\])?\s*\:\s*([\w$\.]+|\"[^\"]+\"|\'[^\']+\'|\{[^\}]+\})/g,
                define: /\{\{##\s*([\w\.$]+)\s*(\:|=)([\s\S]+?)#\}\}/g,
                defineParams: /^\s*([\w$]+):([\s\S]+)/,
                conditional: /\{\{\?(\?)?\s*([\s\S]*?)\s*\}\}/g,
                iterate: /\{\{~\s*(?:\}\}|([\s\S]+?)\s*\:\s*([\w$]+)\s*(?:\:\s*([\w$]+))?\s*\}\})/g,
                varname: "it",
                strip: true,
                append: true,
                selfcontained: false
            },
            template: undefined,
            compile: undefined
        },
        q;
    if (typeof module !== "undefined" && module.exports) module.exports = j;
    else if (typeof define === "function" && define.amd) define(function() {
        return j
    });
    else {
        q = function() {
            return this || (0, eval)("this")
        }();
        q.doT = j
    }
    String.prototype.encodeHTML = o();
    var r = {
            append: {
                start: "'+(",
                end: ")+'",
                endencode: "||'').toString().encodeHTML()+'"
            },
            split: {
                start: "';out+=(",
                end: ");out+='",
                endencode: "||'').toString().encodeHTML();out+='"
            }
        },
        i = /$^/;
    j.template = function(a, b, c) {
        b = b || j.templateSettings;
        var l = b.append ? r.append :
            r.split,
            e, f = 0,
            g;
        a = b.use || b.define ? p(b, a, c || {}) : a;
        a = ("var out='" + (b.strip ? a.replace(/(^|\r|\n)\t* +| +\t*(\r|\n|$)/g, " ").replace(/\r|\n|\t|\/\*[\s\S]*?\*\//g, "") : a).replace(/'|\\/g, "\\$&").replace(b.interpolate || i, function(h, d) {
            return l.start + m(d) + l.end
        }).replace(b.encode || i, function(h, d) {
            e = true;
            return l.start + m(d) + l.endencode
        }).replace(b.conditional || i, function(h, d, k) {
            return d ? k ? "';}else if(" + m(k) + "){out+='" : "';}else{out+='" : k ? "';if(" + m(k) + "){out+='" : "';}out+='"
        }).replace(b.iterate || i, function(h,
                                            d, k, s) {
            if (!d) return "';} } out+='";
            f += 1;
            g = s || "i" + f;
            d = m(d);
            return "';var arr" + f + "=" + d + ";if(arr" + f + "){var " + k + "," + g + "=-1,l" + f + "=arr" + f + ".length-1;while(" + g + "<l" + f + "){" + k + "=arr" + f + "[" + g + "+=1];out+='"
        }).replace(b.evaluate || i, function(h, d) {
            return "';" + m(d) + "out+='"
        }) + "';return out;").replace(/\n/g, "\\n").replace(/\t/g, "\\t").replace(/\r/g, "\\r").replace(/(\s|;|\}|^|\{)out\+='';/g, "$1").replace(/\+''/g, "").replace(/(\s|;|\}|^|\{)out\+=''\+/g, "$1out+=");
        if (e && b.selfcontained) a = "String.prototype.encodeHTML=(" +
            o.toString() + "());" + a;
        try {
            return new Function(b.varname, a)
        } catch (n) {
            typeof console !== "undefined" && console.log("Could not create a template function: " + a);
            throw n;
        }
    };
    j.compile = function(a, b) {
        return j.template(a, null, b)
    }
})();
/*EOF /Scripts/doT-1.0.1.min.js*/
/*/Scripts/Placeholder.js*/
// HTML5 placeholder plugin version 1.01
// Copyright (c) 2010-The End of Time, Mike Taylor, http://miketaylr.com
// MIT Licensed: http://www.opensource.org/licenses/mit-license.php
//
// Enables cross-browser HTML5 placeholder for inputs, by first testing
// for a native implementation before building one.
//
//
// USAGE:
//$('input[placeholder]').placeholder();

// <input type="text" placeholder="username">
;
(function($) {
    //feature detection
    var hasPlaceholder = 'placeholder' in document.createElement('input');

    //sniffy sniff sniff -- just to give extra left padding for the older
    //graphics for type=email and type=url
    var isOldOpera = $.browser.opera && $.browser.version < 10.5;

    $.fn.placeholder = function(options) {
        //merge in passed in options, if any
        var options = $.extend({}, $.fn.placeholder.defaults, options),
            //cache the original 'left' value, for use by Opera later
            o_left = options.placeholderCSS.left;

        //first test for native placeholder support before continuing
        //feature detection inspired by ye olde jquery 1.4 hawtness, with paul irish
        return (hasPlaceholder) ? this : this.each(function() {
            //TODO: if this element already has a placeholder, exit

            //local vars
            var $this = $(this),
                inputVal = $.trim($this.val()),
                inputWidth = $this.width(),
                inputHeight = $this.height(),
                //grab the inputs id for the <label @for>, or make a new one from the Date
                inputId = (this.id) ? this.id : 'placeholder' + (Math.floor(Math.random() * 1123456789)),
                placeholderText = $this.attr('placeholder'),
                placeholder = $('<label for=' + inputId + '>' + placeholderText + '</label>');

            //stuff in some calculated values into the placeholderCSS object
            options.placeholderCSS['width'] = inputWidth + 'px';
            options.placeholderCSS['height'] = inputHeight + 'px';
            options.placeholderCSS['line-height'] = inputHeight + 'px';
            options.placeholderCSS['color'] = options.color;

            // adjust position of placeholder
            options.placeholderCSS.left = (isOldOpera && (this.type == 'email' || this.type == 'url')) ?
                '11%' : o_left;
            placeholder.css(options.placeholderCSS);
            placeholder.addClass('placeholder');

            //place the placeholder
            $this.wrap(options.inputWrapper);
            $this.attr('id', inputId).after(placeholder);

            //if the input isn't empty
            if (inputVal) {
                placeholder.hide();
            };

            //hide placeholder on focus
            $this.focus(function() {
                if (!$.trim($this.val())) {
                    placeholder.hide();
                };
            });

            //show placeholder if the input is empty
            $this.blur(function() {
                if (!$.trim($this.val())) {
                    placeholder.show();
                };
            });
        });
    };

    //expose defaults
    $.fn.placeholder.defaults = {
        //you can pass in a custom wrapper
        inputWrapper: '<span style="position:relative; display:block;"></span>',

        //more or less just emulating what webkit does here
        //tweak to your hearts content
        placeholderCSS: {
            'position': 'absolute',
            'left': '5px',
            'top': '3px',
            'overflow-x': 'hidden',
            'display': 'block',
            cursor: 'text',
            'white-space': 'nowrap'
        }
    };
})(jQuery);
/*EOF /Scripts/Placeholder.js*/
/*/Scripts/Roomster/Roomster.Enums.js*/
var ServiceTypeEnum = function() {
    return {
        "Undefined": 0,
        "NeedRoom": 1,
        "HaveShare": 2,
        "NeedApartment": 4,
        "HaveApartment": 5
    };
}();
var BookmarkTypeEnum = function() {
    return {
        "Bookmarked": 1,
        "Viewed": 2,
        "Emailed": 3,
    };
}();
var SearchViewEnum = function() {
    return {
        "List": 1,
        "Map": 2,
    };
}();

/*EOF /Scripts/Roomster/Roomster.Enums.js*/
/*/Scripts/Roomster/Roomster.SearchSettings.js*/
Roomster.SearchSettings = function() {
    var serviceType = ServiceTypeEnum.Undefined;
    var pageNumber = 1;
    var pageSize = 1;
    var sortOrder = 0;
    var filters = {};
    var formattedAddress = "";
    var actionPath = "";
    var defaultActionPath = "";
    var asyncActionPath = "";
    var actionPathPrefix = "";

    var keepFiltersOnReset = ['Geo', 'Budget'];

    function getActionPath() {
        return actionPath;
    };

    function setActionPath(value) {
        actionPath = value;
        defaultActionPath = value;
    };

    function getAsyncActionPath() {
        return asyncActionPath;
    };

    function setAsyncActionPath(value) {
        asyncActionPath = value;
    };

    function getActionPathPrefix() {
        return actionPathPrefix;
    };

    function setActionPathPrefix(value) {
        actionPathPrefix = value;
    };

    function changeFilter(filterName, filterValue) {
        filters[filterName] = filterValue;
    };

    function getFilter(filterName) {
        return filters[filterName];
    };

    function getFilters() {
        return filters;
    };

    function setFormattedAddress(formattedText) {
        formattedAddress = formattedText;
    };

    function getFormattedAddress() {
        formattedAddress = formattedAddress.replace('/', ' ');
        return formattedAddress;
    };

    function setPage(pagenumber, pagesize) {
        pageNumber = parseInt(pagenumber);
        if (pagesize) {
            pageSize = parseInt(pagesize);
        }
    };

    function setPageNumber(pagenumber) {
        pageNumber = parseInt(pagenumber);
    };

    function incrementPageNumber() {
        pageNumber++;
    };

    function getPageNumber() {
        return pageNumber;
    };

    function getPageSize() {
        return pageSize;
    };

    function setPageSize(pagesize) {
        return pageSize = parseInt(pagesize);
    };

    function setOrder(sortorder) {
        sortOrder = sortorder;
    };

    function getOrder() {
        return sortOrder;
    };

    function resetFilters() {
        var newFilters = {};
        for (var filter in filters) {
            if ($.inArray(filter, keepFiltersOnReset) != -1) {
                newFilters[filter] = filters[filter];
            }
        }
        filters = newFilters;
    };

    function clearFilters() {
        filters = {};
    };

    function setServiceType(servicetype) {
        serviceType = parseInt(servicetype);
    };

    function getServiceType() {
        return serviceType;
    };

    function getSearchString(skipPageInfo) //"Search/{FormattedAddressText}(optional)/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}",
    {
        var searchString = getServiceType();

        if (formattedAddress) {
            searchString = getFormattedAddress() + '/' + searchString;
        }

        if (skipPageInfo !== true) {
            searchString = searchString + '/' + getPageNumber() + '/' + getPageSize();
        }

        return searchString + '/' + getOrder() + getSearchFiltersString();
    };

    function getSearchFiltersString() {
        var result = "";
        for (var filterName in filters) {
            if (filters[filterName] && filters[filterName] != "") {
                result += '/' + filterName + '--' + filters[filterName];
            }
        }
        return result;
    };

    function parseUrlPrefix(url) {
        var matches = /app\/[\w\d]{1,}/gi.exec(url);
        if (matches != null) {
            actionPathPrefix = "/" + matches[0];
            actionPath = actionPathPrefix + defaultActionPath;
        }
        return matches ? url.replace(matches[0] + "/", "") : url;
    };

    function getBrowserString() //"Search/{text}/{ServiceType}/{SearchText}/{sorting}/{*Filters}",
    {
        return (/\/{1}$/.test(actionPath) ? actionPath : actionPath + '/') + getSearchString(true);
    };

    function getSearchUrl() //"Search/{text}/{ServiceType}/{SearchText}/{sorting}/{*Filters}",
    {
        return '/Search/' + getSearchString(true);
    };

    function parseFormattedText(url) {
        url = parseUrlPrefix(url);
        //gets formatted text and sets it to tore
        var matches = /Search\/(.[^\/]+)\/(\d+)\/(\d+)(\/)*(.+$)*/i.exec(url); //"Search/New York/1/1/Age-19-45/Budget-500-700/", "New York", "1", "1", "/", "Age-19-45/Budget-500-700/"]
        if (matches != null) {
            return matches[1];
        } else {
            return '';
        }
    };

    function parseUrl(url) {
        var formattedText = parseFormattedText(url);
        if (formattedText.length) {
            setFormattedAddress(unescape(decodeURI(formattedText)));
            return url.replace(formattedText + "/", "");
        } else {
            return url;
        }
    };

    function parseSearchUrl(url) {
        //"Search/{ServiceType}/{sorting}/{*Filters}" browser url is different form what we use for data request
        //http://roomster.new/{app}/{appcode}/Search/1/1
        var matches = /Search\/(\d+)\/(\d+)(\/)*(.+$)*/i.exec(parseUrl(url)); //["Search/1/1", "1", "1", "/", "15/1"]
        if (!matches || matches.length < 3) {
            return;
        }
        setServiceType(matches[1]);
        setOrder(matches[2]);
        parseFiltersUrl(matches[4]);
    };

    function parseFullSearchUrl(url) {
        //"Search/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}" browser url is different form what we use for data request
        //http://roomster.new/{app}/{appcode}/Search/1/1/15/1
        var matches = /Search\/(\d+)\/(\d+)\/(\d+)\/(\d+)(\/)*(.+$)*/i.exec(parseUrl(url)); //["Search/1/1/15/1", "1","15","1","1","/" "Age--25--56/Budget--300--1000"]
        if (!matches || matches.length < 5) {
            return;
        }
        setServiceType(matches[1]);
        setPage(matches[2], matches[3]);
        setOrder(matches[4]);
        parseFiltersUrl(matches[6]);
    };

    function parseFiltersUrl(filter) {
        if (!filter)
            return;
        filter = decodeURI(filter);
        filter = filter.split('/');
        for (var pos = 0; pos < filter.length; pos++) {
            var filtermatches = /^([\w\s]+)\--(.*)$/.exec(filter[pos]); //["Age-23-5345", "Age", "23-45"]
            if (!filtermatches || filtermatches.length < 3) {
                continue;
            }
            changeFilter(filtermatches[1], filtermatches[2]);
        }
    };

    function setSearchString(searchString, isFullUrl) {
        if (!isFullUrl || isFullUrl == false) {
            parseSearchUrl(searchString);
        } else {
            parseFullSearchUrl(searchString);
        }
    };
    this.ParseUrlPrefix = parseUrlPrefix;
    this.GetBrowserString = getBrowserString;
    this.ParseFormattedText = parseFormattedText;
    this.SetSearchString = setSearchString;
    this.ChangeFilter = changeFilter;
    this.SetFormattedAddress = setFormattedAddress;
    this.GetFormattedAddress = getFormattedAddress;
    this.SetPage = setPage;
    this.GetPageNumber = getPageNumber;
    this.GetPageSize = getPageSize;
    this.SetPageSize = setPageSize;
    this.SetOrder = setOrder;
    this.GetOrder = getOrder;
    this.ResetFilters = resetFilters;
    this.ClearFilters = clearFilters;
    this.SetServiceType = setServiceType;
    this.GetServiceType = getServiceType;
    this.GetFilter = getFilter;
    this.GetFilters = getFilters;
    this.GetActionPath = getActionPath;
    this.SetActionPath = setActionPath;
    this.GetAsyncActionPath = getAsyncActionPath;
    this.SetAsyncActionPath = setAsyncActionPath;
    this.GetActionPathPrefix = getActionPathPrefix;
    this.SetActionPathPrefix = setActionPathPrefix;
    this.GetSearchString = getSearchString;
    this.IncrementPageNumber = incrementPageNumber;
    this.SetPageNumber = setPageNumber;
    this.GetSearchUrl = getSearchUrl;
};

/*EOF /Scripts/Roomster/Roomster.SearchSettings.js*/
/*/Scripts/Roomster/Roomster.UserActionTracker.js*/
(function(ctx) {
    var LOG_INFO_ATTRIBUTE = "logInfo";
    var LOGINFO = {
        Label: 0,
        Events: 1,
        DataFunction: 2
    };
    var LOG_PATH_DELIMETER = '/';

    ctx.UserActionTracker = ctx.UserActionTracker || $.extend({}, {
        Init: function(actionUrl) {

        },
        GetLogInfo: function(element) {
            var result = null;

            if (element.hasAttribute && element.hasAttribute(LOG_INFO_ATTRIBUTE)) {
                var logInfo = element.getAttribute(LOG_INFO_ATTRIBUTE);
                var logInfoArray = logInfo.split(',');

                result = {
                    Label: logInfoArray[LOGINFO.Label],
                    Events: (logInfoArray[LOGINFO.Events] || '').split(' '),
                    DataFunction: (logInfoArray[LOGINFO.DataFunction] || ''),
                };
            }

            return result;
        },
        SetLogInfo: function(targetElement, sourceElement) {
            var sourcePath = this.GetPathArray(sourceElement).join(LOG_PATH_DELIMETER);
            $(targetElement).attr(LOG_INFO_ATTRIBUTE, sourcePath);
        },
        GetPathArray: function(element) {
            var result = [];

            do {
                var logInfo = this.GetLogInfo(element);

                if (logInfo) {
                    result.unshift(logInfo.Label);
                }

                element = element.parentElement;

            } while (element != null && element.parentElement != element)

            return result;
        },
        GetPath: function(element) {
            var result = this.GetPathArray(element);

            var resultString = result.join(LOG_PATH_DELIMETER);

            if (result.length > 0) {
                resultString = LOG_PATH_DELIMETER + resultString + LOG_PATH_DELIMETER;
            }

            return resultString;
        },
        Track: function(action, data) {
            $.post(
                $.url.action({
                    controller: 'ActionTracker',
                    action: 'TrackNew',
                    id: $.url.optional
                }), {
                    userAction: action,
                    data: data
                },
                function(result) {}
            );
        },
        TrackEvent: function($event) {
            var target = $event.target;
            var loginfo = this.GetLogInfo(target);

            if (!loginfo) {
                var $closestAncestor = $(target).closest("[" + LOG_INFO_ATTRIBUTE + "*='," + $event.type + "']");
                if ($closestAncestor && $closestAncestor.length > 0) {
                    target = $closestAncestor[0];
                    loginfo = this.GetLogInfo(target);
                }
            }

            if (loginfo && loginfo.Events.indexOf($event.type) != -1) {
                var action = this.GetPath(target) + $event.type;
                this.Track(action, null);
            }
        },
        RegisterEventListeners: function(events, context) {
            $(context || document).on(events, function(event) {
                Roomster.UserActionTracker.TrackEvent(event);
            });
        }
    });

    $(function() {
        Roomster.UserActionTracker.RegisterEventListeners(
            'click'
            //            "blur focus focusin focusout load resize scroll unload click " +
            //            "dblclick mousedown mouseup mousemove mouseover mouseout mouseenter " +
            //            "mouseleave change select submit keydown keypress keyup error"
        );
    });
})(Roomster);

/*EOF /Scripts/Roomster/Roomster.UserActionTracker.js*/
/*/Scripts/Roomster/Roomster.Images.js*/
$.extend(Roomster.Images, {
    DefaultImage: function(img, size, servicetype) {
        if (!img || !size)
            return;
        img.src = Roomster.Images.GetHost(img.src) + Roomster.Images.DefaultImagePath(size, servicetype);
    },
    DefaultImagePath: function(size, servicetype) {
        if (servicetype == 2 || servicetype == 5) {
            return "/Content/images/placeholders/no_house_" + size + ".gif";
        } else {
            return "/Content/images/placeholders/no_user_" + size + ".gif";
        }
    },
    GetHost: function(url) {
        var link = document.createElement("a");
        link.href = url;
        return "//" + link.host;
    }
});
/*EOF /Scripts/Roomster/Roomster.Images.js*/
/*/Scripts/Roomster/Roomster.Common.js*/
;
Roomster.Common = {
    GetUrlParameterByName: function(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
    },
    IsMobile: {
        ui: function() {
            return (window.isMobileUI ? true : false);
        },
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (Roomster.Common.IsMobile.Android() || Roomster.Common.IsMobile.BlackBerry() || Roomster.Common.IsMobile.iOS() || Roomster.Common.IsMobile.Opera() || Roomster.Common.IsMobile.Windows());
        },
        anyAll: function() {
            return (function(a) {
                return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)));
            })(navigator.userAgent || navigator.vendor || window.opera);
        }
    },
    Log: function(info) {
        console.log(info);
    },
    IsFramed: function() {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    },
    ApiUrl: null,
    CurrentUserId: null,
    GetParameterByName: function(name, source) {
        name = name.toLowerCase();
        source = source.toLowerCase();

        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");

        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(source);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },
};
/*EOF /Scripts/Roomster/Roomster.Common.js*/
/*/Scripts/Roomster/Roomster.User.js*/
(function(ctx) {
    $.extend(
        ctx.User = ctx.User || {}, {
            IsAuthenticated: function() {
                return $('.userMenu').length != 0;
            },
            IsFullAllowed: function() {
                return this.IsAuthenticated() && $('.fullMemberButton').length != 0;
            },
            GetImageUrl: function() {
                return $('span.userImageMenu > img').attr('src');
            }
        });
})(Roomster);

/*EOF /Scripts/Roomster/Roomster.User.js*/
/*/Scripts/Roomster/Roomster.Dialog.js*/
$.extend(Roomster.Dialog, {
    ShareListing: function(listingId) {
        var listingData = Roomster.Profile.UI.GetListingData(listingId);
        if (!listingData)
            return;

        Roomster.InitShareLinksDialog(listingData);

        var dlg = $("#sharelistingpopup");
        dlg.dialog({
            resizable: false,
            closeOnEscape: true,
            width: 691,
            modal: true,
            draggable: false,
            dialogClass: "commonDialog",
        });
    },
    CreditScore: function(isMobile) {
        Roomster.Cookie.CreditScore = new Roomster.Cookie.Base('CreditScoreCookie', 10);

        if (Roomster.Cookie.CreditScore.val() == "true") return;

        Roomster.Cookie.CreditScore.val(true);
        var dlg = $("#creditscorepopup");

        if (!dlg.length) {
            $(window.document.body).append("<div id='creditscorepopup' data-role='popup' style='diplay:none;'></div>");
            dlg = $("#creditscorepopup");
        }
        Roomster.Ads.GetCreditScore(function(html) {
            dlg.html(html);
            if (!isMobile) {
                dlg.dialog({
                    resizable: false,
                    width: 605,
                    dialogClass: "commonDialog creditScoreDialog",
                    modal: true,
                    draggable: false
                });
            } else {
                dlg.popup();
                dlg.popup("open");
            }
        });
    },
    Confirm: function(selector, href, async, clbk, formSelector, appendToSelector) {
        async = async === true;
        var dlg = $(selector).dialog({
            dialogClass: "removeAptPopupHolder",
            resizable: false,
            width: 211,
            minHeight: 0,
            position: {
                my: "bottom",
                at: "top",
                of: appendToSelector
            },
            buttons: [{
                text: Roomster.Resources.GetTranslation("Cancel"),
                click: function() {
                    $(this).dialog("close");
                }
            }, {
                text: Roomster.Resources.GetTranslation("Delete"),
                click: function() {
                    if (async)
                        $.ajax({
                            url: href,
                            success: clbk,
                            type: "POST"
                        });
                    else {
                        var frm = $(formSelector);
                        console.log(frm);
                        frm.attr("action", href);
                        frm.submit();
                    }
                }
            }]
        });
        //.parent().appendTo(appendToSelector);
        return false;
    },
    ConfirmDelete: function(selector, href, async, clbk, formSelector, appendToSelector) {
        async = async === true;
        var dlg = $(selector).dialog({
            dialogClass: "commonDialog listingDeletePopupContainer",
            resizable: false,
            width: 400,
            minHeight: 0,
            /*position: {
                my: "bottom",
                at: "top",
                of: appendToSelector
            },*/
            buttons: [{
                text: Roomster.Resources.GetTranslation("Cancel"),
                click: function() {
                    $(this).dialog("close");
                }
            }, {
                text: Roomster.Resources.GetTranslation("Delete"),
                click: function() {
                    if (async)
                        $.ajax({
                            url: href,
                            success: clbk,
                            type: "POST"
                        });
                    else {
                        var frm = $(formSelector);
                        console.log(frm);
                        frm.attr("action", href);
                        frm.submit();
                    }
                }
            }],
        });
        //.parent().appendTo(appendToSelector);
        return true;
    },
    Phone: {
        Hide: function() {
            var dlg = $("#phonetooltip");
            if (dlg.length) {
                dlg.dialog("close");
            }

            document.removeEventListener("click", Roomster.Dialog.Phone.documentClickHandler, true);

        },
        ignoreToggle: false,
        documentClickHandler: function(nativeEvent) {
            var dlg = $("#phonetooltip");
            var target = (nativeEvent.target) ? nativeEvent.target : nativeEvent.srcElement;
            var element = $(target);
            if (!element.is(dlg.children())) {
                Roomster.Dialog.Phone.Hide();
                Roomster.Dialog.Phone.ignoreToggle = true;
                setTimeout(function() {
                    Roomster.Dialog.Phone.ignoreToggle = false;
                }, 10);
            }
        },
        IsOpen: function() {
            var result = false;
            var dlg = $("#phonetooltip");
            try {
                result = dlg.dialog("isOpen");
            } catch (error) {
                // "isOpen" fire exception on not created dialog;
            }
            return result;
        },
        Toggle: function(element) {
            if (Roomster.Dialog.Phone.ignoreToggle) {
                return;
            }

            var title = '';

            if ($(element).attr("title") || $(element).attr("title") !== '')
                title = $(element).attr("title");

            if (!title || title == '')
                title = $(element).data('value');

            console.log(title);

            var dlg = $("#phonetooltip");

            if (!dlg.length) {
                $(window.document.body).append("<div id='phonetooltip' style='diplay:none;' dir='ltr'></div>");
                dlg = $("#phonetooltip");

                dlg.click(function(event) {
                    event.stopPropagation();
                });
            }

            if (Roomster.Dialog.Phone.IsOpen()) {
                Roomster.Dialog.Phone.Hide();
                return;
            }

            //if (!($(element).attr("title") || $(element).attr("title")==''))
            //    return;

            if (title == '')
                return;

            dlg.dialog({
                resizable: false,
                closeOnEscape: false,
                dialogClass: "userPhonePopup",
                width: 'auto',
                height: 34,
                open: function(event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog || ui).hide();
                    if (Roomster.Common.IsMobile.any())
                        dlg.html('<a href="tel:' + title + '">' + title + '</a>' + '<div class="tooltipPointer"></div>');
                    else
                        dlg.html('<a >' + title + '</a>' + '<div class="tooltipPointer"></div>');

                    setTimeout(function() {
                        // JQuery always use bubbling, but we ned capturing
                        document.addEventListener("click", Roomster.Dialog.Phone.documentClickHandler, true);
                    }, 50);
                },
                position: {
                    my: "left bottom",
                    at: "left-38 top-10",
                    of: $(element)
                }
            });

        }
    },
    MovingQuotes: function() {
        if (!Roomster.User.IsAuthenticated()) {
            this.Login.Show();
            return;
        }
        var dlg = $("#movingquotesdialog");
        if (!dlg[0]) {
            $(window.document.body).append("<div id='movingquotesdialog' style='diplay:none;'></div>");
            dlg = $("#movingquotesdialog");
        }
        var sumbitHandler = function(event) {
            event.preventDefault();
            if ($(this).validate())
                Roomster.Ads.SendMovingQuotes($(this).serialize(), function(data) {
                    dlg.html(data);
                    subscribeOnSubmit();
                });
        };
        var subscribeOnSubmit = function() {
            $("form", dlg).on("submit", sumbitHandler);
        };
        Roomster.Ads.GetMovingQuotes(function(html) {
            dlg.html(html);
            subscribeOnSubmit();
            dlg.dialog({
                resizable: false,
                closeOnEscape: false,
                modal: true,
                dialogClass: "commonDialog",
                width: 605
            });
        });
    },
    Login: {
        Show: function() {
            var dlg = $('#loginPopup');
            dlg.dialog({
                width: Roomster.Common.IsMobile.any() ? '95%' : '409',
                height: 'auto',
                dialogClass: "commonDialog",
                modal: true,
                resizable: false,
                draggable: false
            });
        },
        LoginWithProvider: function(provider) {
            Roomster.SocialConnector.LoginWithProvider(provider);
            this.Close();
        },
        LinkToProvider: function(provider) {
            Roomster.SocialConnector.LinkToProvider(provider);
            this.Close();
        },
        Close: function() {
            $('#loginPopup').dialog("close");
        }
    },
    Languages: function() {
        var dlg = $('#languages');
        dlg.dialog({
            width: 300,
            height: 'auto',
            dialogClass: "commonDialog",
            modal: true,
            resizable: false,
            draggable: false
        });
    },
    Currency: function() {
        var dlg = $('#currencies');
        dlg.dialog({
            width: 'auto',
            height: 'auto',
            dialogClass: "commonDialog",
            modal: true,
            resizable: false,
            draggable: false
        });
    }
});
/*EOF /Scripts/Roomster/Roomster.Dialog.js*/
/*/Scripts/Roomster/Roomster.Dialog.AccessPopup.js*/
$.extend(Roomster.Dialog, {
    AccessPopup: {
        _templateName: 'Roomster_Dialog_AccessPopup',
        LoadTemplates: function() {
            Roomster.Templates.LoadTemplates([{
                name: this._templateName,
                url: '/Content/Templates/' + this._templateName + '.html'
            }]);
        },
        Show: function(userId, iconClass, text) {
            var templateName = this._templateName;
            $.get($.url.action({
                controller: "UserProfile",
                action: "GetUserForAccessDialog",
                id: userId
            })).fail(Roomster.Common.Log).done(function(data) {
                var popupbox = $('.contactPopupBox');
                if (popupbox.length == 0) {
                    $(document.body).append('<div class="contactPopupBox"></div>');
                    popupbox = $('.contactPopupBox');
                }

                data.IconClass = iconClass;
                data.ViaText = text;

                popupbox.html(Roomster.Templates.Apply(templateName, data));
                popupbox.dialog({
                    width: '603',
                    height: 'auto',
                    dialogClass: "commonDialog",
                    modal: true,
                    resizable: false,
                    draggable: false
                });
            });
        }
    }
});

$(function() {
    Roomster.Dialog.AccessPopup.LoadTemplates();
});
/*EOF /Scripts/Roomster/Roomster.Dialog.AccessPopup.js*/
/*/Scripts/Roomster/Roomster.Dialog.SignUp.js*/
$.extend(Roomster.Dialog, {
    SignUp: {
        _templateName: 'Roomster_Dialog_SignUp',
        LoadTemplates: function() {
            Roomster.Templates.LoadTemplates([{
                name: this._templateName,
                url: '/Content/Templates/' + this._templateName + '.html'
            }]);
        },
        Show: function() {
            var popupbox = $('#signupDialog');
            if (popupbox.length == 0) {
                $(document.body).append('<div id ="signupDialog" class="signUpPopupBox"></div>');
                popupbox = $('#signupDialog');
                popupbox.html(Roomster.Templates.Apply(this._templateName));
            }
            popupbox.dialog({
                width: Roomster.Common.IsMobile.any() ? '95%' : '410',
                height: 'auto',
                dialogClass: "commonDialog",
                modal: true,
                resizable: false,
                draggable: false
            });
        },
        LoginWithProvider: function(provider) {
            Roomster.SocialConnector.LoginWithProvider(provider);
            this.Close();
        },
        Close: function() {
            $('#signupDialog').dialog("close");
        },
    }
});

$(function() {
    Roomster.Dialog.SignUp.LoadTemplates();
});
/*EOF /Scripts/Roomster/Roomster.Dialog.SignUp.js*/
/*/Scripts/Roomster/Roomster.Listing.js*/
$.extend(Roomster.Listing, {
    ToggleListingState: function(listingId, activate) {
        var url = $.url.action({
            controller: 'Listing',
            action: 'ManageListingState',
            id: listingId
        }) + "/" + activate;
        $.post(url);
    },
    GetListingProfileViewById: function(listingId, successCallback, errorCallback) {
        var url = $.url.action({
            controller: 'Listing',
            action: 'GetListingProfile',
            id: listingId
        });
        $.get(url).done(successCallback).fail(errorCallback);
    },
    GetNextListingProfileView: function(searchString, successCallback, errorCallback) {
        var url = $.url.action({
            controller: 'Listing',
            action: 'GetNextListingProfile',
            id: $.url.optional
        }) + searchString + "?" + new Date().getTime();
        $.get(url).done(successCallback).fail(errorCallback);
    },
    GetListings: function(listingIds, successCallback, errorCallback) {
        $.post("/Listing/GetListings", $.param({
            ids: listingIds
        }, true)).done(successCallback).fail(errorCallback); //param turns on "traditional" mode to pass arrays
    },
});
/*EOF /Scripts/Roomster/Roomster.Listing.js*/
/*/Scripts/Roomster/Roomster.Profile.js*/
var _roomsterProfileMain = {
    IsAuthenticated: false,
    DEFAULT_PAGE_SIZE: 15,
    SearchSettings: new Roomster.SearchSettings(),
    _doServerAction: function(ajaxRqst) {
        ajaxRqst.data = JSON.stringify(ajaxRqst.data);

        $.extend(ajaxRqst, {
            traditional: true,
            type: 'POST',
            datatype: 'json',
            contentType: 'application/json; charset=UTF-8',
        });

        $.ajax(ajaxRqst);
    },
    GetUserProfileView: function(userProfileId, successCallback, errorCallback) {
        var url = $.url.action({
            controller: 'UserProfile',
            action: 'GetUserProfile',
            id: userProfileId
        }) + '?' + new Date().getTime();
        $.get(url).done(successCallback).fail(errorCallback);
    },

    GetUserListingsView: function(userId, successCallback, errorCallback) {
        $.get("/Listing/GetUserListings?userId=" + userId).done(successCallback).fail(errorCallback);
    },

    GetUserAvatar: function(successCallback, errorCallback) {
        $.get("/UserProfile/Avatar").done(successCallback).fail(errorCallback);
    },

    IsNew: function(updatedDate) {
        updatedDate = Roomster.DateTime.Convert(updatedDate);
        if (!updatedDate)
            return false;
        return (Roomster.DateTime.ToDays(new Date()) - Roomster.DateTime.ToDays(updatedDate)) <= 30;
    },
    SaveAnswersForListingQuestions: function(listingId) {
        Roomster.Profile.UI.HideListingQuestionsMessages(listingId);

        this._doServerAction({
            url: '/Listing/AnswerQuestions',
            data: {
                ListingId: listingId,
                Questions: Roomster.Profile.UI.GetAnswersForListingQuestions(listingId)
            },
            success: function() {
                Roomster.Profile.UI.ShowListingQuestionsSaveResult(listingId, 200);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Roomster.Profile.UI.ShowListingQuestionsSaveResult(listingId, jqXHR.status);
            }
        });
    },
    SaveReviewForUserProfile: function(userId, review, successCallback) {
        if (review != null) {
            this._doServerAction({
                url: '/UserProfile/AddReview',
                data: review,
                success: function(result) {
                    successCallback && successCallback(result);
                }
            });
        }
    },
    ReportUser: function(data, success, error) {
        this._doServerAction({
            url: '/UserProfile/ReportUser',
            data: data,
            success: success,
            error: error
        });
    },
    Init: function() {
        $.extend(Roomster.Profile, this);
        this.SearchSettings.SetPageSize(this.DEFAULT_PAGE_SIZE);
        this.SearchSettings.SetActionPath('/GetNextListingProfile/');
    }
};

Roomster.WaitJquery(
    function() {
        _roomsterProfileMain.Init();
    });

/*EOF /Scripts/Roomster/Roomster.Profile.js*/
/*/Scripts/Roomster/Roomster.Profile.UI.Params.js*/
var _roomsterProfileUIParams = {
    IsMobile: false,
    ProfileTemplates: {
        BaseUrl: '/Content/ProfileTemplates/',
        TemplateNames: {
            Container: 'ProfileContainer',
            Header: 'UserProfileHeader',
            UserProfileView: 'UserProfile',
            Tabs: 'Tabs',
            Listings: 'Listings',
            Questions: 'Questions',
            StreetView: 'StreetView',
            MapView: 'MapView',
            TabRoommateAbout: 'RoommateAbout',
            UserProfileAbout: 'UserProfileAbout',
            TabPhotoAbout: 'PhotoAbout',
            RoomholderListingFooter: 'RoomholderListingFooter',
            AppartmentListingFooter: 'AppartmentListingFooter',
            UserProfileVideo: 'UserProfileVideo',
            TenantAbout: 'TenantAbout',
            Budget: "BudgetTemplate",
            Rate: "RateTemplate",
            Reviews: "Reviews",
            ReviewListItem: "ReviewListItem",
            ContactPopupTemplate: "ContactPopupTemplate",
            ReportUser: "ReportUserTemplate",
            SendMessageToUserTemplate: "UserSendMessageTemplate",
            ShareListing: "ShareListingTemplateMobile",
        },
    },
    ListingTemplates: {
        BaseUrl: '/Content/ProfileTemplates/ListingTemplates/',
        TemplateNames: {
            1: 'ListingItemTempl',
            2: 'ListingItemTempl',
            4: 'ListingItemTemplTenants',
            5: 'ListingItemTemplApartments'
        }
    },
    SearchTemplates: {
        BaseUrl: '/Content/SearchTemplates/',
        TemplateNames: {
            SendMessageTemplate: "SendMessageTemplate",
        }
    }
};

$.extend(Roomster.Profile.UI, _roomsterProfileUIParams);
/*EOF /Scripts/Roomster/Roomster.Profile.UI.Params.js*/
/*/Scripts/Roomster/Roomster.Profile.UI.js*/
function tabData() {
    this.Title = '';
    this.Counter = '';
    this.HtmlContent = '';
}

function profileListings() {
    this.NeedRoomListings = new Array();
    this.HaveRoomListings = new Array();
    this.HaveAppartmentListings = new Array();
    this.NeedAppartmentListings = new Array();
}

$.extend(Roomster.Profile.UI, {
    //ProfileTemplates: {
    //    BaseUrl: '/Content/ProfileTemplates/',
    //    TemplateNames:
    //    {
    //        Container: 'ProfileContainer',
    //        Header: 'UserProfileHeader',
    //        UserProfileView: 'UserProfile',
    //        Tabs: 'Tabs',
    //        Listings: 'Listings',
    //        Questions: 'Questions',
    //        StreetView: 'StreetView',
    //        MapView: 'MapView',
    //        TabRoommateAbout: 'RoommateAbout',
    //        UserProfileAbout: 'UserProfileAbout',
    //        TabPhotoAbout: 'PhotoAbout',
    //        RoomholderListingFooter: 'RoomholderListingFooter',
    //        AppartmentListingFooter: 'AppartmentListingFooter',
    //        UserProfileVideo: 'UserProfileVideo',
    //        TenantAbout: 'TenantAbout',
    //        Budget: "BudgetTemplate",
    //        Rate: "RateTemplate",
    //        Reviews: "Reviews",
    //        ReviewListItem: "ReviewListItem",
    //        ContactPopupTemplate: "ContactPopupTemplate",
    //        ReportUser: "ReportUserTemplate",
    //        SendMessageToUserTemplate: "UserSendMessageTemplate"
    //    },
    //},
    //ListingTemplates: {
    //    BaseUrl: '/Content/ProfileTemplates/ListingTemplates/',
    //    TemplateNames: { 1: 'ListingItemTempl', 2: 'ListingItemTempl', 4: 'ListingItemTemplTenants', 5: 'ListingItemTemplApartments' }
    //},
    //SearchTemplates: {
    //    BaseUrl: '/Content/SearchTemplates/',
    //    TemplateNames: { SendMessageTemplate: "SendMessageTemplate", }
    //},
    CanLoadNext: false,
    ProfileBlockSelector: "#pnProfiles",
    InProgress: false,
    CurrentUserId: 0,
    CurrentUserName: '',
    CurrentUserPhoto: '',
    ServiceType: ServiceTypeEnum.Undefined,
    NextListings: [],
    DisplayedListings: [],
    Initialized: false,
    ShareLinkFacebook: null,
    ShareLinkGoogle: null,
    ShareLinkLinkedIn: null,
    ShareLinkTwitter: null,
    Init: function() {
        if (this.Initialized === true)
            return;

        Roomster.Proxy("_SuccessLoadNextCallback", "_ErrorCallback", "_SuccessProfileCallback", "_GetUserListingsViewSuccessCallback", "_GetUserListingsViewMobileSuccessCallback", "OnScroll", this);
        Roomster.Resources.Load();

        this.ProfileTemplates = Roomster.Profile.UI.ProfileTemplates;
        this.ListingTemplates = Roomster.Profile.UI.ListingTemplates;
        this.SearchTemplates = Roomster.Profile.UI.SearchTemplates;

        var templates = [];

        function prepareTemplates(config) {
            for (var name in config.TemplateNames) {
                templates.push({
                    name: config.TemplateNames[name],
                    url: config.BaseUrl + config.TemplateNames[name] + '.html'
                });
            }
        }

        prepareTemplates(this.ProfileTemplates);
        prepareTemplates(this.ListingTemplates);
        prepareTemplates(this.SearchTemplates);

        Roomster.Templates.LoadTemplates(templates);

        var fromParameter = $.getParam("from");
        if (fromParameter) {
            Roomster.Profile.SearchSettings.SetSearchString(fromParameter);
            Roomster.Search.SearchSettings.SetSearchString(fromParameter);
            this.CanLoadNext = true;
        }
        var nextListingsParameter = $.getParam("nextListings");
        if (nextListingsParameter) {
            var ids = nextListingsParameter.split(",");
            for (var i = 0; i < ids.length; i++) {
                if (ids[i]) {
                    var value = parseInt(ids[i]);
                    //if (this.NextListings.indexOf(value) == -1)
                    if ($.inArray(value, this.NextListings) == -1) {
                        this.NextListings[i] = value;
                    }
                }
            }
        }
        this.Initialized = true;
    },
    GetListingData: function(listingId) {
        return this.DisplayedListings[listingId];
    },
    GetProfileBlock: function() {
        return $(this.ProfileBlockSelector);
    },
    ShowUserProfile: function(profileData) {
        Roomster.Profile.IsAuthenticated = profileData.CurrentUserId > 0 ? true : false;

        Roomster.Profile.UI.CurrentUserId = profileData.CurrentUserId;
        Roomster.Profile.UI.CurrentUserName = profileData.CurrentUserName;
        Roomster.Profile.UI.CurrentUserPhoto = profileData.CurrentUserPhoto;

        var listingTemplates = {
            UserProfile: "",
            Header: "",
            Tabs: ""
        };

        var userProfileData = profileData.UserProfile;
        userProfileData.Photo = userProfileData.Photos.length > 0 ? profileData.MiddleImagePath + userProfileData.Photos[0].FileName : '',
            userProfileData.Sex = Roomster.Resources.GetByValue('Sex', userProfileData.Sex);
        userProfileData.Age = Roomster.DateTime.Age(userProfileData.Birthday);
        userProfileData.LogIn = userProfileData.LastLogin, // Need for SendMessageTemplate
            userProfileData.UserName = userProfileData.FirstName + (userProfileData.LastName ? ' ' + userProfileData.LastName : '');

        listingTemplates.Header = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Header, userProfileData);

        Roomster.Profile.UI.SetMainUserProfilePhotoFirst(userProfileData.Photos);

        userProfileData.MiddleImagePath = profileData.MiddleImagePath;

        userProfileData.ShowExtended = true;

        listingTemplates.UserProfile = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.UserProfileView, profileData);

        if (userProfileData.AboutMe == null) {
            userProfileData.AboutMe = '';
        }

        var aboutTab = new tabData();
        aboutTab.Title = "About";
        aboutTab.HtmlContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.UserProfileAbout, userProfileData);

        var reviewsListHtml = '';
        for (var counter = 0, len = userProfileData.Reviews.length; counter < len; counter++) {
            reviewsListHtml += Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.ReviewListItem, userProfileData.Reviews[counter]);
        }

        var reviewsTab = new tabData();
        reviewsTab.Title = "Reviews";
        reviewsTab.Counter = userProfileData.Reviews.length;
        reviewsTab.HtmlContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Reviews, {
            Id: userProfileData.Id,
            Reviews: reviewsListHtml
        });

        var videoTab = new tabData();
        videoTab.Title = "Videos";
        videoTab.Counter = userProfileData.Videos.length;
        videoTab.HtmlContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.UserProfileVideo, userProfileData);

        var listings = new profileListings();
        listings.NeedRoomListings = profileData.NeedRoomListings;
        listings.HaveRoomListings = profileData.HaveRoomListings;
        listings.HaveAppartmentListings = profileData.HaveAppartmentListings;
        listings.NeedAppartmentListings = profileData.NeedAppartmentListings;

        //listingsTab.HtmlContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Listings, listings);

        var tabs = new Array();

        if (aboutTab != undefined) {
            tabs.push(aboutTab);
        }
        tabs.push(reviewsTab);
        tabs.push(videoTab);

        if (profileData.ListingsCount > 0) {
            var listingsTab = new tabData();
            listingsTab.Title = "Listings";
            listingsTab.Counter = profileData.ListingsCount;
            tabs.push(listingsTab);
        }

        if (Roomster.Profile.UI.IsMobile) {
            listingTemplates.Footer = aboutTab;
            listingTemplates.PhotoAbout = listingTemplates.UserProfile;
            listingTemplates.UserProfile = "";
        } else {
            listingTemplates.Tabs = Roomster.Profile.UI.ApplyTabsContainer({
                Id: userProfileData.Id,
                UserId: profileData.UserProfile.Id,
                Tabs: tabs
            });
        }

        listingTemplates.IsListing = false;
        listingTemplates.IsOwnUserProfile = profileData.IsOwnUserProfile;
        listingTemplates.UserProfileData = userProfileData;
        listingTemplates.UserId = userProfileData.Id;
        listingTemplates.Id = userProfileData.Id;
        this.GetProfileBlock().append(Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Container, listingTemplates));

        if (profileData.ListingsCount > 0) {
            if (!Roomster.Profile.UI.IsMobile) {
                var profileTabId = "#profile" + userProfileData.Id;
                $(profileTabId).on("tabsactivate", function(event, ui) {
                    //Determine that selected tab is listing tab and it is empty. Shoul be refactored later.
                    if (ui.newPanel.children().length == 0) {
                        var userId = ui.newPanel.parent().attr("userid");
                        Roomster.Profile.GetUserListingsView(userId, Roomster.Profile.UI._GetUserListingsViewSuccessCallback, Roomster.Profile.UI._ErrorCallback);
                    }
                });
            } else {
                //Roomster.Profile.GetUserListingsView(profileData.UserProfile.Id, Roomster.Profile.UI._GetUserListingsViewMobileSuccessCallback, Roomster.Profile.UI._ErrorCallback);

                setTimeout(Roomster.Profile.GetUserListingsView(profileData.UserProfile.Id, function(result) {
                    Roomster.Profile.UI._GetUserListingsViewMobileSuccessCallback(result, profileData.UserProfile);
                }, function() {
                    Roomster.Profile.UI._ErrorCallback();
                }), 10);

            };
        }
    },
    GetListingFromListingProfileData: function(listingProfileData) {
        var listings = listingProfileData.NeedRoomListings.concat(listingProfileData.HaveRoomListings, listingProfileData.HaveAppartmentListings, listingProfileData.NeedAppartmentListings);

        $.grep(listings, function(element) {
            return element.Id == listingProfileData.ListingProfile.ListingId;
        });

        return listings[0];
    },
    ShowListingProfile: function(profileData) {
        Roomster.Profile.IsAuthenticated = profileData.CurrentUserId > 0 ? true : false;
        Roomster.Profile.UI.CurrentUserId = profileData.CurrentUserId;

        var listingProfile = profileData.ListingProfile;
        var userProfileData = profileData.UserProfile;
        var listingTemplates = {
            UserProfile: "",
            Header: "",
            Tabs: "",
            PhotoAbout: "",
            Questions: ""
        };

        userProfileData.MiddleImagePath = profileData.MiddleImagePath;

        Roomster.Profile.UI.SetMainUserProfilePhotoFirst(userProfileData.Photos);

        listingTemplates.ProfileData = profileData;

        var photoPath;

        if (listingProfile.ServiceType == ServiceTypeEnum.HaveApartment || listingProfile.ServiceType == ServiceTypeEnum.HaveShare) {
            photoPath = listingProfile.Photos.length > 0 ? profileData.MiddleImagePath + listingProfile.Photos[0] : '';
        } else {
            photoPath = userProfileData.Photos.length > 0 ? profileData.MiddleImagePath + userProfileData.Photos[0].FileName : '';
        }

        var headerData = {
            Id: listingProfile.ListingId,
            UserId: userProfileData.Id,
            FirstName: userProfileData.FirstName,
            UserName: userProfileData.FirstName + ' ' + userProfileData.LastName,
            Sex: userProfileData.Sex && Roomster.Resources.GetByValue('Sex', userProfileData.Sex) ? Roomster.Resources.GetByValue('Sex', userProfileData.Sex) : "",
            Age: Roomster.DateTime.Age(userProfileData.Birthday),
            Availability: listingProfile.FromDate,
            HomeTown: userProfileData.HomeTown,
            Photo: photoPath,
            Headline: listingProfile.Headline,
            LastLogin: userProfileData.LastLogin,
            LogIn: userProfileData.LastLogin, // Need for SendMessageTemplate
            ListingTags: listingProfile.Tags,
            ServiceType: listingProfile.ServiceType,
            GoogleFormattedAddress: listingProfile.GoogleFormattedAddress,
            FromDate: listingProfile.FromDate,
            SocialConnections: userProfileData.SocialConnections,
            Phone: userProfileData.Phone,
            //CurrentListing: this.GetListingFromListingProfileData(profileData)
        };

        userProfileData.Sex = Roomster.Resources.GetByValue('Sex', userProfileData.Sex);
        profileData.IsListing = true;
        profileData.ServiceType = listingProfile.ServiceType;
        profileData.Id = listingProfile.ListingId;
        listingTemplates.UserProfile = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.UserProfileView, profileData);

        if (listingProfile.ServiceType == ServiceTypeEnum.HaveApartment) {
            headerData.ApartmentType = listingProfile.ListingDetails.Apartment_types[0];
        }

        if (listingProfile.ServiceType == ServiceTypeEnum.NeedRoom) {
            if (Roomster.Profile.UI.IsMobile) {
                listingTemplates.Footer = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.TabRoommateAbout, listingProfile);
                listingTemplates.PhotoAbout = listingTemplates.UserProfile;
                listingTemplates.UserProfile = "";
            } else {
                listingTemplates.Tabs = Roomster.Profile.UI.ShowRoommateProfile(profileData);
            }
            listingTemplates.BudgetContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Budget, listingProfile);
        } else if (listingProfile.ServiceType == ServiceTypeEnum.HaveShare) {
            listingTemplates.Tabs = Roomster.Profile.UI.ShowRoomholderProfile(profileData);
            listingTemplates.BudgetContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Rate, listingProfile);
            listingTemplates.Footer = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.RoomholderListingFooter, listingProfile);
            if (Roomster.Profile.UI.IsMobile) {
                //console.log(profileData);
                profileData.ListingProfile.LogIn = profileData.UserProfile.LastLogin;
                profileData.ListingProfile.ThumbnailImagePath = profileData.ThumbnailImagePath;
                profileData.ListingProfile.UserMainPhoto = profileData.UserProfile.MainPhoto;
                listingTemplates.PhotoAbout = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.TabPhotoAbout, profileData.ListingProfile);
            }
        } else if (listingProfile.ServiceType == ServiceTypeEnum.NeedApartment) {
            if (Roomster.Profile.UI.IsMobile) {
                listingTemplates.Footer = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.TenantAbout, listingProfile);
                listingTemplates.PhotoAbout = listingTemplates.UserProfile;
                listingTemplates.UserProfile = "";
            } else {
                listingTemplates.Tabs = Roomster.Profile.UI.ShowTenantProfile(profileData);
            }
            listingTemplates.BudgetContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Budget, listingProfile);
        } else if (listingProfile.ServiceType == ServiceTypeEnum.HaveApartment) {
            listingTemplates.Tabs = Roomster.Profile.UI.ShowAppartmentProfile(profileData);
            listingTemplates.BudgetContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Rate, listingProfile);
            listingTemplates.Footer = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.AppartmentListingFooter, listingProfile);
            if (Roomster.Profile.UI.IsMobile) {
                profileData.ListingProfile.LogIn = profileData.UserProfile.LastLogin;
                profileData.ListingProfile.ThumbnailImagePath = profileData.ThumbnailImagePath;
                profileData.ListingProfile.UserMainPhoto = profileData.UserProfile.MainPhoto;
                listingTemplates.PhotoAbout = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.TabPhotoAbout, profileData.ListingProfile);
            }
        }

        listingTemplates.Header = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Header, headerData);

        if (Roomster.Profile.UI.IsMobile) {
            listingTemplates.Questions = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Questions, profileData.ListingProfile);
        }

        listingTemplates.IsListing = true;
        listingTemplates.Id = listingProfile.ListingId;
        listingTemplates.UserId = userProfileData.Id;
        listingTemplates.UserProfileData = userProfileData;
        this.GetProfileBlock().append(Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Container, listingTemplates));

        if (profileData.ListingsCount > 1) {
            var profileTabId = "#profile" + profileData.ListingProfile.ListingId;
            $(profileTabId).on("tabsactivate", function(event, ui) {
                //Determine that selected tab is listing tab and it is empty. Shoul be refactored later.
                if (ui.newPanel.children().length == 0) {
                    var userId = ui.newPanel.parent().attr("userid");
                    Roomster.Profile.GetUserListingsView(userId, Roomster.Profile.UI._GetUserListingsViewSuccessCallback, Roomster.Profile.UI._ErrorCallback);
                }
            });
        }

        (function(bookmarkButton, user, profileUI) {
            bookmarkButton.UnbindHandlers('.bookmarkContainer', '.bookmarkInList');

            var handlers = bookmarkButton.GetDefaultHandlers();

            if (!user.IsAuthenticated()) {
                handlers.buttonClick = function($el) {
                    profileUI.OnContactClickHandler(event, window.listingProfileHeaderDataStorage[$el.attr('object-id')]);
                };
            }
            bookmarkButton.BindHandlers('.bookmarkContainer', '.bookmarkInList', handlers);
        })(Roomster.Bookmark.UI.BookmarkButton, Roomster.User, Roomster.Profile.UI);
    },
    ShowRoommateProfile: function(profileData) {
        var listingProfile = profileData.ListingProfile;
        listingProfile.OriginalImagePath = profileData.OriginalImagePath;
        listingProfile.ThumbnailImagePath = profileData.ThumbnailImagePath;
        var tabs = new Array();

        var aboutTab = new tabData();
        aboutTab.Title = "About";
        aboutTab.HtmlContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.TabRoommateAbout, listingProfile);
        tabs.push(aboutTab);

        if (listingProfile.Questions.length > 0) {
            tabs.push(this.GetQuestionsTab(listingProfile));
        }

        this.ApplyListingsTab(profileData, tabs);

        return Roomster.Profile.UI.ApplyTabsContainer({
            Id: listingProfile.ListingId,
            UserId: profileData.UserProfile.Id,
            Tabs: tabs
        });
    },
    ShowRoomholderProfile: function(profileData) {
        var listingProfile = profileData.ListingProfile;
        listingProfile.OriginalImagePath = profileData.OriginalImagePath;
        listingProfile.ThumbnailImagePath = profileData.ThumbnailImagePath;

        var tabs = [];

        if (!Roomster.Profile.UI.IsMobile) {
            var aboutTab = new tabData();
            aboutTab.Title = "About";
            aboutTab.HtmlContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.TabPhotoAbout, listingProfile);
            tabs.push(aboutTab);
        }

        var mapViewData = {
            Latitude: listingProfile.Latitude,
            Longitude: listingProfile.Longitude,
            ProfileId: listingProfile.ListingId,
            GoogleFormattedAddress: listingProfile.GoogleFormattedAddress,
            TabIndex: (Roomster.Profile.UI.IsMobile ? 0 : 1)
        };
        var mapTab = new tabData();
        mapTab.Title = "Map";
        mapTab.HtmlContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.MapView, mapViewData);
        tabs.push(mapTab);

        var streetViewData = {
            Latitude: listingProfile.Latitude,
            Longitude: listingProfile.Longitude,
            ProfileId: listingProfile.ListingId,
            TabIndex: (Roomster.Profile.UI.IsMobile ? 1 : 2)
        };
        var streetTab = new tabData();
        streetTab.Title = "Street View";
        streetTab.HtmlContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.StreetView, streetViewData);
        tabs.push(streetTab);

        if (!Roomster.Profile.UI.IsMobile) {
            if (listingProfile.Questions.length > 0) {
                tabs.push(this.GetQuestionsTab(listingProfile));
            }

            this.ApplyListingsTab(profileData, tabs);
        }
        return Roomster.Profile.UI.ApplyTabsContainer({
            Id: listingProfile.ListingId,
            UserId: profileData.UserProfile.Id,
            Tabs: tabs
        });
    },
    ShowTenantProfile: function(profileData) {
        var listingProfile = profileData.ListingProfile;
        listingProfile.OriginalImagePath = profileData.OriginalImagePath;
        listingProfile.ThumbnailImagePath = profileData.ThumbnailImagePath;
        var tabs = new Array();

        var aboutTab = new tabData();
        aboutTab.Title = "About";
        aboutTab.HtmlContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.TenantAbout, listingProfile);

        tabs.push(aboutTab);
        this.ApplyListingsTab(profileData, tabs);

        return Roomster.Profile.UI.ApplyTabsContainer({
            Id: listingProfile.ListingId,
            UserId: profileData.UserProfile.Id,
            Tabs: tabs
        });
    },
    ShowAppartmentProfile: function(profileData) {
        var listingProfile = profileData.ListingProfile;
        listingProfile.OriginalImagePath = profileData.OriginalImagePath;
        listingProfile.ThumbnailImagePath = profileData.ThumbnailImagePath;
        var tabs = new Array();

        if (!Roomster.Profile.UI.IsMobile) {
            var aboutTab = new tabData();
            aboutTab.Title = "About";
            aboutTab.HtmlContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.TabPhotoAbout, listingProfile);
            tabs.push(aboutTab);
        }
        var mapViewData = {
            Latitude: listingProfile.Latitude,
            Longitude: listingProfile.Longitude,
            ProfileId: listingProfile.ListingId,
            GoogleFormattedAddress: listingProfile.GoogleFormattedAddress,
            TabIndex: (Roomster.Profile.UI.IsMobile ? 0 : 1)
        };
        var mapTab = new tabData();
        mapTab.Title = "Map";
        mapTab.HtmlContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.MapView, mapViewData);
        tabs.push(mapTab);

        var streetViewData = {
            Latitude: listingProfile.Latitude,
            Longitude: listingProfile.Longitude,
            ProfileId: listingProfile.ListingId,
            TabIndex: (Roomster.Profile.UI.IsMobile ? 1 : 2)
        };
        var streetTab = new tabData();
        streetTab.Title = "Street View";
        streetTab.HtmlContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.StreetView, streetViewData);
        tabs.push(streetTab);

        if (!Roomster.Profile.UI.IsMobile) {
            this.ApplyListingsTab(profileData, tabs);
        }
        return Roomster.Profile.UI.ApplyTabsContainer({
            Id: listingProfile.ListingId,
            UserId: profileData.UserProfile.Id,
            Tabs: tabs
        });
    },
    GetQuestionsTab: function(listingProfile) {
        var questionsTab = new tabData();
        questionsTab.Title = "Questions";
        questionsTab.HtmlContent = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Questions, listingProfile);
        return questionsTab;
    },
    ApplyListingsTab: function(profileData, tabs) {
        if (profileData.ListingsCount > 1) {
            var listingsTab = new tabData();
            listingsTab.Title = "All Listings";
            listingsTab.Counter = profileData.ListingsCount;
            tabs.push(listingsTab);
        }
    },
    _GetUserListingsViewSuccessCallback: function(result) {
        var listings = new profileListings();
        listings.NeedRoomListings = result.NeedRoomListings;
        listings.HaveRoomListings = result.HaveRoomListings;
        listings.HaveAppartmentListings = result.HaveAppartmentListings;
        listings.NeedAppartmentListings = result.NeedAppartmentListings;

        var content = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Listings, listings);
        $(this.GetListingTab(result.UserId)).html(content);

        $('.searchResultModel').redraw(); //necessary to redraw when rendering in RTL mode
    },
    _GetUserListingsViewMobileSuccessCallback: function(result, userProfile) {
        //console.log(userProfile);
        var listings = new profileListings();
        listings.NeedRoomListings = result.NeedRoomListings;
        listings.HaveRoomListings = result.HaveRoomListings;
        listings.HaveAppartmentListings = result.HaveAppartmentListings;
        listings.NeedAppartmentListings = result.NeedAppartmentListings;
        listings.UserProfile = userProfile;

        var content = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Listings, listings);
        $('#user-listings-' + result.UserId).html(content);

        $('.searchResultModel').redraw(); //necessary to redraw when rendering in RTL mode
    },
    GetListingTab: function(userId) {
        var currentElement = null;
        $("div[userid=" + userId + "]").children().each(function() {
            //Determine that selected tab is listing tab and it is empty. Shoul be refactored later.
            if (this.children.length == 0) {
                currentElement = this;
            }
        });
        return currentElement;
    },
    SetMainUserProfilePhotoFirst: function(photos) {
        if (photos != null && photos.length > 0) {
            var primaryPhoto = null;

            for (var photoCounter = 0; photoCounter < photos.length; photoCounter++) {
                if (photos[photoCounter].IsMain === true) {
                    primaryPhoto = photos[photoCounter];
                    photos.splice(photoCounter, 1);
                    break;
                }
            }

            if (primaryPhoto != null) {
                photos.splice(0, 0, primaryPhoto);
            }
        }
    },
    ApplyTabsContainer: function(tabs) {
        var result = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.Tabs, tabs);
        return result;
    },
    OnScroll: function() {
        if (window.location.href.indexOf("Profile") < 0) {
            return;
        }
        if (this.InProgress) {
            return;
        }

        var scrolledHeight = $(window).scrollTop() + $(window).height();
        var triggerHeight = !window.isMobileUI ?
            $(document).height() - $("#mainFooter").height() - $("#pnProfiles .userProfile").last().height() - $("#pnProfiles iframe").last().height() :
            $(document).height() - $(window).height() * 0.3;

        if (scrolledHeight >= triggerHeight) {
            this.LoadNext();
        }
    },
    LoadListingProfile: function(listingId) {
        if (!this.InProgress) {
            //this.ToggleProcess();
            this.ShowProcess();
        }
        Roomster.Listing.GetListingProfileViewById(listingId, this._SuccessProfileCallback, this._ErrorCallback);
    },
    LoadListingProfilePanel: function(listingId) {
        if (!this.InProgress) {
            //this.ToggleProcess();
            this.ShowProcess();
        }
        Roomster.Listing.GetListingProfileViewById(listingId, this._SuccessProfilePanelCallback, this._ErrorCallback);
    },
    LoadUserProfile: function(userId) {
        this.ToggleProcess();
        Roomster.Profile.GetUserProfileView(userId, this._SuccessProfileCallback, this._ErrorCallback);
    },
    LoadNext: function() {
        if (this.NextListings.length > 0) {
            var listingId = this.NextListings[0];
            this.NextListings.splice(0, 1);
            this.LoadListingProfile(listingId);
        } else if (this.CanLoadNext) {
            if (!this.InProgress) {
                //this.ToggleProcess();
                this.ShowProcess();
            }
            Roomster.Profile.SearchSettings.IncrementPageNumber();
            Roomster.Listing.GetNextListingProfileView(Roomster.Profile.SearchSettings.GetSearchString(), this._SuccessLoadNextCallback, this._ErrorCallback);
        } else if (window.isMobileUI) {
            this.HideProcess();
        }
        return;
    },
    _SuccessLoadNextCallback: function(searchResult) {
        if (searchResult.ListingsIsNotAvailiable) {
            this.CanLoadNext = false;
            if (this.InProgress) {
                //this.ToggleProcess();
                this.HideProcess();
            }
            return;
        }

        var resultId = searchResult.ListingProfile.ServiceType > 0 ? searchResult.ListingProfile.ListingId : searchResult.UserProfile.Id;

        this.CanLoadNext = searchResult.NextListings != null && searchResult.NextListings.length > 0;
        for (var i = 0; i < searchResult.NextListings.length; i++) {
            //if (this.NextListings.indexOf(searchResult.NextListings[i]) == -1)
            //if ($.inArray(searchResult.NextListings[i], this.NextListings) == -1)
            if (!this.DisplayedListings[searchResult.NextListings[i]]) {
                this.NextListings.push(searchResult.NextListings[i]);
            }
        }
        if (!this.DisplayedListings[resultId]) {
            this.DisplayedListings[resultId] = searchResult;
            this._SuccessProfileCallback(searchResult);
        } else {
            this.LoadNext();
        }
    },
    _SuccessProfileCallback: function(profileResult) //{UserProfileView,ListingProfileView}
    {
        $(this).trigger("profile.loaded", [this]);

        Roomster.Profile.UI.IsListingProfileContext = profileResult.ListingProfile.ServiceType > 0;

        var resultId = Roomster.Profile.UI.IsListingProfileContext ? profileResult.ListingProfile.ListingId : profileResult.UserProfile.Id;
        Roomster.Profile.UI.DisplayedListings[resultId] = profileResult;

        if (Roomster.Profile.UI.IsListingProfileContext) {
            Roomster.Profile.UI.ShowListingProfile(profileResult);
        } else {
            Roomster.Profile.UI.ShowUserProfile(profileResult);
        }

        $(this).trigger("profile.rendered", this);

        // MOBILE TABS
        if (window.isMobileUI) {
            $('#profileTab1').hide();
            $('.tabsHeader li:first-child').addClass('ui-tabs-active ui-state-active');

            $(".tabsHeader a").click(function(event) {
                event.preventDefault();
                $(this).parent().addClass("ui-tabs-active ui-state-active");
                $(this).parent().siblings().removeClass("ui-tabs-active ui-state-active");
                var tab = $(this).attr("href");
                $(".tab-content").not(tab).css("display", "none");
                $(tab).fadeIn();
            });

        }

        //var adSlot = Roomster.Ads.UI.Slots.Profile[Roomster.Profile.UI.IsListingProfileContext ? profileResult.ListingProfile.ServiceType : 0];
        //this.GetProfileBlock().append(Roomster.Ads.UI.GetAdsHtml(adSlot));

        if (Roomster.Page && Roomster.Page.Helper) {
            Roomster.Page.Helper.SaveCurrentPage();
        }
        if (Roomster.Profile.UI.InProgress) {
            if (!window.isMobileUI || !Roomster.Profile.UI.IsListingProfileContext) {
                Roomster.Profile.UI.ToggleProcess();
            } else {
                $('.loader').addClass('profile');
                Roomster.Profile.UI.InProgress = false;
            }
        }

    },
    _SuccessProfilePanelCallback: function(profileResult) //{UserProfileView,ListingProfileView}
    {
        //$('#pnProfiles').empty();
        //$('#listing-profile-panel').show();
        //$('#listing-profile-panel').popup({
        //    positionTo: "window",
        //    afteropen: function(event, ui)
        //    {
        //        Roomster.Profile.UI._SuccessProfileCallback(profileResult);
        //        $('#listing-profile-panel').css('top', $(document).scrollTop());

        //        $('body').css("overflow", "hidden")/*.on("touchmove", function() { return false; })*/;
        //    },
        //    afterclose: function(event, ui)
        //    {
        //        $('body').css("overflow", "auto")/*.off("touchmove")*/;
        //        $('#listing-profile-panel').hide();
        //    }
        //});
        //$('#listing-profile-panel').popup("open");
        //return;

        $('#pnProfiles').empty();
        $('#listing-profile-content').css('padding-bottom', $('.ui-page-active').css('padding-top'));
        $("header").toolbar("option", "hideDuringFocus", "false");

        $('#listing-profile-panel').show();
        $('#listing-profile-panel').panel({
            open: function(event, ui) {
                Roomster.Profile.UI._SuccessProfileCallback(profileResult);
                $('#listing-profile-panel').enhanceWithin();

                $('body').css("overflow", "hidden") /*.on("touchmove", function() { return false; })*/ ;

                $('.sendMessageTextArea').focus(function(e) {
                    if (navigator.userAgent.match('CriOS')) {
                        $('#listing-profile-content').scrollTop($('#listing-profile-content').prop("scrollHeight"));
                    }

                }).blur(function() {});
            },
            close: function(event, ui) {
                $('body').css("overflow", "auto") /*.off("touchmove")*/ ;
                $("header").toolbar("option", "hideDuringFocus", "input, textarea, select");
                //$('#listing-profile-content').css('padding-bottom', '0');
                $('#listing-profile-panel').hide();
            }
        });
        $('#listing-profile-panel').panel("open");
    },
    _ErrorCallback: function(par1, par2, par3) {
        this.ToggleProcess();
    },
    ToggleProcess: function() {
        if (!window.isMobileUI) {
            $('#loadingIndicator').toggle();
        } else {
            $('.loader').toggle();
        }
        this.InProgress = !this.InProgress;
    },
    ShowProcess: function() {
        if (!window.isMobileUI || this._SearchView === SearchViewEnum.Map) {
            $('#loadingIndicator').show();
        } else {
            $('.loader').show();
        }
        this.InProgress = true;
    },
    HideProcess: function() {
        if (!window.isMobileUI || this._SearchView === SearchViewEnum.Map) {
            $('#loadingIndicator').hide();
        } else {
            $('.loader').hide();
        }
        this.InProgress = false;
    },
    ShowReviewSaveResult: function(result, userId) {
        var reviewContainer = $('.writeReview', '#profile' + userId);

        var reviewListContainer = $('.reviewListContainer', $('.reviewsContainer', '#profile' + userId));

        if (result > 0) {

            var ratingObject = $('.reviewRating', reviewContainer);
            var commentObject = $('.reviewComment', reviewContainer);

            var review = {
                Comment: commentObject.val(),
                Rating: ratingObject.val(),
                ReviewerUserName: Roomster.Profile.UI.CurrentUserName,
                ReviewerUserPhoto: Roomster.Profile.UI.CurrentUserPhoto,
                ReviewerUserId: Roomster.Profile.UI.CurrentUserId
            };

            ratingObject.val('');
            $('.rating', reviewContainer).removeClass('active');
            commentObject.val('');

            var reviewHtml = Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.ReviewListItem, review);
            reviewListContainer.append(reviewHtml);
        }
    },
    SetReviewdRating: function(userId, rating) {
        var reviewsContainer = $('.writeReview', '#profile' + userId);

        if (reviewsContainer.length == 1) {
            var ratingObject = $('.reviewRating', reviewsContainer);

            if (ratingObject.length == 1) {
                ratingObject.val(rating);
            }
        } else {
            //console.log('Reviews container not found');
        }
    },
    GetReviewForUserProfile: function(userId) {
        var reviewsContainer = $('.writeReview', '#profile' + userId);

        if (reviewsContainer.length == 1) {

            var rating = $('.reviewRating', reviewsContainer).val();
            var comment = $('.reviewComment', reviewsContainer).val();

            return {
                UserId: userId,
                Comment: comment,
                Rating: rating
            };
        } else {
            //console.log('Reviews container not found');
        }

        return null;
    },
    HideListingQuestionsMessages: function(listingId) {
        var questionsContainer = $('.questions', '#profile' + listingId);

        if (questionsContainer.length == 1) {
            $('.answersAreSentMessageContainer', questionsContainer).hide();
            $('.notAuthorizeUserMessageContainer', questionsContainer).hide();
        } else {
            //window.console && window.console.log('Questions container not found');
        }
    },
    GetAnswersForListingQuestions: function(listingId) {
        var questionsContainer = $('.questions', '#profile' + listingId);

        if (questionsContainer.length == 1) {
            var questions = $('.question', questionsContainer);

            var result = new Array();

            for (var counter = 0; counter < questions.length; counter++) {
                var questionObject = questions[counter];

                var question = $('input[type="hidden"]', questionObject).val();
                var answer = $('input[type="text"]', questionObject).val();

                if (answer.length > 0) {
                    result.push({
                        key: question,
                        value: answer
                    });
                }
            }

            return result;
        } else {
            //window.console && window.console.log('Questions container not found');
        }

        return null;
    },
    ShowListingQuestionsSaveResult: function(listingId, responseStatus, saveResult) {
        var questionsContainer = $('.questions', '#profile' + listingId);

        var answers = Roomster.Profile.UI.GetAnswersForListingQuestions(listingId);

        if (questionsContainer.length == 1) {
            var questions = $('.question', questionsContainer);
            var answersAreSentMessageContainer = $('.answersAreSentMessageContainer', questionsContainer);
            var allAnswersAreSentMessageContainer = $('.allAnswersAreSentMessageContainer', questionsContainer);
            var oneAnswersAreSentMessageContainer = $('.oneAnswerIsSentMessageContainer', questionsContainer);
            var notAuthorizedUserMessageContainer = $('.notAuthorizeUserMessageContainer', questionsContainer);

            if (responseStatus == 200) {

                for (var counter = 0; counter < questions.length; counter++) {
                    var questionObject = questions[counter];
                    var answerInputObject = $('input[type="text"]', questionObject);

                    if (answerInputObject.val().length > 0) {
                        answerInputObject.val('');
                        $(questionObject).remove();
                    }
                }

                if (answers.length == questions.length) {
                    allAnswersAreSentMessageContainer.show();

                    oneAnswersAreSentMessageContainer.hide();
                    answersAreSentMessageContainer.hide();
                    $('.sendAnswersContainer', questionsContainer).hide();

                    return;
                }

                if (answers.length == 1) {
                    oneAnswersAreSentMessageContainer.show();

                    return;
                }

                answersAreSentMessageContainer.show();

                oneAnswersAreSentMessageContainer.hide();
            } else if (responseStatus == 404 || responseStatus == 401) {
                notAuthorizedUserMessageContainer.show();
                $('.sendAnswersContainer', questionsContainer).hide();
            }
        } else {
            //console.log('Questions container not found');
        }
    },
    GetLocationString: function(location) {
        var locationString = location.Sublocality ? location.Sublocality : (location.City ? location.City : "");
        if (locationString != "") {
            locationString = locationString + ",";
        }
        return locationString + (location.Region ? location.Region + "," : "") + Roomster.Resources.Get('Countries', location.CountryId);
    },
    Back: function() {
        //if (Roomster.Page)
        //{
        window.History.back();
        //}
        //else
        //{
        //    window.location.href = '/Search/' + Roomster.Search.GetSearchString(true);
        //}
    },
    OnLoaded: function(callback) {
        $(this).on("profile.loaded", callback);
    },
    OnRendered: function(callback) {
        $(this).on("profile.rendered", callback);
    },
    OnContactClickHandler: function(nativeEvent, data) {
        var target = $.event.fix(nativeEvent).target;
        var $target = $(target);

        if (!Roomster.User.IsAuthenticated()) {
            Roomster.Dialog.Login.Show();
            return;
        }

        if ($target.hasClass('icoMail')) {
            if (Roomster.Profile.UI.IsListingProfileContext) {
                this.ShowSendMailDlg(data, target);
            } else {
                this.ShowSendMailToUserDlg(data, target);
            }
            return;
        }

        if (!Roomster.User.IsFullAllowed()) {
            var userId = data.CurrentListing ? data.CurrentListing.UserId : data.Id
            var connections = Roomster.Resources.GetValues('SocialConnections');
            var className = "icoPhone";
            var viaText = "Telephone";
            $.each(connections, function(index, value) {
                if ($target.hasClass(value.Name)) {
                    className = value.Name;
                    viaText = value.Name;
                    return false;
                }
            });
            Roomster.Dialog.AccessPopup.Show(userId, className, viaText);
        }
    },
    ShowContactPopup: function(data) {
        var popupbox = $('.contactPopupBox');
        if (popupbox.length == 0) {
            $(document.body).append('<div class="contactPopupBox"></div>');
            popupbox = $('.contactPopupBox');
        }

        popupbox.html(Roomster.Templates.Apply(this.ProfileTemplates.TemplateNames.ContactPopupTemplate, data));
        popupbox.dialog({
            width: 706,
            height: 'auto',
            dialogClass: "commonDialog",
            modal: true,
            resizable: false,
            draggable: false
        });
    },
    ShowMailDlg: function(template, data, dialgPrams, nativeElement) {
        var emailbox = $(".emailbox");
        if (emailbox.length == 0) {
            $(document.body).append("<div class='emailbox'></div>");
            emailbox = $(".emailbox");
            Roomster.UserActionTracker.SetLogInfo(emailbox[0], nativeElement);
        }

        var html = Roomster.Templates.Apply(template, data);
        emailbox.html(html);
        emailbox.dialog(dialgPrams);

    },
    ShowSendMailDlg: function(data, nativeElement) {
        var templateName = this.SearchTemplates.TemplateNames.SendMessageTemplate;

        $.get($.url.action({
            controller: "UserProfile",
            action: "GetUserForAccessDialog",
            id: data.UserId
        })).fail(Roomster.Common.Log).done(function(result) {
            data.UserData = result;
            data.PhotoMedium = data.Photo;
            Roomster.Profile.UI.ShowMailDlg(
                templateName,
                data, {
                    width: 605,
                    height: 'auto',
                    dialogClass: "commonDialog",
                    modal: true,
                    resizable: false,
                    draggable: false,
                    position: ({
                        at: "center center-60"
                    })
                }, nativeElement);
        });

    },
    ShowSendMailToUserDlg: function(data, nativeElement) {
        var templateName = this.ProfileTemplates.TemplateNames.SendMessageToUserTemplate;
        $.get($.url.action({
            controller: "UserProfile",
            action: "GetUserForAccessDialog",
            id: data.Id
        })).fail(Roomster.Common.Log).done(function(result) {
            data.UserData = result;
            Roomster.Profile.UI.ShowMailDlg(
                templateName,
                data, {
                    width: 605,
                    height: 'auto',
                    dialogClass: "commonDialog",
                    modal: true,
                    resizable: false,
                    draggable: false,
                    position: ({
                        at: "center center-60"
                    })
                }, nativeElement);
        });
    },
    ShowReportUserPopup: function(nativeElement, data) {
        var popupbox = $('.reportUserPopup');
        if (popupbox.length == 0) {
            $(document.body).append('<div class="reportUserPopup"></div>');
            popupbox = $('.reportUserPopup');
        }
        popupbox.html(Roomster.Templates.Apply(this.ProfileTemplates.TemplateNames.ReportUser, data));
        popupbox.dialog({
            width: 605,
            height: 'auto',
            dialogClass: "commonDialog",
            modal: true,
            resizable: false,
            draggable: false,
            position: ({
                at: "center center-60"
            })
        });
    },
    ShowReportUser: function(data, selectorToHide) {
        $(selectorToHide).hide();

        var reportUserForm = $('.report-user-form');
        if (reportUserForm.length === 0) {
            $(document.body).append('<div class="report-user-form" style="display:none;"></div>');
            reportUserForm = $('.report-user-form');
        }
        reportUserForm
            .empty()
            .attr('data-selector-to-show', selectorToHide)
            .html(Roomster.Templates.Apply(this.ProfileTemplates.TemplateNames.ReportUser, data))
            .css('min-height', '100%')
            .show();
    },
    HideReportUser: function() {
        $('.report-user-form').hide();
        $($('.report-user-form').data('selector-to-show')).show();
        $('.jcarousel').jcarousel('scroll', '+=0');
    },
    ShowShareListing: function(data, selectorToHide) {
        $(selectorToHide).hide();

        var show = function(listingData) {
            var shareListingForm = $('.share-listing-form');
            if (shareListingForm.length === 0) {
                $('.ui-page-active').append('<div class="share-listing-form" style="display:none;"></div>');
                shareListingForm = $('.share-listing-form');
            }
            shareListingForm
                .empty()
                .attr('data-selector-to-show', selectorToHide)
                .html(Roomster.Templates.Apply(Roomster.Profile.UI.ProfileTemplates.TemplateNames.ShareListing, listingData))
                .css('min-height', '100%')
                //.css('height', $.mobile.getScreenHeight())
                .show();
        };

        var listingData = Roomster.Profile.UI.GetListingData(data);
        if (listingData) {
            setTimeout(show(listingData), 0);

        } else {
            Roomster.Listing.GetListingProfileViewById(data, function(listingData) {
                show(listingData);
            }, this._ErrorCallback);
        }
    },
    HideShareListing: function() {
        $('.share-listing-form').hide();
        $($('.share-listing-form').data('selector-to-show')).show();
        $('.jcarousel').jcarousel('scroll', '+=0');
    },
    MergeShareLink: function(shareLink, listingData) {
        var listing_url = window.location.protocol + '//' + window.location.host + $.url.home() + 'Listing/Profile/' + listingData.ListingProfile.ListingId;

        return shareLink.replace("{URL}", escape(listing_url))
            .replace("{IMAGE}", escape(window.location.protocol + listingData.MainPhotoFilePathMiddle))
            .replace("{TITLE}", escape(listingData.ListingProfile.Headline))
            .replace("{SUMMARY}", escape(listingData.Header))
            .replace("{TWITTER_STATUS}", escape("I like this - check it out @roomster " + listing_url))
            .replace(/&amp;/g, '&');
    },
    GetListingItemHtml: function(listingItem, userProfile) {
        if (userProfile)
            listingItem.UserProfile = userProfile;

        var result = '';
        if (Roomster.Profile.UI.ListingTemplates.TemplateNames[listingItem.ServiceType]) {
            result = Roomster.Templates.Apply(Roomster.Profile.UI.ListingTemplates.TemplateNames[listingItem.ServiceType], listingItem);
        }
        return result;
    },
    InitJcarousel: function(containerId, selectedIndex) {
        if (!selectedIndex) selectedIndex = 0;

        $('#' + containerId + ' .jcarousel li').width($(document).width());

        $('#' + containerId + ' .jcarousel').on('jcarousel:createend', function() {
            $(this).jcarousel('scroll', selectedIndex, false);
        }).jcarousel({
            wrap: 'circular',
            animation: 100,
            center: true
        });

        $('#' + containerId + ' .jcarousel-pagination').jcarouselPagination({
            item: function(page) {
                return '<a class="fa fa-circle-thin" href="#' + page + '"></a>';
            }
        }).on('jcarouselpagination:active', 'a', function() {
            $(this).removeClass('fa-circle-thin').addClass('fa-circle');
        })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('fa-circle').addClass('fa-circle-thin');
            });

        $('#' + containerId + ' .jcarousel-pagination a:nth-child(' + (selectedIndex + 1) + ')').removeClass('fa-circle-thin').addClass('fa-circle');

        var carousel = $('#' + containerId + ' .jcarousel');
        carousel.swipe({
            swipeLeft: function(event, direction, distance, duration, fingerCount) {
                carousel.jcarousel('scroll', '+=1');
            },
            swipeRight: function(event, direction, distance, duration, fingerCount) {
                carousel.jcarousel('scroll', '-=1');
            }
        });
    },
    LanguagesHandler: function(select) {
        select = $(select);
        var selectedOption = $('option:selected', select);

        selectedOption.hide();
        if ($('option:hidden', select).length > 0) {
            $("#languagesSelectFirstOption", select).text("Add another language");
        }

        $('#languageButton' + select.val()).show();
        $('#languageButton' + select.val() + ' input').attr('name', 'Languages');
        select.val('');
        return true;
    },
    LanguageButtonClick: function(button) {
        button = $(button);
        button.hide();
        var select = $('#languagesSelect');
        $('option[value=' + $('input', button).val() + ']', select).show();
        $('input', button).attr('name', '');
        if ($('option:hidden', select).length == 0) {
            $("#languagesSelectFirstOption", select).text("Select language");
        }
        return true;
    },

    GetUserAvatar: function() {
        Roomster.Profile.GetUserAvatar(this.GetUserAvatarCallback);
    },
    GetUserAvatarCallback: function(data) {
        if (window.isMobileUI) {
            if (data && data.useravatar) {
                $('nav .user-avatar').css('background-image', 'url(' + data.useravatar + ')');
            }
        } else {
            $('nav .userImageMenu img').attr('src', data.useravatar);
        }
    },
    GetUserInterests: function(user_id, provider, container_id) {
        provider.GetLikes(user_id, function(data) {
            Roomster.Profile.UI.ShowInterests(data, container_id);
        });
    },

    GetMutualUserInterests: function(user_id, user_id_compare, provider, container_id, parent_container_id) {
        provider.GetMutualLikes(user_id, user_id_compare, function(data) {
            Roomster.Profile.UI.ShowInterests(data, container_id, parent_container_id);
        });
    },

    ShowInterests: function(data, container_id, parent_container_id) {
        var likes = '';
        for (var i = 0; i < data.length; i++) {
            likes += '<div class="user-like"><a href="https://www.facebook.com/' + data[i].FacebookId + '" target="_blank"><img class="circle" src="https://graph.facebook.com/' + data[i].FacebookId + '/picture" />' + data[i].Name + "</a></div>";
        }
        if (likes !== '') {
            $(container_id).html(likes).show();
            if (parent_container_id) $(parent_container_id).show();
        } else {
            $(container_id).hide();
            if (parent_container_id) $(parent_container_id).hide();
        }
    },

    GetMutualFriendsCount: function(user_id, provider, container_id, parent_container_id) {
        provider.GetMutualFriendsCount(user_id, function(data) {
            if (data.total_count && Number(data.total_count) > 0) {
                $(container_id).html(data.total_count);
                $(parent_container_id).show();
            }
        });
    },
});
if (!window.isMobileUI) {
    $(function() {
        Roomster.Profile.UI.Init();
    });
} else {
    $(document).on('pagecontainershow', function(event, ui) {
        Roomster.Profile.UI.Init();
    });
}

/*EOF /Scripts/Roomster/Roomster.Profile.UI.js*/
/*/Scripts/Roomster/Roomster.Listing.UI.js*/
$.extend(Roomster.Listing.UI, {
    _SendMessageTemplate: 'SendMessageTemplate',
    PageNumberBeforeShowListing: 1,

    GetStateField: function(name) {
        if (this[name] == null || window.isMobileUI) {
            this[name] = $("input:hidden[name=" + name + "]");
        }
        return this[name];
    },
    HandleStateChange: function(selector) {
        $(selector).change(function() {
            if (Roomster.Listing.UI.GetStateField("ListingStateField").attr("listingId") == "0") {
                return;
            }
            Roomster.Listing.ToggleListingState(Roomster.Listing.UI.GetStateField("ListingStateField").attr("listingId"), this.checked);
            Roomster.Listing.UI.GetStateField("ListingStateField").val(this.checked);
            Roomster.Listing.UI.ChangeBoxState(this.checked);
        });
    },
    HandleToggleAction: function(selector, activate) {
        $(selector).click(function() {
            if (Roomster.Listing.UI.GetStateField("ListingStateField").attr("listingId") == "0") {
                return;
            }
            Roomster.Listing.ToggleListingState(Roomster.Listing.UI.GetStateField("ListingStateField").attr("listingId"), activate);
            Roomster.Listing.UI.GetStateField("ListingStateField").val(activate);
            Roomster.Listing.UI.DisplayShowHideActionLinks(activate);
        });
    },
    HandleDeleteAction: function(selector) {
        $(selector).click(function(e) {
            e.preventDefault ? e.preventDefault() : e.returnValue = false;

            return Roomster.Dialog.ConfirmDelete('.listingDeletePopup', $(this)[0].href, false, null, '#frmDeleteListing', null);
        });
    },
    ChangeBoxState: function(activate) {
        if (activate) {
            $(".listingstatebox").parent().find("label.showHideFields").removeClass("hiddenFields");
            $(".showHideFields .showText").css('display', 'inline');
            $(".showHideFields .hiddenText").hide();

        } else {
            $(".listingstatebox").parent().find("label.showHideFields").addClass("hiddenFields");
            $(".showHideFields .showText").hide();
            $(".showHideFields .hiddenText").css('display', 'inline');
        }
    },
    InitStateBox: function(listingId) {
        var field = this.GetStateField("ListingStateField");
        if (field.length === 0) {
            return;
        }

        /*
        if (field.val().toLowerCase() == "true")
        {
            $(".listingstatebox").attr("checked", "checked");
            this.ChangeBoxState(true);
        } else
        {
            this.ChangeBoxState(false);
        }
        this.HandleStateChange(".listingstatebox");
        */
        this.DisplayShowHideActionLinks(field.val().toLowerCase() == "true");
        this.HandleToggleAction('.hideListing', false);
        this.HandleToggleAction('.showListing', true);
        this.HandleDeleteAction('.deleteListing a');
    },
    OnContactClickHandler: function(nativeEvent, data) {
        var target = $.event.fix(nativeEvent).target;
        var $target = $(target);

        if (!data.UserId)
            data.UserId = data.Id;

        if (!Roomster.User.IsAuthenticated()) {
            Roomster.Dialog.SignUp.Show();
            return false;
        }

        if ($target.hasClass('icoMail')) {
            this.ShowSendMailDlg(target, data);
            return;
        }

        if (!Roomster.User.IsFullAllowed()) {
            var connections = Roomster.Resources.GetValues('SocialConnections');
            var className = "icoPhone";
            var viaText = Roomster.Resources.GetTranslation("via telephone"); //"Telephone";
            $.each(connections, function(index, value) {
                if ($target.hasClass(value.Name)) {
                    className = value.Name;
                    viaText = Roomster.Resources.GetTranslation("on") + " " + value.Name; //value.Name;
                    return false;
                }
            });
            Roomster.Dialog.AccessPopup.Show(data.UserId, className, viaText);
        }
    },
    OnListingLinkClickHandler: function(el) {
        Roomster.Listing.UI.PageNumberBeforeShowListing = Roomster.Search.SearchSettings.GetPageNumber();
        var positionParam = '&position=' + Roomster.Search.UI.GetPosition(el);
        var nextListingsParam = Roomster.Search.UI.GetNextListings(el);
        if (nextListingsParam)
            nextListingsParam = "&nextListings=" + nextListingsParam;
        Roomster.Page.Helper.Load($(el).attr("href") + '?from=' + Roomster.Search.SearchSettings.GetBrowserString() + "/" + positionParam + nextListingsParam);
    },
    ShowSendMailDlg: function(nativeElement, listingData) {
        var templateName = this._SendMessageTemplate;
        $.get($.url.action({
            controller: "UserProfile",
            action: "GetUserForAccessDialog",
            id: listingData.UserId
        })).fail(Roomster.Common.Log).done(function(data) {

            listingData.UserData = data;
            listingData.PhotoMedium = listingData.Photo;

            var emailbox = $(".emailbox");
            if (emailbox.length == 0) {
                $(document.body).append("<div class='emailbox'></div>");
                emailbox = $(".emailbox");
                Roomster.UserActionTracker.SetLogInfo(emailbox[0], nativeElement);
            }

            var html = Roomster.Templates.Apply(templateName, listingData);
            emailbox.html(html);
            emailbox.dialog({
                width: 605,
                height: 'auto',
                dialogClass: "commonDialog",
                modal: true,
                resizable: false,
                draggable: false,
                position: ({
                    at: "center center-60"
                })
            });

        });
    },
    LoadTemplates: function() {
        var fileExt = '.html';
        Roomster.Templates.LoadTemplates([{
            name: this._SendMessageTemplate,
            url: ['/Content/SearchTemplates/', this._SendMessageTemplate, fileExt].join('')
        }]);
    },
    DisplayShowHideActionLinks: function(listingIsActive) {
        if (listingIsActive) {
            $('.hideListing').show();
            $('.showListing').hide();
            $('#hiddenListingWarning').hide();
        } else {
            $('.hideListing').hide();
            $('.showListing').show();
            $('#hiddenListingWarning').show();
        }
    },
});

$(function() {
    Roomster.Listing.UI.InitStateBox();
});
$(function() {
    Roomster.Listing.UI.LoadTemplates();
});

/*EOF /Scripts/Roomster/Roomster.Listing.UI.js*/
/*/Scripts/Roomster/Roomster.Cookie.js*/
Roomster.Cookie.Base =
    function(name, ttl) {
        this._cookieName = name;
        this._ttl = ttl;
    };
$.extend(Roomster.Cookie.Base.prototype, {
    //_cookieName: "COOKIE.NAME",
    //_ttl: 12345 // days
    val: function(value) {
        if (value === undefined) {
            return this._get();
        } else {
            this._set(value);
        }
    },
    _get: function() {
        return $.cookie(this._cookieName);
    },
    _set: function(value) {
        $.cookie(this._cookieName, value, {
            expires: this._ttl,
            path: '/'
        });
    }
});
/*EOF /Scripts/Roomster/Roomster.Cookie.js*/
/*/Scripts/Roomster/Roomster.Templates.js*/
(function(ctx) {
    var self = ctx.Templates = (ctx.Templates || {});

    $.extend(self, {
        _Cache: {},
        _DownloadCache: {},
        TemplateVersion: 635991095509755859 || 0,
        _BeforeInitRequestedTemplates: [],
        Initalised: false,
        Init: function(url) {

            url = self.GetVersionedUrl(url);

            self.DownloadCombinedTemplates(url, function() {
                self.Initalised = true;

                if (self._BeforeInitRequestedTemplates.length) {
                    $.each(self._BeforeInitRequestedTemplates, function(idx, value) {
                        self.DownloadTemplate(value.url, value.callback);
                    });
                }
            });
        },
        Apply: function(content, data, selector) {
            if (!content)
                return null;

            var tmlFn;
            try {
                if (!/\W/.test(content)) {
                    tmlFn = self._Cache[content] = self._Cache[content] || self.Apply($(content).html());
                } else {
                    tmlFn = doT.template(content);
                }

                if (!tmlFn)
                    return null;

                return data ? tmlFn(data) : tmlFn;
            } catch (err) {
                if (/\W/.test(content))
                    throw err;
                console.log(err.message);
                console.log(content);
                return self._GetErrorContent(err.message, content, data);
            }
        },
        GetVersionedUrl: function(url) {
            return url + (url.indexOf('?') != -1 ? '&' : '?') + self.TemplateVersion;
        },
        DownloadCombinedTemplates: function(url, callback) {
            $.get(url, function(content) {
                // Remove last EOF info
                content = content.replace(/\/\*EOF \/.+\*\/\s*$/g, '');

                var filesData = content.split(/\/\*EOF \/.+\*\/\s*/g);

                $.each(filesData, function(idx, value) {
                    var templateUrl = value.match(/^\/\*\/.+\*\/\s*/g, '')[0].replace(/(^\/\*)|(\*\/\s*$)/g, '');
                    var templateContent = value.replace(/^\/\*\/.+\*\/\s*/g, '');
                    self._DownloadCache[templateUrl] = templateContent;
                });

                callback && callback();
            });
        },
        DownloadTemplate: function(url, callback) {
            if (self.Initalised) {
                var template = self._DownloadCache[url];
                if (template) {
                    callback(template);
                } else {
                    var versionedUrl = self.GetVersionedUrl(url);
                    $.get(versionedUrl, function(content) {
                        self._DownloadCache[url] = content;
                        callback(content);
                    });
                }
            } else {
                self._BeforeInitRequestedTemplates.push({
                    url: url,
                    callback: callback
                });
            }
        },
        ApplyRemoteTemplate: function(tml, append, data) {
            function apply(tmlFn) {
                if (append == null) return;
                try {
                    $.isFunction(append) ? append(tmlFn(data)) : $(append).append(tmlFn(data));
                } catch (err) {
                    console.log(err.message);
                    console.log(tml.name);
                    var errorContent = self._GetErrorContent(err.message, tml.name, model);
                    $.isFunction(append) ? append(errorContent) : $(append).append(errorContent);
                }
            };

            if (true) {
                self.DownloadTemplate(tml.url, function(content) {
                    self._Cache[tml.name] = doT.template(content);
                    apply(self._Cache[tml.name]);
                });
            } else {
                apply(self._Cache[tml.name]);
            }
        },
        LoadTemplates: function(templates) {
            $.each(templates, function(key, value) {
                self.ApplyRemoteTemplate(value);
            });
        },
        EnrichTemplates: function(context, array, urlPref) {
            for (var i = 0; i < array.length; i++) {
                context[array[i]] = {
                    name: array[i],
                    url: urlPref + array[i] + ".html"
                };
            }
        },
        _GetErrorContent: function(errorMesage, tml, model) {
            if (!/debug/gi.test(location.search))
                return "";
            var modelDescription = !model ? "Template model is null or undefined" : "Model:  " + JSON.stringify(model);
            var tmlDescription = !tml ? "Template is null!" : "Template name/selector: " + tml;

            var result = "<div class='templateError'>" +
                tmlDescription +
                "<br/>" +
                modelDescription +
                "<br/>" +
                "Error message: " + errorMesage +
                "</div>";
            return result;
        }
    });

    //self.Init('/Content/templates_.html');

})(Roomster);

/*EOF /Scripts/Roomster/Roomster.Templates.js*/
/*/Scripts/Roomster/Roomster.Templates.Desktop.js*/
(function(ctx) {
    ctx.Init('/Content/templates_.html');

})(Roomster.Templates);

/*EOF /Scripts/Roomster/Roomster.Templates.Desktop.js*/
/*/Scripts/Roomster/Roomster.Search.js*/
var _roomsterSearchMain = {
    DEFAULT_PAGE_SIZE: 15,
    SearchSettings: new Roomster.SearchSettings(),
    InProgress: false,
    Initialized: false,

    DoSearch: function(callback, errorCallback) {
        var _this = this;

        _this.InProgress = true;
        this.RunIt({
            url: this.SearchSettings.GetAsyncActionPath() + this.SearchSettings.GetSearchString(),
            xCallback: function() {
                _this.InProgress = false;
                callback.apply(null, arguments);
            },
            xError: function() {
                _this.InProgress = false;
                errorCallback.apply(null, arguments);
            }
        });
    },
    GetAjaxSettings: function() {
        return {
            url: this.SearchSettings.GetAsyncActionPath(),
            cache: false,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if (this.xCallback != undefined) {
                    this.xCallback(data);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (this.xError != undefined) {
                    this.xError(textStatus, errorThrown);
                }
            }
        };
    },
    RunIt: function(settings) {
        $.ajax($.extend(this.GetAjaxSettings(), settings));
    },
    Init: function() {
        if (this.Initialized === true)
            return;

        $.extend(Roomster.Search, this);
        this.SearchSettings.SetActionPath('/Search/');
        this.SearchSettings.SetAsyncActionPath('/AsyncSearch/');

        this.Initialized = true;
    }
};

if (!window.isMobileUI) {
    Roomster.WaitJquery(function() {
        _roomsterSearchMain.Init();
    });
} else {
    _roomsterSearchMain.Init();
}

/*EOF /Scripts/Roomster/Roomster.Search.js*/
/*/Scripts/Roomster/Roomster.Search.UI.Params.js*/
var _roomsterSearchUIParams = {
    _ListingItemTempletes: {
        1: 'SearchItemTempl',
        2: 'SearchItemTempl',
        4: 'SearchItemTemplTenants',
        5: 'SearchItemTemplApartments'
    },
    _NoResultsTemplate: 'NoResultsTemplate'
};

$.extend(Roomster.Search.UI, _roomsterSearchUIParams);

/*EOF /Scripts/Roomster/Roomster.Search.UI.Params.js*/
/*/Scripts/Roomster/Roomster.Search.UI.js*/
var _roomsterSearchUIMain = {
    _DEFAULT_PAGE_SIZE: 15,
    _MAX_PAGE_SIZE: 1000,
    _CLUSTER_ZOOM_CUTOFF: 15,

    _FiltersBlockId: 'searchFiltersBlock',
    _MainSearchTabsBlockId: 'MainSearchTabs',
    _TotalCounterId: 'lblTotalTop',
    _TotalCounterCaptionId: 'lblTotalTopCaption',
    _SearchSortId: 'SearchSorting',
    _ListingItemsBlockId: 'pnList',
    _MapBlockId: 'mapResults',
    _FavListBlockId: 'favList',
    _searchResultsContainer: 'searchResultsContainer',
    _FiltersRootPath: '/Content/Filters/',
    _FilterPostfix: 'Filter.html',
    _Filters: ['Budget', 'Geo', 'Age', 'Category', 'NoPetsPreferred', 'NoPetsOwned'],
    _ReplaceFilters: {
        'Preferred animals': 'NoPetsPreferred',
        'Own animals': 'NoPetsOwned'
    },
    _DefaultFilter: 'Category',
    _IgnoreFilters: ['ServiceType', 'Bookmark', 'NoPetsPreferred', 'NoPetsOwned'],
    //_ListingItemTempletes: { 1: 'SearchItemTempl', 2: 'SearchItemTempl', 4: 'SearchItemTemplTenants', 5: 'SearchItemTemplApartments' },
    _MapItemTemplates: {
        1: 'MapItemTempl',
        2: 'MapItemTempl',
        4: 'MapItemTempl',
        5: 'MapItemTempl'
    },
    _SearchTemplatePostfix: '.html',
    _SearchTemplatesRootPath: '/Content/SearchTemplates/',
    _PosibleLoadNextPage: false,
    _FiltersMode: false,
    //_NoResultsTemplate: 'NoResultsTemplate',
    _DisplayedListings: [],
    _ServerFilters: null,
    _SearchView: SearchViewEnum.List,
    _SearchViewDefault: SearchViewEnum.List,
    _ExecutedSearch: {},
    _MapInfoWindow: null,
    _Initialized: false,
    _RunInitialSearch: true,
    _ScrollPosition: null,

    GetPosition: function(element) {
        return $('.searchResultModel').index($(element).closest('.searchResultModel'));
    },
    GetNextListings: function(element) {
        var result = "";
        var index = this.GetPosition(element);
        var elements = $('.searchResultModel');
        for (var i = index + 1; i < elements.length; i++) {
            result += $(elements[i]).find(".bookmarkInList").attr("object-id") + ",";
        }

        return result;
    },
    BindTo: function(eventName, callback) {
        $(this).on("search" + eventName, callback);
    },
    ChangePage: function() {
        //this.SetDefaultView();

        Roomster.Search.SearchSettings.IncrementPageNumber();
        this.DoSearchByView();
    },
    IsPosibleLoadNextPage: function() {
        return this._PosibleLoadNextPage;
    },
    ChangeOrder: function(sortOrder) {
        //this.SetDefaultView();

        Roomster.Search.SearchSettings.SetPage(1);
        Roomster.Search.SearchSettings.SetOrder(sortOrder);
        this.DoSearchByView();
    },
    ChangeServiceType: function(serviceType) {
        //this.SetDefaultView();

        this.HideFilters();
        this._ClearTotalCounter();
        Roomster.Search.SearchSettings.ResetFilters();
        Roomster.Search.SearchSettings.SetPage(1);
        Roomster.Search.SearchSettings.SetServiceType(serviceType);

        $('#' + this._ListingItemsBlockId).html('');
        $('div.searchTopTabActive').toggleClass('searchTopTabActive');
        $('div[servicetypeid=' + serviceType + ']').toggleClass('searchTopTabActive');

        $('#FormattedGeo')[0].RoomsterGeo.SetGeoByLastPlace();

        $(this).trigger("searchChangeServiceType", serviceType);
    },
    ChangeFormattedText: function(formattedText) {
        Roomster.Search.SearchSettings.SetFormattedAddress(formattedText);
    },
    GetFormattedText: function() {
        return Roomster.Search.SearchSettings.GetFormattedAddress();
    },
    ChangeFilter: function(filterName, filterValue) {
        //this.SetDefaultView();

        Roomster.Search.SearchSettings.SetPage(1);
        Roomster.Search.SearchSettings.ChangeFilter(filterName, filterValue);
        if (!this._FiltersMode) {
            this._ClearTotalCounter();
            this.DoSearchByView();
        }
    },
    DoSearchByView: function() {
        if (this._SearchView === SearchViewEnum.List) {
            this.DoSearch();
        }
        if (this._SearchView === SearchViewEnum.Map) {
            this.DoMapSearch();
        }
    },
    DoSearch: function(successCallback) {
        //SET VIEW
        this._SearchView = SearchViewEnum.List;
        this.ToggleView();
        this.ToggleSwitchButton();
        //SET LAST SEARCH
        this._ExecutedSearch[SearchViewEnum.List] = Roomster.Search.SearchSettings.GetSearchString(true);

        if (Roomster.Search.SearchSettings.GetPageNumber() === 1) {
            $('#' + this._ListingItemsBlockId).html('');
        }

        var newSuccessCallback = successCallback == undefined ? this._CompleteCallback : $.proxy(
            function(result) {
                this._CompleteCallback(result);
                successCallback();
            }, this);

        if (!window.isMobileUI)
            this.ToggleLoader();
        else
            this.ShowLoader();

        this.ToogleButtonsStyle();
        Roomster.Search.DoSearch(newSuccessCallback, this._ErrorCallback);
    },
    DoMapSearch: function(successCallback) {
        //SET VIEW
        this._SearchView = SearchViewEnum.Map;
        this.ToggleView();
        this.ToggleSwitchButton();

        //SET LAST SEARCH
        this._ExecutedSearch[SearchViewEnum.Map] = Roomster.Search.SearchSettings.GetSearchString(true);

        //RESIZE MAP CONTAINER TO STRETCH VERTICALLY
        this.ResizeMap('#' + this._MapBlockId);

        //STORE CURRENT PAGE SETTINGS
        var currentPageNumber = Roomster.Search.SearchSettings.GetPageNumber();
        var currentPageSize = Roomster.Search.SearchSettings.GetPageSize();

        // CHANGE PAGING TO ONE ENORMOUS PAGE AND DO MAP SEARCH
        var newSuccessCallback = successCallback == undefined ? this._CompleteMapSearchCallback : $.proxy(
            function(result) {
                this._CompleteMapSearchCallback(result);
                successCallback();
            }, this);

        Roomster.Search.SearchSettings.SetPage(1, this._MAX_PAGE_SIZE);
        Roomster.Search.SearchSettings.SetAsyncActionPath('/SearchMap/');

        $('.loader').hide();
        this.ToggleLoader();
        this.ToogleButtonsStyle();

        Roomster.Search.DoSearch(newSuccessCallback, this._ErrorCallback);

        //RESET PAGE SETTINGS
        Roomster.Search.SearchSettings.SetPage(currentPageNumber, currentPageSize);
        Roomster.Search.SearchSettings.SetAsyncActionPath('/AsyncSearch/');
    },
    ShowSearchResults: function() {
        $("#" + this._searchResultsContainer).show();
        $("#" + this._FavListBlockId).hide();
        $("#" + this._MainSearchTabsBlockId).hide();
    },
    SwitchFiltersView: function(isMobile) {
        if ($("#" + this._MainSearchTabsBlockId).is(':visible')) {
            this.HideFilters(isMobile);
            if (isMobile) {
                this.ToggleView();
            }

        } else {
            this.ShowFilters(isMobile);
        }
    },
    SwitchFavView: function() {
        $("#" + this._MainSearchTabsBlockId).hide();
        if ($("#" + this._FavListBlockId).is(':visible')) {
            $("#" + this._searchResultsContainer).show();
            $("#" + this._FavListBlockId).hide();
        } else {
            $("#" + this._searchResultsContainer).hide();
            $("#" + this._FavListBlockId).show();
        }
    },
    SwitchView: function() {
        this._SearchView = this._SearchView === SearchViewEnum.List ? SearchViewEnum.Map : SearchViewEnum.List;

        Roomster.UserActionTracker.Track('/Search/Show View/' + (this._SearchView === SearchViewEnum.List ? 'List' : 'Map') + '/click');

        this.ToggleSwitchButton();
        this.ToggleView();

        // DO NOT RERUN SEARCH IF PARAMS hAVE NOT CHANGED
        if (this._ExecutedSearch[this._SearchView] === Roomster.Search.SearchSettings.GetSearchString(true)) {
            return;
        }

        if (this._SearchView === SearchViewEnum.List) {
            this.DoSearch();
        }
        if (this._SearchView === SearchViewEnum.Map) {
            $('#' + this._MapBlockId).html('');
            this.DoMapSearch();
        }
    },
    ShowListings: function(isMobile) {
        this._ClearTotalCounter();
        this.HideFilters(isMobile);
        this.ToggleView();
        this.DoSearchByView();
    },
    GetMapItemHtml: function(listingItem) {
        var result = '';
        if (this._MapItemTemplates[listingItem.ListingProfile.ServiceType]) {
            if (listingItem.UserProfile.Sex !== undefined) {
                listingItem.UserProfile.Sex = Roomster.Resources.GetByValue('Sex', listingItem.UserProfile.Sex);
            }

            result = Roomster.Templates.Apply(this._MapItemTemplates[listingItem.ListingProfile.ServiceType], listingItem);
        }
        return result;
    },
    GetListingItemHtml: function(listingItem) {
        var result = '';
        if (this._ListingItemTempletes[listingItem.ServiceType]) {
            if (listingItem.Sex !== undefined) {
                listingItem.Sex = Roomster.Resources.GetByValue('Sex', listingItem.Sex);
            }

            result = Roomster.Templates.Apply(this._ListingItemTempletes[listingItem.ServiceType], listingItem);
        }
        return result;
    },
    GetListingItemsHtml: function(listingItems) {
        var result = '';
        for (var i = 0, count = listingItems.length; i < count; i++) {
            result += this.GetListingItemHtml(listingItems[i]);
        }
        return result;
    },
    _GetFilterPath: function(filter) {
        return this._FiltersRootPath + filter + this._FilterPostfix;
    },
    GetFilterHtml: function(filter) {
        //console.log('filter before: ' + filter.Name);

        var result = '';
        if ($.inArray(filter.Name, this._IgnoreFilters) == -1) {
            var filterName = filter.Name;

            var replacedFilterName = this._ReplaceFilters[filterName];
            if (replacedFilterName) {
                filterName = replacedFilterName;
                for (var i = 0, count = this._ServerFilters.length; i < count; i++) {
                    if (this._ServerFilters[i].Name == filterName) {
                        filter['ParentFilter'] = this._ServerFilters[i];
                    }
                }
                //console.log(filter);
            } else if ($.inArray(filter.Name, this._Filters) == -1) {
                filterName = this._DefaultFilter;
            }
            //console.log('filter after ' + filterName);
            result = Roomster.Templates.Apply(filterName, filter);
        }
        return result;
    },
    GetFiltersHtml: function(filters) {
        this._ServerFilters = filters;

        var result = '';
        for (var j = 0, filtersCount = this._Filters.length; j < filtersCount; j++) {
            for (var i = 0, count = filters.length; i < count; i++) {
                if (filters[i].Name == this._Filters[j] || (this._Filters[j] == this._DefaultFilter && $.inArray(filters[i].Name, this._Filters) == -1)) {
                    result += this.GetFilterHtml(filters[i]);
                }
            }
        }
        return result;
    },
    SetServiceType: function(serviceType) {
        Roomster.Search.SearchSettings.SetServiceType(serviceType);
        this.ToogleButtonsStyle();
    },
    ToogleButtonsStyle: function() {
        var serviceType = Roomster.Search.SearchSettings.GetServiceType();
        var menuSelector = ".searchContainer, #MainSearchTabs";
        $(menuSelector).removeClass("peopleService");
        $(menuSelector).removeClass("placesService");
        $(menuSelector).removeClass("peopleService");
        $(menuSelector).removeClass("placesService");
        var activeClass = serviceType == ServiceTypeEnum.HaveApartment || ServiceTypeEnum.HaveShare ? "placesService" : "peopleService";
        $(menuSelector).addClass(activeClass);
        $(menuSelector).addClass(activeClass);

        // WAS REMOVED BY HALCYON
        $(".buttonATcommon").removeClass("ui-state-active");
        $(".buttonATcommon[href$=" + serviceType + "]").addClass("ui-state-active");
        //////////////////////////
        $(".buttonATcommonHolder").removeClass("ui-state-active");
        $(".buttonATcommon[href$=" + serviceType + "]").parent(".buttonATcommonHolder").addClass("ui-state-active");
        $("#FormattedGeo").attr("placeholder", this.GetSearchTextBoxPlaceHolder());
    },
    ShowFilters: function(isMobile) {
        this._FiltersMode = true;

        $("#searchFiltersBlock").css('height', 'auto');
        $("#pnList").hide();
        $("#hideFiltersButton").show();
        $(".searchResultsHolder").hide();
        if (this.ScrollChangeHandler.backToTop) {
            this.ScrollChangeHandler.backToTop.hide();
        }

        if (isMobile) {
            //$("#" + this._FavListBlockId).hide();
            $("#" + this._searchResultsContainer).hide();
            $("#" + this._MainSearchTabsBlockId).show();

            $.mobile.silentScroll(0);

        }
    },
    HideFilters: function(isMobile) {
        this._FiltersMode = false;

        $("#searchFiltersBlock").css('height', '87px');
        //$("#pnList").show();
        $("#hideFiltersButton").hide();
        $(".searchResultsHolder").show();

        if (isMobile) {
            $("#" + this._searchResultsContainer).show();
            $("#" + this._MainSearchTabsBlockId).hide();
            //$("#" + this._FavListBlockId).hide();
        }
    },
    ToggleLoader: function() {
        if (!window.isMobileUI || this._SearchView === SearchViewEnum.Map) {
            $('#loadingIndicator').toggle();
        } else {
            $('.loader').toggle();
        }

        this.ToggleLoader.Visible = !this.ToggleLoader.Visible;
    },
    ShowLoader: function() {
        if (!window.isMobileUI || this._SearchView === SearchViewEnum.Map) {
            $('#loadingIndicator').show();
        } else {
            $('.loader').show();
        }
        this.ToggleLoader.Visible = true;
    },
    HideLoader: function() {
        if (!window.isMobileUI || this._SearchView === SearchViewEnum.Map) {
            $('#loadingIndicator').hide();
        } else {
            $('.loader').hide();
        }
        this.ToggleLoader.Visible = false;
    },
    ToggleView: function() {
        if (this._SearchView === SearchViewEnum.List) {
            $('#' + this._ListingItemsBlockId).show();
            $('#' + this._MapBlockId).hide();
        }
        if (this._SearchView === SearchViewEnum.Map) {
            $('#' + this._MapBlockId).show();
            $('#' + this._ListingItemsBlockId).hide();
            if (this.ScrollChangeHandler && this.ScrollChangeHandler.backToTop) {
                this.ScrollChangeHandler.backToTop.hide();
            }
        }
    },
    ToggleSwitchButton: function() {
        if (this._SearchView === SearchViewEnum.List) {
            $('.mapViewBtn').show();
            $('.listViewBtn').hide();
            $('#SortControl').visible();
        }
        if (this._SearchView === SearchViewEnum.Map) {
            $('.listViewBtn').show();
            $('.mapViewBtn').hide();
            $('#SortControl').invisible();
            this.ScrollToBottom();
        }
        $('#SwitchViewControl').show();
    },
    SetDefaultView: function() {
        this._SearchView = this._SearchViewDefault;
        this.ToggleSwitchButton();
        this.ToggleView();
    },
    SyncUrl: function(searchString) {
        if (searchString == undefined) {
            Roomster.Search.SearchSettings.SetSearchString(window.location.href);
            Roomster.Search.DoSearch(this._CompleteCallback, this._ErrorCallback);
        } else {
            if (Roomster.Page) {
                Roomster.Page.Helper.ReplaceUrlAndSaveCurrentPage(searchString);
            } else {
                window.location.href = searchString;
            }
        }
    },
    GetSearchTextBoxPlaceHolder: function() {
        switch (Roomster.Search.SearchSettings.GetServiceType()) {
            case ServiceTypeEnum.NeedRoom:
                return Roomster.Resources.GetTranslation('Where is your room?');
            case ServiceTypeEnum.HaveShare:
                return Roomster.Resources.GetTranslation('Search Location');
            case ServiceTypeEnum.NeedApartment:
                return Roomster.Resources.GetTranslation('Where is your place?');
            case ServiceTypeEnum.HaveApartment:
                return Roomster.Resources.GetTranslation('Search Location');
        }
        return "";
    },
    GetSearchResultsCaption: function() {
        var result = "";
        switch (Roomster.Search.SearchSettings.GetServiceType()) {
            case ServiceTypeEnum.NeedRoom:
                result = Roomster.Resources.GetTranslation('People Looking for Rooms');
                break;
            case ServiceTypeEnum.HaveShare:
                result = Roomster.Resources.GetTranslation('Private Rooms');
                break;
            case ServiceTypeEnum.NeedApartment:
                result = Roomster.Resources.GetTranslation('People Looking for Entire Places');
                break;
            case ServiceTypeEnum.HaveApartment:
                result = Roomster.Resources.GetTranslation('Entire Places');
                break;
        }
        switch (Roomster.Search.SearchSettings.GetFilter("Bookmark")) {
            case BookmarkTypeEnum.Bookmarked:
                result += " Bookmarked";
                break;
            case BookmarkTypeEnum.Viewed:
                result += " Viewed";
                break;
            case BookmarkTypeEnum.Emailed:
                result += " Emailed";
                break;
        }
        return result;
    },
    _ClearTotalCounter: function() {
        $('#' + this._TotalCounterId).text("");
    },
    _CompleteCallback: function(searchResult) {
        try {
            var listingItemsHtml = '';
            if (searchResult.ListingItems.length == 0 && Roomster.Search.SearchSettings.GetPageNumber() == 1) {
                if (!Roomster.User.IsAuthenticated()) {
                    window.location = $.url.action('RegisterUser', {
                        controller: 'Registration'
                    });
                    return;
                } else {
                    listingItemsHtml = Roomster.Templates.Apply(this._NoResultsTemplate, {});
                }
            }
            this._PosibleLoadNextPage = searchResult.ListingItems != null && searchResult.ListingItems.length > 0;

            if (this._PosibleLoadNextPage) //Clean duplicates
            {
                if (Roomster.Search.SearchSettings.GetPageNumber() == 1)
                    this._DisplayedListings.length = 0;

                //for (var i = 0; i < searchResult.ListingItems.length; i++)
                for (var i = searchResult.ListingItems.length - 1; i >= 0; i--) //reverse loop to make sure there's NO conflict with forward splicing
                {
                    if ($.inArray(searchResult.ListingItems[i].Id, this._DisplayedListings) == -1) {
                        this._DisplayedListings.push(searchResult.ListingItems[i].Id); //add to log of displayed listings
                    } else {
                        searchResult.ListingItems.splice(i, 1); // DUPLICATE! Remove from search
                    }
                }
            }

            var $ListingItemsBlock = $('#' + this._ListingItemsBlockId);
            if (!listingItemsHtml.length) {
                listingItemsHtml = this.GetListingItemsHtml(searchResult.ListingItems);
            }

            this.UpdateTotal(searchResult.Count);

            var $SearchSort = $('#' + this._SearchSortId);
            $SearchSort.val(searchResult.SearchSettings.SearchSort);

            var filtered = $(searchResult.SearchSettings.Filters).filter(function() {
                return this.Name == "Geo";
            });

            if (filtered.length > 0) {
                var geoFilter = filtered[0];
                Roomster.Search.SearchSettings.ChangeFilter("Geo", [geoFilter.GeoSettings.SouthWest.Latitude, geoFilter.GeoSettings.SouthWest.Longitude, geoFilter.GeoSettings.NorthEast.Latitude, geoFilter.GeoSettings.NorthEast.Longitude, geoFilter.GeoSettings.Radius].join("--"));
            }

            if (Roomster.Search.SearchSettings.GetPageNumber() == 1) {
                $('#' + this._FiltersBlockId).html(this.GetFiltersHtml(searchResult.SearchSettings.Filters));

                $ListingItemsBlock.html(this.GetClearTagFilterHtml(searchResult.ListingItems) + listingItemsHtml);
            } else {
                $ListingItemsBlock.append(listingItemsHtml);
            }

            if (listingItemsHtml != "" && !window.isMobileUI) {
                var adSlot = Roomster.Ads.UI.Slots.Search[Roomster.Search.SearchSettings.GetServiceType()];
                $ListingItemsBlock.append(Roomster.Ads.UI.GetAdsHtml(adSlot));
            }

            var browserString = Roomster.Search.SearchSettings.GetBrowserString();

            if (!window.isMobileUI) {
                this.SyncUrl(encodeURI(browserString));
            } else {
                history.pushState({}, '', browserString);
                //$.mobile.activePage.jqmData('url', browserString);
                $.mobile.activePage.attr('data-url', browserString);

                //this._ExecutedSearch[SearchViewEnum.List] = Roomster.Search.SearchSettings.GetSearchString(true);
            }

            (function(bookmarkButton) {
                bookmarkButton.UnbindHandlers('.bookmarkContainer', '.bookmarkInList');

                var handlers = bookmarkButton.GetDefaultHandlers();

                if (!Roomster.User.IsAuthenticated()) {
                    // we set on click button from template;
                    handlers.buttonClick = null;
                }
                bookmarkButton.BindHandlers('.bookmarkContainer', '.bookmarkInList', handlers);
            })(Roomster.Bookmark.UI.BookmarkButton);

            if (Roomster.User.IsAuthenticated())
                Roomster.SavedSearch.UI.InitSaveSearchControls(browserString);

            $('.searchTopBlockR').show();

        } finally {
            if (!window.isMobileUI)
                this.ToggleLoader();
            else {
                //console.log(searchResult.ListingItems.length + ':' + searchResult.SearchSettings.PageSetting.PageSize);
                if (searchResult.ListingItems.length < searchResult.SearchSettings.PageSetting.PageSize) {
                    this.HideLoader();
                }
            }
        }
    },
    _CompleteMapSearchCallback: function(searchResult) {
        try {
            //UPDATE COUNTER
            Roomster.Search.UI.UpdateTotal(searchResult.Count);

            // GET MAP DATA
            var geoFilter = $.grep(searchResult.SearchSettings.Filters, function(e) {
                return e.Name == 'Geo';
            })[0];

            var midPoint = Roomster.Search.UI.GetMidPoint(geoFilter.GeoSettings.MidPoint, searchResult.ListingItems);
            var bounds = Roomster.Search.UI.GetBounds(searchResult.ListingItems);

            var map_div = $('#' + Roomster.Search.UI._MapBlockId);
            var zoom = Roomster.Search.UI.GetBoundsZoomLevel(bounds, {
                height: map_div.height(),
                width: map_div.width()
            });
            //console.log("zoom: " + zoom);

            var center = new google.maps.LatLng(midPoint.Latitude, midPoint.Longitude);
            var map = new google.maps.Map(document.getElementById(Roomster.Search.UI._MapBlockId), {
                //zoom: 11,
                zoom: zoom,
                center: center,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var markerImgUrl = '/Content/images/map/map-marker.png';
            var markerImage = new google.maps.MarkerImage(markerImgUrl, new google.maps.Size(24, 32));

            var markers = [];
            for (var i = 0; i < searchResult.ListingItems.length; i++) {
                var listing = searchResult.ListingItems[i];

                var latLng = new google.maps.LatLng(listing.Lat, listing.Long);
                var marker = new google.maps.Marker({
                    position: latLng,
                    icon: markerImage,
                    listingid: listing.Id
                });

                google.maps.event.addListener(marker, 'click', function() {
                    Roomster.Search.UI.MarkerClick(map, this);
                });

                markers.push(marker);
            }

            // IMAGES FROM SMALLEST TO BIGGEST
            var styles = [{
                url: '/Content/images/map/cluster1.png',
                width: 53,
                height: 52,
                textColor: '#fff',
                textSize: 12
            }, {
                url: '/Content/images/map/cluster2.png',
                width: 56,
                height: 55,
                textColor: '#fff',
                textSize: 12
            }, {
                url: '/Content/images/map/cluster3.png',
                width: 66,
                height: 65,
                textColor: '#fff',
                textSize: 12
            }, {
                url: '/Content/images/map/cluster4.png',
                width: 78,
                height: 77,
                textColor: '#fff',
                textSize: 12
            }, {
                url: '/Content/images/map/cluster5.png',
                width: 90,
                height: 89,
                textColor: '#fff',
                textSize: 12
            }, ];
            //var options = { imagePath: '/Content/images/map/m' }
            var options = {
                styles: styles
            };
            var markerCluster = new MarkerClusterer(map, markers, options);

            Roomster.Search.UI._MapInfoWindow = new google.maps.InfoWindow();

            google.maps.event.addListener(markerCluster, 'clusterclick', function(cluster) {
                Roomster.Search.UI.ClusterClick(map, cluster, markerCluster);
            });

            // UPDATE UI
            $('#' + Roomster.Search.UI._FiltersBlockId).html(Roomster.Search.UI.GetFiltersHtml(searchResult.SearchSettings.Filters));

            var browserString = Roomster.Search.SearchSettings.GetBrowserString();
            Roomster.Search.UI.SyncUrl(encodeURI(browserString));

            if (Roomster.User.IsAuthenticated())
                Roomster.SavedSearch.UI.InitSaveSearchControls(browserString);

        } finally {
            Roomster.Search.UI.ToggleLoader();

        }
    },
    ClusterClick: function(map, miniCluster, markerCluster) {
        //console.log('Map zoom before click: ' + map.getZoom());

        var setZoomClick = map.getZoom() < Roomster.Search.UI._CLUSTER_ZOOM_CUTOFF;

        markerCluster.setZoomClick(setZoomClick);

        if (!setZoomClick) {
            if (!miniCluster.Content) {
                var markers = miniCluster.getMarkers();
                var listingIds = [];
                for (var i = 0; i < markers.length; i++) {
                    listingIds.push(markers[i].listingid);
                }

                var content = '<div class="mapInfo loading"><i class="fa fa-cog fa-spin fa-2x green"></i></div>';
                Roomster.Search.UI._MapInfoWindow.setContent(content);
                Roomster.Search.UI._MapInfoWindow.setPosition(miniCluster.getCenter());
                Roomster.Search.UI._MapInfoWindow.open(map);

                Roomster.Listing.GetListings(listingIds, function(results) {
                    content = '<div class="mapInfoList">';
                    for (var j = 0; j < results.length; j++) {
                        var listing = results[j];

                        content += Roomster.Search.UI.GetMapItemHtml(listing);
                    }
                    content += '</div>';
                    Roomster.Search.UI._MapInfoWindow.setContent(content);
                    Roomster.Search.UI._MapInfoWindow.setPosition(miniCluster.getCenter());
                    miniCluster.Content = content;
                    Roomster.Search.UI._MapInfoWindow.open(map);
                });
            } else {
                Roomster.Search.UI._MapInfoWindow.setContent(miniCluster.Content);
                Roomster.Search.UI._MapInfoWindow.setPosition(miniCluster.getCenter());
                Roomster.Search.UI._MapInfoWindow.open(map);
            }

        }
    },
    MarkerClick: function(map, marker) {
        if (!marker.Content) {
            var content = '<div class="mapInfo loading"><i class="fa fa-cog fa-spin fa-2x green"></i></div>';
            Roomster.Search.UI._MapInfoWindow.setContent(content);
            Roomster.Search.UI._MapInfoWindow.open(map, marker);

            Roomster.Listing.GetListingProfileViewById(marker.listingid, function(data) {

                //content = data.ListingProfile.Description;
                content = Roomster.Search.UI.GetMapItemHtml(data);
                Roomster.Search.UI._MapInfoWindow.setContent(content);
                marker.Content = content;
                Roomster.Search.UI._MapInfoWindow.open(map, marker);
            });
        } else {
            Roomster.Search.UI._MapInfoWindow.setContent(marker.Content);
            Roomster.Search.UI._MapInfoWindow.open(map, marker);
        }
    },
    GetMidPoint: function(midPoint, listings) {
        // If correct midpoint, return it and skip the rest
        if (midPoint.Latitude != 0 && midPoint.Latitude != -180)
            return midPoint;

        // If there's only one listing, use it's location as midpoint
        if (listings.length == 1)
            return {
                Latitude: listings[0].Lat,
                Longitude: listings[0].Long
            };

        // Get bounds and center
        var bounds = this.GetBounds(listings);
        var center = bounds.getCenter();

        return {
            Latitude: center.lat(),
            Longitude: center.lng()
        };

        //return { Latitude: 42.52677220056902, Longitude: -71.773681640625 };
    },
    GetBounds: function(listings) {
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < listings.length; ++i) {
            var point = new google.maps.LatLng(listings[i].Lat, listings[i].Long);
            bounds.extend(point);
        }
        return bounds;
    },
    // http://stackoverflow.com/a/13274361
    // The "map_dim" parameter value should be an object with "height" and "width" properties that represent the height
    // and width of the DOM element that displays the map. You may want to decrease these values if you want to ensure
    // padding. That is, you may not want map markers within the bounds to be too close to the edge of the map.
    GetBoundsZoomLevel: function(bounds, map_dim) {
        var WORLD_DIM = {
            height: 256,
            width: 256
        };
        var ZOOM_MAX = 18; //21;

        function latRad(lat) {
            var sin = Math.sin(lat * Math.PI / 180);
            var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
            return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
        }

        function zoom(mapPx, worldPx, fraction) {
            return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
        }

        var ne = bounds.getNorthEast();
        var sw = bounds.getSouthWest();

        var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;

        var lngDiff = ne.lng() - sw.lng();
        var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        var latZoom = zoom(map_dim.height, WORLD_DIM.height, latFraction);
        var lngZoom = zoom(map_dim.width, WORLD_DIM.width, lngFraction);

        return Math.min(latZoom, lngZoom, ZOOM_MAX);
    },
    GetClearTagFilterHtml: function(listingItems) {
        var result = '';
        var searchSetings = Roomster.Search.SearchSettings;
        var tagFilterName = Roomster.Resources.GetValues('ListingTagsCategory') ? Roomster.Resources.GetValues('ListingTagsCategory')[searchSetings.GetServiceType()] : "";
        var tagFilterValueId = searchSetings.GetFilter(tagFilterName);

        if (tagFilterValueId) {
            $(listingItems).each(function(idx, el) {
                $(el.Tags).each(function(idx, el) {
                    if (tagFilterValueId == el.Id) {
                        result = Roomster.Templates.Apply('<a class="floatRight selectedTag" onclick="{{=it.OnClickHandler}}" style="margin:2px 10px;padding:3px 7px;">{{=it.Value}}&nbsp;&nbsp;x</a><div class="clear"></div>', {
                            OnClickHandler: "Roomster.Search.UI.ChangeFilter('" + tagFilterName + "','')",
                            Value: el.Label
                        });
                    }
                    return result == '';
                });
                return result == '';
            });
        }

        return result;
    },
    _ErrorCallback: function() {
        this.ToggleLoader();
    },
    UpdateTotal: function(total) {
        var $TotalCounter = $('#' + this._TotalCounterId);
        if (!$TotalCounter.text()) {
            var totalCountString = total >= 1000 ? "1000+" : total.toString();
            $TotalCounter.text(totalCountString);
            $('#' + this._TotalCounterCaptionId).text(this.GetSearchResultsCaption());
        }
    },
    AdjustSearchNav: function() {
        var pnSearchTopBlock = $('#MainSearchTabs');
        if (!pnSearchTopBlock || !pnSearchTopBlock.offset()) {
            return;
        }

        //console.log(pnSearchTopBlock.offset().top);
        //console.log(pnSearchTopBlock.outerHeight());

        //var offset = 255;
        var div = $('.searchHeader');
        var divResultsHolder = $('.searchResultsHolder');
        var divHeader = $('header');

        if (this._SearchView === SearchViewEnum.Map) {
            if (div.hasClass('is-fixed')) {
                div.removeClass('is-fixed');
                divResultsHolder.removeClass('is-fixed');
            }
            return;
        }

        //console.log('searchHeader height: ' + div.outerHeight());

        var offset = pnSearchTopBlock.offset().top + pnSearchTopBlock.outerHeight() - divHeader.outerHeight();

        //console.log(offset);

        if ($(window).scrollTop() > offset && !div.hasClass('is-fixed')) {
            div.addClass('is-fixed');
            divResultsHolder.addClass('is-fixed');

        } else if ($(window).scrollTop() <= offset) {
            div.removeClass('is-fixed');
            divResultsHolder.removeClass('is-fixed');

        }
    },
    ResizeMap: function(selector) {
        if (this._SearchView !== SearchViewEnum.Map)
            return;

        if (!selector || selector == '') {
            selector = '#' + this._MapBlockId;
        }

        var div = $(selector);
        var searchHeaderDiv = $('.searchHeader');
        var header = $('header');

        var searchHeaderDivHeight = !searchHeaderDiv.hasClass('is-fixed') ? searchHeaderDiv.outerHeight() + 30 : searchHeaderDiv.outerHeight(); //30px - extra padding for is-fixed

        //var topOffset = searchResultsHolder.offset().top;
        //var divHeight = $(window).height() - topOffset;

        //var divHeight = $(window).height() - header.outerHeight() - searchHeaderDivHeight - 1 - 20; //20px - extra map margin
        var divHeight = $(window).height() - header.outerHeight() - searchHeaderDivHeight - 5;
        div.height(divHeight);
        //div.css("margin-top", "8px");

        //console.log('window.height: ' + $(window).height());
        //console.log('header.height: ' + header.outerHeight());
        //console.log('search-header.height: ' + searchHeaderDivHeight);

        //////SCROLL TO THE BOTTOM
        this.ScrollToBottom();

    },
    ScrollToBottom: function() {
        setTimeout(function() {
            $('html, body').scrollTop($(document).height());
        }, 100);

        //window.scrollTo(0, $(document).height());
        //$(document).scrollTop($(document).scrollHeight);
    },
    ScrollChangeHandler: function() {
        if ($('.ui-panel-open').length !== 0) return;

        //this._ScrollPosition = $(window).scrollTop();
        this.AdjustSearchNav();

        if (this._SearchView === SearchViewEnum.Map || this._FiltersMode || Roomster.Search.InProgress)
            return;

        //var state = History.getState();

        if ( /*state.url.indexOf('?from=/') == -1 && */ this.IsPosibleLoadNextPage()) {
            if (!this.ScrollChangeHandler.backToTop) {
                this.ScrollChangeHandler.backToTop = $(".scrollTopButton");
            }

            if ($(window).scrollTop() > 0) {
                this.ScrollChangeHandler.backToTop.show();
            } else {
                this.ScrollChangeHandler.backToTop.hide();
            }

            //if ($(window).scrollTop() + $(window).height() >=
            //    $(document).height() - $("#mainFooter").height()
            //    - $("#pnList .searchResultModel").last().height() * 7
            //    - $("#pnList iframe").last().height()) {
            //    this.ChangePage();
            //}

            var scrolledHeight = $(window).scrollTop() + $(window).height();
            var triggerHeight = !window.isMobileUI ?
                $(document).height() - $("#mainFooter").height() - $("#pnList .searchResultModel").last().height() * 7 - $("#pnList iframe").last().height() :
                $(document).height() - $(window).height() * 0.3;

            if (scrolledHeight >= triggerHeight) {
                this.ChangePage();
            }
        }
    },
    Init: function() {

        if (this._Initialized === true)
            return;

        this._CompleteCallback = $.proxy(this._CompleteCallback, Roomster.Search.UI);
        this._ErrorCallback = $.proxy(this._ErrorCallback, Roomster.Search.UI);
        this.ScrollChangeHandler = $.proxy(this.ScrollChangeHandler, Roomster.Search.UI);

        this._ListingItemTempletes = Roomster.Search.UI._ListingItemTempletes;
        this._NoResultsTemplate = Roomster.Search.UI._NoResultsTemplate;

        $.extend(Roomster.Search.UI, this);

        Roomster.Resources.Load();
        for (var i = 0, count = this._Filters.length; i < count; i++) {
            Roomster.Templates.LoadTemplates([{
                name: this._Filters[i],
                url: this._GetFilterPath(this._Filters[i])
            }]);
        }
        for (var serviceTypeId in this._ListingItemTempletes) {
            Roomster.Templates.LoadTemplates([{
                name: this._ListingItemTempletes[serviceTypeId],
                url: this._SearchTemplatesRootPath + this._ListingItemTempletes[serviceTypeId] + this._SearchTemplatePostfix
            }]);
            Roomster.Templates.LoadTemplates([{
                name: this._MapItemTemplates[serviceTypeId],
                url: this._SearchTemplatesRootPath + this._MapItemTemplates[serviceTypeId] + this._SearchTemplatePostfix
            }]);
        }

        Roomster.Templates.LoadTemplates([{
            name: this._NoResultsTemplate,
            url: this._SearchTemplatesRootPath + this._NoResultsTemplate + this._SearchTemplatePostfix
        }]);

        if (!window.isMobileUI) {
            $(window).scroll(this.ScrollChangeHandler);
        } else {
            // jQuerymobile doesn't fire $(window).scroll.
            // Use  $(document).on("scrollstart",function() {}); instead
            $(document).on("scrollstop", function() {

                if ($.mobile.activePage.find('#searchResultsContainer').first().length === 0) return;

                Roomster.Search.UI.ScrollChangeHandler();
            });
        }

        this._Initialized = true;
    }
};

if (!window.isMobileUI) {
    Roomster.WaitJquery(function() {
        _roomsterSearchUIMain.Init();
    });
} else {
    _roomsterSearchUIMain.Init();
}

/*EOF /Scripts/Roomster/Roomster.Search.UI.js*/
/*/Scripts/Roomster/Roomster.Geo.js*/
Roomster.Geo.AddressType =
    function(name, index) {
        this.toString =
            function() {
                return name;
            };
        this.Index =
            function() {
                return index;
            };
    };

$.extend(
    Roomster.Geo, {
        _ADDRESS_TYPES: {
            Political: new Roomster.Geo.AddressType('political', 1),
            Country: new Roomster.Geo.AddressType('country', 10),
            NaturalFeature: new Roomster.Geo.AddressType('natural_feature', 14),
            AdminArea1: new Roomster.Geo.AddressType('administrative_area_level_1', 15),
            AdminArea2: new Roomster.Geo.AddressType('administrative_area_level_2', 20),
            AdminArea3: new Roomster.Geo.AddressType('administrative_area_level_3', 25),
            SubLocality: new Roomster.Geo.AddressType('sublocality', 29),
            Locality: new Roomster.Geo.AddressType('locality', 30),
            Neighborhood: new Roomster.Geo.AddressType('neighborhood', 31),
            Subpremise: new Roomster.Geo.AddressType('subpremise', 33),
            PostalCode: new Roomster.Geo.AddressType('postal_code', 34),
            Intersection: new Roomster.Geo.AddressType('intersection', 35),
            Street: new Roomster.Geo.AddressType('route', 40),
            Address: new Roomster.Geo.AddressType('street_address', 45),
            StreetNumber: new Roomster.Geo.AddressType('street_number', 50),
            Establishment: new Roomster.Geo.AddressType('establishment', 51)

        },
        _GoogleMapsLoading: false,
        GoogleMapsLoaded: $.Deferred(),
        GoogleMapsCallback: function() {
            Roomster.Geo.GoogleMapsLoaded.resolve();
        },
        LoadGoogleMaps: function() {
            if (typeof google === "undefined" && !Roomster.Geo._GoogleMapsLoading) {
                var apiUrl = "https://maps.googleapis.com/maps/api/js?v=3.7&sensor=false&libraries=places&language=en&callback=Roomster.Geo.GoogleMapsCallback";
                //$.ajax({url:apiUrl, dataType: "script"}).fail(Roomster.Geo.GoogleMapsLoaded.reject);

                Roomster.Geo._GoogleMapsLoading = true;
                $.cachedScript(apiUrl).fail(Roomster.Geo.GoogleMapsLoaded.reject).done(function() {
                    Roomster.Geo._GoogleMapsLoading = false;
                    Roomster.UserActionTracker.Track('GoogleMapsLoaded');
                });
            }

            return Roomster.Geo.GoogleMapsLoaded.promise();
        },
        InitSearchGeo: function(geo, geoInput, callback) {
            if (!geo) {
                Roomster.Geo.LoadGoogleMaps().done(function() {
                    geo = new Roomster.Search.UI.Geo(geoInput);
                    if (callback) callback();
                });
            } else {
                if (callback) callback();
            }
        },
        GetComponent: function(results, minDefault, maxDefault) {
            if (maxDefault == undefined) {
                maxDefault = minDefault;
            }
            var currentIndex = minDefault - 1;

            var reverseSearch = false;
            if (minDefault > maxDefault) {
                reverseSearch = true;
                maxDefault += minDefault;
                minDefault = maxDefault - minDefault;
                maxDefault -= minDefault;
                currentIndex = maxDefault + 1;
            }
            var component = null;
            if (!$.isArray(results)) {
                results = [results];
            }
            if (results != null && results.length != 0) {
                for (var j = 0, resultsLen = results.length, result = results[j]; j < resultsLen; j++, result = results[j]) {
                    for (var i = 0, len = result.address_components.length; i < len; i++) {
                        var addressType = this.GetAddressType(result.address_components[i].types);
                        if (addressType != null && (Roomster.Geo._ADDRESS_TYPES.PostalCode.Index() == addressType.Index() || !/^\d+$/.test(result.address_components[i].long_name)) && addressType.Index() >= minDefault && addressType.Index() <= maxDefault && ((reverseSearch && addressType.Index() <= currentIndex) || (!reverseSearch && addressType.Index() >= currentIndex))) {
                            component = result.address_components[i];
                            currentIndex = addressType.Index();
                        }
                    }
                }
            }
            return component;
        },
        GetLongName: function(results, minDefault, maxDefault) {
            var result = '';
            var component = this.GetComponent(results, minDefault, maxDefault);
            if (component != null) {
                result = component.long_name;
            }
            return result;
        },
        GetShortName: function(results, minDefault, maxDefault) {
            var result = '';
            var component = this.GetComponent(results, minDefault, maxDefault);
            if (component != null) {
                result = component.short_name;
            }
            return result;
        },
        GetAddressTypeByName: function(name) {
            var found = null;
            for (var addressType in this._ADDRESS_TYPES) {
                if (addressType.toString() == name) {
                    found = addressType;
                }
            }
            return found;
        },
        GetAddressType: function(addressTypes) {
            var result = null;
            var maxIndex = this._ADDRESS_TYPES.Country.Index() - 1;
            for (var name in this._ADDRESS_TYPES) {
                if ($.inArray(this._ADDRESS_TYPES[name].toString(), addressTypes) != -1 && this._ADDRESS_TYPES[name].Index() > maxIndex) {
                    result = this._ADDRESS_TYPES[name];
                    maxIndex = this._ADDRESS_TYPES[name].Index();
                }
            }
            return result;
        },
        GetAddress: function(result) {
            var street = '';
            var homeNum = '';
            street = this.GetLongName(result, this._ADDRESS_TYPES.Street.Index());
            homeNum = this.GetLongName(result, this._ADDRESS_TYPES.StreetNumber.Index());

            var address = street + (homeNum.length > 0 ? ', ' : '') + homeNum;
            return address;
        },
        GetCity: function(results) {
            return this.GetLongName(results, this._ADDRESS_TYPES.AdminArea3.Index(), this._ADDRESS_TYPES.Locality.Index());
        },
        GetCityFormattedAddress: function(result) {
            var formattedAddress = result.formatted_address;
            var minComponent = this._ADDRESS_TYPES.Country;
            var maxComponent = this._ADDRESS_TYPES.Locality;

            for (var i = 0, len = result.address_components.length; i < len; i++) {
                var addressType = this.GetAddressType(result.address_components[i].types);
                if (addressType != null && (addressType.Index() < minComponent.Index() || addressType.Index() > maxComponent.Index())) {
                    var stringStart = formattedAddress.indexOf(result.address_components[i].long_name);
                    if (stringStart != -1) {
                        formattedAddress = formattedAddress.substring(0, stringStart) + formattedAddress.substring(stringStart + result.address_components[i].long_name.length);
                    }
                }
            }
            return formattedAddress;
        },
        GetSublocality: function(results) {
            return this.GetLongName(results, this._ADDRESS_TYPES.SubLocality.Index());
        },
        GetRegion: function(results) {
            return this.GetLongName(results, this._ADDRESS_TYPES.AdminArea2.Index(), this._ADDRESS_TYPES.AdminArea1.Index());
        },
        GetPostalCode: function(results) {
            return this.GetLongName(results, this._ADDRESS_TYPES.PostalCode.Index());
        },
        GetCountry: function(results, getShort) {
            if (getShort == undefined) {
                getShort = false;
            }
            var countryIndex = this._ADDRESS_TYPES.Country.Index();
            if (getShort) {
                return this.GetShortName(results, countryIndex);
            } else {
                return this.GetLongName(results, countryIndex);
            }
        },
        GetSublocalityResult: function(results, addressResult) {
            var lat, lng = null;
            if (addressResult != undefined) {
                lat = addressResult.geometry.location.lat();
                lng = addressResult.geometry.location.lng();
            }

            var sublocality = null;
            for (var i = 0, len = results.length; i < len; i++) {
                var addressType = this.GetAddressType(results[i].types);
                var neLocation = results[i].geometry.viewport.getNorthEast();
                var swLocation = results[i].geometry.viewport.getSouthWest();
                if (addressType != null && addressType.Index() == this._ADDRESS_TYPES.SubLocality.Index() && $.inArray(this._ADDRESS_TYPES.Political.toString(), results[i].types) != -1 && ((lat == null && lng == null) || (swLocation.lat() <= lat && swLocation.lng() <= lng && neLocation.lat() >= lat && neLocation.lng() >= lng))) {
                    sublocality = results[i];
                }
            }
            return sublocality;
        },
        GetCityResult: function(results, addressResult) {
            var city = null;

            var lat, lng = null;
            if (addressResult != undefined) {
                lat = addressResult.geometry.location.lat();
                lng = addressResult.geometry.location.lng();
            }

            for (var i = 0, len = results.length, result = results[i]; i < len; i++, result = results[i]) {
                var addressType = this.GetAddressType(result.types);
                var neLocation = result.geometry.viewport.getNorthEast();
                var swLocation = result.geometry.viewport.getSouthWest();
                if (addressType != null && addressType.Index() >= this._ADDRESS_TYPES.AdminArea3.Index() && addressType.Index() <= this._ADDRESS_TYPES.Locality.Index() && $.inArray(this._ADDRESS_TYPES.Political.toString(), result.types) != -1 && ((lat == null && lng == null) || (swLocation.lat() <= lat && swLocation.lng() <= lng && neLocation.lat() >= lat && neLocation.lng() >= lng))) {
                    city = result;
                }
            }
            return city;
        },
        GetMostPreciseResult: function(results) {
            var address = null;
            var currentIndex = this._ADDRESS_TYPES.Country.Index();
            var maxIndex = this._ADDRESS_TYPES.StreetNumber.Index();

            for (var i = 0, len = results.length, result = results[i]; i < len; i++, result = results[i]) {
                var component = this.GetComponent(result, currentIndex, maxIndex);
                if (component != null) {
                    var addressType = this.GetAddressType(component.types);
                    if (addressType != null && addressType.Index() > currentIndex) {
                        address = result;
                        currentIndex = addressType.Index() + 1;
                    }
                }
            }
            return address;
        },
        GetResultWithType: function(results) {
            if (results === null || results === undefined)
                return null;
            var result = null;
            for (var i = 0; i < results.length; i++) {
                if (results[i] != null && results[i].types != undefined && results[i].types != null && results[i].types.length > 0) {
                    result = results[i];
                    break;
                }
            }
            return result;
        }
    });
/*EOF /Scripts/Roomster/Roomster.Geo.js*/
/*/Scripts/Roomster/Roomster.Search.UI.Geo.js*/
Roomster.Search.UI.Geo =
    function(geoInputId) {
        this.Init();
        if (typeof(geoInputId) == 'string') {
            this._GeoInputId = geoInputId;
        } else {
            this._GeoInput = geoInputId;
        }
        this.InitGeoInput();
    };
$.extend(
    Roomster.Search.UI.Geo.prototype, {
        //_GeoInputId: '',
        _GeoInput: null,
        _Autocomplete: null,
        _LastPlace: null,
        ChangeGeo: function(place) {
            if (place == undefined) {
                this.GetPlaceByGeocoder();
            } else {
                var neLocation = place.geometry.viewport.getNorthEast();
                var swLocation = place.geometry.viewport.getSouthWest();
                var geoFilter = swLocation.lat() + '--' + swLocation.lng() + '--' + neLocation.lat() + '--' + neLocation.lng();
                Roomster.Search.UI.ChangeFormattedText(this._GeoInput.val());
                Roomster.Search.SearchSettings.ClearFilters();
                Roomster.Search.UI.ChangeFilter('Geo', geoFilter);
                this._GeoInput.trigger('geochanged');
            }
        },
        GetPlaceByGeocoder: function() {
            if (this._LastPlace && this._GeoInput.val() == this._LastPlace.formatted_address) {
                this.SetGeoByLastPlace();
            }

            if (this._GeoInput.val().length != 0) {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    'address': this._GeoInput.val()
                }, this._GeocodeCallback);
            } else {
                Roomster.Search.UI.ChangeFormattedText(this._GeoInput.val());
                Roomster.Search.UI.ChangeFilter('Geo', '');
            }

        },
        SetGeoByLastPlace: function() {
            this.ChangeGeo(this._LastPlace);
        },
        _GeocodeCallback: function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var place = Roomster.Geo.GetMostPreciseResult(results && $.isArray(results) && results.length > 0 ? [results[0]] : []);
                if (place != null) {
                    this._LastPlace = place;
                    this.ChangeGeo(place);
                } else {
                    this.AddressIsInvalid();
                }
            } else {
                this.AddressIsInvalid();
            }
        },
        InitGeoInput: function() {
            if (this._GeoInput == null) {
                this._GeoInput = $('#' + this._GeoInputId).first();
            }
            var input = this._GeoInput.get(0);
            if (input.RoomsterGeo) {
                return;
            }
            input.RoomsterGeo = this;
            var options = {};
            if (this._GeoInput.val() == "") {
                this._GeoInput.val(Roomster.Search.SearchSettings.ParseFormattedText(unescape(decodeURI(window.location.href))));
            }
            this._Autocomplete = new google.maps.places.Autocomplete(input, options);
            google.maps.event.addListener(this._Autocomplete, 'place_changed', this.PlaceChanged);

            this._GeoInput.off('click').on('click', function() {
                var input = this;
                input.focus();
                input.setSelectionRange(0, 999);
            });

            this.ApplyWinphoneFix(this._GeoInput);
        },
        SetGeoInput: function(formattedGeo) {
            if (this._GeoInput == null) {
                this._GeoInput = $('#' + this._GeoInputId).first();
            }
            var input = this._GeoInput.get(0);
            if (!formattedGeo || formattedGeo == '') {
                return;
            }
            input.RoomsterGeo = this;
            var options = {};
            if (this._GeoInput.val() == "") {
                this._GeoInput.val(formattedGeo);
            }
            this._Autocomplete = new google.maps.places.Autocomplete(input, options);
            google.maps.event.addListener(this._Autocomplete, 'place_changed', this.PlaceChanged);

            this._GeoInput.off('click').on('click', function() {
                var input = this;
                input.focus();
                input.setSelectionRange(0, 999);
            });

            this.ApplyWinphoneFix(this._GeoInput);
        },
        PlaceChanged: function() {
            var place = this._Autocomplete.getPlace();
            if (place && place.geometry && place.geometry.viewport) {
                if (this._LastPlace == null || (this._LastPlace.geometry.location.lat() != place.geometry.location.lat() && this._LastPlace.geometry.location.lng() != place.geometry.location.lng())) {
                    this._LastPlace = place;
                    this.ChangeGeo(this._LastPlace);
                }
            } else {
                this.GetPlaceByGeocoder();
            }
        },
        ApplyWinphoneFix: function($address) {
            console.log("Apply Winphone Fix");
            if (navigator.userAgent && navigator.userAgent.toLowerCase().indexOf("windows phone") >= 0) {

                // Fix autocomplete selection in Windows Phone 8.1.
                window.setInterval(function() {
                    // A workaround for missing property that was deprecated.
                    $.browser = {
                        msie: (/msie|trident/i).test(navigator.userAgent)
                    };
                    $items = $('.pac-container .pac-item').not('.tracking');
                    $items.addClass('tracking'); // Add event listener once only.
                    $items.mousedown(function(e) {
                        // Get the text selected item.
                        var $item = $(this);
                        var $query = $item.find('.pac-item-query');
                        var text = $query.text() + ', ' + $query.next().text();
                        // Let this event to finish before we continue tricking.
                        setTimeout(function() {
                            // Check if Autocomplete works as expected and exit if so.
                            if ($address.val() == text) {
                                console.log('exit');
                                return
                            }
                            $address.trigger("focus");
                            // Press key down until the clicked item is selected.
                            var interval = setInterval(function() {
                                $address.simulate("keydown", {
                                    keyCode: 40
                                });
                                // If selected item is not that clicked one then continue;
                                var $query = $('.pac-container .pac-item-selected .pac-item-query:first ');
                                if (text != $query.text() + ', ' + $query.next().text()) return;
                                // Found the clicked item, choice it and finish;
                                $address.simulate("keydown", {
                                    keyCode: 13
                                });
                                $address.blur();
                                clearInterval(interval);
                            }, 1);
                        }, 1);
                    });
                }, 500);
            }
        },
        AddressIsInvalid: function() {
            jQuery.facebox('Address is invalid!');
        },
        Init: function() {
            this.PlaceChanged = $.proxy(this.PlaceChanged, this);
            this._GeocodeCallback = $.proxy(this._GeocodeCallback, this);

        }
    });
/*EOF /Scripts/Roomster/Roomster.Search.UI.Geo.js*/
/*/Scripts/Roomster/Roomster.SavedSearch.js*/
$.extend(
    Roomster.SavedSearch, {
        ActionPath: '/Savesearch/',
        GetAjaxSettings: function() {
            return {
                url: this.ActionPath,
                cache: false,
                dataType: 'json',
                success: function(data) {
                    if (this.xCallback != undefined) {
                        this.xCallback(data.result);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (this.xError != undefined) {
                        this.xError(textStatus, errorThrown);
                    }
                }
            };
        },
        GetList: function(serviceType, callback, errorCallback) {
            this.RunIt({
                url: this.ActionPath + 'Get',
                data: {
                    serviceType: serviceType
                },
                xCallback: callback,
                xError: errorCallback,
                type: 'GET'
            });
        },
        Delete: function(id, callback, errorCallback) {
            this.RunIt({
                url: this.ActionPath + 'Delete',
                data: {
                    id: id
                },
                xCallback: callback,
                xError: errorCallback,
                type: 'POST'
            });
        },
        Add: function(searchPath, name, serviceType, callback, errorCallback) {

            this.RunIt({
                url: this.ActionPath + 'Add',
                data: {
                    searchPath: searchPath,
                    name: name,
                    serviceType: serviceType
                },
                xCallback: callback,
                xError: errorCallback,
                type: 'POST'
            });
        },
        RunIt: function(settings) {
            $.ajax($.extend(this.GetAjaxSettings(), settings));
        }
    });
/*EOF /Scripts/Roomster/Roomster.SavedSearch.js*/
/*/Scripts/Roomster/Roomster.SavedSearch.UI.js*/
function savedSearchItem() {
    this.Id = 0;
    this.Name = '';
    this.SearchUrl = '';
    this.IsOriginal = false;
    this.IsDefault = false;
}

function viewControls() {
    this.Container = null;
    this.Field = null;
    this.Action = null;
}

$.extend(
    Roomster.SavedSearch.UI, {
        SearchUrl: '',
        ServiceType: 0,
        SavedSearchItems: new Array(),
        OriginalItem: null,
        ListItemTemplateName: 'savedSearchItemTemplate',
        ListItemTemplate: null,
        Views: {
            Container: null,
            DefaultView: null,
            EditView: null,
            SaveView: null,
            ListView: null
        },
        Initialized: false,
        Init: function() {
            if (Roomster.SavedSearch.UI.Initialized === true)
                return;

            Roomster.SavedSearch.UI.InitViews();
            Roomster.SavedSearch.UI.InitTemplates();

            Roomster.SavedSearch.UI.Initialized = true;
        },
        InitSaveSearchControls: function(searchUrl) {
            Roomster.SavedSearch.UI.InitAutoHideList();

            if (Roomster.SavedSearch.UI.ServiceType != Roomster.Search.SearchSettings.GetServiceType()) {
                Roomster.SavedSearch.UI.CheckAndProcessServiceTypeChanges(searchUrl);
            } else if (Roomster.SavedSearch.UI.SearchUrl != searchUrl) {
                Roomster.SavedSearch.UI.SearchUrl = searchUrl;
                Roomster.SavedSearch.UI.ShowEditView();
            }
        },
        LoadSavedSearchList: function(serviceType, searchUrl) {
            Roomster.SavedSearch.UI.SavedSearchItems.length = 0;

            Roomster.SavedSearch.GetList(serviceType, function(data) {
                if (data != null) {
                    for (var counter = 0; counter < data.length; counter++) {
                        var item = {
                            Id: data[counter].Id,
                            Name: data[counter].Name,
                            SearchUrl: data[counter].RelativePath,
                            IsDefault: data[counter].IsDefault
                        };

                        Roomster.SavedSearch.UI.SavedSearchItems.push(item);
                    }
                }

                Roomster.SavedSearch.UI.SetDefaultSavedSearchItem(searchUrl);
                var savedSearchItem = Roomster.SavedSearch.UI.GeSavedSearchItem(searchUrl);

                if (savedSearchItem != null) {
                    Roomster.SavedSearch.UI.SetDefaultViewText(savedSearchItem.Name);
                    Roomster.SavedSearch.UI.ShowDefaultView();
                } else {
                    Roomster.SavedSearch.UI.ShowEditView();
                }

                Roomster.SavedSearch.UI.SearchUrl = searchUrl;
                Roomster.SavedSearch.UI.ServiceType = serviceType;

                Roomster.SavedSearch.UI.RenderListView();
            });
        },
        RenderListView: function() {
            if (window.isMobileUI)
                return;

            $('ul', Roomster.SavedSearch.UI.Views.ListView.Container).empty();

            if (Roomster.SavedSearch.UI.SavedSearchItems.length == 0) {
                $(Roomster.SavedSearch.UI.Views.ListView.Container).hide();
            }

            for (var counter = 0; counter < Roomster.SavedSearch.UI.SavedSearchItems.length; counter++) {
                var itemData = Roomster.SavedSearch.UI.SavedSearchItems[counter];
                $('ul', Roomster.SavedSearch.UI.Views.ListView.Container).append(Roomster.Templates.Apply(Roomster.SavedSearch.UI.ListItemTemplate, itemData));
            }
        },
        TryAddOriginalItem: function() {
            if (Roomster.SavedSearch.UI.OriginalItem != null) {

                for (var counter = 0; counter < Roomster.SavedSearch.UI.SavedSearchItems.length; counter++) {
                    if (Roomster.SavedSearch.UI.SavedSearchItems[counter].Name == Roomster.SavedSearch.UI.OriginalItem.Name) {
                        return;
                    }
                }

                Roomster.SavedSearch.UI.SavedSearchItems.splice(0, 0, Roomster.SavedSearch.UI.OriginalItem);
            }
        },
        CheckAndProcessServiceTypeChanges: function(searchUrl) {
            var currentServiceType = Roomster.Search.SearchSettings.GetServiceType();

            if (currentServiceType !== Roomster.SavedSearch.UI.ServiceType) {
                Roomster.SavedSearch.UI.LoadSavedSearchList(currentServiceType, searchUrl);
            }
        },
        GetDefaultSavedSearchName: function(searchUrl) {
            for (var counter = 0; counter < Roomster.SavedSearch.UI.SavedSearchItems.length; counter++) {
                if (Roomster.SavedSearch.UI.SavedSearchItems[counter].SearchUrl == searchUrl) {
                    return Roomster.SavedSearch.UI.SavedSearchItems[counter].Name;
                }
            }

            return '';
        },
        GeSavedSearchItem: function(searchUrl) {
            for (var counter = 0; counter < Roomster.SavedSearch.UI.SavedSearchItems.length; counter++) {
                if (Roomster.SavedSearch.UI.SavedSearchItems[counter].SearchUrl == searchUrl) {
                    return Roomster.SavedSearch.UI.SavedSearchItems[counter];
                }
            }

            return null;
        },
        SetDefaultSavedSearchItem: function(searchUrl) {
            var defaultItem = Roomster.SavedSearch.UI.GeSavedSearchItem(searchUrl);

            if (defaultItem != null) {
                defaultItem.IsDefault = true;
                defaultItem.IsOriginal = true;
                Roomster.SavedSearch.UI.OriginalItem = defaultItem;
            }
        },
        SetDefaultViewText: function(text) {
            $('.title', Roomster.SavedSearch.UI.Views.DefaultView.Field).text(text);
        },
        InitTemplates: function() {
            Roomster.SavedSearch.UI.ListItemTemplate = $('#' + Roomster.SavedSearch.UI.ListItemTemplateName).text();

        },
        InitViews: function() {
            if (Roomster.SavedSearch.UI.Views.Main == null) {
                Roomster.SavedSearch.UI.Views.Container = $('.savedSearchContainer', '.filterBlock')[0];

                if (Roomster.SavedSearch.UI.Views.Container != null) {
                    Roomster.SavedSearch.UI.InitDefaultView();
                    Roomster.SavedSearch.UI.InitSaveView();
                    Roomster.SavedSearch.UI.InitEditView();
                    Roomster.SavedSearch.UI.InitListView();
                }
            }
        },
        InitDefaultView: function() {
            Roomster.SavedSearch.UI.Views.DefaultView = new viewControls();
            Roomster.SavedSearch.UI.Views.DefaultView.Container = $('.defaultSavedSearchView', Roomster.SavedSearch.UI.Views.Container)[0];

            if (Roomster.SavedSearch.UI.Views.DefaultView.Container != null) {
                Roomster.SavedSearch.UI.Views.DefaultView.Field = $('.field', Roomster.SavedSearch.UI.Views.DefaultView.Container)[0];
                Roomster.SavedSearch.UI.Views.DefaultView.Action = $('.action', Roomster.SavedSearch.UI.Views.DefaultView.Container)[0];
            }
        },
        InitEditView: function() {
            Roomster.SavedSearch.UI.Views.EditView = new viewControls();
            Roomster.SavedSearch.UI.Views.EditView.Container = $('.editSavedSearchView', Roomster.SavedSearch.UI.Views.Container)[0];

            if (Roomster.SavedSearch.UI.Views.EditView.Container != null) {
                Roomster.SavedSearch.UI.Views.EditView.Field = $('.field', Roomster.SavedSearch.UI.Views.EditView.Container)[0];
                Roomster.SavedSearch.UI.Views.EditView.Action = $('.action', Roomster.SavedSearch.UI.Views.EditView.Container)[0];
            }
        },
        InitSaveView: function() {
            Roomster.SavedSearch.UI.Views.SaveView = new viewControls();
            Roomster.SavedSearch.UI.Views.SaveView.Container = $('.saveSavedSearchView', Roomster.SavedSearch.UI.Views.Container)[0];

            if (Roomster.SavedSearch.UI.Views.SaveView.Container != null) {
                Roomster.SavedSearch.UI.Views.SaveView.Field = $('.field', Roomster.SavedSearch.UI.Views.SaveView.Container);
                Roomster.SavedSearch.UI.Views.SaveView.Action = $('.action', Roomster.SavedSearch.UI.Views.SaveView.Container)[0];
            }
        },
        InitListView: function() {
            Roomster.SavedSearch.UI.Views.ListView = new viewControls();
            Roomster.SavedSearch.UI.Views.ListView.Container = $('.listSavedSearchView', Roomster.SavedSearch.UI.Views.Container)[0];
        },
        HideAllViews: function() {
            if (window.isMobileUI)
                return;
            $(Roomster.SavedSearch.UI.Views.DefaultView.Field).unbind('click');
            $(Roomster.SavedSearch.UI.Views.DefaultView.Action).unbind('click');

            $(Roomster.SavedSearch.UI.Views.EditView.Field).unbind('click');
            $(Roomster.SavedSearch.UI.Views.EditView.Action).unbind('click');

            $(Roomster.SavedSearch.UI.Views.SaveView.Action).unbind('click');

            $(Roomster.SavedSearch.UI.Views.DefaultView.Container).hide();
            $(Roomster.SavedSearch.UI.Views.EditView.Container).hide();
            $(Roomster.SavedSearch.UI.Views.SaveView.Container).hide();
            $(Roomster.SavedSearch.UI.Views.ListView.Container).hide();
        },
        ShowDefaultView: function() {
            Roomster.SavedSearch.UI.HideAllViews();
            $(Roomster.SavedSearch.UI.Views.DefaultView.Container).show();

            var field = Roomster.SavedSearch.UI.Views.DefaultView.Field;
            var action = Roomster.SavedSearch.UI.Views.DefaultView.Action;

            $(action).removeClass('openList');

            $(action).click(function() {
                Roomster.SavedSearch.UI.ToggleListView();

                if ($(Roomster.SavedSearch.UI.Views.ListView.Container).is(':visible')) {
                    $(action).addClass('openList');
                } else {
                    $(action).removeClass('openList');
                }
            });

            Roomster.SavedSearch.UI.ShowCounter(Roomster.SavedSearch.UI.Views.DefaultView.Action);
        },
        ShowEditView: function() {
            if (window.isMobileUI)
                return;

            Roomster.SavedSearch.UI.HideAllViews();

            $(Roomster.SavedSearch.UI.Views.EditView.Container).show();

            var field = Roomster.SavedSearch.UI.Views.EditView.Field;
            var action = Roomster.SavedSearch.UI.Views.EditView.Action;

            $(action).removeClass('openList');
            Roomster.SavedSearch.UI.ShowCounter(Roomster.SavedSearch.UI.Views.EditView.Action);
            $(field).click(function() {
                Roomster.SavedSearch.UI.ShowSaveView();
            });

            $(action).click(function() {
                Roomster.SavedSearch.UI.ToggleListView();

                if ($(Roomster.SavedSearch.UI.Views.ListView.Container).is(':visible')) {
                    $(action).addClass('openList');
                } else {
                    $(action).removeClass('openList');
                }
            });
        },
        ShowSaveView: function() {
            Roomster.SavedSearch.UI.HideAllViews();
            $(Roomster.SavedSearch.UI.Views.SaveView.Container).show();

            var field = Roomster.SavedSearch.UI.Views.SaveView.Field;
            var action = Roomster.SavedSearch.UI.Views.SaveView.Action;

            $(field).focus();

            $(action).click(function() {
                if (field.val().length > 0) {
                    Roomster.SavedSearch.Add(Roomster.SavedSearch.UI.SearchUrl, field.val(), Roomster.SavedSearch.UI.ServiceType, function(data) {
                        var savedItem = new savedSearchItem();
                        savedItem.Id = data;
                        savedItem.Name = field.val();
                        savedItem.SearchUrl = Roomster.SavedSearch.UI.SearchUrl;

                        Roomster.SavedSearch.UI.SavedSearchItems.splice(0, 0, savedItem);
                        $('ul', Roomster.SavedSearch.UI.Views.ListView.Container).prepend(Roomster.Templates.Apply(Roomster.SavedSearch.UI.ListItemTemplate, savedItem));

                        field.val('');
                        Roomster.SavedSearch.UI.SetDefaultViewText(savedItem.Name);
                        Roomster.SavedSearch.UI.ShowDefaultView();
                    });
                }
            });
        },
        ToggleListView: function() {
            if (Roomster.SavedSearch.UI.SavedSearchItems.length > 0) {
                $(Roomster.SavedSearch.UI.Views.ListView.Container).toggle();
            }
        },
        DeleteListItem: function(id) {
            for (var counter = 0; counter < Roomster.SavedSearch.UI.SavedSearchItems.length; counter++) {
                if (Roomster.SavedSearch.UI.SavedSearchItems[counter].Id == id) {
                    Roomster.SavedSearch.UI.SavedSearchItems.splice(counter, 1);
                    $('.' + id, Roomster.SavedSearch.UI.Views.ListView.Container).remove();
                }
            }
            Roomster.SavedSearch.UI.ShowCounter(Roomster.SavedSearch.UI.Views.DefaultView.Action);
            Roomster.SavedSearch.UI.ShowEditView();
        },
        ShowCounter: function(action) {
            if (Roomster.SavedSearch.UI.SavedSearchItems.length > 0) {
                $(".btnTitleToggle").show();
                $('.counter', action).show();
                $('.counter', action).text(Roomster.SavedSearch.UI.SavedSearchItems.length);
            } else {
                $(".btnTitleToggle").hide();
            }

        },
        InitAutoHideList: function() {
            if (window.isMobileUI)
                return;
            $(document).mouseup(function(e) {
                if (!$(Roomster.SavedSearch.UI.Views.Container).is(e.target) && $(Roomster.SavedSearch.UI.Views.Container).has(e.target).length === 0) {
                    $(Roomster.SavedSearch.UI.Views.ListView.Container).hide();
                    $(Roomster.SavedSearch.UI.Views.EditView.Action).removeClass('openList');
                    $(Roomster.SavedSearch.UI.Views.DefaultView.Action).removeClass('openList');
                }
            });
        }
    });

if (!window.isMobileUI) {
    $(function() {
        Roomster.SavedSearch.UI.Init();
    });
} else {
    $(document).on('pagecontainershow', function(event, ui) {
        Roomster.SavedSearch.UI.Init();
    });
}
/*EOF /Scripts/Roomster/Roomster.SavedSearch.UI.js*/
/*/Scripts/Roomster/Roomster.Bookmark.js*/
(function(ctx) {
    var self;

    $.extend(
        self = ctx.Bookmark = ctx.Bookmark || {}, {
            Controller: '/Bookmarks/',
            BaseSendAjaxData: function(action, data, successHandler, errorHandler) {
                var ajaxObject = {
                    url: self.Controller + action,
                    contentType: "application/json; charset=utf-8",
                    traditional: true,
                    dataType: 'json',
                    type: 'POST',
                    timeout: 30000, // 30 seconds
                    data: JSON.stringify(data),
                    success: successHandler,
                    error: errorHandler
                };

                $.ajax(ajaxObject);
            },
            GetCounters: function(successHandler, errorHandler) {
                self.BaseSendAjaxData("GetCounters", null, successHandler, errorHandler);
            },
            AddBookmark: function(service, objectId, successHandler, errorHandler) {
                self.BaseSendAjaxData("AddBookmark", {
                    service: service,
                    objectId: objectId
                });
            },
            DeleteBookmark: function(service, objectId, successHandler, errorHandler) {
                self.BaseSendAjaxData("DeleteBookmark", {
                    service: service,
                    objectId: objectId
                });
            },
            ClearBookmarks: function(data, successHandler, errorHandler) {
                self.BaseSendAjaxData("ClearBookmarks", data, successHandler, errorHandler);
            }
        });
})(Roomster);

/*EOF /Scripts/Roomster/Roomster.Bookmark.js*/
/*/Scripts/Roomster/Roomster.Bookmark.UI.BookmarkButton.js*/
(function(ctx) {
    ctx.UI = ctx.UI || {};

    var self;

    $.extend(
        self = ctx.UI.BookmarkButton = ctx.UI.BookmarkButton || {}, {
            BOOKMARKED: Roomster.Resources.GetTranslation('Bookmarked'),
            REMOVE: Roomster.Resources.GetTranslation('Remove'),
            BOOKMARK: Roomster.Resources.GetTranslation('Bookmark'),
            VALUE_ATTR: 'text-value',
            BUTTON_CLASS: 'bookmarkInList',
            ButtonStyle: {
                Bookmarked: 'Bookmarked',
                BookmarkGray: 'BookmarkGray',
                BookmarkDark: 'BookmarkDark',
                Remove: "Remove"
            },
            setButtonStyle: function($el, buttonStyle) {
                $el.removeClass();
                $el.addClass(self.BUTTON_CLASS);
                $el.addClass(self.BUTTON_CLASS + buttonStyle);
            },
            SearchResultItemHoverIn: function($el, buttonSelector) {

                function SearchResultItemHoverInCore() {
                    var $btn = $el.find(buttonSelector);
                    if ($btn.text() != self.BOOKMARKED && $btn.text() != self.REMOVE) // check on REMOVE too, because $bookmarkInList.hover "In" may be called first.
                    {
                        $btn.text(self.BOOKMARK);
                        self.setButtonStyle($btn, self.ButtonStyle.BookmarkGray);
                        $btn.show();
                    }
                }

                if (jQuery.support.touch) {
                    // Use set timeout for compatible with touch devices
                    setTimeout(SearchResultItemHoverInCore, 500);
                } else {
                    SearchResultItemHoverInCore();
                }
            },
            SearchResultItemHoverOut: function($el, buttonSelector) {
                var $btn = $el.find(buttonSelector);
                if ($btn.text() != self.BOOKMARKED) {
                    $btn.hide();
                }
            },
            ButtonHoverIn: function($el) {
                var text = $el.text();
                $el.attr(self.VALUE_ATTR, text);
                if (text == self.BOOKMARKED) {
                    $el.text(self.REMOVE);
                    self.setButtonStyle($el, self.ButtonStyle.Remove);
                } else {
                    self.setButtonStyle($el, self.ButtonStyle.BookmarkDark);
                }
            },
            ButtonHoverOut: function($el) {
                var storedTextValue = $el.attr(self.VALUE_ATTR);
                $el.text(storedTextValue);
                self.setButtonStyle($el, storedTextValue == self.BOOKMARKED ? self.ButtonStyle.Bookmarked : self.ButtonStyle.BookmarkGray);
            },
            ButtonClick: function($el) {
                var data = {
                    service: $el.attr('service-id'),
                    objectId: $el.attr('object-id')
                };

                if ($el.text() == self.BOOKMARK) {
                    $el.attr(self.VALUE_ATTR, self.BOOKMARKED);
                    $el.text(self.REMOVE);
                    self.setButtonStyle($el, self.ButtonStyle.Remove);
                    Roomster.Bookmark.AddBookmark(data.service, data.objectId);
                } else {
                    $el.attr(self.VALUE_ATTR, self.BOOKMARK);
                    $el.text(self.BOOKMARK);
                    self.setButtonStyle($el, self.ButtonStyle.BookmarkDark);
                    Roomster.Bookmark.DeleteBookmark(data.service, data.objectId);
                }
            },
            BindHandlers: function(itemClassSelector, buttonsSelector, handlers) {
                $(itemClassSelector).hover(
                    function() {
                        handlers.itemHoverIn && handlers.itemHoverIn($(this), buttonsSelector);
                    },
                    function() {
                        handlers.itemHoverOut && handlers.itemHoverOut($(this), buttonsSelector);
                    });

                $(buttonsSelector).hover(
                    function() {
                        handlers.buttonHoverIn && handlers.buttonHoverIn($(this));
                    },
                    function() {
                        handlers.buttonHoverOut && handlers.buttonHoverOut($(this));
                    });

                $(buttonsSelector).click(function() {
                    handlers.buttonClick && handlers.buttonClick($(this));
                });
            },
            GetDefaultHandlers: function(handlers) {
                handlers = handlers || {};
                handlers.itemHoverIn = self.SearchResultItemHoverIn;
                handlers.itemHoverOut = self.SearchResultItemHoverOut;

                handlers.buttonHoverIn = self.ButtonHoverIn;
                handlers.buttonHoverOut = self.ButtonHoverOut;
                handlers.buttonClick = self.ButtonClick;
                return handlers;
            },
            RemoveItemHoverHandlers: function(handlers) {
                handlers.itemHoverIn = null;
                handlers.itemHoverOut = null;
                return handlers;
            },
            BindHandlersForNotHideOnHover: function(itemClassSelector, buttonsSelector) {
                var handlers = {};
                self.GetDefaultHandlers(handlers);
                self.RemoveItemHoverHandlers(handlers);
                self.BindHandlers(itemClassSelector, buttonsSelector, handlers);
            },
            UnbindHandlers: function(itemSelector, buttonsSelector) {
                $(itemSelector).unbind('mouseenter mouseleave');
                $(buttonsSelector).unbind('mouseenter mouseleave click');
            }
        });
})(Roomster.Bookmark);

/*EOF /Scripts/Roomster/Roomster.Bookmark.UI.BookmarkButton.js*/
/*/Scripts/Roomster/Roomster.Bookmark.UI.BookmarkMenu.js*/
(function(ctx) {
    ctx.UI = ctx.UI || {};

    var self;

    $.extend(
        self = ctx.UI.BookmarkMenu = ctx.UI.BookmarkMenu || {}, {
            wasCleared: false,
            appUrl: "",
            formatCounter: function(iCounter) {
                return iCounter <= 100 ? iCounter.toString() : "100+";
            },
            getCountersHandler: function(data) {
                // Set all counter labels to 0
                $('[bookmark-counter="true"]').text(0);

                // Set new values from server to counter labels
                for (var i = 0, item; item = data[i]; i++) {
                    $('[bookmark-type="' + item.Group.BookmarkType + '"][service-type="' + item.Group.Service + '"][bookmark-checkbox!="true"]').text(self.formatCounter(item.Value));
                }
            },

            clearBookmarksHandler: function(data) {
                // Refresh counters label
                Roomster.Bookmark.GetCounters(self.getCountersHandler);
                $(".bkmPopup .guideLine").hide();
                $(".bkmPopup .succesfullChanges").show();
                wasCleared = true;
            },
            clearBookmarksErrorHandler: function(data) {
                $(".bkmPopup .guideLine").hide();
                $(".bkmPopup .errorMessage").show();
            },
            hideNotificationMessages: function() {
                $(".messBoxNotepadStyle").hide();
                $(".succesfullChanges").hide();
                $(".bkmPopup .errorMessage").hide();
            },
            showBookmarksMenu: function(e) {
                Roomster.Bookmark.GetCounters(self.getCountersHandler);
                $('.bookmarksMenu').toggle();
                $('.bookmarksNavItem').toggleClass('selected');
            },
            btnClearBookmarksOnClickHandler: function(e) {
                var requestData = {
                    groups: []
                };

                self.hideNotificationMessages();

                $('[bookmark-checkbox="true"]').each(function(idx, el) {
                    var $el = $(el);
                    if ($el.is(':checked')) {
                        requestData.groups.push({
                            BookmarkType: $el.attr("bookmark-type"),
                            Service: $el.attr("service-type")
                        });

                        $el.attr('checked', false);
                    }
                });

                if (!requestData.groups.length) {
                    $(".bkmPopup .errorNotifyBox").show();
                    return;
                }

                Roomster.Bookmark.ClearBookmarks(requestData, self.clearBookmarksHandler, self.clearBookmarksErrorHandler);
            },
            bkmPopupClose: function() {
                $('.bkmPopup').dialog("close");
            },
            btnCancelClearBookmarksOnClickHandler: function(e) {
                self.bkmPopupClose();
            },
            menuItemClickHandler: function(e) {
                var el = $(e.currentTarget || e.srcElement).find(".bookmarkItem");
                var serviceType = el.attr('service-type');
                var bookmarkType = el.attr("bookmark-type");
                if (serviceType && bookmarkType) {
                    var s = ['Search/', serviceType, '/1/Bookmark--', bookmarkType].join('');
                    if (!window.isMobileUI) {
                        window.location.href = self.appUrl + s;
                    } else {
                        $(":mobile-pagecontainer").pagecontainer("change", $.url.home() + s);
                    }

                    $('.bookmarksMenu').hide();
                    $('.bookmarksNavItem').removeClass('selected');
                    return;
                }
            },
            showClearBookmarkDlg: function() {
                self.wasCleared = false;
                self.hideNotificationMessages();
                $(".messBoxNotepadStyle").show();
                $('[bookmark-checkbox="true"]').attr('checked', false);
                $('.bookmarksMenu').hide();
                $('.bookmarksNavItem').removeClass('selected');
                $(".bkmPopup .guideLine").show();
                $(".bkmPopup .succesfullChanges").hide();
                $(".bkmPopup .errorMessage").hide();
                $('.bkmPopup').dialog({
                    width: 605,
                    dialogClass: 'commonDialog',
                    modal: true,
                    resizable: false,
                    draggable: false,
                    close: function(event, ui) {
                        if (self.wasCleared)
                            window.location.reload(true);
                    }
                });
            }
        });
})(Roomster.Bookmark);
/*EOF /Scripts/Roomster/Roomster.Bookmark.UI.BookmarkMenu.js*/
/*/Scripts/Roomster/Roomster.DateTime.js*/
;
$.extend(
    Roomster.DateTime, {
        _DAY: 1000 * 60 * 60 * 24,
        _HOUR: 1000 * 60 * 60,
        _MINUTE: 1000 * 60,
        _SECOND: 1000,
        _ToTimeSpan: function(date) {
            return Object.prototype.toString.call(date) === "[object Date]" ? date.getTime() : date;
        },
        ToHours: function(timespan) {
            timespan = this._ToTimeSpan(timespan);
            return Math.round(timespan / this._HOUR);
        },
        ToSeconds: function(timespan) {
            timespan = this._ToTimeSpan(timespan);
            return Math.round(timespan / this._SECOND);
        },
        ToMinutes: function(timespan) {
            timespan = this._ToTimeSpan(timespan);
            return Math.round(timespan / this._MINUTE);
        },
        ToDays: function(timespan) {
            timespan = this._ToTimeSpan(timespan);
            return Math.round(timespan / this._DAY);
        },
        ToHumanFriendly: function(date, prefix) {
            date = Roomster.DateTime.Convert(date);
            var dateDiff = new Date() - date.getTime();
            var postfix = "";
            prefix = !prefix ? "" : prefix;
            if (this.ToSeconds(dateDiff) <= 0) {
                postfix = " 0 secs ago";
            } else if (this.ToSeconds(dateDiff) < 60) {
                postfix = this.ToSeconds(dateDiff) + " secs ago";
            } else if (this.ToMinutes(dateDiff) < 60) {
                postfix = this.ToMinutes(dateDiff) + " mins ago";
            } else if (this.ToHours(dateDiff) < 24) {
                postfix = this.ToHours(dateDiff) + " hrs " + new Date(dateDiff).getMinutes() + " mins ago";
            } else if (this.ToDays(dateDiff) <= 30) {
                postfix = this.ToDays(dateDiff) + " days ago";
            }
            return postfix !== "" ? prefix + postfix : "";
        },
        ToFriendlyTimeDiff: function(date, prefix, showAll) {
            date = Roomster.DateTime.Convert(date);
            var dateDiff = new Date() - date.getTime();
            var postfix = "";
            prefix = !prefix ? "" : prefix;
            if (this.ToSeconds(dateDiff) <= 0) {
                postfix = Roomster.Resources.GetTranslation('now');
            } else if (this.ToSeconds(dateDiff) < 60) {
                postfix = this.ToSeconds(dateDiff) + " " + Roomster.Resources.GetTranslation('secs');
            } else if (this.ToMinutes(dateDiff) < 60) {
                postfix = this.ToMinutes(dateDiff) + " " + Roomster.Resources.GetTranslation('mins');
            } else if (this.ToHours(dateDiff) < 24) {
                postfix = this.ToHours(dateDiff) + " " + Roomster.Resources.GetTranslation('hrs');
            } else if (this.ToHours(dateDiff) < 48) {
                postfix = Roomster.Resources.GetTranslation('Yesterday') + " " + Roomster.Resources.GetTranslation('at') + " " + moment(date).format('LT');
            } else if (this.ToDays(dateDiff) <= 30) {
                postfix = moment(date).format('ll') + " " + Roomster.Resources.GetTranslation('at') + " " + moment(date).format('LT');
            } else if (showAll == true && this.ToDays(dateDiff) <= 90) {
                postfix = moment(date).format('ll');
            } else if (showAll == true && this.ToDays(dateDiff) > 90) {
                postfix = moment(date).format('ll');
            }

            return postfix !== "" ? prefix + postfix : "";

        },
        ToFriendlyTimeDiffShort: function(date, prefix) {
            date = Roomster.DateTime.Convert(date);
            var dateDiff = new Date() - date.getTime();
            var postfix = "";
            prefix = !prefix ? "" : prefix;

            if (this.ToSeconds(dateDiff) <= 0) {
                postfix = Roomster.Resources.GetTranslation('now');
            } else if (this.ToSeconds(dateDiff) < 60) {
                postfix = this.ToSeconds(dateDiff) + " " + Roomster.Resources.GetTranslation('secs');
            } else if (this.ToMinutes(dateDiff) < 60) {
                postfix = this.ToMinutes(dateDiff) + " " + Roomster.Resources.GetTranslation('mins');
            } else if (this.ToHours(dateDiff) < 24) {
                postfix = this.ToHours(dateDiff) + " " + Roomster.Resources.GetTranslation('hrs');
            } else if (this.ToDays(dateDiff) < 7) {
                postfix = moment(date).format('ddd');
            } else if (this.ToDays(dateDiff) < 365) {
                postfix = moment(date).format('ll');
            } else {
                postfix = moment(date).format('l');
            }

            return postfix !== "" ? prefix + postfix : "";

        },
        Age: function(date) {
            date = Roomster.DateTime.Convert(date);
            var years = new Date().getFullYear() - date.getFullYear();
            if (new Date().getMonth() < date.getMonth() || (new Date().getMonth() == date.getMonth() && new Date().getDay() < date.getDay())) {
                years--;
            }
            return years;
        },
        FormatedDate: function(date) {
            var result = '';

            result = date.getMonth() + 1;

            if (result < 10) {
                result = '0' + result;
            }

            return '' + result + '/' + date.getDate() + '/' + date.getFullYear();
        },
        Format: function(date, format) {
            date = Roomster.DateTime.Convert(date);
            return date.format(format);
        },
        AvailabilityFormat: function(date, format, outOfDateExplanation) {
            date = Roomster.DateTime.Convert(date);
            var diff = date - new Date();
            //return Math.round(diff / 1000 / 60 / 60 / 24) > 0 ? Roomster.DateTime.Format(date, format) : outOfDateExplanation;
            //return Math.round(diff / 1000 / 60 / 60 / 24) > 0 ? date.toLocaleDateString(Roomster.Resources.GetTranslation('culture'), { "month": "short", "day": "numeric", "year": "numeric" }) : outOfDateExplanation;
            return Math.round(diff / 1000 / 60 / 60 / 24) > 0 ? moment(date).format('ll') : outOfDateExplanation;
        },
        Convert: function(date) {
            var D = new Date('2011-06-02T09:34:29+02:00');
            if (!D || +D !== 1307000069000) {
                this.Convert = function(s) {
                    if (typeof(s) == "string") {
                        var day, tz,
                            rx = /^(\d{4}\-\d\d\-\d\d([tT ][\d:\.]*)?)([zZ]|([+\-])(\d\d):(\d\d))?$/,
                            p = rx.exec(s) || [];
                        if (p[1]) {
                            day = p[1].split(/\D/);
                            for (var i = 0, L = day.length; i < L; i++) {
                                day[i] = parseInt(day[i], 10) || 0;
                            };
                            day[1] -= 1;
                            day = new Date(Date.UTC.apply(Date, day));
                            if (!day.getDate()) return NaN;
                            if (p[5]) {
                                tz = (parseInt(p[5], 10) * 60);
                                if (p[6]) tz += parseInt(p[6], 10);
                                if (p[4] == '+') tz *= -1;
                                if (tz) day.setUTCMinutes(day.getUTCMinutes() + tz);
                            }
                            return day;
                        }
                        return NaN;
                    } else {
                        return s;
                    }
                };
            } else {
                this.Convert = function(s) {
                    return typeof(s) == "string" ? new Date(s) : s;
                };
            }
            return this.Convert(date);
        }
    }
);

/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function() {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function(val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function(date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d: d,
                dd: pad(d),
                ddd: dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m: m + 1,
                mm: pad(m + 1),
                mmm: dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy: String(y).slice(2),
                yyyy: y,
                h: H % 12 || 12,
                hh: pad(H % 12 || 12),
                H: H,
                HH: pad(H),
                M: M,
                MM: pad(M),
                s: s,
                ss: pad(s),
                l: pad(L, 3),
                L: pad(L > 99 ? Math.round(L / 10) : L),
                t: H < 12 ? "a" : "p",
                tt: H < 12 ? "am" : "pm",
                T: H < 12 ? "A" : "P",
                TT: H < 12 ? "AM" : "PM",
                Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

        return mask.replace(token, function($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default": "ddd mmm dd yyyy HH:MM:ss",
    shortDate: "m/d/yy",
    mediumDate: "mmm d, yyyy",
    longDate: "mmmm d, yyyy",
    fullDate: "dddd, mmmm d, yyyy",
    shortTime: "h:MM TT",
    mediumTime: "h:MM:ss TT",
    longTime: "h:MM:ss TT Z",
    isoDate: "yyyy-mm-dd",
    isoTime: "HH:MM:ss",
    isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};

// For convenience...
Date.prototype.format = function(mask, utc) {
    return dateFormat(this, mask, utc);
};

/*EOF /Scripts/Roomster/Roomster.DateTime.js*/
/*/Scripts/Roomster/Roomster.Tags.UI.js*/
Roomster.WaitJquery(
    function() {
        $.extend(
            Roomster.Tags.UI, {
                //_bubbleTheme: '',
                AddRatePopup: function() {
                    $(document).ready(
                        function() {
                            $('.popular_tag').each(
                                function() {
                                    var tagRate = $('a', $(this)).attr('tagrate');
                                    if (tagRate != undefined && tagRate.length != 0) {
                                        $(this).CreateBubblePopup({
                                            position: 'top',
                                            align: 'left',
                                            tail: {
                                                align: 'left'
                                            },
                                            divStyle: {
                                                margin: '-10px 0px 0px -10px'
                                            },
                                            innerHtml: tagRate + ' Tags',
                                            themeName: 'grey',
                                            themePath: Roomster.Tags.UI._bubbleTheme,
                                            closingSpeed: 0,
                                            manageMouseEvents: false
                                        });
                                    }
                                });
                            $('.popular_tag').hover(
                                function(event) {
                                    $('.popular_tag').HideAllBubblePopups();
                                    $(this).ShowBubblePopup();
                                    stop_bubbling(event);
                                    event.preventDefault();
                                },
                                function(event) {
                                    $('.popular_tag').HideAllBubblePopups();
                                    stop_bubbling(event);
                                    event.preventDefault();
                                }
                            );
                        }
                    );
                },
                HandleTagToTextInput: function() {
                    $(".popular_tag a").on("click", function() {
                        var tag = $(this);
                        tag.closest(".fieldHolder").find(".textInput").filter(function() {
                            var val = $.trim(this.value);
                            if (val == "")
                                return true;
                            return false;
                        }).first().val(tag.text());
                        return false;
                    });

                    $('.mobile_tag').on('click', function() {
                        var tag = $(this);

                        tag.closest('.mobile-form-input').prevAll('.tags-input:first').find('.tag_input').each(function() {
                            var tag_input = $(this);
                            if ($.trim(tag_input.val()) === '') {
                                tag_input.val(tag.text());
                                return false;
                            }
                            return true;
                        });
                    });
                }
            }
        );
    });
/*EOF /Scripts/Roomster/Roomster.Tags.UI.js*/
/*/Scripts/Roomster/Roomster.Currency.js*/
$.extend(
    Roomster.Currency, {
        reloadPage: true,
        _changeHandlers: [],
        MoneyPrefix: '',
        MoneyPostfix: '',
        Rate: 1,
        ConvertFromDefault: function(money) {
            return money * this.Rate;
        },
        Capacity: function(number) {
            return parseInt(Math.pow(10, parseInt(Math.floor(Math.log(number) / Math.LN10))));
        },
        ConvertFromDefaultMax: function(number) {
            var currentMax = Roomster.Currency.ConvertFromDefault(number);
            var capacity = Roomster.Currency.Capacity(currentMax);
            var value = parseInt(Math.ceil(currentMax / capacity) * capacity);
            return value;
        },
        ConvertFromDefaultMin: function(number) {
            var currentRangeStep = Roomster.Currency.ConvertFromDefault(number);
            var capacity = Roomster.Currency.Capacity(currentRangeStep);
            var value = parseInt(Math.floor(currentRangeStep / capacity) * capacity);
            if (value < capacity) {
                value = capacity;
            }
            return value;
        },
        FormatMoney: function(value) {
            return Roomster.Currency.MoneyPrefix + value;
        },
        AddChangeHandler: function(handler) {
            this._changeHandlers.push(handler);
        },
        RemoveChangeHandler: function(handler) {
            this._changeHandlers.splice($.inArray(handler, this._changeHandlers), 1);
        },
        Change: function(newCurrency) {
            if (newCurrency.length !== 3) return;
            var reloadPage = true;
            var oldCurrency = encodeURIComponent(Roomster.Cookie.Currency.val());

            if (oldCurrency.length !== 3) return;

            Roomster.Cookie.Currency.val(newCurrency);
            for (var i = 0, len = this._changeHandlers.length; i < len; i++) {
                Roomster.Cookie.OldCurrency.val(oldCurrency);
                if (this._changeHandlers[i]() === false) {
                    reloadPage = false;
                }
            }
            if (reloadPage || this.reloadPage) {
                Roomster.Cookie.OldCurrency.val(oldCurrency);
                window.location.reload();
            }
        },
        Set: function(new_name, old_name, new_exp_days, old_exp_days, cur_prefix, cur_postfix, rate) {
            if (cur_prefix.length > 3 || cur_postfix.length > 3 || isNaN(Number(rate))) return;

            Roomster.Cookie.Currency = new Roomster.Cookie.Base(new_name, new_exp_days);

            Roomster.Cookie.OldCurrency = new Roomster.Cookie.Base(old_name, old_exp_days);
            Roomster.Currency.MoneyPrefix = cur_prefix;
            Roomster.Currency.MoneyPostfix = cur_postfix;
            Roomster.Currency.Rate = rate;
        },
    });

/*EOF /Scripts/Roomster/Roomster.Currency.js*/
/*/Scripts/Roomster/Roomster.Jquery.js*/
Roomster.WaitJquery(
    function() {
        jQuery.fn.center =
            function() {
                this.css("position", "absolute");
                this.css("top", Math.max(0, (($(window).height() - this.outerHeight()) / 2) +
                    $(window).scrollTop()) + "px");
                this.css("left", Math.max(0, (($(window).width() - this.outerWidth()) / 2) +
                    $(window).scrollLeft()) + "px");
                return this;
            };
        jQuery.getParam = function qs(key) {
            key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
            var url = !window.isMobileUI ? History.getState().url : history.location.toString();
            var match = url.match(new RegExp("[?&]" + key + "=([^&]+)(&|$)"));
            return match && decodeURIComponent(match[1].replace(/\+/g, " "));
        };
    });

jQuery.fn.visible = function() {
    return this.css('visibility', 'visible');
};

jQuery.fn.invisible = function() {
    return this.css('visibility', 'hidden');
};

jQuery.fn.visibilityToggle = function() {
    return this.css('visibility', function(i, visibility) {
        return (visibility == 'visible') ? 'hidden' : 'visible';
    });
};
/*EOF /Scripts/Roomster/Roomster.Jquery.js*/
/*/Scripts/Roomster/Roomster.Tracker.js*/
(function(ctx) {
    ctx.Tracker = ctx.Tracker || $.extend({}, {
        ActionUrl: "",
        Init: function(actionUrl) {
            this.ActionUrl = actionUrl;
        },
        Track: function(actionType, comment) {
            var requestActionUrl = String.Format("{0}/{1}", this.ActionUrl, actionType);
            $.post(requestActionUrl, {
                comment: comment
            }, function() {});
        }
    });

})(Roomster);

/*EOF /Scripts/Roomster/Roomster.Tracker.js*/
/*/Scripts/Roomster/Roomster.Mail.js*/
(function(ctx) {
    ctx.Mail = ctx.Mail || $.extend({}, {
        SendMessageFromDialog: function(view_user_id, view_user_name, txt_message, toServiceId, unitId, isSendCCmail, successHandler, errorHandler) {
            toServiceId = toServiceId || 0;
            //var action_full_path = '/mbox/send_message_from_dialog.aspx';
            var action_full_path = '/message/send';
            var params = {
                text: txt_message, //new
                touserid: view_user_id, //new
                listingid: unitId ? unitId : 0, //new
                toservicetype: toServiceId, //new
                sendcopy: isSendCCmail == 'on' ? true : false, //new

                view_user_id: view_user_id,
                view_user_name: view_user_name,
                txt_message: txt_message,
                to_service_id: toServiceId,
                unit_id: unitId,
                isSendCCmail: isSendCCmail
            };

            jQuery.ajax({
                url: action_full_path,
                data: params,
                dataType: "html",
                type: "POST",
                timeout: 30000, // 30 seconds
                success: successHandler,
                error: errorHandler
            });
        },
    });
})(Roomster);

/*EOF /Scripts/Roomster/Roomster.Mail.js*/
/*/Scripts/Roomster/Roomster.SendMessageTemplate.js*/
(function(ctx) {
    ctx.SendMessageTemplate = ctx.SendMessageTemplate || $.extend({}, {
        CommonModel: {
            ViewUserId: null,
            ViewUserName: null,
            ToServiceId: null,
            UnitId: null,
            IsMobile: null
        },
        SendMessage: function(text, isSendCCmail) {
            $(".sendMessageMain").html($(".sendingProgressView").html());
            var model = this.CommonModel;
            Roomster.Mail.SendMessageFromDialog(model.ViewUserId, model.ViewUserName, text, model.ToServiceId, model.UnitId, isSendCCmail, this._successHandler, this._errorHandler);
            return false;
        },
        SendMessageFromProfile: function(userId, userName, text, serviceId, unitId, sendCC) {
            Roomster.Mail.SendMessageFromDialog(userId, userName, text, serviceId, unitId, sendCC, function(data) {
                if (unitId === 0) unitId = userId;
                //success
                $('.send-message.' + unitId + ' .status').html(data);
                $('.send-message.' + unitId + ' .sendMessageTextArea').val('');
            }, function(jqXHR, textStatus, errorThrown) {
                //error
                $('.send-message.' + unitId + '.status').html($(".sendingProgressErrorView").html());
            });
        },
        CheckForNonEmptyField: function($el) {
            if ($.trim($el.val()) == '') {
                $el.addClass('errorField');
                return false;
            } else {
                $el.removeClass('errorField');
                return true;
            }
        },
        SendElementValue: function($el, isSendCCmail) {
            if (this.CheckForNonEmptyField($el) == true) {
                this.SendMessage($el.val(), isSendCCmail);
            }
        },
        _successHandler: function(data) {
            var title = $('.sendMessageTitle');
            title.trigger('sendMessageSuccessful');
            var mainContainer = title.parent();
            mainContainer.html(data);

            var model = Roomster.SendMessageTemplate.CommonModel;
            if (model.IsMobile) {
                $('.send-message .status').html(data);
                $('.sendMessageTextArea').val('');
            }
        },
        _errorHandler: function(jqXHR, textStatus, errorThrown) {
            var title = $('.sendMessageTitle');
            title.trigger('sendMessageError');
            var mainContainer = title.parent();
            mainContainer.html($(".sendingProgressErrorView").html());

            var model = Roomster.SendMessageTemplate.CommonModel;
            if (model.IsMobile) {
                $('.send-message .status').html($(".sendingProgressErrorView").html());
            }
        },
        AppendText: function(text) {
            $('.sendMessageTextArea').focus().val($('.sendMessageTextArea').val() + text + '\n');
        }
    });

    $(function() {
        Roomster.UserActionTracker.RegisterEventListeners('sendMessageSuccessful sendMessageError');
    });
})(Roomster);

/*EOF /Scripts/Roomster/Roomster.SendMessageTemplate.js*/
/*/Scripts/Roomster/Roomster.ReportUserTemplate.js*/
(function($, ctx, profile) {
    ctx.ReportUserTemplate = ctx.ReportUserTemplate || $.extend({}, {
        SendMessage: function(data) {
            $("#reportUserMainBlock").html($("#messageSendingView").html());
            profile.ReportUser(data, this._successHandler, this._errorHandler);
            return false;
        },
        _successHandler: function(data) {
            $("#reportUserMainBlock").html($("#successView").html());
        },
        _errorHandler: function(jqXHR, textStatus, errorThrown) {
            $("#reportUserMainBlock").html($("#errorView").html());
        }
    });

})($, Roomster, Roomster.Profile);

/*EOF /Scripts/Roomster/Roomster.ReportUserTemplate.js*/
/*/Scripts/Roomster/Roomster.Ads.js*/
$.extend(Roomster.Ads, {
    IsLoadedTemplates: false,
    ContentUrlPrefix: "/Content/Templates/Bills/",
    TemplateNames: ["AdsButtonsPack"],
    Templates: {},
    LoadTemplates: function() {
        Roomster.Templates.EnrichTemplates(this.Templates, this.TemplateNames, this.ContentUrlPrefix);
        Roomster.Templates.LoadTemplates(this.Templates);
    },
    Init: function() {
        if (!this.IsLoadedTemplates)
            this.LoadTemplates();
        this.IsLoadedTemplates = true;
    },
    GetAdsButtonsPack: function(model) {
        return Roomster.Templates.Apply(this.Templates.AdsButtonsPack.name, model);
    },
    GetMovingQuotes: function(applymentCallback) {
        $.get($.url.action({
            controller: "Ads",
            action: "MovingQuotes",
            id: $.url.optional
        })).fail(Roomster.Common.Log).done(applymentCallback);
    },
    SendMovingQuotes: function(serializedData, successCallback) {
        $.post($.url.action({
            controller: "Ads",
            action: "MovingQuotes",
            id: $.url.optional
        }), serializedData).fail(Roomster.Common.Log).done(successCallback);
    },
    GetCreditScore: function(applymentCallback) {
        $.get($.url.action({
            controller: "Ads",
            action: "CreditScore",
            id: $.url.optional
        })).fail(Roomster.Common.Log).done(applymentCallback);
    },
});
$(function() {
    Roomster.Ads.Init();
});
/*EOF /Scripts/Roomster/Roomster.Ads.js*/
/*/Scripts/Roomster/Roomster.Ads.UI.js*/
(function(ctx) {
    var self = ctx.UI = (ctx.UI || {});
    $.extend(self, {
        GetGoogleAds728x90Html: function(adsSlot) {
            var $adsense = $("#adsense");
            if ($adsense.length == 0) {
                $(document.body).append(self.GetAdsTemplateHtml(adsSlot));
                $adsense = $("#adsense");
            }

            var html = $adsense.html().replace(/google_ad_slot = "\d+";/g, 'google_ad_slot = "' + adsSlot + '";');
            return html;
        },
        GetAdsTemplateHtml: function(adsSlot) {
            return ['<div id="adsense" style="display:none">',
                '<script type="text/javascript"><!--',
                'google_ad_client = "ca-pub-4029578820310208";',
                'google_hints = "moving movers";',
                'google_ad_slot = "' + adsSlot + '";',
                'google_ad_width = 728;',
                'google_ad_height = 90;',
                '//-->',
                '</script>',
                '<script type="text/javascript" src="//pagead2.googlesyndication.com/pagead/show_ads.js"></script>',
                '</div>'
            ].join('\r\n');
        },
        GetAdsHtml: function(adsSlot) {
            return '<iframe frameBorder="0" marginWidth="0" marginHeight="0" scrolling="no" vspace="0" hspace="0" style="width: 980px; height:100px; margin: 0px auto; allowTransparency=true;" src="/Content/ads.html?' + 635991095509755859 + '&adsSlot=' + adsSlot + '"></iframe>';
        },
        Slots: {
            Profile: {
                0: 7738325013, // user
                1: 9215058210, // Roommate
                2: 1691791413, //Room
                4: 4645257816, // Tenant
                5: 3168524613, // Apartment
            },
            Search: {
                1: 7877925814, //Roommates
                2: 9354659014, //Rooms
                4: 3308125419, //Tenants
                5: 1831392215 //Apartments
            }
        },
        Init: function() {
            //document.write(this.GetAdsTemplateHtml(1691791413));
        }
    });

    self.Init();

})(Roomster.Ads);

/*EOF /Scripts/Roomster/Roomster.Ads.UI.js*/
/*/Scripts/Roomster/Roomster.Contacts.UI.js*/
(function(ctx) {
    ctx.Contacts = ctx.Contacts || {};
    var self;
    $.extend(
        self = ctx.Contacts.UI = ctx.Contacts.UI || {}, {
            InputChangeHandler: function(e, checkboxId) {
                var target = (e.target) ? e.target : e.srcElement;
                var inputProf = $(target);
                var cb = $("#" + checkboxId);
                setTimeout(function() {
                    if (inputProf.val().length > 0) {
                        cb.removeAttr('disabled');
                        cb.attr("checked", "checked");
                    } else {
                        cb.attr('disabled', 'disabled');
                        cb.removeAttr("checked");
                    }
                }, 100);
            },
            InitSocialUrlInput: function(id) {
                var $el = $("#" + id);
                var placeholderVal = $el.attr("placeholder");
                var sCaret = placeholderVal.substring(placeholderVal.lastIndexOf("/") + 1);

                $el.focusin(function() {
                    setTimeout(function() {
                        if ($el.val() == "") {
                            $el.val(placeholderVal);
                            setTimeout(function() {
                                $el.caret(sCaret);
                            }, 1);
                        }
                    }, 1);
                });

                $el.focusout(function() {
                    if ($el.val() == placeholderVal) {
                        $el.val("");
                    }
                });
            },
            InitContacts: function(contactsContainer, countryCode) {
                var el = $('input[placeholder]', contactsContainer);
                el.placeholder && el.placeholder();
                var countryPhoneCodeSelectors = $('.country-phone-code', contactsContainer);
                countryPhoneCodeSelectors.change(
                    function() {
                        var $select = $(this);
                        var selected = $select.find(":selected");
                        var defaultPhoneMask = '?99999999999999999999';
                        var defaultPhoneCode = '+';
                        var mask = selected.attr('xMask') || defaultPhoneMask;
                        var phoneCode = selected.attr('xPhoneCode') || defaultPhoneCode;
                        var countryId = selected.attr('xCountryId') || '';
                        $('.countryCode').val(phoneCode);
                        $('.countryId').val(countryId);
                        var phoneInput = $('.countryPhoneInput', $select.parent());
                        phoneInput.mask && phoneInput.mask(mask);
                    });
                countryPhoneCodeSelectors.each(
                    function() {
                        var $select = $(this);
                        if (!$select.val() && countryCode && countryCode != '') {
                            $select.val(countryCode);
                        }
                        $select.change();
                    });
            }
        });
})(Roomster);
/*EOF /Scripts/Roomster/Roomster.Contacts.UI.js*/
/*/Scripts/helpers.js*/
$(window).load(function() {
    fixCheckBoxes();
    addArrayIndexOf();
});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isNumberKey(event) {
    if ($.inArray(event.keyCode, [8, 27, 13, 9]) !== -1 ||
        // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
        // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    } else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }
    }
}

function addArrayIndexOf() {
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function(elt /*, from*/ ) {
            var len = this.length >>> 0;

            var from = Number(arguments[1]) || 0;
            from = (from < 0) ? Math.ceil(from) : Math.floor(from);
            if (from < 0)
                from += len;

            for (; from < len; from++) {
                if (from in this &&
                    this[from] === elt)
                    return from;
            }
            return -1;
        };
    }
}

function fixCheckBoxes() {
    $("input:checkbox").change(function() {
        fixHiddenCheckField(this);
    });
}

function fixHiddenCheckField(checkField) {
    var jCb = $(checkField);
    var jHf = $("input[name='" + jCb.attr("name") + "'][type='hidden']");

    if (jCb.is(":checked")) {
        jCb.attr("value", true);
        jHf.attr("value", true);
    } else {
        jCb.attr("value", false);
        jHf.attr("value", false);
    }

}

function stop_bubbling(event) {
    if (event.stopPropagation) {
        event.stopPropagation();
    } else {
        event.cancelBubble = true;
    }
}

function jSonDateFormat(jDate) {
    var date = new Date(parseInt(jDate.substr(6)));
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    return '' + (m <= 9 ? '0' + m : m) + '/' + (d <= 9 ? '0' + d : d) + '/' + y;
}

function jSonDateSearchFormat(jDate, outOfDateExplanation) {
    var date = jSonDate(jDate);
    var diff = date - new Date();
    return Math.round(diff / 1000 / 60 / 60 / 24) > 0 ? jSonDateFormat(jDate) : outOfDateExplanation;
}

function jSonDate(jDate) {
    return new Date(parseInt(jDate.substr(6)));
}

function setNewUserClass(jDate, dateMargin, cssClass, temp) {
    if (!jDate) {
        return "";
    }
    //var diff = new Date() - jSonDate(jDate);
    var diff = new Date() - new Date(jDate);
    if ((diff / 1000 / 60 / 60 / 24) > dateMargin) {
        return cssClass;
    } else {
        return "";
    }
}

function getParamsFromHash(name) {
    var results = new RegExp("[\\?&]" + name + "=([^&#]*)").exec(document.location.hash);
    if (results == null) {
        return "";
    } else {
        return results[1];
    }
}

function getCaretPosition(el) {

    var result = 0;

    // IE Support
    if (document.selection) {

        el.focus();
        var sel = document.selection.createRange();

        sel.moveStart('character', -el.value.length);

        result = sel.text.length;
    }
    // Firefox support
    else if (el.selectionStart || el.selectionStart == '0') {
        result = el.selectionStart;
    }

    return result;
}

function stripTrailingSlash(text) {
    return text.replace(/\/$/, "");
}

jQuery.cachedTemplate = function(url, options) {

    // allow user to set any option except for dataType, cache, and url
    options = $.extend(options || {}, {
        dataType: "html",
        cache: true,
        url: url
    });

    // Use $.ajax() since it is more flexible than $.getScript
    // Return the jqXHR object so we can chain callbacks
    return jQuery.ajax(options);
};

//// Usage
//$.cachedScript("ajax/test.js").done(function (script, textStatus) {
//	console.log(textStatus);
//});

String.Format = function(str) {
    for (var i = 1; i < arguments.length; i++) {
        var r = new RegExp("\{+" + (i - 1) + "+\}", 'g');
        str = str.replace(r, arguments[i]);
    }
    return str;
};

(function($) {
    $.OpenEmailDialog = function(url) {
        var emailbox = $(".emailbox");
        if (emailbox.length == 0) {
            $(document.body).append("<div class='emailbox'></div>");
            emailbox = $(".emailbox");
        }
        emailbox.load(url, function(data) {
            emailbox.html(data);
        }).dialog({
            width: 590,
            height: 530,
            modal: true,
            resizable: false,
            draggable: false
        });
    };
})(jQuery);

(function($) {
    $.OpenFreeMovingQuoteDialog = function(url) {
        var freeMovingQuoteBox = $(".freeMovingQuoteBox");
        freeMovingQuoteBox.html("");
        if (freeMovingQuoteBox.length == 0) {
            $(document.body).append("<div class='freeMovingQuoteBox'></div>");
            freeMovingQuoteBox = $(".freeMovingQuoteBox");
        }
        freeMovingQuoteBox.load(url + "&ttl_=" + new Date().getTime(), function(data) {
            freeMovingQuoteBox.html(data);
        }).dialog({
            width: 590,
            height: 530,
            modal: true,
            resizable: false,
            draggable: false
        });
    };

})(jQuery);

jQuery.fn.redraw = function() {
    return this.hide(0, function() {
        $(this).show();
    });
};

(function() {
    var hidden = "hidden";

    // Standards:
    if (hidden in document)
        document.addEventListener("visibilitychange", onchange);
    else if ((hidden = "mozHidden") in document)
        document.addEventListener("mozvisibilitychange", onchange);
    else if ((hidden = "webkitHidden") in document)
        document.addEventListener("webkitvisibilitychange", onchange);
    else if ((hidden = "msHidden") in document)
        document.addEventListener("msvisibilitychange", onchange);
    // IE 9 and lower:
    else if ('onfocusin' in document)
        document.onfocusin = document.onfocusout = onchange;
    // All others:
    else
        window.onpageshow = window.onpagehide = window.onfocus = window.onblur = onchange;

    function onchange(evt) {
        var v = 'visible',
            h = 'hidden',
            evtMap = {
                focus: v,
                focusin: v,
                pageshow: v,
                blur: h,
                focusout: h,
                pagehide: h
            };

        evt = evt || window.event;
        if (evt.type in evtMap)
            document.body.className = evtMap[evt.type];
        else
            document.body.className = this[hidden] ? "hidden" : "visible";
    }
})();

Object.size = function(obj) {
    var size = 0,
        key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
/*EOF /Scripts/helpers.js*/
/*/Scripts/jquery.validate.js*/
/**
 * Note: While Microsoft is not the author of this file, Microsoft is
 * offering you a license subject to the terms of the Microsoft Software
 * License Terms for Microsoft ASP.NET Model View Controller 3.
 * Microsoft reserves all other rights. The notices below are provided
 * for informational purposes only and are not the license terms under
 * which Microsoft distributed this file.
 *
 * jQuery Validation Plugin 1.8.0
 *
 * http://bassistance.de/jquery-plugins/jquery-plugin-validation/
 * http://docs.jquery.com/Plugins/Validation
 *
 * Copyright (c) 2006 - 2011 Jörn Zaefferer
 */

(function($) {

    $.extend($.fn, {
        // http://docs.jquery.com/Plugins/Validation/validate
        validate: function(options) {

            // if nothing is selected, return nothing; can't chain anyway
            if (!this.length) {
                options && options.debug && window.console && console.warn("nothing selected, can't validate, returning nothing");
                return;
            }

            // check if a validator for this form was already created
            var validator = $.data(this[0], 'validator');
            if (validator) {
                return validator;
            }

            validator = new $.validator(options, this[0]);
            $.data(this[0], 'validator', validator);

            if (validator.settings.onsubmit) {

                // allow suppresing validation by adding a cancel class to the submit button
                this.find("input, button").filter(".cancel").click(function() {
                    validator.cancelSubmit = true;
                });

                // when a submitHandler is used, capture the submitting button
                if (validator.settings.submitHandler) {
                    this.find("input, button").filter(":submit").click(function() {
                        validator.submitButton = this;
                    });
                }

                // validate the form on submit
                this.submit(function(event) {
                    if (validator.settings.debug)
                    // prevent form submit to be able to see console output
                        event.preventDefault();

                    function handle() {
                        if (validator.settings.submitHandler) {
                            if (validator.submitButton) {
                                // insert a hidden input as a replacement for the missing submit button
                                var hidden = $("<input type='hidden'/>").attr("name", validator.submitButton.name).val(validator.submitButton.value).appendTo(validator.currentForm);
                            }
                            validator.settings.submitHandler.call(validator, validator.currentForm);
                            if (validator.submitButton) {
                                // and clean up afterwards; thanks to no-block-scope, hidden can be referenced
                                hidden.remove();
                            }
                            return false;
                        }
                        return true;
                    }

                    // prevent submit for invalid forms or custom submit handlers
                    if (validator.cancelSubmit) {
                        validator.cancelSubmit = false;
                        return handle();
                    }
                    if (validator.form()) {
                        if (validator.pendingRequest) {
                            validator.formSubmitted = true;
                            return false;
                        }
                        return handle();
                    } else {
                        validator.focusInvalid();
                        return false;
                    }
                });
            }

            return validator;
        },
        // http://docs.jquery.com/Plugins/Validation/valid
        valid: function() {
            if ($(this[0]).is('form')) {
                return this.validate().form();
            } else {
                var valid = true;
                var validator = $(this[0].form).validate();
                this.each(function() {
                    valid &= validator.element(this);
                });
                return valid;
            }
        },
        // attributes: space seperated list of attributes to retrieve and remove
        removeAttrs: function(attributes) {
            var result = {},
                $element = this;
            $.each(attributes.split(/\s/), function(index, value) {
                result[value] = $element.attr(value);
                $element.removeAttr(value);
            });
            return result;
        },
        // http://docs.jquery.com/Plugins/Validation/rules
        rules: function(command, argument) {
            var element = this[0];

            if (command) {
                var settings = $.data(element.form, 'validator').settings;
                var staticRules = settings.rules;
                var existingRules = $.validator.staticRules(element);
                switch (command) {
                    case "add":
                        $.extend(existingRules, $.validator.normalizeRule(argument));
                        staticRules[element.name] = existingRules;
                        if (argument.messages)
                            settings.messages[element.name] = $.extend(settings.messages[element.name], argument.messages);
                        break;
                    case "remove":
                        if (!argument) {
                            delete staticRules[element.name];
                            return existingRules;
                        }
                        var filtered = {};
                        $.each(argument.split(/\s/), function(index, method) {
                            filtered[method] = existingRules[method];
                            delete existingRules[method];
                        });
                        return filtered;
                }
            }

            var data = $.validator.normalizeRules(
                $.extend({},
                    $.validator.metadataRules(element),
                    $.validator.classRules(element),
                    $.validator.attributeRules(element),
                    $.validator.staticRules(element)
                ), element);

            // make sure required is at front
            if (data.required) {
                var param = data.required;
                delete data.required;
                data = $.extend({
                    required: param
                }, data);
            }

            return data;
        }
    });

    // Custom selectors
    $.extend($.expr[":"], {
        // http://docs.jquery.com/Plugins/Validation/blank
        blank: function(a) {
            return !$.trim("" + a.value);
        },
        // http://docs.jquery.com/Plugins/Validation/filled
        filled: function(a) {
            return !!$.trim("" + a.value);
        },
        // http://docs.jquery.com/Plugins/Validation/unchecked
        unchecked: function(a) {
            return !a.checked;
        }
    });

    // constructor for validator
    $.validator = function(options, form) {
        this.settings = $.extend(true, {}, $.validator.defaults, options);
        this.currentForm = form;
        this.init();
    };

    $.validator.format = function(source, params) {
        if (arguments.length == 1)
            return function() {
                var args = $.makeArray(arguments);
                args.unshift(source);
                return $.validator.format.apply(this, args);
            };
        if (arguments.length > 2 && params.constructor != Array) {
            params = $.makeArray(arguments).slice(1);
        }
        if (params.constructor != Array) {
            params = [params];
        }
        $.each(params, function(i, n) {
            source = source.replace(new RegExp("\\{" + i + "\\}", "g"), n);
        });
        return source;
    };

    $.extend($.validator, {

        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            validClass: "valid",
            errorElement: "label",
            focusInvalid: true,
            errorContainer: $([]),
            errorLabelContainer: $([]),
            onsubmit: true,
            ignore: [],
            ignoreTitle: false,
            onfocusin: function(element) {
                this.lastActive = element;

                // hide error label and remove error class on focus if enabled
                if (this.settings.focusCleanup && !this.blockFocusCleanup) {
                    this.settings.unhighlight && this.settings.unhighlight.call(this, element, this.settings.errorClass, this.settings.validClass);
                    this.addWrapper(this.errorsFor(element)).hide();
                }
            },
            onfocusout: function(element) {
                if (!this.checkable(element) && (element.name in this.submitted || !this.optional(element))) {
                    this.element(element);
                }
            },
            onkeyup: function(element) {
                if (element.name in this.submitted || element == this.lastElement) {
                    this.element(element);
                }
            },
            onclick: function(element) {
                // click on selects, radiobuttons and checkboxes
                if (element.name in this.submitted)
                    this.element(element);
                // or option elements, check parent select in that case
                else if (element.parentNode.name in this.submitted)
                    this.element(element.parentNode);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
            }
        },

        // http://docs.jquery.com/Plugins/Validation/Validator/setDefaults
        setDefaults: function(settings) {
            $.extend($.validator.defaults, settings);
        },

        messages: {
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            accept: "Please enter a value with a valid extension.",
            maxlength: $.validator.format("Please enter no more than {0} characters."),
            minlength: $.validator.format("Please enter at least {0} characters."),
            rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
            range: $.validator.format("Please enter a value between {0} and {1}."),
            max: $.validator.format("Please enter a value less than or equal to {0}."),
            min: $.validator.format("Please enter a value greater than or equal to {0}.")
        },

        autoCreateRanges: false,

        prototype: {

            init: function() {
                this.labelContainer = $(this.settings.errorLabelContainer);
                this.errorContext = this.labelContainer.length && this.labelContainer || $(this.currentForm);
                this.containers = $(this.settings.errorContainer).add(this.settings.errorLabelContainer);
                this.submitted = {};
                this.valueCache = {};
                this.pendingRequest = 0;
                this.pending = {};
                this.invalid = {};
                this.reset();

                var groups = (this.groups = {});
                $.each(this.settings.groups, function(key, value) {
                    $.each(value.split(/\s/), function(index, name) {
                        groups[name] = key;
                    });
                });
                var rules = this.settings.rules;
                $.each(rules, function(key, value) {
                    rules[key] = $.validator.normalizeRule(value);
                });

                function delegate(event) {
                    var validator = $.data(this[0].form, "validator"),
                        eventType = "on" + event.type.replace(/^validate/, "");
                    validator.settings[eventType] && validator.settings[eventType].call(validator, this[0]);
                }
                $(this.currentForm)
                    .validateDelegate(":text, :password, :file, select, textarea", "focusin focusout keyup", delegate)
                    .validateDelegate(":radio, :checkbox, select, option", "click", delegate);

                if (this.settings.invalidHandler)
                    $(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler);
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/form
            form: function() {
                this.checkForm();
                $.extend(this.submitted, this.errorMap);
                this.invalid = $.extend({}, this.errorMap);
                if (!this.valid())
                    $(this.currentForm).triggerHandler("invalid-form", [this]);
                this.showErrors();
                return this.valid();
            },

            checkForm: function() {
                this.prepareForm();
                for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
                    this.check(elements[i]);
                }
                return this.valid();
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/element
            element: function(element) {
                element = this.clean(element);
                this.lastElement = element;
                this.prepareElement(element);
                this.currentElements = $(element);
                var result = this.check(element);
                if (result) {
                    delete this.invalid[element.name];
                } else {
                    this.invalid[element.name] = true;
                }
                if (!this.numberOfInvalids()) {
                    // Hide error containers on last error
                    this.toHide = this.toHide.add(this.containers);
                }
                this.showErrors();
                return result;
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/showErrors
            showErrors: function(errors) {
                if (errors) {
                    // add items to error list and map
                    $.extend(this.errorMap, errors);
                    this.errorList = [];
                    for (var name in errors) {
                        this.errorList.push({
                            message: errors[name],
                            element: this.findByName(name)[0]
                        });
                    }
                    // remove items from success list
                    this.successList = $.grep(this.successList, function(element) {
                        return !(element.name in errors);
                    });
                }
                this.settings.showErrors ? this.settings.showErrors.call(this, this.errorMap, this.errorList) : this.defaultShowErrors();
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/resetForm
            resetForm: function() {
                if ($.fn.resetForm)
                    $(this.currentForm).resetForm();
                this.submitted = {};
                this.prepareForm();
                this.hideErrors();
                this.elements().removeClass(this.settings.errorClass);
            },

            numberOfInvalids: function() {
                return this.objectLength(this.invalid);
            },

            objectLength: function(obj) {
                var count = 0;
                for (var i in obj)
                    count++;
                return count;
            },

            hideErrors: function() {
                this.addWrapper(this.toHide).hide();
            },

            valid: function() {
                return this.size() == 0;
            },

            size: function() {
                return this.errorList.length;
            },

            focusInvalid: function() {
                if (this.settings.focusInvalid) {
                    try {
                        $(this.findLastActive() || this.errorList.length && this.errorList[0].element || [])
                            .filter(":visible")
                            .focus()
                            // manually trigger focusin event; without it, focusin handler isn't called, findLastActive won't have anything to find
                            .trigger("focusin");
                    } catch (e) {
                        // ignore IE throwing errors when focusing hidden elements
                    }
                }
            },

            findLastActive: function() {
                var lastActive = this.lastActive;
                return lastActive && $.grep(this.errorList, function(n) {
                    return n.element.name == lastActive.name;
                }).length == 1 && lastActive;
            },

            elements: function() {
                var validator = this,
                    rulesCache = {};

                // select all valid inputs inside the form (no submit or reset buttons)
                // workaround $Query([]).add until http://dev.jquery.com/ticket/2114 is solved
                return $([]).add(this.currentForm.elements)
                    .filter(":input")
                    .not(":submit, :reset, :image, [disabled]")
                    .not(this.settings.ignore)
                    .filter(function() {
                        !this.name && validator.settings.debug && window.console && console.error("%o has no name assigned", this);

                        // select only the first element for each name, and only those with rules specified
                        if (this.name in rulesCache || !validator.objectLength($(this).rules()))
                            return false;

                        rulesCache[this.name] = true;
                        return true;
                    });
            },

            clean: function(selector) {
                return $(selector)[0];
            },

            errors: function() {
                return $(this.settings.errorElement + "." + this.settings.errorClass, this.errorContext);
            },

            reset: function() {
                this.successList = [];
                this.errorList = [];
                this.errorMap = {};
                this.toShow = $([]);
                this.toHide = $([]);
                this.currentElements = $([]);
            },

            prepareForm: function() {
                this.reset();
                this.toHide = this.errors().add(this.containers);
            },

            prepareElement: function(element) {
                this.reset();
                this.toHide = this.errorsFor(element);
            },

            check: function(element) {
                element = this.clean(element);

                // if radio/checkbox, validate first element in group instead
                if (this.checkable(element)) {
                    element = this.findByName(element.name).not(this.settings.ignore)[0];
                }

                var rules = $(element).rules();
                var dependencyMismatch = false;
                for (var method in rules) {
                    var rule = {
                        method: method,
                        parameters: rules[method]
                    };
                    try {
                        var result = $.validator.methods[method].call(this, element.value.replace(/\r/g, ""), element, rule.parameters);

                        // if a method indicates that the field is optional and therefore valid,
                        // don't mark it as valid when there are no other rules
                        if (result == "dependency-mismatch") {
                            dependencyMismatch = true;
                            continue;
                        }
                        dependencyMismatch = false;

                        if (result == "pending") {
                            this.toHide = this.toHide.not(this.errorsFor(element));
                            return;
                        }

                        if (!result) {
                            this.formatAndAdd(element, rule);
                            return false;
                        }
                    } catch (e) {
                        this.settings.debug && window.console && console.log("exception occured when checking element " + element.id + ", check the '" + rule.method + "' method", e);
                        throw e;
                    }
                }
                if (dependencyMismatch)
                    return;
                if (this.objectLength(rules))
                    this.successList.push(element);
                return true;
            },

            // return the custom message for the given element and validation method
            // specified in the element's "messages" metadata
            customMetaMessage: function(element, method) {
                if (!$.metadata)
                    return;

                var meta = this.settings.meta ? $(element).metadata()[this.settings.meta] : $(element).metadata();

                return meta && meta.messages && meta.messages[method];
            },

            // return the custom message for the given element name and validation method
            customMessage: function(name, method) {
                var m = this.settings.messages[name];
                return m && (m.constructor == String ? m : m[method]);
            },

            // return the first defined argument, allowing empty strings
            findDefined: function() {
                for (var i = 0; i < arguments.length; i++) {
                    if (arguments[i] !== undefined)
                        return arguments[i];
                }
                return undefined;
            },

            defaultMessage: function(element, method) {
                return this.findDefined(
                    this.customMessage(element.name, method),
                    this.customMetaMessage(element, method),
                    // title is never undefined, so handle empty string as undefined
                    !this.settings.ignoreTitle && element.title || undefined,
                    $.validator.messages[method],
                    "<strong>Warning: No message defined for " + element.name + "</strong>"
                );
            },

            formatAndAdd: function(element, rule) {
                var message = this.defaultMessage(element, rule.method),
                    theregex = /\$?\{(\d+)\}/g;
                if (typeof message == "function") {
                    message = message.call(this, rule.parameters, element);
                } else if (theregex.test(message)) {
                    message = jQuery.format(message.replace(theregex, '{$1}'), rule.parameters);
                }
                this.errorList.push({
                    message: message,
                    element: element
                });

                this.errorMap[element.name] = message;
                this.submitted[element.name] = message;
            },

            addWrapper: function(toToggle) {
                if (this.settings.wrapper)
                    toToggle = toToggle.add(toToggle.parent(this.settings.wrapper));
                return toToggle;
            },

            defaultShowErrors: function() {
                for (var i = 0; this.errorList[i]; i++) {
                    var error = this.errorList[i];
                    this.settings.highlight && this.settings.highlight.call(this, error.element, this.settings.errorClass, this.settings.validClass);
                    this.showLabel(error.element, error.message);
                }
                if (this.errorList.length) {
                    this.toShow = this.toShow.add(this.containers);
                }
                if (this.settings.success) {
                    for (var i = 0; this.successList[i]; i++) {
                        this.showLabel(this.successList[i]);
                    }
                }
                if (this.settings.unhighlight) {
                    for (var i = 0, elements = this.validElements(); elements[i]; i++) {
                        this.settings.unhighlight.call(this, elements[i], this.settings.errorClass, this.settings.validClass);
                    }
                }
                this.toHide = this.toHide.not(this.toShow);
                this.hideErrors();
                this.addWrapper(this.toShow).show();
            },

            validElements: function() {
                return this.currentElements.not(this.invalidElements());
            },

            invalidElements: function() {
                return $(this.errorList).map(function() {
                    return this.element;
                });
            },

            showLabel: function(element, message) {
                var label = this.errorsFor(element);
                if (label.length) {
                    // refresh error/success class
                    label.removeClass().addClass(this.settings.errorClass);

                    // check if we have a generated label, replace the message then
                    label.attr("generated") && label.html(message);
                } else {
                    // create label
                    label = $("<" + this.settings.errorElement + "/>")
                        .attr({
                            "for": this.idOrName(element),
                            generated: true
                        })
                        .addClass(this.settings.errorClass)
                        .html(message || "");
                    if (this.settings.wrapper) {
                        // make sure the element is visible, even in IE
                        // actually showing the wrapped element is handled elsewhere
                        label = label.hide().show().wrap("<" + this.settings.wrapper + "/>").parent();
                    }
                    if (!this.labelContainer.append(label).length)
                        this.settings.errorPlacement ? this.settings.errorPlacement(label, $(element)) : label.insertAfter(element);
                }
                if (!message && this.settings.success) {
                    label.text("");
                    typeof this.settings.success == "string" ? label.addClass(this.settings.success) : this.settings.success(label);
                }
                this.toShow = this.toShow.add(label);
            },

            errorsFor: function(element) {
                var name = this.idOrName(element);
                return this.errors().filter(function() {
                    return $(this).attr('for') == name;
                });
            },

            idOrName: function(element) {
                return this.groups[element.name] || (this.checkable(element) ? element.name : element.id || element.name);
            },

            checkable: function(element) {
                return /radio|checkbox/i.test(element.type);
            },

            findByName: function(name) {
                // select by name and filter by form for performance over form.find("[name=...]")
                var form = this.currentForm;
                return $(document.getElementsByName(name)).map(function(index, element) {
                    return element.form == form && element.name == name && element || null;
                });
            },

            getLength: function(value, element) {
                switch (element.nodeName.toLowerCase()) {
                    case 'select':
                        return $("option:selected", element).length;
                    case 'input':
                        if (this.checkable(element))
                            return this.findByName(element.name).filter(':checked').length;
                }
                return value.length;
            },

            depend: function(param, element) {
                return this.dependTypes[typeof param] ? this.dependTypes[typeof param](param, element) : true;
            },

            dependTypes: {
                "boolean": function(param, element) {
                    return param;
                },
                "string": function(param, element) {
                    return !!$(param, element.form).length;
                },
                "function": function(param, element) {
                    return param(element);
                }
            },

            optional: function(element) {
                return !$.validator.methods.required.call(this, $.trim(element.value), element) && "dependency-mismatch";
            },

            startRequest: function(element) {
                if (!this.pending[element.name]) {
                    this.pendingRequest++;
                    this.pending[element.name] = true;
                }
            },

            stopRequest: function(element, valid) {
                this.pendingRequest--;
                // sometimes synchronization fails, make sure pendingRequest is never < 0
                if (this.pendingRequest < 0)
                    this.pendingRequest = 0;
                delete this.pending[element.name];
                if (valid && this.pendingRequest == 0 && this.formSubmitted && this.form()) {
                    $(this.currentForm).submit();
                    this.formSubmitted = false;
                } else if (!valid && this.pendingRequest == 0 && this.formSubmitted) {
                    $(this.currentForm).triggerHandler("invalid-form", [this]);
                    this.formSubmitted = false;
                }
            },

            previousValue: function(element) {
                return $.data(element, "previousValue") || $.data(element, "previousValue", {
                    old: null,
                    valid: true,
                    message: this.defaultMessage(element, "remote")
                });
            }

        },

        classRuleSettings: {
            required: {
                required: true
            },
            email: {
                email: true
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            dateISO: {
                dateISO: true
            },
            dateDE: {
                dateDE: true
            },
            number: {
                number: true
            },
            numberDE: {
                numberDE: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            }
        },

        addClassRules: function(className, rules) {
            className.constructor == String ?
                this.classRuleSettings[className] = rules :
                $.extend(this.classRuleSettings, className);
        },

        classRules: function(element) {
            var rules = {};
            var classes = $(element).attr('class');
            classes && $.each(classes.split(' '), function() {
                if (this in $.validator.classRuleSettings) {
                    $.extend(rules, $.validator.classRuleSettings[this]);
                }
            });
            return rules;
        },

        attributeRules: function(element) {
            var rules = {};
            var $element = $(element);

            for (var method in $.validator.methods) {
                var value = $element.attr(method);
                if (value) {
                    rules[method] = value;
                }
            }

            // maxlength may be returned as -1, 2147483647 (IE) and 524288 (safari) for text inputs
            if (rules.maxlength && /-1|2147483647|524288/.test(rules.maxlength)) {
                delete rules.maxlength;
            }

            return rules;
        },

        metadataRules: function(element) {
            if (!$.metadata) return {};

            var meta = $.data(element.form, 'validator').settings.meta;
            return meta ?
                $(element).metadata()[meta] :
                $(element).metadata();
        },

        staticRules: function(element) {
            var rules = {};
            var validator = $.data(element.form, 'validator');
            if (validator.settings.rules) {
                rules = $.validator.normalizeRule(validator.settings.rules[element.name]) || {};
            }
            return rules;
        },

        normalizeRules: function(rules, element) {
            // handle dependency check
            $.each(rules, function(prop, val) {
                // ignore rule when param is explicitly false, eg. required:false
                if (val === false) {
                    delete rules[prop];
                    return;
                }
                if (val.param || val.depends) {
                    var keepRule = true;
                    switch (typeof val.depends) {
                        case "string":
                            keepRule = !!$(val.depends, element.form).length;
                            break;
                        case "function":
                            keepRule = val.depends.call(element, element);
                            break;
                    }
                    if (keepRule) {
                        rules[prop] = val.param !== undefined ? val.param : true;
                    } else {
                        delete rules[prop];
                    }
                }
            });

            // evaluate parameters
            $.each(rules, function(rule, parameter) {
                rules[rule] = $.isFunction(parameter) ? parameter(element) : parameter;
            });

            // clean number parameters
            $.each(['minlength', 'maxlength', 'min', 'max'], function() {
                if (rules[this]) {
                    rules[this] = Number(rules[this]);
                }
            });
            $.each(['rangelength', 'range'], function() {
                if (rules[this]) {
                    rules[this] = [Number(rules[this][0]), Number(rules[this][1])];
                }
            });

            if ($.validator.autoCreateRanges) {
                // auto-create ranges
                if (rules.min && rules.max) {
                    rules.range = [rules.min, rules.max];
                    delete rules.min;
                    delete rules.max;
                }
                if (rules.minlength && rules.maxlength) {
                    rules.rangelength = [rules.minlength, rules.maxlength];
                    delete rules.minlength;
                    delete rules.maxlength;
                }
            }

            // To support custom messages in metadata ignore rule methods titled "messages"
            if (rules.messages) {
                delete rules.messages;
            }

            return rules;
        },

        // Converts a simple string to a {string: true} rule, e.g., "required" to {required:true}
        normalizeRule: function(data) {
            if (typeof data == "string") {
                var transformed = {};
                $.each(data.split(/\s/), function() {
                    transformed[this] = true;
                });
                data = transformed;
            }
            return data;
        },

        // http://docs.jquery.com/Plugins/Validation/Validator/addMethod
        addMethod: function(name, method, message) {
            $.validator.methods[name] = method;
            $.validator.messages[name] = message != undefined ? message : $.validator.messages[name];
            if (method.length < 3) {
                $.validator.addClassRules(name, $.validator.normalizeRule(name));
            }
        },

        methods: {

            // http://docs.jquery.com/Plugins/Validation/Methods/required
            required: function(value, element, param) {
                // check if dependency is met
                if (!this.depend(param, element))
                    return "dependency-mismatch";
                switch (element.nodeName.toLowerCase()) {
                    case 'select':
                        // could be an array for select-multiple or a string, both are fine this way
                        var val = $(element).val();
                        return val && val.length > 0;
                    case 'input':
                        if (this.checkable(element))
                            return this.getLength(value, element) > 0;
                    default:
                        return $.trim(value).length > 0;
                }
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/remote
            remote: function(value, element, param) {
                if (this.optional(element))
                    return "dependency-mismatch";

                var previous = this.previousValue(element);
                if (!this.settings.messages[element.name])
                    this.settings.messages[element.name] = {};
                previous.originalMessage = this.settings.messages[element.name].remote;
                this.settings.messages[element.name].remote = previous.message;

                param = typeof param == "string" && {
                    url: param
                } || param;

                if (this.pending[element.name]) {
                    return "pending";
                }
                if (previous.old === value) {
                    return previous.valid;
                }

                previous.old = value;
                var validator = this;
                this.startRequest(element);
                var data = {};
                data[element.name] = value;
                $.ajax($.extend(true, {
                    url: param,
                    mode: "abort",
                    port: "validate" + element.name,
                    dataType: "json",
                    data: data,
                    success: function(response) {
                        validator.settings.messages[element.name].remote = previous.originalMessage;
                        var valid = response === true;
                        if (valid) {
                            var submitted = validator.formSubmitted;
                            validator.prepareElement(element);
                            validator.formSubmitted = submitted;
                            validator.successList.push(element);
                            validator.showErrors();
                        } else {
                            var errors = {};
                            var message = response || validator.defaultMessage(element, "remote");
                            errors[element.name] = previous.message = $.isFunction(message) ? message(value) : message;
                            validator.showErrors(errors);
                        }
                        previous.valid = valid;
                        validator.stopRequest(element, valid);
                    }
                }, param));
                return "pending";
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/minlength
            minlength: function(value, element, param) {
                return this.optional(element) || this.getLength($.trim(value), element) >= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/maxlength
            maxlength: function(value, element, param) {
                return this.optional(element) || this.getLength($.trim(value), element) <= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/rangelength
            rangelength: function(value, element, param) {
                var length = this.getLength($.trim(value), element);
                return this.optional(element) || (length >= param[0] && length <= param[1]);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/min
            min: function(value, element, param) {
                return this.optional(element) || value >= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/max
            max: function(value, element, param) {
                return this.optional(element) || value <= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/range
            range: function(value, element, param) {
                return this.optional(element) || (value >= param[0] && value <= param[1]);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/email
            email: function(value, element) {
                // contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
                return this.optional(element) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/url
            url: function(value, element) {
                // contributed by Scott Gonzalez: http://projects.scottsplayground.com/iri/
                return this.optional(element) || /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/date
            date: function(value, element) {
                return this.optional(element) || !/Invalid|NaN/.test(new Date(value));
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/dateISO
            dateISO: function(value, element) {
                return this.optional(element) || /^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/number
            number: function(value, element) {
                return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/digits
            digits: function(value, element) {
                return this.optional(element) || /^\d+$/.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/creditcard
            // based on http://en.wikipedia.org/wiki/Luhn
            creditcard: function(value, element) {
                if (this.optional(element))
                    return "dependency-mismatch";
                // accept only digits and dashes
                if (/[^0-9-]+/.test(value))
                    return false;
                var nCheck = 0,
                    nDigit = 0,
                    bEven = false;

                value = value.replace(/\D/g, "");

                for (var n = value.length - 1; n >= 0; n--) {
                    var cDigit = value.charAt(n);
                    var nDigit = parseInt(cDigit, 10);
                    if (bEven) {
                        if ((nDigit *= 2) > 9)
                            nDigit -= 9;
                    }
                    nCheck += nDigit;
                    bEven = !bEven;
                }

                return (nCheck % 10) == 0;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/accept
            accept: function(value, element, param) {
                param = typeof param == "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif";
                return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/equalTo
            equalTo: function(value, element, param) {
                // bind to the blur event of the target in order to revalidate whenever the target field is updated
                // TODO find a way to bind the event just once, avoiding the unbind-rebind overhead
                var target = $(param).unbind(".validate-equalTo").bind("blur.validate-equalTo", function() {
                    $(element).valid();
                });
                return value == target.val();
            }

        }

    });

    // deprecated, use $.validator.format instead
    $.format = $.validator.format;

})(jQuery);

// ajax mode: abort
// usage: $.ajax({ mode: "abort"[, port: "uniqueport"]});
// if mode:"abort" is used, the previous request on that port (port can be undefined) is aborted via XMLHttpRequest.abort()
;
(function($) {
    var pendingRequests = {};
    // Use a prefilter if available (1.5+)
    if ($.ajaxPrefilter) {
        $.ajaxPrefilter(function(settings, _, xhr) {
            var port = settings.port;
            if (settings.mode == "abort") {
                if (pendingRequests[port]) {
                    pendingRequests[port].abort();
                }
                pendingRequests[port] = xhr;
            }
        });
    } else {
        // Proxy ajax
        var ajax = $.ajax;
        $.ajax = function(settings) {
            var mode = ("mode" in settings ? settings : $.ajaxSettings).mode,
                port = ("port" in settings ? settings : $.ajaxSettings).port;
            if (mode == "abort") {
                if (pendingRequests[port]) {
                    pendingRequests[port].abort();
                }
                return (pendingRequests[port] = ajax.apply(this, arguments));
            }
            return ajax.apply(this, arguments);
        };
    }
})(jQuery);

// provides cross-browser focusin and focusout events
// IE has native support, in other browsers, use event caputuring (neither bubbles)

// provides delegate(type: String, delegate: Selector, handler: Callback) plugin for easier event delegation
// handler is only called when $(event.target).is(delegate), in the scope of the jquery-object for event.target
;
(function($) {
    // only implement if not provided by jQuery core (since 1.4)
    // TODO verify if jQuery 1.4's implementation is compatible with older jQuery special-event APIs
    if (!jQuery.event.special.focusin && !jQuery.event.special.focusout && document.addEventListener) {
        $.each({
            focus: 'focusin',
            blur: 'focusout'
        }, function(original, fix) {
            $.event.special[fix] = {
                setup: function() {
                    this.addEventListener(original, handler, true);
                },
                teardown: function() {
                    this.removeEventListener(original, handler, true);
                },
                handler: function(e) {
                    arguments[0] = $.event.fix(e);
                    arguments[0].type = fix;
                    return $.event.handle.apply(this, arguments);
                }
            };

            function handler(e) {
                e = $.event.fix(e);
                e.type = fix;
                return $.event.handle.call(this, e);
            }
        });
    };
    $.extend($.fn, {
        validateDelegate: function(delegate, type, handler) {
            return this.bind(type, function(event) {
                var target = $(event.target);
                if (target.is(delegate)) {
                    return handler.apply(target, arguments);
                }
            });
        }
    });
})(jQuery);

/*EOF /Scripts/jquery.validate.js*/
/*/Scripts/jquery.validate.unobtrusive.js*/
/// <reference path="jquery-1.5.1.js" />
/// <reference path="jquery.validate.js" />

/*!
 ** Unobtrusive validation support library for jQuery and jQuery Validate
 ** Copyright (C) Microsoft Corporation. All rights reserved.
 */

/*jslint white: true, browser: true, onevar: true, undef: true, nomen: true, eqeqeq: true, plusplus: true, bitwise: true, regexp: true, newcap: true, immed: true, strict: false */
/*global document: false, jQuery: false */

(function($) {
    var $jQval = $.validator,
        adapters,
        data_validation = "unobtrusiveValidation";

    function setValidationValues(options, ruleName, value) {
        options.rules[ruleName] = value;
        if (options.message) {
            options.messages[ruleName] = options.message;
        }
    }

    function splitAndTrim(value) {
        return value.replace(/^\s+|\s+$/g, "").split(/\s*,\s*/g);
    }

    function getModelPrefix(fieldName) {
        return fieldName.substr(0, fieldName.lastIndexOf(".") + 1);
    }

    function appendModelPrefix(value, prefix) {
        if (value.indexOf("*.") === 0) {
            value = value.replace("*.", prefix);
        }
        return value;
    }

    function onError(error, inputElement) { // 'this' is the form element
        var container = $(this).find("[data-valmsg-for='" + inputElement[0].name + "']"),
            replace = $.parseJSON(container.attr("data-valmsg-replace")) !== false;
        if (container.length == 0 && $(inputElement[0]).attr("valmsg-container") != undefined) {
            container = $(this).find("[data-valmsg-for='" + $(inputElement[0]).attr("valmsg-container") + "']");
            replace = $.parseJSON(container.attr("data-valmsg-replace")) !== false;
        }
        container.removeClass("field-validation-valid").addClass("field-validation-error");
        error.data("unobtrusiveContainer", container);

        if (replace) {
            container.empty();
            error.removeClass("input-validation-error").appendTo(container);
        } else {
            error.hide();
        }
    }

    function onErrors(form, validator) { // 'this' is the form element
        var container = $(this).find("[data-valmsg-summary=true]"),
            list = container.find("ul");

        if (list && list.length && validator.errorList.length) {
            list.empty();
            container.addClass("validation-summary-errors").removeClass("validation-summary-valid");

            $.each(validator.errorList, function() {
                $("<li />").html(this.message).appendTo(list);
            });
        }
    }

    function onSuccess(error) { // 'this' is the form element
        var container = error.data("unobtrusiveContainer"),
            replace = $.parseJSON(container.attr("data-valmsg-replace"));

        if (container) {
            container.addClass("field-validation-valid").removeClass("field-validation-error");
            error.removeData("unobtrusiveContainer");

            if (replace) {
                container.empty();
            }
        }
    }

    function validationInfo(form) {
        var $form = $(form),
            result = $form.data(data_validation);

        if (!result) {
            result = {
                options: { // options structure passed to jQuery Validate's validate() method
                    errorClass: "input-validation-error",
                    errorElement: "span",
                    errorPlacement: $.proxy(onError, form),
                    invalidHandler: $.proxy(onErrors, form),
                    messages: {},
                    rules: {},
                    success: $.proxy(onSuccess, form)
                },
                attachValidation: function() {
                    $form.validate(this.options);
                },
                validate: function() { // a validation function that is called by unobtrusive Ajax
                    $form.validate();
                    return $form.valid();
                }
            };
            $form.data(data_validation, result);
        }

        return result;
    }

    $jQval.unobtrusive = {
        adapters: [],

        parseElement: function(element, skipAttach) {
            /// <summary>
            /// Parses a single HTML element for unobtrusive validation attributes.
            /// </summary>
            /// <param name="element" domElement="true">The HTML element to be parsed.</param>
            /// <param name="skipAttach" type="Boolean">[Optional] true to skip attaching the
            /// validation to the form. If parsing just this single element, you should specify true.
            /// If parsing several elements, you should specify false, and manually attach the validation
            /// to the form when you are finished. The default is false.</param>
            var $element = $(element),
                form = $element.parents("form")[0],
                valInfo, rules, messages;

            if (!form) { // Cannot do client-side validation without a form
                return;
            }

            valInfo = validationInfo(form);
            valInfo.options.rules[element.name] = rules = {};
            valInfo.options.messages[element.name] = messages = {};

            $.each(this.adapters, function() {
                var prefix = "data-val-" + this.name,
                    message = $element.attr(prefix),
                    paramValues = {};

                if (message !== undefined) { // Compare against undefined, because an empty message is legal (and falsy)
                    prefix += "-";

                    $.each(this.params, function() {
                        paramValues[this] = $element.attr(prefix + this);
                    });

                    this.adapt({
                        element: element,
                        form: form,
                        message: message,
                        params: paramValues,
                        rules: rules,
                        messages: messages
                    });
                }
            });

            jQuery.extend(rules, {
                "__dummy__": true
            });

            if (!skipAttach) {
                valInfo.attachValidation();
            }
        },

        parse: function(selector) {
            /// <summary>
            /// Parses all the HTML elements in the specified selector. It looks for input elements decorated
            /// with the [data-val=true] attribute value and enables validation according to the data-val-*
            /// attribute values.
            /// </summary>
            /// <param name="selector" type="String">Any valid jQuery selector.</param>
            $(selector).find(":input[data-val=true]").each(function() {
                $jQval.unobtrusive.parseElement(this, true);
            });

            $("form").each(function() {
                var info = validationInfo(this);
                if (info) {
                    info.attachValidation();
                }
            });
        }
    };

    adapters = $jQval.unobtrusive.adapters;

    adapters.add = function(adapterName, params, fn) {
        /// <summary>Adds a new adapter to convert unobtrusive HTML into a jQuery Validate validation.</summary>
        /// <param name="adapterName" type="String">The name of the adapter to be added. This matches the name used
        /// in the data-val-nnnn HTML attribute (where nnnn is the adapter name).</param>
        /// <param name="params" type="Array" optional="true">[Optional] An array of parameter names (strings) that will
        /// be extracted from the data-val-nnnn-mmmm HTML attributes (where nnnn is the adapter name, and
        /// mmmm is the parameter name).</param>
        /// <param name="fn" type="Function">The function to call, which adapts the values from the HTML
        /// attributes into jQuery Validate rules and/or messages.</param>
        /// <returns type="jQuery.validator.unobtrusive.adapters" />
        if (!fn) { // Called with no params, just a function
            fn = params;
            params = [];
        }
        this.push({
            name: adapterName,
            params: params,
            adapt: fn
        });
        return this;
    };

    adapters.addBool = function(adapterName, ruleName) {
        /// <summary>Adds a new adapter to convert unobtrusive HTML into a jQuery Validate validation, where
        /// the jQuery Validate validation rule has no parameter values.</summary>
        /// <param name="adapterName" type="String">The name of the adapter to be added. This matches the name used
        /// in the data-val-nnnn HTML attribute (where nnnn is the adapter name).</param>
        /// <param name="ruleName" type="String" optional="true">[Optional] The name of the jQuery Validate rule. If not provided, the value
        /// of adapterName will be used instead.</param>
        /// <returns type="jQuery.validator.unobtrusive.adapters" />
        return this.add(adapterName, function(options) {
            setValidationValues(options, ruleName || adapterName, true);
        });
    };

    adapters.addMinMax = function(adapterName, minRuleName, maxRuleName, minMaxRuleName, minAttribute, maxAttribute) {
        /// <summary>Adds a new adapter to convert unobtrusive HTML into a jQuery Validate validation, where
        /// the jQuery Validate validation has three potential rules (one for min-only, one for max-only, and
        /// one for min-and-max). The HTML parameters are expected to be named -min and -max.</summary>
        /// <param name="adapterName" type="String">The name of the adapter to be added. This matches the name used
        /// in the data-val-nnnn HTML attribute (where nnnn is the adapter name).</param>
        /// <param name="minRuleName" type="String">The name of the jQuery Validate rule to be used when you only
        /// have a minimum value.</param>
        /// <param name="maxRuleName" type="String">The name of the jQuery Validate rule to be used when you only
        /// have a maximum value.</param>
        /// <param name="minMaxRuleName" type="String">The name of the jQuery Validate rule to be used when you
        /// have both a minimum and maximum value.</param>
        /// <param name="minAttribute" type="String" optional="true">[Optional] The name of the HTML attribute that
        /// contains the minimum value. The default is "min".</param>
        /// <param name="maxAttribute" type="String" optional="true">[Optional] The name of the HTML attribute that
        /// contains the maximum value. The default is "max".</param>
        /// <returns type="jQuery.validator.unobtrusive.adapters" />
        return this.add(adapterName, [minAttribute || "min", maxAttribute || "max"], function(options) {
            var min = options.params.min,
                max = options.params.max;

            if (min && max) {
                setValidationValues(options, minMaxRuleName, [min, max]);
            } else if (min) {
                setValidationValues(options, minRuleName, min);
            } else if (max) {
                setValidationValues(options, maxRuleName, max);
            }
        });
    };

    adapters.addSingleVal = function(adapterName, attribute, ruleName) {
        /// <summary>Adds a new adapter to convert unobtrusive HTML into a jQuery Validate validation, where
        /// the jQuery Validate validation rule has a single value.</summary>
        /// <param name="adapterName" type="String">The name of the adapter to be added. This matches the name used
        /// in the data-val-nnnn HTML attribute(where nnnn is the adapter name).</param>
        /// <param name="attribute" type="String">[Optional] The name of the HTML attribute that contains the value.
        /// The default is "val".</param>
        /// <param name="ruleName" type="String" optional="true">[Optional] The name of the jQuery Validate rule. If not provided, the value
        /// of adapterName will be used instead.</param>
        /// <returns type="jQuery.validator.unobtrusive.adapters" />
        return this.add(adapterName, [attribute || "val"], function(options) {
            setValidationValues(options, ruleName || adapterName, options.params[attribute]);
        });
    };

    $jQval.addMethod("__dummy__", function(value, element, params) {
        return true;
    });

    $jQval.addMethod("regex", function(value, element, params) {
        var match;
        if (this.optional(element)) {
            return true;
        }

        match = new RegExp(params).exec(value);
        return (match && (match.index === 0) && (match[0].length === value.length));
    });

    adapters.addSingleVal("accept", "exts").addSingleVal("regex", "pattern");
    adapters.addBool("creditcard").addBool("date").addBool("digits").addBool("email").addBool("number").addBool("url");
    adapters.addMinMax("length", "minlength", "maxlength", "rangelength").addMinMax("range", "min", "max", "range");
    adapters.add("equalto", ["other"], function(options) {
        var prefix = getModelPrefix(options.element.name),
            other = options.params.other,
            fullOtherName = appendModelPrefix(other, prefix),
            element = $(options.form).find(":input[name=" + fullOtherName + "]")[0];

        setValidationValues(options, "equalTo", element);
    });
    adapters.add("required", function(options) {
        // jQuery Validate equates "required" with "mandatory" for checkbox elements
        if (options.element.tagName.toUpperCase() !== "INPUT" || options.element.type.toUpperCase() !== "CHECKBOX") {
            setValidationValues(options, "required", true);
        }
    });
    adapters.add("remote", ["url", "type", "additionalfields"], function(options) {
        var value = {
                url: options.params.url,
                type: options.params.type || "GET",
                data: {}
            },
            prefix = getModelPrefix(options.element.name);

        $.each(splitAndTrim(options.params.additionalfields || options.element.name), function(i, fieldName) {
            var paramName = appendModelPrefix(fieldName, prefix);
            value.data[paramName] = function() {
                return $(options.form).find(":input[name='" + paramName + "']").val();
            };
        });

        setValidationValues(options, "remote", value);
    });

    $(function() {
        $jQval.unobtrusive.parse(document);
    });
}(jQuery));
/*EOF /Scripts/jquery.validate.unobtrusive.js*/
/*/Scripts/jquery.validate.unobtrusive.enforcetrue.js*/
jQuery.validator.addMethod("enforcetrue", function(value, element, param) {
    return element.checked;
});
jQuery.validator.unobtrusive.adapters.addBool("enforcetrue");
/*EOF /Scripts/jquery.validate.unobtrusive.enforcetrue.js*/
/*/Scripts/jquery.validate.unobtrusive.enforcetrueany.js*/
jQuery.validator.addMethod("enforcetrueany", function(value, element, param) {
    var name = element.attributes["group_name"].value;
    return $(element).closest("form").find("[group_name='" + name + "']:checked").length > 0;
});
jQuery.validator.unobtrusive.adapters.addBool("enforcetrueany");
/*EOF /Scripts/jquery.validate.unobtrusive.enforcetrueany.js*/
/*/Scripts/jquery.validate.unobtrusive.valueany.js*/
jQuery.validator.addMethod("valueany", function(value, element, param) {
    var name = element.attributes["group_name"].value;
    return $(element).closest("form").find('input:text[value!=""][group_name="' + name + '"]').length > 0;
});
jQuery.validator.unobtrusive.adapters.addBool("valueany");
/*EOF /Scripts/jquery.validate.unobtrusive.valueany.js*/
/*/Scripts/jquery.validate.unobtrusive.dategreaterthan.js*/
jQuery.validator.addMethod("dategreaterthan", function(value, element, param) {
    if (isNaN(Date.parse(value)))
        return true;

    var inputDate = new Date(value);

    if (param.comparepropertyname != undefined) {
        var compareObject = $("input[name='" + param.comparepropertyname + "']");

        if (compareObject != null && compareObject.length == 1) {
            var compareObjectDate = new Date(compareObject.val());

            return inputDate >= compareObjectDate;
        }

        return true;
    }

    if (param.comparedate != undefined) {
        var compareDate = new Date(param.comparedate);

        return inputDate >= compareDate;
    }

    return true;
});
jQuery.validator.unobtrusive.adapters.add("dategreaterthan", ["comparepropertyname", "comparedate"], function(options) {
    options.rules["dategreaterthan"] = options.params;
    options.messages["dategreaterthan"] = options.message;
});
/*EOF /Scripts/jquery.validate.unobtrusive.dategreaterthan.js*/
/*/Scripts/jquery.validate.unobtrusive.charlimit.js*/
$(function() {
    $("form [data-val-charlimit]").each(function() {
        $(this).on("keyup focus", function() {
            setCount($(this));
        });
        setCount($(this));
    });

    function setCount(src) {
        var name = src.attr('name');
        var limit = parseInt(src.attr("data-val-charlimit"));
        var chars = src.val().length;
        if (chars > limit) {
            src.val(src.val().substr(0, limit));
            chars = limit;
        }
        var displayElem = src.prev("[data-val-for-charlimit = '" + name + "']");
        if (displayElem.length == 0)
            displayElem = $("<span  class='characterCounter' data-val-for-charlimit = '" + name + "'></span>").insertBefore(src);
        var rest = limit - chars;
        displayElem.html(rest + ' ' + (rest % 10 == 1 ? Roomster.Resources.GetTranslation('character left') : Roomster.Resources.GetTranslation('characters left')));
    }

});

/*EOF /Scripts/jquery.validate.unobtrusive.charlimit.js*/
/*/Scripts/jquery.carousel.js*/
(function($) {

    function Carousel() {
        this.settings = {};
        this.currentPage = 1;
        this.totalPages = 1;
        this._carousel = null;
    };
    Carousel.prototype = {
        init: function(carousel, options) {
            this.settings = $.extend({
                'perpage': 5,
                'width': '800',
                'showPager': true,
                'animationSpeed': 500,
                'animationType': 'swing',
                'startPage': 1,
                'onPageChangeStart': null,
                'onPageChangeEnd': null
            }, options);

            this._carousel = carousel;

            // paging
            return this._carousel.each($.proxy(function(index, value) {

                // handle building of the pager
                var ul = $(".carousel-holder ul", this._carousel);
                var lis = $("li", ul);
                var liw = $(lis[0]).outerWidth();
                ul.width(liw * lis.length); // set the correct width whilst we're here
                var ulw = ul.outerWidth();
                this.totalPages = Math.round(ulw / liw / this.settings.perpage);
                this.totalItems = Math.round(ulw / liw);

                if (this.settings.showPager && this.totalPages > 1) {
                    for (var i = 0; i < this.totalPages; i++) {
                        $(".controls", this._carousel).append("<li><a href=\"#\"></a></li>");
                    }
                    $(".controls li:first-child", this._carousel).addClass("selected");

                    // set up the events for the pager
                    $(".controls li a", this._carousel).click($.proxy(function(e) {
                        if (!$(".carousel-holder ul", this._carousel).is(":animated")) {
                            // adjust the margin
                            this._carousel.carousel("changeItem", this.getFirstElementIndexByPage($(e.target).parent().index() + 1) + 1);
                        }
                        return false;
                    }, this));
                }
                if (this.totalItems == 1) {
                    $(".arrowl", this._carousel).hide();
                    $(".arrowr", this._carousel).hide();
                } else {
                    // handle left/right arrows
                    $(".arrowl", this._carousel).click($.proxy(function(e) {
                        if (!this._carousel.is(":animated")) {
                            var newIndex = this.selectedItemIndex() > 1 ? this.selectedItemIndex() - 1 : this.totalItems;
                            this._carousel.carousel("changeItem", newIndex);
                        }
                        return false;
                    }, this));
                    $(".arrowr", this._carousel).click($.proxy(function(e) {
                        if (!this._carousel.is(":animated")) {
                            var newIndex = this.selectedItemIndex() < this.totalItems ? this.selectedItemIndex() + 1 : 1;
                            this._carousel.carousel("changeItem", newIndex);
                        }

                        return false;
                    }, this));
                }
                // hide all but the first item!
                $(".items > div", this._carousel).hide();
                $(".items div:first-child", this._carousel).show();

                $(".carousel-holder li", this._carousel).click($.proxy(function(e) {
                    var index = $(e.currentTarget).index() + 1;
                    this._carousel.carousel("changeItem", index);
                    return false;
                }, this));

                if (this.settings.startPage > 1 && this.settings.startPage <= this.totalPages) {
                    this._carousel.carousel("changePage", this.settings.startPage, false);
                }

                $(".holder ul li:eq(" + this.getFirstElementIndexByPage(this.settings.startPage) + ")", this._carousel).addClass("selected");
            }, this));
        },
        getFirstElementIndexByPage: function(pageIndex) {
            return (pageIndex - 1) * this.settings.perpage;
        },
        selectedItemIndex: function() {
            return $(".holder ul li.selected", this._carousel).index() + 1;
        },
        changeItem: function(index) {
            $(".carousel-holder li", this._carousel).removeClass("selected");
            $(".carousel-holder li:eq(" + (index - 1) + ")", this._carousel).addClass("selected");
            $(".items div.item", this._carousel).hide();
            $(".items div.item[rel='" + (index - 1) + "']", this._carousel).show();

            var newpage = parseInt(index / this.settings.perpage) + 1;
            if (index % this.settings.perpage == 0)
                newpage--;
            if (newpage != this.currentPage)
                this._carousel.carousel("changePage", newpage);
            this.currentPage = newpage;
        },
        changePage: function(page, animate) {
            this._carousel.each($.proxy(function(index, value) {

                var ml = ((page - 1) * -1 * this.settings.width) + 'px';

                // animate the slide if we want to!
                if (animate != false) {
                    if (jQuery.isFunction(this.settings.onPageChangeStart)) {
                        this.settings.onPageChangeStart.call(this, page);
                    }
                    $(".carousel-holder ul", this._carousel).animate({
                        'margin-left': ml
                    }, this.settings.animationSpeed, this.settings.animationType, $.proxy(function() {
                        if (jQuery.isFunction(this.settings.onPageChangeEnd)) {
                            this.settings.onPageChangeEnd.call(this, page);
                        }
                    }, this));
                } else {
                    $(".carousel-holder ul", this._carousel).css({
                        'margin-left': ml
                    });
                }

                // update the paging icon!
                $(".controls li.selected", this._carousel).removeClass("selected");
                $(".controls li:nth-child(" + page + ")", this._carousel).addClass("selected");
            }, this));

        }
    };
    $.fn.carousel = function(method, options) {

        return this.each(function() {
            if (!$.data(this, 'carousel')) {
                var obj = new Carousel();
                obj.init($(this), method);
                $.data(this, 'carousel', obj);
            } else if (typeof(method) === "string")
                $.data(this, 'carousel')[method](options);
        });
    };

})(jQuery);
/*EOF /Scripts/jquery.carousel.js*/
/*/script/divs.js*/
function divs_toggle() {
    for (var i = 0; i < arguments.length; i++) {
        divs_clickToToggle(arguments[i]);
    }
    return false;
}

function divs_clickToShow(targetId) {
    return jQuery("#" + targetId).show();
}

function divs_clickToHide(targetId) {
    return jQuery("#" + targetId).hide();
}

function divs_clickToToggle(targetId) {
    return jQuery("#" + targetId).toggle();
}

function toggleCSSClassUserVideo(elAddClass, elRemoveClass, elShow, elHide) {
    $(elAddClass).addClass('userVideoSearchTabSel');
    $(elRemoveClass).removeClass('userVideoSearchTabSel');
    $(elShow).show();
    $(elHide).hide();
}
/*EOF /script/divs.js*/
/*/script/jquery/jquery.autocomplete.min.js*/
/*
 * Autocomplete - jQuery plugin 1.0.2
 *
 * Copyright (c) 2007 Dylan Verheul, Dan G. Switzer, Anjesh Tuladhar, Jörn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Revision: $Id: jquery.autocomplete.js 5747 2008-06-25 18:30:55Z joern.zaefferer $
 *
 */
;
(function($) {
    $.fn.extend({
        autocomplete: function(urlOrData, options) {
            var isUrl = typeof urlOrData == "string";
            options = $.extend({}, $.Autocompleter.defaults, {
                url: isUrl ? urlOrData : null,
                data: isUrl ? null : urlOrData,
                delay: isUrl ? $.Autocompleter.defaults.delay : 10,
                max: options && !options.scroll ? 10 : 150
            }, options);
            options.highlight = options.highlight || function(value) {
                return value;
            };
            options.formatMatch = options.formatMatch || options.formatItem;
            return this.each(function() {
                new $.Autocompleter(this, options);
            });
        },
        result: function(handler) {
            return this.bind("result", handler);
        },
        search: function(handler) {
            return this.trigger("search", [handler]);
        },
        flushCache: function() {
            return this.trigger("flushCache");
        },
        setOptions: function(options) {
            return this.trigger("setOptions", [options]);
        },
        unautocomplete: function() {
            return this.trigger("unautocomplete");
        }
    });
    $.Autocompleter = function(input, options) {
        var KEY = {
            UP: 38,
            DOWN: 40,
            DEL: 46,
            TAB: 9,
            RETURN: 13,
            ESC: 27,
            COMMA: 188,
            PAGEUP: 33,
            PAGEDOWN: 34,
            BACKSPACE: 8
        };
        var $input = $(input).attr("autocomplete", "off").addClass(options.inputClass);
        var timeout;
        var previousValue = "";
        var cache = $.Autocompleter.Cache(options);
        var hasFocus = 0;
        var lastKeyPressCode;
        var config = {
            mouseDownOnSelect: false
        };
        var select = $.Autocompleter.Select(options, input, selectCurrent, config);
        var blockSubmit;
        $.browser.opera && $(input.form).bind("submit.autocomplete", function() {
            if (blockSubmit) {
                blockSubmit = false;
                return false;
            }
        });
        $input.bind(($.browser.opera ? "keypress" : "keydown") + ".autocomplete", function(event) {
            lastKeyPressCode = event.keyCode;
            switch (event.keyCode) {
                case KEY.UP:
                    event.preventDefault();
                    if (select.visible()) {
                        select.prev();
                    } else {
                        onChange(0, true);
                    }
                    break;
                case KEY.DOWN:
                    event.preventDefault();
                    if (select.visible()) {
                        select.next();
                    } else {
                        onChange(0, true);
                    }
                    break;
                case KEY.PAGEUP:
                    event.preventDefault();
                    if (select.visible()) {
                        select.pageUp();
                    } else {
                        onChange(0, true);
                    }
                    break;
                case KEY.PAGEDOWN:
                    event.preventDefault();
                    if (select.visible()) {
                        select.pageDown();
                    } else {
                        onChange(0, true);
                    }
                    break;
                case options.multiple && $.trim(options.multipleSeparator) == "," && KEY.COMMA:
                case KEY.TAB:
                case KEY.RETURN:
                    if (selectCurrent()) {
                        event.preventDefault();
                        blockSubmit = true;
                        return false;
                    }
                    break;
                case KEY.ESC:
                    select.hide();
                    break;
                default:
                    clearTimeout(timeout);
                    timeout = setTimeout(onChange, options.delay);
                    break;
            }
        }).focus(function() {
            hasFocus++;
        }).blur(function() {
            hasFocus = 0;
            if (!config.mouseDownOnSelect) {
                hideResults();
            }
        }).click(function() {
            if (hasFocus++ > 1 && !select.visible()) {
                onChange(0, true);
            }
        }).bind("search", function() {
            var fn = (arguments.length > 1) ? arguments[1] : null;

            function findValueCallback(q, data) {
                var result;
                if (data && data.length) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].result.toLowerCase() == q.toLowerCase()) {
                            result = data[i];
                            break;
                        }
                    }
                }
                if (typeof fn == "function") fn(result);
                else $input.trigger("result", result && [result.data, result.value]);
            }
            $.each(trimWords($input.val()), function(i, value) {
                request(value, findValueCallback, findValueCallback);
            });
        }).bind("flushCache", function() {
            cache.flush();
        }).bind("setOptions", function() {
            $.extend(options, arguments[1]);
            if ("data" in arguments[1]) cache.populate();
        }).bind("unautocomplete", function() {
            select.unbind();
            $input.unbind();
            $(input.form).unbind(".autocomplete");
        });

        function selectCurrent() {
            var selected = select.selected();
            if (!selected) return false;
            var v = selected.result;
            previousValue = v;
            if (options.multiple) {
                var words = trimWords($input.val());
                if (words.length > 1) {
                    v = words.slice(0, words.length - 1).join(options.multipleSeparator) + options.multipleSeparator + v;
                }
                v += options.multipleSeparator;
            }
            $input.val(v);
            hideResultsNow();
            $input.trigger("result", [selected.data, selected.value]);
            return true;
        }

        function onChange(crap, skipPrevCheck) {
            if (lastKeyPressCode == KEY.DEL) {
                select.hide();
                return;
            }
            var currentValue = $input.val();
            if (!skipPrevCheck && currentValue == previousValue) return;
            previousValue = currentValue;
            currentValue = lastWord(currentValue);
            if (currentValue.length >= options.minChars) {
                $input.addClass(options.loadingClass);
                if (!options.matchCase) currentValue = currentValue.toLowerCase();
                request(currentValue, receiveData, hideResultsNow);
            } else {
                stopLoading();
                select.hide();
            }
        };

        function trimWords(value) {
            if (!value) {
                return [""];
            }
            var words = value.split(options.multipleSeparator);
            var result = [];
            $.each(words, function(i, value) {
                if ($.trim(value)) result[i] = $.trim(value);
            });
            return result;
        }

        function lastWord(value) {
            if (!options.multiple) return value;
            var words = trimWords(value);
            return words[words.length - 1];
        }

        function autoFill(q, sValue) {
            if (options.autoFill && (lastWord($input.val()).toLowerCase() == q.toLowerCase()) && lastKeyPressCode != KEY.BACKSPACE) {
                $input.val($input.val() + sValue.substring(lastWord(previousValue).length));
                $.Autocompleter.Selection(input, previousValue.length, previousValue.length + sValue.length);
            }
        };

        function hideResults() {
            clearTimeout(timeout);
            timeout = setTimeout(hideResultsNow, 200);
        };

        function hideResultsNow() {
            var wasVisible = select.visible();
            select.hide();
            clearTimeout(timeout);
            stopLoading();
            if (options.mustMatch) {
                $input.search(function(result) {
                    if (!result) {
                        if (options.multiple) {
                            var words = trimWords($input.val()).slice(0, -1);
                            $input.val(words.join(options.multipleSeparator) + (words.length ? options.multipleSeparator : ""));
                        } else
                            $input.val("");
                    }
                });
            }
            if (wasVisible) $.Autocompleter.Selection(input, input.value.length, input.value.length);
        };

        function receiveData(q, data) {
            if (data && data.length && hasFocus) {
                stopLoading();
                select.display(data, q);
                autoFill(q, data[0].value);
                select.show();
            } else {
                hideResultsNow();
            }
        };

        function request(term, success, failure) {
            if (!options.matchCase) term = term.toLowerCase();
            var data = cache.load(term);
            if (data && data.length) {
                success(term, data);
            } else if ((typeof options.url == "string") && (options.url.length > 0)) {
                var extraParams = {
                    timestamp: +new Date()
                };
                $.each(options.extraParams, function(key, param) {
                    extraParams[key] = typeof param == "function" ? param() : param;
                });
                $.ajax({
                    mode: "abort",
                    port: "autocomplete" + input.name,
                    dataType: options.dataType,
                    url: options.url,
                    data: $.extend({
                        q: lastWord(term),
                        limit: options.max
                    }, extraParams),
                    success: function(data) {
                        var parsed = options.parse && options.parse(data) || parse(data);
                        cache.add(term, parsed);
                        success(term, parsed);
                    }
                });
            } else {
                select.emptyList();
                failure(term);
            }
        };

        function parse(data) {
            var parsed = [];
            var rows = data.split("\n");
            for (var i = 0; i < rows.length; i++) {
                var row = $.trim(rows[i]);
                if (row) {
                    row = row.split("|");
                    parsed[parsed.length] = {
                        data: row,
                        value: row[0],
                        result: options.formatResult && options.formatResult(row, row[0]) || row[0]
                    };
                }
            }
            return parsed;
        };

        function stopLoading() {
            $input.removeClass(options.loadingClass);
        };
    };
    $.Autocompleter.defaults = {
        inputClass: "ac_input",
        resultsClass: "ac_results",
        loadingClass: "ac_loading",
        minChars: 1,
        delay: 400,
        matchCase: false,
        matchSubset: true,
        matchContains: false,
        cacheLength: 10,
        max: 100,
        mustMatch: false,
        extraParams: {},
        selectFirst: true,
        formatItem: function(row) {
            return row[0];
        },
        formatMatch: null,
        autoFill: false,
        width: 0,
        multiple: false,
        multipleSeparator: ", ",
        highlight: function(value, term) {
            return value.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
        },
        scroll: true,
        scrollHeight: 180
    };
    $.Autocompleter.Cache = function(options) {
        var data = {};
        var length = 0;

        function matchSubset(s, sub) {
            if (!options.matchCase) s = s.toLowerCase();
            var i = s.indexOf(sub);
            if (i == -1) return false;
            return i == 0 || options.matchContains;
        };

        function add(q, value) {
            if (length > options.cacheLength) {
                flush();
            }
            if (!data[q]) {
                length++;
            }
            data[q] = value;
        }

        function populate() {
            if (!options.data) return false;
            var stMatchSets = {},
                nullData = 0;
            if (!options.url) options.cacheLength = 1;
            stMatchSets[""] = [];
            for (var i = 0, ol = options.data.length; i < ol; i++) {
                var rawValue = options.data[i];
                rawValue = (typeof rawValue == "string") ? [rawValue] : rawValue;
                var value = options.formatMatch(rawValue, i + 1, options.data.length);
                if (value === false) continue;
                var firstChar = value.charAt(0).toLowerCase();
                if (!stMatchSets[firstChar]) stMatchSets[firstChar] = [];
                var row = {
                    value: value,
                    data: rawValue,
                    result: options.formatResult && options.formatResult(rawValue) || value
                };
                stMatchSets[firstChar].push(row);
                if (nullData++ < options.max) {
                    stMatchSets[""].push(row);
                }
            };
            $.each(stMatchSets, function(i, value) {
                options.cacheLength++;
                add(i, value);
            });
        }
        setTimeout(populate, 25);

        function flush() {
            data = {};
            length = 0;
        }
        return {
            flush: flush,
            add: add,
            populate: populate,
            load: function(q) {
                if (!options.cacheLength || !length) return null;
                if (!options.url && options.matchContains) {
                    var csub = [];
                    for (var k in data) {
                        if (k.length > 0) {
                            var c = data[k];
                            $.each(c, function(i, x) {
                                if (matchSubset(x.value, q)) {
                                    csub.push(x);
                                }
                            });
                        }
                    }
                    return csub;
                } else
                if (data[q]) {
                    return data[q];
                } else
                if (options.matchSubset) {
                    for (var i = q.length - 1; i >= options.minChars; i--) {
                        var c = data[q.substr(0, i)];
                        if (c) {
                            var csub = [];
                            $.each(c, function(i, x) {
                                if (matchSubset(x.value, q)) {
                                    csub[csub.length] = x;
                                }
                            });
                            return csub;
                        }
                    }
                }
                return null;
            }
        };
    };
    $.Autocompleter.Select = function(options, input, select, config) {
        var CLASSES = {
            ACTIVE: "ac_over"
        };
        var listItems, active = -1,
            data, term = "",
            needsInit = true,
            element, list;

        function init() {
            if (!needsInit) return;
            element = $("<div/>").hide().addClass(options.resultsClass).css("position", "absolute").appendTo(document.body);
            list = $("<ul/>").appendTo(element).mouseover(function(event) {
                if (target(event).nodeName && target(event).nodeName.toUpperCase() == 'LI') {
                    active = $("li", list).removeClass(CLASSES.ACTIVE).index(target(event));
                    $(target(event)).addClass(CLASSES.ACTIVE);
                }
            }).click(function(event) {
                $(target(event)).addClass(CLASSES.ACTIVE);
                select();
                input.focus();
                return false;
            }).mousedown(function() {
                config.mouseDownOnSelect = true;
            }).mouseup(function() {
                config.mouseDownOnSelect = false;
            });
            if (options.width > 0) element.css("width", options.width);
            needsInit = false;
        }

        function target(event) {
            var element = event.target;
            while (element && element.tagName != "LI") element = element.parentNode;
            if (!element) return [];
            return element;
        }

        function moveSelect(step) {
            listItems.slice(active, active + 1).removeClass(CLASSES.ACTIVE);
            movePosition(step);
            var activeItem = listItems.slice(active, active + 1).addClass(CLASSES.ACTIVE);
            if (options.scroll) {
                var offset = 0;
                listItems.slice(0, active).each(function() {
                    offset += this.offsetHeight;
                });
                if ((offset + activeItem[0].offsetHeight - list.scrollTop()) > list[0].clientHeight) {
                    list.scrollTop(offset + activeItem[0].offsetHeight - list.innerHeight());
                } else if (offset < list.scrollTop()) {
                    list.scrollTop(offset);
                }
            }
        };

        function movePosition(step) {
            active += step;
            if (active < 0) {
                active = listItems.size() - 1;
            } else if (active >= listItems.size()) {
                active = 0;
            }
        }

        function limitNumberOfItems(available) {
            return options.max && options.max < available ? options.max : available;
        }

        function fillList() {
            list.empty();
            var max = limitNumberOfItems(data.length);
            for (var i = 0; i < max; i++) {
                if (!data[i]) continue;
                var formatted = options.formatItem(data[i].data, i + 1, max, data[i].value, term);
                if (formatted === false) continue;
                var li = $("<li/>").html(options.highlight(formatted, term)).addClass(i % 2 == 0 ? "ac_even" : "ac_odd").appendTo(list)[0];
                $.data(li, "ac_data", data[i]);
            }
            listItems = list.find("li");
            if (options.selectFirst) {
                listItems.slice(0, 1).addClass(CLASSES.ACTIVE);
                active = 0;
            }
            if ($.fn.bgiframe) list.bgiframe();
        }
        return {
            display: function(d, q) {
                init();
                data = d;
                term = q;
                fillList();
            },
            next: function() {
                moveSelect(1);
            },
            prev: function() {
                moveSelect(-1);
            },
            pageUp: function() {
                if (active != 0 && active - 8 < 0) {
                    moveSelect(-active);
                } else {
                    moveSelect(-8);
                }
            },
            pageDown: function() {
                if (active != listItems.size() - 1 && active + 8 > listItems.size()) {
                    moveSelect(listItems.size() - 1 - active);
                } else {
                    moveSelect(8);
                }
            },
            hide: function() {
                element && element.hide();
                listItems && listItems.removeClass(CLASSES.ACTIVE);
                active = -1;
            },
            visible: function() {
                return element && element.is(":visible");
            },
            current: function() {
                return this.visible() && (listItems.filter("." + CLASSES.ACTIVE)[0] || options.selectFirst && listItems[0]);
            },
            show: function() {
                var offset = $(input).offset();
                element.css({
                    width: typeof options.width == "string" || options.width > 0 ? options.width : $(input).width(),
                    top: offset.top + input.offsetHeight,
                    left: offset.left
                }).show();
                if (options.scroll) {
                    list.scrollTop(0);
                    list.css({
                        maxHeight: options.scrollHeight,
                        overflow: 'auto'
                    });
                    if ($.browser.msie && typeof document.body.style.maxHeight === "undefined") {
                        var listHeight = 0;
                        listItems.each(function() {
                            listHeight += this.offsetHeight;
                        });
                        var scrollbarsVisible = listHeight > options.scrollHeight;
                        list.css('height', scrollbarsVisible ? options.scrollHeight : listHeight);
                        if (!scrollbarsVisible) {
                            listItems.width(list.width() - parseInt(listItems.css("padding-left")) - parseInt(listItems.css("padding-right")));
                        }
                    }
                }
            },
            selected: function() {
                var selected = listItems && listItems.filter("." + CLASSES.ACTIVE).removeClass(CLASSES.ACTIVE);
                return selected && selected.length && $.data(selected[0], "ac_data");
            },
            emptyList: function() {
                list && list.empty();
            },
            unbind: function() {
                element && element.remove();
            }
        };
    };
    $.Autocompleter.Selection = function(field, start, end) {
        if (field.createTextRange) {
            var selRange = field.createTextRange();
            selRange.collapse(true);
            selRange.moveStart("character", start);
            selRange.moveEnd("character", end);
            selRange.select();
        } else if (field.setSelectionRange) {
            field.setSelectionRange(start, end);
        } else {
            if (field.selectionStart) {
                field.selectionStart = start;
                field.selectionEnd = end;
            }
        }
        field.focus();
    };
})(jQuery);
/*EOF /script/jquery/jquery.autocomplete.min.js*/
/*/script/jquery/jquery.bgiframe.min.js*/
/*! Copyright (c) 2013 Brandon Aaron (http://brandon.aaron.sh)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Version 3.0.1
 *
 * Requires jQuery >= 1.2.6
 */
(function(a) {
    if (typeof define === "function" && define.amd) {
        define(["jquery"], a)
    } else {
        if (typeof exports === "object") {
            module.exports = a
        } else {
            a(jQuery)
        }
    }
}(function(a) {
    a.fn.bgiframe = function(c) {
        c = a.extend({
            top: "auto",
            left: "auto",
            width: "auto",
            height: "auto",
            opacity: true,
            src: "javascript:false;",
            conditional: /MSIE 6\.0/.test(navigator.userAgent)
        }, c);
        if (!a.isFunction(c.conditional)) {
            var e = c.conditional;
            c.conditional = function() {
                return e
            }
        }
        var d = a('<iframe class="bgiframe"frameborder="0"tabindex="-1"src="' + c.src + '"style="display:block;position:absolute;z-index:-1;"/>');
        return this.each(function() {
            var h = a(this);
            if (c.conditional(this) === false) {
                return
            }
            var g = h.children("iframe.bgiframe");
            var f = g.length === 0 ? d.clone() : g;
            f.css({
                top: c.top == "auto" ? ((parseInt(h.css("borderTopWidth"), 10) || 0) * -1) + "px" : b(c.top),
                left: c.left == "auto" ? ((parseInt(h.css("borderLeftWidth"), 10) || 0) * -1) + "px" : b(c.left),
                width: c.width == "auto" ? (this.offsetWidth + "px") : b(c.width),
                height: c.height == "auto" ? (this.offsetHeight + "px") : b(c.height),
                opacity: c.opacity === true ? 0 : undefined
            });
            if (g.length === 0) {
                h.prepend(f)
            }
        })
    };
    a.fn.bgIframe = a.fn.bgiframe;

    function b(c) {
        return c && c.constructor === Number ? c + "px" : c
    }
}));
/*EOF /script/jquery/jquery.bgiframe.min.js*/
/*/script/jquery/jquery.ajaxQueue.js*/
/**
 * Ajax Queue Plugin
 *
 * Homepage: http://jquery.com/plugins/project/ajaxqueue
 * Documentation: http://docs.jquery.com/AjaxQueue
 */

/**

 <script>
 $(function(){
	jQuery.ajaxQueue({
		url: "test.php",
		success: function(html){ jQuery("ul").append(html); }
	});
	jQuery.ajaxQueue({
		url: "test.php",
		success: function(html){ jQuery("ul").append(html); }
	});
	jQuery.ajaxSync({
		url: "test.php",
		success: function(html){ jQuery("ul").append("<b>"+html+"</b>"); }
	});
	jQuery.ajaxSync({
		url: "test.php",
		success: function(html){ jQuery("ul").append("<b>"+html+"</b>"); }
	});
});
 </script>
 <ul style="position: absolute; top: 5px; right: 5px;"></ul>

 */
/*
 * Queued Ajax requests.
 * A new Ajax request won't be started until the previous queued
 * request has finished.
 */

/*
 * Synced Ajax requests.
 * The Ajax request will happen as soon as you call this method, but
 * the callbacks (success/error/complete) won't fire until all previous
 * synced requests have been completed.
 */

(function($) {

    var ajax = $.ajax;

    var pendingRequests = {};

    var synced = [];
    var syncedData = [];

    $.ajax = function(settings) {
        // create settings for compatibility with ajaxSetup
        settings = jQuery.extend(settings, jQuery.extend({}, jQuery.ajaxSettings, settings));

        var port = settings.port;

        switch (settings.mode) {
            case "abort":
                if (pendingRequests[port]) {
                    pendingRequests[port].abort();
                }
                return pendingRequests[port] = ajax.apply(this, arguments);
            case "queue":
                var _old = settings.complete;
                settings.complete = function() {
                    if (_old)
                        _old.apply(this, arguments);
                    jQuery([ajax]).dequeue("ajax" + port);;
                };

                jQuery([ajax]).queue("ajax" + port, function() {
                    ajax(settings);
                });
                return;
            case "sync":
                var pos = synced.length;

                synced[pos] = {
                    error: settings.error,
                    success: settings.success,
                    complete: settings.complete,
                    done: false
                };

                syncedData[pos] = {
                    error: [],
                    success: [],
                    complete: []
                };

                settings.error = function() {
                    syncedData[pos].error = arguments;
                };
                settings.success = function() {
                    syncedData[pos].success = arguments;
                };
                settings.complete = function() {
                    syncedData[pos].complete = arguments;
                    synced[pos].done = true;

                    if (pos == 0 || !synced[pos - 1])
                        for (var i = pos; i < synced.length && synced[i].done; i++) {
                            if (synced[i].error) synced[i].error.apply(jQuery, syncedData[i].error);
                            if (synced[i].success) synced[i].success.apply(jQuery, syncedData[i].success);
                            if (synced[i].complete) synced[i].complete.apply(jQuery, syncedData[i].complete);

                            synced[i] = null;
                            syncedData[i] = null;
                        }
                };
        }
        return ajax.apply(this, arguments);
    };

})(jQuery);
/*EOF /script/jquery/jquery.ajaxQueue.js*/
/*/script/jquery/ui/jquery.ui.touch-punch.min.js*/
/*
 * jQuery UI Touch Punch 0.2.2
 *
 * Copyright 2011, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
;
(function(b) {
    b.support.touch = "ontouchend" in document;
    if (!b.support.touch) {
        return;
    }
    var c = b.ui.mouse.prototype,
        e = c._mouseInit,
        a;

    function d(g, h) {
        if (g.originalEvent.touches.length > 1) {
            return;
        }
        g.preventDefault();
        var i = g.originalEvent.changedTouches[0],
            f = document.createEvent("MouseEvents");
        f.initMouseEvent(h, true, true, window, 1, i.screenX, i.screenY, i.clientX, i.clientY, false, false, false, false, 0, null);
        g.target.dispatchEvent(f);
    }
    c._touchStart = function(g) {
        var f = this;
        if (a || !f._mouseCapture(g.originalEvent.changedTouches[0])) {
            return;
        }
        a = true;
        f._touchMoved = false;
        d(g, "mouseover");
        d(g, "mousemove");
        d(g, "mousedown");
    };
    c._touchMove = function(f) {
        if (!a) {
            return;
        }
        this._touchMoved = true;
        d(f, "mousemove");
    };
    c._touchEnd = function(f) {
        if (!a) {
            return;
        }
        d(f, "mouseup");
        d(f, "mouseout");
        if (!this._touchMoved) {
            d(f, "click");
        }
        a = false;
    };
    c._mouseInit = function() {
        var f = this;
        f.element.bind("touchstart", b.proxy(f, "_touchStart")).bind("touchmove", b.proxy(f, "_touchMove")).bind("touchend", b.proxy(f, "_touchEnd"));
        e.call(f);
    };
})(jQuery);
/*EOF /script/jquery/ui/jquery.ui.touch-punch.min.js*/
/*/Scripts/jquery.caret/jquery.caret.min.js*/
/*
 *
 * Copyright (c) 2010 C. F., Wong (<a href="http://cloudgen.w0ng.hk">Cloudgen Examplet Store</a>)
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 *
 */
﻿
(function(k, e, i, j) {
    k.fn.caret = function(b, l) {
        var a, c, f = this[0],
            d = k.browser.msie;
        if (typeof b === "object" && typeof b.start === "number" && typeof b.end === "number") {
            a = b.start;
            c = b.end
        } else if (typeof b === "number" && typeof l === "number") {
            a = b;
            c = l
        } else if (typeof b === "string")
            if ((a = f.value.indexOf(b)) > -1) c = a + b[e];
            else a = null;
        else if (Object.prototype.toString.call(b) === "[object RegExp]") {
            b = b.exec(f.value);
            if (b != null) {
                a = b.index;
                c = a + b[0][e]
            }
        }
        if (typeof a != "undefined") {
            if (d) {
                d = this[0].createTextRange();
                d.collapse(true);
                d.moveStart("character", a);
                d.moveEnd("character", c - a);
                d.select()
            } else {
                this[0].selectionStart = a;
                this[0].selectionEnd = c
            }
            this[0].focus();
            return this
        } else {
            if (d) {
                c = document.selection;
                if (this[0].tagName.toLowerCase() != "textarea") {
                    d = this.val();
                    a = c[i]()[j]();
                    a.moveEnd("character", d[e]);
                    var g = a.text == "" ? d[e] : d.lastIndexOf(a.text);
                    a = c[i]()[j]();
                    a.moveStart("character", -d[e]);
                    var h = a.text[e]
                } else {
                    a = c[i]();
                    c = a[j]();
                    c.moveToElementText(this[0]);
                    c.setEndPoint("EndToEnd", a);
                    g = c.text[e] - a.text[e];
                    h = g + a.text[e]
                }
            } else {
                g =
                    f.selectionStart;
                h = f.selectionEnd
            }
            a = f.value.substring(g, h);
            return {
                start: g,
                end: h,
                text: a,
                replace: function(m) {
                    return f.value.substring(0, g) + m + f.value.substring(h, f.value[e])
                }
            }
        }
    }
})(jQuery, "length", "createRange", "duplicate");
/*EOF /Scripts/jquery.caret/jquery.caret.min.js*/
/*/script/jquery/jquery.maskedinput.js*/
/*
	Masked Input plugin for jQuery
	Copyright (c) 2007-2013 Josh Bush (digitalbush.com)
	Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
	Version: 1.3.1
*/
(function(e) {
    function t() {
        var e = document.createElement("input"),
            t = "onpaste";
        return e.setAttribute(t, ""), "function" == typeof e[t] ? "paste" : "input"
    }
    var n, a = t() + ".mask",
        r = navigator.userAgent,
        i = /iphone/i.test(r),
        o = /android/i.test(r);
    e.mask = {
        definitions: {
            9: "[0-9]",
            a: "[A-Za-z]",
            "*": "[A-Za-z0-9]"
        },
        dataName: "rawMaskFn",
        placeholder: "_"
    }, e.fn.extend({
        caret: function(e, t) {
            var n;
            if (0 !== this.length && !this.is(":hidden")) return "number" == typeof e ? (t = "number" == typeof t ? t : e, this.each(function() {
                this.setSelectionRange ? this.setSelectionRange(e, t) : this.createTextRange && (n = this.createTextRange(), n.collapse(!0), n.moveEnd("character", t), n.moveStart("character", e), n.select())
            })) : (this[0].setSelectionRange ? (e = this[0].selectionStart, t = this[0].selectionEnd) : document.selection && document.selection.createRange && (n = document.selection.createRange(), e = 0 - n.duplicate().moveStart("character", -1e5), t = e + n.text.length), {
                begin: e,
                end: t
            })
        },
        unmask: function() {
            return this.trigger("unmask")
        },
        mask: function(t, r) {
            var c, l, s, u, f, h;
            return !t && this.length > 0 ? (c = e(this[0]), c.data(e.mask.dataName)()) : (r = e.extend({
                placeholder: e.mask.placeholder,
                completed: null
            }, r), l = e.mask.definitions, s = [], u = h = t.length, f = null, e.each(t.split(""), function(e, t) {
                "?" == t ? (h--, u = e) : l[t] ? (s.push(RegExp(l[t])), null === f && (f = s.length - 1)) : s.push(null)
            }), this.trigger("unmask").each(function() {
                function c(e) {
                    for (; h > ++e && !s[e];);
                    return e
                }

                function d(e) {
                    for (; --e >= 0 && !s[e];);
                    return e
                }

                function m(e, t) {
                    var n, a;
                    if (!(0 > e)) {
                        for (n = e, a = c(t); h > n; n++)
                            if (s[n]) {
                                if (!(h > a && s[n].test(R[a]))) break;
                                R[n] = R[a], R[a] = r.placeholder, a = c(a)
                            }
                        b(), x.caret(Math.max(f, e))
                    }
                }

                function p(e) {
                    var t, n, a, i;
                    for (t = e, n = r.placeholder; h > t; t++)
                        if (s[t]) {
                            if (a = c(t), i = R[t], R[t] = n, !(h > a && s[a].test(i))) break;
                            n = i
                        }
                }

                function g(e) {
                    var t, n, a, r = e.which;
                    8 === r || 46 === r || i && 127 === r ? (t = x.caret(), n = t.begin, a = t.end, 0 === a - n && (n = 46 !== r ? d(n) : a = c(n - 1), a = 46 === r ? c(a) : a), k(n, a), m(n, a - 1), e.preventDefault()) : 27 == r && (x.val(S), x.caret(0, y()), e.preventDefault())
                }

                function v(t) {
                    var n, a, i, l = t.which,
                        u = x.caret();
                    t.ctrlKey || t.altKey || t.metaKey || 32 > l || l && (0 !== u.end - u.begin && (k(u.begin, u.end), m(u.begin, u.end - 1)), n = c(u.begin - 1), h > n && (a = String.fromCharCode(l), s[n].test(a) && (p(n), R[n] = a, b(), i = c(n), o ? setTimeout(e.proxy(e.fn.caret, x, i), 0) : x.caret(i), r.completed && i >= h && r.completed.call(x))), t.preventDefault())
                }

                function k(e, t) {
                    var n;
                    for (n = e; t > n && h > n; n++) s[n] && (R[n] = r.placeholder)
                }

                function b() {
                    x.val(R.join(""))
                }

                function y(e) {
                    var t, n, a = x.val(),
                        i = -1;
                    for (t = 0, pos = 0; h > t; t++)
                        if (s[t]) {
                            for (R[t] = r.placeholder; pos++ < a.length;)
                                if (n = a.charAt(pos - 1), s[t].test(n)) {
                                    R[t] = n, i = t;
                                    break
                                }
                            if (pos > a.length) break
                        } else R[t] === a.charAt(pos) && t !== u && (pos++, i = t);
                    return e ? b() : u > i + 1 ? (x.val(""), k(0, h)) : (b(), x.val(x.val().substring(0, i + 1))), u ? t : f
                }
                var x = e(this),
                    R = e.map(t.split(""), function(e) {
                        return "?" != e ? l[e] ? r.placeholder : e : void 0
                    }),
                    S = x.val();
                x.data(e.mask.dataName, function() {
                    return e.map(R, function(e, t) {
                        return s[t] && e != r.placeholder ? e : null
                    }).join("")
                }), x.attr("readonly") || x.one("unmask", function() {
                    x.unbind(".mask").removeData(e.mask.dataName)
                }).bind("focus.mask", function() {
                    clearTimeout(n);
                    var e;
                    S = x.val(), e = y(), n = setTimeout(function() {
                        b(), e == t.length ? x.caret(0, e) : x.caret(e)
                    }, 10)
                }).bind("blur.mask", function() {
                    y(), x.val() != S && x.change()
                }).bind("keydown.mask", g).bind("keypress.mask", v).bind(a, function() {
                    setTimeout(function() {
                        var e = y(!0);
                        x.caret(e), r.completed && e == x.val().length && r.completed.call(x)
                    }, 0)
                }), y()
            }))
        }
    })
})(jQuery);
/*EOF /script/jquery/jquery.maskedinput.js*/
/*/Scripts/Roomster/Roomster.Megaphone.js*/
$.extend(Roomster.Megaphone, {
    GetMegahoneUsersCount: function(serviceType, filtersUrl, counterCallBackSuccess, counterCallBackFailure) {
        var url = $.url.action({
            controller: 'Megaphone',
            action: 'GetCount',
            id: $.url.optional
        }) + serviceType + '/' + filtersUrl;
        $.get(url).done(counterCallBackSuccess).fail(counterCallBackFailure);
    },
    SendMessage: function(serviceType, filtersUrl, message, sendMessageCallBackSuccess, sendMessageCallBackFailure) {
        var url = $.url.action({
            controller: 'Megaphone',
            action: 'Send',
            id: $.url.optional
        }) + serviceType + '/' + filtersUrl;
        $.post(url, {
            message: message
        }).done(sendMessageCallBackSuccess).fail(sendMessageCallBackFailure);
    }
});
/*EOF /Scripts/Roomster/Roomster.Megaphone.js*/
/*/Scripts/Roomster/Roomster.Megaphone.UI.js*/
;
(function() {
    var haveThemeClass = "placesService";
    var proposalThemeClass = "peopleService";

    $.extend(Roomster.Megaphone.UI, {
        IsInitialized: false,
        _dialog: null,
        _tabs: null,
        _btn: null,
        _filters: ['MegaphoneFilterPanel', 'Slider', 'MegaphoneCategoryFilter', 'MegaphoneBudgetFilter', 'MegaphoneAgeFilter', 'MegaphoneSexFilter', 'MegaphoneAppartmentTypesFilter'],
        serviceTypeAdditionalData: {
            HaveShare: {
                TitleTemplates: Roomster.Resources.GetTranslation('Introduce yourself to <strong>{{= it.Count}} members with private rooms</strong> in your area.'),
                SuccessMessagePostfix: 'with private rooms.',
                ThemeClass: haveThemeClass,
            },
            HaveApartment: {
                TitleTemplates: Roomster.Resources.GetTranslation('Introduce yourself to <strong>{{= it.Count}} members with entire places</strong> in your area.'),
                SuccessMessagePostfix: 'with entire places.',
                ThemeClass: haveThemeClass,
            },
            NeedRoom: {
                TitleTemplates: Roomster.Resources.GetTranslation('Introduce yourself to <strong>{{= it.Count}} members looking for rooms</strong> in your area.'),
                SuccessMessagePostfix: 'looking for rooms.',
                ThemeClass: proposalThemeClass,
            },
            NeedApartment: {
                TitleTemplates: Roomster.Resources.GetTranslation('Introduce yourself to <strong>{{= it.Count}} members looking for entire places</strong> in your area.'),
                SuccessMessagePostfix: 'looking for entire places.',
                ThemeClass: proposalThemeClass,
            }
        },
        dialogTemplate: {
            name: "MegaphoneDialog",
            url: '' // for support affilate routing should be initialized in Init method
        },
        Init: function() {
            for (var i = 0, count = this._filters.length; i < count; i++) {
                Roomster.Templates.LoadTemplates([{
                    name: this._filters[i],
                    url: '/Content/Filters/' + this._filters[i] + '.html'
                }]);
            }

            this.dialogTemplate.url = $.url.action({
                controller: 'Megaphone',
                action: 'GetDialog'
            });
            Roomster.Templates.LoadTemplates([this.dialogTemplate]);

            Roomster.Proxy("OnMessageSent", "Show", "OnActivateTab", "ShowCountMessage", "_OnMegaphoneClick", "SendMessage", "OnClose", this);

            this.InitMegaphoneButton();
        },
        OnClose: function() {
            $(".successViewMegaphone", this._dialog).hide();
        },
        SendMessage: function() {
            var message = this._dialog.find("textarea").val();
            if (!message || message.replace(/^\s+|\s+$/g, '') == '')
                return false;
            var serviceType = this.GetCurrentServiceType();
            var filtersUrl = this.ComposeUrl(this.GetForm(serviceType));
            Roomster.Megaphone.SendMessage(serviceType, filtersUrl, message, this.OnMessageSent);
            return false;
        },
        OnMessageSent: function() {
            this._tabs.hide();
            this._dialog.find("textarea").val('');
            $("#messageBoard", this._dialog).hide();
            $(".successViewMegaphone", this._dialog).show();
        },
        OnActivateTab: function(event, ui) {
            var curentService = this.GetCurrentServiceType();
            this._dialog.removeClass([proposalThemeClass, haveThemeClass].join(" "));
            this._dialog.addClass(this.serviceTypeAdditionalData[curentService].ThemeClass);
            this.GetCount(curentService);
        },
        Show: function() {
            if (!this._dialog) {
                $(window.document.body).append(Roomster.Templates.Apply(this.dialogTemplate.name));
                this._dialog = $("#megaphoneDialog");
                this._tabs = $("#megaphoneTabs").tabs({
                    activate: this.OnActivateTab
                });
                $(".quickLinkMegaphone", this._dialog).click(function() {
                    $("#megaphoneMessage").val($("#megaphoneMessage").val() + this.textContent + '\n');
                });
                $("#messageBoard form", this._dialog).submit(this.SendMessage);
            }

            this._tabs.show();
            var serviceType = Roomster.Search.SearchSettings.GetServiceType() != ServiceTypeEnum.Undefined ? Roomster.Search.SearchSettings.GetServiceType() : ServiceTypeEnum.HaveShare;
            this._tabs.tabs("option", "active", $('a[servicetypeid="' + serviceType + '"]', this._tabs).parent().index());
            this._dialog.dialog({
                width: '691',
                height: 'auto',
                modal: true,
                resizable: false,
                draggable: false,
                closeOnEscape: true,
                dialogClass: 'commonDialog',
                close: this.OnClose,
                position: ({
                    at: "center center-60"
                })
            });
            this.GetCount(serviceType);
        },
        Hide: function() {
            this._dialog.dialog();
        },
        GetCurrentServiceType: function() {
            return $("li.ui-tabs-active", this._tabs).attr('aria-controls');
        },
        GetCurrentServiceTypeId: function() {
            var currentTab = $("li.ui-tabs-active a", this._tabs);
            if (currentTab.length == 0)
                return ServiceTypeEnum.HaveShare;
            return parseInt(currentTab.attr("servicetypeid"));
        },
        GetForm: function(serviceType) {
            return serviceType ? $("#" + serviceType + " form") : $(">div:eq(" + this._tabs.tabs('option', 'active') + ") form", this._tabs);
        },
        GetCount: function(serviceType) {
            if (!serviceType)
                serviceType = this.GetCurrentServiceTypeId();
            var form = this.GetForm(this.GetCurrentServiceType());
            if (form.length == 0) {
                $("#messageBoard", this._dialog).hide();
                $(".noListings", this._dialog).show();
            } else {
                $("#messageBoard", this._dialog).show();
                $(".noListings", this._dialog).hide();
            }
            var filtersUrl = this.ComposeUrl(form);

            Roomster.Megaphone.GetMegahoneUsersCount(serviceType, filtersUrl, this.ShowCountMessage);
        },
        ComposeUrl: function(form) {
            var url = '';
            $.each($(":hidden", form), function() {
                var $this = $(this);
                if ($this.val() != "")
                    url = url + (url == '' ? url : '/') + $this.attr('name') + '--' + $this.val();
            });
            return url;
        },
        ShowCountMessage: function(result) {
            var curentServiceType = this.GetCurrentServiceType();
            var titleMessage = '';
            if (result.Enabled) {
                $(".recipientsNumber", $('#' + curentServiceType)).html(result.Count);
                $(".recipientsNumber", $('.successViewMegaphone')).html(result.Count);
                //$("#successMegaphoneMessagePostfix", $('.successViewMegaphone')).html(this.serviceTypeAdditionalData[curentServiceType].SuccessMessagePostfix);
                titleMessage = doT.template(this.serviceTypeAdditionalData[curentServiceType].TitleTemplates)(result);
            }

            //if (result.Count > 0)
            //{
            //    $("#messageBoard", this._dialog).show();
            //    $(".noListings", this._dialog).hide();
            //}

            this.SetTitle(titleMessage);
        },
        SetTitle: function(html) {
            $(".recipientsTitle", this._dialog).html(html);
        },
        InitMegaphoneButton: function() {
            this._btn = $(".megaphoneLink");
            this._megaphoneinitattempts = 5;
            this._btn.on("click", this._OnMegaphoneClick);
        },
        _GetActiveServiceTypeBySearchTab: function() {
            return $('.searchTopTab.searchTopTabActive a').attr('href');
        },
        _OnMegaphoneClick: function() {
            this.Show();
            return false;
        }
    });
})();
/*EOF /Scripts/Roomster/Roomster.Megaphone.UI.js*/
/*/Scripts/geo/google.markerclusterer.min.js*/
function MarkerClusterer(a, b, c) {
    this.extend(MarkerClusterer, google.maps.OverlayView);
    this.map_ = a;
    this.markers_ = [];
    this.clusters_ = [];
    this.sizes = [53, 56, 66, 78, 90];
    this.styles_ = [];
    this.ready_ = !1;
    c = c || {};
    this.gridSize_ = c.gridSize || 60;
    this.minClusterSize_ = c.minimumClusterSize || 2;
    this.maxZoom_ = c.maxZoom || null;
    this.styles_ = c.styles || [];
    this.imagePath_ = c.imagePath || this.MARKER_CLUSTER_IMAGE_PATH_;
    this.imageExtension_ = c.imageExtension || this.MARKER_CLUSTER_IMAGE_EXTENSION_;
    this.zoomOnClick_ = !0;
    void 0 != c.zoomOnClick &&
    (this.zoomOnClick_ = c.zoomOnClick);
    this.averageCenter_ = !1;
    void 0 != c.averageCenter && (this.averageCenter_ = c.averageCenter);
    this.setupStyles_();
    this.setMap(a);
    this.prevZoom_ = this.map_.getZoom();
    var d = this;
    google.maps.event.addListener(this.map_, "zoom_changed", function() {
        var a = d.map_.getZoom(),
            b = d.map_.minZoom || 0,
            c = Math.min(d.map_.maxZoom || 100, d.map_.mapTypes[d.map_.getMapTypeId()].maxZoom),
            a = Math.min(Math.max(a, b), c);
        d.prevZoom_ != a && (d.prevZoom_ = a, d.resetViewport())
    });
    google.maps.event.addListener(this.map_,
        "idle",
        function() {
            d.redraw()
        });
    b && (b.length || Object.keys(b).length) && this.addMarkers(b, !1)
}
MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_PATH_ = "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m";
MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_EXTENSION_ = "png";
MarkerClusterer.prototype.extend = function(a, b) {
    return function(a) {
        for (var b in a.prototype) this.prototype[b] = a.prototype[b];
        return this
    }.apply(a, [b])
};
MarkerClusterer.prototype.onAdd = function() {
    this.setReady_(!0)
};
MarkerClusterer.prototype.draw = function() {};
MarkerClusterer.prototype.setupStyles_ = function() {
    if (!this.styles_.length)
        for (var a = 0, b; b = this.sizes[a]; a++) this.styles_.push({
            url: this.imagePath_ + (a + 1) + "." + this.imageExtension_,
            height: b,
            width: b
        })
};
MarkerClusterer.prototype.fitMapToMarkers = function() {
    for (var a = this.getMarkers(), b = new google.maps.LatLngBounds, c = 0, d; d = a[c]; c++) b.extend(d.getPosition());
    this.map_.fitBounds(b)
};
MarkerClusterer.prototype.setStyles = function(a) {
    this.styles_ = a
};
MarkerClusterer.prototype.getStyles = function() {
    return this.styles_
};
MarkerClusterer.prototype.setZoomClick = function(a) {
    this.zoomOnClick_ = a
};
MarkerClusterer.prototype.isZoomOnClick = function() {
    return this.zoomOnClick_
};
MarkerClusterer.prototype.isAverageCenter = function() {
    return this.averageCenter_
};
MarkerClusterer.prototype.getMarkers = function() {
    return this.markers_
};
MarkerClusterer.prototype.getTotalMarkers = function() {
    return this.markers_.length
};
MarkerClusterer.prototype.setMaxZoom = function(a) {
    this.maxZoom_ = a
};
MarkerClusterer.prototype.getMaxZoom = function() {
    return this.maxZoom_
};
MarkerClusterer.prototype.calculator_ = function(a, b) {
    for (var c = 0, d = a.length, e = d; 0 !== e;) e = parseInt(e / 10, 10), c++;
    c = Math.min(c, b);
    return {
        text: d,
        index: c
    }
};
MarkerClusterer.prototype.setCalculator = function(a) {
    this.calculator_ = a
};
MarkerClusterer.prototype.getCalculator = function() {
    return this.calculator_
};
MarkerClusterer.prototype.addMarkers = function(a, b) {
    if (a.length)
        for (var c = 0, d; d = a[c]; c++) this.pushMarkerTo_(d);
    else if (Object.keys(a).length)
        for (d in a) this.pushMarkerTo_(a[d]);
    b || this.redraw()
};
MarkerClusterer.prototype.pushMarkerTo_ = function(a) {
    a.isAdded = !1;
    if (a.draggable) {
        var b = this;
        google.maps.event.addListener(a, "dragend", function() {
            a.isAdded = !1;
            b.repaint()
        })
    }
    this.markers_.push(a)
};
MarkerClusterer.prototype.addMarker = function(a, b) {
    this.pushMarkerTo_(a);
    b || this.redraw()
};
MarkerClusterer.prototype.removeMarker_ = function(a) {
    var b = -1;
    if (this.markers_.indexOf) b = this.markers_.indexOf(a);
    else
        for (var c = 0, d; d = this.markers_[c]; c++)
            if (d == a) {
                b = c;
                break
            } if (-1 == b) return !1;
    a.setMap(null);
    this.markers_.splice(b, 1);
    return !0
};
MarkerClusterer.prototype.removeMarker = function(a, b) {
    var c = this.removeMarker_(a);
    return !b && c ? (this.resetViewport(), this.redraw(), !0) : !1
};
MarkerClusterer.prototype.removeMarkers = function(a, b) {
    for (var c = !1, d = 0, e; e = a[d]; d++) e = this.removeMarker_(e), c = c || e;
    if (!b && c) return this.resetViewport(), this.redraw(), !0
};
MarkerClusterer.prototype.setReady_ = function(a) {
    this.ready_ || (this.ready_ = a, this.createClusters_())
};
MarkerClusterer.prototype.getTotalClusters = function() {
    return this.clusters_.length
};
MarkerClusterer.prototype.getMap = function() {
    return this.map_
};
MarkerClusterer.prototype.setMap = function(a) {
    this.map_ = a
};
MarkerClusterer.prototype.getGridSize = function() {
    return this.gridSize_
};
MarkerClusterer.prototype.setGridSize = function(a) {
    this.gridSize_ = a
};
MarkerClusterer.prototype.getMinClusterSize = function() {
    return this.minClusterSize_
};
MarkerClusterer.prototype.setMinClusterSize = function(a) {
    this.minClusterSize_ = a
};
MarkerClusterer.prototype.getExtendedBounds = function(a) {
    var b = this.getProjection(),
        c = new google.maps.LatLng(a.getNorthEast().lat(), a.getNorthEast().lng()),
        d = new google.maps.LatLng(a.getSouthWest().lat(), a.getSouthWest().lng()),
        c = b.fromLatLngToDivPixel(c);
    c.x += this.gridSize_;
    c.y -= this.gridSize_;
    d = b.fromLatLngToDivPixel(d);
    d.x -= this.gridSize_;
    d.y += this.gridSize_;
    c = b.fromDivPixelToLatLng(c);
    b = b.fromDivPixelToLatLng(d);
    a.extend(c);
    a.extend(b);
    return a
};
MarkerClusterer.prototype.isMarkerInBounds_ = function(a, b) {
    return b.contains(a.getPosition())
};
MarkerClusterer.prototype.clearMarkers = function() {
    this.resetViewport(!0);
    this.markers_ = []
};
MarkerClusterer.prototype.resetViewport = function(a) {
    for (var b = 0, c; c = this.clusters_[b]; b++) c.remove();
    for (b = 0; c = this.markers_[b]; b++) c.isAdded = !1, a && c.setMap(null);
    this.clusters_ = []
};
MarkerClusterer.prototype.repaint = function() {
    var a = this.clusters_.slice();
    this.clusters_.length = 0;
    this.resetViewport();
    this.redraw();
    window.setTimeout(function() {
        for (var b = 0, c; c = a[b]; b++) c.remove()
    }, 0)
};
MarkerClusterer.prototype.redraw = function() {
    this.createClusters_()
};
MarkerClusterer.prototype.distanceBetweenPoints_ = function(a, b) {
    if (!a || !b) return 0;
    var c = (b.lat() - a.lat()) * Math.PI / 180,
        d = (b.lng() - a.lng()) * Math.PI / 180,
        c = Math.sin(c / 2) * Math.sin(c / 2) + Math.cos(a.lat() * Math.PI / 180) * Math.cos(b.lat() * Math.PI / 180) * Math.sin(d / 2) * Math.sin(d / 2);
    return 12742 * Math.atan2(Math.sqrt(c), Math.sqrt(1 - c))
};
MarkerClusterer.prototype.addToClosestCluster_ = function(a) {
    var b = 4E4,
        c = null;
    a.getPosition();
    for (var d = 0, e; e = this.clusters_[d]; d++) {
        var f = e.getCenter();
        f && (f = this.distanceBetweenPoints_(f, a.getPosition()), f < b && (b = f, c = e))
    }
    c && c.isMarkerInClusterBounds(a) ? c.addMarker(a) : (e = new Cluster(this), e.addMarker(a), this.clusters_.push(e))
};
MarkerClusterer.prototype.createClusters_ = function() {
    if (this.ready_)
        for (var a = new google.maps.LatLngBounds(this.map_.getBounds().getSouthWest(), this.map_.getBounds().getNorthEast()), a = this.getExtendedBounds(a), b = 0, c; c = this.markers_[b]; b++) !c.isAdded && this.isMarkerInBounds_(c, a) && this.addToClosestCluster_(c)
};

function Cluster(a) {
    this.markerClusterer_ = a;
    this.map_ = a.getMap();
    this.gridSize_ = a.getGridSize();
    this.minClusterSize_ = a.getMinClusterSize();
    this.averageCenter_ = a.isAverageCenter();
    this.center_ = null;
    this.markers_ = [];
    this.bounds_ = null;
    this.clusterIcon_ = new ClusterIcon(this, a.getStyles(), a.getGridSize())
}
Cluster.prototype.isMarkerAlreadyAdded = function(a) {
    if (this.markers_.indexOf) return -1 != this.markers_.indexOf(a);
    for (var b = 0, c; c = this.markers_[b]; b++)
        if (c == a) return !0;
    return !1
};
Cluster.prototype.addMarker = function(a) {
    if (this.isMarkerAlreadyAdded(a)) return !1;
    if (!this.center_) this.center_ = a.getPosition(), this.calculateBounds_();
    else if (this.averageCenter_) {
        var b = this.markers_.length + 1,
            c = (this.center_.lat() * (b - 1) + a.getPosition().lat()) / b,
            b = (this.center_.lng() * (b - 1) + a.getPosition().lng()) / b;
        this.center_ = new google.maps.LatLng(c, b);
        this.calculateBounds_()
    }
    a.isAdded = !0;
    this.markers_.push(a);
    c = this.markers_.length;
    c < this.minClusterSize_ && a.getMap() != this.map_ && a.setMap(this.map_);
    if (c == this.minClusterSize_)
        for (b = 0; b < c; b++) this.markers_[b].setMap(null);
    c >= this.minClusterSize_ && a.setMap(null);
    this.updateIcon();
    return !0
};
Cluster.prototype.getMarkerClusterer = function() {
    return this.markerClusterer_
};
Cluster.prototype.getBounds = function() {
    for (var a = new google.maps.LatLngBounds(this.center_, this.center_), b = this.getMarkers(), c = 0, d; d = b[c]; c++) a.extend(d.getPosition());
    return a
};
Cluster.prototype.remove = function() {
    this.clusterIcon_.remove();
    this.markers_.length = 0;
    delete this.markers_
};
Cluster.prototype.getSize = function() {
    return this.markers_.length
};
Cluster.prototype.getMarkers = function() {
    return this.markers_
};
Cluster.prototype.getCenter = function() {
    return this.center_
};
Cluster.prototype.calculateBounds_ = function() {
    var a = new google.maps.LatLngBounds(this.center_, this.center_);
    this.bounds_ = this.markerClusterer_.getExtendedBounds(a)
};
Cluster.prototype.isMarkerInClusterBounds = function(a) {
    return this.bounds_.contains(a.getPosition())
};
Cluster.prototype.getMap = function() {
    return this.map_
};
Cluster.prototype.updateIcon = function() {
    var a = this.map_.getZoom(),
        b = this.markerClusterer_.getMaxZoom();
    if (b && a > b)
        for (a = 0; b = this.markers_[a]; a++) b.setMap(this.map_);
    else this.markers_.length < this.minClusterSize_ ? this.clusterIcon_.hide() : (a = this.markerClusterer_.getStyles().length, a = this.markerClusterer_.getCalculator()(this.markers_, a), this.clusterIcon_.setCenter(this.center_), this.clusterIcon_.setSums(a), this.clusterIcon_.show())
};

function ClusterIcon(a, b, c) {
    a.getMarkerClusterer().extend(ClusterIcon, google.maps.OverlayView);
    this.styles_ = b;
    this.padding_ = c || 0;
    this.cluster_ = a;
    this.center_ = null;
    this.map_ = a.getMap();
    this.sums_ = this.div_ = null;
    this.visible_ = !1;
    this.setMap(this.map_)
}
ClusterIcon.prototype.triggerClusterClick = function() {
    var a = this.cluster_.getMarkerClusterer();
    google.maps.event.trigger(a, "clusterclick", this.cluster_);
    a.isZoomOnClick() && this.map_.fitBounds(this.cluster_.getBounds())
};
ClusterIcon.prototype.onAdd = function() {
    this.div_ = document.createElement("DIV");
    if (this.visible_) {
        var a = this.getPosFromLatLng_(this.center_);
        this.div_.style.cssText = this.createCss(a);
        this.div_.innerHTML = this.sums_.text
    }
    this.getPanes().overlayMouseTarget.appendChild(this.div_);
    var b = this;
    google.maps.event.addDomListener(this.div_, "click", function() {
        b.triggerClusterClick()
    })
};
ClusterIcon.prototype.getPosFromLatLng_ = function(a) {
    a = this.getProjection().fromLatLngToDivPixel(a);
    a.x -= parseInt(this.width_ / 2, 10);
    a.y -= parseInt(this.height_ / 2, 10);
    return a
};
ClusterIcon.prototype.draw = function() {
    if (this.visible_) {
        var a = this.getPosFromLatLng_(this.center_);
        this.div_.style.top = a.y + "px";
        this.div_.style.left = a.x + "px"
    }
};
ClusterIcon.prototype.hide = function() {
    this.div_ && (this.div_.style.display = "none");
    this.visible_ = !1
};
ClusterIcon.prototype.show = function() {
    if (this.div_) {
        var a = this.getPosFromLatLng_(this.center_);
        this.div_.style.cssText = this.createCss(a);
        this.div_.style.display = ""
    }
    this.visible_ = !0
};
ClusterIcon.prototype.remove = function() {
    this.setMap(null)
};
ClusterIcon.prototype.onRemove = function() {
    this.div_ && this.div_.parentNode && (this.hide(), this.div_.parentNode.removeChild(this.div_), this.div_ = null)
};
ClusterIcon.prototype.setSums = function(a) {
    this.sums_ = a;
    this.text_ = a.text;
    this.index_ = a.index;
    this.div_ && (this.div_.innerHTML = a.text);
    this.useStyle()
};
ClusterIcon.prototype.useStyle = function() {
    var a = Math.max(0, this.sums_.index - 1),
        a = Math.min(this.styles_.length - 1, a),
        a = this.styles_[a];
    this.url_ = a.url;
    this.height_ = a.height;
    this.width_ = a.width;
    this.textColor_ = a.textColor;
    this.anchor_ = a.anchor;
    this.textSize_ = a.textSize;
    this.backgroundPosition_ = a.backgroundPosition
};
ClusterIcon.prototype.setCenter = function(a) {
    this.center_ = a
};
ClusterIcon.prototype.createCss = function(a) {
    var b = [];
    b.push("background-image:url(" + this.url_ + ");");
    b.push("background-position:" + (this.backgroundPosition_ ? this.backgroundPosition_ : "0 0") + ";");
    "object" === typeof this.anchor_ ? ("number" === typeof this.anchor_[0] && 0 < this.anchor_[0] && this.anchor_[0] < this.height_ ? b.push("height:" + (this.height_ - this.anchor_[0]) + "px; padding-top:" + this.anchor_[0] + "px;") : b.push("height:" + this.height_ + "px; line-height:" + this.height_ + "px;"), "number" === typeof this.anchor_[1] &&
    0 < this.anchor_[1] && this.anchor_[1] < this.width_ ? b.push("width:" + (this.width_ - this.anchor_[1]) + "px; padding-left:" + this.anchor_[1] + "px;") : b.push("width:" + this.width_ + "px; text-align:center;")) : b.push("height:" + this.height_ + "px; line-height:" + this.height_ + "px; width:" + this.width_ + "px; text-align:center;");
    b.push("cursor:pointer; top:" + a.y + "px; left:" + a.x + "px; color:" + (this.textColor_ ? this.textColor_ : "black") + "; position:absolute; font-size:" + (this.textSize_ ? this.textSize_ : 11) + "px; font-family:Arial,sans-serif; font-weight:bold");
    return b.join("")
};
window.MarkerClusterer = MarkerClusterer;
MarkerClusterer.prototype.addMarker = MarkerClusterer.prototype.addMarker;
MarkerClusterer.prototype.addMarkers = MarkerClusterer.prototype.addMarkers;
MarkerClusterer.prototype.clearMarkers = MarkerClusterer.prototype.clearMarkers;
MarkerClusterer.prototype.fitMapToMarkers = MarkerClusterer.prototype.fitMapToMarkers;
MarkerClusterer.prototype.getCalculator = MarkerClusterer.prototype.getCalculator;
MarkerClusterer.prototype.getGridSize = MarkerClusterer.prototype.getGridSize;
MarkerClusterer.prototype.getExtendedBounds = MarkerClusterer.prototype.getExtendedBounds;
MarkerClusterer.prototype.getMap = MarkerClusterer.prototype.getMap;
MarkerClusterer.prototype.getMarkers = MarkerClusterer.prototype.getMarkers;
MarkerClusterer.prototype.getMaxZoom = MarkerClusterer.prototype.getMaxZoom;
MarkerClusterer.prototype.getStyles = MarkerClusterer.prototype.getStyles;
MarkerClusterer.prototype.getTotalClusters = MarkerClusterer.prototype.getTotalClusters;
MarkerClusterer.prototype.getTotalMarkers = MarkerClusterer.prototype.getTotalMarkers;
MarkerClusterer.prototype.redraw = MarkerClusterer.prototype.redraw;
MarkerClusterer.prototype.removeMarker = MarkerClusterer.prototype.removeMarker;
MarkerClusterer.prototype.removeMarkers = MarkerClusterer.prototype.removeMarkers;
MarkerClusterer.prototype.resetViewport = MarkerClusterer.prototype.resetViewport;
MarkerClusterer.prototype.repaint = MarkerClusterer.prototype.repaint;
MarkerClusterer.prototype.setCalculator = MarkerClusterer.prototype.setCalculator;
MarkerClusterer.prototype.setGridSize = MarkerClusterer.prototype.setGridSize;
MarkerClusterer.prototype.setMaxZoom = MarkerClusterer.prototype.setMaxZoom;
MarkerClusterer.prototype.onAdd = MarkerClusterer.prototype.onAdd;
MarkerClusterer.prototype.draw = MarkerClusterer.prototype.draw;
MarkerClusterer.prototype.setZoomClick = MarkerClusterer.prototype.setZoomClick;
Cluster.prototype.getCenter = Cluster.prototype.getCenter;
Cluster.prototype.getSize = Cluster.prototype.getSize;
Cluster.prototype.getMarkers = Cluster.prototype.getMarkers;
ClusterIcon.prototype.onAdd = ClusterIcon.prototype.onAdd;
ClusterIcon.prototype.draw = ClusterIcon.prototype.draw;
ClusterIcon.prototype.onRemove = ClusterIcon.prototype.onRemove;
Object.keys = Object.keys || function(a) {
    var b = [],
        c;
    for (c in a) a.hasOwnProperty(c) && b.push(c);
    return b
};
/*EOF /Scripts/geo/google.markerclusterer.min.js*/