/*/Scripts/jquery.selectric.min.js*/
/*! Selectric ϟ v1.9.2 (2015-04-07) - git.io/tjl9sQ - Copyright (c) 2015 Leonardo Santos - Dual licensed: MIT/GPL */
!function(e){"use strict";var t="selectric",s="Input Items Open Disabled TempShow HideSelect Wrapper Hover Responsive Above Scroll Group GroupLabel",o=".sl",i={onChange:function(t){e(t).change()},maxHeight:300,keySearchTimeout:500,arrowButtonMarkup:'<b class="button">&#x25be;</b>',disableOnMobile:!0,openOnHover:!1,hoverIntentTimeout:500,expandToItemText:!1,responsive:!1,preventWindowScroll:!0,inheritOriginalWidth:!1,allowWrap:!0,customClass:{prefix:t,camelCase:!1,overwrite:!0},optionsItemBuilder:"{text}",labelBuilder:"{text}"},n={add:function(e,t,s){this[e]||(this[e]={}),this[e][t]=s},remove:function(e,t){delete this[e][t]}},l={replaceDiacritics:function(e){for(var t="40-46 50-53 54-57 62-70 71-74 61 47 77".replace(/\d+/g,"\\3$&").split(" "),s=t.length;s--;)e=e.toLowerCase().replace(RegExp("["+t[s]+"]","g"),"aeiouncy".charAt(s));return e},format:function(e){var t=arguments;return(""+e).replace(/{(\d+|(\w+))}/g,function(e,s,o){return o&&t[1]?t[1][o]:t[s]})},nextEnabledItem:function(e,t){for(;e[t=(t+1)%e.length].disabled;);return t},previousEnabledItem:function(e,t){for(;e[t=(t>0?t:e.length)-1].disabled;);return t},toDash:function(e){return e.replace(/([a-z])([A-Z])/g,"$1-$2").toLowerCase()},triggerCallback:function(s,o){var i=o.element,a=o.options["on"+s];e.isFunction(a)&&a.call(i,i,o),n[s]&&e.each(n[s],function(){this.call(i,i,o)}),e(i).trigger(t+"-"+l.toDash(s),o)}},a=e(document),r=e(window),c=function(n,c){function d(t){if(j.options=e.extend(!0,{},i,j.options,t),j.classes={},j.element=n,l.triggerCallback("BeforeInit",j),j.options.disableOnMobile&&Y)return void(j.disableOnMobile=!0);C(!0);var o=j.options.customClass,a=s.split(" "),r=F.width();e.each(a,function(e,t){var s=o.prefix+t;j.classes[t.toLowerCase()]=o.camelCase?s:l.toDash(s)}),x=e("<input/>",{"class":j.classes.input,readonly:Y}),k=e("<div/>",{"class":j.classes.items,tabindex:-1}),T=e("<div/>",{"class":j.classes.scroll}),D=e("<div/>",{"class":o.prefix,html:j.options.arrowButtonMarkup}),I=e('<p class="label"/>'),y=F.wrap("<div>").parent().append(D.prepend(I),k,x),A={open:m,close:g,destroy:C,refresh:u,init:d},F.on(A).wrap('<div class="'+j.classes.hideselect+'">'),e.extend(j,A),$=j.options.labelBuilder,j.options.inheritOriginalWidth&&r>0&&y.width(r),p()}function p(){j.items=[];var t=F.children(),s="<ul>",i=t.filter(":selected").index(),n=0;B=S=~i?i:0,(E=t.length)&&(t.each(function(){function t(){var t=e(this),o=t.html(),i=t.prop("disabled"),a=j.options.optionsItemBuilder;j.items[n]={element:t,value:t.val(),text:o,slug:l.replaceDiacritics(o),disabled:i},s+=l.format('<li data-index="{1}" class="{2}">{3}</li>',n,e.trim([n==B?"selected":"",n==E-1?"last":"",i?"disabled":""].join(" ")),e.isFunction(a)?a(j.items[n],t,n):l.format(a,j.items[n])),n++}var o=e(this);if(o.is("optgroup")){var i=o.prop("disabled"),a=o.children();s+=l.format('<ul class="{1}"><li class="{2}">{3}</li>',e.trim([j.classes.group,i?"disabled":"",o.prop("class")].join(" ")),j.classes.grouplabel,o.prop("label")),i&&a.prop("disabled",!0),a.each(t),s+="</ul>"}else t.call(o)}),k.append(T.html(s+"</ul>")),I.html(e.isFunction($)?$(j.items[B]):l.format($,j.items[B]))),D.add(F).add(y).add(x).off(o),y.prop("class",[j.classes.wrapper,j.options.customClass.overwrite?F.prop("class").replace(/\S+/g,j.options.customClass.prefix+"-$&"):F.prop("class"),j.options.responsive?j.classes.responsive:""].join(" ")),F.prop("disabled")?(y.addClass(j.classes.disabled),x.prop("disabled",!0)):(R=!0,y.removeClass(j.classes.disabled).on("mouseenter"+o+" mouseleave"+o,function(t){e(this).toggleClass(j.classes.hover),j.options.openOnHover&&(clearTimeout(j.closeTimer),"mouseleave"==t.type?j.closeTimer=setTimeout(g,j.options.hoverIntentTimeout):m())}),D.on("click"+o,function(e){L?g():m(e)}),x.prop({tabindex:P,disabled:!1}).on("keypress"+o,h).on("keydown"+o,function(e){h(e),clearTimeout(j.resetStr),j.resetStr=setTimeout(function(){x.val("")},j.options.keySearchTimeout);var t=e.keyCode||e.which;if(t>36&&41>t){if(!j.options.allowWrap&&(39>t&&0==S||t>38&&S+1==j.items.length))return;b(l[(39>t?"previous":"next")+"EnabledItem"](j.items,S))}}).on("focusin"+o,function(e){x.one("blur",function(){x.blur()}),L||m(e)}).on("oninput"in x[0]?"input":"keyup",function(){x.val().length&&e.each(j.items,function(e,t){return RegExp("^"+x.val(),"i").test(t.slug)&&!t.disabled?(b(e),!1):void 0})}),F.prop("tabindex",!1),O=e("li",k.removeAttr("style")).on({mousedown:function(e){e.preventDefault(),e.stopPropagation()},click:function(){return b(e(this).data("index"),!0),!1}}).filter("[data-index]")),l.triggerCallback("Init",j)}function u(){l.triggerCallback("Refresh",j),p()}function h(e){var t=e.keyCode||e.which;13==t&&e.preventDefault(),/^(9|13|27)$/.test(t)&&(e.stopPropagation(),b(S,!0))}function f(){var e=k.closest(":visible").children(":hidden").addClass(j.classes.tempshow),t=j.options.maxHeight,s=k.outerWidth(),o=D.outerWidth()-(s-k.width());!j.options.expandToItemText||o>s?M=o:(k.css("overflow","scroll"),y.width(9e4),M=k.width(),k.css("overflow",""),y.width("")),k.width(M).height()>t&&k.height(t),e.removeClass(j.classes.tempshow)}function m(s){l.triggerCallback("BeforeOpen",j),s&&(s.preventDefault(),s.stopPropagation()),R&&(f(),e("."+j.classes.hideselect,"."+j.classes.open).children()[t]("close"),L=!0,H=k.outerHeight(),W=k.height(),y.addClass(j.classes.open),x.val("").is(":focus")||x.focus(),a.on("click"+o,g).on("scroll"+o,v),v(),j.options.preventWindowScroll&&a.on("mousewheel"+o+" DOMMouseScroll"+o,"."+j.classes.scroll,function(t){var s=t.originalEvent,o=e(this).scrollTop(),i=0;"detail"in s&&(i=-1*s.detail),"wheelDelta"in s&&(i=s.wheelDelta),"wheelDeltaY"in s&&(i=s.wheelDeltaY),"deltaY"in s&&(i=-1*s.deltaY),(o==this.scrollHeight-W&&0>i||0==o&&i>0)&&t.preventDefault()}),w(S),l.triggerCallback("Open",j))}function v(){y.toggleClass(j.classes.above,y.offset().top+y.outerHeight()+H>r.scrollTop()+r.height())}function g(){if(l.triggerCallback("BeforeClose",j),B!=S){l.triggerCallback("BeforeChange",j);var t=j.items[S].text;F.prop("selectedIndex",B=S).data("value",t),I.html(e.isFunction($)?$(j.items[S]):l.format($,j.items[S])),l.triggerCallback("Change",j)}a.off(o),y.removeClass(j.classes.open),L=!1,l.triggerCallback("Close",j)}function b(e,t){void 0!=e&&(j.items[e].disabled||(O.removeClass("selected").eq(S=e).addClass("selected"),w(e),t&&g()))}function w(e){var t=O.eq(e).outerHeight(),s=O[e].offsetTop,o=T.scrollTop(),i=s+2*t;T.scrollTop(i>o+H?i-H:o>s-t?s-t:o)}function C(e){R&&(k.add(D).add(x).remove(),!e&&F.removeData(t).removeData("value"),F.prop("tabindex",P).off(o).off(A).unwrap().unwrap(),R=!1)}var x,k,T,D,I,y,O,S,B,H,W,M,E,A,$,j=this,F=e(n),L=!1,R=!1,Y=/android|ip(hone|od|ad)/i.test(navigator.userAgent),P=F.prop("tabindex");d(c)};e.fn[t]=function(s){return this.each(function(){var o=e.data(this,t);o&&!o.disableOnMobile?""+s===s&&o[s]?o[s]():o.init(s):e.data(this,t,new c(this,s))})},e.fn[t].hooks=n}(jQuery);
/*EOF /Scripts/jquery.selectric.min.js*/
/*/Scripts/pikaday.min.js*/
/*!
 * Pikaday
 *
 * Copyright © 2014 David Bushell | BSD & MIT license | https://github.com/dbushell/Pikaday
 */
!function (t, e) { "use strict"; var n; if ("object" == typeof exports) { try { n = require("moment") } catch (i) { } module.exports = e(n) } else "function" == typeof define && define.amd ? define(function (t) { var i = "moment"; try { n = t(i) } catch (a) { } return e(n) }) : t.Pikaday = e(t.moment) }(this, function (t) { "use strict"; var e = "function" == typeof t, n = !!window.addEventListener, i = window.document, a = window.setTimeout, o = function (t, e, i, a) { n ? t.addEventListener(e, i, !!a) : t.attachEvent("on" + e, i) }, s = function (t, e, i, a) { n ? t.removeEventListener(e, i, !!a) : t.detachEvent("on" + e, i) }, r = function (t, e, n) { var a; i.createEvent ? (a = i.createEvent("HTMLEvents"), a.initEvent(e, !0, !1), a = D(a, n), t.dispatchEvent(a)) : i.createEventObject && (a = i.createEventObject(), a = D(a, n), t.fireEvent("on" + e, a)) }, h = function (t) { return t.trim ? t.trim() : t.replace(/^\s+|\s+$/g, "") }, l = function (t, e) { return -1 !== (" " + t.className + " ").indexOf(" " + e + " ") }, u = function (t, e) { l(t, e) || (t.className = "" === t.className ? e : t.className + " " + e) }, d = function (t, e) { t.className = h((" " + t.className + " ").replace(" " + e + " ", " ")) }, c = function (t) { return /Array/.test(Object.prototype.toString.call(t)) }, f = function (t) { return /Date/.test(Object.prototype.toString.call(t)) && !isNaN(t.getTime()) }, m = function (t) { var e = t.getDay(); return 0 === e || 6 === e }, p = function (t) { return t % 4 === 0 && t % 100 !== 0 || t % 400 === 0 }, g = function (t, e) { return [31, p(t) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][e] }, y = function (t) { f(t) && t.setHours(0, 0, 0, 0) }, v = function (t, e) { return t.getTime() === e.getTime() }, D = function (t, e, n) { var i, a; for (i in e) a = void 0 !== t[i], a && "object" == typeof e[i] && null !== e[i] && void 0 === e[i].nodeName ? f(e[i]) ? n && (t[i] = new Date(e[i].getTime())) : c(e[i]) ? n && (t[i] = e[i].slice(0)) : t[i] = D({}, e[i], n) : (n || !a) && (t[i] = e[i]); return t }, _ = function (t) { return t.month < 0 && (t.year -= Math.ceil(Math.abs(t.month) / 12), t.month += 12), t.month > 11 && (t.year += Math.floor(Math.abs(t.month) / 12), t.month -= 12), t }, b = { field: null, bound: void 0, position: "bottom left", reposition: !0, format: "YYYY-MM-DD", defaultDate: null, setDefaultDate: !1, firstDay: 0, minDate: null, maxDate: null, yearRange: 10, showWeekNumber: !1, minYear: 0, maxYear: 9999, minMonth: void 0, maxMonth: void 0, isRTL: !1, yearSuffix: "", showMonthAfterYear: !1, numberOfMonths: 1, mainCalendar: "left", container: void 0, i18n: { previousMonth: "Previous Month", nextMonth: "Next Month", months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], weekdays: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], weekdaysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"] }, theme: null, onSelect: null, onOpen: null, onClose: null, onDraw: null }, w = function (t, e, n) { for (e += t.firstDay; e >= 7;) e -= 7; return n ? t.i18n.weekdaysShort[e] : t.i18n.weekdays[e] }, M = function (t, e, n, i, a, o, s) { if (s) return '<td class="is-empty"></td>'; var r = []; return o && r.push("is-disabled"), a && r.push("is-today"), i && r.push("is-selected"), '<td data-day="' + t + '" class="' + r.join(" ") + '"><button class="pika-button pika-day" type="button" data-pika-year="' + n + '" data-pika-month="' + e + '" data-pika-day="' + t + '">' + t + "</button></td>" }, k = function (t, e, n) { var i = new Date(n, 0, 1), a = Math.ceil(((new Date(n, e, t) - i) / 864e5 + i.getDay() + 1) / 7); return '<td class="pika-week">' + a + "</td>" }, x = function (t, e) { return "<tr>" + (e ? t.reverse() : t).join("") + "</tr>" }, N = function (t) { return "<tbody>" + t.join("") + "</tbody>" }, C = function (t) { var e, n = []; for (t.showWeekNumber && n.push("<th></th>"), e = 0; 7 > e; e++) n.push('<th scope="col"><abbr title="' + w(t, e) + '">' + w(t, e, !0) + "</abbr></th>"); return "<thead>" + (t.isRTL ? n.reverse() : n).join("") + "</thead>" }, T = function (t, e, n, i, a) { var o, s, r, h, l, u = t._o, d = n === u.minYear, f = n === u.maxYear, m = '<div class="pika-title">', p = !0, g = !0; for (r = [], o = 0; 12 > o; o++) r.push('<option value="' + (n === a ? o - e : 12 + o - e) + '"' + (o === i ? " selected" : "") + (d && o < u.minMonth || f && o > u.maxMonth ? "disabled" : "") + ">" + u.i18n.months[o] + "</option>"); for (h = '<div class="pika-label">' + u.i18n.months[i] + '<select class="pika-select pika-select-month">' + r.join("") + "</select></div>", c(u.yearRange) ? (o = u.yearRange[0], s = u.yearRange[1] + 1) : (o = n - u.yearRange, s = 1 + n + u.yearRange), r = []; s > o && o <= u.maxYear; o++) o >= u.minYear && r.push('<option value="' + o + '"' + (o === n ? " selected" : "") + ">" + o + "</option>"); return l = '<div class="pika-label">' + n + u.yearSuffix + '<select class="pika-select pika-select-year">' + r.join("") + "</select></div>", m += u.showMonthAfterYear ? l + h : h + l, d && (0 === i || u.minMonth >= i) && (p = !1), f && (11 === i || u.maxMonth <= i) && (g = !1), 0 === e && (m += '<button class="pika-prev' + (p ? "" : " is-disabled") + '" type="button">' + u.i18n.previousMonth + "</button>"), e === t._o.numberOfMonths - 1 && (m += '<button class="pika-next' + (g ? "" : " is-disabled") + '" type="button">' + u.i18n.nextMonth + "</button>"), m += "</div>" }, R = function (t, e) { return '<table cellpadding="0" cellspacing="0" class="pika-table">' + C(t) + N(e) + "</table>" }, Y = function (s) { var r = this, h = r.config(s); r._onMouseDown = function (t) { if (r._v) { t = t || window.event; var e = t.target || t.srcElement; if (e) { if (!l(e.parentNode, "is-disabled")) { if (l(e, "pika-button") && !l(e, "is-empty")) return r.setDate(new Date(e.getAttribute("data-pika-year"), e.getAttribute("data-pika-month"), e.getAttribute("data-pika-day"))), void (h.bound && a(function () { r.hide(), h.field && h.field.blur() }, 100)); l(e, "pika-prev") ? r.prevMonth() : l(e, "pika-next") && r.nextMonth() } if (l(e, "pika-select")) r._c = !0; else { if (!t.preventDefault) return t.returnValue = !1, !1; t.preventDefault() } } } }, r._onChange = function (t) { t = t || window.event; var e = t.target || t.srcElement; e && (l(e, "pika-select-month") ? r.gotoMonth(e.value) : l(e, "pika-select-year") && r.gotoYear(e.value)) }, r._onInputChange = function (n) { var i; n.firedBy !== r && (e ? (i = t(h.field.value, h.format), i = i && i.isValid() ? i.toDate() : null) : i = new Date(Date.parse(h.field.value)), r.setDate(f(i) ? i : null), r._v || r.show()) }, r._onInputFocus = function () { r.show() }, r._onInputClick = function () { r.show() }, r._onInputBlur = function () { var t = i.activeElement; do if (l(t, "pika-single")) return; while (t = t.parentNode); r._c || (r._b = a(function () { r.hide() }, 50)), r._c = !1 }, r._onClick = function (t) { t = t || window.event; var e = t.target || t.srcElement, i = e; if (e) { !n && l(e, "pika-select") && (e.onchange || (e.setAttribute("onchange", "return;"), o(e, "change", r._onChange))); do if (l(i, "pika-single") || i === h.trigger) return; while (i = i.parentNode); r._v && e !== h.trigger && i !== h.trigger && r.hide() } }, r.el = i.createElement("div"), r.el.className = "pika-single" + (h.isRTL ? " is-rtl" : "") + (h.theme ? " " + h.theme : ""), o(r.el, "ontouchend" in i ? "touchend" : "mousedown", r._onMouseDown, !0), o(r.el, "change", r._onChange), h.field && (h.container ? h.container.appendChild(r.el) : h.bound ? i.body.appendChild(r.el) : h.field.parentNode.insertBefore(r.el, h.field.nextSibling), o(h.field, "change", r._onInputChange), h.defaultDate || (h.defaultDate = e && h.field.value ? t(h.field.value, h.format).toDate() : new Date(Date.parse(h.field.value)), h.setDefaultDate = !0)); var u = h.defaultDate; f(u) ? h.setDefaultDate ? r.setDate(u, !0) : r.gotoDate(u) : r.gotoDate(new Date), h.bound ? (this.hide(), r.el.className += " is-bound", o(h.trigger, "click", r._onInputClick), o(h.trigger, "focus", r._onInputFocus), o(h.trigger, "blur", r._onInputBlur)) : this.show() }; return Y.prototype = { config: function (t) { this._o || (this._o = D({}, b, !0)); var e = D(this._o, t, !0); e.isRTL = !!e.isRTL, e.field = e.field && e.field.nodeName ? e.field : null, e.theme = "string" == typeof e.theme && e.theme ? e.theme : null, e.bound = !!(void 0 !== e.bound ? e.field && e.bound : e.field), e.trigger = e.trigger && e.trigger.nodeName ? e.trigger : e.field, e.disableWeekends = !!e.disableWeekends, e.disableDayFn = "function" == typeof e.disableDayFn ? e.disableDayFn : null; var n = parseInt(e.numberOfMonths, 10) || 1; if (e.numberOfMonths = n > 4 ? 4 : n, f(e.minDate) || (e.minDate = !1), f(e.maxDate) || (e.maxDate = !1), e.minDate && e.maxDate && e.maxDate < e.minDate && (e.maxDate = e.minDate = !1), e.minDate && this.setMinDate(e.minDate), e.maxDate && (y(e.maxDate), e.maxYear = e.maxDate.getFullYear(), e.maxMonth = e.maxDate.getMonth()), c(e.yearRange)) { var i = (new Date).getFullYear() - 10; e.yearRange[0] = parseInt(e.yearRange[0], 10) || i, e.yearRange[1] = parseInt(e.yearRange[1], 10) || i } else e.yearRange = Math.abs(parseInt(e.yearRange, 10)) || b.yearRange, e.yearRange > 100 && (e.yearRange = 100); return e }, toString: function (n) { return f(this._d) ? e ? t(this._d).format(n || this._o.format) : this._d.toDateString() : "" }, getMoment: function () { return e ? t(this._d) : null }, setMoment: function (n, i) { e && t.isMoment(n) && this.setDate(n.toDate(), i) }, getDate: function () { return f(this._d) ? new Date(this._d.getTime()) : null }, setDate: function (t, e) { if (!t) return this._d = null, this._o.field && (this._o.field.value = "", r(this._o.field, "change", { firedBy: this })), this.draw(); if ("string" == typeof t && (t = new Date(Date.parse(t))), f(t)) { var n = this._o.minDate, i = this._o.maxDate; f(n) && n > t ? t = n : f(i) && t > i && (t = i), this._d = new Date(t.getTime()), y(this._d), this.gotoDate(this._d), this._o.field && (this._o.field.value = this.toString(), r(this._o.field, "change", { firedBy: this })), e || "function" != typeof this._o.onSelect || this._o.onSelect.call(this, this.getDate()) } }, gotoDate: function (t) { var e = !0; if (f(t)) { if (this.calendars) { var n = new Date(this.calendars[0].year, this.calendars[0].month, 1), i = new Date(this.calendars[this.calendars.length - 1].year, this.calendars[this.calendars.length - 1].month, 1), a = t.getTime(); i.setMonth(i.getMonth() + 1), i.setDate(i.getDate() - 1), e = a < n.getTime() || i.getTime() < a } e && (this.calendars = [{ month: t.getMonth(), year: t.getFullYear() }], "right" === this._o.mainCalendar && (this.calendars[0].month += 1 - this._o.numberOfMonths)), this.adjustCalendars() } }, adjustCalendars: function () { this.calendars[0] = _(this.calendars[0]); for (var t = 1; t < this._o.numberOfMonths; t++) this.calendars[t] = _({ month: this.calendars[0].month + t, year: this.calendars[0].year }); this.draw() }, gotoToday: function () { this.gotoDate(new Date) }, gotoMonth: function (t) { isNaN(t) || (this.calendars[0].month = parseInt(t, 10), this.adjustCalendars()) }, nextMonth: function () { this.calendars[0].month++, this.adjustCalendars() }, prevMonth: function () { this.calendars[0].month--, this.adjustCalendars() }, gotoYear: function (t) { isNaN(t) || (this.calendars[0].year = parseInt(t, 10), this.adjustCalendars()) }, setMinDate: function (t) { y(t), this._o.minDate = t, this._o.minYear = t.getFullYear(), this._o.minMonth = t.getMonth() }, setMaxDate: function (t) { this._o.maxDate = t }, draw: function (t) { if (this._v || t) { var e = this._o, n = e.minYear, i = e.maxYear, o = e.minMonth, s = e.maxMonth, r = ""; this._y <= n && (this._y = n, !isNaN(o) && this._m < o && (this._m = o)), this._y >= i && (this._y = i, !isNaN(s) && this._m > s && (this._m = s)); for (var h = 0; h < e.numberOfMonths; h++) r += '<div class="pika-lendar">' + T(this, h, this.calendars[h].year, this.calendars[h].month, this.calendars[0].year) + this.render(this.calendars[h].year, this.calendars[h].month) + "</div>"; if (this.el.innerHTML = r, e.bound && "hidden" !== e.field.type && a(function () { e.trigger.focus() }, 1), "function" == typeof this._o.onDraw) { var l = this; a(function () { l._o.onDraw.call(l) }, 0) } } }, adjustPosition: function () { if (!this._o.container) { var t, e, n, a = this._o.trigger, o = a, s = this.el.offsetWidth, r = this.el.offsetHeight, h = window.innerWidth || i.documentElement.clientWidth, l = window.innerHeight || i.documentElement.clientHeight, u = window.pageYOffset || i.body.scrollTop || i.documentElement.scrollTop; if ("function" == typeof a.getBoundingClientRect) n = a.getBoundingClientRect(), t = n.left + window.pageXOffset, e = n.bottom + window.pageYOffset; else for (t = o.offsetLeft, e = o.offsetTop + o.offsetHeight; o = o.offsetParent;) t += o.offsetLeft, e += o.offsetTop; (this._o.reposition && t + s > h || this._o.position.indexOf("right") > -1 && t - s + a.offsetWidth > 0) && (t = t - s + a.offsetWidth), (this._o.reposition && e + r > l + u || this._o.position.indexOf("top") > -1 && e - r - a.offsetHeight > 0) && (e = e - r - a.offsetHeight), this.el.style.cssText = ["position: absolute", "left: " + t + "px", "top: " + e + "px"].join(";") } }, render: function (t, e) { var n = this._o, i = new Date, a = g(t, e), o = new Date(t, e, 1).getDay(), s = [], r = []; y(i), n.firstDay > 0 && (o -= n.firstDay, 0 > o && (o += 7)); for (var h = a + o, l = h; l > 7;) l -= 7; h += 7 - l; for (var u = 0, d = 0; h > u; u++) { var c = new Date(t, e, 1 + (u - o)), p = f(this._d) ? v(c, this._d) : !1, D = v(c, i), _ = o > u || u >= a + o, b = n.minDate && c < n.minDate || n.maxDate && c > n.maxDate || n.disableWeekends && m(c) || n.disableDayFn && n.disableDayFn(c); r.push(M(1 + (u - o), e, t, p, D, b, _)), 7 === ++d && (n.showWeekNumber && r.unshift(k(u - o, e, t)), s.push(x(r, n.isRTL)), r = [], d = 0) } return R(n, s) }, isVisible: function () { return this._v }, show: function () { this._v || (d(this.el, "is-hidden"), this._v = !0, this.draw(), this._o.bound && (o(i, "click", this._onClick), this.adjustPosition()), "function" == typeof this._o.onOpen && this._o.onOpen.call(this)) }, hide: function () { var t = this._v; t !== !1 && (this._o.bound && s(i, "click", this._onClick), this.el.style.cssText = "", u(this.el, "is-hidden"), this._v = !1, void 0 !== t && "function" == typeof this._o.onClose && this._o.onClose.call(this)) }, destroy: function () { this.hide(), s(this.el, "mousedown", this._onMouseDown, !0), s(this.el, "change", this._onChange), this._o.field && (s(this._o.field, "change", this._onInputChange), this._o.bound && (s(this._o.trigger, "click", this._onInputClick), s(this._o.trigger, "focus", this._onInputFocus), s(this._o.trigger, "blur", this._onInputBlur))), this.el.parentNode && this.el.parentNode.removeChild(this.el) } }, Y });
/*EOF /Scripts/pikaday.min.js*/
/*/Scripts/Roomster/Roomster.Affiliate.js*/
$.extend(Roomster.Affiliate,
{
    Loader: "loader",
    PromoAssets: {},

    Init: function()
    {
    },

    LoadStates: function(statesElement, state)
    {
        $.get("StatesByCountry?countryCode=" + state, function(data) {
            var stateList = $(statesElement);
            stateList.html("");
            for (var i = 0; i < data.length; i++)
            {
                stateList.append($("<option></option>").val(data[i].Code).html(data[i].Name));
            }
            if (data.length === 0)
            {
                stateList.append($("<option></option>").val("--").html("Not applicable"));
            }
            stateList.prop("disabled", data.length === 0);
        });
    },
    ShowFinInfoForm: function(paymentType)
    {
        $(".pmtWire").hide();
        $(".pmtCheck").hide();
        $(".pmtPayPal").hide();

        $(".pmt" + paymentType).show();
    },
    AdjustValidation: function(paymentType)
    {
        $(".pmtWire input").addClass("ignore");
        $(".pmtCheck input").addClass("ignore");
        $(".pmtPayPal input").addClass("ignore");

        $(".pmt" + paymentType + " input").removeClass("ignore");

        //$('.pmtWire input').attr('data-val', 'false');
        //$('.pmtCheck input').attr('data-val', 'false');
        //$('.pmtPayPal input').attr('data-val', 'false');

        //$('.pmt' + paymentType + ' input').attr('data-val', 'true');
    },
    LoadQualityScore: function(selector)
    {
        $.get("/api/affiliate/qualityscore", function(data) {
            var min = data.min;
            var max = data.max;

            var gaugeOptions = {
                chart: { type: "solidgauge" },
                title: null,
                pane: {
                    center: ["50%", "85%"],
                    size: "140%",
                    startAngle: -90,
                    endAngle: 90,
                    background: {
                        backgroundColor: "#EEE",
                        innerRadius: "60%",
                        outerRadius: "100%",
                        shape: "arc"
                    }
                },
                tooltip: {
                    enabled: false
                },
                // the value axis
                yAxis: {
                    stops: [
                        [0.25, "#FF4136"], // red
                        [0.50, "#FF851B"], // orange
                        [0.75, "#FFDC00"], // yellow
                        [1.00, "#2ECC40"] // green
                    ],
                    lineWidth: 0,
                    minorTickInterval: null,
                    tickPixelInterval: 400,
                    tickWidth: 0,
                    title: {
                        y: -80
                    },
                    labels: {
                        y: 16,
                        style: { "display": "none" }
                    }
                },
                plotOptions: {
                    solidgauge: {
                        dataLabels: {
                            y: 5,
                            borderWidth: 0,
                            useHTML: true
                        }
                    }
                }
            };
            // The gauge
            $(selector).highcharts(Highcharts.merge(gaugeOptions, {
                yAxis: {
                    min: min,
                    max: max,
                    title: {
                        text: ""
                    }
                },
                credits: {
                    enabled: false
                },
                series: [
                    {
                        name: "QualityScore",
                        data: [data.Score * 100],
                        dataLabels: {
                            //format: '<div style="text-align:center"><span style="font-size:25px;color:black;">{y}</span>'
                            formatter: function()
                            {
                                var lbl = "";

                                if (this.y / max <= 0.25)
                                {
                                    lbl = "Poor";
                                } else if (this.y / max <= 0.50)
                                {
                                    lbl = "Fair";
                                } else if (this.y / max <= 0.75)
                                {
                                    lbl = "Good";
                                } else
                                {
                                    lbl = "Excellent";
                                }

                                return "<div class=\"align-center quality-score-label\"><span>" + lbl + "</span></div>";
                            }
                        },
                        tooltip: {
                            valueSuffix: ""
                        }
                    }
                ]
            }));
        });
    },
    AccountsByCountry: function(selector)
    {
        $('.loader').show();

        $.get("/api/affiliate/incentiveratestats",
            function(data) {

                var categories = [];
                var series_data = [];

                for (var i = 0; i < data.Stats.length; i++)
                {
                    //console.log(data.Stats[i]);
                    categories.push(data.Stats[i].CountryName);
                    series_data.push(data.Stats[i].PaymentPeriodCurrentAccounts || 0);
                }

                $(selector)
                    .highcharts({
                            chart: {
                                type: 'bar',
                                height: 60 * data.Stats.length
                            },
                            title: {
                                text: null
                            },
                            subtitle: {
                                text: null
                            },
                            xAxis: {
                                categories: categories,
                                title: {
                                    text: null
                                }
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: null,
                                    align: 'high'
                                },
                                labels: {
                                    overflow: 'justify'
                                },
                                allowDecimals: false
                            },
                            tooltip: {
                                valueSuffix: ''
                            },
                            plotOptions: {
                                bar: {
                                    dataLabels: {
                                        enabled: true
                                    }
                                }
                            },
                            legend: {
                                enabled: false
                            },
                            credits: {
                                enabled: false
                            },
                            exporting: {
                                enabled: false
                            },
                            series: [
                                {
                                    name: 'accounts',
                                    data: series_data
                                }
                            ]
                        }
                    );
            });
        $('.loader').hide();
    },
    IncentiveRates: function(selector)
    {
        $('.loader').show();

        var bulletOptions = {
            chart: {
                inverted: true,
                marginLeft: 135,
                type: "bullet",
                marginTop: 15,
                marginBottom: 20
            },
            title: {
                text: null
            },
            legend: {
                enabled: false
            },
            yAxis: {
                gridLineWidth: 0,
                allowDecimals: false
            },
            plotOptions: {
                series: {
                    pointPadding: 0.25,
                    borderWidth: 1,
                    color: "#fff",                    
                    borderColor: "#555",
                    targetOptions: {
                        width: "0%" //200%
                    }
                },
                bullet: {
                    dataLabels: {
                        enabled: true,
                        //style: {"color": "contrast", "fontSize": "11px", "fontWeight": "bold", "textOutline": "1px contrast" },
                        style: {"color": "contrast", "fontSize": "12px", "fontWeight": "normal", "textOutline": "none" },
                    }
                }
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            }
        };

        $.get("/api/affiliate/incentiveratestats",
            function(data) {
                
                for (var i = 0; i < data.Stats.length; i++)
                {
                    Roomster.Affiliate.IncentiveRateGraph(selector, i, data.Stats[i], bulletOptions);
                }

                $('.loader').hide();

            });
    },
    IncentiveRateGraph: function(selector, graph_id, stats, bulletOptions)
    {
        //console.log(graph_id);
        //console.log(stats);

        var current_count = stats.PaymentPeriodCurrentAccounts || 0;

        var t1_count = stats.Rates[0].Count;
        var t1_rate = stats.Rates[0].Rate;
        var t1_payout = stats.PayoutRate * t1_rate;

        var t2_count = stats.Rates[1].Count;
        var t2_rate = stats.Rates[1].Rate;
        var t2_payout = stats.PayoutRate * t2_rate;

        var target = (t2_count - t1_count) * 0.5 + t2_count;
        if (target % 2 !== 0) target++;

        var weak = '#ff8080'; //#e00;
        var fair = '#ffd633'; //#fc0
        var strong = '#5cd65c'; //#4dc964

        var graph = $("<div />", { "class": "incentive-graph", "id": "incentive-graph-" + graph_id, /*text: stats.CountryName*/ });

        var graphOptions = {                
            xAxis: {
                categories: ['<span class="hc-cat-title">' + stats.CountryName + '</span>'/* + '<br/>' + current_count*/]
            },
            yAxis: {
                plotBands: [
                    {
                        from: 0,
                        to: t1_count,
                        color: weak,
                        color_: {
                            linearGradient: { x1: 0, x2: 1, y1: 0, y2: 0 },
                            stops: [
                                [0, '#ea4848'],
                                [1, '#eadc48']
                            ]
                        },
                        borderWidth: 0,
                        borderColor: "#666",
                        label: {
                            text: Roomster.Affiliate.FormatCurrency(stats.PayoutRate),
                            y: -3
                        }
                    }, {
                        from: t1_count,
                        to: t2_count,
                        color:fair,
                        color_: {
                            linearGradient: { x1: 0, x2: 1, y1: 1, y2: 1 },
                            stops: [
                                [0, '#eadc48'],
                                [1, '#99ea48']
                            ]
                        },
                        borderWidth: 0,
                        borderColor: "#666",
                        label: {
                            text: Roomster.Affiliate.FormatCurrency(t1_payout),
                            y: -3
                        }
                    }, {
                        from: t2_count,
                        to: 9e9,
                        color: strong,
                        color_: {
                            linearGradient: { x1: 0, x2: 1, y1: 1, y2: 1 },
                            stops: [
                                [0, '#99ea48'],
                                [1, '#48ea75']
                            ]
                        },
                        borderWidth: 0,
                        borderColor: "#666",
                        label: {
                            text: Roomster.Affiliate.FormatCurrency(t2_payout),
                            y: -3
                        }
                    }
                ],
                title: null
            },
            series: [
                {
                    data: [
                        {
                            y: current_count,
                            target: target
                        }
                    ]
                }
            ],
            tooltip: {
                pointFormat: '{point.y}'
            }
        };

        graph.highcharts(Highcharts.merge(bulletOptions, graphOptions));
        

        $(selector).append(graph);
    },
    InsentiveMeter: function(selector, useNew)
    {
        $.get("/api/affiliate/insentiverates", function(data) {
             
            //console.log(data); 

            var enabled = data.Enabled;
            //enabled = true;

            var users = data.Users;
            //users = 800;


            if (enabled === false)
            {
                $(selector).css("opacity", "0.7");
                $('#insentive-meter-disabled').show();
            }
            
            //colors
            var colors = [
                "#FF851B", // orange
                "#FFDC00", // yellow
                "#2ECC40", // green
            ]; 

            //min, max
            var min = data.Rates[0].Min;
            var max = data.Rates[data.Rates.length-1].Max;
            
            var categories = [];
            var tickPositions = [];  //tick positions for categories (midpoint for center alignment of text)
            var tickPositions2 = [];  //tick positions for grid)
            var plotBands = [];

            var activeIndex = 0;
            for (var i = 0; i < data.Rates.length; i++)
            {
                if (data.Rates[i].PayoutType === "PerRegistration")
                {
                    if (useNew)
                    {
                        categories[i] = (Number(data.Rates[i].Rate) * 100).toFixed(0) + "% <br/> per lead";
                    } else
                    {
                        categories[i] = "$" + Number(data.Rates[i].Rate).toFixed(2) + "<br/> per lead";
                    }
                    
                    
                } else
                {
                    if (useNew)
                    {
                        categories[i] = (Number(data.Rates[i].Rate) * 100).toFixed(0) + "% <br/> per payment";
                    } else
                    {
                        categories[i] = (Number(data.Rates[i].Rate) * 100).toFixed(2) + "% <br/> per payment";    
                    }
                    
                    
                }



                tickPositions[i] = (data.Rates[i].Max - data.Rates[i].Min) * 0.5 + data.Rates[i].Min;
                tickPositions2[i] = data.Rates[i].Min;

                plotBands[i] = {
                    from: data.Rates[i].Min,
                    to: data.Rates[i].Max,
                    outerRadius: "100%",
                    innerRadius: "40%",
                    color: colors[i]
                };

                if (users > data.Rates[i].Min)
                {
                    activeIndex = i;
                }
            }

            tickPositions2.push(max);

            if (enabled === true && !useNew)
            {
                $('.current-payout').html(Roomster.Affiliate.FormatRate(data.Rates[activeIndex].PayoutType, data.Rates[activeIndex].Rate));
            }

            //console.log(tickPositions);                        

            var gaugeOptions = {
                chart: { type: "gauge" },
                title: null,
                pane: {
                    center: ["50%", "38%"],
                    size: "68%",
                    startAngle: -90,
                    endAngle: 90,
                    background: null
                },
                tooltip: {
                    enabled: false
                },                                
            };
            // The gauge
            $(selector).highcharts(Highcharts.merge(gaugeOptions, {
                yAxis: [
                    {
                        min: min,
                        max: max,
                        tickLength: 0,
                        categories: categories,
                        tickPositions: tickPositions,
                        labels: {
                            rotation: "none",
                            style: {
                                color: "#000",
                            },
                            formatter: function()
                            {
                                //console.log(this.value);
                                //console.log(this.axis.tickPositions);
                                for (var j = 0; j < this.axis.tickPositions.length; j++)
                                {
                                    if (this.axis.tickPositions[j] === this.value)
                                    {
                                        return this.axis.categories[j];
                                    }
                                }
                            }
                        },
                        plotBands: plotBands,
                    },
                    {
                        min: min,
                        max: max,
                        tickPositions:tickPositions2,
                        tickLength: 0,
                        minorTickLength: 0,
                        lineWidth: 0,
                        minorGridLineWidth: 0,
                        lineColor: "transparent",
                        labels: {
                            distance: 10,
                            rotation: "auto",
                            formatter: function()
                            {
                                return Highcharts.numberFormat(this.value, 0, '.', ',');
                            }
                        },
                        showFirstLabel: false,
                        showLastLabel: false
                    },
                ],
                credits: {
                    enabled: false
                },
                series: [
                {
                    name: "InsentiveMeter",
                    data: [users],
                    tooltip: {
                        valueSuffix: ""
                    },
                    dataLabels: {
                        backgroundColor: "#fff",
                        enabled: true,
                        formatter: function()
                        {
                            return Highcharts.numberFormat(this.y, 0, '.', ',');         
                        }
                    }
                 }
                ]
            }));
        });
    },
    FormatRate: function(payoutType, rate)
    {
        if (payoutType === "PerRegistration"){
            return "$" + Number(rate).toFixed(2) + " per lead";
        } else {
            return (Number(rate) * 100).toFixed(2) + "% per payment";
        }
    },
    RateToPercent: function(rate)
    {
        return (Number(rate) * 100).toFixed(0) + "%";
    },
    FormatCurrency: function(number)
        {
        return (Math.floor(Number(number) * 100) / 100) /*Number(number)*/
                .toLocaleString(undefined,
                {
                    style: 'currency',
                    currency: 'USD',
                    currencyDisplay: 'symbol',
                    useGrouping: true
                });
        },
    LoadCampaigns: function(selector)
    {
        $.get("/api/affiliate/campaigns", function(data) {            
            if (data.Campaigns)
            {
                for (var i = 0; i < data.Campaigns.length; i++)
                {
                    var id = data.Campaigns[i].TrackingId;
                    //var url = data.Campaigns[i].TrackingUrl;
                    var url = 'https://bnc.lt/diNm/gXarFVq2IB?referral=' + id;
                    var name = data.Campaigns[i].Name || "[no name]";


                    $(selector)
                         .append($("<option></option>")
                         .attr("value",url)
                         .text(name + " (" + id + ")"));            
                }
            }

            $(selector).selectric({ expandToItemText: true }).show();
            $('#promo_types').selectric({ expandToItemText: true }).show();
            

        });
    },
    LoadPromoAssets: function()
    {
        $.get("/api/affiliate/promoassets", function(data) {            
            this.PromoAssets = data;

            console.log(this.PromoAssets['Banners']);

        });
    },

});
$(function () { Roomster.Affiliate.Init(); });

/*EOF /Scripts/Roomster/Roomster.Affiliate.js*/
