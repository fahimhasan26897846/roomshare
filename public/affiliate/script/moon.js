/*!
** Moon.JsRouting - ASP.NET MVC routing and UrlHelper for JavaScript.
** Copyright © 2012+ Dusan Janosik. MIT license.
*/
var Moon;
(function (Moon) {
    (function (JsRouting) {
        var Utils = (function () {
            function Utils() { }
            Utils.Extend = function Extend(target, source) {
                return jQuery.extend(target, source);
            };
            Utils.Trim = function Trim(text) {
                return jQuery.trim(text);
            };
            Utils.RemoveLeadingSlash = function RemoveLeadingSlash(url) {
                url = Utils.Normalize(url);
                if(url.substr(0, 1) === "/") {
                    url = url.substr(1);
                }
                return url;
            };
            Utils.PrependLeadingSlash = function PrependLeadingSlash(url) {
                url = Utils.Normalize(url);
                if(url.substr(0, 1) !== "/") {
                    url = "/" + url;
                }
                return url;
            };
            Utils.RemoveTrailingSlash = function RemoveTrailingSlash(url) {
                url = Utils.Normalize(url);
                if(url.substring(url.length - 1, url.length) === "/") {
                    url = url.substring(0, url.length - 1);
                }
                return url;
            };
            Utils.AppendTrailingSlash = function AppendTrailingSlash(url) {
                url = Utils.Normalize(url);
                if(url.substring(url.length - 1, url.length) !== "/") {
                    url = url + "/";
                }
                return url;
            };
            Utils.Normalize = function Normalize(url) {
                url = Utils.ReplaceBackSlashes(url);
                url = Utils.RemoveRedundantSlashes(url);
                return Utils.Trim(url);
            };
            Utils.ReplaceBackSlashes = function ReplaceBackSlashes(url) {
                return url.replace("\\", "/");
            };
            Utils.RemoveRedundantSlashes = function RemoveRedundantSlashes(url) {
                while(url.indexOf("//") >= 0) {
                    url = url.replace("//", "/");
                }
                return url;
            };
            Utils.IsNotEmpty = function IsNotEmpty(routeValue) {
                return routeValue && routeValue.length > 0;
            };
            Utils.Equals = function Equals(value1, value2, caseInsensitive) {
                if (typeof caseInsensitive === "undefined") { caseInsensitive = false; }
                if(value1 == undefined || value2 == undefined) {
                    return false;
                }
                if(caseInsensitive) {
                    return value1.toLowerCase() === value2.toLowerCase();
                }
                return value1 === value2;
            };
            return Utils;
        })();
        JsRouting.Utils = Utils;
    })(Moon.JsRouting || (Moon.JsRouting = {}));
    var JsRouting = Moon.JsRouting;
})(Moon || (Moon = {}));

var Moon;
(function (Moon) {
    (function (JsRouting) {
        var RouteParameter = (function () {
            function RouteParameter(parameterName) {
                this.isCatchAll = false;
                this.name = parameterName;
                var firstChar = parameterName.substr(0, 1);
                if(firstChar === "*") {
                    this.name = parameterName.substr(1);
                    this.isCatchAll = true;
                }
            }
            RouteParameter.prototype.groupName = function () {
                var result = "{";
                if(this.isCatchAll) {
                    result += "*";
                }
                result += this.name + "}";
                return result;
            };
            return RouteParameter;
        })();
        JsRouting.RouteParameter = RouteParameter;
    })(Moon.JsRouting || (Moon.JsRouting = {}));
    var JsRouting = Moon.JsRouting;
})(Moon || (Moon = {}));

var Moon;
(function (Moon) {
    (function (JsRouting) {
        var BoundUrl = (function () {
            function BoundUrl(url, values) {
                this.url = url;
                this.values = values;
            }
            return BoundUrl;
        })();
        JsRouting.BoundUrl = BoundUrl;
    })(Moon.JsRouting || (Moon.JsRouting = {}));
    var JsRouting = Moon.JsRouting;
})(Moon || (Moon = {}));

var Moon;
(function (Moon) {
    (function (JsRouting) {
        var ParsedRoute = (function () {
            function ParsedRoute(url, defaultValues, constraints) {
                this.url = url;
                this.defaultValues = defaultValues;
                this.constraints = constraints;
                this.beginAndEndTag = new RegExp("([\\{|\\}])", "g");
                this.groupParameters = new RegExp("(?:{(\\*?[a-zA-Z]+)})", "g");
                this.parameters = this.getParameters(url);
            }
            ParsedRoute.prototype.bind = function (currentValues, newValues) {
                var acceptedValues = {
                };
                var unusedNewValues = JsRouting.Utils.Extend({
                }, newValues);
                for(var parameterName in this.parameters) {
                    var parameter = this.parameters[parameterName];
                    var hasNewValue = parameterName in newValues;
                    var newValue = this.getValue(newValues, parameterName);
                    if(hasNewValue) {
                        delete unusedNewValues[parameterName];
                    }
                    var hasCurrentValue = parameterName in currentValues;
                    var currentValue = this.getValue(currentValues, parameterName);
                    if(hasNewValue && hasCurrentValue) {
                        if(!JsRouting.Utils.Equals(newValue, currentValue, true)) {
                            break;
                        }
                    }
                    if(hasNewValue) {
                        if(JsRouting.Utils.IsNotEmpty(newValue)) {
                            acceptedValues[parameterName] = newValue;
                        }
                    } else if(hasCurrentValue) {
                        acceptedValues[parameterName] = currentValue;
                    }
                }
                //debugger;
                this.addRemainingNewValues(acceptedValues, newValues);
                this.addCurrentValuesThatAreNotInTheUrl(acceptedValues, currentValues);
                this.addDefaultValuesForOptionalParameters(acceptedValues);
                if(!this.allRequiredParametersHasValue(acceptedValues)) {
                    return null;
                }
                if(this.areThereOtherNotMatchingDefaultValues(newValues, unusedNewValues)) {
                    return null;
                }
                var boundUrl = this.formatUrl(acceptedValues, unusedNewValues);
                return new JsRouting.BoundUrl(boundUrl, acceptedValues);
            };
            ParsedRoute.prototype.getParameters = function (url) {
                var parameters = {
                };
                var matches = this.url.match(this.groupParameters) || new Array();
                for(var i = 0; i < matches.length; i++) {
                    var name = matches[i].replace(this.beginAndEndTag, '');
                    var parameter = new JsRouting.RouteParameter(name);
                    parameters[parameter.name] = parameter;
                }
                return parameters;
            };
            ParsedRoute.prototype.addRemainingNewValues = function (acceptedValues, newValues) {
                for(var key in newValues) {
                    if(key in acceptedValues) {
                        continue;
                    }
                    var newValue = this.getValue(newValues, key);
                    if(JsRouting.Utils.IsNotEmpty(newValue)) {
                        acceptedValues[key] = newValue;
                    }
                }
            };
            ParsedRoute.prototype.addCurrentValuesThatAreNotInTheUrl = function (acceptedValues, currentValues) {
                for(var key in currentValues) {
                    if(key in acceptedValues || key in this.parameters) {
                        continue;
                    }
                    acceptedValues[key] = this.getValue(currentValues, key);
                }
            };
            ParsedRoute.prototype.addDefaultValuesForOptionalParameters = function (acceptedValues) {
                for(var parameterName in this.parameters) {
                    if(parameterName in acceptedValues) {
                        continue;
                    }
                    if(!this.isParameterRequired(this.parameters[parameterName])) {
                        acceptedValues[parameterName] = this.getDefaultValue(parameterName);
                    }
                }
            };
            ParsedRoute.prototype.allRequiredParametersHasValue = function (acceptedValues) {

                for(var parameterName in this.parameters) {
                    if(this.isParameterRequired(this.parameters[parameterName]) && !(parameterName in acceptedValues)) {
                        return false;
                    }
                }
                return true;
            };
            ParsedRoute.prototype.areThereOtherNotMatchingDefaultValues = function (newValues, unusedNewValues) {
                var otherDefaultValues = JsRouting.Utils.Extend({
                }, this.defaultValues);
                for(var parameterName in this.parameters) {
                    delete otherDefaultValues[parameterName];
                }
                for(var key in otherDefaultValues) {
                    var newValue = this.getValue(newValues, key);
                    if(key in newValues) {
                        delete unusedNewValues[key];
                        var otherValue = this.getValue(otherDefaultValues, key);
                        if(!JsRouting.Utils.Equals(newValue, otherValue, true)) {
                            return true;
                        }
                    }
                }
                return false;
            };
            ParsedRoute.prototype.formatUrl = function (acceptedValues, unusedNewValues) {
                var formattedUrl = this.url;
                formattedUrl = JsRouting.Utils.RemoveTrailingSlash(formattedUrl);
                formattedUrl = this.replaceParameters(formattedUrl, acceptedValues, unusedNewValues);
                formattedUrl = JsRouting.Utils.PrependLeadingSlash(formattedUrl);
                formattedUrl = JsRouting.Utils.AppendTrailingSlash(formattedUrl);
                if(this.constraints != null) {
                    for(var key in this.constraints) {
                        delete unusedNewValues[key];
                    }
                }
                formattedUrl = formattedUrl.toLowerCase();
                formattedUrl = this.appendQueryString(formattedUrl, acceptedValues, unusedNewValues);
                return formattedUrl;
            };
            ParsedRoute.prototype.replaceParameters = function (url, acceptedValues, unusedNewValues) {
                var reversedNames = new Array();
                for(var parameterName in this.parameters) {
                    reversedNames.unshift(parameterName);
                }
                for(var i = 0; i < reversedNames.length; i++) {
                    var parameterName = reversedNames[i];
                    var parameterGroup = this.parameters[parameterName].groupName();
                    var acceptedParameterValue = acceptedValues[parameterName];
                    var hasAcceptedParameterValue = parameterName in acceptedValues;
                    if(hasAcceptedParameterValue) {
                        delete unusedNewValues[parameterName];
                    }

                    var defaultParameterValue = this.defaultValues[parameterName];
                    if ((new RegExp(parameterGroup + "$").test(url) && JsRouting.Utils.Equals(acceptedParameterValue, defaultParameterValue, true) )|| acceptedParameterValue == "param.optional") {
                        url = url.replace(parameterGroup, "");
                        url = url.substring(0, url.length - 1);
                    } else {
                        url = url.replace(parameterGroup, encodeURI(acceptedParameterValue));
                    }
                }
                return url;
            };
            ParsedRoute.prototype.appendQueryString = function (url, acceptedValues, unusedNewValues) {
                var separator = "?";
                if(url.indexOf(separator) >= 0) {
                    separator = "&";
                }
                for(var key in unusedNewValues) {
                    if(key in acceptedValues) {
                        url = url + separator + encodeURIComponent(key) + "=" + encodeURIComponent(acceptedValues[key]);
                        separator = "&";
                    }
                }
                return url;
            };
            ParsedRoute.prototype.isParameterRequired = function (parameter) {
                if(!parameter.isCatchAll) {
                    return !(parameter.name in this.defaultValues);
                }
                this.defaultValues[parameter.name] = "";
                return false;
            };
            ParsedRoute.prototype.getDefaultValue = function (parameterName) {
                return this.getValue(this.defaultValues, parameterName);
            };
            ParsedRoute.prototype.getValue = function (array, key) {
                if(key in array) {
                    var value = array[key];
                    if(value != undefined) {
                        return array[key].toString();
                    }
                }
                return undefined;
            };
            return ParsedRoute;
        })();
        JsRouting.ParsedRoute = ParsedRoute;
    })(Moon.JsRouting || (Moon.JsRouting = {}));
    var JsRouting = Moon.JsRouting;
})(Moon || (Moon = {}));

var Moon;
(function (Moon) {
    (function (JsRouting) {
        var RequestContext = (function () {
            function RequestContext() {
                this.applicationRoot = "/";
                this.routeValues = {
                };
                this.dataTokens = {
                };
            }
            return RequestContext;
        })();
        JsRouting.RequestContext = RequestContext;
    })(Moon.JsRouting || (Moon.JsRouting = {}));
    var JsRouting = Moon.JsRouting;
})(Moon || (Moon = {}));

var Moon;
(function (Moon) {
    (function (JsRouting) {
        var Route = (function () {
            function Route(url, area, defaultValues, constraints) {
                if (typeof defaultValues === "undefined") { defaultValues = {
                }; }
                if (typeof constraints === "undefined") { constraints = null; }
                this.url = url;
                this.area = area;
                this.defaultValues = defaultValues;
                this.constraints = constraints;
                this.parsedRoute = new JsRouting.ParsedRoute(url, defaultValues, constraints);
            }
            Route.prototype.getUrl = function (requestContext, routeValues)
            {
                var boundUrl = this.parsedRoute.bind(requestContext.routeValues, routeValues);
                if(!boundUrl) {
                    return null;
                }
                if(!this.processConstraints(boundUrl.values)) {
                    return null;
                }
                var result = boundUrl.url;
                result = requestContext.applicationRoot + result;
                return JsRouting.Utils.Normalize(result);
            };
            Route.prototype.processConstraints = function (values) {
                if(this.constraints != null) {
                    for(var parameterName in this.constraints) {
                        if(!this.processConstraint(parameterName, this.constraints[parameterName], values)) {
                            return false;
                        }
                    }
                }
                return true;
            };
            Route.prototype.processConstraint = function (parameterName, constraint, values) {
                var value = values[parameterName];
                var constraintRegex = new RegExp(constraint);
                return constraintRegex.test(value);
            };
            return Route;
        })();
        JsRouting.Route = Route;
    })(Moon.JsRouting || (Moon.JsRouting = {}));
    var JsRouting = Moon.JsRouting;
})(Moon || (Moon = {}));

var Moon;
(function (Moon) {
    (function (JsRouting) {
        var RouteTable = (function () {
            function RouteTable() {
                this.routes = {
                };
            }
            RouteTable.prototype.add = function (name, route) {
                if(this.routes[name]) {
                    throw "A route with the specified name has already been added to route table.";
                }
                this.routes[name] = route;
            };
            RouteTable.prototype.mapRoute = function (name, url, area, defaults, constraints) {
                var route = new JsRouting.Route(url, area, defaults, constraints);
                this.add(name, route);
                return route;
            };
            RouteTable.prototype.mapHttpRoute = function (name, url, defaults, constraints) {
                defaults = defaults || {
                };
                defaults["httproute"] = true;
                name = "Http_" + name;
                var route = new JsRouting.Route(url, null, defaults, constraints);
                this.add(name, route);
                return route;
            };
            RouteTable.prototype.clear = function () {
                this.routes = {
                };
            };
            RouteTable.prototype.getUrl = function (requestContext, routeName, routeValues) {
                if (typeof routeName === "undefined") { routeName = null; }
                if (typeof routeValues === "undefined") { routeValues = {
                };
                }
                if (JsRouting.Utils.IsNotEmpty(routeName))
                {
                    for (var route in this.routes)
                        routeName = route.toLowerCase() == routeName.toLowerCase()? route : routeName;
                    if(routeName in this.routes) {
                        return this.routes[routeName].getUrl(requestContext, routeValues);
                    }
                    return null;
                }
                var filteredRoutes;
                var targetArea = this.getTargetArea(requestContext, routeValues);
                if(targetArea) {
                    filteredRoutes = this.getRoutesInArea(targetArea);
                } else {
                    filteredRoutes = this.getRoutesNotInArea(targetArea);
                }
                delete routeValues["area"];
                for(var currentName in filteredRoutes) {
                    var route = filteredRoutes[currentName];
                    var url = route.getUrl(requestContext, routeValues);
                    if(JsRouting.Utils.IsNotEmpty(url)) {
                        return url;
                    }
                }
                return null;
            };
            RouteTable.prototype.getTargetArea = function (requestContext, routeValues) {
                var targetArea;
                if("area" in routeValues) {
                    targetArea = routeValues["area"];
                } else if("area" in requestContext.routeValues) {
                    targetArea = requestContext.routeValues["area"];
                } else if("area" in requestContext.dataTokens) {
                    targetArea = requestContext.dataTokens["area"];
                }
                return targetArea;
            };
            RouteTable.prototype.getRoutesInArea = function (area) {
                var filteredRoutes = {
                };
                for(var name in this.routes) {
                    var route = this.routes[name];
                    if(JsRouting.Utils.Equals(route.area, area, true)) {
                        filteredRoutes[name] = route;
                    }
                }
                return filteredRoutes;
            };
            RouteTable.prototype.getRoutesNotInArea = function (area) {
                var filteredRoutes = {
                };
                for(var name in this.routes) {
                    var route = this.routes[name];
                    if(!route.area) {
                        filteredRoutes[name] = route;
                    }
                }
                return filteredRoutes;
            };
            return RouteTable;
        })();
        JsRouting.RouteTable = RouteTable;
    })(Moon.JsRouting || (Moon.JsRouting = {}));
    var JsRouting = Moon.JsRouting;
})(Moon || (Moon = {}));

var Moon;
(function (Moon) {
    (function (JsRouting) {
        var UrlHelper = (function () {
            function UrlHelper(requestContext, routeTable) {
                this.requestContext = requestContext;
                this.routeTable = routeTable;
            }

            UrlHelper.prototype.optional = "param.optional";
            UrlHelper.prototype.action = function () {
                var routeValues = this.getActionRouteValues(arguments);
                return this.routeTable.getUrl(this.requestContext, null, routeValues);
            };
            UrlHelper.prototype.home = function ()
            {
                return this.routeUrl((/app\/[\w\d]{1,}/gi.test(window.location.href) ? "app_" : "") + "Home");
            };
            UrlHelper.prototype.routeUrl = function ()
            {
                var routeName;
                var routeValues = {
                };
                if(arguments.length == 1) {
                    var firstValue = arguments[0];
                    if(typeof firstValue === "string") {
                        routeName = firstValue;
                    } else {
                        JsRouting.Utils.Extend(routeValues, firstValue);
                    }
                } else if(arguments.length == 2) {
                    routeName = arguments[0];
                    JsRouting.Utils.Extend(routeValues, arguments[1]);
                }
                return this.routeTable.getUrl(this.requestContext, routeName, routeValues);
            };
            UrlHelper.prototype.httpRouteUrl = function () {
                var routeName;
                var routeValues = {
                };
                if(arguments.length == 1) {
                    var firstValue = arguments[0];
                    if(typeof firstValue === "string") {
                        routeName = firstValue;
                    } else {
                        JsRouting.Utils.Extend(routeValues, firstValue);
                    }
                } else if(arguments.length == 2) {
                    routeName = arguments[0];
                    JsRouting.Utils.Extend(routeValues, arguments[1]);
                }
                if(routeName) {
                    routeName = "Http_" + routeName;
                }
                return this.routeTable.getUrl(this.requestContext, routeName, routeValues);
            };
            UrlHelper.prototype.getActionRouteValues = function (args) {
                var routeValues = {
                };
                if("controller" in this.requestContext.routeValues) {
                    routeValues["controller"] = this.requestContext.routeValues["controller"];
                }
                if("action" in this.requestContext.routeValues) {
                    routeValues["action"] = this.requestContext.routeValues["action"];
                }
                if(args.length == 1) {
                    var firstValue = args[0];
                    if(typeof firstValue === "string") {
                        routeValues["action"] = firstValue;
                    } else {
                        JsRouting.Utils.Extend(routeValues, firstValue);
                    }
                } else if(args.length == 2) {
                    routeValues["action"] = args[0];
                    var secondValue = args[1];
                    if(typeof secondValue === "string") {
                        routeValues["controller"] = secondValue;
                    } else {
                        JsRouting.Utils.Extend(routeValues, secondValue);
                    }
                } else if(args.length == 3) {
                    routeValues["action"] = args[0];
                    routeValues["controller"] = args[1];
                    JsRouting.Utils.Extend(routeValues, args[2]);
                }
                return routeValues;
            };
            return UrlHelper;
        })();
        JsRouting.UrlHelper = UrlHelper;
    })(Moon.JsRouting || (Moon.JsRouting = {}));
    var JsRouting = Moon.JsRouting;
})(Moon || (Moon = {}));

(function ($, undefined) {
    var requestContext = new Moon.JsRouting.RequestContext();
    var routeTable = new Moon.JsRouting.RouteTable();
    $.url = new Moon.JsRouting.UrlHelper(requestContext, routeTable);
})(jQuery);