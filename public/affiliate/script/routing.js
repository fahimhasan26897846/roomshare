(function($) {
        var routeTable=$.url.routeTable;
        routeTable.mapRoute("app_ListingProfile", "{app}/{partnerid}/listings/{id}/{headline}", undefined, {
                controller: "Listing", action: "Profile", app: "app"
            }
            , {
                id: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("ListingProfile", "listings/{id}/{headline}", undefined, {
                controller: "Listing", action: "Profile"
            }
            , {
                id: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_SearchDataNoText", "{app}/{partnerid}/AsyncSearch/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}", undefined, {
                controller: "Search", action: "Search", pagenumber: "1", pagesize: "15", app: "app"
            }
            , {
                ServiceType: "^[0-9]+$", pagesize: "^[0-9]+$", pagenumber: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("SearchDataNoText", "AsyncSearch/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}", undefined, {
                controller: "Search", action: "Search", pagenumber: "1", pagesize: "15"
            }
            , {
                ServiceType: "^[0-9]+$", pagesize: "^[0-9]+$", pagenumber: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_SearchDataWithText", "{app}/{partnerid}/AsyncSearch/{text}/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}", undefined, {
                controller: "Search", action: "Search", pagenumber: "1", pagesize: "15", app: "app"
            }
            , {
                ServiceType: "^[0-9]+$", pagesize: "^[0-9]+$", pagenumber: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("SearchDataWithText", "AsyncSearch/{text}/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}", undefined, {
                controller: "Search", action: "Search", pagenumber: "1", pagesize: "15"
            }
            , {
                ServiceType: "^[0-9]+$", pagesize: "^[0-9]+$", pagenumber: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_SearchMapNoText", "{app}/{partnerid}/SearchMap/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}", undefined, {
                controller: "Search", action: "SearchMap", pagenumber: "1", pagesize: "15", app: "app"
            }
            , {
                ServiceType: "^[0-9]+$", pagesize: "^[0-9]+$", pagenumber: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("SearchMapNoText", "SearchMap/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}", undefined, {
                controller: "Search", action: "SearchMap", pagenumber: "1", pagesize: "15"
            }
            , {
                ServiceType: "^[0-9]+$", pagesize: "^[0-9]+$", pagenumber: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_SearchMapWithText", "{app}/{partnerid}/SearchMap/{text}/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}", undefined, {
                controller: "Search", action: "SearchMap", pagenumber: "1", pagesize: "15", app: "app"
            }
            , {
                ServiceType: "^[0-9]+$", pagesize: "^[0-9]+$", pagenumber: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("SearchMapWithText", "SearchMap/{text}/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}", undefined, {
                controller: "Search", action: "SearchMap", pagenumber: "1", pagesize: "15"
            }
            , {
                ServiceType: "^[0-9]+$", pagesize: "^[0-9]+$", pagenumber: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_SearchView", "{app}/{partnerid}/Search/{ServiceType}/{sorting}/{*Filters}", undefined, {
                controller: "Search", action: "View", app: "app"
            }
            , {
                ServiceType: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("SearchView", "Search/{ServiceType}/{sorting}/{*Filters}", undefined, {
                controller: "Search", action: "View"
            }
            , {
                ServiceType: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_SearchViewText", "{app}/{partnerid}/Search/{text}/{ServiceType}/{sorting}/{*Filters}", undefined, {
                controller: "Search", action: "View", app: "app"
            }
            , {
                ServiceType: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("SearchViewText", "Search/{text}/{ServiceType}/{sorting}/{*Filters}", undefined, {
                controller: "Search", action: "View"
            }
            , {
                ServiceType: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_GetNextListingProfileData", "{app}/{partnerid}/Listing/GetNextListingProfile/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}", undefined, {
                controller: "Listing", action: "GetNextListingProfile", pagenumber: "1", pagesize: "15", app: "app"
            }
            , {
                ServiceType: "^[0-9]+$", pagesize: "^[0-9]+$", pagenumber: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("GetNextListingProfileData", "Listing/GetNextListingProfile/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}", undefined, {
                controller: "Listing", action: "GetNextListingProfile", pagenumber: "1", pagesize: "15"
            }
            , {
                ServiceType: "^[0-9]+$", pagesize: "^[0-9]+$", pagenumber: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_GetNextListingProfileDataWithText", "{app}/{partnerid}/Listing/GetNextListingProfile/{text}/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}", undefined, {
                controller: "Listing", action: "GetNextListingProfile", pagenumber: "1", pagesize: "15", app: "app"
            }
            , {
                ServiceType: "^[0-9]+$", pagesize: "^[0-9]+$", pagenumber: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("GetNextListingProfileDataWithText", "Listing/GetNextListingProfile/{text}/{ServiceType}/{pagenumber}/{pagesize}/{sorting}/{*Filters}", undefined, {
                controller: "Listing", action: "GetNextListingProfile", pagenumber: "1", pagesize: "15"
            }
            , {
                ServiceType: "^[0-9]+$", pagesize: "^[0-9]+$", pagenumber: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_APIMailboxConversations", "{app}/{partnerid}/api/conversations/{label}/{pagesize}/{isnewdelta}", undefined, {
                controller: "Message", action: "GetConversations", label: "0", pagesize: "50", isnewdelta: "True", app: "app"
            }
            , {
                label: "^[0-9]+$", pagesize: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("APIMailboxConversations", "api/conversations/{label}/{pagesize}/{isnewdelta}", undefined, {
                controller: "Message", action: "GetConversations", label: "0", pagesize: "50", isnewdelta: "True"
            }
            , {
                label: "^[0-9]+$", pagesize: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_APIMailboxMessages", "{app}/{partnerid}/api/messages/{conversation}/{pagesize}/{isnewdelta}", undefined, {
                controller: "Message", action: "GetMessages", pagesize: "100", isnewdelta: "True", app: "app"
            }
            , {
                pagesize: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("APIMailboxMessages", "api/messages/{conversation}/{pagesize}/{isnewdelta}", undefined, {
                controller: "Message", action: "GetMessages", pagesize: "100", isnewdelta: "True"
            }
            , {
                pagesize: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_APIMailboxSendConversationMessage", "{app}/{partnerid}/api/message/send", undefined, {
                controller: "Message", action: "SendConversationMessage", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("APIMailboxSendConversationMessage", "api/message/send", undefined, {
                controller: "Message", action: "SendConversationMessage"
            }
            , {}
        );
        routeTable.mapRoute("app_APIMailboxConversationMarkRead", "{app}/{partnerid}/api/conversation/{conversation}/markread", undefined, {
                controller: "Message", action: "MarkRead", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("APIMailboxConversationMarkRead", "api/conversation/{conversation}/markread", undefined, {
                controller: "Message", action: "MarkRead"
            }
            , {}
        );
        routeTable.mapRoute("app_APIMailboxConversationMarkNew", "{app}/{partnerid}/api/conversation/{conversation}/marknew", undefined, {
                controller: "Message", action: "MarkNew", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("APIMailboxConversationMarkNew", "api/conversation/{conversation}/marknew", undefined, {
                controller: "Message", action: "MarkNew"
            }
            , {}
        );
        routeTable.mapRoute("app_APIMailboxConversationToArchive", "{app}/{partnerid}/api/conversation/{conversation}/toarchive", undefined, {
                controller: "Message", action: "ToArchive", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("APIMailboxConversationToArchive", "api/conversation/{conversation}/toarchive", undefined, {
                controller: "Message", action: "ToArchive"
            }
            , {}
        );
        routeTable.mapRoute("app_APIMailboxConversationToInbox", "{app}/{partnerid}/api/conversation/{conversation}/toinbox", undefined, {
                controller: "Message", action: "ToInbox", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("APIMailboxConversationToInbox", "api/conversation/{conversation}/toinbox", undefined, {
                controller: "Message", action: "ToInbox"
            }
            , {}
        );
        routeTable.mapRoute("app_APIMailboxConversationDelete", "{app}/{partnerid}/api/conversation/{conversation}/delete", undefined, {
                controller: "Message", action: "DeleteConversation", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("APIMailboxConversationDelete", "api/conversation/{conversation}/delete", undefined, {
                controller: "Message", action: "DeleteConversation"
            }
            , {}
        );
        routeTable.mapRoute("app_APIMailboxCounters", "{app}/{partnerid}/api/mailbox/counters/{userid}", undefined, {
                controller: "Message", action: "MailboxCounters", userid: "0", app: "app"
            }
            , {
                userid: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("APIMailboxCounters", "api/mailbox/counters/{userid}", undefined, {
                controller: "Message", action: "MailboxCounters", userid: "0"
            }
            , {
                userid: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_APIMailboxMigrate", "{app}/{partnerid}/api/mailbox/migrate", undefined, {
                controller: "Message", action: "MigrateMailbox", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("APIMailboxMigrate", "api/mailbox/migrate", undefined, {
                controller: "Message", action: "MigrateMailbox"
            }
            , {}
        );
        routeTable.mapRoute("app_Mailbox", "{app}/{partnerid}/messages/{label}/{conversation}", undefined, {
                controller: "Message", action: "Messages", label: "0", app: "app"
            }
            , {
                label: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("Mailbox", "messages/{label}/{conversation}", undefined, {
                controller: "Message", action: "Messages", label: "0"
            }
            , {
                label: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_APIUserBlock", "{app}/{partnerid}/api/user/{userid}/block", undefined, {
                controller: "UserProfile", action: "SetUserBlock", toblock: "True", app: "app"
            }
            , {
                userid: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("APIUserBlock", "api/user/{userid}/block", undefined, {
                controller: "UserProfile", action: "SetUserBlock", toblock: "True"
            }
            , {
                userid: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_APIUserUnBlock", "{app}/{partnerid}/api/user/{userid}/unblock", undefined, {
                controller: "UserProfile", action: "SetUserBlock", toblock: "False", app: "app"
            }
            , {
                userid: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("APIUserUnBlock", "api/user/{userid}/unblock", undefined, {
                controller: "UserProfile", action: "SetUserBlock", toblock: "False"
            }
            , {
                userid: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_PayPalApi", "{app}/{partnerid}/payments/paypal/{packageId}/{todo}/{token}", undefined, {
                controller: "Payments", action: "PayPal", app: "app"
            }
            , {
                packageId: "^[0-9]+$", app: "app"
            }
        );
        routeTable.mapRoute("PayPalApi", "payments/paypal/{packageId}/{todo}/{token}", undefined, {
                controller: "Payments", action: "PayPal"
            }
            , {
                packageId: "^[0-9]+$"
            }
        );
        routeTable.mapRoute("app_PayPalApiWebhook", "{app}/{partnerid}/payments/paypal/webhook", undefined, {
                controller: "Payments", action: "PayPalWebhook", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("PayPalApiWebhook", "payments/paypal/webhook", undefined, {
                controller: "Payments", action: "PayPalWebhook"
            }
            , {}
        );
        routeTable.mapRoute("app_UserRegistration", "{app}/{partnerid}/Registration", undefined, {
                controller: "Registration", action: "RegisterUser", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("UserRegistration", "Registration", undefined, {
                controller: "Registration", action: "RegisterUser"
            }
            , {}
        );
        routeTable.mapRoute("app_UserLogin", "{app}/{partnerid}/Login", undefined, {
                controller: "Account", action: "Login", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("UserLogin", "Login", undefined, {
                controller: "Account", action: "Login"
            }
            , {}
        );
        routeTable.mapRoute("app_UserLoginRedirect", "{app}/{partnerid}/Registration/Login", undefined, {
                controller: "Account", action: "Login", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("UserLoginRedirect", "Registration/Login", undefined, {
                controller: "Account", action: "Login"
            }
            , {}
        );
        routeTable.mapRoute("app_HaveShareRegistration", "{app}/{partnerid}/Registration/HaveShare/{action}", undefined, {
                controller: "RoomholderRegistration", ServiceType: "HaveShare", action: "PostListing", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("HaveShareRegistration", "Registration/HaveShare/{action}", undefined, {
                controller: "RoomholderRegistration", ServiceType: "HaveShare", action: "PostListing"
            }
            , {}
        );
        routeTable.mapRoute("app_NeedRoomRegistration", "{app}/{partnerid}/Registration/NeedRoom/{action}", undefined, {
                controller: "RoommateRegistration", ServiceType: "NeedRoom", action: "PostListing", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("NeedRoomRegistration", "Registration/NeedRoom/{action}", undefined, {
                controller: "RoommateRegistration", ServiceType: "NeedRoom", action: "PostListing"
            }
            , {}
        );
        routeTable.mapRoute("app_NeedApartmentRegistration", "{app}/{partnerid}/Registration/NeedApartment/{action}", undefined, {
                controller: "TenantRegistration", ServiceType: "NeedApartment", action: "PostListing", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("NeedApartmentRegistration", "Registration/NeedApartment/{action}", undefined, {
                controller: "TenantRegistration", ServiceType: "NeedApartment", action: "PostListing"
            }
            , {}
        );
        routeTable.mapRoute("app_HaveApartmentRegistration", "{app}/{partnerid}/Registration/HaveApartment/{action}", undefined, {
                controller: "LandLordRegistration", ServiceType: "HaveApartment", action: "FillContactInfo", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("HaveApartmentRegistration", "Registration/HaveApartment/{action}", undefined, {
                controller: "LandLordRegistration", ServiceType: "HaveApartment", action: "FillContactInfo"
            }
            , {}
        );
        routeTable.mapRoute("app_LandlordListingEdit", "{app}/{partnerid}/LandLordListing/{action}/{listingId}", undefined, {
                controller: "LandLordListing", action: "Edit", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("LandlordListingEdit", "LandLordListing/{action}/{listingId}", undefined, {
                controller: "LandLordListing", action: "Edit"
            }
            , {}
        );
        routeTable.mapRoute("app_TenantListingEdit", "{app}/{partnerid}/TenantListing/{action}/{listingId}", undefined, {
                controller: "TenantListing", action: "Edit", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("TenantListingEdit", "TenantListing/{action}/{listingId}", undefined, {
                controller: "TenantListing", action: "Edit"
            }
            , {}
        );
        routeTable.mapRoute("app_ManageServiceState", "{app}/{partnerid}/UserProfile/ManageServiceState/{serviceType}/{activate}", undefined, {
                controller: "UserProfile", action: "ManageServiceState", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("ManageServiceState", "UserProfile/ManageServiceState/{serviceType}/{activate}", undefined, {
                controller: "UserProfile", action: "ManageServiceState"
            }
            , {}
        );
        routeTable.mapRoute("app_ManageListingState", "{app}/{partnerid}/Listing/ManageListingState/{listingId}/{activate}", undefined, {
                controller: "Listing", action: "ManageListingState", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("ManageListingState", "Listing/ManageListingState/{listingId}/{activate}", undefined, {
                controller: "Listing", action: "ManageListingState"
            }
            , {}
        );
        routeTable.mapRoute("app_ListingEditMenu", "{app}/{partnerid}/Listing/EditAll/{serviceType}/{listingId}", undefined, {
                controller: "Listing", action: "EditAll", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("ListingEditMenu", "Listing/EditAll/{serviceType}/{listingId}", undefined, {
                controller: "Listing", action: "EditAll"
            }
            , {}
        );
        routeTable.mapRoute("app_MegaphoneDialog", "{app}/{partnerid}/Megaphone/GetDialog", undefined, {
                controller: "Megaphone", action: "GetDialog", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("MegaphoneDialog", "Megaphone/GetDialog", undefined, {
                controller: "Megaphone", action: "GetDialog"
            }
            , {}
        );
        routeTable.mapRoute("app_Megaphone", "{app}/{partnerid}/Megaphone/{Action}/{serviceType}/{*filters}", undefined, {
                controller: "Megaphone", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("Megaphone", "Megaphone/{Action}/{serviceType}/{*filters}", undefined, {
                controller: "Megaphone"
            }
            , {}
        );
        routeTable.mapRoute("app_affiliate", "{app}/{partnerid}/affiliate", undefined, {
                controller: "affiliate", action: "default", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("affiliate", "affiliate", undefined, {
                controller: "affiliate", action: "default"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateAutoLoginWithLog", "{app}/{partnerid}/affiliate/l/{encodedguid}/{addtolog}", undefined, {
                controller: "Affiliate", action: "AutoLogin", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateAutoLoginWithLog", "affiliate/l/{encodedguid}/{addtolog}", undefined, {
                controller: "Affiliate", action: "AutoLogin"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateAutoLogin", "{app}/{partnerid}/affiliate/l/{encodedguid}", undefined, {
                controller: "Affiliate", action: "AutoLogin", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateAutoLogin", "affiliate/l/{encodedguid}", undefined, {
                controller: "Affiliate", action: "AutoLogin"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateUnsubscribe", "{app}/{partnerid}/affiliate/unsubscribe/{encryptedEmail}/{encryptedNotificationType}", undefined, {
                controller: "Affiliate", action: "Unsubscribe", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateUnsubscribe", "affiliate/unsubscribe/{encryptedEmail}/{encryptedNotificationType}", undefined, {
                controller: "Affiliate", action: "Unsubscribe"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateCampaigns", "{app}/{partnerid}/affiliate/campaigns/{datefrom}/{dateto}", undefined, {
                controller: "Affiliate", action: "Campaigns", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateCampaigns", "affiliate/campaigns/{datefrom}/{dateto}", undefined, {
                controller: "Affiliate", action: "Campaigns"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateCampaign", "{app}/{partnerid}/affiliate/campaign/{campaignid}", undefined, {
                controller: "Affiliate", action: "Campaign", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateCampaign", "affiliate/campaign/{campaignid}", undefined, {
                controller: "Affiliate", action: "Campaign"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateApiSendHelpdeskIn", "{app}/{partnerid}/api/affiliate/{affiliateId}/sendhelpdeskin", undefined, {
                controller: "Affiliate", action: "SendHelpdeskIn", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateApiSendHelpdeskIn", "api/affiliate/{affiliateId}/sendhelpdeskin", undefined, {
                controller: "Affiliate", action: "SendHelpdeskIn"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateApiSendHelpdeskOut", "{app}/{partnerid}/api/affiliate/{affiliateId}/sendhelpdeskout", undefined, {
                controller: "Affiliate", action: "SendHelpdeskOut", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateApiSendHelpdeskOut", "api/affiliate/{affiliateId}/sendhelpdeskout", undefined, {
                controller: "Affiliate", action: "SendHelpdeskOut"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateApiSendWelcome", "{app}/{partnerid}/api/affiliate/{affiliateId}/sendwelcome", undefined, {
                controller: "Affiliate", action: "SendWelcome", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateApiSendWelcome", "api/affiliate/{affiliateId}/sendwelcome", undefined, {
                controller: "Affiliate", action: "SendWelcome"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateApiCampaignStats", "{app}/{partnerid}/api/affiliate/{affiliateId}/campaignstats/{datefrom}/{dateto}", undefined, {
                controller: "Affiliate", action: "CampaignStats", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateApiCampaignStats", "api/affiliate/{affiliateId}/campaignstats/{datefrom}/{dateto}", undefined, {
                controller: "Affiliate", action: "CampaignStats"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateApiQualityScore", "{app}/{partnerid}/api/affiliate/qualityscore/{affiliateid}/{datefrom}/{dateto}", undefined, {
                controller: "Affiliate", action: "QualityScore", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateApiQualityScore", "api/affiliate/qualityscore/{affiliateid}/{datefrom}/{dateto}", undefined, {
                controller: "Affiliate", action: "QualityScore"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateApiInsentiveRates", "{app}/{partnerid}/api/affiliate/insentiverates/{affiliateid}", undefined, {
                controller: "Affiliate", action: "InsentiveRates", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateApiInsentiveRates", "api/affiliate/insentiverates/{affiliateid}", undefined, {
                controller: "Affiliate", action: "InsentiveRates"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateApiIncentiveRateStats", "{app}/{partnerid}/api/affiliate/incentiveratestats/{affiliateid}", undefined, {
                controller: "Affiliate", action: "IncentiveRateStats", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateApiIncentiveRateStats", "api/affiliate/incentiveratestats/{affiliateid}", undefined, {
                controller: "Affiliate", action: "IncentiveRateStats"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateApiPromoAssets", "{app}/{partnerid}/api/affiliate/promoassets", undefined, {
                controller: "Affiliate", action: "PromoAssets", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateApiPromoAssets", "api/affiliate/promoassets", undefined, {
                controller: "Affiliate", action: "PromoAssets"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateApiCampaignsLoggedInAffiliate", "{app}/{partnerid}/api/affiliate/{affiliateId}/campaigns", undefined, {
                controller: "Affiliate", action: "GetCampaigns", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateApiCampaignsLoggedInAffiliate", "api/affiliate/{affiliateId}/campaigns", undefined, {
                controller: "Affiliate", action: "GetCampaigns"
            }
            , {}
        );
        routeTable.mapRoute("app_AffiliateApiCampaignsForSpecificAffiliateId", "{app}/{partnerid}/api/affiliate/campaigns", undefined, {
                controller: "Affiliate", action: "GetCampaigns", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("AffiliateApiCampaignsForSpecificAffiliateId", "api/affiliate/campaigns", undefined, {
                controller: "Affiliate", action: "GetCampaigns"
            }
            , {}
        );
        routeTable.mapRoute("app_paymentscreenshot", "{app}/{partnerid}/pagescreenshot/{payment}", undefined, {
                app: "app"
            }
            , {
                payment: "payment", app: "app"
            }
        );
        routeTable.mapRoute("paymentscreenshot", "pagescreenshot/{payment}", undefined, {}
            , {
                payment: "payment"
            }
        );
        routeTable.mapRoute("app_PartnerHelpdeskPage", "{app}/{partnerid}/{helpdesk}/", undefined, {
                app: "app"
            }
            , {
                helpdesk: "helpdesk", app: "app"
            }
        );
        routeTable.mapRoute("PartnerHelpdeskPage", "{helpdesk}/", undefined, {}
            , {
                helpdesk: "helpdesk"
            }
        );
        routeTable.mapRoute("app_PartnerInfoDefaultPage", "{app}/{partnerid}/{info}/", undefined, {
                app: "app"
            }
            , {
                info: "info", app: "app"
            }
        );
        routeTable.mapRoute("PartnerInfoDefaultPage", "{info}/", undefined, {}
            , {
                info: "info"
            }
        );
        routeTable.mapRoute("app_securepayment", "{app}/{partnerid}/{secure}/{pagename}.aspx", undefined, {
                app: "app"
            }
            , {
                secure: "secure", app: "app"
            }
        );
        routeTable.mapRoute("securepayment", "{secure}/{pagename}.aspx", undefined, {}
            , {
                secure: "secure"
            }
        );
        routeTable.mapRoute("app_paypal", "{app}/{partnerid}/{paypal}/{pagename}.aspx", undefined, {
                app: "app"
            }
            , {
                paypal: "paypal", app: "app"
            }
        );
        routeTable.mapRoute("paypal", "{paypal}/{pagename}.aspx", undefined, {}
            , {
                paypal: "paypal"
            }
        );
        routeTable.mapRoute("app_PartnerInfoCareersPage", "{app}/{partnerid}/{info}/careers/", undefined, {
                app: "app"
            }
            , {
                info: "info", app: "app"
            }
        );
        routeTable.mapRoute("PartnerInfoCareersPage", "{info}/careers/", undefined, {}
            , {
                info: "info"
            }
        );
        routeTable.mapRoute("app_PartnerInfoResourcesPage", "{app}/{partnerid}/{info}/{resources}/{pagename}.aspx", undefined, {
                app: "app"
            }
            , {
                info: "info", resources: "resources", app: "app"
            }
        );
        routeTable.mapRoute("PartnerInfoResourcesPage", "{info}/{resources}/{pagename}.aspx", undefined, {}
            , {
                info: "info", resources: "resources"
            }
        );
        routeTable.mapRoute("app_PartnerInfoPage", "{app}/{partnerid}/{info}/{pagename}.aspx", undefined, {
                app: "app"
            }
            , {
                info: "info", app: "app"
            }
        );
        routeTable.mapRoute("PartnerInfoPage", "{info}/{pagename}.aspx", undefined, {}
            , {
                info: "info"
            }
        );
        routeTable.mapRoute("app_PartnerInfoPressDefaultPage", "{app}/{partnerid}/{info}/{press}/", undefined, {
                app: "app"
            }
            , {
                info: "info", press: "press", app: "app"
            }
        );
        routeTable.mapRoute("PartnerInfoPressDefaultPage", "{info}/{press}/", undefined, {}
            , {
                info: "info", press: "press"
            }
        );
        routeTable.mapRoute("app_PartnerInfoPressPage", "{app}/{partnerid}/{info}/{press}/{pagename}.aspx", undefined, {
                app: "app"
            }
            , {
                info: "info", press: "press", app: "app"
            }
        );
        routeTable.mapRoute("PartnerInfoPressPage", "{info}/{press}/{pagename}.aspx", undefined, {}
            , {
                info: "info", press: "press"
            }
        );
        routeTable.mapRoute("app_PartnerInfoTipsDefaultPage", "{app}/{partnerid}/{info}/{tips}/", undefined, {
                app: "app"
            }
            , {
                info: "info", tips: "tips", app: "app"
            }
        );
        routeTable.mapRoute("PartnerInfoTipsDefaultPage", "{info}/{tips}/", undefined, {}
            , {
                info: "info", tips: "tips"
            }
        );
        routeTable.mapRoute("app_PartnerInfoTipsPage", "{app}/{partnerid}/{info}/{tips}/{pagename}.aspx", undefined, {
                app: "app"
            }
            , {
                info: "info", tips: "tips", app: "app"
            }
        );
        routeTable.mapRoute("PartnerInfoTipsPage", "{info}/{tips}/{pagename}.aspx", undefined, {}
            , {
                info: "info", tips: "tips"
            }
        );
        routeTable.mapRoute("app_PartnerMailDefaultPage", "{app}/{partnerid}/{mbox}/{pagename}.aspx/", undefined, {
                app: "app"
            }
            , {
                mbox: "mbox", app: "app"
            }
        );
        routeTable.mapRoute("PartnerMailDefaultPage", "{mbox}/{pagename}.aspx/", undefined, {}
            , {
                mbox: "mbox"
            }
        );
        routeTable.mapRoute("app_RootASPX", "{app}/{partnerid}/{pagename}.aspx", undefined, {
                app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("RootASPX", "{pagename}.aspx", undefined, {}
            , {}
        );
        routeTable.mapRoute("app_location", "{app}/{partnerid}/location/{location}", undefined, {
                controller: "Location", action: "Index", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("location", "location/{location}", undefined, {
                controller: "Location", action: "Index"
            }
            , {}
        );
        routeTable.mapRoute("app_socialconnect", "{app}/{partnerid}/socialconnect", undefined, {
                controller: "Home", action: "socialconnect", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("socialconnect", "socialconnect", undefined, {
                controller: "Home", action: "socialconnect"
            }
            , {}
        );
        routeTable.mapRoute("app_privacy", "{app}/{partnerid}/privacy", undefined, {
                controller: "Home", action: "privacy", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("privacy", "privacy", undefined, {
                controller: "Home", action: "privacy"
            }
            , {}
        );
        routeTable.mapRoute("app_terms", "{app}/{partnerid}/terms", undefined, {
                controller: "Home", action: "Terms", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("terms", "terms", undefined, {
                controller: "Home", action: "Terms"
            }
            , {}
        );
        routeTable.mapRoute("app_contact", "{app}/{partnerid}/contact", undefined, {
                controller: "Message", action: "Contact", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("contact", "contact", undefined, {
                controller: "Message", action: "Contact"
            }
            , {}
        );
        routeTable.mapRoute("app_resources", "{app}/{partnerid}/resources", undefined, {
                controller: "Home", action: "Resources", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("resources", "resources", undefined, {
                controller: "Home", action: "Resources"
            }
            , {}
        );
        routeTable.mapRoute("app_Home", "{app}/{partnerid}/", undefined, {
                controller: "Home", action: "default", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("Home", "", undefined, {
                controller: "Home", action: "default"
            }
            , {}
        );
        routeTable.mapRoute("app_Version", "{app}/{partnerid}/version/{id}", undefined, {
                controller: "Version", action: "Index", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("Version", "version/{id}", undefined, {
                controller: "Version", action: "Index"
            }
            , {}
        );
        routeTable.mapRoute("app_Tips-1", "{app}/{partnerid}/9-Tips-to-help-Find-a-Roommate", undefined, {
                controller: "Tips", action: "Index", viewName: "9-Tips-to-help-Find-a-Roommate", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("Tips-1", "9-Tips-to-help-Find-a-Roommate", undefined, {
                controller: "Tips", action: "Index", viewName: "9-Tips-to-help-Find-a-Roommate"
            }
            , {}
        );
        routeTable.mapRoute("app_Tips-2", "{app}/{partnerid}/6-Tips-to-Rent-A-Room-in-your-home", undefined, {
                controller: "Tips", action: "Index", viewName: "6-Tips-to-Rent-A-Room-in-your-home", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("Tips-2", "6-Tips-to-Rent-A-Room-in-your-home", undefined, {
                controller: "Tips", action: "Index", viewName: "6-Tips-to-Rent-A-Room-in-your-home"
            }
            , {}
        );
        routeTable.mapRoute("app_Tips-3", "{app}/{partnerid}/5-tips-when-you-need-a-roommate", undefined, {
                controller: "Tips", action: "Index", viewName: "5-tips-when-you-need-a-roommate", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("Tips-3", "5-tips-when-you-need-a-roommate", undefined, {
                controller: "Tips", action: "Index", viewName: "5-tips-when-you-need-a-roommate"
            }
            , {}
        );
        routeTable.mapRoute("app_Tips-4", "{app}/{partnerid}/room-share-tips", undefined, {
                controller: "Tips", action: "Index", viewName: "room-share-tips", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("Tips-4", "room-share-tips", undefined, {
                controller: "Tips", action: "Index", viewName: "room-share-tips"
            }
            , {}
        );
        routeTable.mapRoute("app_Tips-5", "{app}/{partnerid}/advantages-and-disadvantages-of-renting-a-furnished-room", undefined, {
                controller: "Tips", action: "Index", viewName: "advantages-and-disadvantages-of-renting-a-furnished-room", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("Tips-5", "advantages-and-disadvantages-of-renting-a-furnished-room", undefined, {
                controller: "Tips", action: "Index", viewName: "advantages-and-disadvantages-of-renting-a-furnished-room"
            }
            , {}
        );
        routeTable.mapRoute("app_Default", "{app}/{partnerid}/{controller}/{action}/{id}", undefined, {
                controller: "Home", action: "Default", app: "app"
            }
            , {
                app: "app"
            }
        );
        routeTable.mapRoute("Default", "{controller}/{action}/{id}", undefined, {
                controller: "Home", action: "Default"
            }
            , {}
        );
    }

)(jQuery);