<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'slug' => 'super_admin','name' =>'Super Admin','permissions'=>'{"admin.add":true,"admin.view":true,"admin.delete":true,"admin.edit":true,"user.add":true,"user.view":true,"user.delete":true,"user.edit":true,"mailbox.add":true,"mailbox.view":true,"mailbox.delete":true,"mailbox.edit":true,"newsletter.add":true,"newsletter.view":true,"newsletter.delete":true,"newsletter.edit":true,"contacts.add":true,"contacts.view":true,"contacts.delete":true,"contacts.edit":true,"faq.add":true,"faq.view":true,"faq.delete":true,"faq.edit":true,"push_notification.add":true,"push_notification.view":true,"push_notification.delete":true,"push_notification.edit":true,"notice.add":true,"notice.view":true,"notice.delete":true,"notice.edit":true,"support.add":true,"support.view":true,"support.delete":true,"support.edit":true}'],

            ['id' => 2, 'slug' => 'admin','name' =>'Admin','permissions'=>'{"admin.add":false,"admin.view":false,"admin.delete":false,"admin.edit":false,"user.add":true,"user.view":true,"user.delete":true,"user.edit":true,"mailbox.add":true,"mailbox.view":true,"mailbox.delete":true,"mailbox.edit":true,"newsletter.add":true,"newsletter.view":true,"newsletter.delete":true,"newsletter.edit":true,"contacts.add":true,"contacts.view":true,"contacts.delete":true,"contacts.edit":true,"faq.add":true,"faq.view":true,"faq.delete":true,"faq.edit":true,"push_notification.add":true,"push_notification.view":true,"push_notification.delete":true,"push_notification.edit":true,"notice.add":true,"notice.view":true,"notice.delete":true,"notice.edit":true,"support.add":true,"support.view":true,"support.delete":true,"support.edit":true}'],

            ['id' => 4, 'slug' => 'manager','name' =>'Manager','permissions'=>'{"admin.add":false,"admin.view":false,"admin.delete":false,"admin.edit":false,"user.add":false,"user.view":false,"user.delete":false,"user.edit":false,"mailbox.add":false,"mailbox.view":true,"mailbox.delete":true,"mailbox.edit":true,"newsletter.add":true,"newsletter.view":true,"newsletter.delete":true,"newsletter.edit":true,"contacts.add":true,"contacts.view":true,"contacts.delete":true,"contacts.edit":true,"faq.add":true,"faq.view":true,"faq.delete":true,"faq.edit":true,"push_notification.add":true,"push_notification.view":true,"push_notification.delete":true,"push_notification.edit":true,"notice.add":true,"notice.view":true,"notice.delete":true,"notice.edit":true,"support.add":true,"support.view":true,"support.delete":true,"support.edit":true}'],

            ['id' => 5, 'slug' => 'content_creator','name' =>'Content creator','permissions'=>'{"admin.add":false,"admin.view":false,"admin.delete":false,"admin.edit":false,"user.add":false,"user.view":false,"user.delete":false,"user.edit":false,"mailbox.add":false,"mailbox.view":false,"mailbox.delete":false,"mailbox.edit":false,"newsletter.add":false,"newsletter.view":false,"newsletter.delete":false,"newsletter.edit":false,"contacts.add":false,"contacts.view":false,"contacts.delete":false,"contacts.edit":false,"faq.add":false,"faq.view":false,"faq.delete":false,"faq.edit":false,"push_notification.add":false,"push_notification.view":false,"push_notification.delete":false,"push_notification.edit":false,"notice.add":false,"notice.view":false,"notice.delete":false,"notice.edit":false,"support.add":false,"support.view":false,"support.delete":false,"support.edit":false}'],

            ['id' => 6, 'slug' => 'support','name' =>'Support','permissions'=>'{"admin.add":false,"admin.view":false,"admin.delete":false,"admin.edit":false,"user.add":false,"user.view":false,"user.delete":false,"user.edit":false,"mailbox.add":false,"mailbox.view":false,"mailbox.delete":false,"mailbox.edit":false,"newsletter.add":false,"newsletter.view":false,"newsletter.delete":false,"newsletter.edit":false,"contacts.add":false,"contacts.view":false,"contacts.delete":false,"contacts.edit":false,"faq.add":false,"faq.view":false,"faq.delete":false,"faq.edit":false,"push_notification.add":false,"push_notification.view":false,"push_notification.delete":false,"push_notification.edit":false,"notice.add":false,"notice.view":false,"notice.delete":false,"notice.edit":false,"support.add":true,"support.view":true,"support.delete":true,"support.edit":true}'],

              ];
        foreach ($items as $item) {
            Role::create($item);
        }
    }
}
