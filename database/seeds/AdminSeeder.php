<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'email' => 'admin@mail.com','first_name' =>'Fahim','last_name' =>'Fahim','password'=>'$2y$10$9CxN3Qu3U/YeD7TPNO5tNOCeEw6/X.2N8BhsGetvqqjtR0GPhhysa'],
            ['id' => 2, 'email' => 'subadmin@mail.com','first_name' =>'Fahim','last_name'=>'hasan','password'=>'$2y$10$BkMJmr1w1P//FwSi2MC92esUZBtLCkRq.8W8ZZP.lt6Pwifzq2sym',],
            ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
