<?php

use Illuminate\Database\Seeder;
use App\Model\RoleUser;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['user_id' => '1','role_id' =>'1'],
            ['user_id' => '2','role_id' =>'2'],
        ];

        foreach ($items as $item) {
            RoleUser::create($item);
        }
    }
}
