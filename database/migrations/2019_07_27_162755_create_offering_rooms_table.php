<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferingRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offering_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('headline')->nullable();
            $table->string('location');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->integer('monthly_rent');
            $table->string('rental_currency');
            $table->boolean('is_short_term');
            $table->date('move_date');
            $table->date('leave_date')->nullable();
            $table->string('minimum_stay')->nullable();
            $table->text('description')->nullable();
            $table->string('building_type')->nullable();
            $table->integer('move_in_fee')->nullable();
            $table->integer('utilities_cost')->nullable();
            $table->integer('parking_rent')->nullable();
            $table->boolean('is_furnished')->nullable();
            $table->boolean('have_air_conditioning')->nullable();
            $table->boolean('have_deck_or_patio')->nullable();
            $table->boolean('have_wood_floors')->nullable();
            $table->boolean('have_yard')->nullable();
            $table->boolean('have_carpet')->nullable();
            $table->boolean('have_storage')->nullable();
            $table->boolean('have_gym')->nullable();
            $table->boolean('have_parking')->nullable();
            $table->boolean('have_elevator')->nullable();
            $table->boolean('have_fireplace')->nullable();
            $table->boolean('have_laundry')->nullable();
            $table->boolean('have_tennis')->nullable();
            $table->boolean('have_jacuzzi')->nullable();
            $table->boolean('have_dishwasher')->nullable();
            $table->boolean('have_pool')->nullable();
            $table->boolean('have_private_bathroom')->nullable();
            $table->boolean('have_phone_jack')->nullable();
            $table->boolean('have_private_entrance')->nullable();
            $table->boolean('have_cable_tv_jack')->nullable();
            $table->boolean('have_private_closet')->nullable();
            $table->boolean('have_internet')->nullable();
            $table->boolean('have_wireless_internet')->nullable();
            $table->string('min_household_age');
            $table->string('max_household_age');
            $table->integer('people_in_household')->nullable();
            $table->string('household_sex')->nullable();
            $table->string('cleanliness')->nullable();
            $table->string('overnight_guests')->nullable();
            $table->string('party_habits')->nullable();
            $table->string('get_up')->nullable();
            $table->string('go_to_bed')->nullable();
            $table->string('food_preference')->nullable();
            $table->string('smoker')->nullable();
            $table->string('work_schedule')->nullable();
            $table->string('occupation')->nullable();
            $table->boolean('have_cats')->nullable();
            $table->boolean('have_dogs')->nullable();
            $table->boolean('have_small_pets')->nullable();
            $table->boolean('have_birds')->nullable();
            $table->boolean('have_fish')->nullable();
            $table->boolean('have_reptiles')->nullable();
            $table->integer('prefer_min_age');
            $table->integer('prefer_max_age');
            $table->string('prefer_smoker')->nullable();
            $table->boolean('prefer_student')->nullable();
            $table->boolean('prefer_cats')->nullable();
            $table->boolean('prefer_dogs')->nullable();
            $table->boolean('prefer_small_pets')->nullable();
            $table->boolean('prefer_birds')->nullable();
            $table->boolean('prefer_fish')->nullable();
            $table->boolean('prefer_reptiles')->nullable();
            $table->text('photos')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('have_shares');
    }
}
