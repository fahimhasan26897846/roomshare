<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLookingForRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('looking_for_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('headline')->nullable();
            $table->string('location');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->integer('monthly_rent');
            $table->string('rental_currency');
            $table->boolean('is_short_term');
            $table->date('move_date');
            $table->date('leave_date')->nullable();
            $table->text('description')->nullable();
            $table->string('cleanliness')->nullable();
            $table->string('overnight_guests')->nullable();
            $table->string('party_habits')->nullable();
            $table->string('get_up')->nullable();
            $table->string('go_to_bed')->nullable();
            $table->string('food_preference')->nullable();
            $table->string('smoker')->nullable();
            $table->string('work_schedule')->nullable();
            $table->string('occupation')->nullable();
            $table->integer('prefer_min_age');
            $table->integer('prefer_max_age');
            $table->string('prefer_smoker')->nullable();
            $table->boolean('prefer_student')->nullable();
            $table->boolean('have_cats')->nullable();
            $table->boolean('have_dogs')->nullable();
            $table->boolean('have_small_pets')->nullable();
            $table->boolean('have_birds')->nullable();
            $table->boolean('have_fish')->nullable();
            $table->boolean('have_reptiles')->nullable();
            $table->boolean('prefer_cats')->nullable();
            $table->boolean('prefer_dogs')->nullable();
            $table->boolean('prefer_small_pets')->nullable();
            $table->boolean('prefer_birds')->nullable();
            $table->boolean('prefer_fish')->nullable();
            $table->boolean('prefer_reptiles')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('looking_for_rooms');
    }
}
