<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLookingForApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('looking_for_apartments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('headline')->nullable();
            $table->string('location');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->integer('monthly_rent');
            $table->string('rental_currency');
            $table->boolean('is_short_term');
            $table->date('move_date');
            $table->date('leave_date')->nullable();
            $table->text('description')->nullable();
            $table->boolean('is_studio')->nullable();
            $table->boolean('is_one_bed')->nullable();
            $table->boolean('is_two_bed')->nullable();
            $table->boolean('is_three_bed')->nullable();
            $table->boolean('is_four_bed')->nullable();
            $table->boolean('is_five_bed')->nullable();
            $table->boolean('is_furnished')->nullable();
            $table->boolean('is_high_rise')->nullable();
            $table->boolean('is_low_rise')->nullable();
            $table->boolean('have_disability_access')->nullable();
            $table->boolean('have_doorman')->nullable();
            $table->boolean('have_elevator')->nullable();
            $table->boolean('have_walkup')->nullable();
            $table->boolean('have_health_club')->nullable();
            $table->boolean('have_laundromat')->nullable();
            $table->boolean('have_covered_parking')->nullable();
            $table->boolean('have_garage')->nullable();
            $table->boolean('have_parking_lot')->nullable();
            $table->boolean('have_street_parking')->nullable();
            $table->boolean('have_near_bus_stop')->nullable();
            $table->boolean('have_near_subway')->nullable();
            $table->boolean('have_electronic_security')->nullable();
            $table->boolean('have_security')->nullable();
            $table->boolean('have_swimming_pool')->nullable();
            $table->boolean('have_internet')->nullable();
            $table->boolean('have_wireless_internet')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('looking_for_apartments');
    }
}
